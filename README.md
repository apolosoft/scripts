# Scripts Genéricos y/o especializados de apoyo.

En este repositorio puede encontrar contenido exclusivo para uso técnico.

Puede descargar el script dando click en el archivo deseado o puede descargar todos los scripts dando [click aquí <img src="https://raw.githubusercontent.com/PKief/vscode-material-icon-theme/master/icons/url.svg" alt="" width="27" height="27" class="jop-noMdConv">](https://gitlab.com/apolosoft/scripts/-/archive/master/scripts-master.zip)

|     |     |
| --- | --- |
| **MODULO** | **DETALLE** |
| [![](https://raw.githubusercontent.com/PKief/vscode-material-icon-theme/master/icons/folder-content.svg)](https://gitlab.com/apolosoft/scripts/-/blob/master/CONTABLE/README.md)<br>***CONTABLE*** | Scripts relacionados a ámbitos contables como por ejemplo:<br>\- Patrones<br>\- Plan de cuentas.<br>\- Movimiento.<br>\- Comprobantes<br>Entre otros.. |

> ## ***En cada Carpeta de modulo encontrara un indice que le permitirá saber que hace cada Script y/o descargarlo.***
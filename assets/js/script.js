"use strict";

function createCookie(name, value, minutes = 15) {
  let date = new Date();
  date.setTime(date.getTime() + minutes * 60 * 1000);
  let expires = `; expires=${date.toUTCString()}`;
  document.cookie = `${name}=${value}${expires}; path=/`.toString();
}

function getCookie(name) {
  let decodeCookie = decodeURIComponent(document.cookie);
  let cookieList = decodeCookie.split(";");
  let value = null;
  cookieList.forEach((element) => {
    let pair = element.trim().split("=", 2);
    if (name == pair[0].trim()) {
      value = pair[1].trim();
    }
  });
  return value;
}

//class manipulations - needed if classList is not supported
function hasClass(el, className) {
  if (el.classList) return el.classList.contains(className);
  else
    return !!el.className.match(new RegExp("(\\s|^)" + className + "(\\s|$)"));
}
function addClass(el, className) {
  var classList = className.split(" ");
  if (el.classList) el.classList.add(classList[0]);
  else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
  if (classList.length > 1) addClass(el, classList.slice(1).join(" "));
}
function removeClass(el, className) {
  var classList = className.split(" ");
  if (el.classList) el.classList.remove(classList[0]);
  else if (hasClass(el, classList[0])) {
    var reg = new RegExp("(\\s|^)" + classList[0] + "(\\s|$)");
    el.className = el.className.replace(reg, " ");
  }
  if (classList.length > 1) removeClass(el, classList.slice(1).join(" "));
}

function toggleClass(el, className, bool) {
  if (bool) addClass(el, className);
  else removeClass(el, className);
}
//credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
function putCursorAtEnd(el) {
  if (el.setSelectionRange) {
    var len = el.value.length * 2;
    el.focus();
    el.setSelectionRange(len, len);
  } else {
    el.value = el.value;
  }
}
class ModalSignin {
  constructor(element) {
    this.element = element;
    this.blocks = this.element.getElementsByClassName("js-signin-modal-block");
    this.switchers = this.element
      .getElementsByClassName("js-signin-modal-switcher")[0]
      .getElementsByTagName("a");
    this.triggers = document.getElementsByClassName("js-signin-modal-trigger");
    this.hidePassword = this.element.getElementsByClassName("js-hide-password");
    this.init();
  }

  init() {
    var self = this;
    //open modal/switch form
    for (var i = 0; i < this.triggers.length; i++) {
      (function (i) {
        self.triggers[i].addEventListener("click", function (event) {
          if (event.target.hasAttribute("data-signin")) {
            event.preventDefault();
            self.showSigninForm(event.target.getAttribute("data-signin"));
          }
        });
      })(i);
    }

    //hide/show password
    for (var i = 0; i < this.hidePassword.length; i++) {
      (function (i) {
        self.hidePassword[i].addEventListener("click", function (event) {
          self.togglePassword(self.hidePassword[i]);
        });
      })(i);
    }
  }

  Close() {
    removeClass(this.element, "cd-signin-modal--is-visible");
  }

  getForm() {
    return this.blocks[0].getElementsByTagName("form")[0];
  }

  togglePassword(target) {
    var password = target.previousElementSibling;
    "password" == password.getAttribute("type")
      ? password.setAttribute("type", "text")
      : password.setAttribute("type", "password");
    target.textContent = "Hide" == target.textContent ? "Show" : "Hide";
    putCursorAtEnd(password);
  }

  showSigninForm(type) {
    // show modal if not visible
    !hasClass(this.element, "cd-signin-modal--is-visible") &&
      addClass(this.element, "cd-signin-modal--is-visible");
    // show selected form
    for (var i = 0; i < this.blocks.length; i++) {
      this.blocks[i].getAttribute("data-type") == type
        ? addClass(this.blocks[i], "cd-signin-modal__block--is-selected")
        : removeClass(this.blocks[i], "cd-signin-modal__block--is-selected");
    }
    //update switcher appearance
    var switcherType = type == "signup" ? "signup" : "login";
    for (var i = 0; i < this.switchers.length; i++) {
      this.switchers[i].getAttribute("data-type") == switcherType
        ? addClass(this.switchers[i], "cd-selected")
        : removeClass(this.switchers[i], "cd-selected");
    }
  }

  toggleError(input, bool) {
    // used to show error messages in the form
    toggleClass(input, "cd-signin-modal__input--has-error", bool);
    toggleClass(
      input.nextElementSibling.nextElementSibling,
      "cd-signin-modal__error--is-visible",
      bool
    );
  }

  varify(password) {
    let key = CryptoJS.AES.decrypt(
      "U2FsdGVkX18GPAdoh7BizaVAnW29aWelWqJidc1vKGs=",
      password
    ).toString(CryptoJS.enc.Utf8);
    let pair = CryptoJS.AES.decrypt(
      "U2FsdGVkX19VMwde52buRs8oviu8vBySBicIy7PVljs=",
      password
    ).toString(CryptoJS.enc.Utf8);
    if (pair == key && pair && key) {
      createCookie("activeSession", true);
      createCookie("activeSessionPass", password);
      createCookie("sessionKey", "U2FsdGVkX19Ewmv7D8XvbprGBrDQOYVJbwjbCwPqUTk");
      return true;
    }
    return false;
  }
}

document.addEventListener(
  "onload",
  (function () {
    let content = document
      .getElementsByTagName("body")[0]
      .removeChild(document.getElementsByClassName("table-users")[0]);

    if (CryptoJS) {
      let flag = Boolean(getCookie("activeSession"));
      let password = flag ? String(getCookie("activeSessionPass")) : null;

      if (flag) {
        password = password.trim();
        let cookieKey = CryptoJS.AES.decrypt(
          "U2FsdGVkX19dbw4s8vJtQGULXYQlv4+SgcwEOZj3MTw=",
          password
        ).toString(CryptoJS.enc.Utf8);
        let sessionKey =
          CryptoJS.AES.decrypt(getCookie("sessionKey"), password).toString(
            CryptoJS.enc.Utf8
          );        
        if (cookieKey == sessionKey) {
          document.getElementsByTagName("body")[0].appendChild(content);
        }
      }

      if (!password) {
        let signinModal = document.getElementsByClassName("js-signin-modal")[0];
        let Modal;
        if (signinModal) {
          Modal = new ModalSignin(signinModal);
        }
        Modal.showSigninForm("login");

        Modal.getForm().addEventListener("submit", function (event) {
          event.preventDefault();

          let active = Modal.varify(event.target[0].value);
          if (active) {
            Modal.Close();
            document.getElementsByTagName("body")[0].appendChild(content);
          }
          {
            Modal.toggleError(document.getElementById("signin-password"), true);
            setTimeout(
              () =>
                Modal.toggleError(
                  document.getElementById("signin-password"),
                  false
                ),
              3000
            );
          }
        });
      }
    }
  })()
);

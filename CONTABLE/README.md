# CONTENIDO - CONTABLE

Puede descargar el script dando click en el archivo deseado o puede descargar todos los scripts dando [click aquí <img src="https://raw.githubusercontent.com/PKief/vscode-material-icon-theme/master/icons/url.svg" alt="" width="27" height="27" class="jop-noMdConv">](https://gitlab.com/apolosoft/scripts/-/archive/master/scripts-master.zip?path=CONTABLE) 

|     |     |     |
| --- | --- | --- |
| **SCRIPT** |  --- | **DETALLE** |
| ***GENERADO_N.SQL*** | [<img src="https://raw.githubusercontent.com/PKief/vscode-material-icon-theme/master/icons/folder-download.svg" alt="" width="50" height="50" class="jop-noMdConv">](https://gitlab.com/apolosoft/scripts/-/raw/master/CONTABLE/GENERADO_N.SQL?inline=false) | Script Corrección Comprobantes sin envío.<br>***Problema***<br>Al momento de eliminar un comprobante desde movimiento manual el patrón no queda disponible para generar o editar nuevamente.<br>***Solución***<br>A partir de Electrónica el campo Enviado se toma en cuenta para este proceso, de tal forma que si este es **null**, el comprobante no se marca como **N** en el campo generado.<br>El script marca el campo Enviado como N por defecto si este es null; adicional pone el comprobante como generado **N** si el comprobante fue eliminado en movimiento manual. |

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('_RUB001', 'COMPARA RUBROS', 'Compara las formulas de los rubros con otra base de datos y muestras solo los diferentes', 'EXECUTE BLOCK (
    Pbase CHAR(100) = :Ruta_Base)
RETURNS (
    Rubro     CHAR(5),
    Formula_Actual  CHAR(1000),
    Formula_Anterior CHAR(1000))
AS
DECLARE VARIABLE Vsql CHAR(1000);
DECLARE VARIABLE Usuario CHAR(10);
DECLARE VARIABLE Contra CHAR(10);
BEGIN
  Vsql = ''SELECT Codigo, Formula
              FROM Rubros'';
  FOR EXECUTE STATEMENT Vsql                         
          INTO Rubro,
               Formula_Actual
  DO
  BEGIN
    Formula_Anterior = NULL;
    Vsql = ''SELECT Formula
              FROM Rubros
              WHERE Codigo='''''' || Rubro || '''''''';
    EXECUTE STATEMENT Vsql ON EXTERNAL Pbase AS USER ''TECNICO'' PASSWORD ''TECNICO''
        INTO Formula_Anterior;
    IF (Formula_Actual <> Formula_Anterior) THEN
    BEGIN
      Formula_Actual = TRIM(Formula_Actual);
      Formula_Anterior = TRIM(Formula_Anterior);
      SUSPEND;
    END
  END
END', 'N', 'NOMINA', NULL, 'N', 'PROCESOS TECNICOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODDEBITO, CODCREDITO, CODCONJUNTO, CODCOMPONENTE, CODP_PPTO, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES ('PMH01', 'PROMEDIO ANUAL DE VACACIONES MANUAL', 'ADICION', 'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N')
                    ;


COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PM001' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PMH01'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

UPDATE OR INSERT INTO HOMOLOGACION_RUBROS (CODRUBRO1, CODRUBRO2, ACUMULA)
                                   VALUES ('PM001', 'PMH01', 'N')
                                 ;


COMMIT WORK;

UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODDEBITO, CODCREDITO, CODCONJUNTO, CODCOMPONENTE, CODP_PPTO, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES ('HM080', 'SALARIO VACACIONES SIN PROMEDIO', 'ADICION', 'N', 'N', 'N', 'IF (VA012>=1) THEN
     RESULT:=(ROUND(SAL_BASICO/30*DIAS))
ELSE
    RESULT:=(0)', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N')
                    ;


COMMIT WORK;

UPDATE OR INSERT INTO HOMOLOGACION_RUBROS (CODRUBRO1, CODRUBRO2, ACUMULA)
                                   VALUES ('VA080', 'HM080', 'N')
                                 ;
COMMIT WORK;

UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODDEBITO, CODCREDITO, CODCONJUNTO, CODCOMPONENTE, CODP_PPTO, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES ('HM063', 'PROVISION VACACIONES SIN PROMEDIO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_ARL=1) THEN
  RESULT:=(0)
ELSE
IF(ES_ALTOR=1) THEN
  RESULT:=(ROUND(((SAL_BASICO/30)*DI015)/C19_PR_VAC))
ELSE 
  RESULT:=(ROUND(((SAL_BASICO/30)*DI015)/C20_PR_VAC))', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N')
                    ;


COMMIT WORK;

UPDATE OR INSERT INTO HOMOLOGACION_RUBROS (CODRUBRO1, CODRUBRO2, ACUMULA)
                                   VALUES ('PR063', 'HM063', 'N')
                                 ;
COMMIT WORK;

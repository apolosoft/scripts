UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('_NOM001', 'COMPARA NOMINAS', 'Compara los valores de planillas de una nomina en 2 BD distintas y muestra solo las diferencias', 'EXECUTE BLOCK (
    Pbase   CHAR(100) = :Ruta_Base,
    Pnomina CHAR(5) = :Nomina)
RETURNS (
    Codigo         CHAR(15),
    Nombre         CHAR(83),
    Rubro          CHAR(5),
    Nombre_Rubro   CHAR(80),
    Valor_Actual   NUMERIC(17,4),
    Valor_Anterior NUMERIC(17,4))
AS
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN
  Vsql = ''SELECT Em.codigo, Em.Nombre, P.Codrubro, R.Nombre, SUM(P.Adicion + P.Deduccion)
            FROM Planillas P
            JOIN Rubros R ON (P.Codrubro = R.Codigo)
            JOIN personal Em ON (P.codpersonal=Em.Codigo)
            WHERE P.Codnomina ='''''' || Pnomina || ''''''
            GROUP BY 1, 2, 3, 4'';
  FOR EXECUTE STATEMENT Vsql
          INTO Codigo,
               Nombre,
               Rubro,
               Nombre_Rubro,
               Valor_Actual
  DO
  BEGIN
    Valor_Anterior = 0;
    Vsql = ''SELECT SUM(P.Adicion + P.Deduccion)
            FROM Planillas P
            JOIN Rubros R ON (P.Codrubro = R.Codigo)
            WHERE P.Codnomina ='''''' || Pnomina || ''''''
            AND P.Codrubro='''''' || Rubro || ''''''
            AND P.codpersonal='''''' || Codigo || '''''''';
    EXECUTE STATEMENT Vsql ON EXTERNAL Pbase AS USER ''TECNICO'' PASSWORD ''TECNICO''
        INTO Valor_Anterior;
    IF (Valor_Actual <> COALESCE(Valor_Anterior, 0)) THEN
      SUSPEND;
  END
END', 'N', 'NOMINA', NULL, 'N', 'PROCESOS TECNICOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;

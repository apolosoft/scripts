SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Mov_Patrones (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Renglon              INTEGER,
    Codconcepto          VARCHAR(20),
    Concepto             VARCHAR(80),
    Bruto                DOUBLE PRECISION,
    Tarifa               NUMERIC(15,4),
    Impuesto             NUMERIC(17,4),
    Valor                NUMERIC(17,4),
    Porcentaje_Descuento NUMERIC(15,4),
    Descuento            NUMERIC(17,4),
    Cantidad             NUMERIC(17,4),
    Nota                 VARCHAR(200))
AS
DECLARE VARIABLE V_Id            INTEGER;
DECLARE VARIABLE V_Fecha         DATE;

DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
BEGIN
  -- ID
  SELECT Id,
         Fecha
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Id,
       V_Fecha;

  FOR SELECT Renglon,
             TRIM(Codconcepto),
             Bruto,
             Impuesto,
             Valor,
             Descuento,
             Cantidad,
             TRIM(Nota)
      FROM Auto_Movimiento
      WHERE Id = :V_Id
            AND Valor > 0
      INTO :Renglon,
           :Codconcepto,
           :Bruto,
           :Impuesto,
           :Valor,
           :Descuento,
           :Cantidad,
           :Nota
  DO
  BEGIN
    -- Concepto
    SELECT TRIM(Nombre)
    FROM Auto_Conceptos
    WHERE Codigo = :Codconcepto
    INTO Concepto;

    -- Tarifa
    SELECT Codtipoimpuesto
    FROM Auto_Impuestos
    WHERE Id = :V_Id
          AND Renglon = :Renglon
    INTO V_Tipo_Impuesto;

    SELECT Tarifa
    FROM Data_Impuestos
    WHERE Codtipoimpuesto = :V_Tipo_Impuesto
          AND Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO Tarifa;

    -- Descuento
    Porcentaje_Descuento = :Descuento * 100 / (:Bruto + :Descuento);

    SUSPEND;
  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON AUTO_COMP TO PROCEDURE PZ_MOV_PATRONES;
GRANT SELECT ON AUTO_MOVIMIENTO TO PROCEDURE PZ_MOV_PATRONES;
GRANT SELECT ON AUTO_CONCEPTOS TO PROCEDURE PZ_MOV_PATRONES;
GRANT SELECT ON AUTO_IMPUESTOS TO PROCEDURE PZ_MOV_PATRONES;
GRANT SELECT ON DATA_IMPUESTOS TO PROCEDURE PZ_MOV_PATRONES;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_MOV_PATRONES TO SYSDBA;
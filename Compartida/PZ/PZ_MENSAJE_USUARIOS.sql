SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Mensaje_Usuarios (
    Origen_  CHAR(10),
    Destino_ CHAR(10),
    Mensaje_ BLOB SUB_TYPE 1 SEGMENT SIZE 80)
AS
DECLARE VARIABLE V_Destino CHAR(10);
DECLARE VARIABLE V_User    INTEGER;
BEGIN
  -- Valida origen
  SELECT COUNT(1)
  FROM Usuarios
  WHERE Codigo = :Origen_
  INTO V_User;

  IF (V_User > 0) THEN
    IF (TRIM(:Destino_) = '%') THEN
      -- Todos
      FOR SELECT Codigo
          FROM Usuarios
          WHERE Activo = 'S'
          INTO V_Destino
      DO
      BEGIN
        EXECUTE PROCEDURE Mensaje_Inserta(:Origen_, :V_Destino, :Mensaje_);
      END
    ELSE
    BEGIN
      -- Validacion usuario
      SELECT COUNT(1)
      FROM Usuarios
      WHERE Codigo = :Destino_
      INTO V_User;

      IF (V_User > 0) THEN
        EXECUTE PROCEDURE Mensaje_Inserta(:Origen_, :Destino_, :Mensaje_);

    END
END^

SET TERM ; ^

COMMENT ON PROCEDURE PZ_MENSAJE_USUARIOS IS
'Envio mensajes a usuarios
Destino % envia a todos los usuarios activos';

/* Following GRANT statements are generated automatically */

GRANT SELECT ON USUARIOS TO PROCEDURE PZ_MENSAJE_USUARIOS;
GRANT EXECUTE ON PROCEDURE MENSAJE_INSERTA TO PROCEDURE PZ_MENSAJE_USUARIOS;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_MENSAJE_USUARIOS TO SYSDBA;
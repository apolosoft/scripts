SET TERM ^ ;

CREATE OR ALTER PROCEDURE pz_fe_vector_salud_usu (
    tipo_     CHAR(5),
    prefijo_  CHAR(5),
    numero_   CHAR(10),
    modulo_   CHAR(10),
    receptor_ CHAR(15))
RETURNS (
    reps              CHAR(15),
    mcon              CHAR(5),
    cobe              CHAR(5),
    numero_contrato   CHAR(15),
    poliza            CHAR(15),
    copago            NUMERIC(18,2),
    cuota_moderadora  NUMERIC(18,2),
    pagos_compartidos NUMERIC(18,2))
AS
DECLARE VARIABLE v_pagador     CHAR(1);
DECLARE VARIABLE v_resumen     CHAR(10);
DECLARE VARIABLE es_salud      CHAR(1);
DECLARE VARIABLE v_sin_resumen CHAR(1);
BEGIN

  -- Es salud
  SELECT salud
  FROM documentos
  WHERE codigo = :tipo_
  INTO es_salud;
  IF (es_salud = 'N') THEN
    EXIT;

  -- Es sin resumen?
  SELECT sin_resumen
  FROM documentos
  WHERE codigo = :tipo_
  INTO v_sin_resumen;

  -- datos del tercero
  SELECT pagador
  FROM terceros t
  WHERE codigo = :receptor_
  INTO v_pagador;

  IF (:v_sin_resumen = 'S') THEN
    SELECT reps,
           cod_modalidad,
           cod_cobertura,
           numero_contrato,
           poliza,
           copago,
           cuota_moderadora,
           pagos_compartidos
    FROM fe_vector_salud_comprobante(:tipo_, :prefijo_, :numero_, :modulo_)
    INTO reps,
         mcon,
         cobe,
         numero_contrato,
         poliza,
         copago,
         cuota_moderadora,
         pagos_compartidos;

  ELSE
  BEGIN
    IF (:v_pagador = 'N') THEN
      EXIT;

    -- Buscar registro resumen
    SELECT codrips
    FROM comprobantes
    WHERE tipo = :tipo_ AND
          prefijo = :prefijo_ AND
          numero = :numero_
    INTO :v_resumen;

    SELECT reps,
           cod_modalidad,
           cod_cobertura,
           numero_contrato,
           poliza,
           copago,
           cuota_moderadora,
           pagos_compartidos
    FROM fe_vector_salud(:v_resumen) s
    INTO reps,
         mcon,
         cobe,
         numero_contrato,
         poliza,
         copago,
         cuota_moderadora,
         pagos_compartidos;
  END
  SUSPEND;
END
^

SET TERM ; ^
commit work;

CREATE OR ALTER PROCEDURE Pz_Gestion_A_Hoja_Contable (
    Fecha_Desde_ DATE,
    Fecha_Hasta_ DATE)
RETURNS (
    Tipo           VARCHAR(5),
    Prefijo        VARCHAR(5),
    Numero         VARCHAR(10),
    Fecha          DATE,
    Cuenta         VARCHAR(30),
    Tercero        VARCHAR(15),
    Centro         VARCHAR(5),
    Detalle        VARCHAR(80),
    Debito         NUMERIC(17,4),
    Credito        NUMERIC(17,4),
    Base           NUMERIC(17,4),
    Usuario        VARCHAR(10),
    Nombre_Tercero VARCHAR(163),
    Nombre_Centro  VARCHAR(80))
AS
BEGIN
  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Codusuario
      FROM Comprobantes
      WHERE Fecha >= :Fecha_Desde_
            AND Fecha <= :Fecha_Hasta_
      INTO Tipo,
           Prefijo,
           Numero,
           Fecha,
           Usuario
  DO
  BEGIN
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Detalle,
               Debito,
               Credito,
               Base

        FROM Reg_Contable
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND TRIM(COALESCE(Codcuenta, '')) <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Detalle,
             Debito,
             Credito,
             Base

    DO
    BEGIN
      Tercero = COALESCE(Tercero, '');
      Centro = COALESCE(Centro, '');
      Detalle = COALESCE(Detalle, '_');

      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');

      SUSPEND;
    END

    Detalle = 'JUEGO DE INVENTARIOS';
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Debito,
               Credito,
               Base

        FROM Reg_Juego
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND TRIM(COALESCE(Codcuenta, '')) <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Debito,
             Credito,
             Base

    DO
    BEGIN
      Tercero = COALESCE(Tercero, '');
      Centro = COALESCE(Centro, '');
      Detalle = COALESCE(Detalle, '_');

      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');

      SUSPEND;
    END
  END
END
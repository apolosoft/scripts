SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Sel_Conteo_Fisico (
    Fecha_Corte_ DATE,
    Tipo_        VARCHAR(5),
    Prefijo_     VARCHAR(5),
    Numero_      VARCHAR(10),
    Referencia_  VARCHAR(20),
    Bodega_      VARCHAR(5))
RETURNS (
    Referencia        VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Bodega            VARCHAR(5),
    Categoria         VARCHAR(15),
    Lote              VARCHAR(20),
    Linea             VARCHAR(5),
    Nombre_Linea      VARCHAR(80),
    Ubicacion         VARCHAR(20),
    Saldo             DOUBLE PRECISION,
    Ponderado         DOUBLE PRECISION,
    Valor             DOUBLE PRECISION,
    Conteo            DOUBLE PRECISION,
    Valor_Conteo      DOUBLE PRECISION,
    Diferencia        DOUBLE PRECISION)
AS
BEGIN

  FOR SELECT Bodega,
             Referencia,
             Categoria,
             Lote,
             SUM(Saldo),
             SUM(Conteo)
      FROM Pz_Sel_Conteo_Base(:Fecha_Corte_, :Tipo_, :Prefijo_, :Numero_)
      WHERE (TRIM(Referencia) LIKE :Referencia_)
            AND (TRIM(Bodega) LIKE :Bodega_)
      GROUP BY 1, 2, 3, 4
      INTO :Bodega,
           :Referencia,
           :Categoria,
           :Lote,
           :Saldo,
           :Conteo
  DO
  BEGIN
    Categoria = COALESCE(Categoria, 'NA');
    Lote = COALESCE(Lote, 'NA');
    Saldo = COALESCE(Saldo, 0);

    /* Nombre referencia */
    SELECT TRIM(Nombre_Referencia)
    FROM Fn_Nombre_Referencia(:Referencia)
    INTO Nombre_Referencia;

    /* Linea y ubicacion */
    SELECT TRIM(Codlinea),
           TRIM(Ubicacion)
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Linea,
         Ubicacion;

    /* Nombre linea */
    SELECT TRIM(Nombre)
    FROM Lineas
    WHERE Codigo = :Linea
    INTO Nombre_Linea;

    /* Ponderado */
    SELECT Ponderado
    FROM Fn_Ponderado(:Referencia, :Fecha_Corte_)
    INTO Ponderado;
    Ponderado = COALESCE(Ponderado, 0);

    /* Calculos */
    Valor = Saldo * Ponderado;
    Valor_Conteo = Conteo * Ponderado;
    Diferencia = Saldo - Conteo;
    SUSPEND;
  END

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE PZ_SEL_CONTEO_BASE TO PROCEDURE PZ_SEL_CONTEO_FISICO;
GRANT EXECUTE ON PROCEDURE FN_NOMBRE_REFERENCIA TO PROCEDURE PZ_SEL_CONTEO_FISICO;
GRANT SELECT ON REFERENCIAS TO PROCEDURE PZ_SEL_CONTEO_FISICO;
GRANT SELECT ON LINEAS TO PROCEDURE PZ_SEL_CONTEO_FISICO;
GRANT EXECUTE ON PROCEDURE FN_PONDERADO TO PROCEDURE PZ_SEL_CONTEO_FISICO;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_SEL_CONTEO_FISICO TO SYSDBA;
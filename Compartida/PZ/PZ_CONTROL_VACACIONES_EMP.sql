SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Control_Vacaciones_Emp (
    Empleado_    VARCHAR(15),
    Fecha_Corte_ DATE)
RETURNS (
    Empleado        VARCHAR(15),
    Desde           DATE,
    Hasta           DATE,
    Dias_Laborados  INTEGER,
    Dias_Periodo    INTEGER,
    Dias_Vac        INTEGER,
    Dias_Pendientes INTEGER)
AS
DECLARE VARIABLE V_Fecha_Ret      DATE;
DECLARE VARIABLE V_Fecha_Ing      DATE;
DECLARE VARIABLE V_Fecha_Saldos   DATE;
DECLARE VARIABLE V_Nuevo_Hasta    DATE;
DECLARE VARIABLE V_Dias_No        DOUBLE PRECISION;
DECLARE VARIABLE V_Dias_Laborados DOUBLE PRECISION;
DECLARE VARIABLE V_Total_Vac      DOUBLE PRECISION;
DECLARE VARIABLE V_Hay_Datos      CHAR(2);
DECLARE VARIABLE V_Num_Reg        INTEGER;
DECLARE VARIABLE V_Clase_Lab      VARCHAR(30) = 'C_DIAS_LABORADOS_VACACIONES';
DECLARE VARIABLE V_Clase_No_Vac   VARCHAR(30) = 'C_NO_VACACIONES';

BEGIN
  -- Fechas de Salarios
  SELECT Ingreso,
         Retiro
  FROM Pz_Fecha_Salarios(:Empleado_)
  INTO V_Fecha_Ing,
       V_Fecha_Ret;

  V_Fecha_Ret = COALESCE(V_Fecha_Ret, V_Fecha_Ing);

  IF (V_Fecha_Ing >= V_Fecha_Ret) THEN
  BEGIN
    V_Num_Reg = 0;
    Empleado = Empleado_;
    Desde = :V_Fecha_Ing;

    -- Dias total vacaciones
    SELECT Valor
    FROM Pz_Dias_Vacaciones(:Empleado_, :V_Fecha_Ing, :Fecha_Corte_)
    INTO V_Total_Vac;

    --  Datos para primer registro segun saldos iniciales
    SELECT FIRST 1 Fecha
    FROM Nominas
    WHERE Codliquidacion = 'LQ0INI'
    ORDER BY Fecha
    INTO V_Fecha_Saldos;

    IF (:V_Fecha_Ing < :V_Fecha_Saldos) THEN
    BEGIN
      V_Num_Reg = :V_Num_Reg + 1;
      Desde = :V_Fecha_Ing;
      Hasta = :V_Fecha_Saldos;

      SELECT Valor
      FROM Pz_Valor_Clase(:Empleado_, :Desde, :Hasta + 1, :V_Clase_Lab)
      INTO Dias_Laborados;

      Dias_Periodo = Dias_Laborados * 15 / 360;
      IF (:Dias_Periodo > :V_Total_Vac) THEN
        Dias_Vac = :V_Total_Vac;
      ELSE
        Dias_Vac = :Dias_Periodo;

      V_Total_Vac = :V_Total_Vac - :Dias_Vac;
      Dias_Pendientes = Dias_Periodo - Dias_Vac;

      SUSPEND;
    END

    V_Hay_Datos = 'S';
    WHILE (V_Hay_Datos = 'S') DO
    BEGIN
      V_Num_Reg = V_Num_Reg + 1;

      IF (:V_Num_Reg = 2) THEN
      BEGIN
        V_Nuevo_Hasta = :V_Fecha_Ing;
        WHILE (V_Nuevo_Hasta < :V_Fecha_Saldos) DO
        BEGIN
          V_Nuevo_Hasta = DATEADD(1 YEAR TO :V_Nuevo_Hasta);
        END
        Desde = Hasta;
        Hasta = :V_Nuevo_Hasta;
        SELECT Valor
        FROM Pz_Valor_Clase(:Empleado_, :Desde + 1, :Hasta - 1, :V_Clase_Lab)
        INTO V_Dias_Laborados;

      END
      ELSE
      BEGIN
        Desde = :Hasta;
        IF (:Fecha_Corte_ - :Desde < 365) THEN
          Hasta = :Fecha_Corte_;
        ELSE
          Hasta = DATEADD(1 YEAR TO :Desde);

        SELECT Valor
        FROM Pz_Valor_Clase(:Empleado_, :Desde, :Hasta - 1, :V_Clase_Lab)
        INTO V_Dias_Laborados;
      END

      SELECT Valor
      FROM Pz_Valor_Clase(:Empleado_, :Desde, :Hasta - 1, :V_Clase_No_Vac)
      INTO V_Dias_No;

      V_Dias_No = COALESCE(:V_Dias_No, 0);
      V_Dias_Laborados = V_Dias_Laborados - :V_Dias_No;

      Dias_Periodo = V_Dias_Laborados * 15 / 360;

      IF (:Dias_Periodo > :V_Total_Vac) THEN
        Dias_Vac = :V_Total_Vac;
      ELSE
        Dias_Vac = :Dias_Periodo;

      V_Total_Vac = :V_Total_Vac - :Dias_Vac;

      Dias_Pendientes = Dias_Periodo - Dias_Vac;

      Dias_Laborados = ROUND(:V_Dias_Laborados);
      IF (V_Dias_Laborados > 0) THEN
        SUSPEND;

      IF (:Hasta >= :Fecha_Corte_) THEN
        V_Hay_Datos = 'N';
      Dias_Vac = 0;

    END

  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE PZ_FECHA_SALARIOS TO PROCEDURE PZ_CONTROL_VACACIONES_EMP;
GRANT EXECUTE ON PROCEDURE PZ_DIAS_VACACIONES TO PROCEDURE PZ_CONTROL_VACACIONES_EMP;
GRANT SELECT ON NOMINAS TO PROCEDURE PZ_CONTROL_VACACIONES_EMP;
GRANT EXECUTE ON PROCEDURE PZ_VALOR_CLASE TO PROCEDURE PZ_CONTROL_VACACIONES_EMP;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_CONTROL_VACACIONES_EMP TO SYSDBA;
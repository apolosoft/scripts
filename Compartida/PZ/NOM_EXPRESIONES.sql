CREATE OR ALTER PROCEDURE Nom_Expresiones (
    Nomina_      CHAR(5),
    Liquidacion_ CHAR(10))
RETURNS (
    Empleado  CHAR(15),
    Expresion CHAR(10),
    Valor     DOUBLE PRECISION,
    Nomina    CHAR(5))
AS
DECLARE VARIABLE V_Resultado INTEGER;
DECLARE VARIABLE V_Sql       VARCHAR(3000);
BEGIN

  /* GUARDA EN VARIABLE GLOBAL */
  SELECT Rdb$Set_Context('USER_SESSION', 'G_NOMINA', :Nomina_)
  FROM Rdb$Database
  INTO :V_Resultado;

  /* LEE LA VARIABLE LOCAL */
  SELECT Rdb$Get_Context('USER_SESSION', 'G_NOMINA')
  FROM Rdb$Database
  INTO :Nomina;

  FOR SELECT Codigo,
             TRIM(Sql_Bloque)
      FROM Nom_Expresiones_Base(:Liquidacion_)
      INTO :Expresion,
           :V_Sql
  DO
  BEGIN

    Empleado = NULL;
    Valor = NULL;

    /* las expresiones deben hacer referencia a la tabla tmp_empleado */

    FOR EXECUTE STATEMENT V_Sql
            INTO :Empleado, :Valor
    DO
    BEGIN

      IF (Valor IS NULL) THEN
        Valor = 0;

      SUSPEND;

    END

  END
END
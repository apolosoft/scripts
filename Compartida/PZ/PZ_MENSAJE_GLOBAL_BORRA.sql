SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Mensaje_Global_Borra
AS
BEGIN
  UPDATE Mensajes M
  SET Visto = 'S'
  WHERE M.Codusuario_I IS NULL
        AND Visto = 'N';
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT,UPDATE ON MENSAJES TO PROCEDURE PZ_MENSAJE_GLOBAL_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_MENSAJE_GLOBAL_BORRA TO SYSDBA;
CREATE OR ALTER PROCEDURE Rest_Insert_Gestion_Primario_Bloque (
    Tipo_     VARCHAR(5),
    Prefijo_  VARCHAR(5),
    Numero_   VARCHAR(10),
    Fecha_    DATE,
    Vence_    DATE,
    Tercero_  VARCHAR(15),
    Vendedor_ VARCHAR(15),
    Lista_    VARCHAR(5),
    Banco_    VARCHAR(15),
    Usuario_  VARCHAR(10),
    Detalle_  VARCHAR(300))
AS
DECLARE VARIABLE V_Plazo          INTEGER;
DECLARE VARIABLE V_Renglon        INTEGER;
DECLARE VARIABLE V_Registro       INTEGER;
DECLARE VARIABLE V_Fecha          DATE;
DECLARE VARIABLE V_Doc            VARCHAR(20);
DECLARE VARIABLE V_Descuento      NUMERIC(17,4);
DECLARE VARIABLE V_Bloqueado      VARCHAR(1);
DECLARE VARIABLE V_Centro         CHAR(5);
DECLARE VARIABLE V_Bodega         CHAR(5);
DECLARE VARIABLE V_Referencia     CHAR(20);
DECLARE VARIABLE V_Entrada        NUMERIC(17,4);
DECLARE VARIABLE V_Salida         NUMERIC(17,4);
DECLARE VARIABLE V_Unitario       NUMERIC(17,4);
DECLARE VARIABLE V_Porc_Descuento NUMERIC(17,4);
DECLARE VARIABLE V_Nota           VARCHAR(200);
BEGIN

  V_Doc = TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_);

  /* Validamos que no exista el doc */
  SELECT COUNT(1)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN

    /* Valida que el tercero exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

    /* Valida que el vendedor exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Empleado(:Vendedor_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Vendedor_) || ' NO EXISTE ***';

    /* Valida que la caja o banco exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Banco(:Banco_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA CAJA o BANCO ' || TRIM(Banco_) || ' NO EXISTE ***';

    /* Valida que la lista de precios exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Lista(:Lista_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA LISTA DE PRECIOS ' || TRIM(Lista_) || ' NO EXISTE ***';

    /* Valida que el usuario exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

    /* Valida que el centro exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';

    /* Valida que la bodega exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Bodega(:Bodega_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA BODEGA ' || TRIM(Bodega_) || ' NO EXISTE ***';

    /* Valida que la referencia exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Referencia(:Referencia_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA REFERENCIA ' || TRIM(Referencia_) || ' NO EXISTE ***';

    /* Hallamos plazo */
    IF (:Vence_ > :Fecha_) THEN
      V_Plazo = :Vence_ - :Fecha_;
    ELSE
      V_Plazo = 0;

    /*bloqueado char(1)
   si es positivo = R
   si es negativo = N*/
    IF (:Numero_ < 0) THEN
      V_Bloqueado = 'N';
    ELSE
      V_Bloqueado = 'R';

    /* Insertamos en COMPROBANTES */
    INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Plazo, Vence, Nota, Bloqueado, Codtercero, Codvendedor, Codlista, Codbanco, Codusuario,
                              Codescenario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :Fecha_, :V_Plazo, :Vence_, '-', :V_Bloqueado, :Tercero_, :Vendedor_, :Lista_, :Banco_, :Usuario_, 'NA');

    V_Renglon = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                 FROM Rdb$Database);

    /* Insertamos TR_INVENTARIO */
    /*Calcular el descuento en base al porc_descuento*/
    V_Descuento = ((:Porc_Descuento_ / 100) * :Unitario_);

    INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codcentro, Codbodega, Codreferencia, Entrada, Salida, Unitario, Porcentaje_Descuento,
                               Descuento, Nota, Codusuario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Centro_, :Bodega_, :Referencia_, :Entrada_, :Salida_, :Unitario_, :Porc_Descuento_,
            :V_Descuento, :Nota_, :Usuario_);

    /* Con base al unitario(con o sin IVA) hallamos el BRUTO y demás cálculos */
    EXECUTE PROCEDURE Fx_Recalcula_Registro(:Tipo_, :Prefijo_, :Numero_, :V_Renglon);

  END

  ELSE
  /*Aquí encuentra que el documento ya existe y continua insertando renglones*/
  BEGIN

    /* Validamos la fecha del documento existente. Si es la misma del ws continuamos insertando. */
    SELECT Fecha
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Fecha;

    IF (V_Fecha = :Fecha_) THEN
    BEGIN /* Validamos que el doc no esté bloqueado */
      SELECT COUNT(1)
      FROM Comprobantes
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
            AND (Bloqueado = 'R' OR Bloqueado = 'N')
      INTO V_Registro;

      IF (V_Registro = 0) THEN
        EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' SE ENCUENTRA TERMINADO, POR FAVOR DESBLOQUEAR PARA CONTINUAR ***';
      ELSE


      /* Valida que el centro exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

      /* Valida que el vendedor exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Empleado(:Vendedor_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Vendedor_) || ' NO EXISTE ***';

      /* Valida que la caja o banco exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Banco(:Banco_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA CAJA o BANCO ' || TRIM(Banco_) || ' NO EXISTE ***';

      /* Valida que la lista de precios exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Lista(:Lista_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA LISTA DE PRECIOS ' || TRIM(Lista_) || ' NO EXISTE ***';

      /* Valida que el usuario exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

      /* Valida que el centro exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';

      /* Valida que la bodega exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Bodega(:Bodega_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA BODEGA ' || TRIM(Bodega_) || ' NO EXISTE ***';

      /* Valida que la referencia exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Referencia(:Referencia_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA REFERENCIA ' || TRIM(Referencia_) || ' NO EXISTE ***';

      V_Renglon = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                   FROM Rdb$Database);

      /* Insertamos TR_INVENTARIO */
      /*Calcular el descuento en base al porc_descuento*/
      V_Descuento = ((:Porc_Descuento_ / 100) * :Unitario_);

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codcentro, Codbodega, Codreferencia, Entrada, Salida, Unitario, Porcentaje_Descuento,
                                 Descuento, Nota, Codusuario)
      VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Centro_, :Bodega_, :Referencia_, :Entrada_, :Salida_, :Unitario_, :Porc_Descuento_,
              :V_Descuento, :Nota_, :Usuario_);

      /* Con base al unitario(con o sin IVA) hallamos el BRUTO y demás cálculos */
      EXECUTE PROCEDURE Fx_Recalcula_Registro(:Tipo_, :Prefijo_, :Numero_, :V_Renglon);
    END
    ELSE
      EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' YA EXISTE ***';
  END
END
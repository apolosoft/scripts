SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Auto_Comp (
    Tipo_     CHAR(5),
    Prefijo_  CHAR(5),
    Numero_   CHAR(10),
    Emisor_   VARCHAR(15),
    Receptor_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(10),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(200),
    "noov:Nvema_copi" VARCHAR(200),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80))
AS
DECLARE VARIABLE V_Id              INTEGER;
DECLARE VARIABLE V_Plazo           INTEGER;
DECLARE VARIABLE V_Codidentidad    CHAR(5);
DECLARE VARIABLE V_Fecha           CHAR(19);
DECLARE VARIABLE V_Vence           CHAR(19);
DECLARE VARIABLE V_Aiu             INTEGER;
DECLARE VARIABLE V_Manda           INTEGER;
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Dv              CHAR(1);
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Pais_Fe         VARCHAR(5);
DECLARE VARIABLE V_Muni            VARCHAR(5);
DECLARE VARIABLE V_Nombre_Depa     VARCHAR(80);
DECLARE VARIABLE V_Nombre_Ciud     VARCHAR(80);
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Direccion       VARCHAR(80);
DECLARE VARIABLE V_Telefono        VARCHAR(100);
DECLARE VARIABLE V_Contc           VARCHAR(80);
DECLARE VARIABLE V_Sociedad        VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Fe     VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Nom    VARCHAR(80);
DECLARE VARIABLE V_Autoretenedor   CHAR(1);
DECLARE VARIABLE V_Jerarquia       INTEGER;
DECLARE VARIABLE V_Empresa         VARCHAR(80);
DECLARE VARIABLE V_Nom1            VARCHAR(20);
DECLARE VARIABLE V_Nom2            VARCHAR(20);
DECLARE VARIABLE V_Apl             VARCHAR(40);
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Orden           VARCHAR(40);
DECLARE VARIABLE V_Remision        VARCHAR(40);
DECLARE VARIABLE V_Recepcion       VARCHAR(40);
DECLARE VARIABLE V_Ean             VARCHAR(40);
DECLARE VARIABLE V_Centro          VARCHAR(40);
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Tiporef         VARCHAR(5);
DECLARE VARIABLE V_Prefijoref      VARCHAR(5);
DECLARE VARIABLE V_Fecha_Ref       CHAR(19);
DECLARE VARIABLE V_Numeroref       VARCHAR(10);
DECLARE VARIABLE V_Docuref         VARCHAR(20);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     CHAR(1);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Mail_1          VARCHAR(250);
DECLARE VARIABLE V_Mail_2          VARCHAR(250);
DECLARE VARIABLE V_Leng_Mail       INTEGER;
DECLARE VARIABLE V_Leng_Otro_Mail  INTEGER;
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Auto_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  SELECT Vence
  FROM Val_Documentos
  WHERE Coddocumento = :Tipo_
        AND Codprefijo = :Prefijo_
  INTO V_Plazo;

  SELECT Id,
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Fecha + COALESCE(:V_Plazo, 0)) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || 'T' || SUBSTRING(Hora FROM 1 FOR 8)
  FROM Fe_Apo_Comprobante(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Id, V_Fecha, V_Vence;

  --Terceros
  SELECT Codidentidad, Naturaleza, Dv, Codpais, Codmunicipio, Direccion, COALESCE(TRIM(Telefono), '') || ', ' || COALESCE(Movil, ''), Codsociedad,
         Empresa, Nom1, Nom2, COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''), Email,
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Codidentidad, V_Naturaleza, V_Dv, V_Pais, V_Muni, V_Direccion, V_Telefono, V_Sociedad, V_Empresa, V_Nom1, V_Nom2, V_Apl, V_Mail_1,
       V_Postal_Code;

  --Pais
  SELECT Codigo_Fe
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO V_Pais_Fe;

  --Departamento
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Muni FROM 1 FOR 2)
  INTO V_Nombre_Depa;

  --Municipio
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = :V_Muni
  INTO V_Nombre_Ciud;

  --AIU
  SELECT COUNT(*)
  FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_) P
  WHERE (TRIM("noov:Nvpro_codi") STARTING WITH 'AIU')
        AND (P."noov:Nvfac_cant" * P."noov:Nvfac_valo") > 0
  INTO V_Aiu;

  --Mandatario
  SELECT COUNT(*)
  FROM Auto_Comp M
  JOIN Centros C ON (M.Codcentro = C.Codigo)
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND COALESCE(C.Codtercero, '') <> ''
  INTO V_Manda;

  --Documentos
  SELECT Codigo_Fe, Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu, V_Valida_Fe;

  --Documento ref
  SELECT Tiporef, Prefijoref, Numeroref
  FROM Notas_Contable
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tiporef, V_Prefijoref, V_Numeroref;

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tiporef
  INTO V_Docuref;

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00'
  FROM Auto_Comp
  WHERE Tipo = :V_Tiporef
        AND Prefijo = :V_Prefijoref
        AND Numero = :V_Numeroref
  INTO V_Fecha_Ref;

  --Contacto
  SELECT FIRST 1 Nombre
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO V_Contc;
  V_Contc = COALESCE(V_Contc, '');

  --Otro email
  FOR SELECT Email
      FROM Contactos
      WHERE Codtercero = :Receptor_
            AND Codcargo = 'ZZZ'
      ORDER BY Codigo
      INTO V_Email_Otro
  DO
  BEGIN
    V_Email_Otro = COALESCE(V_Email_Otro, '');
    V_Leng_Mail = CHAR_LENGTH(TRIM(V_Mail_1));
    V_Leng_Otro_Mail = CHAR_LENGTH(TRIM(V_Email_Otro));
    IF (:V_Leng_Mail + :V_Leng_Otro_Mail > 199) THEN
    BEGIN
      V_Leng_Mail = CHAR_LENGTH(TRIM(V_Mail_2));
      IF (:V_Leng_Mail + :V_Leng_Otro_Mail < 199) THEN
        V_Mail_2 = TRIM(:V_Mail_2) || IIF(TRIM(V_Email_Otro) = '', '', ';' || :V_Email_Otro);
    END
    ELSE
      V_Mail_1 = TRIM(:V_Mail_1) || IIF(TRIM(V_Email_Otro) = '', '', ';' || :V_Email_Otro);
  END
  IF (CHAR_LENGTH(TRIM(V_Mail_2)) > 0) THEN
    V_Mail_2 = SUBSTRING(:V_Mail_2 FROM 2);

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Sociedades
  SELECT Codigo_Fe, Nombre, Autoretenedor, Jerarquia
  FROM Sociedades
  WHERE Codigo = :V_Sociedad
  INTO V_Sociedad_Fe, V_Sociedad_Nom, V_Autoretenedor, V_Jerarquia;

  --Auto Comp
  SELECT Nota, Concepto
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Nota, V_Concepto_Nc;

  --Mega nota
  SELECT LIST(TRIM(C.Nota), IIF(RIGHT(TRIM(C.Nota), 1) = '.', ASCII_CHAR(13), ' '))
  FROM Auto_Movimiento C
  JOIN Auto_Comp C1 USING (Id)
  WHERE (C1.Tipo = :Tipo_
        AND C1.Prefijo = :Prefijo_
        AND C1.Numero = :Numero_)
        AND (C.Codconcepto = 'MEGANOTA')
        AND (TRIM(COALESCE(C.Nota, '')) <> '')
  INTO V_Meganota;

  --Anexos
  SELECT Orden, Remision, Recepcion, Ean, Centro
  FROM Anexo_Contabilidad
  WHERE Id = :V_Id
  INTO V_Orden, V_Remision, V_Recepcion, V_Ean, V_Centro;

  --Impuestos
  SELECT SUM((SELECT Valor
              FROM Redondeo_Dian("noov:Nvimp_valo", 2)))
  FROM Pz_Fe_Auto_Imp(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  -------
  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvres_nume" = (SELECT Numero
                       FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_));
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');
  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);
  "noov:Nvfac_fech" = V_Fecha;
  "noov:Nvfac_venc" = V_Vence;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'
                      END;
  "noov:Nvfac_cdet" = (SELECT COUNT(1)
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_)
                       WHERE ("noov:Nvfac_cant" * "noov:Nvfac_valo") > 0);
  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Aiu <> 0 THEN '11'
                                              WHEN V_Manda <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Aiu <> 0 THEN '15'
                                                   WHEN V_Manda <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Aiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Aiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;
  "noov:Nvven_nomb" = '';
  "noov:Nvfac_fpag" = 'ZZZ';
  "noov:Nvfac_conv" = IIF(COALESCE(V_Plazo, 0) <> 0, '2', '1');
  "noov:Nvcli_cper" = IIF(V_Naturaleza = 'N', 2, 1);
  "noov:Nvcli_cdoc" = V_Codidentidad;
  "noov:Nvcli_docu" = TRIM(Receptor_) || IIF(TRIM(V_Codidentidad) = '31', IIF(V_Dv IS NULL, '', '-' || TRIM(V_Dv)), '');
  "noov:Nvcli_pais" = V_Pais_Fe;
  "noov:Nvcli_depa" = V_Nombre_Depa;
  "noov:Nvcli_ciud" = TRIM(V_Nombre_Ciud) || IIF(V_Docu = 'EXPORTACION', '', '@' || TRIM(V_Muni));
  "noov:Nvcli_loca" = '';
  "noov:Nvcli_dire" = V_Direccion;
  "noov:Nvcli_ntel" = V_Telefono;
  "noov:Nvcli_regi" = V_Sociedad_Fe;
  "noov:Nvcli_fisc" = IIF(UPPER(TRIM(V_Sociedad_Nom)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(V_Autoretenedor = 'S', 'O-15' || IIF(V_Jerarquia > 5, ';O-13', ''), IIF(V_Jerarquia > 5, 'O-13', 'R-99-PN')));
  "noov:Nvcli_nomb" = COALESCE(TRIM(V_Empresa), '');
  "noov:Nvcli_pnom" = COALESCE(TRIM(V_Nom1), '');
  "noov:Nvcli_snom" = COALESCE(TRIM(V_Nom2), '');
  "noov:Nvcli_apel" = V_Apl;
  "noov:Nvcli_mail" = :V_Mail_1;
  "noov:Nvema_copi" = :V_Mail_2;
  "noov:Nvfac_obse" = SUBSTRING(TRIM(V_Nota) || IIF(TRIM(COALESCE(V_Meganota, '')) = '', '', ', ' || TRIM(V_Meganota)) FROM 1 FOR 4000);
  "noov:Nvfac_orde" = COALESCE(V_Orden, '');
  "noov:Nvfac_remi" = COALESCE(V_Remision, '');
  "noov:Nvfac_rece" = COALESCE(V_Recepcion, '');
  "noov:Nvfac_entr" = COALESCE(V_Ean, '');
  "noov:Nvfac_ccos" = COALESCE(V_Centro, '');
  "noov:Nvfac_stot" = (SELECT SUM("noov:Nvfac_stot")
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_));

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";
  "noov:Nvfac_totp" = "noov:Nvfac_stot" + V_Impuestos;
  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_obsb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvfac_tcru" = 'R';
    "noov:Nvfac_fecb" = V_Fecha_Ref;
    "noov:Nvcon_codi" = V_Concepto_Nc;
    -- Nombre concepto
    IF (V_Docu = 'NOTA CREDITO') THEN
      SELECT Nombre
      FROM Notas_Cre
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DEBITO') THEN
      SELECT Nombre
      FROM Notas_Deb
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
      SELECT Nombre
      FROM Notas_Ds
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    "noov:Nvfac_numb" = '';

    IF (TRIM(V_Prefijoref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijoref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numeroref);

    IF (V_Valida_Fe = 'N') THEN
    BEGIN
      "noov:Nvfac_tcru" = 'L';
      "noov:Nvfac_fecb" = V_Fecha;
      "noov:Nvfac_numb" = '';
    END
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END

  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_TRM TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON VAL_DOCUMENTOS TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT EXECUTE ON PROCEDURE FE_APO_COMPROBANTE TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON TERCEROS TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON PAISES TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON MUNICIPIOS TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_DET TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON AUTO_COMP TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON CENTROS TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON DOCUMENTOS TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON NOTAS_CONTABLE TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON CONTACTOS TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON SOCIEDADES TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON AUTO_MOVIMIENTO TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON ANEXO_CONTABILIDAD TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT EXECUTE ON PROCEDURE REDONDEO_DIAN TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_IMP TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT EXECUTE ON PROCEDURE LEE_RESOLUCION TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON NOTAS_CRE TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON NOTAS_DEB TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT SELECT ON NOTAS_DS TO PROCEDURE PZ_FE_AUTO_COMP;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_COMP TO SYSDBA;
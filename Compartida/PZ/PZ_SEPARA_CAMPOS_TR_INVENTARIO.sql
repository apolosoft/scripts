CREATE OR ALTER PROCEDURE Pz_Separa_Campos_Tr_Inventario (
    Detalle_ VARCHAR(300))
RETURNS (
    Centro         CHAR(5),
    Bodega         CHAR(5),
    Referencia     CHAR(20),
    Entrada        NUMERIC(17,4),
    Salida         NUMERIC(17,4),
    Unitario       NUMERIC(17,4),
    Porc_Descuento NUMERIC(17,4),
    Nota           VARCHAR(200))
AS
-- Centro,Bodega, Referencia, Entrada, Salida, Unitario
DECLARE VARIABLE Detalle VARCHAR(100);
DECLARE VARIABLE Det     VARCHAR(30);
DECLARE VARIABLE Coma    INTEGER;
BEGIN
  Detalle = Detalle_;
  Centro = 'NA';
  Bodega = 'NA';
  Referencia = 'NA';
  Entrada = 0;
  Salida = 0;
  Unitario = 0;

  -- Centro
  Coma = POSITION(',', Detalle);
  IF (Coma > 1) THEN
  BEGIN
    Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
    Detalle = SUBSTRING(Detalle FROM Coma + 1);
    Centro = LEFT(Det, 5);

    -- Bodega
    Coma = POSITION(',', Detalle);
    IF (Coma > 1) THEN
    BEGIN
      Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
      Detalle = SUBSTRING(Detalle FROM Coma + 1);
      Bodega = LEFT(Det, 5);

      -- Referencia
      Coma = POSITION(',', Detalle);
      IF (Coma > 1) THEN
      BEGIN
        Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
        Detalle = SUBSTRING(Detalle FROM Coma + 1);
        Referencia = LEFT(Det, 20);

        -- Entrada
        Coma = POSITION(',', Detalle);
        IF (Coma > 1) THEN
        BEGIN
          Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
          Detalle = SUBSTRING(Detalle FROM Coma + 1);
          Entrada = LEFT(Det, 17);

          -- Salida
          Coma = POSITION(',', Detalle);
          IF (Coma > 1) THEN
          BEGIN
            Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
            Detalle = SUBSTRING(Detalle FROM Coma + 1);
            Salida = LEFT(Det, 17);

            -- Unitario
            Coma = POSITION(',', Detalle);
            IF (Coma > 1) THEN
            BEGIN
              Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
              Detalle = SUBSTRING(Detalle FROM Coma + 1);
              Unitario = LEFT(Det, 17);

              -- Porc. Descuento
              Coma = POSITION(',', Detalle);
              IF (Coma > 1) THEN
              BEGIN
                Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
                Detalle = SUBSTRING(Detalle FROM Coma + 1);
                Porc_Descuento = LEFT(Det, 17);

                -- Nota
                Coma = POSITION(',', Detalle);
                IF (Coma > 1) THEN
                BEGIN
                  Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
                  Detalle = SUBSTRING(Detalle FROM Coma + 1);
                  Porc_Descuento = LEFT(Det, 200);

                END
                ELSE
                  Nota = LEFT(Detalle, 200);
              END
              ELSE
                Porc_Descuento = LEFT(Detalle, 17);
            END
            ELSE
              Unitario = LEFT(Detalle, 17);
          END
          ELSE
            Salida = LEFT(Detalle, 17);
        END
        ELSE
          Entrada = LEFT(Detalle, 17);
      END
      ELSE
        Referencia = LEFT(Detalle, 20);
    END
    ELSE
      Bodega = LEFT(Detalle, 5);
  END
  ELSE
    Centro = LEFT(Detalle, 5);
  SUSPEND;
END
CREATE OR ALTER PROCEDURE Pz_Fe_Comprobantes (
    Tipo_     VARCHAR(5),
    Prefijo_  VARCHAR(5),
    Numero_   VARCHAR(10),
    Emisor_   VARCHAR(15),
    Receptor_ VARCHAR(15),
    Clase_Fe_ VARCHAR(20))
RETURNS (
    Nvfac_Orig CHAR(1),
    Nvemp_Nnit VARCHAR(15),
    Nvres_Nume VARCHAR(20),
    Nvfac_Tipo VARCHAR(5),
    Nvfac_Tcru CHAR(1),
    Nvres_Pref VARCHAR(5),
    Nvfac_Nume VARCHAR(15),
    Nvfac_Fech VARCHAR(20),
    Nvfac_Cdet INTEGER,
    Nvfac_Venc VARCHAR(20),
    Nvsuc_Codi CHAR(1),
    Nvmon_Codi VARCHAR(5),
    Nvfor_Codi VARCHAR(2),
    Nvven_Nomb VARCHAR(84),
    Nvfac_Fpag VARCHAR(5),
    Nvfac_Conv CHAR(1),
    Nvcli_Cper CHAR(1),
    Nvcli_Cdoc VARCHAR(5),
    Nvcli_Docu VARCHAR(20),
    Nvcli_Pais VARCHAR(5),
    Nvcli_Depa VARCHAR(80),
    Nvcli_Ciud VARCHAR(100),
    Nvcli_Loca VARCHAR(10),
    Nvcli_Dire VARCHAR(80),
    Nvcli_Ntel VARCHAR(200),
    Nvcli_Regi VARCHAR(5),
    Nvcli_Fisc VARCHAR(30),
    Nvcli_Nomb VARCHAR(200),
    Nvcli_Pnom VARCHAR(20),
    Nvcli_Snom VARCHAR(20),
    Nvcli_Apel VARCHAR(40),
    Nvcli_Mail VARCHAR(300),
    Nvema_Copi VARCHAR(50),
    Nvfac_Obse VARCHAR(4000),
    Nvfac_Orde VARCHAR(40),
    Nvfac_Remi VARCHAR(40),
    Nvfac_Rece VARCHAR(40),
    Nvfac_Entr VARCHAR(40),
    Nvfac_Ccos VARCHAR(40),
    Nvfac_Stot DOUBLE PRECISION,
    Nvfac_Desc DOUBLE PRECISION,
    Nvfac_Anti DOUBLE PRECISION,
    Nvfac_Carg DOUBLE PRECISION,
    Nvfac_Tota DOUBLE PRECISION,
    Nvfac_Totp DOUBLE PRECISION,
    Nvfac_Roun NUMERIC(12,2),
    Nvfac_Vcop DOUBLE PRECISION,
    Nvfac_Timp DOUBLE PRECISION,
    Nvfac_Coid CHAR(1),
    Nvfac_Obsb CHAR(1),
    Nvcli_Ncon VARCHAR(80),
    Nvcon_Codi VARCHAR(2),
    Nvcon_Desc VARCHAR(200),
    Nvfac_Numb VARCHAR(20),
    Nvfac_Fecb VARCHAR(20),
    Nvfor_Oper VARCHAR(20),
    Nvpro_Cper CHAR(1),
    Nvpro_Cdoc VARCHAR(5),
    Nvpro_Docu VARCHAR(15),
    Nvpro_Dive VARCHAR(1),
    Nvpro_Pais VARCHAR(5),
    Nvpro_Depa VARCHAR(80),
    Nvpro_Ciud VARCHAR(100),
    Nvpro_Zipc VARCHAR(6),
    Nvpro_Loca VARCHAR(10),
    Nvpro_Dire VARCHAR(80),
    Nvpro_Ntel VARCHAR(200),
    Nvpro_Regi VARCHAR(5),
    Nvpro_Fisc VARCHAR(30),
    Nvpro_Nomb VARCHAR(200),
    Nvpro_Pnom VARCHAR(20),
    Nvpro_Snom VARCHAR(20),
    Nvpro_Apel VARCHAR(40),
    Nvpro_Mail VARCHAR(300),
    Nvpro_Ncon VARCHAR(80),
    V_Docu_Ref VARCHAR(20),
    Com_Total  DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Caiu            INTEGER;
DECLARE VARIABLE V_Dsi             INTEGER;
DECLARE VARIABLE V_Mand            INTEGER;
DECLARE VARIABLE V_Cod_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Tesoreria       VARCHAR(10);
DECLARE VARIABLE V_Formapago       VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe    VARCHAR(5);
DECLARE VARIABLE V_Codbanco        VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe     VARCHAR(5);
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Codsociedad     VARCHAR(5);
DECLARE VARIABLE V_Email_Ter       VARCHAR(80);
DECLARE VARIABLE V_Codsucursal     VARCHAR(15);
DECLARE VARIABLE V_Email_Suc       VARCHAR(80);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Copag           INTEGER;
DECLARE VARIABLE V_Copago          DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Municipio       VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref        VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref     VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref      VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac       VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref        VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Stot_Red        DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det   DOUBLE PRECISION;
DECLARE VARIABLE V_Siif            VARCHAR(80);
DECLARE VARIABLE V_Fecre           INTEGER;
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
DECLARE VARIABLE V_Transporte      INTEGER;
DECLARE VARIABLE V_Impuestos_Red   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Totp      DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Mekano    DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Imp       DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Desc      DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Mek_Xml     DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Xml         DOUBLE PRECISION;
DECLARE VARIABLE V_Oper_Salud      VARCHAR(20);
DECLARE VARIABLE V_Largo           VARCHAR(80);
BEGIN
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO Nvemp_Nnit;

  IF (Clase_Fe_ IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN

    -- Si es NC o ND se toman datos del doc referencia
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo_Ref,
         V_Prefijo_Ref,
         V_Numero_Ref;

    V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
    V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
    V_Numero_Ref = COALESCE(V_Numero_Ref, '');

    SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
    FROM Comprobantes
    WHERE Tipo = :V_Tipo_Ref
          AND Prefijo = :V_Prefijo_Ref
          AND Numero = :V_Numero_Ref
    INTO V_Fecha_Ref;
    -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

    SELECT Cufe,
           EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
    FROM Facturas
    WHERE Tipo = :V_Tipo_Ref
          AND Prefijo = :V_Prefijo_Ref
          AND Numero = :V_Numero_Ref
    INTO V_Cufe_Ref,
         V_Fecha_Fac;
    V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
    V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

    SELECT Codigo_Fe
    FROM Documentos
    WHERE Codigo = :V_Tipo_Ref
    INTO V_Docu_Ref;
    V_Docu_Ref = COALESCE(V_Docu_Ref, '');
  END

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       Nvfac_Fech,
       Nvfac_Venc,
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  -- Transportes
  V_Transporte = 0;
  SELECT COUNT(1)
  FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
  INTO V_Transporte;

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe,
         Tipo_Operacion
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe,
       V_Oper_Salud;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO Nvres_Nume;
  Nvres_Nume = COALESCE(Nvres_Nume, '0');

  Nvres_Nume = CASE TRIM(V_Docu)
                 WHEN 'NOTA DEBITO' THEN '1'
                 WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                 ELSE Nvres_Nume
               END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO Nvfac_Cdet;

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO Nvcli_Ncon;

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  Nvcli_Ncon = COALESCE(Nvcli_Ncon, '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO Nvven_Nomb;

  Nvven_Nomb = COALESCE(Nvven_Nomb, '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       Nvcli_Cdoc,
       Nvcli_Docu,
       Nvcli_Dire,
       Nvcli_Ntel,
       V_Codsociedad,
       Nvcli_Nomb,
       Nvcli_Pnom,
       Nvcli_Snom,
       Nvcli_Apel,
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Razon social larga
  SELECT FIRST 1 Nombre
  FROM Contactos
  WHERE Codcargo = 'LARGO'
  INTO V_Largo;
  V_Largo = COALESCE(V_Largo, '');

  Nvcli_Nomb = TRIM(Nvcli_Nomb) || ' ' || TRIM(:V_Largo);

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO Nvcli_Regi,
       Nvcli_Fisc;

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Nvcli_Pais;

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO Nvcli_Depa;

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:Nvcli_Pais) = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO Nvcli_Ciud;

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Nvfac_Orde,
       Nvfac_Remi,
       Nvfac_Rece,
       Nvfac_Entr,
       Nvfac_Ccos;

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  Nvfac_Orde = COALESCE(Nvfac_Orde, '');
  Nvfac_Remi = COALESCE(Nvfac_Remi, '');
  Nvfac_Rece = COALESCE(Nvfac_Rece, '');
  Nvfac_Entr = COALESCE(Nvfac_Entr, '');
  Nvfac_Ccos = COALESCE(Nvfac_Ccos, '');

  Nvfac_Orig = 'E';
  --  Nvemp_nnit = :Emisor_;
  Nvfac_Tipo = CASE TRIM(V_Docu)
                 WHEN 'FACTURA' THEN 'FV'
                 WHEN 'CONTINGENCIA' THEN 'FC'
                 WHEN 'EXPORTACION' THEN 'FE'
                 WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                            WHEN 'EXPORTACION' THEN 'CE'
                                            WHEN 'CONTINGENCIA' THEN 'CC'
                                            ELSE 'CV'
                                          END
                 WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                           WHEN 'EXPORTACION' THEN 'DE'
                                           WHEN 'CONTINGENCIA' THEN 'DC'
                                           ELSE 'DV'
                                         END
                 WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                 WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

               END;

  Nvres_Pref = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    Nvres_Pref = '';
  Nvfac_Nume = TRIM(Nvres_Pref) || TRIM(Numero_);

  Nvsuc_Codi = '1';
  Nvmon_Codi = 'COP';
  Nvfor_Codi = CASE TRIM(V_Docu)
                 WHEN 'FACTURA' THEN CASE
                                       WHEN V_Caiu <> 0 THEN '11'
                                       WHEN V_Transporte <> 0 THEN '18'
                                       WHEN V_Mand <> 0 THEN '12'
                                       ELSE '1'
                                     END
                 WHEN 'CONTINGENCIA' THEN CASE
                                            WHEN V_Caiu <> 0 THEN '15'
                                            WHEN V_Mand <> 0 THEN '16'
                                            ELSE '7'
                                          END
                 WHEN 'EXPORTACION' THEN IIF(Nvcli_Pais = 'CO', '10', '4')
                 WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                            WHEN 'EXPORTACION' THEN '5'
                                            WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                            WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                            ELSE '2'
                                          END
                 WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                           WHEN 'EXPORTACION' THEN '6'
                                           WHEN 'FACTURA' THEN '3'
                                           ELSE '3'
                                         END
                 WHEN 'DOCUMENTO SOPORTE' THEN '20'
                 WHEN 'NOTA DE AJUSTE A DS' THEN '21'
               END;

  Nvfac_Fpag = CASE
                 WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                 WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                 ELSE 'ZZZ'
               END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      Nvfac_Conv = '2';
    ELSE
      Nvfac_Conv = '1';
  END
  ELSE
  BEGIN
    IF (Nvfac_Fech <> Nvfac_Venc) THEN
      Nvfac_Conv = '2';
    ELSE
      Nvfac_Conv = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    Nvcli_Cper = '2';
  ELSE
    Nvcli_Cper = '1';

  Nvcli_Loca = '';
  Nvcli_Mail = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  Nvema_Copi = '';

  Nvfac_Obse = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_pdes", 0)),
         SUM("noov:Nvfac_stot"),
         SUM(Stot_Red)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det,
       V_Nvfac_Stot,
       V_Stot_Red;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  Nvfac_Stot = V_Stot_Red;

  Nvfac_Desc = 0;
  Nvfac_Anti = 0;

  IF (V_Copag = 0) THEN
    Nvfac_Desc = V_Copago;
  ELSE
    Nvfac_Anti = V_Copago;

  Nvfac_Desc = Nvfac_Desc + V_Descuento_Det;
  Nvfac_Carg = 0;
  Nvfac_Tota = Nvfac_Stot;

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo"),
         SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos,
       :V_Impuestos_Red;

  V_Impuestos = COALESCE(V_Impuestos, 0);
  V_Impuestos_Red = COALESCE(V_Impuestos_Red, 0);

  Nvfac_Totp = V_Stot_Red + V_Impuestos - Nvfac_Desc;

  Nvfor_Oper = '';
  IF (Nvfac_Anti > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      Nvfac_Desc = Nvfac_Anti;
      Nvfac_Anti = 0;
    END
    ELSE
      Nvfac_Totp = Nvfac_Totp - Nvfac_Anti;
    Nvfor_Oper = 'SS-CUFE';
  END

  -- Salud
  IF (TRIM(:V_Oper_Salud) <> 'NA') THEN
  BEGIN
    Nvfor_Oper = :V_Oper_Salud;
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    Nvfor_Oper = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  -- redondeo
  SELECT Valor
  FROM Redondeo_Dian(:Nvfac_Totp, 2)
  INTO V_Valor_Totp;

  SELECT Valor
  FROM Redondeo_Dian(:Com_Total, 2)
  INTO V_Valor_Mekano;

  SELECT Valor
  FROM Redondeo_Dian(:V_Stot_Red, 2)
  INTO V_Valor_Stot;

  SELECT Valor
  FROM Redondeo_Dian(:V_Impuestos_Red, 2)
  INTO V_Valor_Imp;

  SELECT Valor
  FROM Redondeo_Dian(:Nvfac_Desc + :Nvfac_Anti, 2)
  INTO V_Valor_Desc;

  V_Dif_Mek_Xml = :V_Valor_Mekano - :V_Valor_Totp;

  V_Dif_Xml = :V_Valor_Totp - :V_Valor_Stot - :V_Valor_Imp + COALESCE(:V_Valor_Desc, 0);

  IF (:Clase_Fe_ IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
    Nvfac_Roun = V_Dif_Xml;
  ELSE
    Nvfac_Roun = V_Dif_Mek_Xml + V_Dif_Xml;

  IF (Nvfac_Roun > 5000 OR Nvfac_Roun < -5000) THEN
    Nvfac_Roun = 0;

  Nvfac_Roun = COALESCE(Nvfac_Roun, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  Nvfac_Vcop = 0;
  Nvfac_Timp = 0;
  Nvfac_Coid = '';
  Nvfac_Numb = '';
  Nvfac_Obsb = '';
  Nvcon_Codi = '';
  Nvcon_Desc = '';
  Nvfac_Tcru = '';
  Nvfac_Numb = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    Nvcon_Codi = V_Concepto_Nc;
    Nvcon_Desc = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      Nvfac_Tcru = 'L';
    ELSE
      Nvfac_Tcru = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      Nvfac_Numb = TRIM(V_Prefijo_Ref);
    Nvfac_Numb = TRIM(Nvfac_Numb) || TRIM(V_Numero_Ref);
    Nvfac_Fecb = TRIM(COALESCE(V_Fecha_Ref, Nvfac_Fech));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    Nvpro_Cper = Nvcli_Cper;
    Nvpro_Cdoc = Nvcli_Cdoc;
    Nvpro_Docu = TRIM(Receptor_);
    Nvpro_Dive = '';

    IF (Nvpro_Cdoc = '31') THEN
      Nvpro_Dive = RIGHT(Nvcli_Docu, 1);

    Nvpro_Pais = Nvcli_Pais;
    Nvpro_Depa = Nvcli_Depa;
    Nvpro_Ciud = Nvcli_Ciud;
    Nvpro_Zipc = V_Postal_Code;
    Nvpro_Loca = Nvcli_Loca;
    Nvpro_Dire = Nvcli_Dire;
    Nvpro_Ntel = Nvcli_Ntel;
    Nvpro_Regi = Nvcli_Regi;
    Nvpro_Fisc = Nvcli_Fisc;
    Nvpro_Nomb = Nvcli_Nomb;
    Nvpro_Pnom = Nvcli_Pnom;
    Nvpro_Snom = Nvcli_Snom;
    Nvpro_Apel = Nvcli_Apel;
    Nvpro_Mail = V_Dscor;
    Nvpro_Ncon = Nvcli_Ncon;
    Nvcli_Cper = '';
    Nvcli_Cdoc = '';
    Nvcli_Docu = '';
    Nvcli_Pais = '';
    Nvcli_Depa = '';
    Nvcli_Ciud = '';
    Nvcli_Loca = '';
    Nvcli_Dire = '';
    Nvcli_Ntel = '';
    Nvcli_Regi = '';
    Nvcli_Fisc = '';
    Nvcli_Nomb = '';
    Nvcli_Pnom = '';
    Nvcli_Snom = '';
    Nvcli_Apel = '';
    Nvcli_Mail = '';
    Nvcli_Ncon = '';
  END
  SUSPEND;

END
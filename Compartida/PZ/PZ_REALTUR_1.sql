SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Realtur_1 (
    Tipo    VARCHAR(5),
    Prefijo VARCHAR(5),
    Numero  VARCHAR(10))
AS
DECLARE VARIABLE V_Centro          VARCHAR(5);
DECLARE VARIABLE V_Tercero         VARCHAR(15);
DECLARE VARIABLE V_Fecha           DATE;
DECLARE VARIABLE V_Consecutivo     INTEGER;
DECLARE VARIABLE V_Generador       VARCHAR(20);
DECLARE VARIABLE V_Numero_Positivo VARCHAR(10);
BEGIN

  /* F6 */
  /* Asigna retención manual */
  UPDATE Comprobantes C
  SET C.Retencion_Manual = 'S'
  WHERE (SELECT COUNT(*)
         FROM (SELECT R.Tipo_Retencion,
                      D.Base Base_Real,
                      SUM(R.Base) Base
               FROM Reg_Retenciones R
               JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
               JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
               JOIN Tiporetenciones T1 ON (T1.Codigo = 'BASE')
               LEFT JOIN Data_Retenciones D ON (T1.Codigo = D.Codtiporetencion AND
                     D.Ano = EXTRACT(YEAR FROM C.Fecha))
               WHERE Tipo = :Tipo
                     AND Prefijo = :Prefijo
                     AND Numero = :Numero
                     AND T.Codliga = 'RETECXP'
                     AND T.Clase = 'RETEFUENTE'
               GROUP BY 1, 2
               HAVING SUM(R.Base) < D.Base)) > 0
        AND C.Tipo = :Tipo
        AND C.Prefijo = :Prefijo
        AND C.Numero = :Numero;

  /* Elimina las retenciones */
  DELETE FROM Reg_Retenciones R1
  WHERE R1.Tipo_Retencion IN (SELECT Codigo
                              FROM Tiporetenciones
                              WHERE Clase = 'RETEFUENTE'
                                    AND Codliga = 'RETECXP')
        AND (SELECT COUNT(*)
             FROM (SELECT T.Codliga,
                          T.Clase,
                          D.Base Base_Real,
                          SUM(R.Base) Base
                   FROM Reg_Retenciones R
                   JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
                   JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
                   JOIN Tiporetenciones T1 ON (T1.Codigo = 'BASE')
                   LEFT JOIN Data_Retenciones D ON (T1.Codigo = D.Codtiporetencion AND
                         D.Ano = EXTRACT(YEAR FROM C.Fecha))
                   WHERE Tipo = :Tipo
                         AND Prefijo = :Prefijo
                         AND Numero = :Numero
                         AND T.Codliga = 'RETECXP'
                         AND T.Clase = 'RETEFUENTE'
                   GROUP BY 1, 2, 3
                   HAVING SUM(R.Base) < D.Base)) > 0
        AND R1.Tipo = :Tipo
        AND R1.Prefijo = :Prefijo
        AND R1.Numero = :Numero;

  SELECT Fecha
  FROM Comprobantes
  WHERE Tipo = :Tipo
        AND Prefijo = :Prefijo
        AND Numero = :Numero
  INTO V_Fecha;

  /* F7 */
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:Tipo, :Prefijo, :Numero);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:Tipo, :Prefijo, :Numero);

  V_Numero_Positivo = :Numero;

  IF (:Numero < 0) THEN
  BEGIN

    SELECT Generador
    FROM Consecutivos
    WHERE Coddocumento = :Tipo
          AND Codprefijo = :Prefijo
          AND :V_Fecha BETWEEN Fecha AND Vence
    INTO V_Generador;

    EXECUTE STATEMENT 'SELECT GEN_ID(' || :V_Generador || ', 1)
    FROM Rdb$Database '
        INTO V_Consecutivo;

    UPDATE Comprobantes
    SET Numero = :V_Consecutivo
    WHERE Tipo = :Tipo
          AND Prefijo = :Prefijo
          AND Numero = :Numero;

    V_Numero_Positivo = :V_Consecutivo;

  END

  /* Proceso PER001:
  Cambia tercero en contabilidad por tercero del centro de costos */
  FOR SELECT DISTINCT T.Codcentro,
                      C1.Codtercero,
                      C.Fecha
      FROM Comprobantes C
      JOIN Tr_Inventario T USING (Tipo, Prefijo, Numero)
      JOIN Referencias R ON (T.Codreferencia = R.Codigo)
      JOIN Centros C1 ON (T.Codcentro = C1.Codigo)
      WHERE Tipo = :Tipo
            AND Prefijo = :Prefijo
            AND Numero = :V_Numero_Positivo
            AND R.Codlinea = 'CXP'
            AND C1.Codtercero <> C.Codtercero
      INTO V_Centro,
           V_Tercero,
           V_Fecha
  DO
  BEGIN
    UPDATE Reg_Contable R
    SET R.Codtercero = :V_Tercero
    WHERE R.Tipo = :Tipo
          AND R.Prefijo = :Prefijo
          AND R.Numero = :V_Numero_Positivo
          AND R.Codcentro = :V_Centro
          AND R.Codcuenta IN (SELECT E.Cta_Credito
                              FROM Esquema_Contable E
                              WHERE Codesquema IN (SELECT R.Cod_Esqcontable
                                                   FROM Referencias R
                                                   WHERE Codlinea = 'CXP')
                                    AND E.Grupo = 'VENTA'
                                    AND E.Concepto = 'PARTIDA');

    UPDATE Reg_Contable R
    SET R.Codtercero = :V_Tercero
    WHERE R.Tipo = :Tipo
          AND R.Prefijo = :Prefijo
          AND R.Numero = :V_Numero_Positivo
          AND R.Codcentro = :V_Centro
          AND R.Codcuenta IN (SELECT T.Coddebito
                              FROM Tiporetenciones T
                              WHERE T.Codliga = 'RETECXP');

    UPDATE Reg_Contable R
    SET R.Codtercero = :V_Tercero
    WHERE R.Tipo = :Tipo
          AND R.Prefijo = :Prefijo
          AND R.Numero = :V_Numero_Positivo
          AND R.Codcentro = :V_Centro
          AND R.Codcuenta IN (SELECT T.Codcredito
                              FROM Tiporetenciones T
                              WHERE T.Codliga = 'RETECXP');

    UPDATE OR INSERT INTO Todo_Dias (Fecha)
    VALUES (:V_Fecha);
  END

  /* Proceso de automator, generacion cuentas de cobros */
  EXECUTE PROCEDURE Pz_Realtur_2(:Tipo, :Prefijo, :V_Numero_Positivo);

  -- Crea el RC de la FE1
  EXECUTE PROCEDURE Pz_Realtur_3(:Tipo, :Prefijo, :V_Numero_Positivo);

  -- Crea el CE de las CXP
  EXECUTE PROCEDURE Pz_Realtur_4(:Tipo, :Prefijo, :V_Numero_Positivo);

  /* Bandera en el documento para no ejecutarlo de nuevo */
  UPDATE Comprobantes
  SET Archivar = 'S'
  WHERE Tipo = :Tipo
        AND Prefijo = :Prefijo
        AND Numero = :V_Numero_Positivo;

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT,UPDATE ON COMPROBANTES TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT,DELETE ON REG_RETENCIONES TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON TIPORETENCIONES TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON DATA_RETENCIONES TO PROCEDURE PZ_REALTUR_1;
GRANT EXECUTE ON PROCEDURE FX_RECALCULA_COMPROBANTE TO PROCEDURE PZ_REALTUR_1;
GRANT EXECUTE ON PROCEDURE FX_PERFECCIONA_EN_BATCH TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON CONSECUTIVOS TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON TR_INVENTARIO TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON REFERENCIAS TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON CENTROS TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT,UPDATE ON REG_CONTABLE TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT ON ESQUEMA_CONTABLE TO PROCEDURE PZ_REALTUR_1;
GRANT SELECT,INSERT,UPDATE ON TODO_DIAS TO PROCEDURE PZ_REALTUR_1;
GRANT EXECUTE ON PROCEDURE PZ_REALTUR_2 TO PROCEDURE PZ_REALTUR_1;
GRANT EXECUTE ON PROCEDURE PZ_REALTUR_3 TO PROCEDURE PZ_REALTUR_1;
GRANT EXECUTE ON PROCEDURE PZ_REALTUR_4 TO PROCEDURE PZ_REALTUR_1;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_REALTUR_1 TO SYSDBA;
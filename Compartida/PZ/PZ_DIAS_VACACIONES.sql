SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Dias_Vacaciones (
    Empleado_ VARCHAR(15),
    Desde_    DATE,
    Hasta_    DATE)
RETURNS (
    Valor INTEGER)
AS
DECLARE VARIABLE V_Nomina   VARCHAR(5);
DECLARE VARIABLE V_Dias_Vac INTEGER;
BEGIN
  Valor = 0;
  FOR SELECT Codigo
      FROM Nominas
      WHERE Fecha >= :Desde_
            AND Fecha < :Hasta_
      INTO V_Nomina
  DO
  BEGIN
    SELECT SUM(P.Adicion + P.Deduccion)
    FROM Planillas P
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    WHERE R.Control_Vacaciones = 'S'
          AND P.Codnomina = :V_Nomina
          AND P.Codpersonal = :Empleado_
    INTO V_Dias_Vac;
    V_Dias_Vac = COALESCE(:V_Dias_Vac, 0);
    Valor = :Valor + V_Dias_Vac;
  END
  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON NOMINAS TO PROCEDURE PZ_DIAS_VACACIONES;
GRANT SELECT ON PLANILLAS TO PROCEDURE PZ_DIAS_VACACIONES;
GRANT SELECT ON RUBROS TO PROCEDURE PZ_DIAS_VACACIONES;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_DIAS_VACACIONES TO PROCEDURE PZ_CONTROL_VACACIONES;
GRANT EXECUTE ON PROCEDURE PZ_DIAS_VACACIONES TO SYSDBA;
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Trm (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Codigo CHAR(5),
    Valor  DOUBLE PRECISION,
    Fecha  CHAR(10))
AS
DECLARE VARIABLE V_Docu    CHAR(20);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Trm     DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Lista   CHAR(5);
BEGIN
  -- Se valida el tipo de documento
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

  SELECT Trm,
         Fecha,
         Codlista
  FROM Comprobantes
  WHERE Tipo = :V_Tipo
        AND Prefijo = :V_Prefijo
        AND Numero = :V_Numero
  INTO V_Trm,
       V_Fecha,
       V_Lista;

  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista
  INTO Codigo;

  IF (TRIM(COALESCE(Codigo, '')) = '') THEN
    Codigo = 'COP';

  Valor = 1;
  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Valor > 0
        AND Codmoneda = :Codigo
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO Valor;

  Valor = COALESCE(Valor, 1);

  IF (V_Trm > 0) THEN
    Valor = V_Trm;

  IF (Codigo = 'COP') THEN
    Valor = 1;

  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON DOCUMENTOS TO PROCEDURE PZ_FE_TRM;
GRANT SELECT ON NOTAS_GESTION TO PROCEDURE PZ_FE_TRM;
GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_FE_TRM;
GRANT SELECT ON LISTAS TO PROCEDURE PZ_FE_TRM;
GRANT SELECT ON TASAS TO PROCEDURE PZ_FE_TRM;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_DETALLE;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_DETALLE_BASE;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_IMPUESTOS;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_IMPUESTOS_DETALLE;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_IMPUESTOS_RENGLON;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO ANYTA;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO JG;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO SYSDBA;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO TECNICO;
GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO TECNICOAS;
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Inserta_Rubro_Clase (
    Clase_  CHAR(30),
    Rubro_  CHAR(5),
    Activo_ CHAR(1))
AS
DECLARE VARIABLE V_Cant INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Rubros
  WHERE Codigo = :Rubro_
  INTO V_Cant;

  IF (V_Cant > 0) THEN
    UPDATE OR INSERT INTO Clases_Rubros (Codclase, Codrubro, Activo)
    VALUES (:Clase_, :Rubro_, :Activo_);
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON RUBROS TO PROCEDURE PZ_INSERTA_RUBRO_CLASE;
GRANT SELECT,INSERT,UPDATE ON CLASES_RUBROS TO PROCEDURE PZ_INSERTA_RUBRO_CLASE;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_INSERTA_RUBRO_CLASE TO SYSDBA;
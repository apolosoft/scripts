CREATE OR ALTER PROCEDURE Pz_Control_Vacaciones (
    Empleado_    VARCHAR(15),
    Fecha_Corte_ DATE)
RETURNS (
    Empleado        VARCHAR(15),
    Desde           DATE,
    Hasta           DATE,
    Dias_Laborados  INTEGER,
    Dias_Periodo    INTEGER,
    Dias_Vac        INTEGER,
    Dias_Pendientes INTEGER)
AS
DECLARE VARIABLE V_Empleado VARCHAR(15);
BEGIN

  IF (:Empleado_ = '%') THEN
  BEGIN
    FOR SELECT Codigo
        FROM Personal
        INTO V_Empleado
    DO
    BEGIN
      FOR SELECT Empleado, Desde, Hasta, Dias_Laborados, Dias_Periodo, Dias_Vac,
                 Dias_Pendientes
          FROM Pz_Control_Vacaciones_Emp(:V_Empleado, :Fecha_Corte_)
          INTO :Empleado, :Desde, :Hasta, :Dias_Laborados, :Dias_Periodo,
               :Dias_Vac, :Dias_Pendientes
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
  ELSE
    FOR SELECT Empleado, Desde, Hasta, Dias_Laborados, Dias_Periodo, Dias_Vac,
               Dias_Pendientes
        FROM Pz_Control_Vacaciones_Emp(:Empleado_, :Fecha_Corte_)
        INTO :Empleado, :Desde, :Hasta, :Dias_Laborados, :Dias_Periodo,
             :Dias_Vac, :Dias_Pendientes
    DO
    BEGIN
      SUSPEND;
    END
END
------------------------------

CREATE OR ALTER PROCEDURE Pz_Control_Vacaciones_Emp (
    Empleado_    VARCHAR(15),
    Fecha_Corte_ DATE)
RETURNS (
    Empleado        VARCHAR(15),
    Desde           DATE,
    Hasta           DATE,
    Dias_Laborados  INTEGER,
    Dias_Periodo    INTEGER,
    Dias_Vac        INTEGER,
    Dias_Pendientes INTEGER)
AS
DECLARE VARIABLE V_Fecha_Ret      DATE;
DECLARE VARIABLE V_Fecha_Ing      DATE;
DECLARE VARIABLE V_Fecha_Saldos   DATE;
DECLARE VARIABLE V_Nuevo_Hasta    DATE;
DECLARE VARIABLE V_Dias_No        DOUBLE PRECISION;
DECLARE VARIABLE V_Dias_Laborados DOUBLE PRECISION;
DECLARE VARIABLE V_Total_Vac      DOUBLE PRECISION;
DECLARE VARIABLE V_Hay_Datos      CHAR(2);
DECLARE VARIABLE V_Num_Reg        INTEGER;
DECLARE VARIABLE V_Clase_Lab      VARCHAR(30) = 'C_DIAS_LABORADOS_VACACIONES';
DECLARE VARIABLE V_Clase_No_Vac   VARCHAR(30) = 'C_NO_VACACIONES';
BEGIN
  -- Fechas de Salarios
  SELECT Ingreso,
         Retiro
  FROM Pz_Fecha_Salarios(:Empleado_)
  INTO V_Fecha_Ing,
       V_Fecha_Ret;

  V_Fecha_Ret = COALESCE(V_Fecha_Ret, V_Fecha_Ing);

  IF (V_Fecha_Ing >= V_Fecha_Ret) THEN
  BEGIN
    V_Num_Reg = 0;
    Empleado = Empleado_;
    Desde = :V_Fecha_Ing;

    -- Dias total vacaciones
    SELECT Valor
    FROM Pz_Dias_Vacaciones(:Empleado_, :V_Fecha_Ing, :Fecha_Corte_)
    INTO V_Total_Vac;

    --  Datos para primer registro segun saldos iniciales
    SELECT FIRST 1 Fecha
    FROM Nominas
    WHERE Codliquidacion = 'LQ0INI'
    ORDER BY Fecha
    INTO V_Fecha_Saldos;

    IF (:V_Fecha_Ing < :V_Fecha_Saldos) THEN
    BEGIN
      V_Num_Reg = :V_Num_Reg + 1;
      Desde = :V_Fecha_Ing;
      Hasta = :V_Fecha_Saldos;

      SELECT Valor
      FROM Pz_Valor_Clase(:Empleado_, :Desde, :Hasta + 1, :V_Clase_Lab)
      INTO Dias_Laborados;

      Dias_Periodo = Dias_Laborados * 15 / 360;
      IF (:Dias_Periodo > :V_Total_Vac) THEN
        Dias_Vac = :V_Total_Vac;
      ELSE
        Dias_Vac = :Dias_Periodo;

      V_Total_Vac = :V_Total_Vac - :Dias_Vac;
      Dias_Pendientes = Dias_Periodo - Dias_Vac;

      SUSPEND;
    END

    V_Hay_Datos = 'S';
    WHILE (V_Hay_Datos = 'S') DO
    BEGIN
      V_Num_Reg = V_Num_Reg + 1;

      IF (:V_Num_Reg = 2) THEN
      BEGIN
        V_Nuevo_Hasta = :V_Fecha_Ing;
        WHILE (V_Nuevo_Hasta < :V_Fecha_Saldos) DO
        BEGIN
          V_Nuevo_Hasta = DATEADD(1 YEAR TO :V_Nuevo_Hasta);
        END
        Desde = Hasta;
        Hasta = :V_Nuevo_Hasta;
        SELECT Valor
        FROM Pz_Valor_Clase(:Empleado_, :Desde + 1, :Hasta - 1, :V_Clase_Lab)
        INTO V_Dias_Laborados;

      END
      ELSE
      BEGIN
        Desde = :Hasta;
        IF (:Fecha_Corte_ - :Desde < 365) THEN
          Hasta = :Fecha_Corte_;
        ELSE
          Hasta = DATEADD(1 YEAR TO :Desde);

        SELECT Valor
        FROM Pz_Valor_Clase(:Empleado_, :Desde, :Hasta - 1, :V_Clase_Lab)
        INTO V_Dias_Laborados;
      END

      SELECT Valor
      FROM Pz_Valor_Clase(:Empleado_, :Desde, :Hasta - 1, :V_Clase_No_Vac)
      INTO V_Dias_No;

      V_Dias_No = COALESCE(:V_Dias_No, 0);
      V_Dias_Laborados = V_Dias_Laborados - :V_Dias_No;

      Dias_Periodo = V_Dias_Laborados * 15 / 360;

      IF (:Dias_Periodo > :V_Total_Vac) THEN
        Dias_Vac = :V_Total_Vac;
      ELSE
        Dias_Vac = :Dias_Periodo;

      V_Total_Vac = :V_Total_Vac - :Dias_Vac;

      Dias_Pendientes = Dias_Periodo - Dias_Vac;

      Dias_Laborados = ROUND(:V_Dias_Laborados);
      IF (V_Dias_Laborados > 0) THEN
        SUSPEND;

      IF (:Hasta >= :Fecha_Corte_) THEN
        V_Hay_Datos = 'N';
      Dias_Vac = 0;

    END

  END
END
------------------------------

CREATE OR ALTER PROCEDURE Pz_Dias_Vacaciones (
    Empleado_ VARCHAR(15),
    Desde_    DATE,
    Hasta_    DATE)
RETURNS (
    Valor INTEGER)
AS
DECLARE VARIABLE V_Nomina   VARCHAR(5);
DECLARE VARIABLE V_Dias_Vac INTEGER;
BEGIN
  Valor = 0;
  FOR SELECT Codigo
      FROM Nominas
      WHERE Fecha >= :Desde_
            AND Fecha < :Hasta_
      INTO V_Nomina
  DO
  BEGIN
    SELECT SUM(P.Adicion + P.Deduccion)
    FROM Planillas P
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    WHERE R.Control_Vacaciones = 'S'
          AND P.Codnomina = :V_Nomina
          AND P.Codpersonal = :Empleado_
    INTO V_Dias_Vac;
    V_Dias_Vac = COALESCE(:V_Dias_Vac, 0);
    Valor = :Valor + V_Dias_Vac;
  END
  SUSPEND;
END
-----------------------------

CREATE OR ALTER PROCEDURE Pz_Valor_Clase (
    Empleado_ VARCHAR(15),
    Desde_    DATE,
    Hasta_    DATE,
    Clase_    VARCHAR(30))
RETURNS (
    Valor DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nomina VARCHAR(5);
DECLARE VARIABLE V_Valor  INTEGER;
BEGIN
  Valor = 0;
  FOR SELECT Codigo
      FROM Nominas
      WHERE Fecha >= :Desde_
            AND Fecha < :Hasta_
      INTO V_Nomina
  DO
  BEGIN
    SELECT SUM(P.Adicion + P.Deduccion)
    FROM Planillas P
    JOIN Clases_Rubros R ON (P.Codrubro = R.Codrubro)
    WHERE R.Codclase = :Clase_
          AND R.Activo = 'S'
          AND P.Codnomina = :V_Nomina
          AND P.Codpersonal = :Empleado_
    INTO V_Valor;
    V_Valor = COALESCE(:V_Valor, 0);
    Valor = :Valor + V_Valor;
  END
  SUSPEND;
END

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Realtur_3 (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10))
AS
DECLARE VARIABLE V_Fecha      DATE;
DECLARE VARIABLE V_Vence      DATE;
DECLARE VARIABLE V_Valor      NUMERIC(17,4);
DECLARE VARIABLE V_Nota       VARCHAR(80);
DECLARE VARIABLE V_Tercero    VARCHAR(15);
DECLARE VARIABLE V_Usuario    VARCHAR(10);
DECLARE VARIABLE V_Computador VARCHAR(20);
DECLARE VARIABLE V_Numero     INTEGER;
DECLARE VARIABLE V_Renglon    INTEGER;
DECLARE VARIABLE V_Generador  VARCHAR(20);
DECLARE VARIABLE V_Tipo_R     VARCHAR(5) = 'RC';
DECLARE VARIABLE V_Prefijo_R  VARCHAR(5) = '_';
BEGIN

  /* Select datos de la factura */
  SELECT C.Fecha,
         C.Vence,
         C.Codtercero,
         C.Codusuario,
         C.Computador,
         C.Total
  FROM Comprobantes C
  WHERE (C.Tipo = :Tipo_)
        AND (C.Prefijo = :Prefijo_)
        AND (C.Numero = :Numero_)
  INTO V_Fecha,
       V_Vence,
       V_Tercero,
       V_Usuario,
       V_Computador,
       V_Valor;

  -- Numero para recibo
  SELECT Generador
  FROM Consecutivos
  WHERE Coddocumento = :V_Tipo_R
        AND Codprefijo = :V_Prefijo_R
  INTO V_Generador;

  EXECUTE STATEMENT 'SELECT GEN_ID(' || :V_Generador || ', 1)
    FROM Rdb$Database '
      INTO V_Numero;

  --NOTA
  V_Nota = 'Pago Fact: ' || TRIM(:Tipo_) || ' ' || TRIM(:Prefijo_) || ' ' || TRIM(:Numero_);

  --Crea encabezado del recibo
  INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Vence, Codvendedor, Codusuario, Nota, Bloqueado, Computador, Codtercero, Codescenario,
                            Flujo, Total)
  VALUES (:V_Tipo_R, :V_Prefijo_R, :V_Numero, :V_Fecha, :V_Fecha, 'NULO', :V_Usuario, :V_Nota, 'R', :V_Computador, :V_Tercero, 'NA', :V_Valor,
          :V_Valor);

  -- Renglon de Tr_abonos
  SELECT GEN_ID(Gen_Tr_Abonos, 1)
  FROM Rdb$Database
  INTO V_Renglon;

  -- Tr_abonos
  INSERT INTO Tr_Abonos (Tipo, Prefijo, Numero, Renglon, Tiporef, Prefijoref, Numeroref, Procesar, Fecha, Vence, Cartera, Abono, Codbanco)
  VALUES (:V_Tipo_R, :V_Prefijo_R, :V_Numero, :V_Renglon, :Tipo_, :Prefijo_, :Numero_, 'S', :V_Fecha, :V_Vence, :V_Valor, :V_Valor, 'CG');

  EXECUTE PROCEDURE Fx_Inserta_Tesoreria(:V_Tipo_R, :V_Prefijo_R, :V_Numero);
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_R, :V_Prefijo_R, :V_Numero);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_R, :V_Prefijo_R, :V_Numero);

END^

SET TERM ; ^

COMMENT ON PROCEDURE PZ_REALTUR_3 IS
'Crea el RC cancelando la FE1';

/* Following GRANT statements are generated automatically */

GRANT SELECT,INSERT ON COMPROBANTES TO PROCEDURE PZ_REALTUR_3;
GRANT SELECT ON CONSECUTIVOS TO PROCEDURE PZ_REALTUR_3;
GRANT INSERT ON TR_ABONOS TO PROCEDURE PZ_REALTUR_3;
GRANT EXECUTE ON PROCEDURE FX_INSERTA_TESORERIA TO PROCEDURE PZ_REALTUR_3;
GRANT EXECUTE ON PROCEDURE FX_RECALCULA_COMPROBANTE TO PROCEDURE PZ_REALTUR_3;
GRANT EXECUTE ON PROCEDURE FX_PERFECCIONA_EN_BATCH TO PROCEDURE PZ_REALTUR_3;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_REALTUR_3 TO SYSDBA;
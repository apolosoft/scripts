CREATE OR ALTER PROCEDURE Pz_Separa_Renglones (
    Detalle_ BLOB SUB_TYPE 1 SEGMENT SIZE 80)
RETURNS (
    Detalle VARCHAR(300))
AS
DECLARE VARIABLE Posicion INTEGER;
DECLARE VARIABLE Pos      INTEGER;
BEGIN
  Pos = 0;
  WHILE (Pos >= 0) DO
  BEGIN
    Posicion = POSITION('|', Detalle_, Pos + 1);
    IF (Posicion > 0) THEN
      Detalle = SUBSTRING(Detalle_ FROM Pos + 1 FOR Posicion - Pos - 1);
    ELSE
    BEGIN
      Detalle = SUBSTRING(Detalle_ FROM Pos + 1);
      Posicion = -1;
    END
    Pos = Posicion;
    SUSPEND;
  END
END
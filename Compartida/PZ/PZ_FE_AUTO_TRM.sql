
/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
DECLARE VARIABLE V_Tercero    VARCHAR(15);
DECLARE VARIABLE V_Lista_Ter  VARCHAR(5);
DECLARE VARIABLE V_Lista_Fac  VARCHAR(5);
DECLARE VARIABLE V_Moneda_Ter VARCHAR(5);
DECLARE VARIABLE V_Moneda_Fac VARCHAR(5);
DECLARE VARIABLE V_Valor      DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Fac  DOUBLE PRECISION;
DECLARE VARIABLE V_Docuref    VARCHAR(20) = '';
DECLARE VARIABLE V_Docu       VARCHAR(20);
DECLARE VARIABLE V_Tipo       VARCHAR(5);
DECLARE VARIABLE V_Prefijo    VARCHAR(5);
DECLARE VARIABLE V_Numero     VARCHAR(10);
DECLARE VARIABLE V_Fecha      DATE;
BEGIN
  --Docu
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  --Comprobante
  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
  BEGIN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Contable
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

    SELECT Codigo_Fe
    FROM Documentos
    WHERE Codigo = :V_Tipo
    INTO V_Docuref;
  END

  SELECT Codtercero,
         Fecha
  FROM Fe_Apo_Comprobante(:V_Tipo, :V_Prefijo, :V_Numero)
  INTO V_Tercero,
       V_Fecha;

  --Lista tercero
  SELECT Codlista
  FROM Terceros
  WHERE Codigo = :V_Tercero
  INTO V_Lista_Ter;

  --Listas
  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista_Ter
  INTO V_Moneda_Ter;
  V_Moneda_Ter = COALESCE(V_Moneda_Ter, 'COP');

  -- Si es FACTURA en dolares
  IF (V_Docuref = 'FACTURA') THEN
  BEGIN
    SELECT Codlista
    FROM Val_Documentos
    WHERE Coddocumento = :V_Tipo
    INTO V_Lista_Fac;

    SELECT Codmoneda
    FROM Listas
    WHERE Codigo = :V_Lista_Fac
    INTO V_Moneda_Fac;
    V_Moneda_Fac = COALESCE(V_Moneda_Fac, 'COP');

    SELECT FIRST 1 Valor
    FROM Tasas
    WHERE Codmoneda = :V_Moneda_Fac
          AND Fecha <= :V_Fecha
    ORDER BY Fecha DESC
    INTO V_Valor_Fac;
  END

  --Tasas
  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Codmoneda = :V_Moneda_Ter
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO V_Valor;
  V_Valor = COALESCE(V_Valor, 1);
  IF (V_Valor = 0) THEN
    V_Valor = 1;

  Codigo = CASE V_Docu
             WHEN 'EXPORTACION' THEN V_Moneda_Ter
             WHEN 'NOTA CREDITO' THEN CASE V_Docuref
                                        WHEN 'EXPORTACION' THEN V_Moneda_Ter
                                        WHEN 'FACTURA' THEN V_Moneda_Fac
                                        ELSE 'COP'
                                      END
             WHEN 'NOTA DEBITO' THEN CASE V_Docuref
                                       WHEN 'EXPORTACION' THEN V_Moneda_Ter
                                       WHEN 'FACTURA' THEN V_Moneda_Fac
                                       ELSE 'COP'
                                     END
             ELSE V_Moneda_Ter
           END;
  Valor = CASE V_Docu
            WHEN 'EXPORTACION' THEN V_Valor
            WHEN 'NOTA CREDITO' THEN CASE V_Docuref
                                       WHEN 'EXPORTACION' THEN V_Valor
                                       WHEN 'FACTURA' THEN V_Valor_Fac
                                       ELSE 1
                                     END
            WHEN 'NOTA DEBITO' THEN CASE V_Docuref
                                      WHEN 'EXPORTACION' THEN V_Valor
                                      WHEN 'FACTURA' THEN V_Valor_Fac
                                      ELSE 1
                                    END
            ELSE V_Valor
          END;
  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^



SET TERM ; ^


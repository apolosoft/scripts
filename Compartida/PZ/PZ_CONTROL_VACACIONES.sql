SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Control_Vacaciones (
    Empleado_    VARCHAR(15),
    Fecha_Corte_ DATE)
RETURNS (
    Empleado        VARCHAR(15),
    Desde           DATE,
    Hasta           DATE,
    Dias_Laborados  INTEGER,
    Dias_Periodo    INTEGER,
    Dias_Vac        INTEGER,
    Dias_Pendientes INTEGER)
AS
DECLARE VARIABLE V_Empleado VARCHAR(15);
BEGIN

  IF (:Empleado_ = '%') THEN
  BEGIN
    FOR SELECT Codigo
        FROM Personal
        INTO V_Empleado
    DO
    BEGIN
      FOR SELECT Empleado, Desde, Hasta, Dias_Laborados, Dias_Periodo, Dias_Vac,
                 Dias_Pendientes
          FROM Pz_Control_Vacaciones_Emp(:V_Empleado, :Fecha_Corte_)
          INTO :Empleado, :Desde, :Hasta, :Dias_Laborados, :Dias_Periodo,
               :Dias_Vac, :Dias_Pendientes
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
  ELSE
    FOR SELECT Empleado, Desde, Hasta, Dias_Laborados, Dias_Periodo, Dias_Vac,
               Dias_Pendientes
        FROM Pz_Control_Vacaciones_Emp(:Empleado_, :Fecha_Corte_)
        INTO :Empleado, :Desde, :Hasta, :Dias_Laborados, :Dias_Periodo,
             :Dias_Vac, :Dias_Pendientes
    DO
    BEGIN
      SUSPEND;
    END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON PERSONAL TO PROCEDURE PZ_CONTROL_VACACIONES;
GRANT EXECUTE ON PROCEDURE PZ_CONTROL_VACACIONES_EMP TO PROCEDURE PZ_CONTROL_VACACIONES;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_CONTROL_VACACIONES TO SYSDBA;
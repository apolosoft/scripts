SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Valor_Clase (
    Empleado_ VARCHAR(15),
    Desde_    DATE,
    Hasta_    DATE,
    Clase_    VARCHAR(30))
RETURNS (
    Valor DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nomina VARCHAR(5);
DECLARE VARIABLE V_Valor  INTEGER;
BEGIN
  Valor = 0;
  FOR SELECT Codigo
      FROM Nominas
      WHERE Fecha >= :Desde_
            AND Fecha < :Hasta_
      INTO V_Nomina
  DO
  BEGIN
    SELECT SUM(P.Adicion + P.Deduccion)
    FROM Planillas P
    JOIN Clases_Rubros R ON (P.Codrubro = R.Codrubro)
    WHERE R.Codclase = :Clase_
          AND R.Activo = 'S'
          AND P.Codnomina = :V_Nomina
          AND P.Codpersonal = :Empleado_
    INTO V_Valor;
    V_Valor = COALESCE(:V_Valor, 0);
    Valor = :Valor + V_Valor;
  END
  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON NOMINAS TO PROCEDURE PZ_VALOR_CLASE;
GRANT SELECT ON PLANILLAS TO PROCEDURE PZ_VALOR_CLASE;
GRANT SELECT ON CLASES_RUBROS TO PROCEDURE PZ_VALOR_CLASE;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_VALOR_CLASE TO PROCEDURE PZ_CONTROL_VACACIONES_EMP;
GRANT EXECUTE ON PROCEDURE PZ_VALOR_CLASE TO SYSDBA;
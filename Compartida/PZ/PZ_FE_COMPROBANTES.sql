SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Comprobantes (
    Tipo_     VARCHAR(5),
    Prefijo_  VARCHAR(5),
    Numero_   VARCHAR(10),
    Emisor_   VARCHAR(15),
    Receptor_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(20),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(250),
    "noov:Nvema_copi" VARCHAR(250),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_Docu_Ref        VARCHAR(20),
    Com_Total         DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Caiu            INTEGER;
DECLARE VARIABLE V_Dsi             INTEGER;
DECLARE VARIABLE V_Mand            INTEGER;
DECLARE VARIABLE V_Cod_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Tesoreria       VARCHAR(10);
DECLARE VARIABLE V_Formapago       VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe    VARCHAR(5);
DECLARE VARIABLE V_Codbanco        VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe     VARCHAR(5);
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Codsociedad     VARCHAR(5);
DECLARE VARIABLE V_Email_Ter       VARCHAR(50);
DECLARE VARIABLE V_Codsucursal     VARCHAR(15);
DECLARE VARIABLE V_Email_Suc       VARCHAR(50);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Copag           INTEGER;
DECLARE VARIABLE V_Copago          DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Municipio       VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref        VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref     VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref      VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac       VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref        VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Stot_Red        DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det   DOUBLE PRECISION;
DECLARE VARIABLE V_Siif            VARCHAR(80);
DECLARE VARIABLE V_Fecre           INTEGER;
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
DECLARE VARIABLE V_Mail_1          VARCHAR(250);
DECLARE VARIABLE V_Mail_2          VARCHAR(250);
DECLARE VARIABLE V_Leng_Mail       INTEGER;
DECLARE VARIABLE V_Leng_Otro_Mail  INTEGER;
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  -- Si es NC o ND se toman datos del doc referencia
  SELECT Tiporef, Prefijoref, Numeroref
  FROM Notas_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tipo_Ref, V_Prefijo_Ref, V_Numero_Ref;

  V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
  V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
  V_Numero_Ref = COALESCE(V_Numero_Ref, '');

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Comprobantes
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Fecha_Ref;
  -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

  SELECT Cufe,
         EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Facturas
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Cufe_Ref, V_Fecha_Fac;
  V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
  V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tipo_Ref
  INTO V_Docu_Ref;
  V_Docu_Ref = COALESCE(V_Docu_Ref, '');

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor, Codbanco, Codsucursal, Nota, Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago, "noov:Nvfac_fech", "noov:Nvfac_venc", V_Cod_Vendedor, V_Codbanco, V_Codsucursal, V_Nota, V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  --Documentos
  SELECT TRIM(Codigo_Fe), Tesoreria, Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu, V_Tesoreria, V_Valida_Fe;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvres_nume";
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');

  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvfac_cdet";

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO "noov:Nvcli_ncon";

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  "noov:Nvcli_ncon" = COALESCE("noov:Nvcli_ncon", '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO "noov:Nvven_nomb";

  "noov:Nvven_nomb" = COALESCE("noov:Nvven_nomb", '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza, Codidentidad, TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')), IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''), Codsociedad,
         COALESCE(TRIM(Empresa), ''), COALESCE(TRIM(Nom1), ''), COALESCE(TRIM(Nom2), ''), COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''), COALESCE(Codpais, ''), COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza, "noov:Nvcli_cdoc", "noov:Nvcli_docu", "noov:Nvcli_dire", "noov:Nvcli_ntel", V_Codsociedad, "noov:Nvcli_nomb", "noov:Nvcli_pnom",
       "noov:Nvcli_snom", "noov:Nvcli_apel", V_Email_Ter, V_Pais, V_Municipio, V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO "noov:Nvcli_regi", "noov:Nvcli_fisc";

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  V_Mail_1 = TRIM(:V_Email_Ter) || IIF(COALESCE(:V_Email_Suc, '') = '', '', IIF(TRIM(COALESCE(:V_Email_Suc, '')) = TRIM(:V_Email_Ter), '', ';' || :V_Email_Suc));
  V_Mail_2 = '';

  --Otro Email
  FOR SELECT Email
      FROM Contactos
      WHERE Codtercero = :Receptor_
            AND Codcargo = 'ZZZ'
      ORDER BY Codigo
      INTO V_Email_Otro
  DO
  BEGIN
    V_Email_Otro = COALESCE(V_Email_Otro, '');
    V_Leng_Mail = CHAR_LENGTH(TRIM(V_Mail_1));
    V_Leng_Otro_Mail = CHAR_LENGTH(TRIM(V_Email_Otro));
    IF (:V_Leng_Mail + :V_Leng_Otro_Mail > 199) THEN
    BEGIN
      V_Leng_Mail = CHAR_LENGTH(TRIM(V_Mail_2));
      IF (:V_Leng_Mail + :V_Leng_Otro_Mail < 199) THEN
        V_Mail_2 = TRIM(:V_Mail_2) || IIF(TRIM(V_Email_Otro) = '', '', ';' || :V_Email_Otro);
    END
    ELSE
      V_Mail_1 = TRIM(:V_Mail_1) || IIF(TRIM(V_Email_Otro) = '', '', ';' || :V_Email_Otro);
  END
  IF (CHAR_LENGTH(TRIM(V_Mail_2)) > 0) THEN
    V_Mail_2 = SUBSTRING(:V_Mail_2 FROM 2);

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO "noov:Nvcli_pais";

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO "noov:Nvcli_depa";

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:"noov:Nvcli_pais") = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO "noov:Nvcli_ciud";

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden, Remision, Recepcion, Ean, Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO "noov:Nvfac_orde", "noov:Nvfac_remi", "noov:Nvfac_rece", "noov:Nvfac_entr", "noov:Nvfac_ccos";

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  "noov:Nvfac_orde" = COALESCE("noov:Nvfac_orde", '');
  "noov:Nvfac_remi" = COALESCE("noov:Nvfac_remi", '');
  "noov:Nvfac_rece" = COALESCE("noov:Nvfac_rece", '');
  "noov:Nvfac_entr" = COALESCE("noov:Nvfac_entr", '');
  "noov:Nvfac_ccos" = COALESCE("noov:Nvfac_ccos", '');

  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);

  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Caiu <> 0 THEN '11'
                                              WHEN V_Mand <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Caiu <> 0 THEN '15'
                                                   WHEN V_Mand <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;

  "noov:Nvfac_fpag" = CASE
                        WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                        WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                        ELSE 'ZZZ'
                      END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END
  ELSE
  BEGIN
    IF ("noov:Nvfac_fech" <> "noov:Nvfac_venc") THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    "noov:Nvcli_cper" = '2';
  ELSE
    "noov:Nvcli_cper" = '1';

  "noov:Nvcli_loca" = '';
  "noov:Nvcli_mail" = :V_Mail_1;
  "noov:Nvema_copi" = '';

  "noov:Nvfac_obse" = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  SELECT SUM("noov:Nvfac_stot")
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Nvfac_Stot;

  "noov:Nvfac_stot" = V_Nvfac_Stot;

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;

  IF (V_Copag = 0) THEN
    "noov:Nvfac_desc" = V_Copago;
  ELSE
    "noov:Nvfac_anti" = V_Copago;

  "noov:Nvfac_desc" = "noov:Nvfac_desc" + V_Descuento_Det;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo")
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;

  V_Impuestos = COALESCE(V_Impuestos, 0);

  SELECT SUM(Stot_Red)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Stot_Red;

  "noov:Nvfac_totp" = V_Stot_Red + V_Impuestos - "noov:Nvfac_desc";

  "noov:Nvfor_oper" = '';
  IF ("noov:Nvfac_anti" > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      "noov:Nvfac_desc" = "noov:Nvfac_anti";
      "noov:Nvfac_anti" = 0;
    END
    ELSE
      "noov:Nvfac_totp" = "noov:Nvfac_totp" - "noov:Nvfac_anti";
    "noov:Nvfor_oper" = 'SS-CUFE';
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    "noov:Nvfor_oper" = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_numb" = '';
  "noov:Nvfac_obsb" = '';
  "noov:Nvcon_codi" = '';
  "noov:Nvcon_desc" = '';
  "noov:Nvfac_tcru" = '';
  "noov:Nvfac_numb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvcon_codi" = V_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      "noov:Nvfac_tcru" = 'L';
    ELSE
      "noov:Nvfac_tcru" = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijo_Ref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numero_Ref);
    "noov:Nvfac_fecb" = TRIM(COALESCE(V_Fecha_Ref, "noov:Nvfac_fech"));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END
  SUSPEND;

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE PZ_FE_TRM TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON NOTAS_GESTION TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON FACTURAS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON DOCUMENTOS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT EXECUTE ON PROCEDURE LEE_RESOLUCION TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT EXECUTE ON PROCEDURE PZ_FE_DETALLE TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT EXECUTE ON PROCEDURE PZ_FE_DETALLE_BASE TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON TR_INVENTARIO TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON CENTROS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON TERCEROS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON CONTACTOS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT EXECUTE ON PROCEDURE FN_NOMBRE_EMPLEADO TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON REG_PAGOS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON FORMASPAGO TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON BANCOS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON SOCIEDADES TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON SUCURSALES TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON PAISES TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON MUNICIPIOS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT,INSERT,UPDATE ON TR_NOTAS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON ANEXO_GESTION TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON NOTAS_CRE TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON NOTAS_DEB TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT SELECT ON NOTAS_DS TO PROCEDURE PZ_FE_COMPROBANTES;
GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_SUM TO PROCEDURE PZ_FE_COMPROBANTES;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_COMPROBANTES TO ANYTA;
GRANT EXECUTE ON PROCEDURE PZ_FE_COMPROBANTES TO JG;
GRANT EXECUTE ON PROCEDURE PZ_FE_COMPROBANTES TO SYSDBA;
GRANT EXECUTE ON PROCEDURE PZ_FE_COMPROBANTES TO TECNICO;
GRANT EXECUTE ON PROCEDURE PZ_FE_COMPROBANTES TO TECNICOAS;
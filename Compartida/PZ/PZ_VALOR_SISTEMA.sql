SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Valor_Sistema (
    Codigo_ CHAR(20))
RETURNS (
    Valor CHAR(100))
AS
BEGIN
  SELECT Valor
  FROM Sistema
  WHERE TRIM(Codigo) = TRIM(:Codigo_)
  INTO Valor;
  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON SISTEMA TO PROCEDURE PZ_VALOR_SISTEMA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_VALOR_SISTEMA TO SYSDBA;
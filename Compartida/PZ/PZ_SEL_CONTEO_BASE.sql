SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Sel_Conteo_Base (
    Fecha_Corte_ DATE,
    Tipo_        CHAR(5),
    Prefijo_     CHAR(5),
    Numero_      CHAR(10))
RETURNS (
    Bodega     VARCHAR(5),
    Referencia VARCHAR(20),
    Categoria  VARCHAR(15),
    Lote       VARCHAR(20),
    Saldo      DOUBLE PRECISION,
    Conteo     DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Afecta  CHAR(1);
DECLARE VARIABLE V_Saldos  CHAR(1);
BEGIN
  Conteo = 0;
  FOR SELECT Tipo, Prefijo, Numero, Codbodega, Codreferencia, Codcategoria,
             Codlote, SUM(Entrada - Salida)
      FROM Tr_Inventario
      GROUP BY 1, 2, 3, 4, 5, 6, 7
      INTO :V_Tipo, :V_Prefijo, :V_Numero, :Bodega, :Referencia, :Categoria,
           :Lote, :Saldo
  DO
  BEGIN
    Saldo = COALESCE(Saldo, 0);
    -- Valida fecha
    V_Fecha = NULL;
    SELECT Fecha
    FROM Comprobantes
    WHERE Tipo = :V_Tipo
          AND Prefijo = :V_Prefijo
          AND Numero = :V_Numero
    INTO V_Fecha;

    IF (V_Fecha <= :Fecha_Corte_) THEN
    BEGIN
      --Valida documento
      SELECT Afecta
      FROM Documentos
      WHERE Codigo = :V_Tipo
      INTO V_Afecta;

      IF (:V_Afecta = 'S') THEN
      BEGIN
        --Valida saldos
        SELECT Saldos
        FROM Referencias
        WHERE Codigo = :Referencia
        INTO V_Saldos;
        IF (V_Saldos = 'S') THEN
          SUSPEND;
      END
    END
  END

  FOR SELECT Tipo, Prefijo, Numero, Codbodega, Codreferencia, Codcategoria,
             Codlote, SUM(Entrada - Salida)
      FROM Reg_Produccion
      GROUP BY 1, 2, 3, 4, 5, 6, 7
      INTO :V_Tipo, :V_Prefijo, :V_Numero, :Bodega, :Referencia, :Categoria,
           :Lote, :Saldo
  DO
  BEGIN
    Saldo = COALESCE(Saldo, 0);
    -- Valida fecha
    V_Fecha = NULL;
    SELECT Fecha
    FROM Comprobantes
    WHERE Tipo = :V_Tipo
          AND Prefijo = :V_Prefijo
          AND Numero = :V_Numero
    INTO V_Fecha;

    IF (V_Fecha <= :Fecha_Corte_) THEN
    BEGIN
      --Valida documento
      SELECT Afecta
      FROM Documentos
      WHERE Codigo = :V_Tipo
      INTO V_Afecta;

      IF (:V_Afecta = 'S') THEN
      BEGIN
        --Valida saldos
        SELECT Saldos
        FROM Referencias
        WHERE Codigo = :Referencia
        INTO V_Saldos;
        IF (V_Saldos = 'S') THEN
          SUSPEND;
      END
    END
  END

  Bodega = NULL;
  Referencia = NULL;
  Categoria = NULL;
  Lote = NULL;
  Conteo = 0;
  Saldo = 0;
  FOR SELECT Codbodega, Codreferencia, Codcategoria, Codlote, SUM(Entrada)
      FROM Tr_Inventario
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
      GROUP BY 1, 2, 3, 4
      INTO :Bodega, :Referencia, :Categoria, :Lote, :Conteo
  DO
  BEGIN

    Conteo = COALESCE(Conteo, 0);
    SUSPEND;
  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TR_INVENTARIO TO PROCEDURE PZ_SEL_CONTEO_BASE;
GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_SEL_CONTEO_BASE;
GRANT SELECT ON DOCUMENTOS TO PROCEDURE PZ_SEL_CONTEO_BASE;
GRANT SELECT ON REFERENCIAS TO PROCEDURE PZ_SEL_CONTEO_BASE;
GRANT SELECT ON REG_PRODUCCION TO PROCEDURE PZ_SEL_CONTEO_BASE;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_SEL_CONTEO_BASE TO SYSDBA;
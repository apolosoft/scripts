SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Auto_Det (
    Tipo_ CHAR(5),
    Prefijo_ CHAR(5),
    Numero_ CHAR(10))
RETURNS (
    Consecutivo INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    Stot_Red DOUBLE PRECISION)
AS
DECLARE VARIABLE Referencia CHAR(20);
DECLARE VARIABLE Bruto DOUBLE PRECISION;
DECLARE VARIABLE Stot DOUBLE PRECISION;
DECLARE VARIABLE Descuento DOUBLE PRECISION;
DECLARE VARIABLE Porc_Descuento DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa DOUBLE PRECISION;
DECLARE VARIABLE V_Cdia CHAR(5);
DECLARE VARIABLE Nom_Referencia VARCHAR(198);
DECLARE VARIABLE Cantidad DOUBLE PRECISION;
DECLARE VARIABLE V_Valo DOUBLE PRECISION;
DECLARE VARIABLE V_Desc DOUBLE PRECISION;
DECLARE VARIABLE V_Base DOUBLE PRECISION;
DECLARE VARIABLE Nota VARCHAR(200);
BEGIN
  FOR SELECT Codconcepto,
             SUBSTRING(IIF(TRIM(Nota) = '', Nombre_Concepto, Nota) FROM 1 FOR 198),
             Cantidad,
             Bruto + Descuento,
             Descuento,
             Nota
      FROM Fe_Apo_Detalle(:Tipo_, :Prefijo_, :Numero_)
      INTO :Referencia,
           :Nom_Referencia,
           :Cantidad,
           :Bruto,
           :Descuento,
           :Nota
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);

    IF (Bruto * Cantidad > 0) THEN
    BEGIN
      Porc_Descuento = Descuento * 100 / Bruto;
      Stot = Cantidad * (Bruto - Descuento);
      -- Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento * :Cantidad, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      V_Tarifa = 0;
      V_Cdia = '00';
      SELECT FIRST 1 "noov:Nvimp_porc",
                     "noov:Nvimp_cdia"

      FROM Pz_Fe_Auto_Imp_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Referencia = :Referencia
      INTO V_Tarifa,
           V_Cdia;

      V_Base = (Bruto - Descuento) * Cantidad;
      V_Valo = V_Base * V_Tarifa / 100;

      Consecutivo = 1;
      "noov:Nvpro_codi" = Referencia;
      "noov:Nvpro_nomb" = Nom_Referencia;
      "noov:Nvuni_desc" = '94';
      "noov:Nvfac_cant" = Cantidad;
      "noov:Nvfac_valo" = Bruto;
      "noov:Nvimp_cdia" = V_Cdia;
      "noov:Nvfac_pdes" = (SELECT Valor
                           FROM Redondeo_Dian(:Porc_Descuento, 2));
      "noov:Nvfac_desc" = Descuento * Cantidad;
      "noov:Nvfac_stot" = Stot;
      "noov:Nvdet_piva" = V_Tarifa;
      "noov:Nvdet_viva" = V_Valo;
      "noov:Nvdet_tcod" = '';
      "noov:Nvpro_cean" = '';
      "noov:Nvdet_entr" = '';
      "noov:Nvuni_quan" = 0;

      "noov:Nvdet_nota" = CASE TRIM(Referencia)
                            WHEN 'AIU_A' THEN 'Contrato de servicios AIU por concepto de: Servicios'
                            ELSE TRIM(Nota)
                          END;

      "noov:Nvdet_padr" = 0;
      "noov:Nvdet_marc" = '';
      "noov:Nvdet_mode" = '';

      SUSPEND;

    END
  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE FE_APO_DETALLE TO PROCEDURE PZ_FE_AUTO_DET;
GRANT EXECUTE ON PROCEDURE REDONDEO_DIAN TO PROCEDURE PZ_FE_AUTO_DET;
GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_IMP_BASE TO PROCEDURE PZ_FE_AUTO_DET;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_DET TO PROCEDURE PZ_FE_AUTO_COMP;
GRANT EXECUTE ON PROCEDURE PZ_FE_AUTO_DET TO SYSDBA;
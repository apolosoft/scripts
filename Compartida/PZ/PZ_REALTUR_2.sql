SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Realtur_2 (
    Tipo    VARCHAR(5),
    Prefijo VARCHAR(5),
    Numero  VARCHAR(10))
AS
DECLARE VARIABLE V_Fecha       DATE;
DECLARE VARIABLE V_Valor       NUMERIC(17,4);
DECLARE VARIABLE V_Nota        VARCHAR(200);
DECLARE VARIABLE V_Tercero     VARCHAR(15);
DECLARE VARIABLE V_Usuario     VARCHAR(10);
DECLARE VARIABLE V_Centro      VARCHAR(5);
DECLARE VARIABLE V_Renglon     INTEGER;
DECLARE VARIABLE V_Retefuente  NUMERIC(17,4);
DECLARE VARIABLE V_Reteica     NUMERIC(17,4);
DECLARE VARIABLE V_Insertar    INTEGER;
DECLARE VARIABLE V_Nom_Centro  VARCHAR(80);
DECLARE VARIABLE V_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Nota_C      VARCHAR(80);
DECLARE VARIABLE V_Renglon_New INTEGER;
DECLARE VARIABLE V_Unitario    NUMERIC(17,4);
BEGIN

  /* Select principal */
  FOR SELECT C.Fecha,
             C1.Codtercero,
             C.Codusuario,
             T.Codcentro,
             C.Nota,
             SUM(T.Salida * T.Unitario)
      FROM Comprobantes C
      JOIN Tr_Inventario T USING (Tipo, Prefijo, Numero)
      JOIN Referencias R ON (R.Codigo = T.Codreferencia)
      JOIN Centros C1 ON (C1.Codigo = T.Codcentro)
      WHERE (C.Tipo = :Tipo)
            AND (C.Prefijo = :Prefijo)
            AND (C.Numero = :Numero)
            AND (R.Codlinea = 'CXP')
      GROUP BY 1, 2, 3, 4, 5
      INTO V_Fecha,
           V_Tercero,
           V_Usuario,
           V_Centro,
           V_Nota,
           V_Valor
  DO
  BEGIN

    /* Retefuente */
    SELECT SUM(Rc.Debito) AS Valor
    FROM Reg_Retenciones Rc
    JOIN Tiporetenciones Tr ON (Tr.Codigo = Rc.Tipo_Retencion)
    WHERE (Rc.Tipo = :Tipo)
          AND (Rc.Prefijo = :Prefijo)
          AND (Rc.Numero = :Numero)
          AND (Rc.Centro = :V_Centro)
          AND Tr.Codliga = 'RETECXP'
          AND Tr.Clase = 'RETEFUENTE'
    INTO V_Retefuente;

    /* Reteica */
    SELECT SUM(Rc.Debito) AS Valor
    FROM Reg_Retenciones Rc
    JOIN Tiporetenciones Tr ON (Tr.Codigo = Rc.Tipo_Retencion)
    WHERE (Rc.Tipo = :Tipo)
          AND (Rc.Prefijo = :Prefijo)
          AND (Rc.Numero = :Numero)
          AND (Rc.Centro = :V_Centro)
          AND (Tr.Codliga = 'RETECXP')
          AND (Tr.Clase = 'RETEICA')
    INTO V_Reteica;

    /* Se valida que no exista CXP */
    SELECT COUNT(C.Tipo)
    FROM Tr_Inventario C
    JOIN Comprobantes C1 USING (Tipo, Prefijo, Numero)
    WHERE (C.Tiporef = :Tipo)
          AND (C.Prefijoref = :Prefijo)
          AND (C.Numeroref = :Numero)
          AND (C.Tipo = 'CXP')
          AND (C1.Codtercero = :V_Tercero)
          AND (C.Codcentro = :V_Centro)
    INTO V_Insertar;

    IF (V_Insertar = 0) THEN
    BEGIN

      V_Nota_C = SUBSTRING(:V_Nota FROM 1 FOR 80);

      V_Renglon_New = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                       FROM Rdb$Database);

      V_Unitario = ROUND(COALESCE(:V_Valor, 0) - COALESCE(:V_Reteica, 0) - COALESCE(:V_Retefuente, 0));

      /* Consultar el centro para crearlo como prefijo */
      SELECT SUBSTRING(TRIM(Nombre) FROM 1 FOR 65) || ' ' || TRIM(Codtercero)
      FROM Centros
      WHERE (Codigo = :V_Centro)
      INTO V_Nom_Centro;

      UPDATE OR INSERT INTO Prefijos (Codigo, Nombre)
      VALUES (:V_Centro, :V_Nom_Centro);

      /* Consultar vendedor en valores x defecto */
      SELECT Codvendedor
      FROM Val_Documentos
      WHERE Coddocumento = 'CXP'
      INTO V_Vendedor;

      /* Si vendedor IS NULL se inserta sin este en comprobantes */
      IF (V_Vendedor IS NULL) THEN
        INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Codtercero, Nota, Bloqueado, Codusuario, Codescenario, Codlista, Vence)
        VALUES ('CXP', :V_Centro, :Numero, :V_Fecha, :V_Tercero, :V_Nota_C, 'N', :V_Usuario, 'NA', 'NA', :V_Fecha);
      ELSE
        INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Codtercero, Nota, Bloqueado, Codusuario, Codescenario, Codlista, Vence, Codvendedor)
        VALUES ('CXP', :V_Centro, :Numero, :V_Fecha, :V_Tercero, :V_Nota_C, 'N', :V_Usuario, 'NA', 'NA', :V_Fecha, :V_Vendedor);

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Tiporef, Prefijoref, Numeroref, Codcentro, Codbodega, Codreferencia, Entrada,
                                 Unitario, Bruto, Nota, Codusuario)
      VALUES ('CXP', :V_Centro, :Numero, :V_Renglon_New, :Tipo, :Prefijo, :Numero, :V_Centro, 'BG', 'CXP', 1, :V_Unitario, :V_Unitario, :V_Nota,
              :V_Usuario);

      EXECUTE PROCEDURE Fx_Recalcula_Comprobante('CXP', :V_Centro, :Numero);
      EXECUTE PROCEDURE Fx_Perfecciona_En_Batch('CXP', :V_Centro, :Numero);

    END

  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT,INSERT ON COMPROBANTES TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT,INSERT ON TR_INVENTARIO TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT ON REFERENCIAS TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT ON CENTROS TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT ON REG_RETENCIONES TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT ON TIPORETENCIONES TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT,INSERT,UPDATE ON PREFIJOS TO PROCEDURE PZ_REALTUR_2;
GRANT SELECT ON VAL_DOCUMENTOS TO PROCEDURE PZ_REALTUR_2;
GRANT EXECUTE ON PROCEDURE FX_RECALCULA_COMPROBANTE TO PROCEDURE PZ_REALTUR_2;
GRANT EXECUTE ON PROCEDURE FX_PERFECCIONA_EN_BATCH TO PROCEDURE PZ_REALTUR_2;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_REALTUR_2 TO SYSDBA;
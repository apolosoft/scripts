SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Poner_Precio_Lista (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10),
    Lista_   VARCHAR(5))
AS
DECLARE VARIABLE V_Renglon    INTEGER;
DECLARE VARIABLE V_Referencia VARCHAR(20);
DECLARE VARIABLE V_Precio     NUMERIC(17,4);
BEGIN
  FOR SELECT Renglon,
             Codreferencia
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO V_Renglon,
           V_Referencia
  DO
  BEGIN
    -- buscamos el precio
    SELECT Precio
    FROM Precios
    WHERE Codlista = :Lista_
          AND Codreferencia = :V_Referencia
    INTO V_Precio;
    V_Precio = COALESCE(V_Precio, 0);

    IF (:V_Precio > 0) THEN
      UPDATE Tr_Inventario
      SET Unitario = :V_Precio
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
            AND Renglon = :V_Renglon;

  END
  EXECUTE PROCEDURE Fx_Recalcula_Brutos(:Tipo_, :Prefijo_, :Numero_);
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT,UPDATE ON TR_INVENTARIO TO PROCEDURE PZ_PONER_PRECIO_LISTA;
GRANT SELECT ON PRECIOS TO PROCEDURE PZ_PONER_PRECIO_LISTA;
GRANT EXECUTE ON PROCEDURE FX_RECALCULA_BRUTOS TO PROCEDURE PZ_PONER_PRECIO_LISTA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_PONER_PRECIO_LISTA TO SYSDBA;
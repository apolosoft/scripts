SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Conteo_Fisico (
    Fecha_Corte_ DATE,
    Tipo_        VARCHAR(5),
    Prefijo_     VARCHAR(5),
    Numero_      VARCHAR(10),
    Usuario_     VARCHAR(10))
AS
DECLARE VARIABLE V_Nit          VARCHAR(15);
DECLARE VARIABLE V_Referencia   VARCHAR(20);
DECLARE VARIABLE V_Bodega       VARCHAR(5);
DECLARE VARIABLE V_Categoria    VARCHAR(15);
DECLARE VARIABLE V_Lote         VARCHAR(20);
DECLARE VARIABLE V_Ponderado    DOUBLE PRECISION;
DECLARE VARIABLE V_Diferencia   DOUBLE PRECISION;
DECLARE VARIABLE V_Renglon      INTEGER;
DECLARE VARIABLE V_Tipo_Entrada VARCHAR(5) = 'AE1';
DECLARE VARIABLE V_Tipo_Salida  VARCHAR(5) = 'AS1';
DECLARE VARIABLE V_Prefijo      VARCHAR(5) = '_';
DECLARE VARIABLE V_Numero_E     VARCHAR(10);
DECLARE VARIABLE V_Numero_S     VARCHAR(10);
DECLARE VARIABLE V_Registro     INTEGER;
DECLARE VARIABLE V_Valida       VARCHAR(10);
DECLARE VARIABLE V_Nota         VARCHAR(80);
BEGIN

  /* Insertar prefijo tipo AJUST para evitar primary_key */
  /*$$IBEC$$ SELECT COUNT(1)
 FROM Prefijos
 WHERE Codigo = 'AJUST'
 INTO V_Registro;

 IF (V_Registro = 0) THEN
  INSERT INTO Prefijos (Codigo,Nombre)
  VALUES ('AJUST','AJUSTE DE INVENTARIO'); $$IBEC$$*/

  /* Insertar lista de precios NA */
  SELECT COUNT(1)
  FROM Listas
  WHERE Codigo = 'NA'
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Listas (Codigo, Nombre)
    VALUES ('NA', 'NO APLICA');

  /* Seleccionar el nit del tercero empresa para asignar como tercero en los comprobantes */
  SELECT TRIM(Codigo)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Nit;

  /* Insertar Tipos de Documento AE1 y AS1 si no existen */
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = :V_Tipo_Entrada
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('AE1', 'AJUSTE DE ENTRADA AL INVENTARIO FISICO', NULL,
            'Si valoriza y no contabiliza - Ej: sirve para resetar ponderado con cantidad 0',
            NULL, 'FORAE1CAR101.FR3', 'FORAE1MEO101.FR3', 'FORAE1MEC101.FR3',
            'FORCONTABLE100.fr3', 'S', 'N', 'N', 'N', 'DEBITO', 'AJUSTES', 'N',
            'N', 'NO', 'ENTRADA', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'S', 'S', 'S', 'N', 'S', 'N', NULL, NULL, NULL, NULL, 'N',
            'N', 0, 365, 'N', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N', 'N',
            '', 'S', 'N', 'N', NULL, 'NA', NULL, 'N', 'N', 'N', 'N', 'S');

  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = :V_Tipo_Salida
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('AS1', 'AJUSTE DE SALIDA DEL INVENTARIO FISICO', NULL,
            'Si contabiliza seg?n esquema contable grupo Ajuste (concepto Juego). Atributo contabiliza debe estar inactivo, de lo contrario, quedar?a doble',
            NULL, 'FORAS1CAR101.FR3', 'FORAS1MEC101.FR3', 'FORAS1MEO101.FR3',
            'FORCONTABLE100.fr3', 'S', 'N', 'N', 'N', 'CREDITO', 'AJUSTES', 'N',
            'N', 'NO', 'SALIDA', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'S', 'S', 'N', 'S', 'S', 'N', NULL, NULL, NULL, NULL, 'N',
            'N', 0, 365, 'N', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N', 'N',
            NULL, 'S', 'N', 'N', NULL, 'NA', '2020-05-15', 'N', 'N', 'N', 'N',
            'S');

  /* Insertar en comprobantes el doc AE1 */
  V_Nota = 'AJUSTE INVENTARIO CONTEO FISICO SEGUN ' || TRIM(:Tipo_) || ' ' || TRIM(:Prefijo_) || ' ' || TRIM(:Numero_);
  V_Numero_E = GEN_ID(Gen_Comprobante, -1);
  INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Codescenario, Fecha, Vence,
                            Codlista, Nota, Bloqueado, Codtercero, Codvendedor,
                            Codusuario)
  VALUES (TRIM(:V_Tipo_Entrada), TRIM(:V_Prefijo), TRIM(:V_Numero_E), 'NA',
          :Fecha_Corte_, :Fecha_Corte_, 'NA', :V_Nota, 'N', TRIM(:V_Nit),
          'NULO', TRIM(:Usuario_));

  /* Insertar en comprobantes el doc AS1 */
  V_Numero_S = GEN_ID(Gen_Comprobante, -1);
  INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Codescenario, Fecha, Vence,
                            Codlista, Nota, Bloqueado, Codtercero, Codvendedor,
                            Codusuario)
  VALUES (TRIM(:V_Tipo_Salida), TRIM(:V_Prefijo), TRIM(:V_Numero_S), 'NA',
          :Fecha_Corte_, :Fecha_Corte_, 'NA', :V_Nota, 'N', TRIM(:V_Nit),
          'NULO', TRIM(:Usuario_));

  /* Referencias a insertar en los ajustes */
  FOR SELECT Referencia, Bodega, Categoria, Lote, Ponderado, Diferencia
      FROM Pz_Sel_Conteo_Fisico(:Fecha_Corte_, :Tipo_, :Prefijo_, :Numero_, '%', '%')
      WHERE Diferencia <> 0
      INTO V_Referencia, V_Bodega, V_Categoria, V_Lote, V_Ponderado,
           V_Diferencia
  DO
  BEGIN
    V_Renglon = (SELECT (GEN_ID(Gen_Tr_Inventario, 1))
                 FROM Rdb$Database);

    /* Validacion de la diferencia para saber el tipo de inserccion a realizar si es de entrada o salida
       Si el saldo es negativo = entrada
       Si el saldo es positivo = salida */
    IF (V_Diferencia < 0) THEN
    BEGIN
      V_Diferencia = V_Diferencia * -1;
      SELECT Validacion
      FROM Fx_Sel_Referencia(:V_Referencia)
      INTO V_Valida;

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codbodega,
                                 Codcentro, Codreferencia, Entrada, Bruto,
                                 Unitario, Codusuario, Nota, Validacion)
      VALUES (TRIM(:V_Tipo_Entrada), TRIM(:V_Prefijo), TRIM(:V_Numero_E),
              :V_Renglon, TRIM(:V_Bodega), 'NA', TRIM(:V_Referencia),
              :V_Diferencia, :V_Ponderado, :V_Ponderado, :Usuario_, :V_Nota,
              :V_Valida);
    END
    ELSE
    BEGIN
      SELECT Validacion
      FROM Fx_Sel_Referencia(:V_Referencia)
      INTO V_Valida;

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codbodega,
                                 Codcentro, Codreferencia, Salida, Bruto,
                                 Unitario, Codusuario, Nota, Validacion)
      VALUES (TRIM(:V_Tipo_Salida), TRIM(:V_Prefijo), TRIM(:V_Numero_S),
              :V_Renglon, TRIM(:V_Bodega), 'NA', TRIM(:V_Referencia),
              :V_Diferencia, :V_Ponderado, :V_Ponderado, :Usuario_, :V_Nota,
              :V_Valida);
    END
  END
  /* Ejecuccion para organizar validacion en tr_inventario */
 /*$$IBEC$$  EXECUTE PROCEDURE Fx_Repara_Validacion('MEKANO_2012');

   $$IBEC$$*//* Terminar documentos *//*$$IBEC$$ 
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_Entrada, :V_Prefijo,
      :Numero_);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_Entrada, :V_Prefijo,
      :Numero_);
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_Salida, :V_Prefijo,
      :Numero_);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_Salida, :V_Prefijo,
      :Numero_); $$IBEC$$*/
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT,INSERT ON LISTAS TO PROCEDURE PZ_CONTEO_FISICO;
GRANT SELECT ON TERCEROS TO PROCEDURE PZ_CONTEO_FISICO;
GRANT SELECT,INSERT ON DOCUMENTOS TO PROCEDURE PZ_CONTEO_FISICO;
GRANT INSERT ON COMPROBANTES TO PROCEDURE PZ_CONTEO_FISICO;
GRANT EXECUTE ON PROCEDURE PZ_SEL_CONTEO_FISICO TO PROCEDURE PZ_CONTEO_FISICO;
GRANT EXECUTE ON PROCEDURE FX_SEL_REFERENCIA TO PROCEDURE PZ_CONTEO_FISICO;
GRANT INSERT ON TR_INVENTARIO TO PROCEDURE PZ_CONTEO_FISICO;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_CONTEO_FISICO TO SYSDBA;
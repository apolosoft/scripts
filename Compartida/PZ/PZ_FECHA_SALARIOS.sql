SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fecha_Salarios (
    Empleado_ VARCHAR(15))
RETURNS (
    Ingreso DATE,
    Retiro  DATE)
AS
BEGIN
  SELECT MAX(Desde)
  FROM Salarios
  WHERE Codpersonal = :Empleado_
        AND Columna_Desde = 'INGRESO'
  INTO Ingreso;

  SELECT MAX(S.Hasta)
  FROM Salarios S
  WHERE S.Codpersonal = :Empleado_
        AND S.Columna_Hasta = 'RETIRO'
  INTO Retiro;
  IF (Ingreso > Retiro) THEN
    Retiro = NULL;
  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON SALARIOS TO PROCEDURE PZ_FECHA_SALARIOS;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FECHA_SALARIOS TO PROCEDURE PZ_CONTROL_VACACIONES;
GRANT EXECUTE ON PROCEDURE PZ_FECHA_SALARIOS TO SYSDBA;
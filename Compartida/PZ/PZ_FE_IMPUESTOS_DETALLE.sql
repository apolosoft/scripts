SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Detalle (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10),
    Renglon_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Ano           INTEGER;
DECLARE VARIABLE V_Cantidad      DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto         DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento     DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu           DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa        DOUBLE PRECISION;
DECLARE VARIABLE V_Base          DOUBLE PRECISION;
DECLARE VARIABLE V_Valor         DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
DECLARE VARIABLE V_Es_Tarifa     CHAR(1);
DECLARE VARIABLE V_Valor_Nominal DOUBLE PRECISION;
BEGIN
  /* IMPUESTOS */
  "noov:Nvimp_oper" = 'S';

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  FOR SELECT Cantidad,
             Bruto,
             Descuento,
             Aiu
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Descuento,
           :V_Aiu
  DO
  BEGIN

    V_Cantidad = COALESCE(V_Cantidad, 0);
    V_Bruto = COALESCE(V_Bruto, 0);
    V_Descuento = COALESCE(V_Descuento, 0);
    V_Aiu = COALESCE(V_Aiu, 0);

    IF (V_Aiu = 0) THEN
      V_Aiu = 100;

    V_Tipo_Impuesto = NULL;
    V_Es_Tarifa = NULL;

    FOR SELECT SKIP 1 Tipo_Impuesto,
                      Es_Tarifa,
                      SUM(Debito + Credito)
        FROM Reg_Impuestos
        WHERE Tipo = :Tipo_
              AND Prefijo = :Prefijo_
              AND Numero = :Numero_
              AND Renglon = :Renglon_
        GROUP BY Codigo_Fe, Tipo_Impuesto, Es_Tarifa
        INTO :V_Tipo_Impuesto,
             :V_Es_Tarifa,
             :V_Valor_Nominal
    DO
    BEGIN
      V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
      V_Tarifa = NULL;

      SELECT Tarifa
      FROM Data_Impuestos
      WHERE Codtipoimpuesto = :V_Tipo_Impuesto
            AND Ano = :V_Ano
      INTO :V_Tarifa;

      V_Tarifa = COALESCE(V_Tarifa, 0);
      -- V_Tarifa = CAST(V_Tarifa AS NUMERIC(17,4));

      V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento;
      -- v_base = v_cantidad * (v_bruto - v_descuento) * v_aiu / 100;

      IF (V_Es_Tarifa = 'S') THEN
        V_Valor = V_Base * V_Tarifa / 100;

      IF (V_Es_Tarifa = 'N') THEN
      BEGIN
        IF (V_Cantidad = 0) THEN
          V_Tarifa = 0;
        ELSE
          V_Tarifa = V_Valor_Nominal / V_Cantidad;
        V_Valor = V_Valor_Nominal;
      END

      SELECT Codigo_Fe,
             SUBSTRING(Nombre FROM 1 FOR 40)
      FROM Tipoimpuestos
      WHERE Codigo = :V_Tipo_Impuesto
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc";

      IF ("noov:Nvimp_cdia" = '22') THEN
        V_Base = 0;

      "noov:Nvimp_desc" = CASE
                            WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                            WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                            WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                            WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                            WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                            ELSE "noov:Nvimp_desc"
                          END;

      "noov:Nvimp_base" = V_Base;
      "noov:Nvimp_porc" = V_Tarifa;
      "noov:Nvimp_valo" = V_Valor;

      IF ("noov:Nvimp_cdia" NOT IN ('22', '99') AND
          NOT("noov:Nvimp_cdia" = '04' AND
          V_Tarifa = 0)) THEN
        SUSPEND;

    END

  END

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_FE_IMPUESTOS_DETALLE;
GRANT EXECUTE ON PROCEDURE PZ_FE_DETALLE_BASE TO PROCEDURE PZ_FE_IMPUESTOS_DETALLE;
GRANT SELECT ON REG_IMPUESTOS TO PROCEDURE PZ_FE_IMPUESTOS_DETALLE;
GRANT SELECT ON DATA_IMPUESTOS TO PROCEDURE PZ_FE_IMPUESTOS_DETALLE;
GRANT SELECT ON TIPOIMPUESTOS TO PROCEDURE PZ_FE_IMPUESTOS_DETALLE;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_DETALLE TO PROCEDURE PZ_FE_DETALLE_BASE;
GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_DETALLE TO ANYTA;
GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_DETALLE TO JG;
GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_DETALLE TO SYSDBA;
GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_DETALLE TO TECNICO;
GRANT EXECUTE ON PROCEDURE PZ_FE_IMPUESTOS_DETALLE TO TECNICOAS;
CREATE OR ALTER PROCEDURE Pz_Realtur
AS
DECLARE VARIABLE V_Tipo    VARCHAR(5);
DECLARE VARIABLE V_Prefijo VARCHAR(5);
DECLARE VARIABLE V_Numero  VARCHAR(10);
BEGIN

 /* Documentos sin ejecutar proceso */
 FOR SELECT Tipo,
            Prefijo,
            Numero
     FROM Comprobantes
     WHERE (Tipo = 'FE1')
           AND (Archivar = 'N')
     INTO :V_Tipo,
          :V_Prefijo,
          :V_Numero
 DO
 BEGIN
  /* Update y delete que se ejecutaban en el formato(F6), mas sp
  de terminar documento(F7) */
  EXECUTE PROCEDURE Pz_Realtur_1(:V_Tipo, :V_Prefijo, :V_Numero);

 END
END
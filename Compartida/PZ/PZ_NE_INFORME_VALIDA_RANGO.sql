SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Ne_Informe_Valida_Rango (
    Desde_ DATE,
    Hasta_ DATE)
RETURNS (
    Empleado        VARCHAR(15),
    Nombre_Empleado VARCHAR(84),
    Grupo           VARCHAR(10),
    Nombre          VARCHAR(80),
    Valor           DOUBLE PRECISION,
    Adicion         DOUBLE PRECISION,
    Deduccion       DOUBLE PRECISION)
AS
DECLARE VARIABLE Vcodigo   CHAR(10);
DECLARE VARIABLE Vmanual   INTEGER;
DECLARE VARIABLE Vorden    INTEGER;
DECLARE VARIABLE Vfecha_Ne DATE;
BEGIN
  SELECT COUNT(1)
  FROM Nominas
  WHERE Fecha >= :Desde_
        AND Fecha <= :Hasta_
        AND Automatica = 'N'
        AND Procesado = 'S'
        AND COALESCE(Fecha_Ne, '') <> ''
  INTO Vmanual;

  IF (Vmanual = 0) THEN
  BEGIN
    FOR SELECT P.Codpersonal,
               (SELECT Nombre_Empleado
                FROM Fn_Nombre_Empleado(P.Codpersonal)),
               Codgrupo_Ne,
               G.Nombre,
               G.Orden,
               SUM(P.Valor),
               SUM(P.Adicion),
               SUM(P.Deduccion)
        FROM Planillas P
        JOIN Nominas N ON (P.Codnomina = N.Codigo)
        JOIN Rubros R ON (P.Codrubro = R.Codigo)
        JOIN Grupo_Ne G ON (R.Codgrupo_Ne = G.Codigo)
        JOIN Envios_Ne E ON (P.Codpersonal = E.Codpersonal AND
              N.Fecha_Ne = E.Fecha_Ne)
        WHERE Fecha >= :Desde_
              AND Fecha <= :Hasta_
              AND Procesado = 'S'
              AND N.Fecha_Ne IS NOT NULL
              AND E.Enviado = 'S'
        GROUP BY 1, 2, 3, 4, 5
        ORDER BY 2, 5
        INTO Empleado,
             Nombre_Empleado,
             Grupo,
             Nombre,
             Vorden,
             Valor,
             Adicion,
             Deduccion
    DO
    BEGIN
      SUSPEND;
    END
  END
  ELSE
  BEGIN
    FOR SELECT Codigo,
               Fecha_Ne
        FROM Nominas
        WHERE Fecha >= :Desde_
              AND Fecha <= :Hasta_
              AND Automatica = 'N'
              AND Procesado = 'S'
              AND Fecha_Ne IS NOT NULL
        INTO Vcodigo,
             Vfecha_Ne
    DO
    BEGIN
      FOR SELECT P.Codpersonal,
                 (SELECT Nombre_Empleado
                  FROM Fn_Nombre_Empleado(P.Codpersonal)),
                 P.Codgrupo_Ne,
                 G.Nombre,
                 G.Orden,
                 SUM(P.Valor),
                 SUM(P.Adicion),
                 SUM(P.Deduccion)
          FROM Planillas_Ne P
          JOIN Grupo_Ne G ON (P.Codgrupo_Ne = G.Codigo)
          JOIN Envios_Ne E ON (P.Codpersonal = E.Codpersonal AND
                E.Fecha_Ne = :Vfecha_Ne)
          WHERE Codnomina = :Vcodigo
          GROUP BY 1, 2, 3, 4, 5
          ORDER BY 2, 5
          INTO Empleado,
               Nombre_Empleado,
               Grupo,
               Nombre,
               Vorden,
               Valor,
               Adicion,
               Deduccion
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON NOMINAS TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;
GRANT EXECUTE ON PROCEDURE FN_NOMBRE_EMPLEADO TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;
GRANT SELECT ON PLANILLAS TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;
GRANT SELECT ON RUBROS TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;
GRANT SELECT ON GRUPO_NE TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;
GRANT SELECT ON ENVIOS_NE TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;
GRANT SELECT ON PLANILLAS_NE TO PROCEDURE PZ_NE_INFORME_VALIDA_RANGO;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_NE_INFORME_VALIDA_RANGO TO SYSDBA;
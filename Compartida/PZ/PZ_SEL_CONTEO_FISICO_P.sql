SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Sel_Conteo_Fisico_P (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10))
RETURNS (
    Fecha_Corte       DATE,
    Referencia        VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Bodega            VARCHAR(5),
    Categoria         VARCHAR(15),
    Lote              VARCHAR(20),
    Linea             VARCHAR(5),
    Nombre_Linea      VARCHAR(80),
    Ubicacion         VARCHAR(20),
    Saldo             DOUBLE PRECISION,
    Ponderado         DOUBLE PRECISION,
    Valor             DOUBLE PRECISION,
    Conteo            DOUBLE PRECISION,
    Valor_Conteo      DOUBLE PRECISION,
    Diferencia        DOUBLE PRECISION)
AS
BEGIN
  --Fecha Corte
  SELECT Fecha
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO :Fecha_Corte;

  FOR SELECT Codbodega, Codreferencia, Codcategoria, Codlote, SUM(Entrada)
      FROM Tr_Inventario
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
      GROUP BY 1, 2, 3, 4
      INTO :Bodega, :Referencia, :Categoria, :Lote, :Conteo
  DO
  BEGIN
    Conteo = COALESCE(Conteo, 0);

    SELECT SUM(Entrada - Salida)
    FROM Tr_Inventario T
    JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
    JOIN Documentos D ON (T.Tipo = D.Codigo)
    WHERE C.Fecha <= :Fecha_Corte
          AND T.Codreferencia = :Referencia
          AND T.Codbodega = :Bodega
          AND D.Afecta = 'S'
    INTO :Saldo;

    Categoria = COALESCE(Categoria, 'NA');
    Lote = COALESCE(Lote, 'NA');
    Saldo = COALESCE(Saldo, 0);

    /* Nombre referencia */
    SELECT TRIM(Nombre_Referencia)
    FROM Fn_Nombre_Referencia(:Referencia)
    INTO Nombre_Referencia;

    /* Linea y ubicacion */
    SELECT TRIM(Codlinea), TRIM(Ubicacion)
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Linea, Ubicacion;

    /* Nombre linea */
    SELECT TRIM(Nombre)
    FROM Lineas
    WHERE Codigo = :Linea
    INTO Nombre_Linea;

    /* Ponderado */
    SELECT Ponderado
    FROM Fn_Ponderado(:Referencia, :Fecha_Corte)
    INTO Ponderado;
    Ponderado = COALESCE(Ponderado, 0);

    /* Calculos */
    Valor = Saldo * Ponderado;
    Valor_Conteo = Conteo * Ponderado;
    Diferencia = Saldo - Conteo;

    SUSPEND;
  END
END^

SET TERM ; ^

COMMENT ON PROCEDURE PZ_SEL_CONTEO_FISICO_P IS
'Conteo fisico parcial
Solo tiene en cuenta las referencias digitadas en el SF1';

/* Following GRANT statements are generated automatically */

GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;
GRANT SELECT ON TR_INVENTARIO TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;
GRANT SELECT ON DOCUMENTOS TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;
GRANT EXECUTE ON PROCEDURE FN_NOMBRE_REFERENCIA TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;
GRANT SELECT ON REFERENCIAS TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;
GRANT SELECT ON LINEAS TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;
GRANT EXECUTE ON PROCEDURE FN_PONDERADO TO PROCEDURE PZ_SEL_CONTEO_FISICO_P;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_SEL_CONTEO_FISICO_P TO PROCEDURE PZ_CONTEO_FISICO_P;
GRANT EXECUTE ON PROCEDURE PZ_SEL_CONTEO_FISICO_P TO SYSDBA;
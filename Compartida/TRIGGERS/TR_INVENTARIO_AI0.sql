SET SQL DIALECT 3;



SET TERM ^ ;



CREATE OR ALTER TRIGGER TR_INVENTARIO_AI0 FOR TR_INVENTARIO
ACTIVE AFTER INSERT POSITION 0
AS
DECLARE VARIABLE V_Valida CHAR(10);
BEGIN
  IF (CURRENT_USER = 'SYSDBA' AND
      NEW.Bruto = 0) THEN
  BEGIN
    EXECUTE PROCEDURE Fx_Recalcula_Bruto(NEW.Tipo, NEW.Prefijo, NEW.Numero, NEW.Renglon);

    SELECT Validacion
    FROM Fx_Sel_Referencia(NEW.Codreferencia)
    INTO V_Valida;

    UPDATE Tr_Inventario
    SET Validacion = :V_Valida
    WHERE Tipo = NEW.Tipo
          AND Prefijo = NEW.Prefijo
          AND Numero = NEW.Numero
          AND Renglon = NEW.Renglon;
  END
END
^
SET TERM ; ^

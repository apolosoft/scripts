SET TERM ^ ;

CREATE OR ALTER PROCEDURE Rest_Termina_Comprobante (
    Tipo_ VARCHAR(5),
    Prefijo_ VARCHAR(10),
    Numero_ VARCHAR(10))
AS
DECLARE VARIABLE V_Cantidad INTEGER;
DECLARE VARIABLE V_Generador VARCHAR(20);
DECLARE VARIABLE V_Consecutivo INTEGER;
DECLARE VARIABLE V_Fecha DATE;
BEGIN

  /*Se valida si existe el comprobante*/
  SELECT COUNT(1)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Cantidad;

  SELECT Fecha
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Fecha;

  IF (V_Cantidad = 1) THEN
  BEGIN
    EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:Tipo_, :Prefijo_, :Numero_);
    EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:Tipo_, :Prefijo_, :Numero_);

    IF (:Numero_ < 0) THEN
    BEGIN

      SELECT Generador
      FROM Consecutivos
      WHERE Coddocumento = :Tipo_
            AND Codprefijo = :Prefijo_
            AND :V_Fecha BETWEEN Fecha AND Vence
      INTO V_Generador;

      EXECUTE STATEMENT 'SELECT GEN_ID(' || :V_Generador || ', 1)
    FROM Rdb$Database '
          INTO V_Consecutivo;

      UPDATE Comprobantes
      SET Numero = :V_Consecutivo
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_;

    END /*DEBE QUEDAR BLOQUEADO, TERMINADO, CON NUMERO CONSECUTIVO (POSITIVO) FE EN PENDIENTES*/

  END

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT,UPDATE ON COMPROBANTES TO PROCEDURE REST_TERMINA_COMPROBANTE;
GRANT EXECUTE ON PROCEDURE FX_RECALCULA_COMPROBANTE TO PROCEDURE REST_TERMINA_COMPROBANTE;
GRANT EXECUTE ON PROCEDURE FX_PERFECCIONA_EN_BATCH TO PROCEDURE REST_TERMINA_COMPROBANTE;
GRANT SELECT ON CONSECUTIVOS TO PROCEDURE REST_TERMINA_COMPROBANTE;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE REST_TERMINA_COMPROBANTE TO SYSDBA;


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_INSERT_CONTABLE_PRIMARIO (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    FECHA_ DATE,
    CUENTA_ VARCHAR(30),
    TERCERO_ VARCHAR(15),
    CENTRO_ VARCHAR(5),
    ACTIVO_ VARCHAR(15),
    EMPLEADO_ VARCHAR(15),
    DEBITO_ NUMERIC(17,4),
    CREDITO_ NUMERIC(17,4),
    BASE_ NUMERIC(17,4),
    NOTA_ VARCHAR(80),
    USUARIO_ VARCHAR(10))
AS
DECLARE VARIABLE V_Renglon INTEGER;
DECLARE VARIABLE V_Registro INTEGER;
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Doc VARCHAR(20);
DECLARE VARIABLE V_Atb_Tercero VARCHAR(1);
DECLARE VARIABLE V_Atb_Centro VARCHAR(1);
DECLARE VARIABLE V_Atb_Activo VARCHAR(1);
DECLARE VARIABLE V_Atb_Empleado VARCHAR(1);
DECLARE VARIABLE V_Validacion VARCHAR(10);
BEGIN

  V_Doc = TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_);

  /* Validamos que no exista el doc */
  SELECT COUNT(1)
  FROM Cocomp
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN

    /* Valida que la cuenta exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Cuenta(:Cuenta_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO EXISTE ***';

    /* Valida que la cuenta sea auxiliar */
    IF ((SELECT Nivel
         FROM Rest_Valida_Cuenta(:Cuenta_)) = 'S') THEN
      EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO PUEDE SER DE NIVEL ***';

    /* Atributos de la cuenta para validaciones de datos */
    SELECT IIF(COALESCE(Tercero, '') = '', 'N', Tercero),
           IIF(COALESCE(Centro, '') = '', 'N', Centro),
           IIF(COALESCE(Activo, '') = '', 'N', Activo),
           IIF(COALESCE(Empleado, '') = '', 'N', Empleado)
    FROM Cuentas
    WHERE Codigo = :Cuenta_
    INTO V_Atb_Tercero,
         V_Atb_Centro,
         V_Atb_Activo,
         V_Atb_Empleado;

    /* Validaciones para tercero */
    IF (V_Atb_Tercero = 'S') THEN
    BEGIN
      IF (TRIM(Tercero_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA TERCERO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el tercero exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Tercero_ = NULL;

    /* Validaciones para centro */
    IF (V_Atb_Centro = 'S') THEN
    BEGIN
      IF (TRIM(Centro_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA CENTRO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el centro exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Centro_ = NULL;

    /* Validaciones para activo */
    IF (V_Atb_Activo = 'S') THEN
    BEGIN
      IF (TRIM(Activo_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA ACTIVO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el activo exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Activo(:Activo_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL ACTIVO ' || TRIM(Activo_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Activo_ = NULL;

    /* Validaciones para empleado */
    IF (V_Atb_Empleado = 'S') THEN
    BEGIN
      IF (TRIM(Empleado_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA EMPLEADO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el empleado exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Empleado(:Empleado_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Empleado_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Empleado_ = NULL;

    /* Valida que el usuario exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

    /* Insertamos COCOMP */
    INSERT INTO Cocomp (Tipo, Prefijo, Numero, Fecha, Nota, Bloqueado, Codusuario, Codescenario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :Fecha_, '-', 'R', :Usuario_, 'NA');

    V_Renglon = (SELECT GEN_ID(Gen_Comovi, 1)
                 FROM Rdb$Database);

    /*Campo Validacion*/
    SELECT Validacion
    FROM Apo_Valida_Cuentas(:Cuenta_, NULL)
    INTO V_Validacion;

    /* Insertamos COMOVI */
    INSERT INTO Comovi (Tipo, Prefijo, Numero, Renglon, Debito, Credito, Base, Nota, Codcuenta, Codtercero, Codcentro,
                        Codactivo, Codpersonal, Codusuario, Validacion)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Debito_, :Credito_, :Base_, :Nota_, :Cuenta_, :Tercero_, :Centro_,
            :Activo_, :Empleado_, :Usuario_, :V_Validacion);

    /* Se organiza campo VALIDACION en COMOVI */
    --   EXECUTE PROCEDURE Apo_Repara_Validacion_Filtro(:Tipo_, :Prefijo_, :Numero_);
  END
  ELSE
  BEGIN
    /* Validamos la fecha del documento existente.
   Si es la misma del ws continuamos insertando. */
    SELECT Fecha
    FROM Cocomp
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Fecha;

    IF (V_Fecha = Fecha_) THEN
    BEGIN

      /* Validamos que el doc no este bloqueado */
      SELECT COUNT(1)
      FROM Cocomp
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
            AND (Bloqueado = 'R')
      INTO V_Registro;

      IF (V_Registro = 0) THEN
        EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' SE ENCUENTRA TERMINADO, POR FAVOR DESBLOQUEAR PARA CONTINUAR ***';
      ELSE


      /* Valida que la cuenta exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Cuenta(:Cuenta_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO EXISTE ***';

      /* Valida que la cuenta sea auxiliar */
      IF ((SELECT Nivel
           FROM Rest_Valida_Cuenta(:Cuenta_)) = 'S') THEN
        EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO PUEDE SER DE NIVEL ***';

      /* Atributos de la cuenta para validaciones de datos */
      SELECT IIF(COALESCE(Tercero, '') = '', 'N', Tercero),
             IIF(COALESCE(Centro, '') = '', 'N', Centro),
             IIF(COALESCE(Activo, '') = '', 'N', Activo),
             IIF(COALESCE(Empleado, '') = '', 'N', Empleado)
      FROM Cuentas
      WHERE Codigo = :Cuenta_
      INTO V_Atb_Tercero,
           V_Atb_Centro,
           V_Atb_Activo,
           V_Atb_Empleado;

      /* Validaciones para tercero */
      IF (V_Atb_Tercero = 'S') THEN
      BEGIN
        IF (TRIM(Tercero_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA TERCERO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el tercero exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Tercero_ = NULL;

      /* Validaciones para centro */
      IF (V_Atb_Centro = 'S') THEN
      BEGIN
        IF (TRIM(Centro_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA CENTRO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el centro exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Centro_ = NULL;

      /* Validaciones para activo */
      IF (V_Atb_Activo = 'S') THEN
      BEGIN
        IF (TRIM(Activo_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA ACTIVO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el activo exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Activo(:Activo_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL ACTIVO ' || TRIM(Activo_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Activo_ = NULL;

      /* Validaciones para empleado */
      IF (V_Atb_Empleado = 'S') THEN
      BEGIN
        IF (TRIM(Empleado_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA EMPLEADO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el empleado exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Empleado(:Empleado_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Empleado_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Empleado_ = NULL;

      /* Valida que el usuario exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

      V_Renglon = (SELECT GEN_ID(Gen_Comovi, 1)
                   FROM Rdb$Database);

      /*Campo Validacion*/
      SELECT Validacion
      FROM Apo_Valida_Cuentas(:Cuenta_, NULL)
      INTO V_Validacion;

      /* Insertamos COMOVI */
      INSERT INTO Comovi (Tipo, Prefijo, Numero, Renglon, Debito, Credito, Base, Nota, Codcuenta, Codtercero, Codcentro,
                          Codactivo, Codpersonal, Codusuario, Validacion)
      VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Debito_, :Credito_, :Base_, :Nota_, :Cuenta_, :Tercero_,
              :Centro_, :Activo_, :Empleado_, :Usuario_, :V_Validacion);

      /* Se organiza campo VALIDACION en COMOVI */
      --EXECUTE PROCEDURE Apo_Repara_Validacion_Filtro(:Tipo_, :Prefijo_, :Numero_);
    END
    ELSE
      EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' YA EXISTE ***';
  END
END^



SET TERM ; ^



/******************************************************************************/
/***                               Privileges                               ***/
/******************************************************************************/


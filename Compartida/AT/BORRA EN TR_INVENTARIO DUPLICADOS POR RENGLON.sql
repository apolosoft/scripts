EXECUTE BLOCK
AS
DECLARE VARIABLE V_Tipo       CHAR(5);
DECLARE VARIABLE V_Prefijo    CHAR(5);
DECLARE VARIABLE V_Numero     CHAR(10);
DECLARE VARIABLE V_Referencia CHAR(20);
DECLARE VARIABLE V_Min        INTEGER;
DECLARE VARIABLE V_Max        INTEGER;
BEGIN
  FOR SELECT Tipo,
             Prefijo,
             Numero
      FROM Comprobantes
      WHERE Tipo = 'FVR'
            AND Prefijo = 'FE'
            AND Numero >= 20924
            AND Numero <= 20931
      INTO V_Tipo,
           V_Prefijo,
           V_Numero
  DO
  BEGIN

    FOR SELECT DISTINCT Codreferencia
        FROM Tr_Inventario
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :V_Numero
        INTO V_Referencia
    DO
    BEGIN
      SELECT MIN(Renglon),
             MAX(Renglon)
      FROM Tr_Inventario
      WHERE Tipo = :V_Tipo
            AND Prefijo = :V_Prefijo
            AND Numero = :V_Numero
            AND Codreferencia = :V_Referencia
      INTO V_Min,
           V_Max;
      IF (V_Min <> V_Max) THEN
        DELETE FROM Tr_Inventario
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :V_Numero
              AND Renglon = :V_Max;
    END
  END
  FOR SELECT Tipo,
             Prefijo,
             Numero
      FROM Comprobantes
      WHERE Tipo = 'FVA'
            AND Prefijo = 'FE'
            AND Numero >= 20618
            AND Numero <= 21261
      INTO V_Tipo,
           V_Prefijo,
           V_Numero
  DO
  BEGIN

    FOR SELECT DISTINCT Codreferencia
        FROM Tr_Inventario
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :V_Numero
        INTO V_Referencia
    DO
    BEGIN
      SELECT MIN(Renglon),
             MAX(Renglon)
      FROM Tr_Inventario
      WHERE Tipo = :V_Tipo
            AND Prefijo = :V_Prefijo
            AND Numero = :V_Numero
            AND Codreferencia = :V_Referencia
      INTO V_Min,
           V_Max;
      IF (V_Min <> V_Max) THEN
        DELETE FROM Tr_Inventario
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :V_Numero
              AND Renglon = :V_Max;
    END
  END
END
CREATE OR ALTER PROCEDURE Pz_Lee_Detalle (
    Detalle_ BLOB SUB_TYPE 1 SEGMENT SIZE 80)
RETURNS (
    Detalle VARCHAR(100))
AS
DECLARE VARIABLE Posicion INTEGER;
DECLARE VARIABLE Pos      INTEGER;
BEGIN
  Posicion = POSITION('|', Detalle_);
  WHILE (Posicion > 0) DO
  BEGIN
    Detalle = SUBSTRING(Detalle_ FROM 1 FOR Posicion - 1);
    Pos = Posicion;
  END

  SUSPEND;
END
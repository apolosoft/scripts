UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 965 29/May/2024 v1

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (272, 'ges:transporte1', 'DOC', '-- Transporte 1
 SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr"
        FROM Fe_Vector_Transporte(:Tipo:, :Prefijo:, :Numero:, ''GESTION'')
        WHERE Renglon = :Renglon:
    AND  Natr IN (''01'', ''02'')', 'S', 32, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (273, 'ges:transporte2', 'DOC', '-- Transporte 2
 
SELECT   Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               Uatr "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
FROM Pz_Fe_Transporte(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
WHERE  Natr =''03''', 'S', 33, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (274, 'ges:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Apl1 "noov:Nvsal_pape",
       Apl2 "noov:Nvsal_sape",
       Nom1 "noov:Nvsal_pnom",
       Nom2 "noov:Nvsal_snom",
       '''' "noov:Nvsal_tusu",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       '''' "noov:Nvsal_npre",
       '''' "noov:Nvsal_ndip",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:)', 'S', 36, 'NOOVA', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuarioSalud', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (275, 'ges:salud_ben', 'DOC', '-- Sector Salud beneficiario

SELECT Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Nombre "noov:Nvsal_nomb",
       Apellido "noov:Nvsal_apel",
       Ciudad "noov:Nvsal_cciu",
       Direccion "noov:Nvsal_dire",
       Pais "noov:Nvsal_pais",
       ''Registraduria Nacional'' "noov:Nvent_nomb"
FROM Pz_Fe_Vector_Salud_Ben(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Renglon:)
WHERE Tdoc <> ''NA''', 'S', 37, 'NOOVA', 'FACTURA', 'noov:DTOBeneficiario', '', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (276, 'ges:salud_ref', 'DOC', '-- Sector Salud Referenciados

SELECT Nume "noov:Nvdoc_nume",
       Cufd "noov:Nvdoc_cufd",
       Algr "noov:Nvdoc_algr",
       Femi "noov:Nvdoc_femi",
       Idto "noov:Nvdoc_idto",
       Ndoc "noov:Nvusu_ndoc",
       Oper "noov:Nvfor_oper",
       Reps "noov:Nvsal_cpsa",
       '''' "noov:Nvsal_naut"
FROM Pz_Fe_Vector_Salud_Ref(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:)', 'S', 38, 'NOOVA', 'FACTURA', 'noov:lDocumentoReferencia', 'noov:DTODocumentoReferencia', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon:

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo:, :Prefijo:, :Numero:)
WHERE Renglon = :Renglon:', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
WHERE Nvfor_oper = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"

FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(COALESCE(Com_Total, 0), 2)) - (SELECT COALESCE(Valor, 0)
                                                          FROM Redondeo_Dian(COALESCE(Nvfac_Stot, 0), 2)) - (SELECT COALESCE(Valor, 0)
                                                                                                             FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                                 FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                                 WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                        FROM Redondeo_Dian(COALESCE(Nvfac_Desc, 0), 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste A Ds

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Zipc "noov:Nvpro_zipc",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE  "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (461, 'con:comprobante', 'DOC', '-- Comprobante Contable Factura 964 02/May/2024 v1

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM((SELECT Valor
                                                 FROM Redondeo_Dian("noov:Nvimp_valo", 2)))
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (462, 'con:detalle', 'DOC', '-- Detalle Contable Factura

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVAC', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (463, 'con:impuestos', 'DOC', '-- Impuestos Contable Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (464, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE FACTURA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (465, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Factura

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 25, 'NOOVAC', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (466, 'con:transporte1', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" IN (''01'', ''02'')', 'S', 26, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (467, 'con:transporte2', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr",
       "noov:Nvfac_uatr",
       "noov:Nvfac_catr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" =''03''', 'S', 27, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (481, 'con:comprobante', 'DOC', '-- Comprobante Contable Exportacion

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (482, 'con:detalle', 'DOC', '-- Detalle Contable Exportacion

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (483, 'con:impuestos', 'DOC', '--Impuestos CONTABLE EXPORTACION

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (484, 'con:tasa_cambio', 'DOC', '--Tasa Cambio CONTABLE EXPORTACION

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (501, 'con:comprobante', 'DOC', '--Comprobante CONTABLE CONTINGENCIA

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (502, 'con:detalle', 'DOC', '--Detalle CONTABLE CONTINGENCIA

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (503, 'con:impuestos', 'DOC', '--Impuestos CONTABLE CONTINGENCIA

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (504, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE CONTINGENCIA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (521, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Debito

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian(("noov:Nvfac_desc" + (SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
                                                          FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:))), 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (522, 'con:detalle', 'DOC', '-- Detalle Contable Nota Debito

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (523, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (524, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Debito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
                Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (525, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA DEBITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (541, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Credito

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (542, 'con:detalle', 'DOC', '-- Detalle Contable Nota Credito

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (543, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (544, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Credito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (545, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA CREDITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (561, 'con:comprobante', 'DOC', '-- Comprobante Contable Doc Soporte

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (562, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 40, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:Proveedor', NULL, '', 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (565, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Documento Soporte

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (581, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Ajuste a DS

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 5, 'NOOVAC', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (582, 'con:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero, :Emisor:, :Receptor:)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (583, 'con:detalle', 'DOC', '-- Detalle Contable Nota Ajuste a DS

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (584, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota de Ajuste a DS

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (585, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Nota de Ajuste a DS

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1005, 'nom:nomina', 'DOC', '-- Encabezado NE 963 Nomina 05/ABR/2024 v1

SELECT 1 "noov:Nvsuc_codi",
       ''NE'' "noov:Nvnom_pref",
       :Numero: "noov:Nvnom_cons",
       ''NE'' || :Numero: "noov:Nvnom_nume",
       ''NM'' "noov:Nvope_tipo",
       EXTRACT(YEAR FROM CAST(:Fecha_Hasta: AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) "noov:Nvnom_fpag",
       2000 "noov:Nvnom_redo",
       COALESCE(Adicion, 0) "noov:Nvnom_devt",
       COALESCE(Deduccion, 0) "noov:Nvnom_dedt",
       COALESCE(Neto, 0) "noov:Nvnom_comt",
       IIF(:Clase_Fe: = ''FACTURA'', '''', ''CUNE'') "noov:Nvnom_cnov",
       (SELECT Ajuste
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvnom_tipo"
FROM Pz_Ne_Resumen(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)', 'S', 5, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1010, 'nom:periodo', 'DOC', '-- Periodo

WITH V_Fechas
AS (SELECT Codpersonal,
           Desde,
           Hasta,
           MAX(Ingreso) Ingreso,
           MAX(Retiro) Retiro
    FROM (SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE) Desde,
                 CAST(:Fecha_Hasta: AS DATE) Hasta,
                 MAX(S.Desde) Ingreso,
                 CAST(''01/01/1900'' AS DATE) Retiro
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Desde = ''INGRESO''
                AND S.Desde <= :Fecha_Hasta:
          GROUP BY 1, 2, 3
          UNION
          SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE),
                 CAST(:Fecha_Hasta: AS DATE),
                 CAST(''01/01/1900'' AS DATE),
                 MAX(S.Hasta)
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Hasta = ''RETIRO''
                AND S.Hasta <= :Fecha_Hasta:
          GROUP BY 1, 2, 3)
    GROUP BY 1, 2, 3)

SELECT EXTRACT(YEAR FROM Fe.Ingreso) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Ingreso) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Ingreso) + 100, 2) "noov:Nvper_fing",
       IIF(Fe.Retiro >= Fe.Desde AND Fe.Retiro <= Fe.Hasta, EXTRACT(YEAR FROM Fe.Retiro) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Retiro) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Retiro) + 100, 2), EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2)) "noov:Nvper_fret",
       EXTRACT(YEAR FROM Fe.Desde) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Desde) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Desde) + 100, 2) "noov:Nvper_fpin",
       EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2) "noov:Nvper_fpfi",
       DATEDIFF(DAY FROM Fe.Ingreso TO Fe.Hasta)+1 "noov:Nvper_tlab"
FROM V_Fechas Fe', 'S', 10, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1015, 'nom:inf_general', 'DOC', '-- Inf general

SELECT (SELECT Tipo_Nomina
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvinf_tnom",
        P.Codtipoperiodo "noov:Nvinf_pnom",
        ''COP'' "noov:Nvinf_tmon"
FROM Personal P
WHERE P.Codigo = :Receptor:', 'S', 15, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1020, 'nom:notas', 'DOC', '-- Notas

SELECT DISTINCT N.Nombre "noov:string"
FROM Nominas N
JOIN Planillas P ON (N.Codigo = P.Codnomina)
WHERE P.Codpersonal = :Receptor:
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:', 'S', 20, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1025, 'nom:empleador', 'DOC', '-- Empleador

SELECT FIRST 1 T.Nombre "noov:Nvemp_nomb",
               T.Codigo "noov:Nvemp_nnit",
               T.Dv "noov:Nvemp_endv",
               Pa.Codigo_Fe "noov:Nvemp_pais",
               SUBSTRING(T.Codmunicipio FROM 1 FOR 2) "noov:Nvemp_depa",
               T.Codmunicipio "noov:Nvemp_ciud",
               T.Direccion "noov:Nvemp_dire"
FROM Terceros T
JOIN Paises Pa ON (T.Codpais = Pa.Codigo)
WHERE T.Codigo = :Emisor:', 'S', 25, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1030, 'nom:trabajador', 'DOC', '-- Trabajador

WITH V_Correo
AS (SELECT FIRST 1 Codigo,
                   TRIM(Email) Email,
                   COUNT(1) Cant
    FROM Contactos
    WHERE Codcargo = ''NECOR''
          AND Codtercero = :Emisor:
    GROUP BY 1, 2
    ORDER BY Codigo)
SELECT LPAD(IIF(TRIM(E.Codcotizante) = ''20'', ''23'', TRIM(E.Codcotizante)), 2, 0) "noov:Nvtra_tipo",
       LPAD(IIF(TRIM(E.Codsubcotizante) <> ''0'', ''1'', TRIM(E.Codsubcotizante)), 2, 0) "noov:Nvtra_stip",
       IIF(COALESCE(E.Alto_Riesgo, 0) = ''N'', ''false'', ''true'') "noov:Nvtra_arpe", --Actividades de alto riesgo
       IIF(E.Codidentidad = ''48'', ''47'', E.Codidentidad) "noov:Nvtra_dtip",
       E.Codigo "noov:Nvtra_ndoc",
       E.Apl1 "noov:Nvtra_pape",
       IIF(COALESCE(E.Apl2, '''') = '''', ''.'', E.Apl2) "noov:Nvtra_sape",
       E.Nom1 "noov:Nvtra_pnom",
       E.Nom2 "noov:Nvtra_onom",
       ''CO'' "noov:Nvtra_ltpa",
       SUBSTRING(E.Codmunicipio FROM 1 FOR 2) "noov:Nvtra_ltde",
       E.Codmunicipio "noov:Nvtra_ltci",
       E.Direccion "noov:Nvtra_ltdi",
       IIF(E.Salario_Integral = ''X'', ''true'', ''false'') "noov:Nvtra_sint",
       C.Codtipocontrato "noov:Nvtra_tcon", --Tipo Contrato
       COALESCE((SELECT FIRST 1 Basico
                 FROM Nom_Pila_Salarios(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvtra_suel",
       E.Codigo "noov:Nvtra_codt",
       IIF(Ne.Cant > 0, COALESCE(Ne.Email, E.Email), ''ne.mekano@gmail.com'') "noov:Nvtra_mail"
FROM Personal E
JOIN Identidades I ON (E.Codidentidad = I.Codigo)
LEFT JOIN Contratacion C ON (E.Codigo = C.Codpersonal)
LEFT JOIN V_Correo Ne ON 1 = 1
WHERE E.Codigo = :Receptor:
      AND ((C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:))', 'S', 30, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1035, 'nom:pago', 'DOC', '--  Pagos

WITH V_Medio
AS (SELECT COALESCE((SELECT FIRST 1 F.Codigo_Fe
                     FROM Medios_Pago M
                     JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
                     WHERE M.Codpersonal = :Receptor:),
           (SELECT FIRST 1 F.Codigo_Fe
            FROM Medios_Pago M
            JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
            WHERE Codpersonal IS NULL)) Medio
    FROM Rdb$Database)
SELECT 1 "noov:Nvpag_form",
       M.Medio "noov:Nvpag_meto", --Metodo de pago
       B.Nombre "noov:Nvpag_banc", --Nombre banco
       Tc.Nombre "noov:Nvpag_tcue",
       E.Banco "noov:Nvpag_ncue"
FROM Personal E
LEFT JOIN Entidades B ON (E.Codentidad = B.Codigo)
LEFT JOIN Tipocuentas Tc ON (E.Codtipocuenta = Tc.Codigo)
JOIN V_Medio M ON (1 = 1)
WHERE E.Codigo = :Receptor:', 'S', 35, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1040, 'nom:basico', 'DOC', '-- Basico

SELECT COALESCE((SELECT IIF(ROUND(Valor) = 0, 1, ROUND(Valor))
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DTRA'')), 0) "noov:Nvbas_dtra",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''STRA'')), 0) "noov:Nvbas_stra"
FROM Rdb$Database', 'S', 40, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1042, 'nom:transporte', 'DOC', '-- Transporte NE

SELECT SUM(COALESCE(Valor, 0)) "noov:Nvtrn_auxt",
       SUM(0) "noov:Nvbon_vias",
       SUM(0) "noov:Nvbon_vins"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUX'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(COALESCE(Valor, 0)),
       SUM(0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VIAS'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(0),
       SUM(COALESCE(Valor, 0))
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VINS'')
HAVING SUM(Valor) > 0', 'S', 42, 'NOOVA_NE', 'FACTURA', 'noov:Transporte', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1045, 'nom:horas_extras', 'DOC', '-- Horas Extras NE

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HENDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HED''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRNDF''
      AND Valor > 0', 'S', 45, 'NOOVA_NE', 'FACTURA', 'noov:DTOHorasExtras', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1160, 'nom:vacaciones', 'DOC', '-- Vacaciones NE

SELECT
--'''' "noov:Nvcom_fini",
--      '''' "noov:Nvcom_ffin",
       SUM(IIF(Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q''), IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0)) "noov:Nvcom_cant",
       SUM(IIF(Codgrupo_Ne IN (''VAC1S'', ''VAC2S''), Valor, 0)) "noov:Nvcom_pago",
       Codigo_Ne "noov:Nvvac_tipo"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q'', ''VAC1S'', ''VAC2S'')
GROUP BY 3
HAVING SUM(Valor) > 0', 'S', 160, 'NOOVA_NE', 'FACTURA', 'noov:DTOVacaciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1165, 'nom:primas', 'DOC', '-- Primas NE

SELECT SUM("noov:Nvpri_cant") "noov:Nvpri_cant",
       SUM("noov:Nvpri_pago") "noov:Nvpri_pago",
       SUM("noov:Nvpri_pagn") "noov:Nvpri_pagn"
FROM (SELECT COALESCE(IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0) "noov:Nvpri_cant",
             0 "noov:Nvpri_pago",
             0 "noov:Nvpri_pagn"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIQ'')

      UNION ALL

      SELECT 0,
             COALESCE(Valor, 0),
             0
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIP'')
            AND (Valor > 0)

      UNION ALL

      SELECT 0,
             0,
             COALESCE(Valor, 0)
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIPN'')
            AND (Valor > 0))
HAVING SUM("noov:Nvpri_cant" + "noov:Nvpri_pago" + "noov:Nvpri_pagn") > 1', 'S', 165, 'NOOVA_NE', 'FACTURA', 'noov:Primas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1170, 'nom:cesantias', 'DOC', '-- Cesantias NE

SELECT COALESCE((SELECT SUM(Valor)
                 FROM Pz_Ne_Grupo_Ces(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvces_pago",
       IIF(COALESCE((SELECT Valor
                     FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESPA'')), 0) = 0, 0,
       (SELECT Codigo_Ne
        FROM Grupo_Ne
        WHERE Codigo = ''CESPOR'')) "noov:Nvces_porc",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESIPA'')), 0) "noov:Nvces_pagi"
FROM Rdb$Database
WHERE COALESCE((SELECT SUM(Valor)
                FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
                WHERE Codgrupo_Ne IN (''CESPA'', ''CESIPA'')), 0) > 0', 'S', 170, 'NOOVA_NE', 'FACTURA', 'noov:Cesantias', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1175, 'nom:incapacidad', 'DOC', '-- Incapacidades NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvinc_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvinc_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOQ'', ''INPRQ'', ''INLAQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOV'', ''INPRV'', ''INLAV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 175, 'NOOVA_NE', 'FACTURA', 'noov:DTOIncapacidad', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1195, 'nom:licencias', 'DOC', '-- Licencias NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvlic_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvlic_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMQ'', ''LRQ'', ''LNQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMV'', ''LRV'', ''LNV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 195, 'NOOVA_NE', 'FACTURA', 'noov:DTOLicencia', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1197, 'nom:bonificacion', 'DOC', '-- Bonificacion NE

SELECT COALESCE(Valor, 0) "noov:Nvbon_bofs",
       0 "noov:Nvbon_bons"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BOFS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BONS'')
WHERE (Valor > 0)', 'S', 197, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonificacion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1199, 'nom:auxilios', 'DOC', '-- Auxilios

SELECT COALESCE(Valor, 0) "noov:Nvaux_auxs",
       0 "noov:Nvaux_auns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUXS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUNS'')
WHERE (Valor > 0)', 'S', 199, 'NOOVA_NE', 'FACTURA', 'noov:DTOAuxilio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1200, 'nom:huelgas', 'DOC', '-- Huelgas NE

WITH V_Pago
AS (SELECT Gn.Codigo Tipo,
           SUM(P.Adicion + P.Deduccion) Valor
    FROM Planillas P
    JOIN Nominas N ON (P.Codnomina = N.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
    LEFT JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
    WHERE P.Codpersonal = :Receptor:
          AND N.Fecha >= :Fecha_Desde:
          AND N.Fecha <= :Fecha_Hasta:
          AND R.Codgrupo_Ne = ''HUV''
    GROUP BY 1)

SELECT G.Inicio "noov:Nvcom_fini",
       G.Fin "noov:Nvcom_ffin",
       P.Valor "noov:Nvcom_cant",
       Pago.Valor "noov:Nvcom_pago"
FROM Planillas P
JOIN Nominas N ON (P.Codnomina = N.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
JOIN V_Pago Pago ON (Gn.Codigo = Pago.Tipo)
WHERE P.Codpersonal = :Receptor:
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:
      AND R.Codgrupo_Ne = ''HUQ''', 'S', 235, 'NOOVA_NE', 'FACTURA', 'noov:DTOHuelgaLegal', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1205, 'nom:bonos', 'DOC', '-- Bonos

SELECT COALESCE(IIF(Codgrupo_Ne = ''PAGS'', Valor, 0), 0) "noov:Nvbon_pags",
       COALESCE(IIF(Codgrupo_Ne = ''PANS'', Valor, 0), 0) "noov:Nvbon_pans",
       COALESCE(IIF(Codgrupo_Ne = ''ALIS'', Valor, 0), 0) "noov:Nvbon_alis",
       COALESCE(IIF(Codgrupo_Ne = ''ALNS'', Valor, 0), 0) "noov:Nvbon_alns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''PAGS'', ''PANS'', ''ALIS'', ''ALNS'')', 'S', 236, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonoEPCTV', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1210, 'nom:comision', 'DOC', '-- Comisiones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''COMIS'')
WHERE Valor > 0', 'S', 210, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1215, 'nom:dotacion', 'DOC', '-- Dotacion

SELECT Valor "noov:Dotacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DOTAC'')
WHERE Valor > 0', 'S', 215, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1240, 'nom:otro_devengados', 'DOC', '-- Otros devengados

SELECT NOMRUBRO "noov:Nvotr_desc",
       IIF(CODGRUPO_NE = ''ODSA'', VALOR, 0) "noov:Nvotr_pags",
       IIF(CODGRUPO_NE = ''ODNS'', VALOR, 0) "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE (CODGRUPO_NE IN (''ODSA'', ''ODNS''))
      AND (VALOR > 0)

UNION ALL

SELECT ''CAUSACION VACACIONES'' "noov:Nvotr_desc",
       0 "noov:Nvotr_pags",
       VALOR "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE CODGRUPO_NE = ''ODNS_V''
      AND VALOR > 0', 'S', 240, 'NOOVA_NE', 'FACTURA', 'noov:DTOOtroDevengado', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1355, 'nom:salud', 'DOC', '-- Salud NE

SELECT COALESCE((SELECT IIF(Codcotizante IN (''51'', ''12'', ''19''), 0, Codigo_Ne)
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_dedu"
FROM Rdb$Database', 'S', 355, 'NOOVA_NE', 'FACTURA', 'noov:Salud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1360, 'nom:pension', 'DOC', '-- Pension NE

SELECT COALESCE((SELECT Codigo_Ne
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_dedu"
FROM Rdb$Database', 'S', 360, 'NOOVA_NE', 'FACTURA', 'noov:FondoPension', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1365, 'nom:fondosp', 'DOC', 'SELECT "noov:Nvfsp_porc",
       "noov:Nvfsp_dedu",
       "noov:Nvfsp_posb",
       "noov:Nvfsp_desb"
FROM Pz_Ne_Fondo_Sp(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE "noov:Nvfsp_dedu" + "noov:Nvfsp_desb" > 0', 'S', 365, 'NOOVA_NE', 'FACTURA', 'noov:FondoSP', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1370, 'nom:sindicato', 'DOC', '-- Sindicato NE

SELECT Valor "noov:Nvsin_porc",
       0 "noov:Nvsin_dedu"
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''PORSIN''
      AND Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''VAL_SIN''
      AND Valor > 0', 'S', 370, 'NOOVA_NE', 'FACTURA', 'noov:DTOSindicato', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1375, 'nom:sancion', 'DOC', '-- Sancion NE

SELECT Valor "noov:Nvsan_sapu",
       0 "noov:Nvsan_sapv"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPU'')
WHERE Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPV'')
WHERE Valor > 0', 'S', 375, 'NOOVA_NE', 'FACTURA', 'noov:DTOSancion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1380, 'nom:libranzas', 'DOC', '-- Libranzas

SELECT Nomrubro "noov:Nvlib_desc",
       Valor "noov:Nvlib_dedu"
FROM Pz_Ne_Grupo_Base(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE Codgrupo_Ne = ''LIBRA''
      AND Valor > 0', 'S', 380, 'NOOVA_NE', 'FACTURA', 'noov:DTOLibranza', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1382, 'nom:pag_ter', 'DOC', '-- Pagos a Terceros

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''PAG_TER'')
WHERE Valor > 0', 'S', 382, 'NOOVA_NE', 'FACTURA', 'noov:LPagosTerceros', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1383, 'nom:anticipos', 'DOC', '-- Anticipos

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT02'')
WHERE Valor > 0', 'S', 383, 'NOOVA_NE', 'FACTURA', 'noov:LAnticipos', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1384, 'nom:pvoluntaria', 'DOC', '-- Pension Voluntaria

SELECT Valor "noov:PensionVoluntaria"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT03'')
WHERE Valor > 0', 'S', 384, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1385, 'nom:retencionfuente', 'DOC', '-- RetencionFuente

SELECT Valor "noov:RetencionFuente"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT04'')
WHERE Valor > 0', 'S', 385, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1386, 'nom:ahorrofomentoconstr', 'DOC', '-- Ahorro Fomento Construccion

SELECT Valor "noov:AhorroFomentoConstr"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT06'')
WHERE Valor > 0', 'S', 386, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1387, 'nom:cooperativa', 'DOC', '-- Cooperativa

SELECT Valor "noov:Cooperativa"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT07'')
WHERE Valor > 0', 'S', 387, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1388, 'nom:embargofiscal', 'DOC', '-- Embargo Fiscal

SELECT Valor "noov:EmbargoFiscal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT08'')
WHERE Valor > 0', 'S', 388, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1389, 'nom:plancomplementarios', 'DOC', '-- Plan Complementarios

SELECT Valor "noov:PlanComplementarios"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT09'')
WHERE Valor > 0', 'S', 389, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1390, 'nom:educacion', 'DOC', '-- Educacion

SELECT Valor "noov:Educacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT10'')
WHERE Valor > 0', 'S', 390, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1391, 'nom:reintegro', 'DOC', '-- Reintegro

SELECT Valor "noov:Reintegro"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT11'')
WHERE Valor > 0', 'S', 391, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1392, 'nom:deuda', 'DOC', '-- Deuda

SELECT Valor "noov:Deuda"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT12'')
WHERE Valor > 0', 'S', 392, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1393, 'nom:otras_ded', 'DOC', '-- Otros deducciones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT00'')
WHERE Valor > 0', 'S', 393, 'NOOVA_NE', 'FACTURA', 'noov:LOtrasDeducciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1394, 'nom:predecesor', 'DOC', '-- Predecesor

SELECT Numero "noov:Nvpre_nume",
       Cune "noov:Nvpre_cune",
       EXTRACT(YEAR FROM CAST(Fecha_Ne AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(Fecha_Ne AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(Fecha_Ne AS DATE)) + 100, 2) "noov:Nvpre_fgen"
FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE trim(Tipo_Nomina) = 103', 'S', 394, 'NOOVA_NE', 'FACTURA', 'noov:Predecesor', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1395, 'nom:indemnizacion', 'DOC', '--INDEMNIZACION

SELECT Valor "noov:indemnizacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''INDEM'')
WHERE Valor > 0', 'S', 216, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1561, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 964 21/May/2024 v1

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 5, 'NOOVA', 'POS ELECTRONICO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1562, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'POS ELECTRONICO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1563, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)', 'S', 15, 'NOOVA', 'POS ELECTRONICO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1564, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'POS ELECTRONICO', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1565, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'POS ELECTRONICO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1566, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'POS ELECTRONICO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1567, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'POS ELECTRONICO', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1568, 'ges:comprador', 'DOC', '-- Beneficios Comprador

SELECT Codigo "noov:Codigo",
       Nombresapellidos "noov:NombresApellidos",
       Cant_Puntos "noov:Puntos"
FROM Pz_Fe_Benef_Comprador(:Tipo:, :Prefijo:, :Numero:)', 'S', 40, 'NOOVA', 'POS ELECTRONICO', 'noov:InformacionBeneficiosComprador', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1569, 'ges:caja_venta', 'DOC', '-- Informacion Caja Venta

SELECT Placa "noov:PlacaCaja",
       Ubicacion "noov:UbicacionCaja",
       Cajero "noov:Cajero",
       Tipo_Caja "noov:TipoCaja",
       Codigo_Venta "noov:CodigoVenta",
       (SELECT Valor
        FROM Redondeo_Dian(Subtotal, 2)) "noov:Subtotal"
FROM Pz_Fe_Datos_Caja(:Tipo:, :Prefijo:, :Numero:)
WHERE TRIM(Codigo_Fe) = ''POS ELECTRONICO''', 'S', 45, 'NOOVA', 'POS ELECTRONICO', 'noov:InformacionCajaVenta', NULL, NULL, '')
                        MATCHING (ID);


COMMIT WORK;


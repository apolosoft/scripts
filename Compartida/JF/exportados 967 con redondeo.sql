UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (461, 'con:comprobante', 'DOC', '-- Comprobante Contable Factura 967 20/Ago/2024 v1

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM((SELECT Valor
                                                 FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)))
                                     FROM Pz_Fe_Auto_Imp(:Tipo: , :Prefijo: , :Numero: )
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo: , :Prefijo: , :Numero: , :Emisor: , :Receptor: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (462, 'con:detalle', 'DOC', '-- Detalle Contable Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (463, 'con:impuestos', 'DOC', '-- Impuestos Contable Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (464, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE FACTURA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (465, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Factura

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 25, 'NOOVAC', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (466, 'con:transporte1', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" IN (''01'', ''02'')', 'S', 26, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (467, 'con:transporte2', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr",
       "noov:Nvfac_uatr",
       "noov:Nvfac_catr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" =''03''', 'S', 27, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (481, 'con:comprobante', 'DOC', '-- Comprobante Contable Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (482, 'con:detalle', 'DOC', '-- Detalle Contable Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (483, 'con:impuestos', 'DOC', '--Impuestos CONTABLE EXPORTACION

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (484, 'con:tasa_cambio', 'DOC', '--Tasa Cambio CONTABLE EXPORTACION

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (501, 'con:comprobante', 'DOC', '--Comprobante CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (502, 'con:detalle', 'DOC', '--Detalle CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (503, 'con:impuestos', 'DOC', '--Impuestos CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (504, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE CONTINGENCIA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (521, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian(("noov:Nvfac_desc" + (SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
                                                          FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:))), Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (522, 'con:detalle', 'DOC', '-- Detalle Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (523, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (524, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Debito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
                Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (525, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA DEBITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (541, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (542, 'con:detalle', 'DOC', '-- Detalle Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))
SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (543, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (544, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Credito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (545, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA CREDITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (561, 'con:comprobante', 'DOC', '-- Comprobante Contable Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (562, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 40, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:Proveedor', NULL, '', 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (565, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Documento Soporte

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (581, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (582, 'con:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero, :Emisor:, :Receptor:)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (583, 'con:detalle', 'DOC', '-- Detalle Contable Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (584, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota de Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (585, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Nota de Ajuste a DS

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


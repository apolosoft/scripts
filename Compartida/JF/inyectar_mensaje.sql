EXECUTE PROCEDURE Mensaje_Global_Envia('Buenos días!
Recuerden que a partir de Mayo 1/2024 entro en vigencia el Anexo técnico 1.9 de la DIAN, uno de los requisitos es que la facturación electrónica debe ser enviada el mismo día.

Ante lo cual se sugiere que el usuario encargado verifique constantemente, que no le hayan quedado facturas electrónicas en estado pendiente o rechazado, porque al siguiente día ya no podrá enviarlas a la DIAN; en tal caso, el usuario deberá organizar la fecha a esos documentos pendientes, asignando a cada uno la fecha actual.

Cordial Saludo.');

COMMIT WORK;


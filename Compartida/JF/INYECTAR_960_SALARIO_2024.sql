/* SALARIO MINIMO 2024 */

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P80300', 'ACTUALIZA EL SALARIO MINIMO PARA 2024  (1.300.000)', 'Crea nuevos registros de salarios solo a los que tengan salario minimo', 'EXECUTE BLOCK
AS
DECLARE VARIABLE V_2024        INTEGER;
DECLARE VARIABLE V_Codpersonal VARCHAR(15);
DECLARE VARIABLE V_Sql         VARCHAR(500);
BEGIN
  FOR SELECT Codpersonal
      FROM Salarios
      WHERE Basico = 1160000
            AND Desde >= ''01/01/2023''
            AND Hasta IN (''31.12.2023'',''30.12.2023'')
            AND TRIM(COALESCE(Columna_Hasta, '''')) <> ''RETIRO''
      INTO V_Codpersonal
  DO
  BEGIN
    -- Valida si ya tiene datos para 2023
    V_Sql = ''SELECT COUNT(1)
    FROM Salarios
    WHERE Codpersonal = '''''' || V_Codpersonal || ''''''
          AND Desde = ''''01/01/2024'''''';

    EXECUTE STATEMENT(V_Sql)
        INTO V_2024;
    IF (V_2024 = 0) THEN
    BEGIN
      V_Sql = ''INSERT INTO Salarios (Codpersonal, Renglon, Basico, Desde, Hasta,
                            Columna_Desde)
      VALUES ('''''' || V_Codpersonal || '''''', (SELECT GEN_ID(Gen_Aportes, 1)
                               FROM Rdb$Database), 1300000, ''''01.01.2024'''',
              ''''31.12.2024'''', ''''VARIACION PERMANENTE'''')'';
      EXECUTE STATEMENT (V_Sql);
    END
  END
END', 'N', 'NOMINA', 'RUTINAS ESPECIALES', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


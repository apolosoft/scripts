UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LOG010101', 'RELACION LOG DE CAMBIOS TABLA ENSAMBLES', 'Muestra cambios sobre la tabla Ensambes.  Basta con indicar rango de fecha, solo ensamble o por todos', 'SELECT id,
       codreferencia ensamble,
       (SELECT nombre_referencia
        FROM fn_nombre_referencia(codreferencia)) nombre_ensamble,
       codreferencia2 componente,
       (SELECT nombre_referencia
        FROM fn_nombre_referencia(codreferencia2)) nombre_componente,
       cantidad,
       codusuario usuario,
       SUBSTRING(fecha_accion FROM 1 FOR 19) fecha_accion,
       accion
FROM log_ensambles
WHERE fecha_accion >= CAST(:_desde AS DATE) || '' 00:00:00'' AND
      fecha_accion <= CAST(:_hasta AS DATE) || '' 23:59:59'' AND
      trim(codreferencia) LIKE :ensamble
ORDER BY id', 'S', 'HERRAMIENT', 'N', 'AUDITORIAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


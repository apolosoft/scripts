CREATE OR ALTER PROCEDURE X_Valida_Nominas
RETURNS (
    Registros_Nomina INTEGER)
AS
DECLARE VARIABLE V_Plani INTEGER;
DECLARE VARIABLE V_Nomi INTEGER;

BEGIN
  SELECT COUNT(1)
  FROM Planillas
  INTO V_Plani;

  SELECT COUNT(1) D
  FROM Reg_Nomina
  INTO V_Nomi;
  Registros_Nomina = V_Plani + V_Nomi;
  SUSPEND;
END;


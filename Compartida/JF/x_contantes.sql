CREATE OR ALTER PROCEDURE X_Constantes
AS
BEGIN
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C05_S_INT', 'FACTOR SALARIO INTEGRAL', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C36_AFP_S1',
          'PORCENTAJE DE SUBSISTENCIA ADICIONAL 16 SMMLV <= 17 SMMLV 0,2%',
          'B_03', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C36_AFP_S2',
          'PORCENTAJE DE SUBSISTENCIA ADICIONAL >= 17 SMMLV <= 18 SMMLV 0,4%',
          'B_03', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C36_AFP_S3',
          'PORCENTAJE DE SUBSISTENCIA ADICIONAL >= 18 SMMLV <= 19 SMMLV 0,6%',
          'B_03', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C36_AFP_S4',
          'PORCENTAJE DE SUBSISTENCIA ADICIONAL >= 19 SMMLV = 20 SMMLV 0,8%',
          'B_03', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C36_AFP_S5', 'PORCENTAJE DE SUBSISTENCIA ADICIONAL > 20 SMMLV  1%',
          'B_03', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C16_RNDCC',
          'FACTOR RECARGO NOCTURNO DOMINICAL CON COMPENSATORIO 110%', NULL,
          NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C18_RDFCC',
          'FACTOR RECARGO DOMINICAL / FESTIVO CON COMPENSATORIO 75%', NULL,
          NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C44_EXO', 'EMPRESA EXONERADA (1=S y 0=N)', 'B_10', 'C_76', NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C00_VERSION', 'FECHA DE VERSION DE RUBROS', NULL, NULL, NULL, 'N');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C13_HEND', 'FACTOR HORA EXTRA NOCTURNA DOMINICAL / FESTIVO 250%',
          NULL, NULL, 'HENDF', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C01_SMLV', 'SALARIO MINIMO LEGAL VIGENTE', 'C_00', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C02_AUXT', 'AUXILIO DE TRANSPORTE', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C03_HMES', 'HORAS MES', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C10_HED', 'FACTOR HORA EXTRA DIURNA 125%', NULL, NULL, 'HED', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C11_HEN', 'FACTOR HORA EXTRA NOCTURNA 175%', NULL, NULL, 'HEN', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C30_EPS_PE', 'PORCENTAJE DESCUENTO EPS EMPLEADO', 'B_04', NULL, NULL,
          'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C33_AFP_PE', 'PORCENTAJE DESCUENTO AFP EMPLEADO', NULL, NULL, NULL,
          'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C22_PR_CES', 'PROVISION CESANTIAS', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C21_PR_PRI', 'PROVISION PRIMA DE SERVICIOS', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C20_PR_VAC', 'PROVISION VACACIONES', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C23_PR_INT', 'PROVISION INTERESES A LAS CESANTIAS', NULL, NULL,
          'CESPOR', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C38_ARL', 'PORCENTAJE APORTE ARL Y NIVEL DE RIESGO', 'C_61', 'C_78',
          NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C37_CCF', 'PORCENTAJE APORTE CCF', 'C_64', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C31_EPS_EM', 'PORCENTAJE APORTE EPS EMPRESA', 'B_01', NULL, NULL,
          'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C39_ICBF', 'PORCENTAJE APORTE ICBF', 'C_68', '', NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C34_AFP_EM', 'PORCENTAJE APORTE AFP EMPRESA', 'B_00', NULL, NULL,
          'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C40_SENA', 'PORCENTAJE APORTES SENA', 'C_66', '', NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C14_RN', 'FACTOR RECARGO NOCTURNO 35%', NULL, NULL, 'HRN', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C15_RND', 'FACTOR RECARGO NOCTURNO DOMINICAL 210%', NULL, NULL,
          'HRNDF', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C17_RDF', 'FACTOR RECARGO DOMINICAL / FESTIVO 175%', NULL, NULL,
          'HRDDF', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C12_HEDD', 'FACTOR HORA EXTRA DIURNA DOMINICAL / FESTIVO 200%', NULL,
          NULL, 'HEDDF', 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C36_AFP_SP', 'PORCENTAJE DESCUENTO AFP SOLIDARIDAD PENSIONAL',
          'B_02', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C04_S_IGE', 'FACTOR SALARIO INCAPACIDAD', NULL, NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C41_ESAP', 'PORCENTAJE APORTE ESAP', 'C_70', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C42_MEN', 'PORCENTAJE APORTES MEN', 'C_72', NULL, NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C43_CT', 'CODIGO DEL CENTRO DE TRABAJO PARA PILA', 'C_62', NULL,
          NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C35_AFP_PI',
          'TARIFA AFP COMPLETA PARA PILA E INDICADOR TARIFA ESPECIAL', 'C_46',
          'C_79', NULL, 'S');
  INSERT INTO Constantes (Codigo, Nombre, Columna_Pila, Columna_Texto,
                          Codgrupo_Ne, Nativo)
  VALUES ('C32_EPS_PI', 'TARIFA EPS COMPLETA PARA PILA', 'C_54', '', NULL, 'S');
END;


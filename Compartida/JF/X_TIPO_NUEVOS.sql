CREATE OR ALTER PROCEDURE X_Tipo_Nuevos
AS
BEGIN
  INSERT INTO Tipocontratos (Codigo, Nombre)
  VALUES ('1', 'TERMINO FIJO');
  INSERT INTO Tipocontratos (Codigo, Nombre)
  VALUES ('2', 'TERMINO INDEFINIDO');
  INSERT INTO Tipocontratos (Codigo, Nombre)
  VALUES ('3', 'OBRA O LABOR');
  INSERT INTO Tipocontratos (Codigo, Nombre)
  VALUES ('4', 'APRENDIZAJE');
  INSERT INTO Tipocontratos (Codigo, Nombre)
  VALUES ('5', 'PRACTICAS');

  INSERT INTO Tipocuentas (Codigo, Nombre)
  VALUES ('A', 'CUENTA DE AHORROS');
  INSERT INTO Tipocuentas (Codigo, Nombre)
  VALUES ('C', 'CUENTA DE CORRIENTE');
  INSERT INTO Tipocuentas (Codigo, Nombre)
  VALUES ('DP', 'DAVIPLATA');
  INSERT INTO Tipocuentas (Codigo, Nombre)
  VALUES ('NE', 'NEQUI');

  INSERT INTO Tipoperiodos (Codigo, Nombre)
  VALUES ('1', 'SEMANAL');
  INSERT INTO Tipoperiodos (Codigo, Nombre)
  VALUES ('2', 'DECENAL');
  INSERT INTO Tipoperiodos (Codigo, Nombre)
  VALUES ('3', 'CATORCENAL');
  INSERT INTO Tipoperiodos (Codigo, Nombre)
  VALUES ('4', 'QUINCENAL');
  INSERT INTO Tipoperiodos (Codigo, Nombre)
  VALUES ('5', 'MENSUAL');

  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('00', 'BANCO DE LA REPUBLICA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('01', 'BANCO DE BOGOTA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('02', 'BANCO POPULAR');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('06', 'ITAU CORPBANCA COLOMBIA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('07', 'BANCOLOMBIA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('09', 'CITIBANK COLOMBIA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('12', 'GNB SUDAMERIS S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('13', 'BBVA COLOMBIA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('19', 'COLPATRIA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('23', 'BANCO DE OCCIDENTE');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('32', 'BANCO CAJA SOCIAL - BCSC S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('40', 'BANCO AGRARIO DE COLOMBIA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('51', 'BANCO DAVIVIENDA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('52', 'BANCO AV VILLAS');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('53', 'BANCO W S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('58', 'BANCO CREDIFINANCIERA S.A.C.F');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('59', 'BANCAMIA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('60', 'BANCO PICHINCHA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('61', 'BANCOOMEVA');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('62', 'CMR FALABELLA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('63', 'BANCO FINANDINA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('65', 'BANCO SANTANDER DE NEGOCIOS COLOMBIA S.A.');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('66', 'BANCO COOPERATIVO COOPCENTRAL');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('67', 'BANCO COMPARTIR S.A');
  INSERT INTO Entidades (Codigo, Nombre)
  VALUES ('69', 'BANCO SERFINANZA S.A');
END;


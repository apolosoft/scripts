
/* TRANSPORTE */

SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_TRANSPORTE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(10),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    ATRI VARCHAR(1),
    NATR VARCHAR(2),
    VATR VARCHAR(20),
    UATR VARCHAR(20),
    CATR VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^


SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_TRANSPORTE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(10),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    ATRI VARCHAR(1),
    NATR VARCHAR(2),
    VATR VARCHAR(20),
    UATR VARCHAR(20),
    CATR VARCHAR(20))
AS
BEGIN

  FOR SELECT Atri,
             Natr,
             Vatr,
             Uatr,
             Catr
      FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
      WHERE Renglon = :Renglon_
      INTO Atri,
           Natr,
           Vatr,
           Uatr,
           Catr
  DO
  BEGIN

    IF (TRIM(COALESCE(Uatr, '')) NOT IN ('KGM', 'GLL')) THEN
      Uatr = 'KGM';

    SUSPEND;
  END
END^

SET TERM ; ^

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (273, 'ges:transporte2', 'DOC', '-- Transporte 2
 
SELECT   Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               Uatr "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
FROM Pz_Fe_Transporte(:Tipo, :Prefijo, :Numero, :Renglon)  
WHERE  Natr =''03''', 'S', 33, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);

COMMIT WORK;



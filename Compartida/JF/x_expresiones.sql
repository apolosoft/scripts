CREATE OR ALTER PROCEDURE X_Expresiones_1
AS
BEGIN
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('PROM_HR_VA', 'PROMEDIO HORAS EXTRAS Y RECARGOS NOCTURNOS VACACIONES', '--he_re_vaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina     CHAR(5);
DECLARE V_Liq        CHAR(10);
DECLARE V_Desde_Nom  DATE;
DECLARE V_Hasta_Nom  DATE;
DECLARE V_Fecha_Nom  DATE;
DECLARE V_Fecha_Ini  DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Dias       NUMERIC(17,4);
DECLARE V_Rubro1     CHAR(5) = ''HE024'';
DECLARE V_Rubro2     CHAR(5) = ''HE025'';
DECLARE V_Rubro3     CHAR(5) = ''HE026'';
DECLARE V_Rubro4     CHAR(5) = ''HE027'';
DECLARE V_Rubro5     CHAR(5) = ''HE028'';
DECLARE V_Rubro_Dias CHAR(5) = ''DI015'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Fecha, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Fecha_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing

  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini AND
          N.Fecha <= :V_Fecha_Nom AND
          S.Codrubro = :V_Rubro_Dias AND
          S.Codpersonal = :Empleado
    INTO V_Dias;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini AND
          N.Fecha <= :V_Fecha_Nom AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3, :V_Rubro4, :V_Rubro5) AND
          S.Codpersonal = :Empleado
    INTO Valor;
    IF (V_Dias > 30) THEN
      IF (V_Dias > 0) THEN
        Valor = ROUND(Valor / V_Dias * 30);
      ELSE
        Valor = 0;
    Valor = COALESCE(Valor, 0);

    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIAS_PRIMA', 'DIAS PARA LIQUIDACION PRIMA', 'EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina     CHAR(5);

DECLARE V_Liq        CHAR(10);
DECLARE V_Sena       CHAR(2);
DECLARE V_Integral   CHAR(1);
DECLARE V_Fecha      DATE;
DECLARE V_Desde      DATE;
DECLARE V_Hasta      DATE;
DECLARE M_Hasta      DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Rubro1     CHAR(5) = ''LD070'';
DECLARE V_Rubro2     CHAR(5) = ''LD075'';
DECLARE V_Rubro_Dias1 CHAR(5) = ''DI015'';
DECLARE V_Rubro_Dias2 CHAR(5) = ''SI050'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha,
       V_Desde,
       V_Hasta,
       V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante,
                      P.Salario_Integral
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena,
           V_Integral
  DO
  BEGIN
    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado
          AND S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha
          AND N.Fecha >= :V_Desde
          AND S.Codpersonal = :Empleado
    INTO M_Hasta;

    M_Hasta = IIF(M_Hasta IS NULL, V_Desde, M_Hasta + 1);

    M_Hasta = IIF(V_Fecha_Ing > M_Hasta, V_Fecha_Ing, M_Hasta);

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :M_Hasta
          AND N.Fecha <= :V_Fecha
          AND S.Codrubro IN (:V_Rubro_Dias1, :V_Rubro_Dias2)
          AND S.Codpersonal = :Empleado
    INTO Valor;

    IF (Valor IS NOT NULL) THEN
    BEGIN
      IF (V_Sena IN (''12'', ''19'', ''23'') OR V_Integral = ''S'') THEN
        Valor = 0;
      SUSPEND;
    END
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIAS_VACA', 'DIAS PARA VACACIONES', '--Dias Vacaciones
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina         CHAR(5);
DECLARE V_Sena           CHAR(2);
DECLARE V_Liq            CHAR(10);
DECLARE V_Fecha_Nom      DATE;
DECLARE V_Desde_Nom      DATE;
DECLARE V_Hasta_Nom      DATE;
DECLARE V_Dias_Pag       NUMERIC(17,4);
DECLARE V_Rubro_Dias_Pag1 CHAR(5) = ''VA012'';
DECLARE V_Rubro_Dias_Pag2 CHAR(5) = ''DI011'';
DECLARE V_Rubro_Dias_Pag3 CHAR(5) = ''DI010'';
DECLARE V_Rubro1         CHAR(5) = ''SI015'';
DECLARE V_Rubro2         CHAR(5) = ''DI015'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena
  DO
  BEGIN
    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Desde >= :V_Desde_Nom AND
          N.Fecha <= :V_Fecha_Nom AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2) AND
          S.Codpersonal = :Empleado
    INTO Valor;

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Desde >= :V_Desde_Nom AND
          N.Fecha <= :V_Fecha_Nom AND
          S.Codrubro in (:V_Rubro_Dias_Pag1, :V_Rubro_Dias_Pag2, :V_Rubro_Dias_Pag3) AND
          S.Codpersonal = :Empleado
    INTO V_Dias_Pag;
    V_Dias_Pag = COALESCE(V_Dias_Pag, 0);
    Valor = Valor - V_Dias_Pag * 360 / 15;

    IF (Valor IS NOT NULL) THEN
    BEGIN
      IF (V_Sena IN (''12'', ''19'', ''23'')) THEN
        Valor = 0;
      SUSPEND;
    END
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_INTEGRA', 'VALIDACION SI EMPLEADO TIENE SALARIO INTEGRAL (1)', '--SALARIO INTEGRAL (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);

DECLARE V_Liq CHAR(10);
DECLARE V_Integral CHAR(1);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Salario_Integral
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Integral
  DO
  BEGIN
    Valor = 0;
    IF (V_Integral IN (''X'', ''S'')) THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_SENA_PR',
          'VALIDACION SI EMPLEADO ES APRENDIZ SENA ETAPA PRODUCTIVA(1) - COTIZANTE', '--ES SENA (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_Sena      CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena
  DO
  BEGIN
    Valor = 0;
    IF (V_Sena=''19'') THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_SENA',
          'VALIDACION SI EMPLEADO ES APRENDIZ SENA DEVUELVE (1) - COTIZANTE', '--ES SENA (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_Sena      CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena
  DO
  BEGIN
    Valor = 0;
    IF (V_Sena IN (''12'', ''19'')) THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_PENSION',
          'VALIDACION SI EMPLEADO ES PENSIONADO DEVUELVE (1) - SUBCOTIZANTE', '--ES PENSION (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);

DECLARE V_Liq CHAR(10);
DECLARE V_Pension CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codsubcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Pension
  DO
  BEGIN
    Valor = 0;
    IF (TRIM(COALESCE(V_Pension, '''')) NOT IN ('''', ''0'', ''00'')) THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ANTE_SAL', 'SALARIO ANTERIORES PERIODOS EN EL MISMO MES', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Rubro1 CHAR(5) = ''SA026'';
DECLARE V_Rubro2 CHAR(5) = ''VA080'';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom,
       V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
      INTO Empleado
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom)
          AND EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom)
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado
          AND N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('SAL_PREST', 'SALARIO BASE PARA CESANTIAS Y PRIMA', 'EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina     CHAR(5);
DECLARE V_Liq        CHAR(10);
DECLARE V_Desde_Nom  DATE;
DECLARE V_Hasta_Nom  DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Fecha      DATE;
DECLARE V_Fecha_Ini  DATE;
DECLARE V_Dias       NUMERIC(17,4);
DECLARE V_Basico     NUMERIC(17,4);
DECLARE V_Val_Minimo NUMERIC(17,4);
DECLARE V_Minimo     CHAR(10) = ''C01_SMLV'';
DECLARE V_Rubro1     CHAR(5) = ''SA027'';
DECLARE V_Rubro_Dias CHAR(5) = ''DI015'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  SELECT Valor
  FROM Constantesvalor
  WHERE Codconstante = :V_Minimo
        AND Ano = EXTRACT(YEAR FROM :V_Hasta_Nom)
  INTO V_Val_Minimo;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    SELECT First 1 Basico
    FROM Nom_Pila_Salarios(:Empleado, :V_Hasta_Nom, :V_Hasta_Nom)
    INTO V_Basico;
    IF (V_Basico = V_Val_Minimo) THEN
      Valor = V_Basico;
    ELSE
    BEGIN
      SELECT COUNT(Basico)
      FROM (SELECT DISTINCT Basico
            FROM Nom_Salarios(DATEADD(-3 MONTH TO :V_Fecha), :V_Fecha) S
            WHERE S.Codpersonal = :Empleado)
      INTO Valor;
      IF (Valor = 1) THEN
        Valor = V_Basico;
      ELSE
      BEGIN
        IF (V_Fecha_Ing > V_Desde_Nom) THEN
          V_Fecha_Ini = V_Fecha_Ing;
        ELSE
          V_Fecha_Ini = V_Desde_Nom;

        SELECT SUM(S.Adicion)
        FROM Planillas S
        JOIN Nominas N ON (S.Codnomina = N.Codigo)
        WHERE N.Fecha >= :V_Fecha_Ini
              AND N.Fecha <= :V_Fecha
              AND S.Codrubro = :V_Rubro_Dias
              AND S.Codpersonal = :Empleado
        INTO V_Dias;

        SELECT SUM(S.Adicion)
        FROM Planillas S
        JOIN Nominas N ON (S.Codnomina = N.Codigo)
        WHERE N.Fecha >= :V_Fecha_Ini
              AND N.Fecha <= :V_Fecha
              AND S.Codrubro = :V_Rubro1
              AND S.Codpersonal = :Empleado
        INTO Valor;

        IF (V_Dias > 0) THEN
          Valor = ROUND(Valor / V_Dias * 30);
        ELSE
          Valor = 0;
      END
    END
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('PROM_HE_RE', 'PROMEDIO HORAS EXTRAS Y RECARGOS', '--he_re
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Nom DATE;
DECLARE V_Fecha_Ini DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Dias NUMERIC(17,4);
DECLARE V_Rubro1 CHAR(5) = ''HE038'';
DECLARE V_Rubro_Dias CHAR(5) = ''DI015'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing

  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro = :V_Rubro_Dias
          AND S.Codpersonal = :Empleado
    INTO V_Dias;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro = :V_Rubro1
          AND S.Codpersonal = :Empleado
    INTO Valor;
    IF (V_Dias > 30) THEN
      IF (V_Dias > 0) THEN
        Valor = ROUND(Valor / V_Dias * 30);
      ELSE
        Valor = 0;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ANTE_SP',
          'DESCUENTO SOLIDARIDAD PENSIONAL REALIZADO EN PERIODOS ANTERIORES EN EL MISMO MES', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DT035'';
DECLARE V_Rubro2    CHAR(5) = ''VA035'';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;
  
  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado 
  DO
  BEGIN
    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom)
          AND EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom)
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado  AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('AUXTRANS', 'AUXILIO TRANSPORTE', 'EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Hasta DATE;
DECLARE V_Desde DATE;
DECLARE V_Fecha DATE;
DECLARE V_Sena NUMERIC(17,4);
DECLARE V_Minimo NUMERIC(17,4);
DECLARE V_Auxilio NUMERIC(17,4);
DECLARE V_Salario NUMERIC(17,4);
DECLARE V_Trans NUMERIC(17,4);

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde,
       V_Hasta,
       V_Fecha,
       V_Liq;

  SELECT Valor
  FROM Constantesvalor
  WHERE Codconstante = ''C01_SMLV''
        AND Ano = EXTRACT(YEAR FROM :V_Hasta)
  INTO V_Minimo;

  SELECT Valor
  FROM Constantesvalor
  WHERE Codconstante = ''C02_AUXT''
        AND Ano = EXTRACT(YEAR FROM :V_Hasta)
  INTO V_Auxilio;

  FOR SELECT Pl.Codpersonal,
                      P.Codcotizante,
                      MAX(S.Basico)
      FROM TMP_Empleados Pl
      JOIN Personal P ON (Pl.Codpersonal = P.Codigo)
      JOIN Nom_Salarios(:V_Desde, :V_Hasta) S ON (Pl.Codpersonal = S.Codpersonal)
      GROUP BY 1, 2
      INTO Empleado,
           V_Sena,
           V_Salario

  DO
  BEGIN

    V_Trans = NULL;
    SELECT Valor
    FROM Constantes_Personal Cp
    WHERE Cp.Codpersonal = :Empleado
          AND Cp.Codconstante = ''C02_AUXT''
          AND Cp.Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO V_Trans;

    IF (V_Trans IS NOT NULL) THEN
      Valor = V_Trans;
    ELSE
    BEGIN
      Valor = :V_Auxilio;

      IF (V_Sena IN (''12'', ''19'', ''23'')) THEN
        Valor = 0;
      ELSE


      IF (V_Salario >= (:V_Minimo * 2)) THEN
        Valor = 0;
    END
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIAS_IGE', 'DIAS IGE CONTINUOS', 'EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina     CHAR(5);

DECLARE V_Liq        CHAR(10);
DECLARE V_Sena       CHAR(2);
DECLARE V_Integral   CHAR(1);
DECLARE V_Fecha      DATE;
DECLARE V_Desde      DATE;
DECLARE V_Hasta      DATE;
DECLARE M_Hasta      DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Rubro1     CHAR(5) = ''LD071'';
DECLARE V_Rubro2     CHAR(5) = '''';
DECLARE V_Rubro_Dias CHAR(5) = ''DI003'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha, V_Desde, V_Hasta, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante,
                      P.Salario_Integral
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena,
           V_Integral
  DO
  BEGIN
    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado AND
          S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro1, :V_Rubro2) AND
          N.Codigo <> :V_Nomina AND
          N.Fecha < :V_Fecha AND
          N.Fecha >= :V_Desde AND
          S.Codpersonal = :Empleado
    INTO M_Hasta;
    M_Hasta = COALESCE(M_Hasta, V_Desde);
    M_Hasta = IIF(V_Fecha_Ing > M_Hasta, V_Fecha_Ing, M_Hasta);

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Desde >= :M_Hasta AND
          N.Fecha <= :V_Fecha AND
          S.Codrubro = :V_Rubro_Dias AND
          S.Codpersonal = :Empleado
    INTO Valor;

    IF (Valor IS NOT NULL) THEN
    BEGIN
      IF (V_Sena IN (''12'', ''19'') OR V_Integral = ''S'') THEN
        Valor = 0;
      SUSPEND;
    END
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_SENA_LE',
          'VALIDACION SI EMPLEADO ES APRENDIZ SENA ETAPA LECTIVA(1) - COTIZANTE', '--ES SENA (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_Sena      CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena
  DO
  BEGIN
    Valor = 0;
    IF (V_Sena=''12'') THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ANTE_SLN', 'DIAS EN EL MES DE QUINCENA ANTERIORES DE SLN', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DI001'';
DECLARE V_Rubro2    CHAR(5) = ''DI004'';
DECLARE V_Rubro3    CHAR(5) = ''DI012'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('QUINCENA', 'NUMERO DE QUNCENA (1 o 2)', '-- QUINCENA (1 o 2)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor INTEGER)
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Desde_Nom DATE;
DECLARE V_Fecha_Ret DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Coti CHAR(1);
DECLARE V_Dias INTEGER;
DECLARE V_Valor INTEGER;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;
  SELECT Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;
  SELECT Cotidiana
  FROM Liquidaciones
  WHERE Codigo = :V_Liq
  INTO :V_Coti;

  IF (V_Coti = ''S'') THEN
  BEGIN
    IF (V_Dias = 30) THEN
      V_Valor = 2;
    ELSE
    BEGIN
      IF (EXTRACT(DAY FROM V_Desde_Nom) > 15) THEN
        V_Valor = 2;
      ELSE
        V_Valor = 1;
    END
  END
  ELSE
    V_Valor = 2;
  Valor = V_Valor;
  FOR SELECT Codpersonal
      FROM TMP_Empleados
    INTO Empleado 
  DO
  BEGIN
    SELECT MAX(S.Hasta)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado AND
          S.Columna_Hasta = ''RETIRO''
    INTO V_Fecha_Ret;

    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado AND
          S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;

    IF (V_Fecha_Ret IS NOT NULL AND
        V_Fecha_Ret BETWEEN V_Desde_Nom AND V_Hasta_Nom AND
        V_Fecha_Ing < COALESCE(V_Fecha_Ret, V_Fecha_Ing)) THEN
      Valor = 2;
    SUSPEND;
    Valor = V_Valor;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('EXONERADA', 'EMPRESA EXONERADA (SI=1 o NO=0)', '-- EXONERADA (1-SI o 0-NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor INTEGER)
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Exo CHAR(15) = ''C44_EXO'';
DECLARE VTexto CHAR(100);

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  SELECT Texto
  FROM Constantesvalor
  WHERE Codconstante = :V_Exo AND
               Ano = EXTRACT(YEAR FROM :V_Desde_Nom)
  INTO Vtexto;

  Valor = 0;
  IF (TRIM(Vtexto) = ''S'') THEN
    Valor = 1;

  FOR SELECT Codpersonal
      FROM TMP_Empleados
    INTO Empleado   
  DO
  BEGIN
    SUSPEND;
  END
END', 'S', 'S');

END;

CREATE OR ALTER PROCEDURE X_Expresiones_2
AS
BEGIN
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('CUMPLE',
          'EMPLEADO CUMPLEA?OS EN EL PERIODO DE NOMINA (1) SI - (0) NO', '--CUMPLEA?OS (0=NO 1=SI) EN EL RANGO DE LA NOMINA
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Nacimiento DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT DISTINCT P.Codigo,
                      P.Nacimiento
      FROM Nom_Salarios(:V_Desde_Nom, :V_Hasta_Nom) S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      WHERE P.Codesqnomina IN (SELECT DISTINCT Codesqnomina
                               FROM Gruponomina
                               WHERE Codliquidacion = :V_Liq)
      INTO Empleado,
           V_Nacimiento
  DO
  BEGIN
    Valor = 0;
    IF (EXTRACT(MONTH FROM V_Nacimiento) = EXTRACT(MONTH FROM V_Desde_Nom) AND
        EXTRACT(DAY FROM V_Nacimiento) BETWEEN EXTRACT(DAY FROM V_Desde_Nom) AND EXTRACT(DAY FROM V_Hasta_Nom)) THEN
      Valor = 1;
    SUSPEND;
  END
END', 'N', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIAS_CES', 'DIAS PARA LIQUIDACION CESANTIAS', 'EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);

DECLARE V_Liq CHAR(10);
DECLARE V_Sena CHAR(2);
DECLARE V_Integral CHAR(1);
DECLARE V_Fecha DATE;
DECLARE V_Desde DATE;
DECLARE V_Hasta DATE;
DECLARE M_Hasta DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Rubro1 CHAR(5) = ''LD071'';
DECLARE V_Rubro2 CHAR(5) = ''LD076'';
DECLARE V_Rubro_Dias1 CHAR(5) = ''DI015'';
DECLARE V_Rubro_Dias2 CHAR(5) = ''SI051'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha,
       V_Desde,
       V_Hasta,
       V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante,
                      P.Salario_Integral
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena,
           V_Integral
  DO
  BEGIN
    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado
          AND S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha < :V_Fecha
          AND N.Fecha >= :V_Desde
          AND S.Codpersonal = :Empleado
    INTO M_Hasta;

    M_Hasta = IIF(M_Hasta IS NULL, V_Desde, M_Hasta + 1);

    M_Hasta = IIF(V_Fecha_Ing > M_Hasta, V_Fecha_Ing, M_Hasta);

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :M_Hasta
          AND N.Fecha <= :V_Fecha
          AND S.Codrubro IN (:V_Rubro_Dias1, :V_Rubro_Dias2)
          AND S.Codpersonal = :Empleado
    INTO Valor;

    IF (Valor IS NOT NULL) THEN
    BEGIN
      IF (V_Sena IN (''12'', ''19'', ''23'') OR V_Integral = ''S'') THEN
        Valor = 0;
      SUSPEND;
    END
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ANTE_OS',
          'VALORES OTROS CONCEPTOS SALARIABLES ANTERIORES EN EL MISMO MES', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''HE038'';
DECLARE V_Rubro2    CHAR(5) = ''DV997'';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_MEDT',
          'VALIDA SI EL EMPLEADO ES MEDIO TIEMPO DEVUELVE (1) - COTIZANTE TIPO 51', '--ES EMPLEADO MEDIO TIEMPO (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_Sena      CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena
  DO
  BEGIN
    Valor = 0;
    IF (V_Sena IN (''51'')) THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIAS_INT', 'DIAS PARA LIQUIDACION INTERESES A LAS CESANTIAS', 'EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina      CHAR(5);

DECLARE V_Liq         CHAR(10);
DECLARE V_Sena        CHAR(2);
DECLARE V_Integral    CHAR(1);
DECLARE V_Fecha       DATE;
DECLARE V_Desde       DATE;
DECLARE V_Hasta       DATE;
DECLARE M_Hasta       DATE;
DECLARE V_Fecha_Ing   DATE;
DECLARE V_Rubro1      CHAR(5) = ''LD072'';
DECLARE V_Rubro2      CHAR(5) = ''LD077'';
DECLARE V_Rubro_Dias1 CHAR(5) = ''DI015'';
DECLARE V_Rubro_Dias2 CHAR(5) = ''SI052'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha,
       V_Desde,
       V_Hasta,
       V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante,
                      P.Salario_Integral
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Sena,
           V_Integral
  DO
  BEGIN
    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado
          AND S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha < :V_Fecha
          AND N.Fecha >= :V_Desde
          AND S.Codpersonal = :Empleado
    INTO M_Hasta;

    M_Hasta = IIF(M_Hasta IS NULL, V_Desde, M_Hasta + 1);

    M_Hasta = IIF(V_Fecha_Ing > M_Hasta, V_Fecha_Ing, M_Hasta);

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :M_Hasta
          AND N.Fecha <= :V_Fecha
          AND S.Codrubro IN (:V_Rubro_Dias1, :V_Rubro_Dias2)
          AND S.Codpersonal = :Empleado
    INTO Valor;

    IF (Valor IS NOT NULL) THEN
    BEGIN
      IF (V_Sena IN (''12'', ''19'', ''23'') OR V_Integral = ''S'') THEN
        Valor = 0;
      SUSPEND;
    END
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ANTE_DIAS', 'DIAS OTROS PARAFISCALES QUINCENA ANTERIOR', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS013'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ANTE_DIASE', 'DIAS OTROS EPS QUINCENA ANTERIOR PARA APORTE', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS012'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_ARL', 'EMPLEADO TIPO 23 QUE SOLO APORTA A ARL', '--ES TIPO 23 APORTA SOLO ARL (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_23        CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
                      P.Codcotizante
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_23
  DO
  BEGIN
    Valor = 0;
    IF (V_23 =''23'') THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('SAL_BASICO', 'SALARIO BASICO PROMEDIO', '--BASICO PRORRATEADO
EXECUTE BLOCK
RETURNS (Empleado CHAR(15), Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);        DECLARE V_Liq       CHAR(10);
DECLARE V_Desde_Nom DATE;           DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;           DECLARE V_Fecha_Ret DATE;
DECLARE V_Dias_Nom  NUMERIC(17,4);  DECLARE V_Dias1     NUMERIC(17,4);
DECLARE V_Basico2   NUMERIC(17,4);  DECLARE V_Dias2     NUMERIC(17,4);
DECLARE V_Cotidiana CHAR(1);        DECLARE V_Cotizante CHAR(2);
BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'') FROM Rdb$Database INTO V_Nomina;   
  SELECT N.Desde, N.Hasta, N.Codliquidacion, N.Dias, L.Cotidiana FROM Nominas N  JOIN Liquidaciones L ON (N.Codliquidacion = L.Codigo)
  WHERE N.Codigo = :V_Nomina  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias_Nom, V_Cotidiana;
  FOR SELECT T.Codpersonal, P.CodCotizante
      FROM TMP_Empleados T
      JOIN personal P ON (T.codpersonal=P.codigo)
    INTO Empleado, V_Cotizante
  DO
  BEGIN
    IF (V_Cotizante=''23'') THEN
    BEGIN
      Valor=0;
      SUSPEND;
    END
    ELSE
    BEGIN
    SELECT MAX(S.Desde) FROM Salarios S  
    WHERE S.Codpersonal = :Empleado AND S.Desde <= :V_Hasta_Nom AND S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;
    V_Fecha_Ret = NULL;
    SELECT FIRST 1 S.Hasta FROM Salarios S WHERE Codpersonal = :Empleado
          AND S.Columna_Hasta = ''RETIRO''   AND S.Hasta >= :V_Desde_Nom   AND S.Hasta <= :V_Hasta_Nom   AND S.HASTA >= :V_FECHA_ING
    ORDER BY Hasta DESC
    INTO V_Fecha_Ret;
    V_Fecha_Ret = COALESCE(V_Fecha_Ret, V_Hasta_Nom);
    IF (V_Cotidiana = ''N'') THEN
    BEGIN
      SELECT FIRST 1 Basico FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ret, :V_Fecha_Ret) INTO Valor;
    END
    ELSE
    BEGIN
      IF (V_Fecha_Ing > V_Desde_Nom) THEN
        SELECT FIRST 1 Basico FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ing, :V_Fecha_Ing)
        INTO Valor;
      ELSE
        SELECT FIRST 1 Basico FROM Nom_Pila_Salarios(:Empleado, :V_Desde_Nom, :V_Desde_Nom)
        INTO Valor;
      IF (V_Fecha_Ret < V_Hasta_Nom) THEN
        SELECT FIRST 1 Basico FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ret, :V_Fecha_Ret)
        INTO V_Basico2;
      ELSE
        SELECT FIRST 1 Basico FROM Nom_Pila_Salarios(:Empleado, :V_Hasta_Nom, :V_Hasta_Nom)
        INTO V_Basico2;
      IF (V_Dias_Nom > 0) THEN
      BEGIN
        IF (Valor <> V_Basico2) THEN
        BEGIN
          SELECT FIRST 1 Dias FROM Nom_Pila_Multisalarios(:Empleado, :V_Desde_Nom, :V_Hasta_Nom)
          ORDER BY Desde
          INTO V_Dias1;
          IF (V_Dias1 > V_Dias_Nom) THEN
            V_Dias1 = V_Dias1 - V_Dias_Nom;
          V_Dias2 = V_Dias_Nom - V_Dias1;
          Valor = Valor / V_Dias_Nom * V_Dias1;
          V_Basico2 = V_Basico2 / V_Dias_Nom * V_Dias2;
          Valor = Valor + V_Basico2;
        END
      END
      ELSE
        Valor = COALESCE(V_Basico2, 0);
    END
    SUSPEND;
  END
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIAS_YA_LQ', 'DIAS YA LIQUIDADOS EN EL PERIODO', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Cod_Liq   CHAR(10) = ''LQ9DEF'';
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DI015'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';


BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom,
       V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT Codpersonal
    FROM TMP_Empleados
    INTO Empleado   
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.fecha >= :V_Desde_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado
          AND N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    IF (:V_Liq = :V_Cod_Liq) THEN
      Valor = 0;
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('IGE_ANT', 'INCAPACIDAD DESDE EL PERIODO ANTERIOR', '--IEG Anterior
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Inicio    DATE;
DECLARE V_Fin       DATE;
DECLARE V_Inicio1   DATE;
DECLARE V_Fin1      DATE;
DECLARE V_Fin_Ant   DATE;
DECLARE Valor_Ant   NUMERIC(17,4);
DECLARE V_Cant      INTEGER;
DECLARE V_Rubro1    CHAR(5) = ''DI003'';
DECLARE V_Rubro2    CHAR(5) = ''DI009'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  Valor = 0;
  FOR SELECT Codpersonal
      FROM TMP_Empleados
    INTO Empleado   
  DO
  BEGIN
    SELECT FIRST 1 Inicio, Fin
    FROM Generalidades
    WHERE Codpersonal = :Empleado AND
          Codrubro IN (:V_Rubro1, :V_Rubro2) AND
          ((Inicio <= :V_Desde_Nom AND
          Fin >= :V_Desde_Nom AND
          Fin <= :V_Hasta_Nom) OR (Inicio <= :V_Desde_Nom AND
          Fin >= :V_Hasta_Nom) OR (Inicio >= :V_Desde_Nom AND
          Inicio <= :V_Hasta_Nom AND
          Fin >= :V_Desde_Nom AND
          Fin <= :V_Hasta_Nom) OR (Inicio >= :V_Desde_Nom AND
          Inicio <= :V_Hasta_Nom AND
          Fin >= :V_Hasta_Nom))
    ORDER BY Inicio
    INTO V_Inicio, V_Fin;

    IF (V_Inicio < :V_Desde_Nom) THEN
      Valor = :V_Desde_Nom - V_Inicio;
    ELSE
    IF (V_Inicio = :V_Desde_Nom) THEN
    BEGIN
      SELECT FIRST 1 Inicio, Fin
      FROM Generalidades
      WHERE Codpersonal = :Empleado AND
            Codrubro IN (:V_Rubro1, :V_Rubro2) AND
            Fin = :V_Inicio - 1
      INTO V_Inicio1, V_Fin1;

      IF (V_Inicio IS NOT NULL) THEN
        Valor = V_Fin1 - V_Inicio1;
    END
    IF (Valor > 0) THEN
      SUSPEND;
    Valor = 0;
    V_Inicio = NULL;
    V_Fin = NULL;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_EPS_PE',
          'VALIDA SI LA CONSTANTE C30_EPS_PE TIENE VALOR EN EL EMPLEADO (1) SI ES SI', '--C30_EPS_PE TIENE VALOR (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Constante CHAR(15) = ''C30_EPS_PE'';
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Integral CHAR(1);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;
  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT Codpersonal
      FROM TMP_Empleados
    INTO Empleado   
  DO
  BEGIN
    Valor = 0;
    SELECT COUNT(*)
    FROM Constantes_Personal
    WHERE Codpersonal = :Empleado AND
          Codconstante = :V_Constante AND
          Ano = EXTRACT(YEAR FROM :V_Desde_Nom)
    INTO Valor;

    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('ES_EPS_EM',
          'VALIDA SI LA CONSTANTE C31_EPS_EM TIENE VALOR EN EL EMPLEADO (1) SI ES SI', '--SALARIO INTEGRAL (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Constante CHAR(15) = ''C31_EPS_EM'';
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Integral CHAR(1);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;
  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT Codpersonal
      FROM TMP_Empleados
    INTO Empleado   
  DO
  BEGIN
    Valor = 0;
    SELECT COUNT(*)
    FROM Constantes_Personal
    WHERE Codpersonal = :Empleado AND
          Codconstante = :V_Constante AND
          Ano = EXTRACT(YEAR FROM :V_Desde_Nom)
    INTO Valor;

    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('DIA_MES',
          'DEVUELVE 1 PARA MESES CON 31, 0 PARA MESES CON 30 Y (-1 ? -2) FEBRERO', '-- PARA PAGAR POR DIAS EN MESES SI ES 30(0) MESES 31(1) ? FEBRERO (-1 o 

-2)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor INTEGER)
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha DATE;
DECLARE V_Dia INTEGER;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha
  FROM Nominas
  WHERE Codigo = :V_Nomina

  INTO V_Fecha;
  V_Dia = CASE EXTRACT(MONTH FROM V_Fecha)
            WHEN 1 THEN 31
            WHEN 2 THEN IIF(TRUNC(EXTRACT(YEAR FROM V_Fecha) / 4) * 4 = 

EXTRACT(YEAR FROM V_Fecha), 29, 28)
            WHEN 3 THEN 31
            WHEN 4 THEN 30
            WHEN 5 THEN 31
            WHEN 6 THEN 31
            WHEN 7 THEN 31
            WHEN 8 THEN 31
            WHEN 9 THEN 30
            WHEN 10 THEN 31
            WHEN 11 THEN 30
            WHEN 12 THEN 31
          END;

  IF (EXTRACT(DAY FROM V_Fecha) = V_Dia) THEN
    Valor = CASE EXTRACT(MONTH FROM V_Fecha)
              WHEN 1 THEN 1
              WHEN 2 THEN IIF(TRUNC(EXTRACT(YEAR FROM V_Fecha) / 4) * 4 

= EXTRACT(YEAR FROM V_Fecha), -1, -2)
              WHEN 3 THEN 1
              WHEN 4 THEN 0
              WHEN 5 THEN 1
              WHEN 6 THEN 0
              WHEN 7 THEN 1
              WHEN 8 THEN 1
              WHEN 9 THEN 0
              WHEN 10 THEN 1
              WHEN 11 THEN 0
              WHEN 12 THEN 1
            END;
  ELSE
    Valor = 0;

  FOR SELECT Codpersonal
      FROM Tmp_Empleados
      INTO :Empleado
  DO
    SUSPEND;
END', 'N', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('TOTA_HE_RE', 'TOTAL HORAS EXTRAS Y RECARGOS', '--Total he_re
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Nom DATE;
DECLARE V_Fecha_Ini DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Dias NUMERIC(17,4);
DECLARE V_Rubro1 CHAR(5) = ''HE038'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro = :V_Rubro1
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');
  INSERT INTO Expresiones (Codigo, Nombre, Sql_Bloque, Activo, Nativo)
  VALUES ('TOTA_DIAS', 'TOTAL DIAS PARA PROMEDIO HORAS EXTRAS Y RECARGOS', '--Total Dias
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Liq CHAR(10);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Nom DATE;
DECLARE V_Fecha_Ini DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Dias NUMERIC(17,4);
DECLARE V_Rubro1 CHAR(5) = ''DI015'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro = :V_Rubro1
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');

END;


CREATE OR ALTER PROCEDURE Pz_Valor_Clase_Nom (
    Empleado_ VARCHAR(15),
    Nomina_   VARCHAR(5),
    Clase_    VARCHAR(30))
RETURNS (
    Empleado VARCHAR(15),
    Clase    VARCHAR(30),
    Valor    DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nomina VARCHAR(5);
DECLARE VARIABLE V_Valor  INTEGER;
BEGIN
  FOR SELECT DISTINCT Codpersonal
      FROM Planillas
      WHERE Codnomina = :Nomina_
            AND Codpersonal LIKE TRIM(:Empleado_) || '%'
      INTO Empleado
  DO
  BEGIN

    FOR SELECT Codigo
        FROM Clases
        WHERE Codigo LIKE TRIM(:Clase_) || '%'
        INTO Clase
    DO
    BEGIN
      SELECT SUM(P.Adicion + P.Deduccion)
      FROM Planillas P
      JOIN Clases_Rubros R ON (P.Codrubro = R.Codrubro)
      WHERE R.Codclase = :Clase
            AND R.Activo = 'S'
            AND P.Codnomina = :Nomina_
            AND P.Codpersonal = :Empleado
      INTO Valor;
      Valor = COALESCE(:Valor, 0);
      SUSPEND;
    END
  END
END
/************************************************************/
/**** MODULO GESTION                                     ****/
/************************************************************/


/* FECHA: 13ABR2023 */
SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CONTEO_FISICO (
    FECHA_CORTE_ DATE,
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    USUARIO_ VARCHAR(10))
AS
BEGIN
  EXIT;
END^





CREATE OR ALTER PROCEDURE PZ_SEL_CONTEO_BASE (
    FECHA_CORTE_ DATE,
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    BODEGA VARCHAR(5),
    REFERENCIA VARCHAR(20),
    CATEGORIA VARCHAR(15),
    LOTE VARCHAR(20),
    SALDO DOUBLE PRECISION,
    CONTEO DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_SEL_CONTEO_FISICO (
    FECHA_CORTE_ DATE,
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    REFERENCIA_ VARCHAR(20),
    BODEGA_ VARCHAR(5))
RETURNS (
    REFERENCIA VARCHAR(20),
    NOMBRE_REFERENCIA VARCHAR(80),
    BODEGA VARCHAR(5),
    CATEGORIA VARCHAR(15),
    LOTE VARCHAR(20),
    LINEA VARCHAR(5),
    NOMBRE_LINEA VARCHAR(80),
    UBICACION VARCHAR(20),
    SALDO DOUBLE PRECISION,
    PONDERADO DOUBLE PRECISION,
    VALOR DOUBLE PRECISION,
    CONTEO DOUBLE PRECISION,
    VALOR_CONTEO DOUBLE PRECISION,
    DIFERENCIA DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^


SET TERM ; ^


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/


SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CONTEO_FISICO (
    FECHA_CORTE_ DATE,
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    USUARIO_ VARCHAR(10))
AS
DECLARE VARIABLE V_Nit          VARCHAR(15);
DECLARE VARIABLE V_Referencia   VARCHAR(20);
DECLARE VARIABLE V_Bodega       VARCHAR(5);
DECLARE VARIABLE V_Categoria    VARCHAR(15);
DECLARE VARIABLE V_Lote         VARCHAR(20);
DECLARE VARIABLE V_Ponderado    DOUBLE PRECISION;
DECLARE VARIABLE V_Diferencia   DOUBLE PRECISION;
DECLARE VARIABLE V_Renglon      INTEGER;
DECLARE VARIABLE V_Tipo_Entrada VARCHAR(5) = 'AE1';
DECLARE VARIABLE V_Tipo_Salida  VARCHAR(5) = 'AS1';
DECLARE VARIABLE V_Prefijo      VARCHAR(5) = 'AJUST';
DECLARE VARIABLE V_Registro     INTEGER;
BEGIN

 /* Insertar prefijo tipo AJUST para evitar primary_key */
 SELECT COUNT(1)
 FROM Prefijos
 WHERE Codigo = 'AJUST'
 INTO V_Registro;

 IF (V_Registro = 0) THEN
  INSERT INTO Prefijos (Codigo,Nombre)
  VALUES ('AJUST','AJUSTE DE INVENTARIO');

 /* Insertar lista de precios NA */
 SELECT COUNT(1)
 FROM Listas
 WHERE Codigo = 'NA'
 INTO V_Registro;

 IF (V_Registro = 0) THEN
  INSERT INTO Listas (Codigo,Nombre)
  VALUES ('NA','NO APLICA');

 /* Seleccionar el nit del tercero empresa para asignar como tercero en los comprobantes */
 SELECT TRIM(Codigo)
 FROM Terceros
 WHERE Datos_Empresa = 'S'
 INTO V_Nit;

 /* Insertar en comprobantes el doc AE1 */
 INSERT INTO Comprobantes (Tipo,Prefijo,Numero,Codescenario,Fecha,Vence,Codlista,Nota,Bloqueado,Codtercero,Codvendedor,Codusuario)
 VALUES (TRIM(:V_Tipo_Entrada),TRIM(:V_Prefijo),TRIM(:Numero_),'NA',:Fecha_Corte_,:Fecha_Corte_,'NA','AJUSTE INVENTARIO CONTEO FISICO','R',TRIM(:V_Nit),'NULO',TRIM(:Usuario_));

 /* Insertar en comprobantes el doc AS1 */
 INSERT INTO Comprobantes (Tipo,Prefijo,Numero,Codescenario,Fecha,Vence,Codlista,Nota,Bloqueado,Codtercero,Codvendedor,Codusuario)
 VALUES (TRIM(:V_Tipo_Salida),TRIM(:V_Prefijo),TRIM(:Numero_),'NA',:Fecha_Corte_,:Fecha_Corte_,'NA','AJUSTE INVENTARIO CONTEO FISICO','R',TRIM(:V_Nit),'NULO',TRIM(:Usuario_));

 /* Referencias a insertar en los ajustes */
 FOR SELECT Referencia,
            Bodega,
            Categoria,
            Lote,
            Ponderado,
            Diferencia
     FROM Pz_Sel_Conteo_Fisico(:Fecha_Corte_,:Tipo_,:Prefijo_,:Numero_,'%','%')
     WHERE Diferencia <> 0
     INTO V_Referencia,
          V_Bodega,
          V_Categoria,
          V_Lote,
          V_Ponderado,
          V_Diferencia
 DO
 BEGIN
  V_Renglon = (SELECT (GEN_ID(Gen_Tr_Inventario,1))
               FROM Rdb$Database);

  /* Validacion de la diferencia para saber el tipo de inserccion a realizar si es de entrada o salida
       Si el saldo es negativo = entrada
       Si el saldo es positivo = salida */
  IF (V_Diferencia < 0) THEN
  BEGIN
   V_Diferencia = V_Diferencia * -1;
   INSERT INTO Tr_Inventario (Tipo,Prefijo,Numero,Renglon,Codbodega,Codcentro,Codreferencia,Entrada,Bruto,Unitario,Codusuario,Nota)
   VALUES (TRIM(:V_Tipo_Entrada),TRIM(:V_Prefijo),TRIM(:Numero_),:V_Renglon,TRIM(:V_Bodega),'NA',TRIM(:V_Referencia),:V_Diferencia,:V_Ponderado,:V_Ponderado,:Usuario_,
           'AJUSTE AUTOMATICO CONTEO FISICO');
  END
  ELSE
  BEGIN
   INSERT INTO Tr_Inventario (Tipo,Prefijo,Numero,Renglon,Codbodega,Codcentro,Codreferencia,Salida,Bruto,Unitario,Codusuario,Nota)
   VALUES (TRIM(:V_Tipo_Salida),TRIM(:V_Prefijo),TRIM(:Numero_),:V_Renglon,TRIM(:V_Bodega),'NA',TRIM(:V_Referencia),:V_Diferencia,:V_Ponderado,:V_Ponderado,:Usuario_,'AJUSTE AUTOMATICO CONTEO FISICO');
  END
 END
 /* Ejecuccion para organizar validacion en tr_inventario */
 EXECUTE PROCEDURE Fx_Repara_Validacion('MEKANO_2012');

 /* Terminar documentos */
 EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_Entrada,:V_Prefijo,:Numero_);
 EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_Entrada,:V_Prefijo,:Numero_);
 EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_Salida,:V_Prefijo,:Numero_);
 EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_Salida,:V_Prefijo,:Numero_);
END^


CREATE OR ALTER PROCEDURE Pz_Sel_Conteo_Base (
    Fecha_Corte_ DATE,
    Tipo_        CHAR(5),
    Prefijo_     CHAR(5),
    Numero_      CHAR(10))
RETURNS (
    Bodega     VARCHAR(5),
    Referencia VARCHAR(20),
    Categoria  VARCHAR(15),
    Lote       VARCHAR(20),
    Saldo      DOUBLE PRECISION,
    Conteo     DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Afecta  CHAR(1);
DECLARE VARIABLE V_Saldos  CHAR(1);
BEGIN
  Conteo = 0;
  FOR SELECT Tipo, Prefijo, Numero, Codbodega, Codreferencia, Codcategoria,
             Codlote, SUM(Entrada - Salida)
      FROM Tr_Inventario
      GROUP BY 1, 2, 3, 4, 5, 6, 7
      INTO :V_Tipo, :V_Prefijo, :V_Numero, :Bodega, :Referencia, :Categoria,
           :Lote, :Saldo
  DO
  BEGIN
    Saldo = COALESCE(Saldo, 0);
    -- Valida fecha
    V_Fecha = NULL;
    SELECT Fecha
    FROM Comprobantes
    WHERE Tipo = :V_Tipo
          AND Prefijo = :V_Prefijo
          AND Numero = :V_Numero
    INTO V_Fecha;

    IF (V_Fecha <= :Fecha_Corte_) THEN
    BEGIN
      --Valida documento
      SELECT Afecta
      FROM Documentos
      WHERE Codigo = :V_Tipo
      INTO V_Afecta;

      IF (:V_Afecta = 'S') THEN
      BEGIN
        --Valida saldos
        SELECT Saldos
        FROM Referencias
        WHERE Codigo = :Referencia
        INTO V_Saldos;
        IF (V_Saldos = 'S') THEN
          SUSPEND;
      END
    END
  END

  FOR SELECT Tipo, Prefijo, Numero, Codbodega, Codreferencia, Codcategoria,
             Codlote, SUM(Entrada - Salida)
      FROM Reg_Produccion
      GROUP BY 1, 2, 3, 4, 5, 6, 7
      INTO :V_Tipo, :V_Prefijo, :V_Numero, :Bodega, :Referencia, :Categoria,
           :Lote, :Saldo
  DO
  BEGIN
    Saldo = COALESCE(Saldo, 0);
    -- Valida fecha
    V_Fecha = NULL;
    SELECT Fecha
    FROM Comprobantes
    WHERE Tipo = :V_Tipo
          AND Prefijo = :V_Prefijo
          AND Numero = :V_Numero
    INTO V_Fecha;

    IF (V_Fecha <= :Fecha_Corte_) THEN
    BEGIN
      --Valida documento
      SELECT Afecta
      FROM Documentos
      WHERE Codigo = :V_Tipo
      INTO V_Afecta;

      IF (:V_Afecta = 'S') THEN
      BEGIN
        --Valida saldos
        SELECT Saldos
        FROM Referencias
        WHERE Codigo = :Referencia
        INTO V_Saldos;
        IF (V_Saldos = 'S') THEN
          SUSPEND;
      END
    END
  END

  Bodega = NULL;
  Referencia = NULL;
  Categoria = NULL;
  Lote = NULL;
  Conteo = 0;
  Saldo = 0;
  FOR SELECT Codbodega, Codreferencia, Codcategoria, Codlote, SUM(Entrada)
      FROM Tr_Inventario
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
      GROUP BY 1, 2, 3, 4
      INTO :Bodega, :Referencia, :Categoria, :Lote, :Conteo
  DO
  BEGIN

    Conteo = COALESCE(Conteo, 0);
    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_SEL_CONTEO_FISICO (
    FECHA_CORTE_ DATE,
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    REFERENCIA_ VARCHAR(20),
    BODEGA_ VARCHAR(5))
RETURNS (
    REFERENCIA VARCHAR(20),
    NOMBRE_REFERENCIA VARCHAR(80),
    BODEGA VARCHAR(5),
    CATEGORIA VARCHAR(15),
    LOTE VARCHAR(20),
    LINEA VARCHAR(5),
    NOMBRE_LINEA VARCHAR(80),
    UBICACION VARCHAR(20),
    SALDO DOUBLE PRECISION,
    PONDERADO DOUBLE PRECISION,
    VALOR DOUBLE PRECISION,
    CONTEO DOUBLE PRECISION,
    VALOR_CONTEO DOUBLE PRECISION,
    DIFERENCIA DOUBLE PRECISION)
AS
BEGIN

  FOR SELECT Bodega,
             Referencia,
             Categoria,
             Lote,
             SUM(Saldo),
             SUM(Conteo)
      FROM Pz_Sel_Conteo_Base(:Fecha_Corte_, :Tipo_, :Prefijo_, :Numero_)
      WHERE (TRIM(Referencia) LIKE :Referencia_)
            AND (TRIM(Bodega) LIKE :Bodega_)
      GROUP BY 1, 2, 3, 4
      INTO :Bodega,
           :Referencia,
           :Categoria,
           :Lote,
           :Saldo,
           :Conteo
  DO
  BEGIN
    Categoria = COALESCE(Categoria, 'NA');
    Lote = COALESCE(Lote, 'NA');
    Saldo = COALESCE(Saldo, 0);

    /* Nombre referencia */
    SELECT TRIM(Nombre_Referencia)
    FROM Fn_Nombre_Referencia(:Referencia)
    INTO Nombre_Referencia;

    /* Linea y ubicacion */
    SELECT TRIM(Codlinea),
           TRIM(Ubicacion)
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Linea,
         Ubicacion;

    /* Nombre linea */
    SELECT TRIM(Nombre)
    FROM Lineas
    WHERE Codigo = :Linea
    INTO Nombre_Linea;

    /* Ponderado */
    SELECT Ponderado
    FROM Fn_Ponderado(:Referencia, :Fecha_Corte_)
    INTO Ponderado;
    Ponderado = COALESCE(Ponderado, 0);

    /* Calculos */
    Valor = Saldo * Ponderado;
    Valor_Conteo = Conteo * Ponderado;
    Diferencia = Saldo - Conteo;
    SUSPEND;
  END

END^

SET TERM ; ^

/* CONTEO INVENTARIO FISICO PARCIAL */
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Sel_Conteo_Fisico_P (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10))
RETURNS (
    Fecha_Corte       DATE,
    Referencia        VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Bodega            VARCHAR(5),
    Categoria         VARCHAR(15),
    Lote              VARCHAR(20),
    Linea             VARCHAR(5),
    Nombre_Linea      VARCHAR(80),
    Ubicacion         VARCHAR(20),
    Saldo             DOUBLE PRECISION,
    Ponderado         DOUBLE PRECISION,
    Valor             DOUBLE PRECISION,
    Conteo            DOUBLE PRECISION,
    Valor_Conteo      DOUBLE PRECISION,
    Diferencia        DOUBLE PRECISION)
AS
BEGIN
  --Fecha Corte
  SELECT Fecha
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO :Fecha_Corte;

  FOR SELECT Codbodega,
             Codreferencia,
             Codcategoria,
             Codlote,
             SUM(Entrada)
      FROM Tr_Inventario
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
      GROUP BY 1, 2, 3, 4
      INTO :Bodega,
           :Referencia,
           :Categoria,
           :Lote,
           :Conteo
  DO
  BEGIN
    Conteo = COALESCE(Conteo, 0);

    SELECT SUM(Entrada - Salida)
    FROM Tr_Inventario T
    JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
    JOIN Documentos D ON (T.Tipo = D.Codigo)
    WHERE C.Fecha <= :Fecha_Corte
          AND T.Codreferencia = :Referencia
          AND T.Codbodega = :Bodega
          AND D.Afecta = 'S'
          AND T.Codcategoria = :Categoria
          AND T.Codlote = :Lote

    INTO :Saldo;

    Categoria = COALESCE(Categoria, 'NA');
    Lote = COALESCE(Lote, 'NA');
    Saldo = COALESCE(Saldo, 0);

    /* Nombre referencia */
    SELECT TRIM(Nombre_Referencia)
    FROM Fn_Nombre_Referencia(:Referencia)
    INTO Nombre_Referencia;

    /* Linea y ubicacion */
    SELECT TRIM(Codlinea),
           TRIM(Ubicacion)
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Linea,
         Ubicacion;

    /* Nombre linea */
    SELECT TRIM(Nombre)
    FROM Lineas
    WHERE Codigo = :Linea
    INTO Nombre_Linea;

    /* Ponderado */
    SELECT Ponderado
    FROM Fn_Ponderado(:Referencia, :Fecha_Corte)
    INTO Ponderado;
    Ponderado = COALESCE(Ponderado, 0);

    /* Calculos */
    Valor = Saldo * Ponderado;
    Valor_Conteo = Conteo * Ponderado;
    Diferencia = Saldo - Conteo;

    SUSPEND;
  END
END^

SET TERM ; ^

COMMENT ON PROCEDURE PZ_SEL_CONTEO_FISICO_P IS
'Conteo fisico parcial
Solo tiene en cuenta las referencias digitadas en el SF1';


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('INVG111002', 'DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO PARCIAL', 'Comparar saldos del sistema con el conteo fisico del inventario solo para las referencias digitadas en el documento SF1 con el que se cargo el inventario fisico.', 'SELECT Fecha_Corte, Referencia, Nombre_Referencia, Bodega, Categoria, Lote,
       Linea, Nombre_Linea, Ubicacion, Saldo, Ponderado, Valor, Conteo,
       Valor_Conteo, Diferencia
FROM Pz_Sel_Conteo_Fisico_P(:A_Tipo, :B_Prefijo, :C_Numero)', 'S', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;

/*FECHA. 19 DE MAYO DE 2023*/

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Comprobantes (
    Tipo_ VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_ VARCHAR(10),
    Emisor_ VARCHAR(15),
    Receptor_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(20),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_Docu_Ref VARCHAR(20),
    Com_Total DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu VARCHAR(20);
DECLARE VARIABLE V_Caiu INTEGER;
DECLARE VARIABLE V_Dsi INTEGER;
DECLARE VARIABLE V_Mand INTEGER;
DECLARE VARIABLE V_Cod_Vendedor VARCHAR(15);
DECLARE VARIABLE V_Tesoreria VARCHAR(10);
DECLARE VARIABLE V_Formapago VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe VARCHAR(5);
DECLARE VARIABLE V_Codbanco VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe VARCHAR(5);
DECLARE VARIABLE V_Naturaleza CHAR(1);
DECLARE VARIABLE V_Codsociedad VARCHAR(5);
DECLARE VARIABLE V_Email_Ter VARCHAR(50);
DECLARE VARIABLE V_Codsucursal VARCHAR(15);
DECLARE VARIABLE V_Email_Suc VARCHAR(50);
DECLARE VARIABLE V_Email_Otro VARCHAR(100);
DECLARE VARIABLE V_Nota VARCHAR(80);
DECLARE VARIABLE V_Meganota VARCHAR(4000);
DECLARE VARIABLE V_Copag INTEGER;
DECLARE VARIABLE V_Copago DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos DOUBLE PRECISION;
DECLARE VARIABLE V_Pais VARCHAR(5);
DECLARE VARIABLE V_Municipio VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe CHAR(1);
DECLARE VARIABLE V_Concepto_Nc VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo VARCHAR(5);
DECLARE VARIABLE V_Stot_Red DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det DOUBLE PRECISION;
DECLARE VARIABLE V_Siif VARCHAR(80);
DECLARE VARIABLE V_Fecre INTEGER;
DECLARE VARIABLE V_Dscor VARCHAR(50);
DECLARE VARIABLE V_Postal_Code VARCHAR(6);
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  -- Si es NC o ND se toman datos del doc referencia
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tipo_Ref,
       V_Prefijo_Ref,
       V_Numero_Ref;

  V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
  V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
  V_Numero_Ref = COALESCE(V_Numero_Ref, '');

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Comprobantes
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Fecha_Ref;
  -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

  SELECT Cufe,
         EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Facturas
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Cufe_Ref,
       V_Fecha_Fac;
  V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
  V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tipo_Ref
  INTO V_Docu_Ref;
  V_Docu_Ref = COALESCE(V_Docu_Ref, '');

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       "noov:Nvfac_fech",
       "noov:Nvfac_venc",
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvres_nume";
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');

  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvfac_cdet";

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO "noov:Nvcli_ncon";

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  "noov:Nvcli_ncon" = COALESCE("noov:Nvcli_ncon", '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO "noov:Nvven_nomb";

  "noov:Nvven_nomb" = COALESCE("noov:Nvven_nomb", '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       V_Codsociedad,
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO "noov:Nvcli_regi",
       "noov:Nvcli_fisc";

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO "noov:Nvcli_pais";

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO "noov:Nvcli_depa";

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:"noov:Nvcli_pais") = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO "noov:Nvcli_ciud";

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos";

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  "noov:Nvfac_orde" = COALESCE("noov:Nvfac_orde", '');
  "noov:Nvfac_remi" = COALESCE("noov:Nvfac_remi", '');
  "noov:Nvfac_rece" = COALESCE("noov:Nvfac_rece", '');
  "noov:Nvfac_entr" = COALESCE("noov:Nvfac_entr", '');
  "noov:Nvfac_ccos" = COALESCE("noov:Nvfac_ccos", '');

  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);

  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Caiu <> 0 THEN '11'
                                              WHEN V_Mand <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Caiu <> 0 THEN '15'
                                                   WHEN V_Mand <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;

  "noov:Nvfac_fpag" = CASE
                        WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                        WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                        ELSE 'ZZZ'
                      END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END
  ELSE
  BEGIN
    IF ("noov:Nvfac_fech" <> "noov:Nvfac_venc") THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    "noov:Nvcli_cper" = '2';
  ELSE
    "noov:Nvcli_cper" = '1';

  "noov:Nvcli_loca" = '';
  "noov:Nvcli_mail" = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  "noov:Nvema_copi" = '';

  "noov:Nvfac_obse" = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  SELECT SUM("noov:Nvfac_stot")
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Nvfac_Stot;

  "noov:Nvfac_stot" = V_Nvfac_Stot;

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;

  IF (V_Copag = 0) THEN
    "noov:Nvfac_desc" = V_Copago;
  ELSE
    "noov:Nvfac_anti" = V_Copago;

  "noov:Nvfac_desc" = "noov:Nvfac_desc" + V_Descuento_Det;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo")
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;

  V_Impuestos = COALESCE(V_Impuestos, 0);

  SELECT SUM(Stot_Red)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Stot_Red;

  "noov:Nvfac_totp" = V_Stot_Red + V_Impuestos - "noov:Nvfac_desc";

  "noov:Nvfor_oper" = '';
  IF ("noov:Nvfac_anti" > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      "noov:Nvfac_desc" = "noov:Nvfac_anti";
      "noov:Nvfac_anti" = 0;
    END
    ELSE
      "noov:Nvfac_totp" = "noov:Nvfac_totp" - "noov:Nvfac_anti";
    "noov:Nvfor_oper" = 'SS-CUFE';
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    "noov:Nvfor_oper" = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_numb" = '';
  "noov:Nvfac_obsb" = '';
  "noov:Nvcon_codi" = '';
  "noov:Nvcon_desc" = '';
  "noov:Nvfac_tcru" = '';
  "noov:Nvfac_numb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvcon_codi" = V_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      "noov:Nvfac_tcru" = 'L';
    ELSE
      "noov:Nvfac_tcru" = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijo_Ref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numero_Ref);
    "noov:Nvfac_fecb" = TRIM(COALESCE(V_Fecha_Ref, "noov:Nvfac_fech"));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END
  SUSPEND;

END^

SET TERM ; ^


/**** MODULO GESTION FINAL                               ****/


/************************************************************/
/**** MODULO CONTABLE                                    ****/
/************************************************************/

/* FECHA: 21ABR2023 */
/* CAMBIOS EXOGENA AG 2022 */
UPDATE MM_CONCEPTOS SET NOMBRE='PASIVOS LABORALES (ESTE CONCEPTO NO SE USA PARA EL AÑO GRAVABLE 2022)' WHERE CODIGO=2205 AND CODFORMATO='1009_V7';

UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=2205 AND CODFORMATO='1009_V7';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8161 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8163 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8205 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8227 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8229 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8235 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8260 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8280 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=8281 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9010 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9022 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9040 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9050 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9064 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9070 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9071 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9104 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9201 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9223 AND CODFORMATO='1011_V6';
UPDATE MM_CONCEPTOS SET CODCUENTA=NULL WHERE CODIGO=9224 AND CODFORMATO='1011_V6';

UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2214','SALDO DE LOS PASIVOS POR APORTES PARAFISCALES,SALUD, PENSION Y CESANTIAS','3887','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2214','SALDO DE LOS PASIVOS POR APORTES PARAFISCALES,SALUD, PENSION Y CESANTIAS','3888','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2214','SALDO DE LOS PASIVOS POR APORTES PARAFISCALES,SALUD, PENSION Y CESANTIAS','3889','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2214','SALDO DE LOS PASIVOS POR APORTES PARAFISCALES,SALUD, PENSION Y CESANTIAS','3890','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2214','SALDO DE LOS PASIVOS POR APORTES PARAFISCALES,SALUD, PENSION Y CESANTIAS','3891','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2214','SALDO DE LOS PASIVOS POR APORTES PARAFISCALES,SALUD, PENSION Y CESANTIAS','3892','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2215','SALDO DE LOS PASIVOS LABORALES REALES CONSOLIDADOS EN CABEZA DEL TRABAJADOR, SIN INCLUIR CESANTIAS','3893','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2215','SALDO DE LOS PASIVOS LABORALES REALES CONSOLIDADOS EN CABEZA DEL TRABAJADOR, SIN INCLUIR CESANTIAS','3894','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2215','SALDO DE LOS PASIVOS LABORALES REALES CONSOLIDADOS EN CABEZA DEL TRABAJADOR, SIN INCLUIR CESANTIAS','3895','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2215','SALDO DE LOS PASIVOS LABORALES REALES CONSOLIDADOS EN CABEZA DEL TRABAJADOR, SIN INCLUIR CESANTIAS','3896','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2215','SALDO DE LOS PASIVOS LABORALES REALES CONSOLIDADOS EN CABEZA DEL TRABAJADOR, SIN INCLUIR CESANTIAS','3897','1009_V7','FINAL','UNO','500000','N','S','N' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO ) VALUES ('2215','SALDO DE LOS PASIVOS LABORALES REALES CONSOLIDADOS EN CABEZA DEL TRABAJADOR, SIN INCLUIR CESANTIAS','3898','1009_V7','FINAL','UNO','500000','N','S','N' ) ;

UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8407','DEDUCCION POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION. E.T., ART.158-1','3899','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8407','DEDUCCION POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION. E.T., ART.158-2','3900','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8408','DONACIONES RECIBIDAS POR EL FONDO NAL. DE FIN. PARA LA CIENCIA, LA TECNOLOGIA Y LA INNOVACION','3901','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8408','DONACIONES RECIBIDAS POR EL FONDO NAL. DE FIN. PARA LA CIENCIA, LA TECNOLOGIA Y LA INNOVACION','3902','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8409','REMUNERACION POR LA VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTR. DE RENTA','3903','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8409','REMUNERACION POR LA VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTR. DE RENTA','3904','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8410','DONACIONES RECIBIDAS DEL ICETEX, DIRIGIDAS A PROG. DE BECAS DE QUIENES INGRESEN A LA FUERZA PUBLICA','3905','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8410','DONACIONES RECIBIDAS DEL ICETEX, DIRIGIDAS A PROG. DE BECAS DE QUIENES INGRESEN A LA FUERZA PUBLICA','3906','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8411','DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA','3907','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8411','DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA','3908','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8412','DEPRECIACION ESPECIAL CONTRIBUYENTES DEL REGIMEN DE MEGA-INVERSIONES QUE REALICEN NUEVAS INVERSIONES','3909','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('8412','DEPRECIACION ESPECIAL CONTRIBUYENTES DEL REGIMEN DE MEGA-INVERSIONES QUE REALICEN NUEVAS INVERSIONES','3910','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('9111','TARIFA DEL 5% HASTA EL 31/DIC/2022 EN LA VENTA DE TIQUETES AEREOS DE PASAJEROS, SERVICIOS CONEXOS','3911','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;
UPDATE OR INSERT INTO MM_CONCEPTOS ( CODIGO, NOMBRE, RENGLON,CODFORMATO,SALDO,VALOR,TOPE,REVELAR,TENDENCIA_CREDITO,EMPLEADO,FORZOSO ) VALUES ('9111','TARIFA DEL 5% HASTA EL 31/DIC/2022 EN LA VENTA DE TIQUETES AEREOS DE PASAJEROS, SERVICIOS CONEXOS','3912','1011_V6','FINAL','UNO','0','N','N','N','S' ) ;

COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Mm INTEGER;
BEGIN
  SELECT MAX(Renglon)
  FROM Mm_Conceptos
  INTO V_Mm;

  EXECUTE PROCEDURE Sys_Inicia_Generador('GEN_MM_CONCEPTOS', :V_Mm);
END;

COMMIT WORK;

/* CONTABILIDAD A DOCUMENTO DS*/
UPDATE Documentos
SET Contabilidad = 'S'
WHERE Codigo = 'DS';

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Tipo CHAR(5);
BEGIN
  FOR SELECT DISTINCT Tipo
      FROM Auto_Patrones
      INTO V_Tipo
  DO
  BEGIN
    UPDATE Documentos
    SET Contabilidad = 'S'
    WHERE Codigo = :V_Tipo;
  END
END;
COMMIT WORK; 

/* FECHA: 19/MAYO/2023 */

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Auto_Det (
    Tipo_ CHAR(5),
    Prefijo_ CHAR(5),
    Numero_ CHAR(10))
RETURNS (
    Consecutivo INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    Stot_Red DOUBLE PRECISION)
AS
DECLARE VARIABLE Referencia CHAR(20);
DECLARE VARIABLE Bruto NUMERIC(17,2);
DECLARE VARIABLE Stot DOUBLE PRECISION;
DECLARE VARIABLE Descuento DOUBLE PRECISION;
DECLARE VARIABLE Porc_Descuento DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa DOUBLE PRECISION;
DECLARE VARIABLE V_Cdia CHAR(5);
DECLARE VARIABLE Nom_Referencia VARCHAR(198);
DECLARE VARIABLE Cantidad DOUBLE PRECISION;
DECLARE VARIABLE V_Valo DOUBLE PRECISION;
DECLARE VARIABLE V_Desc DOUBLE PRECISION;
DECLARE VARIABLE V_Base DOUBLE PRECISION;
DECLARE VARIABLE Nota VARCHAR(200);
BEGIN
  FOR SELECT Codconcepto,
             SUBSTRING(IIF(TRIM(Nota) = '', Nombre_Concepto, Nota) FROM 1 FOR 198),
             Cantidad,
             Bruto + Descuento,
             Descuento,
             Nota
      FROM Fe_Apo_Detalle(:Tipo_, :Prefijo_, :Numero_)
      INTO :Referencia,
           :Nom_Referencia,
           :Cantidad,
           :Bruto,
           :Descuento,
           :Nota
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);


    IF (Bruto * Cantidad > 0) THEN
    BEGIN
      Porc_Descuento = Descuento * 100 / Bruto;
      Stot = Cantidad * (Bruto - Descuento);
      -- Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento * :Cantidad, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      V_Tarifa = 0;
      V_Cdia = '00';
      SELECT FIRST 1 "noov:Nvimp_porc",
                     "noov:Nvimp_cdia"

      FROM Pz_Fe_Auto_Imp_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Referencia = :Referencia
      INTO V_Tarifa,
           V_Cdia;

      V_Base = (Bruto - Descuento) * Cantidad;
      V_Valo = V_Base * V_Tarifa / 100;

      Consecutivo = 1;
      "noov:Nvpro_codi" = Referencia;
      "noov:Nvpro_nomb" = Nom_Referencia;
      "noov:Nvuni_desc" = '94';
      "noov:Nvfac_cant" = Cantidad;
      "noov:Nvfac_valo" = Bruto;
      "noov:Nvimp_cdia" = V_Cdia;
      "noov:Nvfac_pdes" = (SELECT Valor
                           FROM Redondeo_Dian(:Porc_Descuento, 2));
      "noov:Nvfac_desc" = Descuento * Cantidad;
      "noov:Nvfac_stot" = Stot;
      "noov:Nvdet_piva" = V_Tarifa;
      "noov:Nvdet_viva" = V_Valo;
      "noov:Nvdet_tcod" = '';
      "noov:Nvpro_cean" = '';
      "noov:Nvdet_entr" = '';
      "noov:Nvuni_quan" = 0;

      "noov:Nvdet_nota" = CASE TRIM(Referencia)
                            WHEN 'AIU_A' THEN 'Contrato de servicios AIU por concepto de: Servicios'
                            ELSE TRIM(Nota)
                          END;

      "noov:Nvdet_padr" = 0;
      "noov:Nvdet_marc" = '';
      "noov:Nvdet_mode" = '';

      SUSPEND;

    END
  END
END^

SET TERM ; ^


/**** MODULO CONTABLE FINAL                              ****/

/************************************************************/
/**** MODULO NOMINA                                      ****/
/************************************************************/

/* FECHA: 14/ABR/2023 */

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Pila_01
RETURNS (
    E_01 VARCHAR(2),
    E_02 VARCHAR(1),
    E_03 VARCHAR(4),
    E_04 VARCHAR(200),
    E_05 VARCHAR(5),
    E_06 VARCHAR(16),
    E_07 VARCHAR(1),
    E_08 VARCHAR(1),
    E_09 VARCHAR(10),
    E_10 VARCHAR(10),
    E_11 VARCHAR(1),
    E_12 VARCHAR(10),
    E_13 VARCHAR(40),
    E_14 VARCHAR(6),
    E_15 VARCHAR(7),
    E_16 VARCHAR(7),
    E_17 VARCHAR(10),
    E_18 VARCHAR(10),
    E_19 VARCHAR(5),
    E_20 DOUBLE PRECISION,
    E_21 VARCHAR(2),
    E_22 VARCHAR(2))
AS
DECLARE VARIABLE V_Empleado  VARCHAR(15);
DECLARE VARIABLE V_Identidad VARCHAR(5);
DECLARE VARIABLE V_Puesto    VARCHAR(10);
DECLARE VARIABLE V_Tipo      CHAR(2);
DECLARE VARIABLE V_C45       DOUBLE PRECISION;
DECLARE VARIABLE V_B18       DOUBLE PRECISION;
DECLARE VARIABLE V_B19       DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Reg  CHAR(2);
DECLARE VARIABLE V_T39       DOUBLE PRECISION;
DECLARE VARIABLE V_C39       DOUBLE PRECISION;
BEGIN
  E_01 = '01';
  E_02 = '1';
  E_03 = '0001';
  E_08 = 'E';
  E_09 = '';
  E_10 = '';
  E_17 = 0;
  E_18 = '';

  -- Tercero
  SELECT Codidentidad, Nombre, Codigo, Dv
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Identidad, E_04, E_06, E_07;

  -- Identidad
  SELECT SUBSTRING(Codigo2 FROM 1 FOR 2)
  FROM Identidades
  WHERE Codigo = :V_Identidad
  INTO E_05;
  E_05 = COALESCE(E_05, '');

  SELECT FIRST 1 TRIM(Codpuesto)
  FROM Pila_Fechas
  INTO V_Puesto;

  SELECT Forma, Codigo, Nombre
  FROM Puestos
  WHERE Codigo = :V_Puesto
  INTO E_11, E_12, E_13;

  IF (E_11 = 'U') THEN
    E_12 = '';

  IF (E_11 = 'U') THEN
    E_13 = '';

  SELECT
  LEFT(Codigo, 6)
  FROM Tipoaportes
  WHERE Clase = 'ARL'
  INTO E_14;
  E_14 = COALESCE(E_14, '');

  SELECT EXTRACT(YEAR FROM Desde) || '-' || LPAD(EXTRACT(MONTH FROM Desde), 2, '0'),
         EXTRACT(YEAR FROM Hasta + 5) || '-' || LPAD(EXTRACT(MONTH FROM Hasta + 5), 2, '0')
  FROM Pila_Fechas
  INTO E_15, E_16;

  --Numero de empleados
  SELECT COUNT(DISTINCT(Codpersonal))
  FROM Pila
  INTO E_19;

  -- E_20
  E_20 = 0;
  FOR SELECT Codpersonal, C_05, C_39, C_45, B_19, B_18, Tipo
      FROM Pila
      WHERE C_36 + C_37 + C_38 + C_39 > 0
      INTO V_Empleado, V_Tipo, V_C39, V_C45, V_B19, V_B18, V_Tipo_Reg
  DO
  BEGIN

    SELECT SUM(C_39)
    FROM Pila
    WHERE Codpersonal = :V_Empleado
    INTO V_T39;

    IF (V_Tipo NOT IN ('12', '19')) THEN
      IF (V_C39 = V_T39) THEN
        E_20 = E_20 + V_C45 + V_B18 + V_B19;
      ELSE
      IF (V_Tipo_Reg = 'RU') THEN
        E_20 = E_20 + (V_C45 / V_T39 * V_C39) + V_B19 + V_B18;
      ELSE
        E_20 = E_20 + V_C45 / V_T39 * V_C39;
    E_20 = ROUND(CEILING(ROUND(E_20, 2)));
  END
  E_20 = ROUND(E_20);

  E_21 = '01';
  E_22 = '86';

  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TERCEROS TO PROCEDURE PZ_PILA_01;
GRANT SELECT ON IDENTIDADES TO PROCEDURE PZ_PILA_01;
GRANT SELECT ON PILA_FECHAS TO PROCEDURE PZ_PILA_01;
GRANT SELECT ON PUESTOS TO PROCEDURE PZ_PILA_01;
GRANT SELECT ON TIPOAPORTES TO PROCEDURE PZ_PILA_01;
GRANT SELECT ON PILA TO PROCEDURE PZ_PILA_01;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_PILA_01 TO SYSDBA;
  
COMMIT WORK;
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Pila_02
RETURNS (
    C_01 CHAR(2),
    C_02 INTEGER,
    C_03 CHAR(2),
    C_04 CHAR(16),
    C_05 CHAR(2),
    C_06 CHAR(2),
    C_07 CHAR(1),
    C_08 CHAR(1),
    C_09 CHAR(5),
    C_10 CHAR(5),
    C_11 CHAR(20),
    C_12 CHAR(30),
    C_13 CHAR(20),
    C_14 CHAR(30),
    C_15 CHAR(1),
    C_16 CHAR(1),
    C_17 CHAR(1),
    C_18 CHAR(1),
    C_19 CHAR(1),
    C_20 CHAR(1),
    C_21 CHAR(1),
    C_22 CHAR(1),
    C_23 CHAR(1),
    C_24 CHAR(1),
    C_25 CHAR(1),
    C_26 CHAR(1),
    C_27 CHAR(1),
    C_28 CHAR(1),
    C_29 CHAR(1),
    C_30 INTEGER,
    C_31 CHAR(6),
    C_32 CHAR(6),
    C_33 CHAR(6),
    C_34 CHAR(6),
    C_35 CHAR(6),
    C_36 DOUBLE PRECISION,
    C_37 DOUBLE PRECISION,
    C_38 DOUBLE PRECISION,
    C_39 DOUBLE PRECISION,
    C_40 DOUBLE PRECISION,
    C_41 CHAR(1),
    C_42 DOUBLE PRECISION,
    C_43 DOUBLE PRECISION,
    C_44 DOUBLE PRECISION,
    C_45 DOUBLE PRECISION,
    C_46 VARCHAR(7),
    C_47 DOUBLE PRECISION,
    C_48 DOUBLE PRECISION,
    C_49 DOUBLE PRECISION,
    C_50 DOUBLE PRECISION,
    C_51 DOUBLE PRECISION,
    C_52 DOUBLE PRECISION,
    C_53 DOUBLE PRECISION,
    C_54 VARCHAR(7),
    C_55 DOUBLE PRECISION,
    C_56 DOUBLE PRECISION,
    C_57 CHAR(15),
    C_58 DOUBLE PRECISION,
    C_59 CHAR(15),
    C_60 DOUBLE PRECISION,
    C_61 VARCHAR(9),
    C_62 CHAR(10),
    C_63 DOUBLE PRECISION,
    C_64 VARCHAR(7),
    C_65 DOUBLE PRECISION,
    C_66 VARCHAR(7),
    C_67 DOUBLE PRECISION,
    C_68 VARCHAR(7),
    C_69 DOUBLE PRECISION,
    C_70 VARCHAR(7),
    C_71 DOUBLE PRECISION,
    C_72 VARCHAR(7),
    C_73 DOUBLE PRECISION,
    C_74 CHAR(2),
    C_75 CHAR(16),
    C_76 CHAR(1),
    C_77 CHAR(6),
    C_78 CHAR(1),
    C_79 CHAR(1),
    C_80 CHAR(10),
    C_81 CHAR(10),
    C_82 CHAR(10),
    C_83 CHAR(10),
    C_84 CHAR(10),
    C_85 CHAR(10),
    C_86 CHAR(10),
    C_87 CHAR(10),
    C_88 CHAR(10),
    C_89 CHAR(10),
    C_90 CHAR(10),
    C_91 CHAR(10),
    C_92 CHAR(10),
    C_93 CHAR(10),
    C_94 CHAR(10),
    C_95 DOUBLE PRECISION,
    C_96 DOUBLE PRECISION,
    C_97 CHAR(10),
    C_98 CHAR(7))
AS
DECLARE VARIABLE Tipo          CHAR(2);
DECLARE VARIABLE Codpersonal   CHAR(15);
DECLARE VARIABLE V_Sena        CHAR(1);
DECLARE VARIABLE V_Pensionado  CHAR(1);
DECLARE VARIABLE V_Parcial     CHAR(1);
DECLARE VARIABLE V_Lectivo     CHAR(1);
DECLARE VARIABLE V_Ciudad      VARCHAR(5);
DECLARE VARIABLE V_Exonerado   CHAR(1);
DECLARE VARIABLE V_Unico       CHAR(1);
DECLARE VARIABLE V_Alto_Riesgo CHAR(1);
DECLARE VARIABLE T_36          DOUBLE PRECISION;
DECLARE VARIABLE T_37          DOUBLE PRECISION;
DECLARE VARIABLE T_38          DOUBLE PRECISION;
DECLARE VARIABLE V_C_54        DOUBLE PRECISION;
DECLARE VARIABLE V_C_64        DOUBLE PRECISION;
DECLARE VARIABLE V_C_66        DOUBLE PRECISION;
DECLARE VARIABLE V_C_68        DOUBLE PRECISION;
DECLARE VARIABLE V_C_70        DOUBLE PRECISION;
DECLARE VARIABLE V_C_72        DOUBLE PRECISION;
DECLARE VARIABLE V_C_61        DOUBLE PRECISION;
DECLARE VARIABLE V_C_46        DOUBLE PRECISION;
DECLARE VARIABLE T_39          DOUBLE PRECISION;
DECLARE VARIABLE V_C_80        DATE;
DECLARE VARIABLE V_C_81        DATE;
DECLARE VARIABLE V_C_82        DATE;
DECLARE VARIABLE V_C_83        DATE;
DECLARE VARIABLE V_C_84        DATE;
DECLARE VARIABLE V_C_85        DATE;
DECLARE VARIABLE V_C_86        DATE;
DECLARE VARIABLE V_C_87        DATE;
DECLARE VARIABLE V_C_88        DATE;
DECLARE VARIABLE V_C_89        DATE;
DECLARE VARIABLE V_C_90        DATE;
DECLARE VARIABLE V_C_91        DATE;
DECLARE VARIABLE V_C_92        DATE;
DECLARE VARIABLE V_C_93        DATE;
DECLARE VARIABLE V_C_94        DATE;
DECLARE VARIABLE C_00          DOUBLE PRECISION;
DECLARE VARIABLE B_00          DOUBLE PRECISION;
DECLARE VARIABLE B_01          DOUBLE PRECISION;
DECLARE VARIABLE B_02          DOUBLE PRECISION;
DECLARE VARIABLE B_03          DOUBLE PRECISION;
DECLARE VARIABLE B_04          DOUBLE PRECISION;
DECLARE VARIABLE B_05          DOUBLE PRECISION;
DECLARE VARIABLE B_06          DOUBLE PRECISION;
DECLARE VARIABLE B_07          DOUBLE PRECISION;
DECLARE VARIABLE B_08          DOUBLE PRECISION;
DECLARE VARIABLE B_09          DOUBLE PRECISION;
DECLARE VARIABLE B_10          DOUBLE PRECISION;
DECLARE VARIABLE B_11          DOUBLE PRECISION;
DECLARE VARIABLE B_12          DOUBLE PRECISION;
DECLARE VARIABLE B_13          DOUBLE PRECISION;
DECLARE VARIABLE B_14          DOUBLE PRECISION;
DECLARE VARIABLE B_15          DOUBLE PRECISION;
DECLARE VARIABLE B_16          DOUBLE PRECISION;
DECLARE VARIABLE B_17          DOUBLE PRECISION;
DECLARE VARIABLE B_18          DOUBLE PRECISION;
DECLARE VARIABLE B_19          DOUBLE PRECISION;
DECLARE VARIABLE V_Actividad   CHAR(5);
DECLARE VARIABLE V_Actividad_C CHAR(5);
DECLARE VARIABLE V_Actividad_T CHAR(5);
DECLARE VARIABLE V_Subp        INTEGER;
DECLARE VARIABLE V_Subpl       INTEGER;
DECLARE VARIABLE V_Desde       DATE;
BEGIN
  SELECT TRIM(Codmunicipio), TRIM(Codactividad)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Ciudad, V_Actividad_T;

  FOR SELECT Desde, Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06,
             C_07, C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17,
             C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28,
             C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39,
             C_40, C_41, C_42, C_43, C_44, C_45, C_46, C_47, C_48, C_49, C_50,
             C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60, C_61,
             C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72,
             C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83,
             C_84, C_85, C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94,
             C_95, C_96, B_00, B_01, B_02, B_03, B_04, B_05, B_06, B_07, B_08,
             B_09, B_10, B_11, B_12, B_13, B_14, B_15, B_16, B_17, B_18, B_19
      FROM Pila
      WHERE C_36 + C_37 + C_38 + C_39 > 0
      INTO V_Desde, Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06,
           C_07, C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17,
           C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28,
           C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39,
           C_40, C_41, C_42, C_43, C_44, C_45, V_C_46, C_47, C_48, C_49, C_50,
           C_51, C_52, C_53, V_C_54, C_55, C_56, C_57, C_58, C_59, C_60, V_C_61,
           C_62, C_63, V_C_64, C_65, V_C_66, C_67, V_C_68, C_69, V_C_70, C_71,
           V_C_72, C_73, C_74, C_75, C_76, C_77, C_78, C_79, V_C_80, V_C_81,
           V_C_82, V_C_83, V_C_84, V_C_85, V_C_86, V_C_87, V_C_88, V_C_89,
           V_C_90, V_C_91, V_C_92, V_C_93, V_C_94, C_95, C_96, B_00, B_01, B_02,
           B_03, B_04, B_05, B_06, B_07, B_08, B_09, B_10, B_11, B_12, B_13,
           B_14, B_15, B_16, B_17, B_18, B_19
  DO
  BEGIN

    -- Datos de Personal
    SELECT Alto_Riesgo
    FROM Personal
    WHERE Codigo = :Codpersonal
    INTO V_Alto_Riesgo;
    V_Alto_Riesgo = COALESCE(V_Alto_Riesgo, ' ');

    -- Constantes Global (Actividad)
    V_Actividad = V_Actividad_T;
    V_Subp = NULL;
    V_Actividad_c = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantesvalor
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
    INTO V_Subp, V_Actividad_c;
    V_Subp = COALESCE(V_Subp, 0);
    IF (TRIM(COALESCE(V_Actividad_c, '')) <> '') THEN
      V_Actividad = V_Actividad_c;

    -- Constante local
    V_Subpl = NULL;
    V_Actividad_C = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantes_Personal
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
          AND Codpersonal = :Codpersonal
    INTO V_Subpl, V_Actividad_C;
    V_Subp = COALESCE(V_Subpl, V_Subp);
    IF (TRIM(COALESCE(V_Actividad_C, '')) <> '') THEN
      V_Actividad = V_Actividad_C;

    -- Totales
    SELECT SUM(C_36), SUM(C_37), SUM(C_38), SUM(C_39)
    FROM Pila
    WHERE Codpersonal = :Codpersonal
    INTO T_36, T_37, T_38, T_39;

    -- C_05 Tipo Empleado
    IF (TRIM(C_05) = '') THEN
      C_05 = '1';
    ELSE
      C_05 = TRIM(C_05);

    -- Sena
    V_Sena = 'N';
    IF (C_05 IN ('12', '19')) THEN
      V_Sena = 'S';

    -- Tiempo parcial
    V_Parcial = 'N';
    IF (C_05 IN ('51', '68')) THEN
      V_Parcial = 'S';

    -- Sena Lectivo
    V_Lectivo = 'N';
    IF (C_05 = '12') THEN
      V_Lectivo = 'S';

    -- Pensionado
    V_Pensionado = 'N';
    IF (C_06 > 0) THEN
      V_Pensionado = 'S';

    -- Unico Registro
    V_Unico = 'N';
    IF (C_36 = T_36) THEN
      V_Unico = 'S';

    -- Exonerado salud, SENA e ICBF
    V_Exonerado = 'N';
    IF (C_76 = 'S') THEN
      V_Exonerado = 'S';

    IF (V_Sena = 'S') THEN
      C_09 = V_Ciudad;

    IF (C_27 = 'X' AND
        B_17 > 0) THEN
      C_27 = 'L';

    C_28 = '';
    IF (C_48 > 0) THEN
      C_28 = 'X';

    -- C_30 IRL: Dias de incapacidad por accidente de trabajo o enfermedad laboral.
    IF (C_30 <> C_38) THEN
      C_30 = 0;
    C_30 = CAST(C_30 AS CHAR(2));

    C_32 = COALESCE(C_32, '');
    C_33 = COALESCE(C_33, '');
    C_34 = COALESCE(C_34, '');
    C_35 = COALESCE(C_35, '');

    -- C_36 Numero de dias cotizados a pension
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_36 = 0;
    ELSE
    IF (C_36 > 0) THEN
      IF (V_C_46 = 0) THEN
        C_36 = 0;
      ELSE
      IF (V_Parcial = 'S') THEN
        C_36 = B_14;
      ELSE
        C_36 = C_36;
    ELSE
      C_36 = 0;
    C_36 = CAST(ROUND(C_36) AS CHAR(2));

    -- C_37 N?mero de d?as cotizados a salud
    IF (C_37 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_37 = B_15;
    END
    ELSE
      C_37 = 0;
    C_37 = CAST(ROUND(C_37) AS CHAR(2));

    -- C_38 N?mero de d?as cotizados a Riesgos Laborales
    IF (C_38 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_38 = 30;
      ELSE
      IF (V_Lectivo = 'S') THEN
        C_38 = 0;
    END
    ELSE
      C_38 = 0;
    C_38 = CAST(ROUND(C_38) AS CHAR(2));

    -- C_39 N?mero de d?as cotizados a Caja de Compensaci?n Familiar
    IF (V_Sena = 'S') THEN
      C_39 = 0;
    ELSE
    IF (C_39 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_39 = B_16;
    END
    ELSE
      C_39 = 0;
    C_39 = CAST(ROUND(C_39) AS CHAR(2));

    C_40 = CAST(ROUND(C_40) AS CHAR(9));

    -- C_41
    IF (V_Sena = 'S' OR (V_Parcial = 'S')) THEN
      C_41 = '';
    ELSE
    IF (TRIM(COALESCE(C_41, '')) = '') THEN
      C_41 = 'F';

    -- C_42 IBC pensi?n
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_42 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_42 = C_42 + B_19;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_42 = IIF(C_40 < C_00, C_00, C_40) / 30 * C_36 + B_19;
      ELSE
        C_42 = (C_42 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_36 - (B_12 - B_17)) * C_36;
    ELSE
      C_42 = C_42 / T_36 * C_36;
    C_42 = CAST(ROUND(CEILING(ROUND(C_42, 2))) AS CHAR(9));

    -- C_43 IBC salud
    IF (V_Unico = 'S') THEN
      C_43 = C_43 + B_19;
    ELSE
    IF (T_37 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_43 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_37) + B_19;
      ELSE
        C_43 = (C_43 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_37 - (B_12 - B_17)) * C_37;
    ELSE
      C_43 = C_43 / T_37 * C_37;
    C_43 = CAST(ROUND(CEILING(ROUND(C_43, 2))) AS CHAR(9));

    -- C_44 IBC Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      C_44 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_44 = C_44 + B_19;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_44 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_38) + B_19;
      ELSE
        C_44 = (C_44 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_38 - (B_12 - B_17)) * C_38;
    ELSE
      C_44 = C_44 / T_38 * C_38;
    C_44 = CAST(ROUND(CEILING(ROUND(C_44, 2))) AS CHAR(9));

    -- C_45 IBC CCF
    IF (V_Sena = 'S') THEN
      C_45 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_45 = C_45 + B_19 + B_18;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_45 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_39) + B_19 + B_18;
      ELSE
        C_45 = (C_45 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_39 - (B_12 - B_17)) * C_39;
    ELSE
      C_45 = IIF(C_40 < C_00, C_00, C_40) / T_39 * C_39;
    C_45 = CAST(ROUND(CEILING(ROUND(C_45, 2))) AS CHAR(9));

    -- C_46 Tarifa de aportes pensiones,
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      V_C_46 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      V_C_46 = B_00 / 100;
    ELSE
      V_C_46 = V_C_46 / 100;
    C_46 = CAST(V_C_46 AS CHAR(7));

    -- C_47 Cotizaci?n a pensiones
    C_47 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_42 * :V_C_46, 100))) AS CHAR(9));

    -- C_48
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_48, 100))) AS CHAR(9));

    -- C_49
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_49, 100))) AS CHAR(9));

    -- C_50 Total cotizaci?n Sistema General de Pensiones
    C_50 = CAST(ROUND(C_47 + C_48 + C_49) AS CHAR(9));

    -- C_51 Aportes a fondo de solidaridad pensional
    IF (C_40 + B_19 < 4 * C_00) THEN
      C_51 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      C_51 = 0;
    ELSE
    IF (C_36 = T_36) THEN
      C_51 = C_42;
    ELSE
      C_51 = C_42;
    IF (C_51 <> 0) THEN
    BEGIN
      C_51 = (SELECT Tope
              FROM Sys_Redondea(ROUND((CEILING(:C_51) * :B_02 / 100) / 2), 100));
    END
    C_51 = CAST(ROUND(C_51) AS CHAR(9));

    -- C_52 Aportes a subcuenta de subsistencia.
    C_52 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_51 + (:C_52 / :T_36 * :C_36), 100))) AS CHAR(9));

    -- C_53
    C_53 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_53, 100))) AS CHAR(9));

    -- C_76 Cotizante exonerado de pago de aporte salud, SENA e ICBF
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_76 = 'N';
    ELSE
    IF ((C_40 + B_19) >= C_00 * 10) THEN
      C_76 = 'N';

    -- C_54 Tarifa salud
    V_C_54 = CASE
               WHEN V_Parcial = 'S' THEN 0.00
               WHEN V_Sena = 'S' THEN V_C_54
               WHEN C_40 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN C_24 = 'X' AND C_76 = 'S' THEN 0.00
               WHEN C_24 = 'X' AND C_76 = 'N' THEN B_01
               WHEN C_40 + B_19 >= C_00 * 10 AND C_24 = 'X' THEN B_01
               WHEN C_40 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN V_Exonerado = 'S' THEN B_04
               ELSE V_C_54
             END;
    C_54 = CAST(V_C_54 / 100 AS CHAR(7));

    -- C_55 Cotizaci?n salud
    C_55 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_43 * :V_C_54 / 100, 100))) AS CHAR(9));

    -- C_56
    C_56 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_56, 100))) AS CHAR(9));

    C_58 = CAST(ROUND(:C_58) AS CHAR(9));
    C_60 = CAST(ROUND(:C_60) AS CHAR(9));

    -- C_61 Tarifa de aportes a Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      V_C_61 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_27 = 'L' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_61 = 0;
    ELSE
      V_C_61 = V_C_61 / 100;
    C_61 = CAST(V_C_61 AS CHAR(9));

    C_62 = CAST(C_62 AS CHAR(9));

    -- C_63 Cotizacion Riesgos Laborales
    C_63 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_44 * :V_C_61, 100))) AS CHAR(9));

    -- C_64 Tarifa de aportes CCF
    IF (V_Sena = 'S') THEN
      V_C_64 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_64 = 0;
    ELSE
      V_C_64 = V_C_64 / 100;
    C_64 = CAST(V_C_64 AS CHAR(7));

    -- C_65 Valor aporte CCF
    C_65 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_45 * :V_C_64, 100))) AS CHAR(9));

    -- C_95 IBC Otros parafiscales
    IF (C_69 = 0 OR C_24 = 'X' OR C_25 = 'X') THEN
      C_95 = 0;
    ELSE
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_95 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_95 = C_95 + B_19 + B_18;
    ELSE
    IF (Tipo = 'RU') THEN
      C_95 = (IIF(C_40 < C_00, C_00, C_40) / T_36 * C_36) + B_19 + B_18 + B_13;
    ELSE
      C_95 = IIF(C_40 < C_00, C_00, C_40) / T_36 * C_36;
    C_95 = CAST(ROUND(CEILING(ROUND(C_95))) AS CHAR(9));

    -- C_66 Tarifa de aportes SENA
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      V_C_66 = 0;
    ELSE
      V_C_66 = V_C_66 / 100;
    C_66 = CAST(V_C_66 AS CHAR(7));

    -- C_67 Valor aportes SENA
    C_67 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :V_C_66, 100))) AS CHAR(9));

    -- C_68 Tarifa aportes ICBF
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      V_C_68 = 0;
    ELSE
      V_C_68 = V_C_68 / 100;
    C_68 = CAST(V_C_68 AS CHAR(7));

    -- C_69 Valor aporte ICBF
    C_69 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :V_C_68, 100))) AS CHAR(9));

    -- C_70 Tarifa aportes ESAP
    V_C_70 = V_C_70 / 100;
    C_70 = CAST(V_C_70 AS CHAR(7));

    -- C_71 Valor aporte ESAP
    C_71 = CAST(ROUND(C_71) AS CHAR(9));

    -- C_72 Tarifa aportes MEN
    V_C_72 = V_C_72 / 100;
    C_72 = CAST(V_C_72 AS CHAR(7));

    -- C_73 Valor aporte MEN
    C_73 = CAST(ROUND(C_73) AS CHAR(9));

    -- C_77 Codigo ARL
    IF (V_Lectivo = 'S') THEN
      C_77 = '';
    ELSE
      C_77 = COALESCE(C_77, '');

    -- C_78 Clase de riesgo
    IF (V_Lectivo = 'S') THEN
      C_78 = 0;
    --   ELSE
    --    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_30 > 0) AND
    --        (Tipo <> 'RU')) THEN
    --     C_78 = 0;

    -- C_79 tarifa especial pensiones

    IF (V_Alto_Riesgo = 'S') THEN
      C_79 = '1';
    ELSE
      C_79 = ' ';

    -- C_80 Fecha de ingreso
    C_80 = EXTRACT(YEAR FROM V_C_80) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_80) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_80) + 100 FROM 2);

    -- C_81 Fecha de retiro
    C_81 = EXTRACT(YEAR FROM V_C_81) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_81) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_81) + 100 FROM 2);

    -- C_82 Fecha de retiro
    C_82 = EXTRACT(YEAR FROM V_C_82) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_82) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_82) + 100 FROM 2);

    -- C_83 Fecha Inicio SLN
    C_83 = EXTRACT(YEAR FROM V_C_83) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_83) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_83) + 100 FROM 2);

    -- C_84 Fecha fin SLN
    C_84 = EXTRACT(YEAR FROM V_C_84) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_84) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_84) + 100 FROM 2);

    -- C_85 Fecha inicio IGE
    C_85 = EXTRACT(YEAR FROM V_C_85) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_85) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_85) + 100 FROM 2);

    -- C_86 Fecha fin IGE
    C_86 = EXTRACT(YEAR FROM V_C_86) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_86) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_86) + 100 FROM 2);

    -- C_87 Fecha inicio LMA
    C_87 = EXTRACT(YEAR FROM V_C_87) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_87) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_87) + 100 FROM 2);

    -- C_88 Fecha fin LMA
    C_88 = EXTRACT(YEAR FROM V_C_88) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_88) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_88) + 100 FROM 2);

    -- C_89 Fecha inicio VAC - LR
    C_89 = EXTRACT(YEAR FROM V_C_89) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_89) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_89) + 100 FROM 2);

    -- C_90 Fecha fin VAC - LR
    C_90 = EXTRACT(YEAR FROM V_C_90) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_90) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_90) + 100 FROM 2);

    -- C_91 Fecha inicio VCT
    C_91 = EXTRACT(YEAR FROM V_C_91) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_91) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_91) + 100 FROM 2);

    -- C_92 Fecha fin VCT
    C_92 = EXTRACT(YEAR FROM V_C_92) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_92) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_92) + 100 FROM 2);

    -- C_93 Fecha inicio IRL
    C_93 = EXTRACT(YEAR FROM V_C_93) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_93) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_93) + 100 FROM 2);

    -- C_94 Fecha fin IRL
    C_94 = EXTRACT(YEAR FROM V_C_94) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_94) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_94) + 100 FROM 2);

    -- C_96 Numero de horas laboradas
    C_96 = ROUND(C_37 * 8);

    C_97 = ' ';

    -- C_98 Sub Actividad
    IF (V_Lectivo = 'S') THEN
      C_98 = '0000000';
    ELSE
      C_98 = C_78 || SUBSTRING(V_Actividad FROM 1 FOR 4) || SUBSTRING(CAST(V_Subp + 100 AS CHAR(3)) FROM 2 FOR 2);

    SUSPEND;
  END

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TERCEROS TO PROCEDURE PZ_PILA_02;
GRANT SELECT ON PILA TO PROCEDURE PZ_PILA_02;
GRANT SELECT ON PERSONAL TO PROCEDURE PZ_PILA_02;
GRANT SELECT ON CONSTANTESVALOR TO PROCEDURE PZ_PILA_02;
GRANT SELECT ON CONSTANTES_PERSONAL TO PROCEDURE PZ_PILA_02;
GRANT EXECUTE ON PROCEDURE SYS_REDONDEA TO PROCEDURE PZ_PILA_02;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_PILA_02 TO SYSDBA;

COMMIT WORK;

/* SALARIOS PARA EXOGENA*/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fecha_Salarios (
    Empleado_ VARCHAR(15))
RETURNS (
    Ingreso DATE,
    Retiro  DATE)
AS
BEGIN
  SELECT MAX(Desde)
  FROM Salarios
  WHERE Codpersonal = :Empleado_
        AND Columna_Desde = 'INGRESO'
  INTO Ingreso;

  SELECT MAX(S.Hasta)
  FROM Salarios S
  WHERE S.Codpersonal = :Empleado_
        AND S.Columna_Hasta = 'RETIRO'
  INTO Retiro;
  IF (Ingreso > Retiro) THEN
    Retiro = NULL;
  SUSPEND;
END^

SET TERM ; ^

COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Salario_Exo (
    Periodo_ INTEGER)
RETURNS (
    Empleado        VARCHAR(15),
    Nombre_Empleado VARCHAR(83),
    Dias            DOUBLE PRECISION,
    Acumulado_Devengado         DOUBLE PRECISION,
    Promedio        DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nomina   CHAR(5);
DECLARE VARIABLE V_Dias     DOUBLE PRECISION;
DECLARE VARIABLE V_Salario  DOUBLE PRECISION;
DECLARE VARIABLE V_Desde    DATE;
DECLARE VARIABLE V_Hasta    DATE;
DECLARE VARIABLE V_Ingreso  DATE;
DECLARE VARIABLE V_Retiro   DATE;
DECLARE VARIABLE V_Ini      DATE;
DECLARE VARIABLE V_Fin      DATE;
DECLARE VARIABLE V_Meses    DOUBLE PRECISION;
DECLARE VARIABLE V_Mes_Real INTEGER;
BEGIN
  V_Ini = '01.01.' || :Periodo_;
  V_Fin = '31.12.' || :Periodo_;

  FOR SELECT Codigo
      FROM Personal
      INTO Empleado
  DO
  BEGIN
    -- fecha ingreso
    SELECT Ingreso, Retiro
    FROM Pz_Fecha_Salarios(:Empleado)
    INTO V_Ingreso, V_Retiro;
    V_Retiro = COALESCE(:V_Retiro, :V_Fin);
    IF (:V_Retiro > :V_Fin) THEN
      V_Retiro = :V_Fin;

    IF (V_Ingreso < V_Fin AND
        V_Retiro > V_Ini) THEN
    BEGIN
      IF (V_Ingreso < :V_Ini) THEN
        V_Ingreso = :V_Ini;

      V_Meses = (:V_Retiro - :V_Ingreso);
      V_Meses = :V_Meses / 30;
      V_Mes_Real = ROUND(:V_Meses);

      --caso 1
      IF (:V_Mes_Real <= 6) THEN
      BEGIN
        V_Desde = :V_Ingreso;
        V_Hasta = :V_Retiro;
      END
      ELSE
      BEGIN
        -- caso 2
        IF (:V_Retiro = :V_Fin) THEN
        BEGIN
          V_Desde = '01.07.' || :Periodo_;
          V_Hasta = '31.12.' || :Periodo_;
        END
        ELSE
        BEGIN
          V_Desde = DATEADD(-6 MONTH TO :V_Retiro);
          V_Hasta = :V_Retiro;
        END
      END

      -- Planilla
      SELECT SUM(P.Adicion + P.Deduccion)
      FROM Planillas P
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      WHERE P.Codrubro = 'DI015'
            AND P.Codpersonal = :Empleado
            AND N.Fecha >= :V_Desde
            AND N.Fecha <= :V_Hasta
      INTO Dias;
      Dias = COALESCE(Dias, 0);

      SELECT SUM(P.Adicion + P.Deduccion)
      FROM Planillas P
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      WHERE P.Codrubro = 'SA028'
            AND P.Codpersonal = :Empleado
            AND N.Fecha >= :V_Desde
            AND N.Fecha <= :V_Hasta
      INTO Acumulado_Devengado;
      Acumulado_Devengado = COALESCE(Acumulado_Devengado, 0);

      IF (Dias > 0) THEN
        Promedio = (Acumulado_Devengado / Dias) * 30;
      ELSE
        Promedio = 0;

      -- NOmbre empleado
      SELECT Nombre
      FROM Personal
      WHERE Codigo = :Empleado
      INTO Nombre_Empleado;

      SUSPEND;
    END

  END
END^

SET TERM ; ^

COMMENT ON PROCEDURE PZ_SALARIO_EXO IS
'Promedio de Salario para Exogena
Ultimos 6 meses o el tiempo trabajado durante el AG si es menos';

COMMIT WORK;


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('NOM030220', 'PROMEDIO DE SALARIOS PARA EXOGENA', 'Promedio ultimos 6 meses del AG a reportar. Solo Nomimas nativas', 'SELECT Empleado, Nombre_Empleado, Dias, Acumulado_Devengado, Promedio
FROM Pz_Salario_Exo(:A_Periodo)', 'N', 'NOMINA', 'N', 'MOVIMIENTO NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;



/**** MODULO NOMINA FINAL                                ****/

/************************************************************/
/**** MODULO REST SERVER                                 ****/
/************************************************************/
/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_INSERT_GESTION_PRIMARIO (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    FECHA_ DATE,
    VENCE_ DATE,
    TERCERO_ VARCHAR(15),
    VENDEDOR_ VARCHAR(15),
    LISTA_ VARCHAR(5),
    BANCO_ VARCHAR(15),
    USUARIO_ VARCHAR(10),
    CENTRO_ VARCHAR(5),
    BODEGA_ VARCHAR(5),
    REFERENCIA_ VARCHAR(20),
    ENTRADA_ NUMERIC(17,4),
    SALIDA_ NUMERIC(17,4),
    UNITARIO_ NUMERIC(17,4),
    PORC_DESCUENTO_ NUMERIC(17,4),
    NOTA_ VARCHAR(200))
AS
DECLARE VARIABLE V_Plazo INTEGER;
DECLARE VARIABLE V_Renglon INTEGER;
DECLARE VARIABLE V_Registro INTEGER;
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Doc VARCHAR(20);
DECLARE VARIABLE V_Descuento NUMERIC(17,4);
DECLARE VARIABLE V_Bloqueado VARCHAR(1);
BEGIN

  V_Doc = TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_);

  /* Validamos que no exista el doc */
  SELECT COUNT(1)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN

    /* Valida que el tercero exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

    /* Valida que el vendedor exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Empleado(:Vendedor_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Vendedor_) || ' NO EXISTE ***';

    /* Valida que la caja o banco exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Banco(:Banco_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA CAJA o BANCO ' || TRIM(Banco_) || ' NO EXISTE ***';

    /* Valida que la lista de precios exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Lista(:Lista_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA LISTA DE PRECIOS ' || TRIM(Lista_) || ' NO EXISTE ***';

    /* Valida que el usuario exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

    /* Valida que el centro exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';

    /* Valida que la bodega exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Bodega(:Bodega_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA BODEGA ' || TRIM(Bodega_) || ' NO EXISTE ***';

    /* Valida que la referencia exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Referencia(:Referencia_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA REFERENCIA ' || TRIM(Referencia_) || ' NO EXISTE ***';

    /* Hallamos plazo */
    IF (:Vence_ > :Fecha_) THEN
      V_Plazo = :Vence_ - :Fecha_;
    ELSE
      V_Plazo = 0;

    /*bloqueado char(1)
   si es positivo = R
   si es negativo = N*/
    IF (:Numero_ < 0) THEN
      V_Bloqueado = 'N';
    ELSE
      V_Bloqueado = 'R';

    /* Insertamos en COMPROBANTES */
    INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Plazo, Vence, Nota, Bloqueado, Codtercero, Codvendedor,
                              Codlista, Codbanco, Codusuario, Codescenario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :Fecha_, :V_Plazo, :Vence_, '-', :V_Bloqueado, :Tercero_, :Vendedor_, :Lista_,
            :Banco_, :Usuario_, 'NA');

    V_Renglon = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                 FROM Rdb$Database);

    /* Insertamos TR_INVENTARIO */
    /*Calcular el descuento en base al porc_descuento*/
    V_Descuento = ((:Porc_Descuento_ / 100) * :Unitario_);

    INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codcentro, Codbodega, Codreferencia, Entrada, Salida,
                               Unitario, Porcentaje_Descuento, Descuento, Nota, Codusuario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Centro_, :Bodega_, :Referencia_, :Entrada_, :Salida_, :Unitario_,
            :Porc_Descuento_, :V_Descuento, :Nota_, :Usuario_);

    /* Con base al unitario(con o sin IVA) hallamos el BRUTO y demás cálculos */
    EXECUTE PROCEDURE Fx_Recalcula_Registro(:Tipo_, :Prefijo_, :Numero_, :V_Renglon);

  END

  ELSE /*Aquí encuentra que el documento ya existe y continua insertando renglones*/
  BEGIN

    /* Validamos la fecha del documento existente. Si es la misma del ws continuamos insertando. */
    SELECT Fecha
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Fecha;

    IF (V_Fecha = :Fecha_) THEN
    BEGIN
      /* Validamos que el doc no esté bloqueado */
      SELECT COUNT(1)
      FROM Comprobantes
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
            AND (Bloqueado = 'R' OR Bloqueado = 'N')
      INTO V_Registro;

      IF (V_Registro = 0) THEN
        EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' SE ENCUENTRA TERMINADO, POR FAVOR DESBLOQUEAR PARA CONTINUAR ***';
      ELSE


      /* Valida que el centro exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

      /* Valida que el vendedor exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Empleado(:Vendedor_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Vendedor_) || ' NO EXISTE ***';

      /* Valida que la caja o banco exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Banco(:Banco_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA CAJA o BANCO ' || TRIM(Banco_) || ' NO EXISTE ***';

      /* Valida que la lista de precios exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Lista(:Lista_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA LISTA DE PRECIOS ' || TRIM(Lista_) || ' NO EXISTE ***';

      /* Valida que el usuario exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

      /* Valida que el centro exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';

      /* Valida que la bodega exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Bodega(:Bodega_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA BODEGA ' || TRIM(Bodega_) || ' NO EXISTE ***';

      /* Valida que la referencia exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Referencia(:Referencia_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA REFERENCIA ' || TRIM(Referencia_) || ' NO EXISTE ***';

      V_Renglon = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                   FROM Rdb$Database);

      /* Insertamos TR_INVENTARIO */
      /*Calcular el descuento en base al porc_descuento*/
      V_Descuento = ((:Porc_Descuento_ / 100) * :Unitario_);

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codcentro, Codbodega, Codreferencia, Entrada, Salida,
                                 Unitario, Porcentaje_Descuento, Descuento, Nota, Codusuario)
      VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Centro_, :Bodega_, :Referencia_, :Entrada_, :Salida_,
              :Unitario_, :Porc_Descuento_, :V_Descuento, :Nota_, :Usuario_);

      /* Con base al unitario(con o sin IVA) hallamos el BRUTO y demás cálculos */
      EXECUTE PROCEDURE Fx_Recalcula_Registro(:Tipo_, :Prefijo_, :Numero_, :V_Renglon);
    END
    ELSE
      EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' YA EXISTE ***';
  END
END^



SET TERM ; ^

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_TERMINA_COMPROBANTE (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(10),
    NUMERO_ VARCHAR(10))
AS
DECLARE VARIABLE V_Cantidad INTEGER;
DECLARE VARIABLE V_Generador VARCHAR(20);
DECLARE VARIABLE V_Consecutivo INTEGER;
BEGIN

  /*Se valida si existe el comprobante*/
  SELECT COUNT(1)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Cantidad;

  IF (V_Cantidad = 1) THEN
  BEGIN
    EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:Tipo_, :Prefijo_, :Numero_);
    EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:Tipo_, :Prefijo_, :Numero_);

    IF (:Numero_ < 0) THEN
    BEGIN

      SELECT Generador
      FROM Consecutivos
      WHERE Coddocumento = :Tipo_
            AND Codprefijo = :Prefijo_
      INTO V_Generador;

      EXECUTE STATEMENT 'SELECT GEN_ID(' || :V_Generador || ', 1)
    FROM Rdb$Database '
          INTO V_Consecutivo;

      UPDATE Comprobantes
      SET Numero = :V_Consecutivo
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_;

    END /*DEBE QUEDAR BLOQUEADO, TERMINADO, CON NUMERO CONSECUTIVO (POSITIVO) FE EN PENDIENTES*/

  END

END^



SET TERM ; ^

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_INSERT_CONTABLE_PRIMARIO (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    FECHA_ DATE,
    CUENTA_ VARCHAR(30),
    TERCERO_ VARCHAR(15),
    CENTRO_ VARCHAR(5),
    ACTIVO_ VARCHAR(15),
    EMPLEADO_ VARCHAR(15),
    DEBITO_ NUMERIC(17,4),
    CREDITO_ NUMERIC(17,4),
    BASE_ NUMERIC(17,4),
    NOTA_ VARCHAR(80),
    USUARIO_ VARCHAR(10))
AS
DECLARE VARIABLE V_Renglon INTEGER;
DECLARE VARIABLE V_Registro INTEGER;
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Doc VARCHAR(20);
DECLARE VARIABLE V_Atb_Tercero VARCHAR(1);
DECLARE VARIABLE V_Atb_Centro VARCHAR(1);
DECLARE VARIABLE V_Atb_Activo VARCHAR(1);
DECLARE VARIABLE V_Atb_Empleado VARCHAR(1);
DECLARE VARIABLE V_Validacion VARCHAR(10);
BEGIN

  V_Doc = TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_);

  /* Validamos que no exista el doc */
  SELECT COUNT(1)
  FROM Cocomp
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN

    /* Valida que la cuenta exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Cuenta(:Cuenta_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO EXISTE ***';

    /* Valida que la cuenta sea auxiliar */
    IF ((SELECT Nivel
         FROM Rest_Valida_Cuenta(:Cuenta_)) = 'S') THEN
      EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO PUEDE SER DE NIVEL ***';

    /* Atributos de la cuenta para validaciones de datos */
    SELECT IIF(COALESCE(Tercero, '') = '', 'N', Tercero),
           IIF(COALESCE(Centro, '') = '', 'N', Centro),
           IIF(COALESCE(Activo, '') = '', 'N', Activo),
           IIF(COALESCE(Empleado, '') = '', 'N', Empleado)
    FROM Cuentas
    WHERE Codigo = :Cuenta_
    INTO V_Atb_Tercero,
         V_Atb_Centro,
         V_Atb_Activo,
         V_Atb_Empleado;

    /* Validaciones para tercero */
    IF (V_Atb_Tercero = 'S') THEN
    BEGIN
      IF (TRIM(Tercero_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA TERCERO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el tercero exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Tercero_ = NULL;

    /* Validaciones para centro */
    IF (V_Atb_Centro = 'S') THEN
    BEGIN
      IF (TRIM(Centro_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA CENTRO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el centro exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Centro_ = NULL;

    /* Validaciones para activo */
    IF (V_Atb_Activo = 'S') THEN
    BEGIN
      IF (TRIM(Activo_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA ACTIVO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el activo exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Activo(:Activo_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL ACTIVO ' || TRIM(Activo_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Activo_ = NULL;

    /* Validaciones para empleado */
    IF (V_Atb_Empleado = 'S') THEN
    BEGIN
      IF (TRIM(Empleado_) = '') THEN
        EXCEPTION Rest_Error '*** LA CUENTA MANEJA EMPLEADO, ESTE DATO NO PUEDE ESTAR VACIO ***';
      ELSE
      BEGIN
        /* Valida que el empleado exista */
        IF ((SELECT Registro
             FROM Rest_Valida_Empleado(:Empleado_)) = 0) THEN
          EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Empleado_) || ' NO EXISTE ***';
      END
    END
    ELSE
      Empleado_ = NULL;

    /* Valida que el usuario exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

    /* Insertamos COCOMP */
    INSERT INTO Cocomp (Tipo, Prefijo, Numero, Fecha, Nota, Bloqueado, Codusuario, Codescenario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :Fecha_, '-', 'R', :Usuario_, 'NA');

    V_Renglon = (SELECT GEN_ID(Gen_Comovi, 1)
                 FROM Rdb$Database);

    /*Campo Validacion*/
    SELECT Validacion
    FROM Apo_Valida_Cuentas(:Cuenta_, NULL)
    INTO V_Validacion;

    /* Insertamos COMOVI */
    INSERT INTO Comovi (Tipo, Prefijo, Numero, Renglon, Debito, Credito, Base, Nota, Codcuenta, Codtercero, Codcentro,
                        Codactivo, Codpersonal, Codusuario, Validacion)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Debito_, :Credito_, :Base_, :Nota_, :Cuenta_, :Tercero_, :Centro_,
            :Activo_, :Empleado_, :Usuario_, :V_Validacion);

    /* Se organiza campo VALIDACION en COMOVI */
    --   EXECUTE PROCEDURE Apo_Repara_Validacion_Filtro(:Tipo_, :Prefijo_, :Numero_);
  END
  ELSE
  BEGIN
    /* Validamos la fecha del documento existente.
   Si es la misma del ws continuamos insertando. */
    SELECT Fecha
    FROM Cocomp
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Fecha;

    IF (V_Fecha = Fecha_) THEN
    BEGIN

      /* Validamos que el doc no este bloqueado */
      SELECT COUNT(1)
      FROM Cocomp
      WHERE (Tipo = :Tipo_)
            AND (Prefijo = :Prefijo_)
            AND (Numero = :Numero_)
            AND (Bloqueado = 'R')
      INTO V_Registro;

      IF (V_Registro = 0) THEN
        EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' SE ENCUENTRA TERMINADO, POR FAVOR DESBLOQUEAR PARA CONTINUAR ***';
      ELSE


      /* Valida que la cuenta exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Cuenta(:Cuenta_)) = 0) THEN
        EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO EXISTE ***';

      /* Valida que la cuenta sea auxiliar */
      IF ((SELECT Nivel
           FROM Rest_Valida_Cuenta(:Cuenta_)) = 'S') THEN
        EXCEPTION Rest_Error '*** LA CUENTA ' || TRIM(Cuenta_) || ' NO PUEDE SER DE NIVEL ***';

      /* Atributos de la cuenta para validaciones de datos */
      SELECT IIF(COALESCE(Tercero, '') = '', 'N', Tercero),
             IIF(COALESCE(Centro, '') = '', 'N', Centro),
             IIF(COALESCE(Activo, '') = '', 'N', Activo),
             IIF(COALESCE(Empleado, '') = '', 'N', Empleado)
      FROM Cuentas
      WHERE Codigo = :Cuenta_
      INTO V_Atb_Tercero,
           V_Atb_Centro,
           V_Atb_Activo,
           V_Atb_Empleado;

      /* Validaciones para tercero */
      IF (V_Atb_Tercero = 'S') THEN
      BEGIN
        IF (TRIM(Tercero_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA TERCERO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el tercero exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Tercero_ = NULL;

      /* Validaciones para centro */
      IF (V_Atb_Centro = 'S') THEN
      BEGIN
        IF (TRIM(Centro_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA CENTRO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el centro exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Centro(:Centro_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(Centro_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Centro_ = NULL;

      /* Validaciones para activo */
      IF (V_Atb_Activo = 'S') THEN
      BEGIN
        IF (TRIM(Activo_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA ACTIVO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el activo exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Activo(:Activo_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL ACTIVO ' || TRIM(Activo_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Activo_ = NULL;

      /* Validaciones para empleado */
      IF (V_Atb_Empleado = 'S') THEN
      BEGIN
        IF (TRIM(Empleado_) = '') THEN
          EXCEPTION Rest_Error '*** LA CUENTA MANEJA EMPLEADO, ESTE DATO NO PUEDE ESTAR VACIO ***';
        ELSE
        BEGIN
          /* Valida que el empleado exista */
          IF ((SELECT Registro
               FROM Rest_Valida_Empleado(:Empleado_)) = 0) THEN
            EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Empleado_) || ' NO EXISTE ***';
        END
      END
      ELSE
        Empleado_ = NULL;

      /* Valida que el usuario exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
        EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

      V_Renglon = (SELECT GEN_ID(Gen_Comovi, 1)
                   FROM Rdb$Database);

      /*Campo Validacion*/
      SELECT Validacion
      FROM Apo_Valida_Cuentas(:Cuenta_, NULL)
      INTO V_Validacion;

      /* Insertamos COMOVI */
      INSERT INTO Comovi (Tipo, Prefijo, Numero, Renglon, Debito, Credito, Base, Nota, Codcuenta, Codtercero, Codcentro,
                          Codactivo, Codpersonal, Codusuario, Validacion)
      VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :Debito_, :Credito_, :Base_, :Nota_, :Cuenta_, :Tercero_,
              :Centro_, :Activo_, :Empleado_, :Usuario_, :V_Validacion);

      /* Se organiza campo VALIDACION en COMOVI */
      --EXECUTE PROCEDURE Apo_Repara_Validacion_Filtro(:Tipo_, :Prefijo_, :Numero_);
    END
    ELSE
      EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' YA EXISTE ***';
  END
END^



SET TERM ; ^

/* Mensajes estandar para personalizados */
/* EXCEPTION REST_ERROR '*** EL DOCUMENTO '|| ||' SE ENCUENTRA TERMINADO, POR FAVOR DESBLOQUEAR PARA CONTINUAR ***';*/
/* EXCEPTION REST_ERROR '*** EL DOCUMENTO PRIMARIO ' || || ' NO EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** EL DOCUMENTO PRIMARIO ' || || ' NO TIENE SALDO ***'; */
/* EXCEPTION REST_ERROR '*** EL DOCUMENTO ' || || ' YA EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** EL TERCERO ' || || ' YA EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** LA REFERENCIA ' || || ' YA EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** EL EMPLEADO ' || || ' YA EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** EL DOCUMENTO ' || || ' NO EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** EL TERCERO ' || || ' NO EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** LA REFERENCIA ' || || ' NO EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** EL EMPLEADO ' || || ' NO EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** LA CAJA o BANCO ' || || ' NO EXISTE ***'; */
/* EXCEPTION REST_ERROR '*** LA BODEGA ' || || ' NO EXISTE ***'; */
/* EXCEPTION Rest_Error '*** EL TERCERO ' ||  || ' YA EXISTE ***'; */
/* EXCEPTION Rest_Error '*** EL USUARIO ' ||  || ' NO EXISTE ***'; */
/* EXCEPTION Rest_Error '*** LA LISTA DE PRECIOS ' || || ' NO EXISTE ***'; */
/* EXCEPTION Rest_Error '*** EL CENTRO ' ||  || ' NO EXISTE ***'; */
/* EXCEPTION Rest_Error '*** LA BODEGA ' ||  || ' NO EXISTE ***'; */
/* EXCEPTION Rest_Error '*** LA CUENTA ' ||  || ' NO EXISTE ***'; */
/* EXCEPTION Rest_Error '*** LA CUENTA MANEJA TERCERO, ESTE DATO NO PUEDE ESTAR VACIO ***' ;*/
/* EXCEPTION Rest_Error '*** LA CUENTA MANEJA CENTRO, ESTE DATO NO PUEDE ESTAR VACIO ***'; */


/***** MODULO REST SERVER FINAL                         ****/



/************************************************************/
/**** OTROS                                              ****/
/************************************************************/


SET TERM !;

CREATE OR ALTER PROCEDURE Fe_Pendientes (
    Fecha_  DATE,
    Modulo_ VARCHAR(10))
RETURNS (
    Tipo     CHAR(5),
    Prefijo  CHAR(5),
    Numero   CHAR(10),
    Emisor   CHAR(15),
    Receptor CHAR(15),
    Clase_Fe CHAR(20),
    Fase     INTEGER,
    Fecha    DATE)
AS
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Fecha_9 DATE;
BEGIN

  V_Fecha_9 = CURRENT_DATE - 10;

  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO :Emisor;

  IF (Modulo_ = 'GESTION') THEN
    FOR SELECT Co.Tipo, Co.Prefijo, Co.Numero, Co.Fecha, Co.Codtercero,
               Doc.Codigo_Fe
        FROM Comprobantes Co
        JOIN Documentos Doc ON (Doc.Codigo = Co.Tipo)
        WHERE (Co.Fecha >= :V_Fecha_9)
              AND (Co.Fecha <= :Fecha_)
              AND (Co.Bloqueado = 'S')
              AND (Co.Enviado = 'N')
              AND (Doc.Codigo_Fe <> 'NA')
        ORDER BY Tipo, Prefijo, CAST(Numero AS INTEGER)
        INTO :Tipo, :Prefijo, :Numero, :Fecha, :Receptor, :Clase_Fe

    DO
    BEGIN

      Fase = 2;

      IF (Clase_Fe IN ('NOTA DEBITO', 'NOTA CREDITO')) THEN
      BEGIN

        V_Tipo = NULL;
        V_Prefijo = NULL;
        V_Numero = NULL;

        SELECT Tiporef, Prefijoref, Numeroref
        FROM Notas_Gestion
        WHERE (Tipo = :Tipo)
              AND (Prefijo = :Prefijo)
              AND (Numero = :Numero)
        INTO :V_Tipo, :V_Prefijo, :V_Numero;

        SELECT Fase
        FROM Facturas
        WHERE (Tipo = :V_Tipo)
              AND (Prefijo = :V_Prefijo)
              AND (Numero = :V_Numero)
        INTO :Fase;

        Fase = COALESCE(Fase, 0);

      END

      SUSPEND;

    END

  IF (Modulo_ = 'CONTABLE') THEN
    FOR SELECT Co.Tipo, Co.Prefijo, Co.Numero, Co.Fecha, Co.Codtercero,
               Doc.Codigo_Fe
        FROM Auto_Comp Co
        JOIN Documentos Doc ON (Doc.Codigo = Co.Tipo)
        WHERE (Co.Fecha >= :V_Fecha_9)
              AND (Co.Fecha <= :Fecha_)
              AND (Co.Generado = 'S')
              AND (Co.Enviado = 'N')
              AND (Doc.Codigo_Fe <> 'NA')
        ORDER BY Tipo, Prefijo, CAST(Numero AS INTEGER)
        INTO :Tipo, :Prefijo, :Numero, :Fecha, :Receptor, :Clase_Fe

    DO
    BEGIN

      Fase = 2;

      IF (Clase_Fe IN ('NOTA DEBITO', 'NOTA CREDITO')) THEN
      BEGIN

        V_Tipo = NULL;
        V_Prefijo = NULL;
        V_Numero = NULL;

        SELECT Tiporef, Prefijoref, Numeroref
        FROM Notas_Contable
        WHERE (Tipo = :Tipo)
              AND (Prefijo = :Prefijo)
              AND (Numero = :Numero)
        INTO :V_Tipo, :V_Prefijo, :V_Numero;

        SELECT Fase
        FROM Facturas
        WHERE (Tipo = :V_Tipo)
              AND (Prefijo = :V_Prefijo)
              AND (Numero = :V_Numero)
        INTO :Fase;

        Fase = COALESCE(Fase, 0);

      END

      SUSPEND;

    END

END!

SET TERM;
!

GRANT SELECT ON Terceros TO PROCEDURE Fe_Pendientes;

GRANT SELECT ON Comprobantes TO PROCEDURE Fe_Pendientes;

GRANT SELECT ON Documentos TO PROCEDURE Fe_Pendientes;

GRANT SELECT ON Notas_Gestion TO PROCEDURE Fe_Pendientes;

GRANT SELECT ON Facturas TO PROCEDURE Fe_Pendientes;

GRANT SELECT ON Auto_Comp TO PROCEDURE Fe_Pendientes;

GRANT SELECT ON Notas_Contable TO PROCEDURE Fe_Pendientes;

GRANT EXECUTE    ON PROCEDURE Fe_Pendientes TO Sysdba;

COMMIT WORK;

/* Validacin identidad 48  */
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (3, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,47,48,50,91', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (4, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,47,48,50,91', 'CONTABLE')
                          MATCHING (ID);

UPDATE OR INSERT INTO IDENTIDADES (CODIGO, CODIGO2, NOMBRE, CODIGO_NE, CODIGO_SE, MODIFICADO)
                 VALUES ('48', 'PPT', 'PERMISO POR PROTECCION TEMPORAL', NULL, NULL, '2023-05-08');


COMMIT WORK;

/* FORMATO 1647 EXOGENA*/
UPDATE OR INSERT INTO MM_FORMATOS (CODIGO, NOMBRE, ENTINFO, CPT, TDOC, NID, DV, APL1, APL2, NOM1, NOM2, RAZ, DIR, DPTO, MUN, PAIS, EMAIL, VALOR1, VALOR2, VALOR3, VALOR4, VALOR5, VALOR6, VALOR7, VALOR8, VALOR9, VALOR0, VALOR10, VALOR11, VALOR12, VALOR13, VALOR14, VALOR15, VALOR16, VALOR17, VALOR18, VALOR19, VALOR20, VALOR21, VALOR22, VALOR23, VALOR24, VALOR25, VALOR26, TOTAL, IDEM, TDOCM, NITM, DVM, APL1M, APL2M, NOM1M, NOM2M, RAZM, INFORMAR, CON_INFORMADOS, NELEMENTO, NVALOR1, NVALOR2, NVALOR3, NVALOR4, NVALOR5, NVALOR6, NVALOR7, NVALOR8, NVALOR9, NVALOR0, NVALOR10, NVALOR11, NVALOR12, NVALOR13, NVALOR14, NVALOR15, NVALOR16, NVALOR17, NVALOR18, NVALOR19, NVALOR20, NVALOR21, NVALOR22, NVALOR23, NVALOR24, NVALOR25, NVALOR26, NIDEM, NCPT, REFERENCIA, REVISION, CADENA_ADICIONAL, EQUIVALENCIA, VALOR27, VALOR28, VALOR29, NVALOR27, NVALOR28, NVALOR29)
                           VALUES ('1647_V2', 'INFORMACION DE INGRESOS RECIBIDOS PARA TERCEROS', NULL, 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'S', 'S', 'N', 'S', 'S', 'S', 'S', 'S', 'N', 'N', 'ingresos', 'vtotal', 'ving', 'vret', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'con', 'INFORMACION DE INGRESOS RECIBIDOS PARA TERCEROS', 2, 'paist="169"', 'tdocm=tdoc2,nitm=nid2i,dvm=dv2i,razm=razi, apl1m=apl1i,apl2m=apl2i,nom1m=nom1i,nom2m=nom2i,dpto=cdpt,mun=cmcp', NULL, NULL, NULL, NULL, NULL, NULL)
                         MATCHING (CODIGO);

COMMIT WORK;


UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('2', 'Anulacion del Documento Soporte en Adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('3', 'Rebaja o descuento parcial o total')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('4', 'Ajuste de precio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('5', 'Otros')
                      MATCHING (CODIGO);


COMMIT WORK;

-----------
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Conteo_Fisico_P (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10),
    Usuario_ VARCHAR(10))
AS
DECLARE VARIABLE V_Nit          VARCHAR(15);
DECLARE VARIABLE V_Referencia   VARCHAR(20);
DECLARE VARIABLE V_Bodega       VARCHAR(5);
DECLARE VARIABLE V_Categoria    VARCHAR(15);
DECLARE VARIABLE V_Lote         VARCHAR(20);
DECLARE VARIABLE V_Ponderado    DOUBLE PRECISION;
DECLARE VARIABLE V_Diferencia   DOUBLE PRECISION;
DECLARE VARIABLE V_Renglon      INTEGER;
DECLARE VARIABLE V_Tipo_Entrada VARCHAR(5) = 'AE1';
DECLARE VARIABLE V_Tipo_Salida  VARCHAR(5) = 'AS1';
DECLARE VARIABLE V_Prefijo      VARCHAR(5) = 'AJUST';
DECLARE VARIABLE V_Registro     INTEGER;
DECLARE VARIABLE Fecha_Corte    DATE;
BEGIN

  --Fecha Corte
  SELECT Fecha
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO :Fecha_Corte;

  /* Insertar prefijo tipo AJUST para evitar primary_key */
  SELECT COUNT(1)
  FROM Prefijos
  WHERE Codigo = 'AJUST'
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Prefijos (Codigo, Nombre)
    VALUES ('AJUST', 'AJUSTE DE INVENTARIO');

  /* Insertar lista de precios NA */
  SELECT COUNT(1)
  FROM Listas
  WHERE Codigo = 'NA'
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Listas (Codigo, Nombre)
    VALUES ('NA', 'NO APLICA');

  /* Seleccionar el nit del tercero empresa para asignar como tercero en los comprobantes */
  SELECT TRIM(Codigo)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Nit;

  /* Insertar Tipos de Documento AE1 y AS1 si no existen */
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = :V_Tipo_Entrada
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('AE1', 'AJUSTE DE ENTRADA AL INVENTARIO FISICO', NULL,
            'Si valoriza y no contabiliza - Ej: sirve para resetar ponderado con cantidad 0',
            NULL, 'FORAE1CAR101.FR3', 'FORAE1MEO101.FR3', 'FORAE1MEC101.FR3',
            'FORCONTABLE100.fr3', 'S', 'N', 'N', 'N', 'DEBITO', 'AJUSTES', 'N',
            'N', 'NO', 'ENTRADA', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'S', 'S', 'S', 'N', 'S', 'N', NULL, NULL, NULL, NULL, 'N',
            'N', 0, 365, 'N', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N', 'N',
            '', 'S', 'N', 'N', NULL, 'NA', NULL, 'N', 'N', 'N', 'N', 'S');

  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = :V_Tipo_Salida
  INTO V_Registro;

  IF (V_Registro = 0) THEN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('AS1', 'AJUSTE DE SALIDA DEL INVENTARIO FISICO', NULL,
            'Si contabiliza seg�n esquema contable grupo Ajuste (concepto Juego). Atributo contabiliza debe estar inactivo, de lo contrario, quedar�a doble',
            NULL, 'FORAS1CAR101.FR3', 'FORAS1MEC101.FR3', 'FORAS1MEO101.FR3',
            'FORCONTABLE100.fr3', 'S', 'N', 'N', 'N', 'CREDITO', 'AJUSTES', 'N',
            'N', 'NO', 'SALIDA', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'S', 'S', 'N', 'S', 'S', 'N', NULL, NULL, NULL, NULL, 'N',
            'N', 0, 365, 'N', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N', 'N',
            NULL, 'S', 'N', 'N', NULL, 'NA', '2020-05-15', 'N', 'N', 'N', 'N',
            'S');

  /* Insertar en comprobantes el doc AE1 */
  INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Codescenario, Fecha, Vence,
                            Codlista, Nota, Bloqueado, Codtercero, Codvendedor,
                            Codusuario)
  VALUES (TRIM(:V_Tipo_Entrada), TRIM(:V_Prefijo), TRIM(:Numero_), 'NA',
          :Fecha_Corte, :Fecha_Corte, 'NA', 'AJUSTE INVENTARIO CONTEO FISICO',
          'R', TRIM(:V_Nit), 'NULO', TRIM(:Usuario_));

  /* Insertar en comprobantes el doc AS1 */
  INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Codescenario, Fecha, Vence,
                            Codlista, Nota, Bloqueado, Codtercero, Codvendedor,
                            Codusuario)
  VALUES (TRIM(:V_Tipo_Salida), TRIM(:V_Prefijo), TRIM(:Numero_), 'NA',
          :Fecha_Corte, :Fecha_Corte, 'NA', 'AJUSTE INVENTARIO CONTEO FISICO',
          'R', TRIM(:V_Nit), 'NULO', TRIM(:Usuario_));

  /* Referencias a insertar en los ajustes */
  FOR SELECT Referencia, Bodega, Categoria, Lote, Ponderado, Diferencia
      FROM Pz_Sel_Conteo_Fisico_P(:Tipo_, :Prefijo_, :Numero_)
      WHERE Diferencia <> 0
      INTO V_Referencia, V_Bodega, V_Categoria, V_Lote, V_Ponderado,
           V_Diferencia
  DO
  BEGIN
    V_Renglon = (SELECT (GEN_ID(Gen_Tr_Inventario, 1))
                 FROM Rdb$Database);

    /* Validacion de la diferencia para saber el tipo de inserccion a realizar si es de entrada o salida
       Si el saldo es negativo = entrada
       Si el saldo es positivo = salida */
    IF (V_Diferencia < 0) THEN
    BEGIN
      V_Diferencia = V_Diferencia * -1;
      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codbodega,
                                 Codcentro, Codreferencia, Entrada, Bruto,
                                 Unitario, Codusuario, Nota)
      VALUES (TRIM(:V_Tipo_Entrada), TRIM(:V_Prefijo), TRIM(:Numero_),
              :V_Renglon, TRIM(:V_Bodega), 'NA', TRIM(:V_Referencia),
              :V_Diferencia, :V_Ponderado, :V_Ponderado, :Usuario_,
              'AJUSTE AUTOMATICO CONTEO FISICO');
    END
    ELSE
    BEGIN
      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codbodega,
                                 Codcentro, Codreferencia, Salida, Bruto,
                                 Unitario, Codusuario, Nota)
      VALUES (TRIM(:V_Tipo_Salida), TRIM(:V_Prefijo), TRIM(:Numero_),
              :V_Renglon, TRIM(:V_Bodega), 'NA', TRIM(:V_Referencia),
              :V_Diferencia, :V_Ponderado, :V_Ponderado, :Usuario_,
              'AJUSTE AUTOMATICO CONTEO FISICO');
    END
  END
  /* Ejecuccion para organizar validacion en tr_inventario */
  EXECUTE PROCEDURE Fx_Repara_Validacion('MEKANO_2012');

  /* Terminar documentos */
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_Entrada, :V_Prefijo,
      :Numero_);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_Entrada, :V_Prefijo,
      :Numero_);
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:V_Tipo_Salida, :V_Prefijo,
      :Numero_);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:V_Tipo_Salida, :V_Prefijo,
      :Numero_);
END^

SET TERM ; ^

----
UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('SF1_TOTAL', 'AJUSTE DE INVENTARIO TOTAL SEGUN DOCUMENTO SF1', 'Ajuste de Inventario segun documento SF1. Recuerde sacar informe DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO para validar diferencias', 'EXECUTE PROCEDURE Pz_Conteo_Fisico(:Fecha_Corte,:A_Tipo,:B_Prefijo,:C_Numero,:Usuario);', 'S', 'GESTION', 'CIERRE INVENTARIO', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('SF1_PARCIA', 'AJUSTE DE INVENTARIO PARCIAL SEGUN DOCUMENTO SF1 (Solo referencias digitadas)', 'Ajuste de Inventario segun documento SF1. Recuerde sacar informe DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO PARCIAL', 'EXECUTE PROCEDURE Pz_Conteo_Fisico_P(:A_Tipo,:B_Prefijo,:C_Numero, :Usuario);', 'S', 'GESTION', 'CIERRE INVENTARIO', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

---------
-- Crear expresiones para Retroactivo para prima y cesantias

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('TOTA_SAREP', 'TOTAL RETROACTIVO SALARIAL PARA PRIMA', '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha > :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('TOTA_SAREC', 'TOTAL RETROACTIVO SALARIAL PARA CESANTIAS', '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha > :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;

-- Actualizar formula de promedio para prima

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DI015+SI050))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE   
  RESULT:=(ROUND((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DI015+SI050))*30))'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualizar formula de promedio para cesantias

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DCES+DI015+SI051)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DCES+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALA-TOTA_CONS+TOTA_SAREP)+(SA027-SA001+DV300)+SI100)/(TOTA_DCES+DI015+SI051))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE 
  RESULT:=(ROUND((((TOTA_SALA-TOTA_CONS+TOTA_SAREP)+(SA027-SA001+DV300)+SI100)/(TOTA_DCES+DI015+SI051))*30))'
WHERE (CODIGO = 'LD107') AND
      (NATIVO = 'S');

COMMIT WORK;
-------
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CTP070110', 'CONTRATOS EMPLEADOS', 'Contratos de empleados', 'SELECT (SELECT TRIM(Nombre_Empleado)
        FROM Fn_Nombre_Empleado(C.Codpersonal)) AS "NOMBRE EMPLEADO",
        C.Codpersonal Codigo,
        Inicio,
        Fin,
        Codtipocontrato Tipo,
        T.Nombre AS Contrato,
        Descripcion,
        Activo
FROM Contratacion C
JOIN Tipocontratos T ON (T.Codigo = C.Codtipocontrato)
ORDER BY 1', 'S', 'NOMINA', 'N', 'PERSONAL NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

-----
-- IVA EN DSE
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)),
V_Pais
AS (SELECT "noov:Nvpro_pais" AS Pais
    FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       IIF(Pais = ''CO'', 0, "noov:Nvdet_piva") "noov:Nvdet_piva",
       IIF(Pais = ''CO'', 0,
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2))) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
JOIN V_Pais ON (1 = 1)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE  "noov:Nvimp_cdia" NOT IN (''07'', ''99'',''00'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)),
V_Pais
AS (SELECT "noov:Nvpro_pais" AS Pais
    FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       IIF(Pais = ''CO'', 0, "noov:Nvdet_piva") "noov:Nvdet_piva",
       IIF(Pais = ''CO'', 0,
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2))) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
JOIN V_Pais  ON (1 = 1)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE  "noov:Nvimp_cdia" NOT IN (''07'', ''99'',''00'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;
-----------
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (7, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Impuestos I
JOIN Tipoimpuestos T ON (I.Tipo_Impuesto = T.Codigo)
WHERE TRIM(I.Tipo) = '':TIPO''
      AND TRIM(I.Prefijo) = '':PREFIJO''
      AND TRIM(I.Numero) = '':NUMERO''', '00,01,02,03,04,21,22,23,24,25,26,ZY,ZZ,99', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (8, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Impuestos('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tipoimpuestos T ON (T.Codigo = I.Codtipoimpuesto)', '00,01,02,03,04,21,22,23,24,25,26,ZY,ZZ,99', 'CONTABLE')
                          MATCHING (ID);


COMMIT WORK;


/******************************************************************************/
/***                           Stored procedures REALTUR                    ***/
/******************************************************************************/

SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_REALTUR
AS
DECLARE VARIABLE V_Tipo VARCHAR(5);
DECLARE VARIABLE V_Prefijo VARCHAR(5);
DECLARE VARIABLE V_Numero VARCHAR(10);
BEGIN

 /* Documentos sin ejecutar proceso */
 FOR SELECT Tipo,
            Prefijo,
            Numero
     FROM Comprobantes
     WHERE (Tipo = 'FE1')
           AND (Archivar = 'N')
     INTO :V_Tipo,
          :V_Prefijo,
          :V_Numero
 DO
 BEGIN
  /* Update y delete que se ejecutaban en el formato(F6), mas sp
  de terminar documento(F7) */
  EXECUTE PROCEDURE Pz_Realtur_1(:V_Tipo, :V_Prefijo, :V_Numero);

 END
END^


CREATE OR ALTER PROCEDURE PZ_REALTUR_1 (
    TIPO VARCHAR(5),
    PREFIJO VARCHAR(5),
    NUMERO VARCHAR(10))
AS
DECLARE VARIABLE V_Centro VARCHAR(5);
DECLARE VARIABLE V_Tercero VARCHAR(15);
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Consecutivo INTEGER;
DECLARE VARIABLE V_Generador VARCHAR(20);
DECLARE VARIABLE V_Numero_Positivo VARCHAR(10);
BEGIN

  /* F6 */
  /* Asigna retención manual */
  UPDATE Comprobantes C
  SET C.Retencion_Manual = 'S'
  WHERE (SELECT COUNT(*)
         FROM (SELECT R.Tipo_Retencion,
                      D.Base Base_Real,
                      SUM(R.Base) Base
               FROM Reg_Retenciones R
               JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
               JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
               JOIN Tiporetenciones T1 ON (T1.Codigo = 'BASE')
               LEFT JOIN Data_Retenciones D ON (T1.Codigo = D.Codtiporetencion AND
                     D.Ano = EXTRACT(YEAR FROM C.Fecha))
               WHERE Tipo = :Tipo
                     AND Prefijo = :Prefijo
                     AND Numero = :Numero
                     AND T.Codliga = 'RETECXP'
                     AND T.Clase = 'RETEFUENTE'
               GROUP BY 1, 2
               HAVING SUM(R.Base) < D.Base)) > 0
        AND C.Tipo = :Tipo
        AND C.Prefijo = :Prefijo
        AND C.Numero = :Numero;

  /* Elimina las retenciones */
  DELETE FROM Reg_Retenciones R1
  WHERE R1.Tipo_Retencion IN (SELECT Codigo
                              FROM Tiporetenciones
                              WHERE Clase = 'RETEFUENTE'
                                    AND Codliga = 'RETECXP')
        AND (SELECT COUNT(*)
             FROM (SELECT T.Codliga,
                          T.Clase,
                          D.Base Base_Real,
                          SUM(R.Base) Base
                   FROM Reg_Retenciones R
                   JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
                   JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
                   JOIN Tiporetenciones T1 ON (T1.Codigo = 'BASE')
                   LEFT JOIN Data_Retenciones D ON (T1.Codigo = D.Codtiporetencion AND
                         D.Ano = EXTRACT(YEAR FROM C.Fecha))
                   WHERE Tipo = :Tipo
                         AND Prefijo = :Prefijo
                         AND Numero = :Numero
                         AND T.Codliga = 'RETECXP'
                         AND T.Clase = 'RETEFUENTE'
                   GROUP BY 1, 2, 3
                   HAVING SUM(R.Base) < D.Base)) > 0
        AND R1.Tipo = :Tipo
        AND R1.Prefijo = :Prefijo
        AND R1.Numero = :Numero;

  SELECT Fecha
  FROM Comprobantes
  WHERE Tipo = :Tipo
        AND Prefijo = :Prefijo
        AND Numero = :Numero
  INTO V_Fecha;

  /* F7 */
  EXECUTE PROCEDURE Fx_Recalcula_Comprobante(:Tipo, :Prefijo, :Numero);
  EXECUTE PROCEDURE Fx_Perfecciona_En_Batch(:Tipo, :Prefijo, :Numero);

  V_Numero_Positivo = :Numero;

  IF (:Numero < 0) THEN
  BEGIN

    SELECT Generador
    FROM Consecutivos
    WHERE Coddocumento = :Tipo
          AND Codprefijo = :Prefijo
          AND :V_Fecha BETWEEN Fecha AND Vence
    INTO V_Generador;

    EXECUTE STATEMENT 'SELECT GEN_ID(' || :V_Generador || ', 1)
    FROM Rdb$Database '
        INTO V_Consecutivo;

    UPDATE Comprobantes
    SET Numero = :V_Consecutivo
    WHERE Tipo = :Tipo
          AND Prefijo = :Prefijo
          AND Numero = :Numero;

    V_Numero_Positivo = :V_Consecutivo;

  END

  /* Proceso PER001:
  Cambia tercero en contabilidad por tercero del centro de costos */
  FOR SELECT DISTINCT T.Codcentro,
                      C1.Codtercero,
                      C.Fecha
      FROM Comprobantes C
      JOIN Tr_Inventario T USING (Tipo, Prefijo, Numero)
      JOIN Referencias R ON (T.Codreferencia = R.Codigo)
      JOIN Centros C1 ON (T.Codcentro = C1.Codigo)
      WHERE Tipo = :Tipo
            AND Prefijo = :Prefijo
            AND Numero = :V_Numero_Positivo
            AND R.Codlinea = 'CXP'
            AND C1.Codtercero <> C.Codtercero
      INTO V_Centro,
           V_Tercero,
           V_Fecha
  DO
  BEGIN
    UPDATE Reg_Contable R
    SET R.Codtercero = :V_Tercero
    WHERE R.Tipo = :Tipo
          AND R.Prefijo = :Prefijo
          AND R.Numero = :V_Numero_Positivo
          AND R.Codcentro = :V_Centro
          AND R.Codcuenta IN (SELECT E.Cta_Credito
                              FROM Esquema_Contable E
                              WHERE Codesquema IN (SELECT R.Cod_Esqcontable
                                                   FROM Referencias R
                                                   WHERE Codlinea = 'CXP')
                                    AND E.Grupo = 'VENTA'
                                    AND E.Concepto = 'PARTIDA');

    UPDATE Reg_Contable R
    SET R.Codtercero = :V_Tercero
    WHERE R.Tipo = :Tipo
          AND R.Prefijo = :Prefijo
          AND R.Numero = :V_Numero_Positivo
          AND R.Codcentro = :V_Centro
          AND R.Codcuenta IN (SELECT T.Coddebito
                              FROM Tiporetenciones T
                              WHERE T.Codliga = 'RETECXP');

    UPDATE Reg_Contable R
    SET R.Codtercero = :V_Tercero
    WHERE R.Tipo = :Tipo
          AND R.Prefijo = :Prefijo
          AND R.Numero = :V_Numero_Positivo
          AND R.Codcentro = :V_Centro
          AND R.Codcuenta IN (SELECT T.Codcredito
                              FROM Tiporetenciones T
                              WHERE T.Codliga = 'RETECXP');

    UPDATE OR INSERT INTO Todo_Dias (Fecha)
    VALUES (:V_Fecha);
  END

  /* Proceso de automator, generacion cuentas de cobros */
  EXECUTE PROCEDURE Pz_Realtur_2(:Tipo, :Prefijo, :V_Numero_Positivo);

  /* Bandera en el documento para no ejecutarlo de nuevo */
  UPDATE Comprobantes
  SET Archivar = 'S'
  WHERE Tipo = :Tipo
        AND Prefijo = :Prefijo
        AND Numero = :V_Numero_Positivo;

END^


CREATE OR ALTER PROCEDURE Pz_Realtur_2 (
    Tipo    VARCHAR(5),
    Prefijo VARCHAR(5),
    Numero  VARCHAR(10))
AS
DECLARE VARIABLE V_Fecha       DATE;
DECLARE VARIABLE V_Valor       NUMERIC(17,4);
DECLARE VARIABLE V_Nota        VARCHAR(200);
DECLARE VARIABLE V_Tercero     VARCHAR(15);
DECLARE VARIABLE V_Usuario     VARCHAR(10);
DECLARE VARIABLE V_Centro      VARCHAR(5);
DECLARE VARIABLE V_Renglon     INTEGER;
DECLARE VARIABLE V_Retefuente  NUMERIC(17,4);
DECLARE VARIABLE V_Reteica     NUMERIC(17,4);
DECLARE VARIABLE V_Insertar    INTEGER;
DECLARE VARIABLE V_Nom_Centro  VARCHAR(80);
DECLARE VARIABLE V_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Nota_C      VARCHAR(80);
DECLARE VARIABLE V_Renglon_New INTEGER;
DECLARE VARIABLE V_Unitario    NUMERIC(17,4);
BEGIN

  /* Select principal */
  FOR SELECT C.Fecha,
             C1.Codtercero,
             C.Codusuario,
             T.Codcentro,
             C.Nota,
             SUM(T.Salida * T.Unitario)
      FROM Comprobantes C
      JOIN Tr_Inventario T USING (Tipo, Prefijo, Numero)
      JOIN Referencias R ON (R.Codigo = T.Codreferencia)
      JOIN Centros C1 ON (C1.Codigo = T.Codcentro)
      WHERE (C.Tipo = :Tipo)
            AND (C.Prefijo = :Prefijo)
            AND (C.Numero = :Numero)
            AND (R.Codlinea = 'CXP')
      GROUP BY 1, 2, 3, 4, 5
      INTO V_Fecha,
           V_Tercero,
           V_Usuario,
           V_Centro,
           V_Nota,
           V_Valor
  DO
  BEGIN

    /* Retefuente */
    SELECT SUM(Rc.Debito) AS Valor
    FROM Reg_Retenciones Rc
    JOIN Tiporetenciones Tr ON (Tr.Codigo = Rc.Tipo_Retencion)
    WHERE (Rc.Tipo = :Tipo)
          AND (Rc.Prefijo = :Prefijo)
          AND (Rc.Numero = :Numero)
          AND (Rc.Centro = :V_Centro)
          AND Tr.Codliga = 'RETECXP'
          AND Tr.Clase = 'RETEFUENTE'
    INTO V_Retefuente;

    /* Reteica */
    SELECT SUM(Rc.Debito) AS Valor
    FROM Reg_Retenciones Rc
    JOIN Tiporetenciones Tr ON (Tr.Codigo = Rc.Tipo_Retencion)
    WHERE (Rc.Tipo = :Tipo)
          AND (Rc.Prefijo = :Prefijo)
          AND (Rc.Numero = :Numero)
          AND (Rc.Centro = :V_Centro)
          AND (Tr.Codliga = 'RETECXP')
          AND (Tr.Clase = 'RETEICA')
    INTO V_Reteica;

    /* Se valida que no exista CXP */
    SELECT COUNT(C.Tipo)
    FROM Tr_Inventario C
    JOIN Comprobantes C1 USING (Tipo, Prefijo, Numero)
    WHERE (C.Tiporef = :Tipo)
          AND (C.Prefijoref = :Prefijo)
          AND (C.Numeroref = :Numero)
          AND (C.Tipo = 'CXP')
          AND (C1.Codtercero = :V_Tercero)
          AND (C.Codcentro = :V_Centro)
    INTO V_Insertar;

    IF (V_Insertar = 0) THEN
    BEGIN

      V_Nota_C = SUBSTRING(:V_Nota FROM 1 FOR 80);

      V_Renglon_New = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                       FROM Rdb$Database);

      V_Unitario = ROUND(COALESCE(:V_Valor, 0) - COALESCE(:V_Reteica, 0) - COALESCE(:V_Retefuente, 0));

      /* Consultar el centro para crearlo como prefijo */
      SELECT SUBSTRING(TRIM(Nombre) FROM 1 FOR 65) || ' ' || TRIM(Codtercero)
      FROM Centros
      WHERE (Codigo = :V_Centro)
      INTO V_Nom_Centro;

      UPDATE OR INSERT INTO Prefijos (Codigo, Nombre)
      VALUES (:V_Centro, :V_Nom_Centro);

      /* Consultar vendedor en valores x defecto */
      SELECT Codvendedor
      FROM Val_Documentos
      WHERE Coddocumento = 'CXP'
      INTO V_Vendedor;

      /* Si vendedor IS NULL se inserta sin este en comprobantes */
      IF (V_Vendedor IS NULL) THEN
        INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Codtercero, Nota, Bloqueado, Codusuario, Codescenario, Codlista, Vence)
        VALUES ('CXP', :V_Centro, :Numero, :V_Fecha, :V_Tercero, :V_Nota_C, 'N', :V_Usuario, 'NA', 'NA', :V_Fecha);
      ELSE
        INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Codtercero, Nota, Bloqueado, Codusuario, Codescenario, Codlista, Vence, Codvendedor)
        VALUES ('CXP', :V_Centro, :Numero, :V_Fecha, :V_Tercero, :V_Nota_C, 'N', :V_Usuario, 'NA', 'NA', :V_Fecha, :V_Vendedor);

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Tiporef, Prefijoref, Numeroref, Codcentro, Codbodega, Codreferencia, Entrada,
                                 Unitario, Bruto, Nota, Codusuario)
      VALUES ('CXP', :V_Centro, :Numero, :V_Renglon_New, :Tipo, :Prefijo, :Numero, :V_Centro, 'BG', 'CXP', 1, :V_Unitario, :V_Unitario, :V_Nota,
              :V_Usuario);

      EXECUTE PROCEDURE Fx_Recalcula_Comprobante('CXP', :V_Centro, :Numero);
      EXECUTE PROCEDURE Fx_Perfecciona_En_Batch('CXP', :V_Centro, :Numero);

    END

  END
END^



SET TERM ; ^

COMMIT WORK;
------------------------------
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Trm (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Codigo CHAR(5),
    Valor  DOUBLE PRECISION,
    Fecha  CHAR(10))
AS
DECLARE VARIABLE V_Docu    CHAR(20);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Trm     DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Lista   CHAR(5);
BEGIN
  -- Se valida el tipo de documento
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

  SELECT Trm,
         Fecha,
         Codlista
  FROM Comprobantes
  WHERE Tipo = :V_Tipo
        AND Prefijo = :V_Prefijo
        AND Numero = :V_Numero
  INTO V_Trm,
       V_Fecha,
       V_Lista;

  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista
  INTO Codigo;

  IF (TRIM(COALESCE(Codigo, '')) = '') THEN
    Codigo = 'COP';

  Valor = 1;
  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Valor > 0
        AND Codmoneda = :Codigo
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO Valor;

  Valor = COALESCE(Valor, 1);

  IF (V_Trm > 0) THEN
    Valor = V_Trm;

  IF (Codigo = 'COP') THEN
    Valor = 1;

  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^

SET TERM ; ^
COMMIT WORK;

-- SC_NOMINA074
-- Expresion del SA011 para incluir a fin de mes en descuentos
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_VAS', 'VALOR VACACIONES SA011 EN LA NOMINA EN EL MISMO MES - SA011', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''SA011'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;


-- Incluir el SA011 en el DT035

UPDATE RUBROS SET 
    FORMULA = 'IF(QUINCENA<2) THEN
RESULT:=(0)
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
RESULT:=(0)
ELSE
IF((ES_INTEGRA=0) AND ((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL+DV300)>(25*SMLV))) THEN
RESULT:=(ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG)+VA080+SA011+ANTE_VAS+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100))
ELSE
IF((ES_INTEGRA=1) AND (((((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL))*C05_S_INT)+HE038+DV997+DV300)>(25*SMLV))) THEN
RESULT:=(ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG)+VA080+SA011+ANTE_VAS+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100))
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=(ROUND((((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG))*C05_S_INT)+SA011+ANTE_VAS+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100))
ELSE
IF((SA027+ANTE_SAL)>=SMLV*4) THEN
RESULT:=(ROUND(((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG))+SA011+ANTE_VAS+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100))
ELSE
RESULT:=(0)'
WHERE (CODIGO = 'DT035') AND
      (NATIVO = 'S');



COMMIT WORK;

-- SC_NOMINA075
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DIVAN', 'DIAS VACACIONES EN NOMINA MISMO PERIODO O MES - DI011', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI011'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;

-- SC_NOMINA076.sql
-- Se crean rubros PR04 para ajustar provisiones prima cesantias e intereses
EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR060' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR040'', ''AJUSTE VALOR PROVISION CESANTIAS'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR061' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR041'', ''AJUSTE VALOR PROVISION PRIMA'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR062' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR042'', ''AJUSTE VALOR PROVISION INTERES A LAS CESANTIAS'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

-- Se actualizan rubros provisiones incluyendo los ajuestes

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_INTEGRA=1 OR ES_ARL=1) THEN
  RESULT:=(0)
ELSE 
  RESULT:=(ROUND(SA028/C22_PR_CES)+PR040)'
WHERE (CODIGO = 'PR060') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_INTEGRA=1 OR ES_ARL=1) THEN
 RESULT:=(0)
ELSE
 RESULT:=(ROUND(((SAL_BASICO/30*(DI001+DI004+DI012))+SA028)/C21_PR_PRI)+PR041)'
WHERE (CODIGO = 'PR061') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'ROUND(PR060*C23_PR_INT)+PR042'
WHERE (CODIGO = 'PR062') AND
      (NATIVO = 'S');

COMMIT WORK;

-- Se incluyen los rubros PR04 en los esquemas que estan las provisiones

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR060' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR040'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR061' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR041'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR062' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR042'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

-- Actualiza rubro LD102 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(TOTA_HE_RE+HE038)
ELSE 
  RESULT:=(ROUND(((TOTA_HE_RE+HE038+SI102)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD102') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualiza rubro LD104 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(TOTA_SAPR+DV997)
ELSE 
  RESULT:=(ROUND(((TOTA_SAPR+DV997+SI104)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD104') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualiza rubro LD106 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE   
  RESULT:=(ROUND((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');



COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Detalle (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10),
    Renglon_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Ano           INTEGER;
DECLARE VARIABLE V_Cantidad      DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto         DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento     DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu           DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa        DOUBLE PRECISION;
DECLARE VARIABLE V_Base          DOUBLE PRECISION;
DECLARE VARIABLE V_Valor         DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
DECLARE VARIABLE V_Es_Tarifa     CHAR(1);
DECLARE VARIABLE V_Valor_Nominal DOUBLE PRECISION;
BEGIN
  /* IMPUESTOS */
  "noov:Nvimp_oper" = 'S';

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  FOR SELECT Cantidad,
             Bruto,
             Descuento,
             Aiu
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Descuento,
           :V_Aiu
  DO
  BEGIN

    V_Cantidad = COALESCE(V_Cantidad, 0);
    V_Bruto = COALESCE(V_Bruto, 0);
    V_Descuento = COALESCE(V_Descuento, 0);
    V_Aiu = COALESCE(V_Aiu, 0);

    IF (V_Aiu = 0) THEN
      V_Aiu = 100;

    V_Tipo_Impuesto = NULL;
    V_Es_Tarifa = NULL;

    FOR SELECT SKIP 1 Tipo_Impuesto,
                      Es_Tarifa,
                      SUM(Debito + Credito)
        FROM Reg_Impuestos
        WHERE Tipo = :Tipo_
              AND Prefijo = :Prefijo_
              AND Numero = :Numero_
              AND Renglon = :Renglon_
        GROUP BY Codigo_Fe, Tipo_Impuesto, Es_Tarifa
        INTO :V_Tipo_Impuesto,
             :V_Es_Tarifa,
             :V_Valor_Nominal
    DO
    BEGIN
      V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
      V_Tarifa = NULL;

      SELECT Tarifa
      FROM Data_Impuestos
      WHERE Codtipoimpuesto = :V_Tipo_Impuesto
            AND Ano = :V_Ano
      INTO :V_Tarifa;

      V_Tarifa = COALESCE(V_Tarifa, 0);
      -- V_Tarifa = CAST(V_Tarifa AS NUMERIC(17,4));

      V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento;
      -- v_base = v_cantidad * (v_bruto - v_descuento) * v_aiu / 100;

      IF (V_Es_Tarifa = 'S') THEN
        V_Valor = V_Base * V_Tarifa / 100;

      IF (V_Es_Tarifa = 'N') THEN
      BEGIN
        IF (V_Cantidad = 0) THEN
          V_Tarifa = 0;
        ELSE
          V_Tarifa = V_Valor_Nominal / V_Cantidad;
        V_Valor = V_Valor_Nominal;
      END

      SELECT Codigo_Fe,
             SUBSTRING(Nombre FROM 1 FOR 40)
      FROM Tipoimpuestos
      WHERE Codigo = :V_Tipo_Impuesto
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc";

      IF ("noov:Nvimp_cdia" = '22') THEN
        V_Base = 0;

      "noov:Nvimp_desc" = CASE
                            WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                            WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                            WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                            WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                            WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                            ELSE "noov:Nvimp_desc"
                          END;

      "noov:Nvimp_base" = V_Base;
      "noov:Nvimp_porc" = V_Tarifa;
      "noov:Nvimp_valo" = V_Valor;

      IF ("noov:Nvimp_cdia" NOT IN ('22', '99') AND
          NOT("noov:Nvimp_cdia" = '04' AND
          V_Tarifa = 0)) THEN
        SUSPEND;

    END

  END

END^

SET TERM ; ^

COMMIT WORK;

-- Tasa Alterna
/* EXPORTADOS GESTION */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVA';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--952 Gestion 25/JUL/2023 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            ges:sector_salud
            ges:tasa_cambio
            ges:tasa_alterna
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 952 19/MAY/2023 v1

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
      (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
WHERE "noov:Nvfor_oper" = ''SS-CUFE''', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
      (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM pz_fe_impuestos_sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
WHERE "noov:Nvfor_oper" = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)
JOIN V_Trm ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm , 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm , 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm , 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot"/Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti"/Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota"/Trm, 2)) "noov:Nvfac_tota",
       IIF("noov:Nvfac_tipo" IN (''DS'', ''CS''),
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp"/Trm, 2)),
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total/Trm, 2))) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo"/Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red/Trm, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base"/Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo"/Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_COMPROBANTES (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(20),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20),
    COM_TOTAL DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_E (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    REFERENCIA CHAR(20),
    NOM_REFERENCIA CHAR(80),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    UNITARIO DOUBLE PRECISION,
    PORC_DESCUENTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    AIU DOUBLE PRECISION,
    VALOR_AIU DOUBLE PRECISION,
    STOT DOUBLE PRECISION,
    NOTA CHAR(200),
    MEDIDA CHAR(5),
    LINEA CHAR(5),
    DSI CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_RENGLON (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM_E (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^

CREATE OR ALTER PROCEDURE PZ_FE_ROUND (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    TRM_ DOUBLE PRECISION)
RETURNS (
    VALOR DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_COMPROBANTES (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(20),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20),
    COM_TOTAL DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Caiu            INTEGER;
DECLARE VARIABLE V_Dsi             INTEGER;
DECLARE VARIABLE V_Mand            INTEGER;
DECLARE VARIABLE V_Cod_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Tesoreria       VARCHAR(10);
DECLARE VARIABLE V_Formapago       VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe    VARCHAR(5);
DECLARE VARIABLE V_Codbanco        VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe     VARCHAR(5);
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Codsociedad     VARCHAR(5);
DECLARE VARIABLE V_Email_Ter       VARCHAR(50);
DECLARE VARIABLE V_Codsucursal     VARCHAR(15);
DECLARE VARIABLE V_Email_Suc       VARCHAR(50);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Copag           INTEGER;
DECLARE VARIABLE V_Copago          DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Municipio       VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref        VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref     VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref      VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac       VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref        VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Stot_Red        DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det   DOUBLE PRECISION;
DECLARE VARIABLE V_Siif            VARCHAR(80);
DECLARE VARIABLE V_Fecre           INTEGER;
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  -- Si es NC o ND se toman datos del doc referencia
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tipo_Ref,
       V_Prefijo_Ref,
       V_Numero_Ref;

  V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
  V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
  V_Numero_Ref = COALESCE(V_Numero_Ref, '');

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Comprobantes
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Fecha_Ref;
  -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

  SELECT Cufe,
         EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Facturas
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Cufe_Ref,
       V_Fecha_Fac;
  V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
  V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tipo_Ref
  INTO V_Docu_Ref;
  V_Docu_Ref = COALESCE(V_Docu_Ref, '');

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       "noov:Nvfac_fech",
       "noov:Nvfac_venc",
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvres_nume";
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');

  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvfac_cdet";

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO "noov:Nvcli_ncon";

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  "noov:Nvcli_ncon" = COALESCE("noov:Nvcli_ncon", '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO "noov:Nvven_nomb";

  "noov:Nvven_nomb" = COALESCE("noov:Nvven_nomb", '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       V_Codsociedad,
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO "noov:Nvcli_regi",
       "noov:Nvcli_fisc";

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO "noov:Nvcli_pais";

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO "noov:Nvcli_depa";

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:"noov:Nvcli_pais") = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO "noov:Nvcli_ciud";

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos";

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  "noov:Nvfac_orde" = COALESCE("noov:Nvfac_orde", '');
  "noov:Nvfac_remi" = COALESCE("noov:Nvfac_remi", '');
  "noov:Nvfac_rece" = COALESCE("noov:Nvfac_rece", '');
  "noov:Nvfac_entr" = COALESCE("noov:Nvfac_entr", '');
  "noov:Nvfac_ccos" = COALESCE("noov:Nvfac_ccos", '');

  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);

  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Caiu <> 0 THEN '11'
                                              WHEN V_Mand <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Caiu <> 0 THEN '15'
                                                   WHEN V_Mand <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;

  "noov:Nvfac_fpag" = CASE
                        WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                        WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                        ELSE 'ZZZ'
                      END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END
  ELSE
  BEGIN
    IF ("noov:Nvfac_fech" <> "noov:Nvfac_venc") THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    "noov:Nvcli_cper" = '2';
  ELSE
    "noov:Nvcli_cper" = '1';

  "noov:Nvcli_loca" = '';
  "noov:Nvcli_mail" = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  "noov:Nvema_copi" = '';

  "noov:Nvfac_obse" = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  SELECT SUM("noov:Nvfac_stot")
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Nvfac_Stot;

  "noov:Nvfac_stot" = V_Nvfac_Stot;

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;

  IF (V_Copag = 0) THEN
    "noov:Nvfac_desc" = V_Copago;
  ELSE
    "noov:Nvfac_anti" = V_Copago;

  "noov:Nvfac_desc" = "noov:Nvfac_desc" + V_Descuento_Det;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo")
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;

  V_Impuestos = COALESCE(V_Impuestos, 0);

  SELECT SUM(Stot_Red)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Stot_Red;

  "noov:Nvfac_totp" = V_Stot_Red + V_Impuestos - "noov:Nvfac_desc";

  "noov:Nvfor_oper" = '';
  IF ("noov:Nvfac_anti" > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      "noov:Nvfac_desc" = "noov:Nvfac_anti";
      "noov:Nvfac_anti" = 0;
    END
    ELSE
      "noov:Nvfac_totp" = "noov:Nvfac_totp" - "noov:Nvfac_anti";
    "noov:Nvfor_oper" = 'SS-CUFE';
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    "noov:Nvfor_oper" = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_numb" = '';
  "noov:Nvfac_obsb" = '';
  "noov:Nvcon_codi" = '';
  "noov:Nvcon_desc" = '';
  "noov:Nvfac_tcru" = '';
  "noov:Nvfac_numb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvcon_codi" = V_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      "noov:Nvfac_tcru" = 'L';
    ELSE
      "noov:Nvfac_tcru" = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijo_Ref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numero_Ref);
    "noov:Nvfac_fecb" = TRIM(COALESCE(V_Fecha_Ref, "noov:Nvfac_fech"));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota             CHAR(200);
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario         DOUBLE PRECISION;
DECLARE VARIABLE V_Dsi              CHAR(1);
DECLARE VARIABLE V_Empresa          VARCHAR(15);
DECLARE VARIABLE V_Renglon_1        INTEGER;
DECLARE VARIABLE V_Referencia_1     VARCHAR(20);
DECLARE VARIABLE V_Nom_Referencia_1 VARCHAR(198);
DECLARE VARIABLE V_Nota_1           VARCHAR(200);
DECLARE VARIABLE V_Medida_1         VARCHAR(5);
DECLARE VARIABLE V_Cantidad_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto_1          DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Pdescuento_1     DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_1      DOUBLE PRECISION;
DECLARE VARIABLE V_Iva              DOUBLE PRECISION;
DECLARE VARIABLE V_Personalizar     INTEGER;
BEGIN
  -- Datos_Empresa
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  -- Personalizacion
  /******************************************************************/
  /**** COMERCIALIZADORA INDUSTRIAL CENTRO S.A.S  (EDS LA MARIA) ****/
  /******************************************************************/
  IF (TRIM(V_Empresa) = 'XX900253903XX') THEN
  BEGIN

    -- Se valida si se personaliza o no
    SELECT COUNT(1)
    FROM Tr_Inventario
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
          AND Codreferencia STARTING WITH 'IVA'
    INTO V_Personalizar;

    IF (V_Personalizar > 0) THEN
    BEGIN

      -- Se busca el producto real inicial
      SELECT FIRST 1 Renglon,
                     Referencia,
                     Nom_Referencia,
                     Nota,
                     Medida,
                     Cantidad,
                     Bruto,
                     Unitario,
                     Porc_Descuento,
                     Descuento
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Referencia NOT LIKE 'IVA%'
      INTO V_Renglon_1, V_Referencia_1, V_Nom_Referencia_1, V_Nota_1,
           V_Medida_1, V_Cantidad_1, V_Bruto_1, V_Unitario_1, V_Pdescuento_1,
           V_Descuento_1;

      -- Los otros productos (IVA)
      FOR SELECT Renglon,
                 Referencia,
                 Nom_Referencia,
                 Nota,
                 Medida,
                 Cantidad,
                 Bruto,
                 Unitario,
                 Porc_Descuento,
                 Descuento,
                 Dsi
          FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
          WHERE Referencia LIKE 'IVA%'
          INTO Renglon, "noov:Nvpro_codi", "noov:Nvpro_nomb", V_Nota,
               "noov:Nvuni_desc", "noov:Nvfac_cant", V_Bruto, V_Unitario,
               "noov:Nvfac_pdes", "noov:Nvfac_desc", V_Dsi
      DO
      BEGIN
        V_Iva = IIF(TRIM("noov:Nvpro_codi") = 'IVA5', 5, 19);
        "noov:Nvfac_stot" = "noov:Nvfac_cant" * ((V_Unitario * 100) / V_Iva);
        Consecutivo = 1;
        --impuestos
        "noov:Nvimp_cdia" = '01';
        "noov:Nvdet_piva" = V_Iva;
        "noov:Nvdet_viva" = "noov:Nvfac_cant" * V_Unitario;

        "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
        "noov:Nvfac_valo" =  "noov:Nvfac_stot"/"noov:Nvfac_cant";

        "noov:Nvdet_pref" = '';
        IF (V_Bruto = "noov:Nvfac_desc") THEN
          "noov:Nvdet_pref" = '01';

        "noov:Nvdet_tcod" = '';
        "noov:Nvpro_cean" = '';
        "noov:Nvdet_entr" = '';
        "noov:Nvuni_quan" = 0;

        "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

        "noov:Nvdet_padr" = 0;
        "noov:Nvdet_marc" = '';
        "noov:Nvdet_mode" = '';

        IF ("noov:Nvfac_pdes" = 100) THEN
          "noov:Nvfac_desc" = "noov:Nvfac_stot";

        SELECT Valor
        FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
        INTO Stot_Red;

        SUSPEND;
        V_Unitario_1 = V_Unitario_1 - "noov:Nvfac_valo";
      END
      -- Producto Real Digitado
      Renglon = V_Renglon_1;
      Consecutivo = 1;
      "noov:Nvpro_codi" = V_Referencia_1;
      "noov:Nvpro_nomb" = V_Nom_Referencia_1;
      "noov:Nvuni_desc" = V_Medida_1;
      "noov:Nvfac_cant" = V_Cantidad_1;
      "noov:Nvfac_valo" = V_Unitario_1;
      "noov:Nvimp_cdia" = '00';
      "noov:Nvfac_pdes" = V_Pdescuento_1;
      "noov:Nvfac_desc" = V_Descuento_1;
      "noov:Nvdet_pref" = '';
      "noov:Nvfac_stot" = V_Cantidad_1 * V_Unitario_1;
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
      "noov:Nvdet_tcod" = '';
      "noov:Nvpro_cean" = '';
      "noov:Nvdet_entr" = '';
      "noov:Nvuni_quan" = 0;
      "noov:Nvdet_nota" = V_Nota_1;
      "noov:Nvdet_padr" = 0;
      "noov:Nvdet_marc" = '';
      "noov:Nvdet_mode" = '';
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
      INTO Stot_Red;
      SUSPEND;

    END
    ELSE
    BEGIN
      FOR SELECT Renglon,
                 Consecutivo,
                 "noov:Nvpro_codi",
                 "noov:Nvpro_nomb",
                 "noov:Nvuni_desc",
                 "noov:Nvfac_cant",
                 "noov:Nvfac_valo",
                 "noov:Nvimp_cdia",
                 "noov:Nvfac_pdes",
                 "noov:Nvfac_desc",
                 "noov:Nvdet_pref",
                 "noov:Nvfac_stot",
                 "noov:Nvdet_piva",
                 "noov:Nvdet_viva",
                 "noov:Nvdet_tcod",
                 "noov:Nvpro_cean",
                 "noov:Nvdet_entr",
                 "noov:Nvuni_quan",
                 "noov:Nvdet_nota",
                 "noov:Nvdet_padr",
                 "noov:Nvdet_marc",
                 "noov:Nvdet_mode",
                 Stot_Red
          FROM Pz_Fe_Detalle_E(:Tipo_, :Prefijo_, :Numero_)
          INTO :Renglon, :Consecutivo, :"noov:Nvpro_codi", :"noov:Nvpro_nomb",
               :"noov:Nvuni_desc", :"noov:Nvfac_cant", :"noov:Nvfac_valo",
               :"noov:Nvimp_cdia", :"noov:Nvfac_pdes", :"noov:Nvfac_desc",
               :"noov:Nvdet_pref", :"noov:Nvfac_stot", :"noov:Nvdet_piva",
               :"noov:Nvdet_viva", :"noov:Nvdet_tcod", :"noov:Nvpro_cean",
               :"noov:Nvdet_entr", :"noov:Nvuni_quan", :"noov:Nvdet_nota",
               :"noov:Nvdet_padr", :"noov:Nvdet_marc", :"noov:Nvdet_mode",
               :Stot_Red
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
  ELSE
  /************************************************************/
  /****                   XML ESTANDAR                     ****/
  /************************************************************/
  BEGIN
    FOR SELECT Renglon,
               Consecutivo,
               "noov:Nvpro_codi",
               "noov:Nvpro_nomb",
               "noov:Nvuni_desc",
               "noov:Nvfac_cant",
               "noov:Nvfac_valo",
               "noov:Nvimp_cdia",
               "noov:Nvfac_pdes",
               "noov:Nvfac_desc",
               "noov:Nvdet_pref",
               "noov:Nvfac_stot",
               "noov:Nvdet_piva",
               "noov:Nvdet_viva",
               "noov:Nvdet_tcod",
               "noov:Nvpro_cean",
               "noov:Nvdet_entr",
               "noov:Nvuni_quan",
               "noov:Nvdet_nota",
               "noov:Nvdet_padr",
               "noov:Nvdet_marc",
               "noov:Nvdet_mode",
               Stot_Red
        FROM Pz_Fe_Detalle_E(:Tipo_, :Prefijo_, :Numero_)
        INTO :Renglon, :Consecutivo, :"noov:Nvpro_codi", :"noov:Nvpro_nomb",
             :"noov:Nvuni_desc", :"noov:Nvfac_cant", :"noov:Nvfac_valo",
             :"noov:Nvimp_cdia", :"noov:Nvfac_pdes", :"noov:Nvfac_desc",
             :"noov:Nvdet_pref", :"noov:Nvfac_stot", :"noov:Nvdet_piva",
             :"noov:Nvdet_viva", :"noov:Nvdet_tcod", :"noov:Nvpro_cean",
             :"noov:Nvdet_entr", :"noov:Nvuni_quan", :"noov:Nvdet_nota",
             :"noov:Nvdet_padr", :"noov:Nvdet_marc", :"noov:Nvdet_mode",
             :Stot_Red
    DO
    BEGIN
      SUSPEND;
    END
  END

END^
CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_E (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota             CHAR(200);
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario         DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Aiu        DOUBLE PRECISION;
DECLARE VARIABLE V_Linea            CHAR(5);
DECLARE VARIABLE V_Dsi              CHAR(1);
DECLARE VARIABLE V_Num_Linea        INTEGER;
DECLARE VARIABLE V_Empresa          VARCHAR(15);
DECLARE VARIABLE V_Renglon_1        INTEGER;
DECLARE VARIABLE V_Referencia_1     VARCHAR(20);
DECLARE VARIABLE V_Nom_Referencia_1 VARCHAR(198);
DECLARE VARIABLE V_Nota_1           VARCHAR(200);
DECLARE VARIABLE V_Medida_1         VARCHAR(5);
DECLARE VARIABLE V_Cantidad_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto_1          DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Pdescuento_1     DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_1      DOUBLE PRECISION;
DECLARE VARIABLE V_Iva              DOUBLE PRECISION;
BEGIN
  V_Num_Linea = 0;

  FOR SELECT Renglon,
             Referencia,
             Nom_Referencia,
             Nota,
             Medida,
             Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Valor_Aiu,
             Stot_Red,
             Linea,
             Dsi
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      INTO Renglon, "noov:Nvpro_codi", "noov:Nvpro_nomb", V_Nota,
           "noov:Nvuni_desc", "noov:Nvfac_cant", V_Bruto, "noov:Nvfac_pdes",
           "noov:Nvfac_desc", V_Valor_Aiu, "noov:Nvfac_stot", V_Linea, V_Dsi
  DO
  BEGIN
    Consecutivo = 1;
    --impuestos
    "noov:Nvimp_cdia" = '00';
    "noov:Nvdet_piva" = 0;
    "noov:Nvdet_viva" = 0;

    IF (V_Linea = 'INC' AND
        V_Bruto > 0 AND
        V_Num_Linea > 0) THEN
      SELECT SKIP 1 "noov:Nvimp_cdia",
                    "noov:Nvimp_porc",
                    "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia", "noov:Nvdet_piva", "noov:Nvdet_viva";

    ELSE
      SELECT FIRST 1 "noov:Nvimp_cdia",
                     "noov:Nvimp_porc",
                     "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia", "noov:Nvdet_piva", "noov:Nvdet_viva";

    "noov:Nvimp_cdia" = COALESCE("noov:Nvimp_cdia", '00');
    "noov:Nvdet_piva" = COALESCE("noov:Nvdet_piva", 0);
    "noov:Nvdet_viva" = COALESCE("noov:Nvdet_viva", 0);

    "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
    "noov:Nvfac_valo" = V_Bruto;

    IF (V_Linea = 'INC' AND
        V_Bruto = 0) THEN
      "noov:Nvfac_valo" = "noov:Nvdet_viva" / "noov:Nvfac_cant";

    IF (V_Valor_Aiu > 0) THEN
      "noov:Nvimp_cdia" = '00';

    "noov:Nvdet_pref" = '';
    IF (V_Bruto = "noov:Nvfac_desc") THEN
      "noov:Nvdet_pref" = '01';

    IF (V_Valor_Aiu > 0) THEN
    BEGIN
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
    END
    "noov:Nvdet_tcod" = '';
    "noov:Nvpro_cean" = '';
    "noov:Nvdet_entr" = '';
    "noov:Nvuni_quan" = 0;

    IF (TRIM(V_Linea) IN ('AIU_A', 'AIU')) THEN
      "noov:Nvdet_nota" = 'Contrato de servicios AIU por concepto de: Servicios';
    ELSE
      "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

    "noov:Nvdet_padr" = 0;
    "noov:Nvdet_marc" = '';
    "noov:Nvdet_mode" = '';
    IF (V_Linea = 'INC' AND
        V_Bruto > 0) THEN
      V_Num_Linea = V_Num_Linea + 1;

    -- "noov:Nvfac_stot" = (V_Valo * "noov:Nvfac_cant") - V_Desc;

    IF ("noov:Nvfac_pdes" = 100) THEN
      "noov:Nvfac_desc" = "noov:Nvfac_stot";

    IF ("noov:Nvimp_cdia" = '22') THEN
      "noov:Nvfac_stot" = 0;

    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
    INTO Stot_Red;

    SUSPEND;
  END

END^



CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    REFERENCIA CHAR(20),
    NOM_REFERENCIA CHAR(80),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    UNITARIO DOUBLE PRECISION,
    PORC_DESCUENTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    AIU DOUBLE PRECISION,
    VALOR_AIU DOUBLE PRECISION,
    STOT DOUBLE PRECISION,
    NOTA CHAR(200),
    MEDIDA CHAR(5),
    LINEA CHAR(5),
    DSI CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Medida    CHAR(5);
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto     DOUBLE PRECISION;
DECLARE VARIABLE V_Valo      DOUBLE PRECISION;
DECLARE VARIABLE V_Desc      DOUBLE PRECISION;
BEGIN
  FOR SELECT Renglon,
             Codreferencia,
             Entrada + Salida,
             Bruto,
             Unitario,
             Porcentaje_Descuento,
             Descuento,
             Aiu,
             Nota,
             Dsi
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO :Renglon, :Referencia, :Cantidad, :Bruto, :Unitario, :Porc_Descuento,
           :Descuento, :Aiu, :Nota, :Dsi
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);
    Aiu = COALESCE(Aiu, 0);

    V_Medida = NULL;
    Medida = NULL;

    --Referencias
    SELECT R.Nombre,
           R.Codmedida,
           R.Codlinea
    FROM Referencias R
    WHERE Codigo = :Referencia
    INTO Nom_Referencia, V_Medida, Linea;

    IF (Bruto * Cantidad > 0 OR (Linea = 'INC' AND
        Bruto * Cantidad = 0)) THEN
    BEGIN

      IF (V_Medida IS NOT NULL) THEN
        SELECT Codigo_Fe
        FROM Medidas
        WHERE Codigo = :V_Medida
        INTO Medida;

      Medida = COALESCE(Medida, '94');

      IF (Porc_Descuento = 0) THEN
        IF (Bruto = 0) THEN
          Porc_Descuento = 0;
        ELSE
          Porc_Descuento = Descuento * 100 / Bruto;

      Descuento = :Bruto * :Porc_Descuento / 100;

      IF (Aiu = 0) THEN
        Aiu = 100;
      Valor_Aiu = (Bruto - Descuento) * Aiu / 100;

      IF (Aiu = 100) THEN
        Valor_Aiu = 0;

      Bruto = Bruto - Valor_Aiu;
      Stot = Cantidad * (Bruto - Descuento);

      IF (Porc_Descuento = 100) THEN
        Stot = Cantidad * Bruto;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        V_Renglon = Renglon;
        SELECT GEN_ID(Gen_Tr_Inventario, 1)
        FROM Rdb$Database
        INTO Renglon;
      END
      Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      SUSPEND;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        Renglon = V_Renglon;
        Bruto = Valor_Aiu;
        V_Valo = (SELECT Valor
                  FROM Redondeo_Dian(:Bruto, 2));
        Porc_Descuento = 0;
        Descuento = 0;
        Stot = Valor_Aiu * Cantidad;
        Stot_Red = (V_Valo * Cantidad);
        Valor_Aiu = 0;
        Aiu = 0;

        SUSPEND;
      END
      IF (Linea = 'INC' AND
          Bruto > 0) THEN
      BEGIN
          SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
BEGIN
  /*
SELECT Valor
FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
INTO V_Trm_Valor;
*/
  FOR SELECT Renglon
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      ORDER BY Renglon
      INTO :V_Renglon
  DO
  BEGIN

    /* Iniciar las varibles */
    "noov:Nvimp_cdia" = NULL;
    "noov:Nvimp_desc" = NULL;
    "noov:Nvimp_base" = 0;
    "noov:Nvimp_porc" = 0;
    "noov:Nvimp_valo" = 0;

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               "noov:Nvimp_base",
               "noov:Nvimp_porc",
               "noov:Nvimp_valo",
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :V_Renglon)
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :"noov:Nvimp_oper",
             :"noov:Nvimp_base",
             :"noov:Nvimp_porc",
             :"noov:Nvimp_valo",
             Es_Tarifa,
             Clase

    DO
    BEGIN
      SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Detalle (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10),
    Renglon_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Ano           INTEGER;
DECLARE VARIABLE V_Cantidad      DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto         DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento     DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu           DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa        DOUBLE PRECISION;
DECLARE VARIABLE V_Base          DOUBLE PRECISION;
DECLARE VARIABLE V_Valor         DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
DECLARE VARIABLE V_Es_Tarifa     CHAR(1);
DECLARE VARIABLE V_Valor_Nominal DOUBLE PRECISION;
BEGIN
  /* IMPUESTOS */
  "noov:Nvimp_oper" = 'S';

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  FOR SELECT Cantidad,
             Bruto,
             Descuento,
             Aiu
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Descuento,
           :V_Aiu
  DO
  BEGIN

    V_Cantidad = COALESCE(V_Cantidad, 0);
    V_Bruto = COALESCE(V_Bruto, 0);
    V_Descuento = COALESCE(V_Descuento, 0);
    V_Aiu = COALESCE(V_Aiu, 0);

    IF (V_Aiu = 0) THEN
      V_Aiu = 100;

    V_Tipo_Impuesto = NULL;
    V_Es_Tarifa = NULL;

    FOR SELECT SKIP 1 Tipo_Impuesto,
                      Es_Tarifa,
                      SUM(Debito + Credito)
        FROM Reg_Impuestos
        WHERE Tipo = :Tipo_
              AND Prefijo = :Prefijo_
              AND Numero = :Numero_
              AND Renglon = :Renglon_
        GROUP BY Codigo_Fe, Tipo_Impuesto, Es_Tarifa
        INTO :V_Tipo_Impuesto,
             :V_Es_Tarifa,
             :V_Valor_Nominal
    DO
    BEGIN
      V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
      V_Tarifa = NULL;

      SELECT Tarifa
      FROM Data_Impuestos
      WHERE Codtipoimpuesto = :V_Tipo_Impuesto
            AND Ano = :V_Ano
      INTO :V_Tarifa;

      V_Tarifa = COALESCE(V_Tarifa, 0);
      -- V_Tarifa = CAST(V_Tarifa AS NUMERIC(17,4));

      V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento;
      -- v_base = v_cantidad * (v_bruto - v_descuento) * v_aiu / 100;

      IF (V_Es_Tarifa = 'S') THEN
        V_Valor = V_Base * V_Tarifa / 100;

      IF (V_Es_Tarifa = 'N') THEN
      BEGIN
        IF (V_Cantidad = 0) THEN
          V_Tarifa = 0;
        ELSE
          V_Tarifa = V_Valor_Nominal / V_Cantidad;
        V_Valor = V_Valor_Nominal;
      END

      SELECT Codigo_Fe,
             SUBSTRING(Nombre FROM 1 FOR 40)
      FROM Tipoimpuestos
      WHERE Codigo = :V_Tipo_Impuesto
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc";

      IF ("noov:Nvimp_cdia" = '22') THEN
        V_Base = 0;

      "noov:Nvimp_desc" = CASE
                            WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                            WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                            WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                            WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                            WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                            ELSE "noov:Nvimp_desc"
                          END;

      "noov:Nvimp_base" = V_Base;
      "noov:Nvimp_porc" = V_Tarifa;
      "noov:Nvimp_valo" = V_Valor;

      IF ("noov:Nvimp_cdia" NOT IN ('22', '99') AND
          NOT("noov:Nvimp_cdia" = '04' AND
          V_Tarifa = 0)) THEN
        SUSPEND;

    END

  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_RENGLON (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Ano              INTEGER;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Porc_Descuento   DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valor            DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  V_Num_Linea = 0;
  FOR SELECT Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Aiu,
             Referencia,
             Stot_Red
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Porc_Descuento,
           :V_Descuento,
           :V_Aiu,
           :V_Referencia,
           :V_Base
  DO
  BEGIN

    IF (V_Num_Linea = 0) THEN
    BEGIN
      V_Cantidad = COALESCE(V_Cantidad, 0);
      V_Bruto = COALESCE(V_Bruto, 0);
      V_Descuento = COALESCE(V_Descuento, 0);
      V_Aiu = COALESCE(V_Aiu, 0);

      IF (V_Aiu = 0) THEN
        V_Aiu = 100;

      V_Esquema_Impuesto = NULL;
      V_Tipo_Impuesto = NULL;

      V_Tipo_Impuesto = NULL;
      Es_Tarifa = NULL;

      /* IMPUESTOS */

      "noov:Nvimp_oper" = 'S';

      FOR SELECT Tipo_Impuesto,
                 Es_Tarifa,
                 SUM(Debito + Credito)
          FROM Reg_Impuestos
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Codigo_Fe <> '00'
          GROUP BY Tipo_Impuesto, Es_Tarifa
          INTO :V_Tipo_Impuesto,
               :Es_Tarifa,
               :V_Valor_Nominal
      DO
      BEGIN
        V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
        V_Tarifa = NULL;

        SELECT Tarifa
        FROM Data_Impuestos
        WHERE Codtipoimpuesto = :V_Tipo_Impuesto
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        /*IF (V_Porc_Descuento=100) THEN
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100);
ELSE
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento; */

        IF (Es_Tarifa = 'S') THEN
          V_Valor = V_Base * V_Tarifa / 100;

        IF (Es_Tarifa = 'N') THEN
        BEGIN
          IF (V_Cantidad = 0) THEN
            V_Tarifa = 0;
          ELSE
            V_Tarifa = V_Valor_Nominal / V_Cantidad;
          V_Valor = V_Valor_Nominal;
        END

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40)
        FROM Tipoimpuestos
        WHERE Codigo = :V_Tipo_Impuesto
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc";

        IF ("noov:Nvimp_cdia" = '22') THEN
        BEGIN
          V_Base = 0;
          V_Num_Linea = 1;
        END

        "noov:Nvimp_desc" = CASE
                              WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                              WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                              WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                              WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                              WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                              WHEN V_Tarifa = 19 THEN 'Impuesto sobre la Ventas 19%'
                              WHEN V_Tarifa = 5 THEN 'Impuesto sobre la Ventas 5%'
                              WHEN V_Tarifa = 0 THEN 'Impuesto sobre la Ventas Exento'
                              ELSE "noov:Nvimp_desc"
                            END;

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Valor;

        IF (NOT("noov:Nvimp_cdia" = '04' AND
            V_Tarifa = 0)) THEN
          SUSPEND;

      END

      /* RETENCIONES */

      "noov:Nvimp_oper" = 'R';
      Es_Tarifa = 'S';

      V_Tipo_Retencion = NULL;
      V_Base = 0;
      V_Valor = 0;

      FOR SELECT Tipo_Retencion,
                 SUM(Base),
                 SUM(Debito + Credito)
          FROM Reg_Retenciones
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Publicar = 'S'
          GROUP BY Tipo_Retencion
          INTO :V_Tipo_Retencion,
               :V_Base,
               :V_Valor

      DO
      BEGIN

        "noov:Nvimp_cdia" = NULL;
        "noov:Nvimp_desc" = NULL;
        "noov:Nvimp_base" = 0;
        "noov:Nvimp_porc" = 0;
        "noov:Nvimp_valo" = 0;
        V_Tarifa = 0;

        V_Base = COALESCE(V_Base, 0);
        V_Valor = COALESCE(V_Valor, 0);

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40),
               Clase
        FROM Tiporetenciones
        WHERE Codigo = :V_Tipo_Retencion
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :Clase;

        SELECT Tarifa
        FROM Data_Retenciones
        WHERE Codtiporetencion = :V_Tipo_Retencion
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Base * V_Tarifa / 100;

        SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Empresa      VARCHAR(15);
DECLARE VARIABLE V_Personalizar INTEGER;
DECLARE VARIABLE V_Iva          INTEGER;
DECLARE VARIABLE V_Ref          VARCHAR(20);
BEGIN
  -- Datos_Empresa
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  -- Personalizacion
  /******************************************************************/
  /**** COMERCIALIZADORA INDUSTRIAL CENTRO S.A.S  (EDS LA MARIA) ****/
  /******************************************************************/
  IF (TRIM(V_Empresa) = 'XX900253903XX') THEN
  BEGIN
    -- Se valida si se personaliza o no
    SELECT COUNT(1)
    FROM Tr_Inventario
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
          AND Codreferencia STARTING WITH 'IVA'
    INTO V_Personalizar;

    IF (V_Personalizar > 0) THEN
    BEGIN
      FOR SELECT "noov:Nvimp_cdia",
                 "noov:Nvdet_piva",
                 "noov:Nvpro_codi",
                 SUM("noov:Nvdet_viva"),
                 SUM("noov:Nvfac_stot")
          FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
          WHERE "noov:Nvimp_cdia" <> '00'
          GROUP BY 1, 2, 3
          INTO :"noov:Nvimp_cdia", :"noov:Nvimp_porc", V_Ref,
               :"noov:Nvimp_valo", :"noov:Nvimp_base"
      DO
      BEGIN
        V_Iva = IIF(TRIM(V_Ref) = 'IVA5', 5, 19);
        "noov:Nvimp_oper" = 'S';
        "noov:Nvimp_desc" = 'IVA ' || V_Iva || '%';

        SELECT Valor
        FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
        INTO Valor_Red;

        SUSPEND;
      END

    END
    ELSE
    /************************************************************/
    /****                   XML ESTANDAR                     ****/
    /************************************************************/
    BEGIN
      FOR SELECT "noov:Nvimp_cdia",
                 "noov:Nvimp_desc",
                 "noov:Nvimp_oper",
                 "noov:Nvimp_base",
                 "noov:Nvimp_porc",
                 "noov:Nvimp_valo",
                 Valor_Red,
                 Es_Tarifa,
                 Clase
          FROM Pz_Fe_Impuestos_Sum_E(:Tipo_, :Prefijo_, :Numero_)
          INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
               :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
               :Valor_Red, :Es_Tarifa, :Clase
      DO
      BEGIN
        SUSPEND;
      END

    END

  END
  ELSE
  /************************************************************/
  /****                   XML ESTANDAR                     ****/
  /************************************************************/
  BEGIN
    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               "noov:Nvimp_base",
               "noov:Nvimp_porc",
               "noov:Nvimp_valo",
               Valor_Red,
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos_Sum_E(:Tipo_, :Prefijo_, :Numero_)
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             :Valor_Red, :Es_Tarifa, :Clase
    DO
    BEGIN
      SUSPEND;
    END

  END

END^

CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM_E (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
DECLARE VARIABLE V_Empresa   VARCHAR(15);
BEGIN

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               SUM("noov:Nvimp_base"),
               "noov:Nvimp_porc",
               SUM("noov:Nvimp_valo"),
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
        WHERE Es_Tarifa = 'S'
        GROUP BY 1, 2, 3, 5, 7, 8
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             Es_Tarifa, Clase
    DO
    BEGIN
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
      INTO Valor_Red;
      SUSPEND;
    END

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               SUM("noov:Nvimp_base"),
               SUM("noov:Nvimp_porc"),
               SUM("noov:Nvimp_valo"),
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
        WHERE Es_Tarifa = 'N'
        GROUP BY 1, 2, 3, 7, 8
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             Es_Tarifa, Clase
    DO
    BEGIN
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
      INTO Valor_Red;
      SUSPEND;
    END


END^


CREATE OR ALTER PROCEDURE PZ_FE_ROUND (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    TRM_ DOUBLE PRECISION)
RETURNS (
    VALOR NUMERIC(12,2))
AS
DECLARE VARIABLE V_Valor_Mekano DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Totp   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Stot   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Desc   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Imp    DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Mek_Xml  DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Xml      DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo         VARCHAR(20);
BEGIN
  --CODIGO_FE
  SELECT TRIM(Codigo_Fe)
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Tipo;

  SELECT Com_Total,
         "noov:Nvfac_totp",
         "noov:Nvfac_stot",
         "noov:Nvfac_desc" + "noov:Nvfac_anti"
  FROM Pz_Fe_Comprobantes(:Tipo_, :Prefijo_, :Numero_, '%', '%')
  INTO V_Valor_Mekano,
       V_Valor_Totp,
       V_Valor_Stot,
       V_Valor_Desc;

  -- Impuestos
  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Valor_Imp;
  V_Valor_Imp = COALESCE(V_Valor_Imp, 0);

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Mekano / :Trm_, 2)
  INTO V_Valor_Mekano;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Totp / :Trm_, 2)
  INTO V_Valor_Totp;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Stot / :Trm_, 2)
  INTO V_Valor_Stot;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Desc / :Trm_, 2)
  INTO V_Valor_Desc;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Imp / :Trm_, 2)
  INTO V_Valor_Imp;

  -- Diferencia Mekano vs XML
  V_Dif_Mek_Xml = :V_Valor_Mekano - :V_Valor_Totp;

  -- Valor XML
  V_Dif_Xml = :V_Valor_Totp - :V_Valor_Stot - :V_Valor_Imp + COALESCE(:V_Valor_Desc, 0);

  IF (V_Tipo IN ('DOCUMENTO SOPORTE','NOTA DE AJUSTE A DS')) THEN
    Valor = V_Dif_Xml;
  ELSE
    Valor = V_Dif_Mek_Xml + V_Dif_Xml;

  IF (Valor > 5000 OR Valor < -5000) THEN
    Valor = 0;

  Valor = COALESCE(Valor, 0);
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
DECLARE VARIABLE V_Docu    CHAR(20);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Trm     DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Lista   CHAR(5);
BEGIN
  -- Se valida el tipo de documento
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

  SELECT Trm,
         Fecha,
         Codlista
  FROM Comprobantes
  WHERE Tipo = :V_Tipo
        AND Prefijo = :V_Prefijo
        AND Numero = :V_Numero
  INTO V_Trm,
       V_Fecha,
       V_Lista;

  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista
  INTO Codigo;

  IF (TRIM(COALESCE(Codigo, '')) = '') THEN
    Codigo = 'COP';

  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Valor > 0
        AND Codmoneda = :Codigo
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO Valor;

  Valor = COALESCE(Valor, 1);

  IF (V_Trm > 0) THEN
    Valor = V_Trm;

  IF (Codigo = 'COP') THEN
    Valor = 1;

  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^



SET TERM ; ^

COMMIT WORK;

-- Tasa Alterna

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--952 Gestion 25/JUL/2023 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            ges:sector_salud
            ges:tasa_cambio
            ges:tasa_alterna
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');



COMMIT WORK;


UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (271, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 31, 'NOOVA', 'FACTURA', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (287, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Exportacion

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 31, 'NOOVA', 'EXPORTACION', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (309, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Contingencia

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 31, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (327, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 31, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (348, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 31, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (367, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 31, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (387, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 21, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


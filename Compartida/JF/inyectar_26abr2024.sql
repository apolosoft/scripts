-- 964037
 
/************************************************************/
/**** MODULO GESTION                                     ****/
/************************************************************/

/* EXPORTADOS GESTION INICIO*/

/* EXPORTADOS GESTION */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVA';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--964 Gestion 26/Abr/2024 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            ges:sector_salud
            ges:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');



COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 964 26/Abr/2024 v1

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)
WHERE Nvfor_oper = ''SS-CUFE''', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (271, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'FACTURA', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (272, 'ges:transporte1', 'DOC', '-- Transporte 1
 SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr"
        FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero, ''GESTION'')
        WHERE Renglon = :Renglon
    AND  Natr IN (''01'', ''02'')', 'S', 32, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (273, 'ges:transporte2', 'DOC', '-- Transporte 2
 
SELECT   Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               Uatr "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
FROM Pz_Fe_Transporte(:Tipo, :Prefijo, :Numero, :Renglon)  
WHERE  Natr =''03''', 'S', 33, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (287, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Exportacion

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'EXPORTACION', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)
WHERE Nvfor_oper = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (309, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Contingencia

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (327, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"

FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (348, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(COALESCE(Com_Total, 0), 2)) - (SELECT COALESCE(Valor, 0)
                                                          FROM Redondeo_Dian(COALESCE(Nvfac_Stot, 0), 2)) - (SELECT COALESCE(Valor, 0)
                                                                                                             FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                                 FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                                                 WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                        FROM Redondeo_Dian(COALESCE(Nvfac_Desc, 0), 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (367, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste A Ds

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, 2)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), 2)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Zipc "noov:Nvpro_zipc",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE  "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (387, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 21, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_COMPROBANTES (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15),
    CLASE_FE_ VARCHAR(20))
RETURNS (
    NVFAC_ORIG CHAR(1),
    NVEMP_NNIT VARCHAR(15),
    NVRES_NUME VARCHAR(20),
    NVFAC_TIPO VARCHAR(5),
    NVFAC_TCRU CHAR(1),
    NVRES_PREF VARCHAR(5),
    NVFAC_NUME VARCHAR(15),
    NVFAC_FECH VARCHAR(20),
    NVFAC_CDET INTEGER,
    NVFAC_VENC VARCHAR(20),
    NVSUC_CODI CHAR(1),
    NVMON_CODI VARCHAR(5),
    NVFOR_CODI VARCHAR(2),
    NVVEN_NOMB VARCHAR(84),
    NVFAC_FPAG VARCHAR(5),
    NVFAC_CONV CHAR(1),
    NVCLI_CPER CHAR(1),
    NVCLI_CDOC VARCHAR(5),
    NVCLI_DOCU VARCHAR(20),
    NVCLI_PAIS VARCHAR(5),
    NVCLI_DEPA VARCHAR(80),
    NVCLI_CIUD VARCHAR(100),
    NVCLI_LOCA VARCHAR(10),
    NVCLI_DIRE VARCHAR(80),
    NVCLI_NTEL VARCHAR(200),
    NVCLI_REGI VARCHAR(5),
    NVCLI_FISC VARCHAR(30),
    NVCLI_NOMB VARCHAR(80),
    NVCLI_PNOM VARCHAR(20),
    NVCLI_SNOM VARCHAR(20),
    NVCLI_APEL VARCHAR(40),
    NVCLI_MAIL VARCHAR(300),
    NVEMA_COPI VARCHAR(50),
    NVFAC_OBSE VARCHAR(4000),
    NVFAC_ORDE VARCHAR(40),
    NVFAC_REMI VARCHAR(40),
    NVFAC_RECE VARCHAR(40),
    NVFAC_ENTR VARCHAR(40),
    NVFAC_CCOS VARCHAR(40),
    NVFAC_STOT DOUBLE PRECISION,
    NVFAC_DESC DOUBLE PRECISION,
    NVFAC_ANTI DOUBLE PRECISION,
    NVFAC_CARG DOUBLE PRECISION,
    NVFAC_TOTA DOUBLE PRECISION,
    NVFAC_TOTP DOUBLE PRECISION,
    NVFAC_ROUN NUMERIC(12,2),
    NVFAC_VCOP DOUBLE PRECISION,
    NVFAC_TIMP DOUBLE PRECISION,
    NVFAC_COID CHAR(1),
    NVFAC_OBSB CHAR(1),
    NVCLI_NCON VARCHAR(80),
    NVCON_CODI VARCHAR(2),
    NVCON_DESC VARCHAR(200),
    NVFAC_NUMB VARCHAR(20),
    NVFAC_FECB VARCHAR(20),
    NVFOR_OPER VARCHAR(20),
    NVPRO_CPER CHAR(1),
    NVPRO_CDOC VARCHAR(5),
    NVPRO_DOCU VARCHAR(15),
    NVPRO_DIVE VARCHAR(1),
    NVPRO_PAIS VARCHAR(5),
    NVPRO_DEPA VARCHAR(80),
    NVPRO_CIUD VARCHAR(100),
    NVPRO_ZIPC VARCHAR(6),
    NVPRO_LOCA VARCHAR(10),
    NVPRO_DIRE VARCHAR(80),
    NVPRO_NTEL VARCHAR(200),
    NVPRO_REGI VARCHAR(5),
    NVPRO_FISC VARCHAR(30),
    NVPRO_NOMB VARCHAR(80),
    NVPRO_PNOM VARCHAR(20),
    NVPRO_SNOM VARCHAR(20),
    NVPRO_APEL VARCHAR(40),
    NVPRO_MAIL VARCHAR(300),
    NVPRO_NCON VARCHAR(80),
    V_DOCU_REF VARCHAR(20),
    COM_TOTAL DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    "noov:Nvdet_tran" CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    REFERENCIA CHAR(20),
    NOM_REFERENCIA CHAR(80),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    UNITARIO DOUBLE PRECISION,
    PORC_DESCUENTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    AIU DOUBLE PRECISION,
    VALOR_AIU DOUBLE PRECISION,
    STOT DOUBLE PRECISION,
    NOTA CHAR(200),
    MEDIDA CHAR(5),
    LINEA CHAR(5),
    DSI CHAR(1),
    STOT_RED DOUBLE PRECISION,
    GRADOS DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_RENGLON (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_NOMBRE_IMPUESTO (
    IMPUESTO_ CHAR(5))
RETURNS (
    NOMBRE_IMPUESTO VARCHAR(80))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_TRANSPORTE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(10),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    ATRI VARCHAR(1),
    NATR VARCHAR(2),
    VATR VARCHAR(20),
    UATR VARCHAR(20),
    CATR VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    DESDE DATE,
    HASTA DATE,
    CODREFERIDO CHAR(15))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_BEN (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    BENEFICIARIO_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    NOMBRE CHAR(40),
    APELLIDO CHAR(40),
    CIUDAD CHAR(5),
    DIRECCION CHAR(80),
    PAIS CHAR(80))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_REF (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    NUME CHAR(15),
    CUFD CHAR(96),
    ALGR CHAR(11),
    FEMI CHAR(10),
    IDTO CHAR(5),
    NDOC CHAR(15),
    OPER CHAR(15),
    REPS CHAR(15))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_USU (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    APL1 CHAR(20),
    APL2 CHAR(20),
    NOM1 CHAR(20),
    NOM2 CHAR(20),
    MCON CHAR(5),
    COBE CHAR(5),
    NUMERO_CONTRATO CHAR(15),
    POLIZA CHAR(15),
    COPAGO NUMERIC(18,2),
    CUOTA_MODERADORA NUMERIC(18,2),
    PAGOS_COMPARTIDOS NUMERIC(18,2))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_COMPROBANTES (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15),
    CLASE_FE_ VARCHAR(20))
RETURNS (
    NVFAC_ORIG CHAR(1),
    NVEMP_NNIT VARCHAR(15),
    NVRES_NUME VARCHAR(20),
    NVFAC_TIPO VARCHAR(5),
    NVFAC_TCRU CHAR(1),
    NVRES_PREF VARCHAR(5),
    NVFAC_NUME VARCHAR(15),
    NVFAC_FECH VARCHAR(20),
    NVFAC_CDET INTEGER,
    NVFAC_VENC VARCHAR(20),
    NVSUC_CODI CHAR(1),
    NVMON_CODI VARCHAR(5),
    NVFOR_CODI VARCHAR(2),
    NVVEN_NOMB VARCHAR(84),
    NVFAC_FPAG VARCHAR(5),
    NVFAC_CONV CHAR(1),
    NVCLI_CPER CHAR(1),
    NVCLI_CDOC VARCHAR(5),
    NVCLI_DOCU VARCHAR(20),
    NVCLI_PAIS VARCHAR(5),
    NVCLI_DEPA VARCHAR(80),
    NVCLI_CIUD VARCHAR(100),
    NVCLI_LOCA VARCHAR(10),
    NVCLI_DIRE VARCHAR(80),
    NVCLI_NTEL VARCHAR(200),
    NVCLI_REGI VARCHAR(5),
    NVCLI_FISC VARCHAR(30),
    NVCLI_NOMB VARCHAR(80),
    NVCLI_PNOM VARCHAR(20),
    NVCLI_SNOM VARCHAR(20),
    NVCLI_APEL VARCHAR(40),
    NVCLI_MAIL VARCHAR(300),
    NVEMA_COPI VARCHAR(50),
    NVFAC_OBSE VARCHAR(4000),
    NVFAC_ORDE VARCHAR(40),
    NVFAC_REMI VARCHAR(40),
    NVFAC_RECE VARCHAR(40),
    NVFAC_ENTR VARCHAR(40),
    NVFAC_CCOS VARCHAR(40),
    NVFAC_STOT DOUBLE PRECISION,
    NVFAC_DESC DOUBLE PRECISION,
    NVFAC_ANTI DOUBLE PRECISION,
    NVFAC_CARG DOUBLE PRECISION,
    NVFAC_TOTA DOUBLE PRECISION,
    NVFAC_TOTP DOUBLE PRECISION,
    NVFAC_ROUN NUMERIC(12,2),
    NVFAC_VCOP DOUBLE PRECISION,
    NVFAC_TIMP DOUBLE PRECISION,
    NVFAC_COID CHAR(1),
    NVFAC_OBSB CHAR(1),
    NVCLI_NCON VARCHAR(80),
    NVCON_CODI VARCHAR(2),
    NVCON_DESC VARCHAR(200),
    NVFAC_NUMB VARCHAR(20),
    NVFAC_FECB VARCHAR(20),
    NVFOR_OPER VARCHAR(20),
    NVPRO_CPER CHAR(1),
    NVPRO_CDOC VARCHAR(5),
    NVPRO_DOCU VARCHAR(15),
    NVPRO_DIVE VARCHAR(1),
    NVPRO_PAIS VARCHAR(5),
    NVPRO_DEPA VARCHAR(80),
    NVPRO_CIUD VARCHAR(100),
    NVPRO_ZIPC VARCHAR(6),
    NVPRO_LOCA VARCHAR(10),
    NVPRO_DIRE VARCHAR(80),
    NVPRO_NTEL VARCHAR(200),
    NVPRO_REGI VARCHAR(5),
    NVPRO_FISC VARCHAR(30),
    NVPRO_NOMB VARCHAR(80),
    NVPRO_PNOM VARCHAR(20),
    NVPRO_SNOM VARCHAR(20),
    NVPRO_APEL VARCHAR(40),
    NVPRO_MAIL VARCHAR(300),
    NVPRO_NCON VARCHAR(80),
    V_DOCU_REF VARCHAR(20),
    COM_TOTAL DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Caiu            INTEGER;
DECLARE VARIABLE V_Dsi             INTEGER;
DECLARE VARIABLE V_Mand            INTEGER;
DECLARE VARIABLE V_Cod_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Tesoreria       VARCHAR(10);
DECLARE VARIABLE V_Formapago       VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe    VARCHAR(5);
DECLARE VARIABLE V_Codbanco        VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe     VARCHAR(5);
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Codsociedad     VARCHAR(5);
DECLARE VARIABLE V_Email_Ter       VARCHAR(80);
DECLARE VARIABLE V_Codsucursal     VARCHAR(15);
DECLARE VARIABLE V_Email_Suc       VARCHAR(80);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Copag           INTEGER;
DECLARE VARIABLE V_Copago          DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Municipio       VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref        VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref     VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref      VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac       VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref        VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Stot_Red        DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det   DOUBLE PRECISION;
DECLARE VARIABLE V_Siif            VARCHAR(80);
DECLARE VARIABLE V_Fecre           INTEGER;
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
DECLARE VARIABLE V_Transporte      INTEGER;
DECLARE VARIABLE V_Impuestos_Red   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Totp      DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Mekano    DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Imp       DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Desc      DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Mek_Xml     DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Xml         DOUBLE PRECISION;
DECLARE VARIABLE V_Oper_Salud      VARCHAR(20);
BEGIN
  Nvemp_Nnit = :Emisor_;

  IF (Clase_Fe_ IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN

    -- Si es NC o ND se toman datos del doc referencia
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo_Ref,
         V_Prefijo_Ref,
         V_Numero_Ref;

    V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
    V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
    V_Numero_Ref = COALESCE(V_Numero_Ref, '');

    SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
    FROM Comprobantes
    WHERE Tipo = :V_Tipo_Ref
          AND Prefijo = :V_Prefijo_Ref
          AND Numero = :V_Numero_Ref
    INTO V_Fecha_Ref;
    -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

    SELECT Cufe,
           EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
    FROM Facturas
    WHERE Tipo = :V_Tipo_Ref
          AND Prefijo = :V_Prefijo_Ref
          AND Numero = :V_Numero_Ref
    INTO V_Cufe_Ref,
         V_Fecha_Fac;
    V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
    V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

    SELECT Codigo_Fe
    FROM Documentos
    WHERE Codigo = :V_Tipo_Ref
    INTO V_Docu_Ref;
    V_Docu_Ref = COALESCE(V_Docu_Ref, '');
  END

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       Nvfac_Fech,
       Nvfac_Venc,
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  -- Transportes
  V_Transporte = 0;
  SELECT COUNT(1)
  FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
  INTO V_Transporte;

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe,
         Tipo_Operacion
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe,
       V_Oper_Salud;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO Nvres_Nume;
  Nvres_Nume = COALESCE(Nvres_Nume, '0');

  Nvres_Nume = CASE TRIM(V_Docu)
                 WHEN 'NOTA DEBITO' THEN '1'
                 WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                 ELSE Nvres_Nume
               END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO Nvfac_Cdet;

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO Nvcli_Ncon;

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  Nvcli_Ncon = COALESCE(Nvcli_Ncon, '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO Nvven_Nomb;

  Nvven_Nomb = COALESCE(Nvven_Nomb, '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       Nvcli_Cdoc,
       Nvcli_Docu,
       Nvcli_Dire,
       Nvcli_Ntel,
       V_Codsociedad,
       Nvcli_Nomb,
       Nvcli_Pnom,
       Nvcli_Snom,
       Nvcli_Apel,
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO Nvcli_Regi,
       Nvcli_Fisc;

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Nvcli_Pais;

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO Nvcli_Depa;

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:Nvcli_Pais) = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO Nvcli_Ciud;

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Nvfac_Orde,
       Nvfac_Remi,
       Nvfac_Rece,
       Nvfac_Entr,
       Nvfac_Ccos;

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  Nvfac_Orde = COALESCE(Nvfac_Orde, '');
  Nvfac_Remi = COALESCE(Nvfac_Remi, '');
  Nvfac_Rece = COALESCE(Nvfac_Rece, '');
  Nvfac_Entr = COALESCE(Nvfac_Entr, '');
  Nvfac_Ccos = COALESCE(Nvfac_Ccos, '');

  Nvfac_Orig = 'E';
  --  Nvemp_nnit = :Emisor_;
  Nvfac_Tipo = CASE TRIM(V_Docu)
                 WHEN 'FACTURA' THEN 'FV'
                 WHEN 'CONTINGENCIA' THEN 'FC'
                 WHEN 'EXPORTACION' THEN 'FE'
                 WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                            WHEN 'EXPORTACION' THEN 'CE'
                                            WHEN 'CONTINGENCIA' THEN 'CC'
                                            ELSE 'CV'
                                          END
                 WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                           WHEN 'EXPORTACION' THEN 'DE'
                                           WHEN 'CONTINGENCIA' THEN 'DC'
                                           ELSE 'DV'
                                         END
                 WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                 WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

               END;

  Nvres_Pref = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    Nvres_Pref = '';
  Nvfac_Nume = TRIM(Nvres_Pref) || TRIM(Numero_);

  Nvsuc_Codi = '1';
  Nvmon_Codi = 'COP';
  Nvfor_Codi = CASE TRIM(V_Docu)
                 WHEN 'FACTURA' THEN CASE
                                       WHEN V_Caiu <> 0 THEN '11'
                                       WHEN V_Mand <> 0 THEN '12'
                                       WHEN V_Transporte <> 0 THEN '18'
                                       ELSE '1'
                                     END
                 WHEN 'CONTINGENCIA' THEN CASE
                                            WHEN V_Caiu <> 0 THEN '15'
                                            WHEN V_Mand <> 0 THEN '16'
                                            ELSE '7'
                                          END
                 WHEN 'EXPORTACION' THEN IIF(Nvcli_Pais = 'CO', '10', '4')
                 WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                            WHEN 'EXPORTACION' THEN '5'
                                            WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                            WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                            ELSE '2'
                                          END
                 WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                           WHEN 'EXPORTACION' THEN '6'
                                           WHEN 'FACTURA' THEN '3'
                                           ELSE '3'
                                         END
                 WHEN 'DOCUMENTO SOPORTE' THEN '20'
                 WHEN 'NOTA DE AJUSTE A DS' THEN '21'
               END;

  Nvfac_Fpag = CASE
                 WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                 WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                 ELSE 'ZZZ'
               END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      Nvfac_Conv = '2';
    ELSE
      Nvfac_Conv = '1';
  END
  ELSE
  BEGIN
    IF (Nvfac_Fech <> Nvfac_Venc) THEN
      Nvfac_Conv = '2';
    ELSE
      Nvfac_Conv = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    Nvcli_Cper = '2';
  ELSE
    Nvcli_Cper = '1';

  Nvcli_Loca = '';
  Nvcli_Mail = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  Nvema_Copi = '';

  Nvfac_Obse = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_pdes", 0)),
         SUM("noov:Nvfac_stot"),
         SUM(Stot_Red)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det,
       V_Nvfac_Stot,
       V_Stot_Red;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  Nvfac_Stot = V_Stot_Red;

  Nvfac_Desc = 0;
  Nvfac_Anti = 0;

  IF (V_Copag = 0) THEN
    Nvfac_Desc = V_Copago;
  ELSE
    Nvfac_Anti = V_Copago;

  Nvfac_Desc = Nvfac_Desc + V_Descuento_Det;
  Nvfac_Carg = 0;
  Nvfac_Tota = Nvfac_Stot;

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo"),
         SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos,
       :V_Impuestos_Red;

  V_Impuestos = COALESCE(V_Impuestos, 0);
  V_Impuestos_Red = COALESCE(V_Impuestos_Red, 0);

  Nvfac_Totp = V_Stot_Red + V_Impuestos - Nvfac_Desc;

  Nvfor_Oper = '';
  IF (Nvfac_Anti > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      Nvfac_Desc = Nvfac_Anti;
      Nvfac_Anti = 0;
    END
    ELSE
      Nvfac_Totp = Nvfac_Totp - Nvfac_Anti;
    Nvfor_Oper = 'SS-CUFE';
  END

  -- Salud
  IF (TRIM(:V_Oper_Salud) <> 'NA') THEN
  BEGIN
    Nvfor_Oper = :V_Oper_Salud;
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    Nvfor_Oper = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  -- redondeo
  SELECT Valor
  FROM Redondeo_Dian(:Nvfac_Totp, 2)
  INTO V_Valor_Totp;

  SELECT Valor
  FROM Redondeo_Dian(:Com_Total, 2)
  INTO V_Valor_Mekano;

  SELECT Valor
  FROM Redondeo_Dian(:V_Stot_Red, 2)
  INTO V_Valor_Stot;

  SELECT Valor
  FROM Redondeo_Dian(:V_Impuestos_Red, 2)
  INTO V_Valor_Imp;

  SELECT Valor
  FROM Redondeo_Dian(:Nvfac_Desc + :Nvfac_Anti, 2)
  INTO V_Valor_Desc;

  V_Dif_Mek_Xml = :V_Valor_Mekano - :V_Valor_Totp;

  V_Dif_Xml = :V_Valor_Totp - :V_Valor_Stot - :V_Valor_Imp + COALESCE(:V_Valor_Desc, 0);

  IF (:Clase_Fe_ IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
    Nvfac_Roun = V_Dif_Xml;
  ELSE
    Nvfac_Roun = V_Dif_Mek_Xml + V_Dif_Xml;

  IF (Nvfac_Roun > 5000 OR Nvfac_Roun < -5000) THEN
    Nvfac_Roun = 0;

  Nvfac_Roun = COALESCE(Nvfac_Roun, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  Nvfac_Vcop = 0;
  Nvfac_Timp = 0;
  Nvfac_Coid = '';
  Nvfac_Numb = '';
  Nvfac_Obsb = '';
  Nvcon_Codi = '';
  Nvcon_Desc = '';
  Nvfac_Tcru = '';
  Nvfac_Numb = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    Nvcon_Codi = V_Concepto_Nc;
    Nvcon_Desc = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      Nvfac_Tcru = 'L';
    ELSE
      Nvfac_Tcru = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      Nvfac_Numb = TRIM(V_Prefijo_Ref);
    Nvfac_Numb = TRIM(Nvfac_Numb) || TRIM(V_Numero_Ref);
    Nvfac_Fecb = TRIM(COALESCE(V_Fecha_Ref, Nvfac_Fech));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    Nvpro_Cper = Nvcli_Cper;
    Nvpro_Cdoc = Nvcli_Cdoc;
    Nvpro_Docu = TRIM(Receptor_);
    Nvpro_Dive = '';

    IF (Nvpro_Cdoc = '31') THEN
      Nvpro_Dive = RIGHT(Nvcli_Docu, 1);

    Nvpro_Pais = Nvcli_Pais;
    Nvpro_Depa = Nvcli_Depa;
    Nvpro_Ciud = Nvcli_Ciud;
    Nvpro_Zipc = V_Postal_Code;
    Nvpro_Loca = Nvcli_Loca;
    Nvpro_Dire = Nvcli_Dire;
    Nvpro_Ntel = Nvcli_Ntel;
    Nvpro_Regi = Nvcli_Regi;
    Nvpro_Fisc = Nvcli_Fisc;
    Nvpro_Nomb = Nvcli_Nomb;
    Nvpro_Pnom = Nvcli_Pnom;
    Nvpro_Snom = Nvcli_Snom;
    Nvpro_Apel = Nvcli_Apel;
    Nvpro_Mail = V_Dscor;
    Nvpro_Ncon = Nvcli_Ncon;
    Nvcli_Cper = '';
    Nvcli_Cdoc = '';
    Nvcli_Docu = '';
    Nvcli_Pais = '';
    Nvcli_Depa = '';
    Nvcli_Ciud = '';
    Nvcli_Loca = '';
    Nvcli_Dire = '';
    Nvcli_Ntel = '';
    Nvcli_Regi = '';
    Nvcli_Fisc = '';
    Nvcli_Nomb = '';
    Nvcli_Pnom = '';
    Nvcli_Snom = '';
    Nvcli_Apel = '';
    Nvcli_Mail = '';
    Nvcli_Ncon = '';
  END
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    "noov:Nvdet_tran" CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota             CHAR(200);
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario         DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Aiu        DOUBLE PRECISION;
DECLARE VARIABLE V_Linea            CHAR(5);
DECLARE VARIABLE V_Dsi              CHAR(1);
DECLARE VARIABLE V_Num_Linea        INTEGER;
DECLARE VARIABLE V_Empresa          VARCHAR(15);
DECLARE VARIABLE V_Renglon_1        INTEGER;
DECLARE VARIABLE V_Referencia_1     VARCHAR(20);
DECLARE VARIABLE V_Nom_Referencia_1 VARCHAR(198);
DECLARE VARIABLE V_Nota_1           VARCHAR(200);
DECLARE VARIABLE V_Medida_1         VARCHAR(5);
DECLARE VARIABLE V_Cantidad_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto_1          DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Pdescuento_1     DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_1      DOUBLE PRECISION;
DECLARE VARIABLE V_Iva              DOUBLE PRECISION;
DECLARE VARIABLE V_Transporte       INTEGER;
DECLARE VARIABLE V_Tipo_Linea       CHAR(20);
BEGIN
  V_Num_Linea = 0;

  FOR SELECT Renglon,
             Referencia,
             Nom_Referencia,
             Nota,
             Medida,
             Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Valor_Aiu,
             Stot_Red,
             Linea,
             Dsi
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      INTO Renglon,
           "noov:Nvpro_codi",
           "noov:Nvpro_nomb",
           V_Nota,
           "noov:Nvuni_desc",
           "noov:Nvfac_cant",
           V_Bruto,
           "noov:Nvfac_pdes",
           "noov:Nvfac_desc",
           V_Valor_Aiu,
           "noov:Nvfac_stot",
           V_Linea,
           V_Dsi
  DO
  BEGIN
    Consecutivo = 1;
    --impuestos
    "noov:Nvimp_cdia" = '00';
    "noov:Nvdet_piva" = 0;
    "noov:Nvdet_viva" = 0;

    IF (V_Linea = 'INC' AND
        V_Bruto > 0 AND
        V_Num_Linea > 0) THEN
      SELECT SKIP 1 "noov:Nvimp_cdia",
                    "noov:Nvimp_porc",
                    "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    ELSE
      SELECT FIRST 1 "noov:Nvimp_cdia",
                     "noov:Nvimp_porc",
                     "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    "noov:Nvimp_cdia" = COALESCE("noov:Nvimp_cdia", '00');
    "noov:Nvdet_piva" = COALESCE("noov:Nvdet_piva", 0);
    "noov:Nvdet_viva" = COALESCE("noov:Nvdet_viva", 0);

    "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
    "noov:Nvfac_valo" = V_Bruto;

    IF (V_Linea = 'INC' AND
        V_Bruto = 0) THEN
      "noov:Nvfac_valo" = "noov:Nvdet_viva" / "noov:Nvfac_cant";

    IF (V_Valor_Aiu > 0) THEN
      "noov:Nvimp_cdia" = '00';

    "noov:Nvdet_pref" = '';
    IF (V_Bruto = "noov:Nvfac_desc") THEN
      "noov:Nvdet_pref" = '01';

    IF (V_Valor_Aiu > 0) THEN
    BEGIN
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
    END
    "noov:Nvdet_tcod" = '';
    "noov:Nvpro_cean" = '';
    "noov:Nvdet_entr" = '';
    "noov:Nvuni_quan" = 0;

    IF (TRIM(V_Linea) IN ('AIU_A', 'AIU')) THEN
      "noov:Nvdet_nota" = 'Contrato de servicios AIU por concepto de: Servicios';
    ELSE
      "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

    "noov:Nvdet_padr" = 0;
    "noov:Nvdet_marc" = '';
    "noov:Nvdet_mode" = '';
    IF (V_Linea = 'INC' AND
        V_Bruto > 0) THEN
      V_Num_Linea = V_Num_Linea + 1;

    -- "noov:Nvfac_stot" = (V_Valo * "noov:Nvfac_cant") - V_Desc;

    IF ("noov:Nvfac_pdes" = 100) THEN
      "noov:Nvfac_desc" = "noov:Nvfac_stot";

    IF ("noov:Nvimp_cdia" = '22') THEN
      "noov:Nvfac_stot" = 0;

    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
    INTO Stot_Red;

    -- Transporte de carga
    SELECT COUNT(1)
    FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
    INTO V_Transporte;
    "noov:Nvdet_tran" = '';
    IF (V_Transporte > 1) THEN
    BEGIN
      SELECT Tipo
      FROM Lineas
      WHERE Codigo = :V_Linea
      INTO :V_Tipo_Linea;

      IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE SERVICIO') THEN
        "noov:Nvdet_tran" = '0';
      ELSE
      IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE REMESA') THEN
        "noov:Nvdet_tran" = '1';
    END

    SUSPEND;
  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    REFERENCIA CHAR(20),
    NOM_REFERENCIA CHAR(80),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    UNITARIO DOUBLE PRECISION,
    PORC_DESCUENTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    AIU DOUBLE PRECISION,
    VALOR_AIU DOUBLE PRECISION,
    STOT DOUBLE PRECISION,
    NOTA CHAR(200),
    MEDIDA CHAR(5),
    LINEA CHAR(5),
    DSI CHAR(1),
    STOT_RED DOUBLE PRECISION,
    GRADOS DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Medida  CHAR(5);
DECLARE VARIABLE V_Renglon INTEGER;
DECLARE VARIABLE V_Valo    DOUBLE PRECISION;
DECLARE VARIABLE V_Desc    DOUBLE PRECISION;
BEGIN
  FOR SELECT Renglon,
             Codreferencia,
             Entrada + Salida,
             Bruto,
             Unitario,
             Porcentaje_Descuento,
             Descuento,
             Aiu,
             Nota,
             Dsi
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO :Renglon,
           :Referencia,
           :Cantidad,
           :Bruto,
           :Unitario,
           :Porc_Descuento,
           :Descuento,
           :Aiu,
           :Nota,
           :Dsi
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);
    Aiu = COALESCE(Aiu, 0);

    V_Medida = NULL;
    Medida = NULL;

    --Referencias
    SELECT R.Nombre,
           R.Codmedida,
           R.Codlinea,
           R.Grados
    FROM Referencias R
    WHERE Codigo = :Referencia
    INTO Nom_Referencia,
         V_Medida,
         Linea,
         Grados;
    Grados = COALESCE(Grados * :Cantidad * 100, 0);

    IF (Bruto * Cantidad > 0 OR (Linea = 'INC' AND
        Bruto * Cantidad = 0)) THEN
    BEGIN

      IF (V_Medida IS NOT NULL) THEN
        SELECT Codigo_Fe
        FROM Medidas
        WHERE Codigo = :V_Medida
        INTO Medida;

      Medida = COALESCE(Medida, '94');

      IF (Porc_Descuento = 0) THEN
        IF (Bruto = 0) THEN
          Porc_Descuento = 0;
        ELSE
          Porc_Descuento = Descuento * 100 / Bruto;

      Descuento = :Bruto * :Porc_Descuento / 100;

      IF (Aiu = 0) THEN
        Aiu = 100;
      Valor_Aiu = (Bruto - Descuento) * Aiu / 100;

      IF (Aiu = 100) THEN
        Valor_Aiu = 0;

      Bruto = Bruto - Valor_Aiu;
      Stot = Cantidad * (Bruto - Descuento);

      IF (Porc_Descuento = 100) THEN
        Stot = Cantidad * Bruto;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        V_Renglon = Renglon;
        SELECT GEN_ID(Gen_Tr_Inventario, 1)
        FROM Rdb$Database
        INTO Renglon;
      END
      Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      SUSPEND;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        Renglon = V_Renglon;
        Bruto = Valor_Aiu;
        V_Valo = (SELECT Valor
                  FROM Redondeo_Dian(:Bruto, 2));
        Porc_Descuento = 0;
        Descuento = 0;
        Stot = Valor_Aiu * Cantidad;
        Stot_Red = (V_Valo * Cantidad);
        Valor_Aiu = 0;
        Aiu = 0;

        SUSPEND;
      END
      IF (Linea = 'INC' AND
          Bruto > 0) THEN
      BEGIN
        SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
BEGIN
  /*
SELECT Valor
FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
INTO V_Trm_Valor;
*/
  FOR SELECT Renglon
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      ORDER BY Renglon
      INTO :V_Renglon
  DO
  BEGIN

    /* Iniciar las varibles */
    "noov:Nvimp_cdia" = NULL;
    "noov:Nvimp_desc" = NULL;
    "noov:Nvimp_base" = 0;
    "noov:Nvimp_porc" = 0;
    "noov:Nvimp_valo" = 0;

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               "noov:Nvimp_base",
               "noov:Nvimp_porc",
               "noov:Nvimp_valo",
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :V_Renglon)
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :"noov:Nvimp_oper",
             :"noov:Nvimp_base",
             :"noov:Nvimp_porc",
             :"noov:Nvimp_valo",
             Es_Tarifa,
             Clase

    DO
    BEGIN
      SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Ano           INTEGER;
DECLARE VARIABLE V_Cantidad      DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto         DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento     DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu           DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa        DOUBLE PRECISION;
DECLARE VARIABLE V_Base          DOUBLE PRECISION;
DECLARE VARIABLE V_Valor         DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
DECLARE VARIABLE V_Es_Tarifa     CHAR(1);
DECLARE VARIABLE V_Valor_Nominal DOUBLE PRECISION;
DECLARE VARIABLE V_Grados        DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Sal     DOUBLE PRECISION;
BEGIN
  /* IMPUESTOS */
  "noov:Nvimp_oper" = 'S';

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  FOR SELECT Cantidad,
             Bruto,
             Descuento,
             Aiu,
             Grados
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Descuento,
           :V_Aiu,
           :V_Grados
  DO
  BEGIN

    V_Cantidad = COALESCE(V_Cantidad, 0);
    V_Bruto = COALESCE(V_Bruto, 0);
    V_Descuento = COALESCE(V_Descuento, 0);
    V_Aiu = COALESCE(V_Aiu, 0);

    IF (V_Aiu = 0) THEN
      V_Aiu = 100;

    V_Tipo_Impuesto = NULL;
    V_Es_Tarifa = NULL;

    FOR SELECT SKIP 1 Tipo_Impuesto,
                      Es_Tarifa,
                      SUM(Debito + Credito)
        FROM Reg_Impuestos
        WHERE Tipo = :Tipo_
              AND Prefijo = :Prefijo_
              AND Numero = :Numero_
              AND Renglon = :Renglon_
        GROUP BY Codigo_Fe, Tipo_Impuesto, Es_Tarifa
        INTO :V_Tipo_Impuesto,
             :V_Es_Tarifa,
             :V_Valor_Nominal
    DO
    BEGIN
      V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
      V_Tarifa = NULL;

      SELECT Tarifa,
             Valor
      FROM Data_Impuestos
      WHERE Codtipoimpuesto = :V_Tipo_Impuesto
            AND Ano = :V_Ano
      INTO :V_Tarifa,
           :V_Valor_Sal;

      V_Tarifa = COALESCE(V_Tarifa, 0);
      -- V_Tarifa = CAST(V_Tarifa AS NUMERIC(17,4));

      V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento;
      -- v_base = v_cantidad * (v_bruto - v_descuento) * v_aiu / 100;

      IF (V_Es_Tarifa = 'S') THEN
        V_Valor = V_Base * V_Tarifa / 100;

      IF (V_Es_Tarifa = 'N') THEN
      BEGIN
        IF (V_Cantidad = 0) THEN
          V_Tarifa = 0;
        ELSE
          V_Tarifa = V_Valor_Nominal / V_Cantidad;
        V_Valor = V_Valor_Nominal;
      END

      SELECT Codigo_Fe,
             SUBSTRING(Nombre FROM 1 FOR 40)
      FROM Tipoimpuestos
      WHERE Codigo = :V_Tipo_Impuesto
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc";

      SELECT Nombre_Impuesto
      FROM Pz_Fe_Nombre_Impuesto(:"noov:Nvimp_cdia")
      INTO "noov:Nvimp_desc";


      "noov:Nvimp_base" = CASE TRIM("noov:Nvimp_cdia")
                            WHEN '22' THEN 0
                            WHEN '33' THEN :V_Grados
                            WHEN '34' THEN :V_Grados
                            ELSE V_Base
                          END;

      "noov:Nvimp_porc" = CASE TRIM("noov:Nvimp_cdia")
                            WHEN '33' THEN :V_Valor_Sal
                            WHEN '34' THEN :V_Valor_Sal
                            ELSE :V_Tarifa
                          END;

      "noov:Nvimp_valo" = V_Valor;

      IF ("noov:Nvimp_cdia" NOT IN ('22', '99') AND
          NOT("noov:Nvimp_cdia" = '04' AND
          V_Tarifa = 0)) THEN
        SUSPEND;

    END

  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_RENGLON (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Ano              INTEGER;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Porc_Descuento   DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valor            DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
DECLARE VARIABLE V_Grados           DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Sal        DOUBLE PRECISION;
BEGIN

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  V_Num_Linea = 0;
  FOR SELECT Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Aiu,
             Referencia,
             Stot_Red,
             Grados
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Porc_Descuento,
           :V_Descuento,
           :V_Aiu,
           :V_Referencia,
           :V_Base,
           :V_Grados
  DO
  BEGIN

    IF (V_Num_Linea = 0) THEN
    BEGIN
      V_Cantidad = COALESCE(V_Cantidad, 0);
      V_Bruto = COALESCE(V_Bruto, 0);
      V_Descuento = COALESCE(V_Descuento, 0);
      V_Aiu = COALESCE(V_Aiu, 0);

      IF (V_Aiu = 0) THEN
        V_Aiu = 100;

      V_Esquema_Impuesto = NULL;
      V_Tipo_Impuesto = NULL;

      V_Tipo_Impuesto = NULL;
      Es_Tarifa = NULL;

      /* IMPUESTOS */

      "noov:Nvimp_oper" = 'S';

      FOR SELECT Tipo_Impuesto,
                 Es_Tarifa,
                 SUM(Debito + Credito)
          FROM Reg_Impuestos
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Codigo_Fe <> '00'
          GROUP BY Tipo_Impuesto, Es_Tarifa
          INTO :V_Tipo_Impuesto,
               :Es_Tarifa,
               :V_Valor_Nominal
      DO
      BEGIN
        V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
        V_Tarifa = NULL;

        SELECT Tarifa,
               Valor
        FROM Data_Impuestos
        WHERE Codtipoimpuesto = :V_Tipo_Impuesto
              AND Ano = :V_Ano
        INTO :V_Tarifa,
             :V_Valor_Sal;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        /*IF (V_Porc_Descuento=100) THEN
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100);
ELSE
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento; */

        IF (Es_Tarifa = 'S') THEN
          V_Valor = V_Base * V_Tarifa / 100;

        IF (Es_Tarifa = 'N') THEN
        BEGIN
          IF (V_Cantidad = 0) THEN
            V_Tarifa = 0;
          ELSE
            V_Tarifa = V_Valor_Nominal / V_Cantidad;
          V_Valor = V_Valor_Nominal;
        END

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40)
        FROM Tipoimpuestos
        WHERE Codigo = :V_Tipo_Impuesto
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc";

        IF ("noov:Nvimp_cdia" = '22') THEN
          V_Num_Linea = 1;

        SELECT Nombre_Impuesto
        FROM Pz_Fe_Nombre_Impuesto(:"noov:Nvimp_cdia")
        INTO "noov:Nvimp_desc";

        "noov:Nvimp_base" = CASE TRIM("noov:Nvimp_cdia")
                              WHEN '22' THEN 0
                              WHEN '33' THEN :V_Grados
                              WHEN '34' THEN :V_Grados
                              ELSE V_Base
                            END;

        "noov:Nvimp_porc" = CASE TRIM("noov:Nvimp_cdia")
                              WHEN '33' THEN :V_Valor_Sal
                              WHEN '34' THEN :V_Valor_Sal
                              ELSE V_Tarifa
                            END;

        "noov:Nvimp_valo" = V_Valor;

        IF (NOT("noov:Nvimp_cdia" = '04' AND
            V_Tarifa = 0)) THEN
          SUSPEND;

      END

      /* RETENCIONES */

      "noov:Nvimp_oper" = 'R';
      Es_Tarifa = 'S';

      V_Tipo_Retencion = NULL;
      V_Base = 0;
      V_Valor = 0;

      FOR SELECT Tipo_Retencion,
                 SUM(Base),
                 SUM(Debito + Credito)
          FROM Reg_Retenciones
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Publicar = 'S'
          GROUP BY Tipo_Retencion
          INTO :V_Tipo_Retencion,
               :V_Base,
               :V_Valor

      DO
      BEGIN

        "noov:Nvimp_cdia" = NULL;
        "noov:Nvimp_desc" = NULL;
        "noov:Nvimp_base" = 0;
        "noov:Nvimp_porc" = 0;
        "noov:Nvimp_valo" = 0;
        V_Tarifa = 0;

        V_Base = COALESCE(V_Base, 0);
        V_Valor = COALESCE(V_Valor, 0);

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40),
               Clase
        FROM Tiporetenciones
        WHERE Codigo = :V_Tipo_Retencion
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :Clase;

        SELECT Tarifa
        FROM Data_Retenciones
        WHERE Codtiporetencion = :V_Tipo_Retencion
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Base * V_Tarifa / 100;

        SELECT Nombre_Impuesto
        FROM Pz_Fe_Nombre_Impuesto(:"noov:Nvimp_cdia")
        INTO "noov:Nvimp_desc";

        SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               SUM("noov:Nvimp_base"),
               "noov:Nvimp_porc",
               SUM("noov:Nvimp_valo"),
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
        WHERE Es_Tarifa = 'S'
        GROUP BY 1, 2, 3, 5, 7, 8
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             Es_Tarifa, Clase
    DO
    BEGIN
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
      INTO Valor_Red;
      SUSPEND;
    END

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               SUM("noov:Nvimp_base"),
               SUM("noov:Nvimp_porc"),
               SUM("noov:Nvimp_valo"),
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
        WHERE Es_Tarifa = 'N'
        GROUP BY 1, 2, 3, 7, 8
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             Es_Tarifa, Clase
    DO
    BEGIN
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
      INTO Valor_Red;
      SUSPEND;
    END


END^


CREATE OR ALTER PROCEDURE PZ_FE_NOMBRE_IMPUESTO (
    IMPUESTO_ CHAR(5))
RETURNS (
    NOMBRE_IMPUESTO VARCHAR(80))
AS
BEGIN
 /*
  Nombre_Impuesto = CASE
                      WHEN TRIM(Impuesto_) = '01' THEN 'Impuesto sobre la Ventas'
                      WHEN TRIM(Impuesto_) = '02' THEN 'Impuesto al Consumo Departamental Nominal'
                      WHEN TRIM(Impuesto_) = '04' THEN 'Impuesto Nacional al Consumo'
                      WHEN TRIM(Impuesto_) = '05' THEN 'Retención sobre el IVA'
                      WHEN TRIM(Impuesto_) = '06' THEN 'Retención sobre Renta'
                      WHEN TRIM(Impuesto_) = '07' THEN 'Retención sobre el ICA'
                      WHEN TRIM(Impuesto_) = '08' THEN 'Impuesto al Consumo Departamental Porcentual'
                      WHEN TRIM(Impuesto_) = '20' THEN 'Cuota de Fomento Hortifrutícula'
                      WHEN TRIM(Impuesto_) = '21' THEN 'Impuesto de Timbre'
                      WHEN TRIM(Impuesto_) = '22' THEN 'Impuesto Nacional al Consumo de Bolsa Plástica'
                      WHEN TRIM(Impuesto_) = '23' THEN 'Impuesto Nacional del Carbono'
                      WHEN TRIM(Impuesto_) = '24' THEN 'Impuesto Nacional a los Combustibles'
                      WHEN TRIM(Impuesto_) = '25' THEN 'Sobretasa a los combustibles'
                      WHEN TRIM(Impuesto_) = '26' THEN 'Contribución minoristas (Combustibles)'
                      WHEN TRIM(Impuesto_) = '30' THEN 'Impuesto al Consumo de Datos'
                      WHEN TRIM(Impuesto_) = '34' THEN 'IBUA'
                      WHEN TRIM(Impuesto_) = 'ZZ' THEN 'Otros tributos, tasas, contribuciones, y similares'
                    END;
                    */
  Nombre_Impuesto = CASE
                      WHEN TRIM(Impuesto_) = '01' THEN 'Impuesto sobre la Ventas'
                      WHEN TRIM(Impuesto_) = '02' THEN 'Impto al Consumo Departamental Nominal'
                      WHEN TRIM(Impuesto_) = '04' THEN 'Impuesto Nacional al Consumo'
                      WHEN TRIM(Impuesto_) = '05' THEN 'Retención sobre el IVA'
                      WHEN TRIM(Impuesto_) = '06' THEN 'Retención sobre Renta'
                      WHEN TRIM(Impuesto_) = '07' THEN 'Retención sobre el ICA'
                      WHEN TRIM(Impuesto_) = '08' THEN 'Impto al Consumo Departamental Por'
                      WHEN TRIM(Impuesto_) = '20' THEN 'Cuota de Fomento Hortifrutícula'
                      WHEN TRIM(Impuesto_) = '21' THEN 'Impuesto de Timbre'
                      WHEN TRIM(Impuesto_) = '22' THEN 'Impuesto Nacional al Consumo de Bolsa'
                      WHEN TRIM(Impuesto_) = '23' THEN 'Impuesto Nacional del Carbono'
                      WHEN TRIM(Impuesto_) = '24' THEN 'Impuesto Nacional a los Combustibles'
                      WHEN TRIM(Impuesto_) = '25' THEN 'Sobretasa a los combustibles'
                      WHEN TRIM(Impuesto_) = '26' THEN 'Contribución minoristas (Combustibles)'
                      WHEN TRIM(Impuesto_) = '30' THEN 'Impuesto al Consumo de Datos'
                      WHEN TRIM(Impuesto_) = '34' THEN 'IBUA'
                      WHEN TRIM(Impuesto_) = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                    END;
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_TRANSPORTE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(10),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    ATRI VARCHAR(1),
    NATR VARCHAR(2),
    VATR VARCHAR(20),
    UATR VARCHAR(20),
    CATR VARCHAR(20))
AS
BEGIN

  FOR SELECT Atri,
             Natr,
             Vatr,
             Uatr,
             Catr
      FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
      WHERE Renglon = :Renglon_
      INTO Atri,
           Natr,
           Vatr,
           Uatr,
           Catr
  DO
  BEGIN

    IF (TRIM(COALESCE(Uatr, '')) NOT IN ('KGM', 'GLL')) THEN
      Uatr = 'KGM';

    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
DECLARE VARIABLE V_Docu       CHAR(20);
DECLARE VARIABLE V_Tipo       CHAR(5);
DECLARE VARIABLE V_Prefijo    CHAR(5);
DECLARE VARIABLE V_Numero     CHAR(10);
DECLARE VARIABLE V_Trm        DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha      DATE;
DECLARE VARIABLE V_Lista      CHAR(5);
DECLARE VARIABLE V_Secundario CHAR(1);
DECLARE VARIABLE V_Tesoreria  CHAR(10);
BEGIN
  -- Se valida el tipo de documento
  SELECT Codigo_Fe,
         M_Aplica,
         Tesoreria
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Secundario,
       V_Tesoreria;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
  BEGIN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;
  END
  ELSE
  BEGIN
    IF (V_Secundario = 'S' AND
        V_Tesoreria IN ('INGRESO', 'EGRESO')) THEN
      SELECT FIRST 1 Tiporef,
                     Prefijoref,
                     Numeroref
      FROM Tr_Abonos
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO V_Tipo,
           V_Prefijo,
           V_Numero;
  END

  SELECT Trm,
         Fecha,
         Codlista
  FROM Comprobantes
  WHERE Tipo = :V_Tipo
        AND Prefijo = :V_Prefijo
        AND Numero = :V_Numero
  INTO V_Trm,
       V_Fecha,
       V_Lista;

  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista
  INTO Codigo;

  IF (TRIM(COALESCE(Codigo, '')) = '') THEN
    Codigo = 'COP';

  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Valor > 0
        AND Codmoneda = :Codigo
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO Valor;

  Valor = COALESCE(Valor, 1);

  IF (V_Trm > 0) THEN
    Valor = V_Trm;

  IF (Codigo = 'COP') THEN
    Valor = 1;

  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    DESDE DATE,
    HASTA DATE,
    CODREFERIDO CHAR(15))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
BEGIN

  -- datos del tercero
  SELECT Pagador

  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'N') THEN
    SELECT Desde,
           Hasta,
           Codreferido
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Desde,
         Hasta,
         Codreferido;
  ELSE
  BEGIN
    Codreferido = 'NA';
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    SELECT FIRST 1 Desde,
                   Hasta
    FROM Fe_Vector_Salud(:V_Resumen)
    INTO Desde,
         Hasta;
  END
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_BEN (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    BENEFICIARIO_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    NOMBRE CHAR(40),
    APELLIDO CHAR(40),
    CIUDAD CHAR(5),
    DIRECCION CHAR(80),
    PAIS CHAR(80))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Nom1           CHAR(20);
DECLARE VARIABLE V_Nom2           CHAR(20);
DECLARE VARIABLE V_Apl1           CHAR(20);
DECLARE VARIABLE V_Apl2           CHAR(20);
DECLARE VARIABLE V_Pais           CHAR(5);
BEGIN

  Ndoc = :Beneficiario_;
  -- datos del tercero
  SELECT Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2,
         Codmunicipio,
         Direccion,
         Codpais
  FROM Terceros T
  WHERE Codigo = :Beneficiario_
  INTO V_Tipo_Documento,
       V_Apl1,
       V_Apl2,
       V_Nom1,
       V_Nom2,
       Ciudad,
       Direccion,
       V_Pais;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  -- pais
  SELECT Nombre
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Pais;

  Nombre = TRIM(:V_Nom1) || IIF(TRIM(:V_Nom2) = '', '', ' ' || TRIM(:V_Nom2));
  Apellido = TRIM(:V_Apl1) || IIF(TRIM(:V_Apl2) = '', '', ' ' || TRIM(:V_Apl2));

  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_REF (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    NUME CHAR(15),
    CUFD CHAR(96),
    ALGR CHAR(11),
    FEMI CHAR(10),
    IDTO CHAR(5),
    NDOC CHAR(15),
    OPER CHAR(15),
    REPS CHAR(15))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
BEGIN
  -- datos del tercero
  SELECT Pagador
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'S') THEN
  BEGIN
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    FOR SELECT Tipo,
               Prefijo,
               Numero,
               SUBSTRING(Fecha FROM 1 FOR 10),
               Codtercero,
               Reps
        FROM Fe_Vector_Salud(:V_Resumen) S
        INTO :V_Tipo,
             :V_Prefijo,
             :V_Numero,
             Femi,
             Ndoc,
             Reps

    DO
    BEGIN
      Nume = IIF(TRIM(:V_Prefijo) = '_', '', TRIM(:V_Prefijo)) || TRIM(:V_Numero);

      -- BUSCAR CUFE
      Cufd = '';
      SELECT Cufe
      FROM Facturas
      WHERE Tipo = :V_Tipo
            AND Prefijo = :V_Prefijo
            AND Numero = :V_Numero
      INTO Cufd;
      Cufd = COALESCE(Cufd, '');

      -- Tipo de operacion
      SELECT Tipo_Operacion
      FROM Documentos
      WHERE Codigo = :Tipo_
      INTO Oper;
      Algr = '';
      Algr = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN 'CUFE-SHA384'
               WHEN 'SS-CUDE' THEN 'CUDE-SHA384'
               WHEN 'SS-POS' THEN 'POS'
             END;

      Idto = '';
      Idto = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN '01'
               WHEN 'SS-CUDE' THEN '215'
               WHEN 'SS-POS' THEN '230'
             END;

      SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_USU (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    APL1 CHAR(20),
    APL2 CHAR(20),
    NOM1 CHAR(20),
    NOM2 CHAR(20),
    MCON CHAR(5),
    COBE CHAR(5),
    NUMERO_CONTRATO CHAR(15),
    POLIZA CHAR(15),
    COPAGO NUMERIC(18,2),
    CUOTA_MODERADORA NUMERIC(18,2),
    PAGOS_COMPARTIDOS NUMERIC(18,2))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Resumen        CHAR(10);
DECLARE VARIABLE V_Usuario        CHAR(15);
BEGIN

  Ndoc = :Receptor_;
  -- datos del tercero
  SELECT Pagador,
         Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador,
       V_Tipo_Documento,
       Apl1,
       Apl2,
       Nom1,
       Nom2;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  IF (:V_Pagador = 'N') THEN
  BEGIN
    --Usuario
    SELECT Reps,
           Cod_Modalidad,
           Cod_Cobertura,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Reps,
         Mcon,
         Cobe,
         Numero_Contrato,
         Poliza,
         Copago,
         Cuota_Moderadora,
         Pagos_Compartidos;
    SUSPEND;
  END
  ELSE
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

  FOR SELECT Reps,
             Cod_Modalidad,
             Cod_Cobertura,
             Numero_Contrato,
             Poliza,
             Copago,
             Cuota_Moderadora,
             Pagos_Compartidos,
             Codtercero
      FROM Fe_Vector_Salud(:V_Resumen) S
      INTO Reps,
           Mcon,
           Cobe,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos,
           V_Usuario
  DO
  BEGIN
    -- Datos del usuario
    SELECT Codidentidad,
           Apl1,
           Apl2,
           Nom1,
           Nom2
    FROM Terceros
    WHERE Codigo = :v_usuario
    INTO V_Tipo_Documento,
         Apl1,
         Apl2,
         Nom1,
         Nom2;

    SELECT Codigo2
    FROM Identidades
    WHERE Codigo = :V_Tipo_Documento
    INTO :Tdoc;

    SUSPEND;
  END

END^



SET TERM ; ^
COMMIT WORK;

/*  SECTOR SALUD */
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo, :Prefijo, :Numero, ''GESTION'', :Receptor) S', 'S', 35, 'NOOVA', 'FACTURA', 'noov:DTOSectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (274, 'ges:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Apl1 "noov:Nvsal_pape",
       Apl2 "noov:Nvsal_sape",
       Nom1 "noov:Nvsal_pnom",
       Nom2 "noov:Nvsal_snom",
       '''' "noov:Nvsal_tusu",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       '''' "noov:Nvsal_npre",
       '''' "noov:Nvsal_ndip",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo, :Prefijo, :Numero, ''GESTION'', :Receptor)', 'S', 36, 'NOOVA', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuariosSalud', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (275, 'ges:salud_ben', 'DOC', '-- Sector Salud beneficiario

SELECT Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Nombre "noov:Nvsal_nomb",
       Apellido "noov:Nvsal_apel",
       Ciudad "noov:Nvsal_cciu",
       Direccion "noov:Nvsal_dire",
       Pais "noov:Nvsal_pais",
       ''Registraduria Nacional'' "noov:Nvent_nomb"
FROM Pz_Fe_Vector_Salud_Ben(:Tipo, :Prefijo, :Numero, ''GESTION'', :Renglon)
WHERE Tdoc <> ''NA''', 'S', 37, 'NOOVA', 'FACTURA', 'noov:DTOBeneficiario', '', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (276, 'ges:salud_ref', 'DOC', '-- Sector Salud Referenciados

SELECT Nume "noov:Nvdoc_nume",
       Cufd "noov:Nvdoc_cufd",
       Algr "noov:Nvdoc_algr",
       Femi "noov:Nvdoc_femi",
       Idto "noov:Nvdoc_idto",
       Ndoc "noov:Nvusu_ndoc",
       Oper "noov:Nvfor_oper",
       Reps "noov:Nvsal_cpsa",
       '''' "noov:Nvsal_naut"
FROM Pz_Fe_Vector_Salud_Ref(:Tipo, :Prefijo, :Numero, ''GESTION'', :Receptor)', 'S', 38, 'NOOVA', 'FACTURA', 'noov:lDocumentoReferencia', 'noov:DTODocumentoReferencia', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (468, 'con:sector_salud', 'DOC', '-- Sector Salud Contable Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Receptor) S', 'S', 35, 'NOOVAC', 'FACTURA', 'noov:DTOSectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (469, 'con:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Apl1 "noov:Nvsal_pape",
       Apl2 "noov:Nvsal_sape",
       Nom1 "noov:Nvsal_pnom",
       Nom2 "noov:Nvsal_snom",
       '''' "noov:Nvsal_tusu",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       '''' "noov:Nvsal_npre",
       '''' "noov:Nvsal_ndip",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Receptor)', 'S', 36, 'NOOVAC', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuariosSalud', NULL, 'con:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (470, 'con:salud_ben', 'DOC', '-- Sector Salud beneficiario

SELECT Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Nombre "noov:Nvsal_nomb",
       Apellido "noov:Nvsal_apel",
       Ciudad "noov:Nvsal_cciu",
       Direccion "noov:Nvsal_dire",
       Pais "noov:Nvsal_pais",
       ''Registraduria Nacional'' "noov:Nvent_nomb"
FROM Pz_Fe_Vector_Salud_Ben(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Renglon)
WHERE Tdoc <> ''NA''', 'S', 37, 'NOOVAC', 'FACTURA', 'noov:DTOBeneficiario', '', NULL, 'con:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (471, 'con:salud_ref', 'DOC', '-- Sector Salud Referenciados

SELECT Nume "noov:Nvdoc_nume",
       Cufd "noov:Nvdoc_cufd",
       Algr "noov:Nvdoc_algr",
       Femi "noov:Nvdoc_femi",
       Idto "noov:Nvdoc_idto",
       Ndoc "noov:Nvusu_ndoc",
       Oper "noov:Nvfor_oper",
       Reps "noov:Nvsal_cpsa",
       '''' "noov:Nvsal_naut"
FROM Pz_Fe_Vector_Salud_Ref(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Receptor)', 'S', 38, 'NOOVAC', 'FACTURA', 'noov:lDocumentoReferencia', 'noov:DTODocumentoReferencia', NULL, 'con:sector_salud')
                        MATCHING (ID);


COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    DESDE DATE,
    HASTA DATE,
    CODREFERIDO CHAR(15),
    ES_SALUD CHAR(1))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_BEN (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    BENEFICIARIO_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    NOMBRE CHAR(40),
    APELLIDO CHAR(40),
    CIUDAD CHAR(5),
    DIRECCION CHAR(80),
    PAIS CHAR(80))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_REF (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    NUME CHAR(15),
    CUFD CHAR(96),
    ALGR CHAR(11),
    FEMI CHAR(10),
    IDTO CHAR(5),
    NDOC CHAR(15),
    OPER CHAR(15),
    REPS CHAR(15))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_USU (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    APL1 CHAR(20),
    APL2 CHAR(20),
    NOM1 CHAR(20),
    NOM2 CHAR(20),
    MCON CHAR(5),
    COBE CHAR(5),
    NUMERO_CONTRATO CHAR(15),
    POLIZA CHAR(15),
    COPAGO NUMERIC(18,2),
    CUOTA_MODERADORA NUMERIC(18,2),
    PAGOS_COMPARTIDOS NUMERIC(18,2))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    DESDE DATE,
    HASTA DATE,
    CODREFERIDO CHAR(15),
    ES_SALUD CHAR(1))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
BEGIN
  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  -- datos del tercero
  SELECT Pagador

  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'N') THEN
    SELECT Desde,
           Hasta,
           Codreferido
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Desde,
         Hasta,
         Codreferido;
  ELSE
  BEGIN
    Codreferido = 'NA';
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    SELECT FIRST 1 Desde,
                   Hasta
    FROM Fe_Vector_Salud(:V_Resumen)
    INTO Desde,
         Hasta;
  END
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_BEN (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    BENEFICIARIO_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    NOMBRE CHAR(40),
    APELLIDO CHAR(40),
    CIUDAD CHAR(5),
    DIRECCION CHAR(80),
    PAIS CHAR(80))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Nom1           CHAR(20);
DECLARE VARIABLE V_Nom2           CHAR(20);
DECLARE VARIABLE V_Apl1           CHAR(20);
DECLARE VARIABLE V_Apl2           CHAR(20);
DECLARE VARIABLE V_Pais           CHAR(5);
DECLARE VARIABLE Es_Salud         CHAR(1);
BEGIN

  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  Ndoc = :Beneficiario_;
  -- datos del tercero
  SELECT Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2,
         Codmunicipio,
         Direccion,
         Codpais
  FROM Terceros T
  WHERE Codigo = :Beneficiario_
  INTO V_Tipo_Documento,
       V_Apl1,
       V_Apl2,
       V_Nom1,
       V_Nom2,
       Ciudad,
       Direccion,
       V_Pais;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  -- pais
  SELECT Nombre
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Pais;

  Nombre = TRIM(:V_Nom1) || IIF(TRIM(:V_Nom2) = '', '', ' ' || TRIM(:V_Nom2));
  Apellido = TRIM(:V_Apl1) || IIF(TRIM(:V_Apl2) = '', '', ' ' || TRIM(:V_Apl2));

  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_REF (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    NUME CHAR(15),
    CUFD CHAR(96),
    ALGR CHAR(11),
    FEMI CHAR(10),
    IDTO CHAR(5),
    NDOC CHAR(15),
    OPER CHAR(15),
    REPS CHAR(15))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Empresa CHAR(15);
DECLARE VARIABLE Es_Salud  CHAR(1);
BEGIN

  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  -- Datos empresa
  SELECT Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  -- datos del tercero
  SELECT Pagador
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'S') THEN
  BEGIN
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    FOR SELECT Tipo,
               Prefijo,
               Numero,
               SUBSTRING(Fecha FROM 1 FOR 10),
               Codtercero,
               Reps
        FROM Fe_Vector_Salud(:V_Resumen) S
        INTO :V_Tipo,
             :V_Prefijo,
             :V_Numero,
             Femi,
             Ndoc,
             Reps

    DO
    BEGIN
      Nume = IIF(TRIM(:V_Prefijo) = '_', '', TRIM(:V_Prefijo)) || TRIM(:V_Numero);

      -- BUSCAR CUFE
      Cufd = '';
      SELECT Cufe
      FROM Facturas
      WHERE Tipo = :V_Tipo
            AND Prefijo = :V_Prefijo
            AND Numero = :V_Numero
      INTO Cufd;
      Cufd = COALESCE(Cufd, '');

      -- Tipo de operacion
      SELECT Tipo_Operacion
      FROM Documentos
      WHERE Codigo = :Tipo_
      INTO Oper;
      Algr = '';
      Algr = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN 'CUFE-SHA384'
               WHEN 'SS-CUDE' THEN 'CUDE-SHA384'
               WHEN 'SS-POS' THEN 'POS'
             END;

      Idto = '';
      Idto = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN '01'
               WHEN 'SS-CUDE' THEN '215'
               WHEN 'SS-POS' THEN '230'
             END;
      IF (:Ndoc <> :V_Empresa) THEN
        SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_USU (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    APL1 CHAR(20),
    APL2 CHAR(20),
    NOM1 CHAR(20),
    NOM2 CHAR(20),
    MCON CHAR(5),
    COBE CHAR(5),
    NUMERO_CONTRATO CHAR(15),
    POLIZA CHAR(15),
    COPAGO NUMERIC(18,2),
    CUOTA_MODERADORA NUMERIC(18,2),
    PAGOS_COMPARTIDOS NUMERIC(18,2))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Resumen        CHAR(10);
DECLARE VARIABLE V_Usuario        CHAR(15);
DECLARE VARIABLE Es_Salud         CHAR(1);
BEGIN

  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  Ndoc = :Receptor_;
  -- datos del tercero
  SELECT Pagador,
         Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador,
       V_Tipo_Documento,
       Apl1,
       Apl2,
       Nom1,
       Nom2;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  IF (:V_Pagador = 'N') THEN
  BEGIN
    --Usuario
    SELECT Reps,
           Cod_Modalidad,
           Cod_Cobertura,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Reps,
         Mcon,
         Cobe,
         Numero_Contrato,
         Poliza,
         Copago,
         Cuota_Moderadora,
         Pagos_Compartidos;
    SUSPEND;
  END
  ELSE
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

  FOR SELECT Reps,
             Cod_Modalidad,
             Cod_Cobertura,
             Numero_Contrato,
             Poliza,
             Copago,
             Cuota_Moderadora,
             Pagos_Compartidos,
             Codtercero
      FROM Fe_Vector_Salud(:V_Resumen) S
      INTO Reps,
           Mcon,
           Cobe,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos,
           V_Usuario
  DO
  BEGIN
    -- Datos del usuario
    SELECT Codidentidad,
           Apl1,
           Apl2,
           Nom1,
           Nom2
    FROM Terceros
    WHERE Codigo = :V_Usuario
    INTO V_Tipo_Documento,
         Apl1,
         Apl2,
         Nom1,
         Nom2;

    SELECT Codigo2
    FROM Identidades
    WHERE Codigo = :V_Tipo_Documento
    INTO :Tdoc;

    SUSPEND;
  END

END^


SET TERM ; ^
COMMIT WORK;


/* EXPORTADOS GESTION FIN */


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('INVG111002', 'DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO PARCIAL', 'Comparar saldos del sistema con el conteo fisico del inventario solo para las referencias digitadas en el documento SF1 con el que se cargo el inventario fisico.', 'SELECT Fecha_Corte, Referencia, Nombre_Referencia, Bodega, Categoria, Lote,
       Linea, Nombre_Linea, Ubicacion, Saldo, Ponderado, Valor, Conteo,
       Valor_Conteo, Diferencia
FROM Pz_Sel_Conteo_Fisico_P(:A_Tipo, :B_Prefijo, :C_Numero)', 'S', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;


/* TRANSPORTE */

-- Contable
SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_TRANSPORTE (
    TIPO CHAR(5),
    PREFIJO CHAR(10),
    NUMERO CHAR(10))
RETURNS (
    RENGLON INTEGER,
    "noov:Nvfac_atri" VARCHAR(1),
    "noov:Nvfac_natr" VARCHAR(2),
    "noov:Nvfac_vatr" VARCHAR(20),
    "noov:Nvfac_uatr" VARCHAR(20),
    "noov:Nvfac_catr" VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^


SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_TRANSPORTE (
    TIPO CHAR(5),
    PREFIJO CHAR(10),
    NUMERO CHAR(10))
RETURNS (
    RENGLON INTEGER,
    "noov:Nvfac_atri" VARCHAR(1),
    "noov:Nvfac_natr" VARCHAR(2),
    "noov:Nvfac_vatr" VARCHAR(20),
    "noov:Nvfac_uatr" VARCHAR(20),
    "noov:Nvfac_catr" VARCHAR(20))
AS
DECLARE VARIABLE V_Medida   CHAR(5);
DECLARE VARIABLE V_Concepto CHAR(20);
BEGIN

  FOR SELECT Codconcepto,
             Renglon
      FROM Pz_Mov_Patrones(:Tipo, :Prefijo, :Numero)
      INTO V_Concepto,
           Renglon
  DO
  BEGIN
    -- MEDIDA
    SELECT Codmedida
    FROM Auto_Conceptos
    WHERE Codigo = :V_Concepto
    INTO V_Medida;

    FOR SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               COALESCE(Uatr, :V_Medida) "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
        FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero, 'CONTABLE')
        WHERE Renglon = :Renglon

        INTO "noov:Nvfac_atri",
             "noov:Nvfac_natr",
             "noov:Nvfac_vatr",
             "noov:Nvfac_uatr",
             "noov:Nvfac_catr"
    DO
    BEGIN
      SUSPEND;
    END
  END
END^

SET TERM ; ^



/**** MODULO GESTION FINAL                               ****/


/************************************************************/
/**** MODULO CONTABLE                                    ****/
/************************************************************/

/* EXPORTADOS CONTABLE INICIO*/

/* EXPORTADOS CONTABLE  */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVAC';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--964 Contable 25/Abr/2024 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            con:comprobante
            <noov:lDetalle>
                con:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
                con:impuestos
            </noov:lImpuestos>
            con:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVAC');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (461, 'con:comprobante', 'DOC', '-- Comprobante Contable Factura 964 25/Abr/2024 v1

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM((SELECT Valor
                                                 FROM Redondeo_Dian("noov:Nvimp_valo", 2)))
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (462, 'con:detalle', 'DOC', '-- Detalle Contable Factura

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVAC', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (463, 'con:impuestos', 'DOC', '-- Impuestos Contable Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (464, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE FACTURA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (465, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Factura

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 25, 'NOOVAC', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (466, 'con:transporte1', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr"
FROM Pz_Transporte(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvfac_natr" IN (''01'', ''02'')', 'S', 26, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (467, 'con:transporte2', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr",
       "noov:Nvfac_uatr",
       "noov:Nvfac_catr"
FROM Pz_Transporte(:Tipo, :Prefijo, :Numero)  
WHERE "noov:Nvfac_natr" =''03''', 'S', 27, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (481, 'con:comprobante', 'DOC', '-- Comprobante Contable Exportacion

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (482, 'con:detalle', 'DOC', '-- Detalle Contable Exportacion

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (483, 'con:impuestos', 'DOC', '--Impuestos CONTABLE EXPORTACION

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (484, 'con:tasa_cambio', 'DOC', '--Tasa Cambio CONTABLE EXPORTACION

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (501, 'con:comprobante', 'DOC', '--Comprobante CONTABLE CONTINGENCIA

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (502, 'con:detalle', 'DOC', '--Detalle CONTABLE CONTINGENCIA

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (503, 'con:impuestos', 'DOC', '--Impuestos CONTABLE CONTINGENCIA

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (504, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE CONTINGENCIA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (521, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Debito

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian(("noov:Nvfac_desc" + (SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
                                                          FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero))), 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (522, 'con:detalle', 'DOC', '-- Detalle Contable Nota Debito

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (523, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (524, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Debito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
                Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (525, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)  
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA DEBITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (541, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Credito

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (542, 'con:detalle', 'DOC', '-- Detalle Contable Nota Credito

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (543, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (544, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Credito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (545, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)  
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA CREDITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (561, 'con:comprobante', 'DOC', '-- Comprobante Contable Doc Soporte

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (562, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 40, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:Proveedor', NULL, '', 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (565, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Documento Soporte

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (581, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Ajuste a DS

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S''), 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 5, 'NOOVAC', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (582, 'con:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (583, 'con:detalle', 'DOC', '-- Detalle Contable Nota Ajuste a DS

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (584, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota de Ajuste a DS

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (585, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Nota de Ajuste a DS

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


/******************************************************************************/
/***          Generated by IBExpert 2022.3.4.1 06/07/2022 14:26:13          ***/
/******************************************************************************/

/******************************************************************************/
/***      Following SET SQL DIALECT is just for the Database Comparer       ***/
/******************************************************************************/
SET SQL DIALECT 3;



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_COMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_roun" NUMERIC(17,4),
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_DET (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
     "noov:Nvdet_tran" CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20),
    REFERENCIA VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_COMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(10),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_roun" NUMERIC(17,4),
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^



SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_COMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(10),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80))
AS
DECLARE VARIABLE V_Id              INTEGER;
DECLARE VARIABLE V_Plazo           INTEGER;
DECLARE VARIABLE V_Codidentidad    CHAR(5);
DECLARE VARIABLE V_Fecha           CHAR(19);
DECLARE VARIABLE V_Vence           CHAR(19);
DECLARE VARIABLE V_Aiu             INTEGER;
DECLARE VARIABLE V_Manda           INTEGER;
DECLARE VARIABLE V_Transporte      INTEGER;
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Dv              CHAR(1);
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Pais_Fe         VARCHAR(5);
DECLARE VARIABLE V_Muni            VARCHAR(5);
DECLARE VARIABLE V_Nombre_Depa     VARCHAR(80);
DECLARE VARIABLE V_Nombre_Ciud     VARCHAR(80);
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Direccion       VARCHAR(80);
DECLARE VARIABLE V_Telefono        VARCHAR(100);
DECLARE VARIABLE V_Contc           VARCHAR(80);
DECLARE VARIABLE V_Sociedad        VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Fe     VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Nom    VARCHAR(80);
DECLARE VARIABLE V_Autoretenedor   CHAR(1);
DECLARE VARIABLE V_Jerarquia       INTEGER;
DECLARE VARIABLE V_Empresa         VARCHAR(80);
DECLARE VARIABLE V_Nom1            VARCHAR(20);
DECLARE VARIABLE V_Nom2            VARCHAR(20);
DECLARE VARIABLE V_Apl             VARCHAR(40);
DECLARE VARIABLE V_Email           VARCHAR(50);
DECLARE VARIABLE V_Otro_Email      VARCHAR(200);
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Orden           VARCHAR(40);
DECLARE VARIABLE V_Remision        VARCHAR(40);
DECLARE VARIABLE V_Recepcion       VARCHAR(40);
DECLARE VARIABLE V_Ean             VARCHAR(40);
DECLARE VARIABLE V_Centro          VARCHAR(40);
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Tiporef         VARCHAR(5);
DECLARE VARIABLE V_Prefijoref      VARCHAR(5);
DECLARE VARIABLE V_Fecha_Ref       CHAR(19);
DECLARE VARIABLE V_Numeroref       VARCHAR(10);
DECLARE VARIABLE V_Docuref         VARCHAR(20);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     CHAR(1);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
BEGIN

  SELECT Vence
  FROM Val_Documentos
  WHERE Coddocumento = :Tipo_
        AND Codprefijo = :Prefijo_
  INTO V_Plazo;

  SELECT Id,
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Fecha + COALESCE(:V_Plazo, 0)) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || 'T' || SUBSTRING(Hora FROM 1 FOR 8)
  FROM Fe_Apo_Comprobante(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Id,
       V_Fecha,
       V_Vence;

  --Terceros
  SELECT Codidentidad,
         Naturaleza,
         Dv,
         Codpais,
         Codmunicipio,
         Direccion,
         COALESCE(TRIM(Telefono), '') || ', ' || COALESCE(Movil, ''),
         Codsociedad,
         Empresa,
         Nom1,
         Nom2,
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         Email,
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Codidentidad,
       V_Naturaleza,
       V_Dv,
       V_Pais,
       V_Muni,
       V_Direccion,
       V_Telefono,
       V_Sociedad,
       V_Empresa,
       V_Nom1,
       V_Nom2,
       V_Apl,
       V_Email,
       V_Postal_Code;

  --Pais
  SELECT Codigo_Fe
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO V_Pais_Fe;

  --Departamento
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Muni FROM 1 FOR 2)
  INTO V_Nombre_Depa;

  --Municipio
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = :V_Muni
  INTO V_Nombre_Ciud;

  --AIU
  SELECT COUNT(*)
  FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_) P
  WHERE (TRIM("noov:Nvpro_codi") STARTING WITH 'AIU')
        AND (P."noov:Nvfac_cant" * P."noov:Nvfac_valo") > 0
  INTO V_Aiu;

  --Mandatario
  SELECT COUNT(*)
  FROM Auto_Comp M
  JOIN Centros C ON (M.Codcentro = C.Codigo)
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND COALESCE(C.Codtercero, '') <> ''
  INTO V_Manda;

  -- Transportes
  SELECT COUNT(1)
  FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'CONTABLE')
  INTO V_Transporte;

  --Documentos
  SELECT Codigo_Fe,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Valida_Fe;

  --Documento ref
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Contable
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tiporef,
       V_Prefijoref,
       V_Numeroref;

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tiporef
  INTO V_Docuref;

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00'
  FROM Auto_Comp
  WHERE Tipo = :V_Tiporef
        AND Prefijo = :V_Prefijoref
        AND Numero = :V_Numeroref
  INTO V_Fecha_Ref;

  --Contacto
  SELECT FIRST 1 Nombre
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO V_Contc;
  V_Contc = COALESCE(V_Contc, '');

  --Otro email
  SELECT LIST(TRIM(Email), ';')
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Otro_Email;
  V_Contc = COALESCE(V_Contc, '');

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Sociedades
  SELECT Codigo_Fe,
         Nombre,
         Autoretenedor,
         Jerarquia
  FROM Sociedades
  WHERE Codigo = :V_Sociedad
  INTO V_Sociedad_Fe,
       V_Sociedad_Nom,
       V_Autoretenedor,
       V_Jerarquia;

  --Auto Comp
  SELECT Nota,
         Concepto
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Nota,
       V_Concepto_Nc;

  --Mega nota
  SELECT LIST(TRIM(C.Nota), IIF(RIGHT(TRIM(C.Nota), 1) = '.', ASCII_CHAR(13), ' '))
  FROM Auto_Movimiento C
  JOIN Auto_Comp C1 USING (Id)
  WHERE (C1.Tipo = :Tipo_
        AND C1.Prefijo = :Prefijo_
        AND C1.Numero = :Numero_)
        AND (C.Codconcepto = 'MEGANOTA')
        AND (TRIM(COALESCE(C.Nota, '')) <> '')
  INTO V_Meganota;

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Contabilidad
  WHERE Id = :V_Id
  INTO V_Orden,
       V_Remision,
       V_Recepcion,
       V_Ean,
       V_Centro;

  --Impuestos
  SELECT SUM((SELECT Valor
              FROM Redondeo_Dian("noov:Nvimp_valo", 2)))
  FROM Pz_Fe_Auto_Imp(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  -------
  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvres_nume" = (SELECT Numero
                       FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_));
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');
  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);
  "noov:Nvfac_fech" = V_Fecha;
  "noov:Nvfac_venc" = V_Vence;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'
                      END;
  "noov:Nvfac_cdet" = (SELECT COUNT(1)
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_)
                       WHERE ("noov:Nvfac_cant" * "noov:Nvfac_valo") > 0);
  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = 'COP';
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Aiu <> 0 THEN '11'
                                              WHEN V_Manda <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Aiu <> 0 THEN '15'
                                                   WHEN V_Manda <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Aiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Aiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;
  "noov:Nvven_nomb" = '';
  "noov:Nvfac_fpag" = 'ZZZ';
  "noov:Nvfac_conv" = IIF(COALESCE(V_Plazo, 0) <> 0, '2', '1');
  "noov:Nvcli_cper" = IIF(V_Naturaleza = 'N', 2, 1);
  "noov:Nvcli_cdoc" = V_Codidentidad;
  "noov:Nvcli_docu" = TRIM(Receptor_) || IIF(TRIM(V_Codidentidad) = '31', IIF(V_Dv IS NULL, '', '-' || TRIM(V_Dv)), '');
  "noov:Nvcli_pais" = V_Pais_Fe;
  "noov:Nvcli_depa" = V_Nombre_Depa;
  "noov:Nvcli_ciud" = TRIM(V_Nombre_Ciud) || IIF(V_Docu = 'EXPORTACION', '', '@' || TRIM(V_Muni));
  "noov:Nvcli_loca" = '';
  "noov:Nvcli_dire" = V_Direccion;
  "noov:Nvcli_ntel" = V_Telefono;
  "noov:Nvcli_regi" = V_Sociedad_Fe;
  "noov:Nvcli_fisc" = IIF(UPPER(TRIM(V_Sociedad_Nom)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(V_Autoretenedor = 'S', 'O-15' || IIF(V_Jerarquia > 5, ';O-13', ''), IIF(V_Jerarquia > 5, 'O-13', 'R-99-PN')));
  "noov:Nvcli_nomb" = COALESCE(TRIM(V_Empresa), '');
  "noov:Nvcli_pnom" = COALESCE(TRIM(V_Nom1), '');
  "noov:Nvcli_snom" = COALESCE(TRIM(V_Nom2), '');
  "noov:Nvcli_apel" = V_Apl;
  "noov:Nvcli_mail" = TRIM(V_Email) || IIF(V_Otro_Email IS NULL OR TRIM(V_Otro_Email) = '', '', ';' || TRIM(V_Otro_Email));
  "noov:Nvema_copi" = '';
  "noov:Nvfac_obse" = SUBSTRING(TRIM(V_Nota) || IIF(TRIM(COALESCE(V_Meganota, '')) = '', '', ', ' || TRIM(V_Meganota)) FROM 1 FOR 4000);
  "noov:Nvfac_orde" = COALESCE(V_Orden, '');
  "noov:Nvfac_remi" = COALESCE(V_Remision, '');
  "noov:Nvfac_rece" = COALESCE(V_Recepcion, '');
  "noov:Nvfac_entr" = COALESCE(V_Ean, '');
  "noov:Nvfac_ccos" = COALESCE(V_Centro, '');
  "noov:Nvfac_stot" = (SELECT SUM("noov:Nvfac_stot")
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_));

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";
  "noov:Nvfac_totp" = "noov:Nvfac_stot" + V_Impuestos;
  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_obsb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvfac_tcru" = 'R';
    "noov:Nvfac_fecb" = V_Fecha_Ref;
    "noov:Nvcon_codi" = V_Concepto_Nc;
    -- Nombre concepto
    IF (V_Docu = 'NOTA CREDITO') THEN
      SELECT Nombre
      FROM Notas_Cre
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DEBITO') THEN
      SELECT Nombre
      FROM Notas_Deb
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
      SELECT Nombre
      FROM Notas_Ds
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    "noov:Nvfac_numb" = '';

    IF (TRIM(V_Prefijoref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijoref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numeroref);

    IF (V_Valida_Fe = 'N') THEN
    BEGIN
      "noov:Nvfac_tcru" = 'L';
      "noov:Nvfac_fecb" = V_Fecha;
      "noov:Nvfac_numb" = '';
    END
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END

  SUSPEND;
END^



CREATE OR ALTER PROCEDURE Pz_Fe_Auto_Det (
    Tipo_ CHAR(5),
    Prefijo_ CHAR(5),
    Numero_ CHAR(10))
RETURNS (
    Consecutivo INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
     "noov:Nvdet_tran" CHAR(1),
    Stot_Red DOUBLE PRECISION)
AS
DECLARE VARIABLE Referencia CHAR(20);
DECLARE VARIABLE Bruto NUMERIC(17,2);
DECLARE VARIABLE Stot DOUBLE PRECISION;
DECLARE VARIABLE Descuento DOUBLE PRECISION;
DECLARE VARIABLE Porc_Descuento DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa DOUBLE PRECISION;
DECLARE VARIABLE V_Cdia CHAR(5);
DECLARE VARIABLE Nom_Referencia VARCHAR(198);
DECLARE VARIABLE Cantidad DOUBLE PRECISION;
DECLARE VARIABLE V_Valo DOUBLE PRECISION;
DECLARE VARIABLE V_Desc DOUBLE PRECISION;
DECLARE VARIABLE V_Base DOUBLE PRECISION;
DECLARE VARIABLE Nota VARCHAR(200);
DECLARE VARIABLE V_Transporte   INTEGER;
DECLARE VARIABLE V_Linea        CHAR(5);
DECLARE VARIABLE V_Tipo_Linea   CHAR(20);
BEGIN
  FOR SELECT Codconcepto,
             SUBSTRING(IIF(TRIM(Nota) = '', Nombre_Concepto, Nota) FROM 1 FOR 198),
             Cantidad,
             Bruto + Descuento,
             Descuento,
             Nota
      FROM Fe_Apo_Detalle(:Tipo_, :Prefijo_, :Numero_)
      INTO :Referencia,
           :Nom_Referencia,
           :Cantidad,
           :Bruto,
           :Descuento,
           :Nota
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);


    IF (Bruto * Cantidad > 0) THEN
    BEGIN
      Porc_Descuento = Descuento * 100 / Bruto;
      Stot = Cantidad * (Bruto - Descuento);
      -- Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento * :Cantidad, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      V_Tarifa = 0;
      V_Cdia = '00';
      SELECT FIRST 1 "noov:Nvimp_porc",
                     "noov:Nvimp_cdia"

      FROM Pz_Fe_Auto_Imp_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Referencia = :Referencia
      INTO V_Tarifa,
           V_Cdia;

      V_Base = (Bruto - Descuento) * Cantidad;
      V_Valo = V_Base * V_Tarifa / 100;

      Consecutivo = 1;
      "noov:Nvpro_codi" = Referencia;
      "noov:Nvpro_nomb" = Nom_Referencia;
      "noov:Nvuni_desc" = '94';
      "noov:Nvfac_cant" = Cantidad;
      "noov:Nvfac_valo" = Bruto;
      "noov:Nvimp_cdia" = V_Cdia;
      "noov:Nvfac_pdes" = (SELECT Valor
                           FROM Redondeo_Dian(:Porc_Descuento, 2));
      "noov:Nvfac_desc" = Descuento * Cantidad;
      "noov:Nvfac_stot" = Stot;
      "noov:Nvdet_piva" = V_Tarifa;
      "noov:Nvdet_viva" = V_Valo;
      "noov:Nvdet_tcod" = '';
      "noov:Nvpro_cean" = '';
      "noov:Nvdet_entr" = '';
      "noov:Nvuni_quan" = 0;

      "noov:Nvdet_nota" = CASE TRIM(Referencia)
                            WHEN 'AIU_A' THEN 'Contrato de servicios AIU por concepto de: Servicios'
                            ELSE TRIM(Nota)
                          END;

      "noov:Nvdet_padr" = 0;
      "noov:Nvdet_marc" = '';
      "noov:Nvdet_mode" = '';
      "noov:Nvdet_tran" = '';

      -- Transporte de carga
      SELECT COUNT(1)
      FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'CONTABLE')
      INTO V_Transporte;

      IF (V_Transporte > 1) THEN
      BEGIN
        SELECT Codlinea
        FROM Auto_Conceptos
        WHERE Codigo = :Referencia
        INTO V_Linea;

        SELECT Tipo
        FROM Lineas
        WHERE Codigo = :V_Linea
        INTO :V_Tipo_Linea;

        IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE SERVICIO') THEN
          "noov:Nvdet_tran" = '0';
        ELSE
        IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE REMESA') THEN
          "noov:Nvdet_tran" = '1';
      END
      SUSPEND;

    END
  END
END^




CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Cdia             VARCHAR(5);
DECLARE VARIABLE V_Fecha            DATE;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Impuesto         VARCHAR(5);
DECLARE VARIABLE V_Nombre_Imp       VARCHAR(40);
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valo             DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN

  FOR SELECT "noov:Nvimp_cdia",
             "noov:Nvimp_desc",
             "noov:Nvimp_oper",
             SUM("noov:Nvimp_base"),
             "noov:Nvimp_porc",
             SUM("noov:Nvimp_valo"),
             Es_Tarifa,
             Clase
      FROM Pz_Fe_Auto_Imp_Base(:Tipo_, :Prefijo_, :Numero_)
      GROUP BY 1, 2, 3, 5, 7, 8
      ORDER BY 1
      INTO "noov:Nvimp_cdia",
           "noov:Nvimp_desc",
           "noov:Nvimp_oper",
           "noov:Nvimp_base",
           "noov:Nvimp_porc",
           "noov:Nvimp_valo",
           Es_Tarifa,
           Clase
  DO
  BEGIN
    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20),
    REFERENCIA VARCHAR(20))
AS
DECLARE VARIABLE V_Cdia             VARCHAR(5);
DECLARE VARIABLE V_Fecha            DATE;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Impuesto         VARCHAR(5);
DECLARE VARIABLE V_Nombre_Imp       VARCHAR(40);
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valo             DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN
  SELECT Fecha
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Fecha;

  FOR SELECT Codigo_Fe,
             Codtipoimpuesto,
             Bruto,
             Cantidad,
             Codconcepto
      FROM Fe_Apo_Detalle(:Tipo_, :Prefijo_, :Numero_) P
      WHERE Valor * Cantidad > 0
            AND COALESCE(Codigo_Fe, '00') <> '00'
      ORDER BY 1, 2
      INTO V_Cdia,
           V_Impuesto,
           V_Bruto,
           V_Cantidad,
           Referencia
  DO
  BEGIN
    V_Impuesto = COALESCE(V_Impuesto, '');
    SELECT D.Tarifa
    FROM Data_Impuestos D
    WHERE Codtipoimpuesto = :V_Impuesto
          AND D.Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO V_Tarifa;
    V_Tarifa = COALESCE(V_Tarifa, 0);

    SELECT SUBSTRING(Nombre FROM 1 FOR 40)
    FROM Tipoimpuestos
    WHERE Codigo = :V_Impuesto
    INTO V_Nombre_Imp;

    V_Base = V_Bruto * V_Cantidad;
    V_Valo = V_Base * V_Tarifa / 100;
    V_Valo = COALESCE(V_Valo, 0);
    V_Base = COALESCE(V_Base, 0);

    "noov:Nvimp_cdia" = V_Cdia;
    "noov:Nvimp_desc" = CASE
                          WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                          WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                          WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                          WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                          WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                          WHEN V_Tarifa = 19 THEN 'Impuesto sobre la Ventas 19%'
                          WHEN V_Tarifa = 5 THEN 'Impuesto sobre la Ventas 5%'
                          WHEN V_Tarifa = 0 THEN 'Impuesto sobre la Ventas Exento'
                          ELSE V_Nombre_Imp
                        END;
    "noov:Nvimp_oper" = 'S';
    "noov:Nvimp_base" = V_Base;
    "noov:Nvimp_porc" = V_Tarifa;
    "noov:Nvimp_valo" = V_Valo;

    SUSPEND;
  END

  FOR SELECT Codigo_Fe,
             Codtiporetencion,
             Base

      FROM Fe_Apo_Retenciones(:Tipo_, :Prefijo_, :Numero_)
      WHERE Retenciones > 0
      INTO V_Cdia,
           V_Impuesto,
           V_Base

  DO
  BEGIN
    SELECT Tarifa
    FROM Data_Retenciones D
    WHERE D.Codtiporetencion = :V_Impuesto
          AND D.Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO V_Tarifa;

    V_Valo = :V_Base * :V_Tarifa / 100;

    SELECT SUBSTRING(Nombre FROM 1 FOR 40)
    FROM Tiporetenciones
    WHERE Codigo = :V_Impuesto
    INTO V_Nombre_Imp;

    "noov:Nvimp_cdia" = V_Cdia;
    "noov:Nvimp_desc" = V_Nombre_Imp;
    "noov:Nvimp_oper" = 'R';
    "noov:Nvimp_base" = V_Base;
    "noov:Nvimp_porc" = V_Tarifa;
    "noov:Nvimp_valo" = V_Valo;

    SUSPEND;
  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_AUTO_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
DECLARE VARIABLE V_Tercero    VARCHAR(15);
DECLARE VARIABLE V_Lista_Ter  VARCHAR(5);
DECLARE VARIABLE V_Moneda_Ter VARCHAR(5);
DECLARE VARIABLE V_Valor      DOUBLE PRECISION;
DECLARE VARIABLE V_Docuref    VARCHAR(20);
DECLARE VARIABLE V_Docu       VARCHAR(20);
DECLARE VARIABLE V_Tipo       VARCHAR(5);
DECLARE VARIABLE V_Prefijo    VARCHAR(5);
DECLARE VARIABLE V_Numero     VARCHAR(10);
BEGIN
  --Docu
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  --Comprobante
  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
  BEGIN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Contable
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

    SELECT Codigo_Fe
    FROM Documentos
    WHERE Codigo = :V_Tipo
    INTO V_Docuref;
  END

  SELECT Codtercero,
         Fecha
  FROM Fe_Apo_Comprobante(:V_Tipo, :V_Prefijo, :V_Numero)
  INTO V_Tercero,
       Fecha;

  --Lista tercero
  SELECT Codlista
  FROM Terceros
  WHERE Codigo = :V_Tercero
  INTO V_Lista_Ter;

  --Listas
  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista_Ter
  INTO V_Moneda_Ter;
  V_Moneda_Ter = COALESCE(V_Moneda_Ter, 'USD');

  --Tasas
  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Codmoneda = :V_Moneda_Ter
        AND Fecha <= :Fecha
  ORDER BY Fecha DESC
  INTO V_Valor;

  Codigo = CASE V_Docu
             WHEN 'EXPORTACION' THEN V_Moneda_Ter
             WHEN 'NOTA CREDITO' THEN CASE V_Docuref
                                        WHEN 'EXPORTACION' THEN V_Moneda_Ter
                                        ELSE 'COP'
                                      END
             WHEN 'NOTA DEBITO' THEN CASE V_Docuref
                                       WHEN 'EXPORTACION' THEN V_Moneda_Ter
                                       ELSE 'COP'
                                     END
             ELSE 'COP'
           END;
  Valor = CASE V_Docu
            WHEN 'EXPORTACION' THEN V_Valor
            WHEN 'NOTA CREDITO' THEN CASE V_Docuref
                                       WHEN 'EXPORTACION' THEN V_Valor
                                       ELSE 1
                                     END
            WHEN 'NOTA DEBITO' THEN CASE V_Docuref
                                      WHEN 'EXPORTACION' THEN V_Valor
                                      ELSE 1
                                    END
            ELSE 1
          END;

  SUSPEND;

END^



SET TERM ; ^


-- PATRONES
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Mov_Patrones (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Renglon              INTEGER,
    Codconcepto          VARCHAR(20),
    Concepto             VARCHAR(80),
    Bruto                DOUBLE PRECISION,
    Tarifa               NUMERIC(15,4),
    Impuesto             NUMERIC(17,4),
    Valor                NUMERIC(17,4),
    Porcentaje_Descuento NUMERIC(15,4),
    Descuento            NUMERIC(17,4),
    Cantidad             NUMERIC(17,4),
    Nota                 VARCHAR(200))
AS
DECLARE VARIABLE V_Id            INTEGER;
DECLARE VARIABLE V_Fecha         DATE;

DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
BEGIN
  -- ID
  SELECT Id,
         Fecha
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Id,
       V_Fecha;

  FOR SELECT Renglon,
             TRIM(Codconcepto),
             Bruto,
             Impuesto,
             Valor,
             Descuento,
             Cantidad,
             TRIM(Nota)
      FROM Auto_Movimiento
      WHERE Id = :V_Id
            AND Valor > 0
      INTO :Renglon,
           :Codconcepto,
           :Bruto,
           :Impuesto,
           :Valor,
           :Descuento,
           :Cantidad,
           :Nota
  DO
  BEGIN
    -- Concepto
    SELECT TRIM(Nombre)
    FROM Auto_Conceptos
    WHERE Codigo = :Codconcepto
    INTO Concepto;

    -- Tarifa
    SELECT Codtipoimpuesto
    FROM Auto_Impuestos
    WHERE Id = :V_Id
          AND Renglon = :Renglon
    INTO V_Tipo_Impuesto;

    SELECT Tarifa
    FROM Data_Impuestos
    WHERE Codtipoimpuesto = :V_Tipo_Impuesto
          AND Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO Tarifa;

    -- Descuento
    Porcentaje_Descuento = :Descuento * 100 / (:Bruto + :Descuento);

    SUSPEND;
  END
END^

SET TERM ; ^

COMMIT WORK;


SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_TRANSPORTE (
    TIPO CHAR(5),
    PREFIJO CHAR(10),
    NUMERO CHAR(10))
RETURNS (
    RENGLON INTEGER,
    "noov:Nvfac_atri" VARCHAR(1),
    "noov:Nvfac_natr" VARCHAR(2),
    "noov:Nvfac_vatr" VARCHAR(20),
    "noov:Nvfac_uatr" VARCHAR(20),
    "noov:Nvfac_catr" VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_TRANSPORTE (
    TIPO CHAR(5),
    PREFIJO CHAR(10),
    NUMERO CHAR(10))
RETURNS (
    RENGLON INTEGER,
    "noov:Nvfac_atri" VARCHAR(1),
    "noov:Nvfac_natr" VARCHAR(2),
    "noov:Nvfac_vatr" VARCHAR(20),
    "noov:Nvfac_uatr" VARCHAR(20),
    "noov:Nvfac_catr" VARCHAR(20))
AS
DECLARE VARIABLE V_Medida   CHAR(5);
DECLARE VARIABLE V_Concepto CHAR(20);
BEGIN

  FOR SELECT Codconcepto,
             Renglon
      FROM Pz_Mov_Patrones(:Tipo, :Prefijo, :Numero)
      INTO V_Concepto,
           Renglon
  DO
  BEGIN
    -- MEDIDA
    SELECT Codmedida
    FROM Auto_Conceptos
    WHERE Codigo = :V_Concepto
    INTO V_Medida;

    FOR SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               COALESCE(Uatr, :V_Medida) "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
        FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero, 'CONTABLE')
        WHERE Renglon = :Renglon

        INTO "noov:Nvfac_atri",
             "noov:Nvfac_natr",
             "noov:Nvfac_vatr",
             "noov:Nvfac_uatr",
             "noov:Nvfac_catr"
    DO
    BEGIN
      SUSPEND;
    END
  END
END^



SET TERM ; ^


COMMIT WORK;


/* EXPORTADOS CONTABLE FIN*/

/* FECHA: 23ABR2024 */
/* CAMBIOS EXOGENA AG 2023 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_1004 INTEGER;
BEGIN
  V_1004 = 0;
  SELECT COUNT(1)
  FROM Mm_Formatos
  WHERE TRIM(Codigo) = '1004_V8'
  INTO V_1004;

  IF (V_1004 = 0) THEN
  BEGIN
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5086',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR RETEFUENTE TRASLADADA A TERCEROS POR PARTICIPACIONES O DIVIDENDOS RECIBIDOS DE SOC. NACIONALES','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5086',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR RETEFUENTE TRASLADADA A TERCEROS POR PARTICIPACIONES O DIVIDENDOS RECIBIDOS DE SOC. NACIONALES','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5087',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR PUNTOS PREMIO REDIMIDOS EN EL PERIODO QUE AFECTAN EL GASTO,PROCED. DE PROG. DE FIDELIZACION','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5087',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR PUNTOS PREMIO REDIMIDOS EN EL PERIODO QUE AFECTAN EL GASTO,PROCED. DE PROG. DE FIDELIZACION','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1001_V10','5088',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'COSTOS O GASTOS POR DIFERENCIA EN CAMBIO.  SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','100000','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1001_V10','5088',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'COSTOS O GASTOS POR DIFERENCIA EN CAMBIO.  SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','100000','S','N' ) ;
    
    INSERT INTO MM_FORMATOS (CODIGO, NOMBRE, ENTINFO, CPT, TDOC, NID, DV, APL1, APL2, NOM1, NOM2, RAZ, DIR, DPTO, MUN, PAIS, EMAIL, VALOR1, VALOR2, VALOR3, VALOR4, VALOR5, VALOR6, VALOR7, VALOR8, VALOR9, VALOR0, VALOR10, VALOR11, VALOR12, VALOR13, VALOR14, VALOR15, VALOR16, VALOR17, VALOR18, VALOR19, VALOR20, VALOR21, VALOR22, VALOR23, VALOR24, VALOR25, VALOR26, TOTAL, IDEM, TDOCM, NITM, DVM, APL1M, APL2M, NOM1M, NOM2M, RAZM, INFORMAR, CON_INFORMADOS, NELEMENTO, NVALOR1, NVALOR2, NVALOR3, NVALOR4, NVALOR5, NVALOR6, NVALOR7, NVALOR8, NVALOR9, NVALOR0, NVALOR10, NVALOR11, NVALOR12, NVALOR13, NVALOR14, NVALOR15, NVALOR16, NVALOR17, NVALOR18, NVALOR19, NVALOR20, NVALOR21, NVALOR22, NVALOR23, NVALOR24, NVALOR25, NVALOR26, NIDEM, NCPT, REFERENCIA, REVISION, CADENA_ADICIONAL, EQUIVALENCIA, VALOR27, VALOR28, VALOR29, NVALOR27, NVALOR28, NVALOR29)
                 VALUES ('1004_V8', 'DESCUENTOS TRIBUTARIOS SOLICITADOS', NULL, 'S', 'S', 'S', 'N', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'descuentos', 'vdesc', 'vdescsol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cpt', 'DESCUENTOS TRIBUTARIOS SOLICITADOS', 8, NULL, 'nid=nit,nom1=pno,nom2=ono,apl1=pap,apl2=sap', 'N', 'N', 'N', NULL, NULL, NULL);

    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8303',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMP. PAG. EN EL EXT. SOLIC. POR LOS CONTRI. NACIONALES QUE PERCIBAN RENTAS DE FUENTE EXTRANJERA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8303',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMP. PAG. EN EL EXT. SOLIC. POR LOS CONTRI. NACIONALES QUE PERCIBAN RENTAS DE FUENTE EXTRANJERA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8305',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'EMPRESAS DE SERVICIOS PUBLICOS DOMICILIARIOS QUE PRESTEN SERVICIOS DE ACUEDUCTO Y ALCANTARILLADO.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8305',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'EMPRESAS DE SERVICIOS PUBLICOS DOMICILIARIOS QUE PRESTEN SERVICIOS DE ACUEDUCTO Y ALCANTARILLADO.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8316',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES DIRIGIDA A PROGRAMAS DE BECAS O CREDITOS CONDONABLES.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8316',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES DIRIGIDA A PROGRAMAS DE BECAS O CREDITOS CONDONABLES.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8317',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8317',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8318',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES SIN ANIMO DE LUCRO PERTENECIENTES AL REG. TRIBUTARIO ESPECIAL','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8318',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES SIN ANIMO DE LUCRO PERTENECIENTES AL REG. TRIBUTARIO ESPECIAL','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8319',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES S.A.L. NO CONTRIB. DE QUE TRATAN LOS ART. 22 Y 23 DEL E. T.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8319',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES S.A.L. NO CONTRIB. DE QUE TRATAN LOS ART. 22 Y 23 DEL E. T.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8320',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES REALIZADAS EN CONTROL, CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8320',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES REALIZADAS EN CONTROL, CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8321',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EN LA RED NACIONAL DE BIBLIOTECAS PUBLICAS Y BIBLIOTECA NACIONAL','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8321',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EN LA RED NACIONAL DE BIBLIOTECAS PUBLICAS Y BIBLIOTECA NACIONAL','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8322',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES A FAVOR DE FONDO PARA REPARACION DE VICTIMAS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8322',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES A FAVOR DE FONDO PARA REPARACION DE VICTIMAS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8323',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTOS PAGADOS EN EL EXTERIOR POR LA ENTIDAD CONTROLADA DEL EXTERIOR (ECE)','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8323',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTOS PAGADOS EN EL EXTERIOR POR LA ENTIDAD CONTROLADA DEL EXTERIOR (ECE)','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8324',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A LA CORP. GUSTAVO MATAMOROS Y DEMAS FUND. DEDICADAS A LA DEFENSA, PROTECCION DE D.H.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8324',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A LA CORP. GUSTAVO MATAMOROS Y DEMAS FUND. DEDICADAS A LA DEFENSA, PROTECCION DE D.H.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8325',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORGANISMOS DE DEPORTE AFICIONADO. E.T., ART. 126- 2, INCISO.2.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8325',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORGANISMOS DE DEPORTE AFICIONADO. E.T., ART. 126- 2, INCISO.2.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8326',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORG. DEPORTIVOS Y RECREATIVOS O CULTURALES PERS. JURIDICAS SIN ANIMO DE LUCRO','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8326',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORG. DEPORTIVOS Y RECREATIVOS O CULTURALES PERS. JURIDICAS SIN ANIMO DE LUCRO','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8327',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS PARA EL APADRINAMIENTO DE PARQUES NATURALES Y CONSER. DE BOSQUES NATURALES','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8327',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS PARA EL APADRINAMIENTO DE PARQUES NATURALES Y CONSER. DE BOSQUES NATURALES','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8328',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR APORTES AL S.G.P. A CARGO DEL EMPLEADOR CONTRIBUYENTE DEL IMP. UNIF. DEL R.S.T.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8328',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR APORTES AL S.G.P. A CARGO DEL EMPLEADOR CONTRIBUYENTE DEL IMP. UNIF. DEL R.S.T.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8329',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR VENTAS DE BIENES O SERVICIOS CON TARJETAS Y OTROS MECANISMOS ELECTRONICOS DE PAGOS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8329',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR VENTAS DE BIENES O SERVICIOS CON TARJETAS Y OTROS MECANISMOS ELECTRONICOS DE PAGOS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8330',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTO DE INDUSTRIA, COMERCIO, AVISOS Y TABLEROS. E.T., ART. 115 DEL E.T.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8330',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTO DE INDUSTRIA, COMERCIO, AVISOS Y TABLEROS. E.T., ART. 115 DEL E.T.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8331',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IVA DE VENTAS EN LA IMPORT/ FORMACION/CONSTRUC. O ADQ. DE ACTIVOS FIJOS REALES PRODUCTIVOS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8331',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IVA DE VENTAS EN LA IMPORT/ FORMACION/CONSTRUC. O ADQ. DE ACTIVOS FIJOS REALES PRODUCTIVOS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8332',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR CONV. CON COLDEPORTES PARA BECAS DE ESTUDIO Y MANUT. A DEPORTISTAS TALENTO O RESERVA DEPORTIVA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8332',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR CONV. CON COLDEPORTES PARA BECAS DE ESTUDIO Y MANUT. A DEPORTISTAS TALENTO O RESERVA DEPORTIVA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8333',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES EN CONTROL,CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE EN ACTIVIDADES TURISTICAS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8333',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES EN CONTROL,CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE EN ACTIVIDADES TURISTICAS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8334',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8334',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8336',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONAC. RECIB. POR EL FONDO NAL.DE FIN.PARA LA CIENCIA TECN. Y LA INNOV.,FRANCISCO JOSE DE CALDAS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8336',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONAC. RECIB. POR EL FONDO NAL.DE FIN.PARA LA CIENCIA TECN. Y LA INNOV.,FRANCISCO JOSE DE CALDAS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8337',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR REMUN. POR VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTRIB. DE RENTA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8337',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR REMUN. POR VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTRIB. DE RENTA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8338',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES RECIB. POR INTERMEDIO DEL ICETEX,BECAS QUE FINANCIEN LA EDUCACION A LA FUERZA PUBLICA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8338',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES RECIB. POR INTERMEDIO DEL ICETEX,BECAS QUE FINANCIEN LA EDUCACION A LA FUERZA PUBLICA','FINAL','DOS','N' ) ;
    
    UPDATE MM_CONCEPTOS SET TENDENCIA_CREDITO='S' WHERE CODFORMATO='1007_V9' AND VALOR='UNO'; 
    UPDATE MM_CONCEPTOS SET TENDENCIA_CREDITO='N' WHERE CODFORMATO='1007_V9' AND VALOR='DOS'; 
    UPDATE MM_CONCEPTOS SET TOPE='0' WHERE CODFORMATO='1007_V9'; 
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1007_V9','4018',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'INGRESOS POR DIFERENCIA EN CAMBIO. SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','0','S','S' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1007_V9','4018',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'INGRESOS POR DIFERENCIA EN CAMBIO. SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','0','S','S' ) ;  
    
    UPDATE MM_CONCEPTOS SET TOPE='500000' WHERE CODFORMATO='1008_V7';
    UPDATE MM_CONCEPTOS SET TOPE='500000' WHERE CODFORMATO='1009_V7';  
    
    UPDATE MM_CONCEPTOS SET TOPE='3000000' WHERE CODFORMATO='1010_V8';
    
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - RENTAS EXENTAS POR INDEMNIZACIONES POR SEGUROS DE VIDA. E.T. ART. 223' WHERE CODFORMATO='1011_V6' AND CODIGO='8161';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - RENTA EX. POR LA GENERACION Y COMERCIALIZACION DE ENERGIA ELECTRICA' WHERE CODFORMATO='1011_V6' AND CODIGO='8163';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - RENTA EXENTA CREACIONES LITERARIAS DE LA ECONOMIA NARANJA' WHERE CODFORMATO='1011_V6' AND CODIGO='8170';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION POR CONCEPTO DE REGALIAS EN EL PAIS' WHERE CODFORMATO='1011_V6' AND CODIGO='8227';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DED. POR DONAC. E INVER. REALIZ. EN INVES., DESARROLLO TECNOLOGICO E INNOVACION' WHERE CODFORMATO='1011_V6' AND CODIGO='8229';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION POR LAS CONTRIBUCIONES A CARTERAS COLECTIVAS' WHERE CODFORMATO='1011_V6' AND CODIGO='8235';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION IMPUESTOS, REGALIAS Y CONTRIBUCIONES PAG. POR ORGAN. DESCENTRALIZADOS' WHERE CODFORMATO='1011_V6' AND CODIGO='8260';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - COSTOS DE IMPUESTOS DEVENGADOS. E.T., ART. 115' WHERE CODFORMATO='1011_V6' AND CODIGO='8280';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION ESPECIAL DE I.V.A. POR ADQ. O IMP.DE BIENES DE CAPITAL GRAV. A LA T.G' WHERE CODFORMATO='1011_V6' AND CODIGO='8281';
    UPDATE MM_CONCEPTOS SET CODIGO='8405',NOMBRE='DEDUCCION POR DETERIORO ( PROVISION INDIVIDUAL ) DE CARTERA DE DUDOSO O DIFICIL COBRO' WHERE CODFORMATO='1011_V6' AND CODIGO='8205';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION DE IVA EN VENTA DE SERVICIOS DE CORRETAJE DE REASEGUROS' WHERE CODFORMATO='1011_V6' AND CODIGO='9022';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX DE IVA EN ALIM, VEST, E.ASEO, MEDI,BICI, MOTOCI,MOTOCA /AMAZ, GUAI Y VAUPES' WHERE CODFORMATO='1011_V6' AND CODIGO='9040';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX DE IVA EN BICI, MOTOCIC,MOTOCA, Y SUS PARTES DEST. AMAZONAS GUAINIA Y VAUPES' WHERE CODFORMATO='1011_V6' AND CODIGO='9050';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX. TRANS. IVA VENTA DE MAT.PRIMAS QUIMICAS NAL. E IMPOR. PARA LA PROD. DE MED.' WHERE CODFORMATO='1011_V6' AND CODIGO='9063';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION TRANSITORIA DE IVA EN CONTRATOS DE FRANQUICIA' WHERE CODFORMATO='1011_V6' AND CODIGO='9064';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX. DE IVA SERV. ARTIST. PREST. PROD. AUDIOV. DE ESPECT. PUB. DE ARTE ESCENICO' WHERE CODFORMATO='1011_V6' AND CODIGO='9068';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION DEL IVA EN VENTAS SOBRE EL ARRENDAMIENTO DE LOCALES COMERCIALES' WHERE CODFORMATO='1011_V6' AND CODIGO='9070';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION TRANSITORIA IVA EN LA PRESTACION DE SERVICIOS DE HOTELERIA Y TURISMO' WHERE CODFORMATO='1011_V6' AND CODIGO='9071';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA 5% EN LA 1A VENTA DE UND.DE VIVIENDA NUEVA CUYO VALOR SUPERE 26.800 UVT' WHERE CODFORMATO='1011_V6' AND CODIGO='9104';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA DEL 5% POR VENTA DE GASOLINA DE AVIACION JET A1 NACIONALES' WHERE CODFORMATO='1011_V6' AND CODIGO='9108';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA DEL 5% EN EL TRANSPORTE AEREO DE PASAJEROS' WHERE CODFORMATO='1011_V6' AND CODIGO='9109';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA DEL 5% HASTA EL 31/DIC/2022 VENTA DE TIQ.AEREOS DE PASAJ., SERV.CONEXOS' WHERE CODFORMATO='1011_V6' AND CODIGO='9111';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA EN VENTA DE BIOCOMBUSTIBLE' WHERE CODFORMATO='1011_V6' AND CODIGO='9201';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA VENTAS NALES DE PROD. DE USO MEDICO E INSUMOS  SEGUN E.TECNICAS' WHERE CODFORMATO='1011_V6' AND CODIGO='9219';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA VENTAS NALES DE BIENES E INSUMOS MEDICOS SIN DERECHO A DEV.' WHERE CODFORMATO='1011_V6' AND CODIGO='9220';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXE. DE IVA IMPO. Y VENTAS NALES DE PROD. DE USO MEDICO E INS. SEGUN E.TECNICAS' WHERE CODFORMATO='1011_V6' AND CODIGO='9223';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA EN SERVICIOS DE VOZ E INTERNET MOVILES' WHERE CODFORMATO='1011_V6' AND CODIGO='9224';      
    
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8406',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR DETERIORO ( PROVISION GENERAL ) DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8406',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR DETERIORO ( PROVISION GENERAL ) DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8413',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PROVISIONES DE CARTERA HIPOTECARIA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8413',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PROVISIONES DE CARTERA HIPOTECARIA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8414',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE E. P. S. - EPS EN LIQUIDACION FORZOSA INTERVENIDAS POR LA S.N.S.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8414',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE E. P. S. - EPS EN LIQUIDACION FORZOSA INTERVENIDAS POR LA S.N.S.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8415',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8415',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8416',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPL. DE HIDROCARBUROS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8416',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPL. DE HIDROCARBUROS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8417',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE GAS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8417',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE GAS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8418',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE CARBON','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8418',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE CARBON','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8419',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAG. A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE OTROS MINERALES ','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8419',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAG. A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE OTROS MINERALES ','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8420',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE SAL Y MAT. DE C.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8420',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE SAL Y MAT. DE C.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8421',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE ACTIVOS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8421',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE ACTIVOS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8422',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE PLUSVALIA','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8422',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE PLUSVALIA','FINAL','UNO','0','S','N' ) ;

  END

END;

COMMIT WORK;

/* CONTABILIDAD A DOCUMENTO DS*/
UPDATE Documentos
SET Contabilidad = 'S'
WHERE Codigo = 'DS';

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Tipo CHAR(5);
BEGIN
  FOR SELECT DISTINCT Tipo
      FROM Auto_Patrones
      INTO V_Tipo
  DO
  BEGIN
    UPDATE Documentos
    SET Contabilidad = 'S'
    WHERE Codigo = :V_Tipo;
  END
END;
COMMIT WORK; 


/**** MODULO CONTABLE FINAL                              ****/

/************************************************************/
/**** MODULO NOMINA                                      ****/
/************************************************************/

/* EXPORTADOS NOMINA */

DELETE FROM Exportados WHERE Pt = 'NOOVA_NE';

COMMIT WORK;

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:noov="http://noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <noov:Username>UsuarioNoova</noov:Username>
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--963 Nomina 05/ABR/2024 v1-->
    <soapenv:Body>
        <noov:SetNomNominas>
            <noov:nomina>
                nom:nomina
                <noov:Periodo>
                    nom:periodo
                </noov:Periodo>
                <noov:InformacionGeneral>nom:inf_general</noov:InformacionGeneral>
                <noov:LNotas>nom:notas</noov:LNotas>
                <noov:Empleador>nom:empleador</noov:Empleador>
                <noov:Trabajador>nom:trabajador</noov:Trabajador>
                <noov:Pago>nom:pago</noov:Pago>
                <noov:Devengados>
                    <noov:Basico>nom:basico</noov:Basico>
                    nom:transporte
                    <noov:LHorasExtras>nom:horas_extras</noov:LHorasExtras>
                    <noov:LVacaciones>nom:vacaciones</noov:LVacaciones>
                    nom:primas
                    nom:cesantias
                    nom:dotacion
                    nom:indemnizacion
                    <noov:LIncapacidades>nom:incapacidad</noov:LIncapacidades>
                    <noov:LLicencias>nom:licencias</noov:LLicencias>
                    <noov:LBonificaciones>nom:bonificacion</noov:LBonificaciones>
                    <noov:LAuxilios>nom:auxilios</noov:LAuxilios>
                    <noov:LHuelgasLegales>nom:huelgas</noov:LHuelgasLegales>
                    <noov:LBonoEPCTVs>nom:bonos</noov:LBonoEPCTVs>
                    <noov:LComisiones>nom:comision</noov:LComisiones>
                    <noov:LOtrosConceptos>nom:otro_devengados</noov:LOtrosConceptos>
                </noov:Devengados>
                <noov:Deducciones>
                    nom:salud
                    nom:pension
                    nom:fondosp
                    <noov:LSindicatos>nom:sindicato</noov:LSindicatos>
                    <noov:LSanciones>nom:sancion</noov:LSanciones>
                    <noov:LLibranzas>nom:libranzas</noov:LLibranzas>
                    nom:pag_ter
                    nom:anticipos
                    nom:pvoluntaria
                    nom:retencionfuente
                    nom:ahorrofomentoconstr
                    nom:cooperativa
                    nom:embargofiscal
                    nom:plancomplementarios
                    nom:educacion
                    nom:reintegro
                    nom:deuda
                    nom:otras_ded
                </noov:Deducciones>
                nom:predecesor
                </noov:nomina>
        </noov:SetNomNominas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA_NE');


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1005, 'nom:nomina', 'DOC', '-- Encabezado NE 963 Nomina 05/ABR/2024 v1

SELECT 1 "noov:Nvsuc_codi",
       ''NE'' "noov:Nvnom_pref",
       :Numero "noov:Nvnom_cons",
       ''NE'' || :Numero "noov:Nvnom_nume",
       ''NM'' "noov:Nvope_tipo",
       EXTRACT(YEAR FROM CAST(:Fecha_Hasta AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(:Fecha_Hasta AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(:Fecha_Hasta AS DATE)) + 100, 2) "noov:Nvnom_fpag",
       2000 "noov:Nvnom_redo",
       COALESCE(Adicion, 0) "noov:Nvnom_devt",
       COALESCE(Deduccion, 0) "noov:Nvnom_dedt",
       COALESCE(Neto, 0) "noov:Nvnom_comt",
       IIF(:Clase_Fe = ''FACTURA'', '''', ''CUNE'') "noov:Nvnom_cnov",
       (SELECT Ajuste
        FROM Pz_Ne_Ajuste(:Receptor, :Fecha_Desde, :Fecha_Hasta)) "noov:Nvnom_tipo"
FROM Pz_Ne_Resumen(:Receptor, :Fecha_Desde, :Fecha_Hasta)', 'S', 5, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1010, 'nom:periodo', 'DOC', '-- Periodo

WITH V_Fechas
AS (SELECT Codpersonal,
           Desde,
           Hasta,
           MAX(Ingreso) Ingreso,
           MAX(Retiro) Retiro
    FROM (SELECT S.Codpersonal,
                 CAST(:Fecha_Desde AS DATE) Desde,
                 CAST(:Fecha_Hasta AS DATE) Hasta,
                 MAX(S.Desde) Ingreso,
                 CAST(''01/01/1900'' AS DATE) Retiro
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor
                AND S.Columna_Desde = ''INGRESO''
                AND S.Desde <= :Fecha_Hasta
          GROUP BY 1, 2, 3
          UNION
          SELECT S.Codpersonal,
                 CAST(:Fecha_Desde AS DATE),
                 CAST(:Fecha_Hasta AS DATE),
                 CAST(''01/01/1900'' AS DATE),
                 MAX(S.Hasta)
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor
                AND S.Columna_Hasta = ''RETIRO''
                AND S.Hasta <= :Fecha_Hasta
          GROUP BY 1, 2, 3)
    GROUP BY 1, 2, 3)

SELECT EXTRACT(YEAR FROM Fe.Ingreso) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Ingreso) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Ingreso) + 100, 2) "noov:Nvper_fing",
       IIF(Fe.Retiro >= Fe.Desde AND Fe.Retiro <= Fe.Hasta, EXTRACT(YEAR FROM Fe.Retiro) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Retiro) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Retiro) + 100, 2), EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2)) "noov:Nvper_fret",
       EXTRACT(YEAR FROM Fe.Desde) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Desde) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Desde) + 100, 2) "noov:Nvper_fpin",
       EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2) "noov:Nvper_fpfi",
       DATEDIFF(DAY FROM Fe.Ingreso TO Fe.Hasta)+1 "noov:Nvper_tlab"
FROM V_Fechas Fe', 'S', 10, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1015, 'nom:inf_general', 'DOC', '-- Inf general

SELECT (SELECT Tipo_Nomina
        FROM Pz_Ne_Ajuste(:Receptor, :Fecha_Desde, :Fecha_Hasta)) "noov:Nvinf_tnom",
        P.Codtipoperiodo "noov:Nvinf_pnom",
        ''COP'' "noov:Nvinf_tmon"
FROM Personal P
WHERE P.Codigo = :Receptor', 'S', 15, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1020, 'nom:notas', 'DOC', '-- Notas

SELECT DISTINCT N.Nombre "noov:string"
FROM Nominas N
JOIN Planillas P ON (N.Codigo = P.Codnomina)
WHERE P.Codpersonal = :Receptor
      AND N.Fecha >= :Fecha_Desde
      AND N.Fecha <= :Fecha_Hasta', 'S', 20, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1025, 'nom:empleador', 'DOC', '-- Empleador

SELECT FIRST 1 T.Nombre "noov:Nvemp_nomb",
               T.Codigo "noov:Nvemp_nnit",
               T.Dv "noov:Nvemp_endv",
               Pa.Codigo_Fe "noov:Nvemp_pais",
               SUBSTRING(T.Codmunicipio FROM 1 FOR 2) "noov:Nvemp_depa",
               T.Codmunicipio "noov:Nvemp_ciud",
               T.Direccion "noov:Nvemp_dire"
FROM Terceros T
JOIN Paises Pa ON (T.Codpais = Pa.Codigo)
WHERE T.Codigo = :Emisor', 'S', 25, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1030, 'nom:trabajador', 'DOC', '-- Trabajador

WITH V_Correo
AS (SELECT FIRST 1 Codigo,
                   TRIM(Email) Email,
                   COUNT(1) Cant
    FROM Contactos
    WHERE Codcargo = ''NECOR''
          AND Codtercero = :Emisor
    GROUP BY 1, 2
    ORDER BY Codigo)
SELECT LPAD(IIF(TRIM(E.Codcotizante) = ''20'', ''23'', TRIM(E.Codcotizante)), 2, 0) "noov:Nvtra_tipo",
       LPAD(IIF(TRIM(E.Codsubcotizante) <> ''0'', ''1'', TRIM(E.Codsubcotizante)), 2, 0) "noov:Nvtra_stip",
       IIF(COALESCE(E.Alto_Riesgo, 0) = ''N'', ''false'', ''true'') "noov:Nvtra_arpe", --Actividades de alto riesgo
       IIF(E.Codidentidad = ''48'', ''47'', E.Codidentidad) "noov:Nvtra_dtip",
       E.Codigo "noov:Nvtra_ndoc",
       E.Apl1 "noov:Nvtra_pape",
       IIF(COALESCE(E.Apl2, '''') = '''', ''.'', E.Apl2) "noov:Nvtra_sape",
       E.Nom1 "noov:Nvtra_pnom",
       E.Nom2 "noov:Nvtra_onom",
       ''CO'' "noov:Nvtra_ltpa",
       SUBSTRING(E.Codmunicipio FROM 1 FOR 2) "noov:Nvtra_ltde",
       E.Codmunicipio "noov:Nvtra_ltci",
       E.Direccion "noov:Nvtra_ltdi",
       IIF(E.Salario_Integral = ''X'', ''true'', ''false'') "noov:Nvtra_sint",
       C.Codtipocontrato "noov:Nvtra_tcon", --Tipo Contrato
       COALESCE((SELECT FIRST 1 Basico
                 FROM Nom_Pila_Salarios(:Receptor, :Fecha_Desde, :Fecha_Hasta)), 0) "noov:Nvtra_suel",
       E.Codigo "noov:Nvtra_codt",
       IIF(Ne.Cant > 0, COALESCE(Ne.Email, E.Email), ''ne.mekano@gmail.com'') "noov:Nvtra_mail"
FROM Personal E
JOIN Identidades I ON (E.Codidentidad = I.Codigo)
LEFT JOIN Contratacion C ON (E.Codigo = C.Codpersonal)
LEFT JOIN V_Correo Ne ON 1 = 1
WHERE E.Codigo = :Receptor
      AND ((C.Inicio <= :Fecha_Desde
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta)

      OR (C.Inicio >= :Fecha_Desde
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta)

      OR (C.Inicio >= :Fecha_Desde
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta)

      OR (C.Inicio <= :Fecha_Desde
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta))', 'S', 30, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1035, 'nom:pago', 'DOC', '--  Pagos

WITH V_Medio
AS (SELECT COALESCE((SELECT FIRST 1 F.Codigo_Fe
                     FROM Medios_Pago M
                     JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
                     WHERE M.Codpersonal = :Receptor),
           (SELECT FIRST 1 F.Codigo_Fe
            FROM Medios_Pago M
            JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
            WHERE Codpersonal IS NULL)) Medio
    FROM Rdb$Database)
SELECT 1 "noov:Nvpag_form",
       M.Medio "noov:Nvpag_meto", --Metodo de pago
       B.Nombre "noov:Nvpag_banc", --Nombre banco
       Tc.Nombre "noov:Nvpag_tcue",
       E.Banco "noov:Nvpag_ncue"
FROM Personal E
LEFT JOIN Entidades B ON (E.Codentidad = B.Codigo)
LEFT JOIN Tipocuentas Tc ON (E.Codtipocuenta = Tc.Codigo)
JOIN V_Medio M ON (1 = 1)
WHERE E.Codigo = :Receptor', 'S', 35, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1040, 'nom:basico', 'DOC', '-- Basico

SELECT COALESCE((SELECT IIF(ROUND(Valor) = 0, 1, ROUND(Valor))
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DTRA'')), 0) "noov:Nvbas_dtra",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''STRA'')), 0) "noov:Nvbas_stra"
FROM Rdb$Database', 'S', 40, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1042, 'nom:transporte', 'DOC', '-- Transporte NE

SELECT SUM(COALESCE(Valor, 0)) "noov:Nvtrn_auxt",
       SUM(0) "noov:Nvbon_vias",
       SUM(0) "noov:Nvbon_vins"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''AUX'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(COALESCE(Valor, 0)),
       SUM(0)
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''VIAS'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(0),
       SUM(COALESCE(Valor, 0))
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''VINS'')
HAVING SUM(Valor) > 0', 'S', 42, 'NOOVA_NE', 'FACTURA', 'noov:Transporte', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1045, 'nom:horas_extras', 'DOC', '-- Horas Extras NE

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HEN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HENDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HED''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HEDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HRN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HRDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne = ''HRNDF''
      AND Valor > 0', 'S', 45, 'NOOVA_NE', 'FACTURA', 'noov:DTOHorasExtras', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1160, 'nom:vacaciones', 'DOC', '-- Vacaciones NE

SELECT
--'''' "noov:Nvcom_fini",
--      '''' "noov:Nvcom_ffin",
       SUM(IIF(Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q''), IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0)) "noov:Nvcom_cant",
       SUM(IIF(Codgrupo_Ne IN (''VAC1S'', ''VAC2S''), Valor, 0)) "noov:Nvcom_pago",
       Codigo_Ne "noov:Nvvac_tipo"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q'', ''VAC1S'', ''VAC2S'')
GROUP BY 3
HAVING SUM(Valor) > 0', 'S', 160, 'NOOVA_NE', 'FACTURA', 'noov:DTOVacaciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1165, 'nom:primas', 'DOC', '-- Primas NE

SELECT SUM("noov:Nvpri_cant") "noov:Nvpri_cant",
       SUM("noov:Nvpri_pago") "noov:Nvpri_pago",
       SUM("noov:Nvpri_pagn") "noov:Nvpri_pagn"
FROM (SELECT COALESCE(IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0) "noov:Nvpri_cant",
             0 "noov:Nvpri_pago",
             0 "noov:Nvpri_pagn"
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE (Codgrupo_Ne = ''PRIQ'')

      UNION ALL

      SELECT 0,
             COALESCE(Valor, 0),
             0
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE (Codgrupo_Ne = ''PRIP'')
            AND (Valor > 0)

      UNION ALL

      SELECT 0,
             0,
             COALESCE(Valor, 0)
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE (Codgrupo_Ne = ''PRIPN'')
            AND (Valor > 0))
HAVING SUM("noov:Nvpri_cant" + "noov:Nvpri_pago" + "noov:Nvpri_pagn") > 1', 'S', 165, 'NOOVA_NE', 'FACTURA', 'noov:Primas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1170, 'nom:cesantias', 'DOC', '-- Cesantias NE

SELECT COALESCE((SELECT SUM(Valor)
                 FROM Pz_Ne_Grupo_Ces(:Receptor, :Fecha_Desde, :Fecha_Hasta)), 0) "noov:Nvces_pago",
       IIF(COALESCE((SELECT Valor
                     FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''CESPA'')), 0) = 0, 0,
       (SELECT Codigo_Ne
        FROM Grupo_Ne
        WHERE Codigo = ''CESPOR'')) "noov:Nvces_porc",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''CESIPA'')), 0) "noov:Nvces_pagi"
FROM Rdb$Database
WHERE COALESCE((SELECT SUM(Valor)
                FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
                WHERE Codgrupo_Ne IN (''CESPA'', ''CESIPA'')), 0) > 0', 'S', 170, 'NOOVA_NE', 'FACTURA', 'noov:Cesantias', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1175, 'nom:incapacidad', 'DOC', '-- Incapacidades NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvinc_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvinc_tipo"
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE Codgrupo_Ne IN (''INCOQ'', ''INPRQ'', ''INLAQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE Codgrupo_Ne IN (''INCOV'', ''INPRV'', ''INLAV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 175, 'NOOVA_NE', 'FACTURA', 'noov:DTOIncapacidad', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1195, 'nom:licencias', 'DOC', '-- Licencias NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvlic_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvlic_tipo"
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE Codgrupo_Ne IN (''LMQ'', ''LRQ'', ''LNQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
      WHERE Codgrupo_Ne IN (''LMV'', ''LRV'', ''LNV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 195, 'NOOVA_NE', 'FACTURA', 'noov:DTOLicencia', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1197, 'nom:bonificacion', 'DOC', '-- Bonificacion NE

SELECT COALESCE(Valor, 0) "noov:Nvbon_bofs",
       0 "noov:Nvbon_bons"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''BOFS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''BONS'')
WHERE (Valor > 0)', 'S', 197, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonificacion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1199, 'nom:auxilios', 'DOC', '-- Auxilios

SELECT COALESCE(Valor, 0) "noov:Nvaux_auxs",
       0 "noov:Nvaux_auns"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''AUXS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''AUNS'')
WHERE (Valor > 0)', 'S', 199, 'NOOVA_NE', 'FACTURA', 'noov:DTOAuxilio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1200, 'nom:huelgas', 'DOC', '-- Huelgas NE

WITH V_Pago
AS (SELECT Gn.Codigo Tipo,
           SUM(P.Adicion + P.Deduccion) Valor
    FROM Planillas P
    JOIN Nominas N ON (P.Codnomina = N.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
    LEFT JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
    WHERE P.Codpersonal = :Receptor
          AND N.Fecha >= :Fecha_Desde
          AND N.Fecha <= :Fecha_Hasta
          AND R.Codgrupo_Ne = ''HUV''
    GROUP BY 1)

SELECT G.Inicio "noov:Nvcom_fini",
       G.Fin "noov:Nvcom_ffin",
       P.Valor "noov:Nvcom_cant",
       Pago.Valor "noov:Nvcom_pago"
FROM Planillas P
JOIN Nominas N ON (P.Codnomina = N.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
JOIN V_Pago Pago ON (Gn.Codigo = Pago.Tipo)
WHERE P.Codpersonal = :Receptor
      AND N.Fecha >= :Fecha_Desde
      AND N.Fecha <= :Fecha_Hasta
      AND R.Codgrupo_Ne = ''HUQ''', 'S', 235, 'NOOVA_NE', 'FACTURA', 'noov:DTOHuelgaLegal', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1205, 'nom:bonos', 'DOC', '-- Bonos

SELECT COALESCE(IIF(Codgrupo_Ne = ''PAGS'', Valor, 0), 0) "noov:Nvbon_pags",
       COALESCE(IIF(Codgrupo_Ne = ''PANS'', Valor, 0), 0) "noov:Nvbon_pans",
       COALESCE(IIF(Codgrupo_Ne = ''ALIS'', Valor, 0), 0) "noov:Nvbon_alis",
       COALESCE(IIF(Codgrupo_Ne = ''ALNS'', Valor, 0), 0) "noov:Nvbon_alns"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''%'')
WHERE Codgrupo_Ne IN (''PAGS'', ''PANS'', ''ALIS'', ''ALNS'')', 'S', 236, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonoEPCTV', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1210, 'nom:comision', 'DOC', '-- Comisiones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''COMIS'')
WHERE Valor > 0', 'S', 210, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1215, 'nom:dotacion', 'DOC', '-- Dotacion

SELECT Valor "noov:Dotacion"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DOTAC'')
WHERE Valor > 0', 'S', 215, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1240, 'nom:otro_devengados', 'DOC', '-- Otros devengados

SELECT NOMRUBRO "noov:Nvotr_desc",
       IIF(CODGRUPO_NE = ''ODSA'', VALOR, 0) "noov:Nvotr_pags",
       IIF(CODGRUPO_NE = ''ODNS'', VALOR, 0) "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR, :FECHA_DESDE, :FECHA_HASTA)
WHERE (CODGRUPO_NE IN (''ODSA'', ''ODNS''))
      AND (VALOR > 0)

UNION ALL

SELECT ''CAUSACION VACACIONES'' "noov:Nvotr_desc",
       0 "noov:Nvotr_pags",
       VALOR "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR, :FECHA_DESDE, :FECHA_HASTA)
WHERE CODGRUPO_NE = ''ODNS_V''
      AND VALOR > 0', 'S', 240, 'NOOVA_NE', 'FACTURA', 'noov:DTOOtroDevengado', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1355, 'nom:salud', 'DOC', '-- Salud NE

SELECT COALESCE((SELECT IIF(Codcotizante IN (''51'', ''12'', ''19''), 0, Codigo_Ne)
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''EPSDTO'') P), 0) "noov:Nvsal_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''EPSDTO'') P), 0) "noov:Nvsal_dedu"
FROM Rdb$Database', 'S', 355, 'NOOVA_NE', 'FACTURA', 'noov:Salud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1360, 'nom:pension', 'DOC', '-- Pension NE

SELECT COALESCE((SELECT Codigo_Ne
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''AFPDTO'') P), 0) "noov:Nvfon_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''AFPDTO'') P), 0) "noov:Nvfon_dedu"
FROM Rdb$Database', 'S', 360, 'NOOVA_NE', 'FACTURA', 'noov:FondoPension', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1365, 'nom:fondosp', 'DOC', 'SELECT "noov:Nvfsp_porc",
       "noov:Nvfsp_dedu",
       "noov:Nvfsp_posb",
       "noov:Nvfsp_desb"
FROM Pz_Ne_Fondo_Sp(:Receptor, :Fecha_Desde, :Fecha_Hasta)
WHERE "noov:Nvfsp_dedu" + "noov:Nvfsp_desb" > 0', 'S', 365, 'NOOVA_NE', 'FACTURA', 'noov:FondoSP', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1370, 'nom:sindicato', 'DOC', '-- Sindicato NE

SELECT Valor "noov:Nvsin_porc",
       0 "noov:Nvsin_dedu"
FROM Pz_Ne_Grupo_Constante(:Receptor, EXTRACT(YEAR FROM CAST(:Fecha_Desde AS DATE)))
WHERE Codgrupo_Ne = ''PORSIN''
      AND Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo_Constante(:Receptor, EXTRACT(YEAR FROM CAST(:Fecha_Desde AS DATE)))
WHERE Codgrupo_Ne = ''VAL_SIN''
      AND Valor > 0', 'S', 370, 'NOOVA_NE', 'FACTURA', 'noov:DTOSindicato', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1375, 'nom:sancion', 'DOC', '-- Sancion NE

SELECT Valor "noov:Nvsan_sapu",
       0 "noov:Nvsan_sapv"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''SAPU'')
WHERE Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''SAPV'')
WHERE Valor > 0', 'S', 375, 'NOOVA_NE', 'FACTURA', 'noov:DTOSancion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1380, 'nom:libranzas', 'DOC', '-- Libranzas

SELECT Nomrubro "noov:Nvlib_desc",
       Valor "noov:Nvlib_dedu"
FROM Pz_Ne_Grupo_Base(:Receptor, :Fecha_Desde, :Fecha_Hasta)
WHERE Codgrupo_Ne = ''LIBRA''
      AND Valor > 0', 'S', 380, 'NOOVA_NE', 'FACTURA', 'noov:DTOLibranza', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1382, 'nom:pag_ter', 'DOC', '-- Pagos a Terceros

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''PAG_TER'')
WHERE Valor > 0', 'S', 382, 'NOOVA_NE', 'FACTURA', 'noov:LPagosTerceros', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1383, 'nom:anticipos', 'DOC', '-- Anticipos

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT02'')
WHERE Valor > 0', 'S', 383, 'NOOVA_NE', 'FACTURA', 'noov:LAnticipos', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1384, 'nom:pvoluntaria', 'DOC', '-- Pension Voluntaria

SELECT Valor "noov:PensionVoluntaria"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT03'')
WHERE Valor > 0', 'S', 384, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1385, 'nom:retencionfuente', 'DOC', '-- RetencionFuente

SELECT Valor "noov:RetencionFuente"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT04'')
WHERE Valor > 0', 'S', 385, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1386, 'nom:ahorrofomentoconstr', 'DOC', '-- Ahorro Fomento Construccion

SELECT Valor "noov:AhorroFomentoConstr"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT06'')
WHERE Valor > 0', 'S', 386, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1387, 'nom:cooperativa', 'DOC', '-- Cooperativa

SELECT Valor "noov:Cooperativa"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT07'')
WHERE Valor > 0', 'S', 387, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1388, 'nom:embargofiscal', 'DOC', '-- Embargo Fiscal

SELECT Valor "noov:EmbargoFiscal"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT08'')
WHERE Valor > 0', 'S', 388, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1389, 'nom:plancomplementarios', 'DOC', '-- Plan Complementarios

SELECT Valor "noov:PlanComplementarios"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT09'')
WHERE Valor > 0', 'S', 389, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1390, 'nom:educacion', 'DOC', '-- Educacion

SELECT Valor "noov:Educacion"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT10'')
WHERE Valor > 0', 'S', 390, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1391, 'nom:reintegro', 'DOC', '-- Reintegro

SELECT Valor "noov:Reintegro"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT11'')
WHERE Valor > 0', 'S', 391, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1392, 'nom:deuda', 'DOC', '-- Deuda

SELECT Valor "noov:Deuda"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT12'')
WHERE Valor > 0', 'S', 392, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1393, 'nom:otras_ded', 'DOC', '-- Otros deducciones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''DT00'')
WHERE Valor > 0', 'S', 393, 'NOOVA_NE', 'FACTURA', 'noov:LOtrasDeducciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1394, 'nom:predecesor', 'DOC', '-- Predecesor

SELECT Numero "noov:Nvpre_nume",
       Cune "noov:Nvpre_cune",
       EXTRACT(YEAR FROM CAST(Fecha_Ne AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(Fecha_Ne AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(Fecha_Ne AS DATE)) + 100, 2) "noov:Nvpre_fgen"
FROM Pz_Ne_Ajuste(:Receptor, :Fecha_Desde, :Fecha_Hasta)
WHERE trim(Tipo_Nomina) = 103', 'S', 394, 'NOOVA_NE', 'FACTURA', 'noov:Predecesor', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1395, 'nom:indemnizacion', 'DOC', '--INDEMNIZACION

SELECT Valor "noov:indemnizacion"
FROM Pz_Ne_Grupo(:Receptor, :Fecha_Desde, :Fecha_Hasta, ''INDEM'')
WHERE Valor > 0', 'S', 216, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('NOM030220', 'PROMEDIO DE SALARIOS PARA EXOGENA', 'Promedio ultimos 6 meses del AG a reportar. Solo Nomimas nativas', 'SELECT Empleado, Nombre_Empleado, Dias, Acumulado_Devengado, Promedio
FROM Pz_Salario_Exo(:A_Periodo)', 'N', 'NOMINA', 'N', 'MOVIMIENTO NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;

-- SC_NOMINA080
UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=(0)
ELSE
IF(DIAS=0) THEN 
   RESULT:=(0)
ELSE
IF(QUINCENA<2 OR ES_MEDT=1) THEN 
   RESULT:=(0)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=(ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF(SAL_BASICO>=SMLV) THEN 
    RESULT:=(ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE 
    RESULT:=(ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ))))'
WHERE (CODIGO = 'SS017') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=(0)
ELSE
IF(QUINCENA<2) THEN 
   RESULT:=(0)
ELSE
IF(ES_ARL=1) THEN 
    RESULT:=(BASICO)
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=(SMLV)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=(ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
  IF(SAL_BASICO>=SMLV) THEN 
     RESULT:=(ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
  ELSE 
      RESULT:=(ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ))))'
WHERE (CODIGO = 'SS015') AND
      (NATIVO = 'S');



COMMIT WORK;



/**** MODULO NOMINA FINAL                                ****/

/************************************************************/
/**** MODULO REST SERVER                                 ****/
/************************************************************/

/***** MODULO REST SERVER FINAL                         ****/



/************************************************************/
/**** OTROS                                              ****/
/************************************************************/



UPDATE OR INSERT INTO IDENTIDADES (CODIGO, CODIGO2, NOMBRE, CODIGO_NE, CODIGO_SE, MODIFICADO)
                 VALUES ('48', 'PPT', 'PERMISO POR PROTECCION TEMPORAL', NULL, NULL, '2023-05-08');


COMMIT WORK;

/* FORMATO 1647 EXOGENA*/
UPDATE OR INSERT INTO MM_FORMATOS (CODIGO, NOMBRE, ENTINFO, CPT, TDOC, NID, DV, APL1, APL2, NOM1, NOM2, RAZ, DIR, DPTO, MUN, PAIS, EMAIL, VALOR1, VALOR2, VALOR3, VALOR4, VALOR5, VALOR6, VALOR7, VALOR8, VALOR9, VALOR0, VALOR10, VALOR11, VALOR12, VALOR13, VALOR14, VALOR15, VALOR16, VALOR17, VALOR18, VALOR19, VALOR20, VALOR21, VALOR22, VALOR23, VALOR24, VALOR25, VALOR26, TOTAL, IDEM, TDOCM, NITM, DVM, APL1M, APL2M, NOM1M, NOM2M, RAZM, INFORMAR, CON_INFORMADOS, NELEMENTO, NVALOR1, NVALOR2, NVALOR3, NVALOR4, NVALOR5, NVALOR6, NVALOR7, NVALOR8, NVALOR9, NVALOR0, NVALOR10, NVALOR11, NVALOR12, NVALOR13, NVALOR14, NVALOR15, NVALOR16, NVALOR17, NVALOR18, NVALOR19, NVALOR20, NVALOR21, NVALOR22, NVALOR23, NVALOR24, NVALOR25, NVALOR26, NIDEM, NCPT, REFERENCIA, REVISION, CADENA_ADICIONAL, EQUIVALENCIA, VALOR27, VALOR28, VALOR29, NVALOR27, NVALOR28, NVALOR29)
                           VALUES ('1647_V2', 'INFORMACION DE INGRESOS RECIBIDOS PARA TERCEROS', NULL, 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'S', 'S', 'N', 'S', 'S', 'S', 'S', 'S', 'N', 'N', 'ingresos', 'vtotal', 'ving', 'vret', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'con', 'INFORMACION DE INGRESOS RECIBIDOS PARA TERCEROS', 2, 'paist="169"', 'tdocm=tdoc2,nitm=nid2i,dvm=dv2i,razm=razi, apl1m=apl1i,apl2m=apl2i,nom1m=nom1i,nom2m=nom2i,dpto=cdpt,mun=cmcp', NULL, NULL, NULL, NULL, NULL, NULL)
                         MATCHING (CODIGO);

COMMIT WORK;


UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('2', 'Anulacion del Documento Soporte en Adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('3', 'Rebaja o descuento parcial o total')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('4', 'Ajuste de precio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('5', 'Otros')
                      MATCHING (CODIGO);


COMMIT WORK;


-- Actualizar formula de promedio para prima

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DI015+SI050))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE   
  RESULT:=(ROUND((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DI015+SI050))*30))'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualizar formula de promedio para cesantias

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DCES+DI015+SI051)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DCES+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALA-TOTA_CONS+TOTA_SAREP)+(SA027-SA001+DV300)+SI100)/(TOTA_DCES+DI015+SI051))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE 
  RESULT:=(ROUND((((TOTA_SALA-TOTA_CONS+TOTA_SAREP)+(SA027-SA001+DV300)+SI100)/(TOTA_DCES+DI015+SI051))*30))'
WHERE (CODIGO = 'LD107') AND
      (NATIVO = 'S');

COMMIT WORK;
-------
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CTP070110', 'CONTRATOS EMPLEADOS', 'Contratos de empleados', 'SELECT (SELECT TRIM(Nombre_Empleado)
        FROM Fn_Nombre_Empleado(C.Codpersonal)) AS "NOMBRE EMPLEADO",
        C.Codpersonal Codigo,
        Inicio,
        Fin,
        Codtipocontrato Tipo,
        T.Nombre AS Contrato,
        Descripcion,
        Activo
FROM Contratacion C
JOIN Tipocontratos T ON (T.Codigo = C.Codtipocontrato)
ORDER BY 1', 'S', 'NOMINA', 'N', 'PERSONAL NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


-- SC_NOMINA074
-- Expresion del SA011 para incluir a fin de mes en descuentos
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_VAS', 'VALOR VACACIONES SA011 EN LA NOMINA EN EL MISMO MES - SA011', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''SA011'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;


-- Incluir el SA011 en el DT035

UPDATE RUBROS SET 
    FORMULA = 'IF(QUINCENA<2) THEN
RESULT:=(0)
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
RESULT:=(0)
ELSE
IF((ES_INTEGRA=0) AND ((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL+DV300)>(25*SMLV))) THEN
RESULT:=(ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG)+VA080+SA011+ANTE_VAS+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100))
ELSE
IF((ES_INTEGRA=1) AND (((((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL))*C05_S_INT)+HE038+DV997+DV300)>(25*SMLV))) THEN
RESULT:=(ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG)+VA080+SA011+ANTE_VAS+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100))
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=(ROUND((((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG))*C05_S_INT)+SA011+ANTE_VAS+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100))
ELSE
IF((SA027+ANTE_SAL)>=SMLV*4) THEN
RESULT:=(ROUND(((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG))+SA011+ANTE_VAS+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100))
ELSE
RESULT:=(0)'
WHERE (CODIGO = 'DT035') AND
      (NATIVO = 'S');



COMMIT WORK;

-- SC_NOMINA075
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DIVAN', 'DIAS VACACIONES EN NOMINA MISMO PERIODO O MES - DI011', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI011'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;

-- SC_NOMINA076.sql
-- Se crean rubros PR04 para ajustar provisiones prima cesantias e intereses
EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR060' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR040'', ''AJUSTE VALOR PROVISION CESANTIAS'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR061' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR041'', ''AJUSTE VALOR PROVISION PRIMA'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR062' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR042'', ''AJUSTE VALOR PROVISION INTERES A LAS CESANTIAS'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

-- Se actualizan rubros provisiones incluyendo los ajuestes

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_INTEGRA=1 OR ES_ARL=1) THEN
  RESULT:=(0)
ELSE 
  RESULT:=(ROUND(SA028/C22_PR_CES)+PR040)'
WHERE (CODIGO = 'PR060') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_INTEGRA=1 OR ES_ARL=1) THEN
 RESULT:=(0)
ELSE
 RESULT:=(ROUND(((SAL_BASICO/30*(DI001+DI004+DI012))+SA028)/C21_PR_PRI)+PR041)'
WHERE (CODIGO = 'PR061') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'ROUND(PR060*C23_PR_INT)+PR042'
WHERE (CODIGO = 'PR062') AND
      (NATIVO = 'S');

COMMIT WORK;

-- Se incluyen los rubros PR04 en los esquemas que estan las provisiones

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR060' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR040'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR061' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR041'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR062' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR042'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

-- Actualiza rubro LD102 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(TOTA_HE_RE+HE038)
ELSE 
  RESULT:=(ROUND(((TOTA_HE_RE+HE038+SI102)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD102') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualiza rubro LD104 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(TOTA_SAPR+DV997)
ELSE 
  RESULT:=(ROUND(((TOTA_SAPR+DV997+SI104)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD104') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualiza rubro LD106 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE   
  RESULT:=(ROUND((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');



COMMIT WORK;



-- PRESUPUESTO
UPDATE INFORMES SET 
    SQL_CONSULTA = 'SELECT  (P.RUBRO) AS CODRUBRO, (P.NOMBRE_RUBRO) AS "NOMBRE_RUBRO", TRIM(P.NIVEL) AS NIVEL,
COALESCE(P.PLANEADO_ANTERIOR, 0) AS "APROPIACION INICIAL",
COALESCE(P.CREDITO, 0) AS CREDITO,
COALESCE(P.CONTRA, 0) AS CONTRACREDITO,
COALESCE(P.ADICION, 0) AS ADICION,
COALESCE(P.REDUCCION, 0) AS REDUCCION,
COALESCE(P.PLANEADO_FINAL,0) AS  "APROPIACION VIGENTE",  
(IIF(P.APROBADO<0,0,P.APROBADO+P.LIBERACION_CDP)) AS "V/R. CDP - APROBADO",
(P.COMPROMISO+P.LIBERACION_RP) AS "V/R. RP - COMPROMISO",
(P.OBLIGACION) AS "V/R. OBLIGACION",
(P.PAGO) AS "V/R. PAGO",
(IIF(P.PAGO>=P.OBLIGACION,0,(P.OBLIGACION-P.PAGO))) AS "V/R. CXP",
(P.COMPROMISO_FINAL-P.OBLIGACION_FINAL) AS "GASTOS X COMPROMETER" ,
(P.APROBADO_FINAL-P.COMPROMISO_FINAL+(P.LIBERACION_CDP-P.LIBERACION_RP)) AS "CDP X COMPROMETER" ,
(P.PLANEADO_FINAL-P.APROBADO_FINAL-P.LIBERACION_CDP) AS "SALDO X EJECUTAR",
TRIM(T.NOMBRE) AS "NOMBRE RUBRO"
FROM GO_ARBOL_PRESUPUESTO(:PROYECTO,:FECHA_DESDE,:FECHA_HASTA) P
         INNER JOIN P_PPTO T ON (T.CODIGO=P.RUBRO)
WHERE P.RUBRO<>''NA''
ORDER BY 1'
WHERE (CODIGO = 'PPTO00010');

UPDATE INFORMES SET 
    SQL_CONSULTA = 'SELECT  (P.RUBRO) AS CODRUBRO, (P.NOMBRE_RUBRO) AS "NOMBRE RUBRO", TRIM(P.NIVEL) AS NIVEL,
COALESCE(P.PLANEADO_ANTERIOR, 0) AS "APROPIACION INICIAL",
COALESCE(P.CREDITO, 0) AS CREDITO,
COALESCE(P.CONTRA, 0) AS CONTRACREDITO,
COALESCE(P.ADICION, 0) AS ADICION,
COALESCE(P.REDUCCION, 0) AS REDUCCION,
COALESCE(P.PLANEADO_FINAL,0) AS  "APROPIACION VIGENTE",  
COALESCE(P.APROBADO_ANTERIOR,0) AS "CDP ANTERIOR",
COALESCE(P.COMPROMISO_ANTERIOR,0) AS "RP ANTERIOR",
COALESCE(P.OBLIGACION_ANTERIOR,0) AS "OBLIGACION ANTERIOR",
COALESCE(P.PAGO_ANTERIOR,0) AS "PAGO ANTERIOR",
(IIF(P.APROBADO<0,0,P.APROBADO+P.LIBERACION_CDP)) AS "V/R. CDP APROBADO",
(P.COMPROMISO+P.LIBERACION_RP) AS "V/R. RP COMPROMISO",
(P.OBLIGACION) AS "V/R. OBLIGACION",
(P.PAGO) AS "V/R. PAGO",
(IIF(P.PAGO>=P.OBLIGACION,0,(P.OBLIGACION-P.PAGO))) AS "V/R. CXP",
(P.APROBADO_FINAL+P.LIBERACION_CDP) AS "CDP ACUMULADO",
(P.COMPROMISO_FINAL+P.LIBERACION_RP) AS "RP  ACUMULADO",
(P.OBLIGACION_FINAL) AS "OBLIGACION  ACUMULADA",
(P.PAGO_FINAL) AS "PAGO ACUMULADO",    
(P.COMPROMISO_FINAL-P.OBLIGACION_FINAL+P.LIBERACION_RP) AS "GASTOS X COMPROMETER" ,
(P.APROBADO_FINAL-P.COMPROMISO_FINAL+(P.LIBERACION_CDP-P.LIBERACION_RP)) AS "CDP X COMPROMETER" ,
(P.PLANEADO_FINAL-P.APROBADO_FINAL-P.LIBERACION_CDP) AS "SALDO X EJECUTAR" ,
(IIF(P.PAGO_FINAL>=P.OBLIGACION_FINAL,0,(P.OBLIGACION_FINAL-P.PAGO_FINAL))) AS "CXP ACUMULADO",
P.LIBERACION_CDP,
P.LIBERACION_RP      
FROM GO_ARBOL_PRESUPUESTO(:PROYECTO,:FECHA_DESDE,:FECHA_HASTA) P
WHERE P.RUBRO<>''NA''
ORDER BY 1'
WHERE (CODIGO = 'PPTO00100');

COMMIT WORK;

---------960
/* CONTROL VACACIONES */

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('NOM040105', 'CONTROL DE VACACIONES POR EMPLEADO', 'Muestra los periodos vacacionales del empleado seleccionado, es importante que se hallan ingresado los dias habiles cada vez que se ha tomado vacaciones.', 'SELECT Empleado,
       (SELECT Nombre_Empleado
        FROM Fn_Nombre_Empleado(Empleado)) Nombre_Empleado,
       Desde,
       Hasta,
       Dias_Laborados,
       Dias_Periodo,
       Dias_Vac,
       Dias_Pendientes
FROM Pz_Control_Vacaciones(:Empleado, :Fecha_Corte)', 'S', 'NOMINA', 'N', 'MOVIMIENTO NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* HOJAS */
DELETE FROM HOJAS;

UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_001', 'GENERALIDADES - NOMINA', 'GENERALIDADES', 'ITEM, CODPERSONAL, CODRUBRO, CODCENTRO, NOTA, TEXTO, INICIO, FIN, VALOR, CODUSUARIO', 'https://n9.cl/pl_001_generalidades', 'CODPERSONAL, CODRUBRO, INICIO, FIN, VALOR, CODUSUARIO, ITEM')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_002', 'NOVEDADES - NOMINA', 'NOVEDADES', 'ITEM,CODPERSONAL, DIA, CODRUBRO, CODCENTRO, NOTA, VALOR, CODPRENOMINA', 'https://n9.cl/pl_002_novedades', 'ITEM, CODPERSONAL, DIA, CODRUBRO,  VALOR, CODPRENOMINA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_003', 'TERCEROS', 'TERCEROS', 'CODIGO, DV, NATURALEZA, NOM1, NOM2, APL1, APL2, EMPRESA, RAZON_COMERCIAL, DIRECCION, TELEFONO, MOVIL, EMAIL, GERENTE, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODACTIVIDAD, CODZONA', 'https://n9.cl/pl_003_terceros', 'CODIGO, NATURALEZA, DIRECCION, EMAIL, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODZONA, CODACTIVIDAD')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_004', 'EMPLEADOS - NOMINA', 'PERSONAL', 'CODIGO,NOM1, NOM2, APL1, APL2, DIRECCION, TELEFONO, MOVIL, EMAIL, NACIMIENTO, CODCARGO, CODPROFESION, CODCENTRO, CODSOCIEDAD, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODCOTIZANTE, CODSUBCOTIZANTE, CODENTIDAD, CODTIPOCUENTA, BANCO,SALARIO_INTEGRAL,CODTIPOPERIODO, ALTO_RIESGO', 'https://n9.cl/pl_004_empleados', 'CODIGO,NOM1, APL1, CODCARGO, CODPROFESION, CODCENTRO, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODSOCIEDAD, CODCOTIZANTE, CODSUBCOTIZANTE, SALARIO_INTEGRAL, CODTIPOPERIODO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_005', 'CARGOS - EMPLEADOS', 'CARGOS', 'CODIGO, NOMBRE', 'https://n9.cl/pl_005_cargos', 'CODIGO, NOMBRE')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_006', 'CONTRATOS - EMPLEADOS', 'CONTRATACION', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, DESCRIPCION, ACTIVO, CODUSUARIO', 'https://n9.cl/pl_006_contratos', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, ACTIVO, CODUSUARIO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_007', 'APORTES SEGURIDAD SOCIAL Y OTROS - EMPLEADOS', 'APORTES_PERSONAL', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA', 'https://n9.cl/pl_007_aportes', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_008', 'SALARIOS - EMPLEADOS', 'SALARIOS', 'Renglon, Codpersonal, Basico, Desde, Hasta,  Columna_Desde, Columna_Hasta', NULL, 'Renglon, Codpersonal, Basico, Desde, Hasta')
                   MATCHING (CODIGO);

COMMIT WORK;


COMMIT WORK;


/* CLASES */

--DELETE FROM CLASES_RUBROS;
--DELETE FROM CLASES;

UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_DIAS_LABORADOS_VACACIONES', 'DIAS LABORADOS PARA CONTROL DE VACACIONES')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_NO_VACACIONES', 'RUBROS QUE RESTAN DE LOS DIAS DE VACACIONES')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_TOTAL_ADICIONES', 'TOTAL ADICIONES (DEVENGADO)')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_TOTAL_DEDUCIONES', 'TOTAL DEDUCCIONES (DEDUCIDO)')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_HORAS_EXTRAS', 'TOTAL HORAS EXTRAS')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_RECARGOS', 'TOTAL RECARGOS')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_ADICIONES_NO_SALARIALES', 'TOTAL ADICIONES NO SALARIABLES')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_ADICIONES_SALARIALES', 'TOTAL ADICIONES SALARIABLES')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_SALARIO_DEVENGADO', 'SALARIO DEVENGADO')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_RETROACTIVO', 'RETROACTIVO SALARIAL')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_AUXILIO_TRANSPORTE', 'AUXILIO DE TRANSPORTE')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_VACACIONES_COMPENSADAS', 'VACACIONES COMPENSADAS EN DINERO')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_BASE_AUXILIO_TRANSPORTE', 'BASE PARA EL CALCULO DE AUXILIO TRANSPORTE')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_DESCUENTO_EPS', 'DESCUENTO SALUD')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_DESCUENTO_AFP', 'DESCUENTO PENSION')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_OTROS_DESCUENTOS', 'OTROS DESCUENTOS')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_DIAS_AUSENTISMOS', 'DIAS AUSENTISMOS')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_DIAS_SUSPENCIONES', 'DIAS SUSPENCIONES O LICENCIA NO REMUNERADAS')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_DIAS_LICENCIA_REMUNERADA', 'DIAS DE LICENCIA REMUNERADA')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CLASES (CODIGO, NOMBRE)
                      VALUES ('C_SALARIO_PROVISION', 'SALARIO DEVENGADO PARA PROVISION')
                    MATCHING (CODIGO);


COMMIT WORK;


EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV100', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV101', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV102', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV103', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV104', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV105', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV106', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV107', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV108', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV109', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_NO_SALARIALES', 'DV110', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV200', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV201', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV202', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV203', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV204', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV205', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV206', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV207', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV208', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV209', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_ADICIONES_SALARIALES', 'DV210', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_AUXILIO_TRANSPORTE', 'DV030', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_BASE_AUXILIO_TRANSPORTE', 'SA100', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DESCUENTO_AFP', 'DT034', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DESCUENTO_AFP', 'DT035', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DESCUENTO_AFP', 'DT036', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DESCUENTO_AFP', 'DT039', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DESCUENTO_AFP', 'VA035', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DESCUENTO_EPS', 'DT033', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI002', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI003', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI006', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI007', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI009', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI011', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI018', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI019', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_AUSENTISMOS', 'DI020', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_LABORADOS_VACACIONES', 'DI015', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_LABORADOS_VACACIONES', 'SI015', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_LICENCIA_REMUNERADA', 'DI008', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_LICENCIA_REMUNERADA', 'DI021', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_LICENCIA_REMUNERADA', 'DI023', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_SUSPENCIONES', 'DI001', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_SUSPENCIONES', 'DI004', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_DIAS_SUSPENCIONES', 'DI012', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_HORAS_EXTRAS', 'HE020', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_HORAS_EXTRAS', 'HE021', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_HORAS_EXTRAS', 'HE022', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_HORAS_EXTRAS', 'HE023', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT200', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT201', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT202', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT203', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT204', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT205', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT206', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT207', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT208', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT209', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT210', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT211', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT212', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT213', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT214', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT215', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT216', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT217', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT218', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT219', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_OTROS_DESCUENTOS', 'DT220', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_RECARGOS', 'HE024', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_RECARGOS', 'HE025', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_RECARGOS', 'HE026', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_RECARGOS', 'HE027', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_RECARGOS', 'HE028', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_RETROACTIVO', 'DV300', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_SALARIO_DEVENGADO', 'SA011', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_SALARIO_DEVENGADO', 'SA017', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_SALARIO_DEVENGADO', 'SA031', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_SALARIO_DEVENGADO', 'SA032', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_SALARIO_PROVISION', 'SA031', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_SALARIO_PROVISION', 'SA032', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_TOTAL_ADICIONES', 'DV999', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_TOTAL_DEDUCIONES', 'DT999', 'S');
EXECUTE PROCEDURE PZ_INSERTA_RUBRO_CLASE('C_VACACIONES_COMPENSADAS', 'DV040', 'S');

COMMIT WORK;

-- SECTOR POR DEFECTO
UPDATE OR INSERT INTO SECTORES (CODIGO, NOMBRE, IMPRESORA)
                        VALUES ('NA', 'NO APLICA', 'NA')
                      MATCHING (CODIGO);


COMMIT WORK;


-----------------
--SEMAFOROS

EXECUTE BLOCK
AS
DECLARE VARIABLE Cant INTEGER;
BEGIN
  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'BLANCO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'BLANCO');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'VERDE'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'VERDE');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'AMARILLO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'AMARILLO');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'ROJO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'ROJO');
END;

COMMIT WORK;


/* EXPRESIONES TOTA_ */
UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAREP') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SI101'';
DECLARE V_Rubro2       CHAR(5) = ''SA027'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SALAP') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV997'';
DECLARE V_Rubro2       CHAR(5) = ''SI105'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SACES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV997'';
DECLARE V_Rubro2       CHAR(5) = ''SI104'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAPR') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SI100'';
DECLARE V_Rubro2       CHAR(5) = ''SA027'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SALA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total cons cesan
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_CONS') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Dces
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = ''SI051'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_DCES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '-- Total Hces
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''HE038'';
DECLARE V_Rubro2       CHAR(5) = ''SI103'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_HCES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''HE038'';
DECLARE V_Rubro2       CHAR(5) = ''SI102'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_HE_RE') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Dias
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = ''SI050'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_DIAS') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAREC') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--TotalProVaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''PR063'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = '''';
DECLARE V_Rubro_Corte2 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_VAGA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--TotalDiasAnualesVaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = '''';
DECLARE V_Rubro_Corte2 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_VADIA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total cons corte prima
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_CONSP') AND
      (NATIVO = 'S');



COMMIT WORK;

/* VALIDACION VA012 PARA VACACIONES */
UPDATE RUBROS SET 
    FORMULA = 'IF ((VA012>0) AND (DI011>0)) THEN
     RESULT:=(ROUND((BASICO+PM001)/30*DI011))
ELSE
    RESULT:=(0)'
WHERE (CODIGO = 'SA011') AND
      (NATIVO = 'S');

COMMIT WORK;



/* SC_NOMINA081 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Cant INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Conjuntos
  WHERE Codigo = 'PLANILLA83'
  INTO V_Cant;
  IF (V_Cant = 0) THEN
    UPDATE OR INSERT INTO Conjuntos (Codigo, Nombre)
    VALUES ('PLANILLA83', 'D.VAC');

  SELECT COUNT(1)
  FROM Conjuntos
  WHERE Codigo = 'PLANILLA87'
  INTO V_Cant;
  IF (V_Cant = 0) THEN
    UPDATE OR INSERT INTO Conjuntos (Codigo, Nombre)
    VALUES ('PLANILLA87', 'T.GVACA');
END; 

COMMIT WORK;


UPDATE RUBROS SET 
    CODCONJUNTO = 'PLANILLA83'
WHERE (CODIGO = 'PR050') AND
      (NATIVO = 'S') AND
      (CODCONJUNTO IS NULL); 

UPDATE RUBROS SET 
    CODCONJUNTO = 'PLANILLA87'
WHERE (CODIGO = 'PR051') AND
      (NATIVO = 'S') AND
      (CODCONJUNTO IS NULL);

COMMIT WORK;


/* FORMA DE PAGO 01 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_01 INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Formaspago
  WHERE Codigo = '01'
  INTO V_01;

  IF (:V_01 = 0) THEN
    UPDATE Formaspago
    SET Codigo = '01'
    WHERE Codigo = 'EF';
END;
COMMIT WORK;


/* Limite UVT*/

UPDATE Documentos
SET Limite_Uvt = 235325
WHERE Limite_Uvt = 212060 ;
COMMIT WORK;

/*MODIFICACION DE CAMPO VALOR POR VALOR_PAGO EN ARQUEOS*/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG020101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL HOY', 'Permite hacer el arqueo de caja a hoy y agrupado por cajeros. Incluye máquina pos y formas de pago
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       ''• '' || TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO",
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(CURRENT_DATE, CURRENT_DATE) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00010', 'ARQUEO DE CAJA RESUMIDO POR MAQUINA - RANGO FECHA', NULL, 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) AS Forma_Pago,
       TRIM(R.Nombre_Forma_Pago) AS Nombre_Forma_Pago,
       TRIM(R.Computador) AS Maquina,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11
ORDER BY 3, 4', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00000', 'ARQUEO DE CAJA RESUMIDO - RANGO FECHA', NULL, 'SELECT * FROM FX_ARQUEO_RESUMEN(:_DESDE,:_HASTA)', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00100', 'ARQUEO DE CAJA DETALLADO POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - RANGO FECHA', 'Permite en un rango de fecha, generar el arqueo de caja agrupado por una máquina pos o por todas. 
Muestra formas de pago, documento, horario, cajero, vendedor y cliente', 'SELECT TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       R.Valor_Pago,
       R.Transacciones,
       TRIM(R.Forma_Pago) AS Forma_Pago,
       TRIM(R.Nombre_Forma_Pago) AS Nombre_Forma_Pago,
       TRIM(R.Computador) AS Maquina,
       C.Impreso,
       C.Hora,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO",
       TRIM(C.Codusuario) AS Codcajero,
       (SELECT TRIM(Nombre_Usuario)
        FROM Fn_Nombre_Usuario(C.Codusuario)) AS "NOMBRE CAJERO",
       TRIM(R.Vendedor) AS Vendedor,
       TRIM(R.Nombre_Vendedor) AS "NOMBRE VENDEDOR",
       TRIM(C.Codsede) AS Codsede,
       TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND C.Bloqueado = ''S''
ORDER BY 4, 1, 2, 3', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG030101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL UNA FECHA', 'Permite hacer el arqueo de caja agrupado por cajeros según la fecha indicada. Incluye máquina pos y forma de pago
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       C.Impreso,
       ''• '' || TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO",
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor,
       C.Codsede,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:Fecha, :Fecha) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
ORDER BY 6, 8', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG040200', 'ARQUEO DE CAJA DETALLADO PARA MEDIO DE PAGO DIFERENTE A EFECTIVO - RANGO FECHA', 'Muestra por rango de fecha los medios de pago diferentes a efectivo, por un documento, maquina, 
Incluye datos descriptivos de transacciones como autorización pagos con tarjeta y número de cheque', 'SELECT trim(R.TIPO) AS TIPO, TRIM(R.PREFIJO) AS PREFIJO, TRIM(R.NUMERO) AS NUMERO, C.FECHA, TRIM(R.CODFORMASPAGO) AS CODFORMASPAGO,  TRIM(R.DOCUMENTO) AS DOCUMENTO,  C.COMPUTADOR,   
      TRIM(F.Nombre) AS NOM_FORMA_PAGO, R.Valor, C.CODTERCERO,  (SELECT TRIM(NOMBRE_TERCERO) FROM FN_NOMBRE_TERCERO (C.CODTERCERO)) AS "NOMBRE TERCERO", C.CODUSUARIO, C.CODVENDEDOR, C.CODBANCO, C.IMPRESO
      FROM Formaspago F
      JOIN Reg_Pagos R ON (R.Codformaspago = F.Codigo)
      JOIN Comprobantes C ON (R.Tipo = C.Tipo AND R.Prefijo = C.Prefijo AND R.Numero = C.Numero)
      JOIN Documentos D ON (C.Tipo = D.Codigo)
      WHERE TRIM(C.Tipo) LIKE :DOCUMENTO || ''%''
            AND ((C.Fecha >= :_Desde) and (C.fecha<=:_hasta)) 
            AND TRIM(C.Computador) LIKE :Computador || ''%''
            AND D.Grupo = ''VENTA''
            AND D.Es_Compra_Venta = ''S''
            AND D.SIGNO=''CREDITO''    AND TRIM(R.CODFORMASPAGO)<>''01'' AND TRIM(C.CODBANCO) LIKE :CAJA  AND C.BLOQUEADO=''S''         
ORDER BY 1,2,3,4,5,6,7', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG040101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha. Incluye cajeros, formas de pago y vendedor.
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_PAGO) AS Valor,
       TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       C.Impreso,
       C.Codsede,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''PERIODO: '' || EXTRACT(YEAR FROM C.Fecha) || '' / '' || RIGHT(EXTRACT(MONTH FROM C.Fecha) + 100, 2) AS "AÑO Y MES",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/*organizacion de cubos arqueos*/
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010103', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha y por una máquina o por todas. Incluye cajeros, formas de pago y vendedor.', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''AÑO: '' || EXTRACT(YEAR FROM C.Fecha) AS "AÑO",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       ''SEMANA: '' || LPAD(TRIM(EXTRACT(WEEK FROM C.Fecha)), 2, ''0'') AS Semana,
       ''DIA: '' || LPAD(TRIM(EXTRACT(DAY FROM C.Fecha)), 2, ''0'') AS Dia,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010101', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - HOY', 'Permite hacer el arqueo de caja a hoy y agrupado por cajeros. Incluye máquina pos y formas de pago', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(CURRENT_DATE, CURRENT_DATE) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010102', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - UNA FECHA', 'Permite hacer el arqueo de caja agrupado por cajeros según la fecha indicada. Incluye máquina pos y forma de pago', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:Fecha, :Fecha) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG01010', 'ARQUEO DE CAJA DETALLADO MAQUINAS Y/O CAJEROS- RANGO FECHA (AÑO/MES/SEMANA/DIA)', 'Permite hacer el arqueo de caja por un rango de fecha, discriminado por uno o todos los cajeros y/o por una máquina o por todas. Incluye forma de pago y documento', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       ''FECHA:'' || C.Fecha || ''  • '' || TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO",
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''AÑO: '' || EXTRACT(YEAR FROM C.Fecha) AS "AÑO",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       ''SEMANA: '' || LPAD(TRIM(EXTRACT(WEEK FROM C.Fecha)), 2, ''0'') AS Semana,
       ''DIA: '' || LPAD(TRIM(EXTRACT(DAY FROM C.Fecha)), 2, ''0'') AS Dia,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND R.Grupo = ''VENTA''
      AND TRIM(C.Codusuario) LIKE :Cajero
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG020101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL HOY', 'Permite hacer el arqueo de caja a hoy y agrupado por cajeros. Incluye máquina pos y formas de pago
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       ''• '' || TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO",
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(CURRENT_DATE, CURRENT_DATE) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
      AND TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG030101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL UNA FECHA', 'Permite hacer el arqueo de caja agrupado por cajeros según la fecha indicada. Incluye máquina pos y forma de pago
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       ''• '' || TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO",
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:Fecha, :Fecha) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
      AND TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG040101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha. Incluye cajeros, formas de pago y vendedor.
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''AÑO: '' || EXTRACT(YEAR FROM C.Fecha) AS "AÑO",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       ''SEMANA: '' || LPAD(TRIM(EXTRACT(WEEK FROM C.Fecha)), 2, ''0'') AS Semana,
       ''DIA: '' || LPAD(TRIM(EXTRACT(DAY FROM C.Fecha)), 2, ''0'') AS Dia,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
      AND TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);


COMMIT WORK;



/*REPORTES PARA MAILING*/


UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('R_DEC_001', '• Envio Certificado de Retencion en la Fuente', 'Util para validar correos antes de ser enviados desde informes (mailing de correspondencia automatica).
REQUISITO PREVIO: Configurar cuentas y texto desde Menu Contable\Declaraciones, Codigo DEC_001', 'R_TRI_CER_01A.FR3', NULL, 'N', 'CONTABLE', 'A_PERIODO', 'N', 'MAILING', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('R_DEC_002', '• Envio Certificado de Retencion en el ICA', 'Util para validar correos antes de ser enviados desde informes (mailing de correspondencia automatica).
REQUISITO PREVIO: Configurar cuentas y texto desde Menu Contable\Declaraciones, Codigo DEC_002', 'R_TRI_CER_02A.FR3', NULL, 'N', 'CONTABLE', 'A_PERIODO', 'N', 'MAILING', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

/* EXPEDIR CERTIFICADO */

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('TRICER022', 'EXPEDIR CERTIFICADO INGRESOS Y RETENCIONES -EXOGENA 2276_V4 (AG.2022/2023)', 'REQUISITO PREVIO: Configurar cuentas y renglones (campo item) desde Mekano Configuracion\Parametros Medios, Formato 2276_V4
Luego elegir el AG 2022 o 2023', 'R_TRI_CER_V4A.FR3', NULL, 'S', 'CONTABLE', 'A_PERIODO', 'N', 'TRIBUTARIOS EXPEDIR', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;

/*cambio de parametros por error en doble codlinea*/
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTP100511', 'PORTAFOLIO DE REFERENCIAS CON FOTO', 'Listado de productos y servicios con atributo Portafolio, incluye foto y descripción. El precio es tomado de la ventana Referencias
Indicar o buscar código de la Lista de Precios a utilizar', 'CT0310.FR3', NULL, 'S', 'PARAMETROS', 'CODLINEA', 'S', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTP100511B', 'PORTAFOLIO DE REFERENCIAS POR UNA LISTA DE PRECIOS Y CON FOTO..', 'Listado de productos y servicios con atributo Portafolio, incluye foto y descripción. 
El precio con o sin impuestos depende de la Lista de Precios seleccionada
Por una o todas las Lineas', 'CT0310B.FR3', NULL, 'S', 'PARAMETROS', 'LISTA_PRECIOS,CODLINEA', 'N', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 10)
                      MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CONTABLE2G', 'INTERFACE DE GESTION -EXPORTA A EXCEL RUTINA HOJA CALCULO CONTABLE COPIAR PEGAR', 'Permite en un rango de fechas mes, exportar todo el movimiento contable para descargar a excel para luego subirlo a cualquier empresa copiando y pegando por hoja calculo a contable', 'SELECT *
FROM Pz_G_Contable2(:Fecha_Desde, :Fecha_Hasta)', 'S', 'GESTION', 'N', 'RUTINAS MIGRACIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_G_Contable2 (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Tipo_Doc       VARCHAR(5),
    Prefijo        VARCHAR(5),
    Numero         VARCHAR(10),
    Fecha          DATE,
    Cuenta         VARCHAR(30),
    Tercero        VARCHAR(15),
    Centro         VARCHAR(5),
    Detalle        VARCHAR(80),
    Debito         DOUBLE PRECISION,
    Credito        DOUBLE PRECISION,
    Base           DOUBLE PRECISION,
    Usuario        VARCHAR(10),
    Nombre_Tercero VARCHAR(163),
    Nombre_Centro  VARCHAR(80))
AS
DECLARE VARIABLE V_Cod_Tercero VARCHAR(15);
BEGIN
  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Codusuario,
             Codtercero
      FROM Comprobantes
      WHERE Fecha >= :Fecha_Desde
            AND Fecha <= :Fecha_Hasta
      INTO Tipo_Doc,
           Prefijo,
           Numero,
           Fecha,
           Usuario,
           V_Cod_Tercero
  DO
  BEGIN
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Detalle,
               Debito,
               Credito,
               Base
        FROM Reg_Contable
        WHERE Tipo = :Tipo_Doc
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND Codcuenta <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Detalle,
             Debito,
             Credito,
             Base
    DO
    BEGIN
      IF (Tercero IS NULL) THEN
        Tercero = COALESCE(Tercero, '');

      IF (Cuenta = '13050501') THEN
        Tercero = V_Cod_Tercero;

      Centro = COALESCE(Centro, '');
      Detalle = COALESCE(Detalle, '-');

      --MOSTRAMOS NOMBRE TERCERO
      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      --MOSTRAMOS NOMBRE CENTRO

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');
      SUSPEND;

    END
    Detalle = 'JUEGO DE INVENTARIOS';
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Debito,
               Credito,
               Base
        FROM Reg_Juego
        WHERE Tipo = :Tipo_Doc
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND Codcuenta <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Debito,
             Credito,
             Base

    DO
    BEGIN
      IF (Tercero IS NULL) THEN
        Tercero = COALESCE(Tercero, '');

      IF (Cuenta = '13050501') THEN
        Tercero = V_Cod_Tercero;

      Centro = COALESCE(Centro, '');
      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      --MOSTRAMOS NOMBRE CENTRO

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');

      SUSPEND;
    END
  END
END^

SET TERM ; ^

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010109', 'COMISIONES POR RECAUDO AGRUPADO POR TIPO', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por tipo.', 'VEN034.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010111', 'COMISIONES POR VENTA DETALLADO POR VENDEDOR', 'Muestra la relación de Comisiones de Ventas discriminando Vendedor detallado, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN020.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010101', 'COMISIONES POR VENTA', 'Muestra la relación de Comisiones de Ventas discriminando Vendedor, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN001.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010104', 'COMISIONES POR VENTA AGRUPADO POR TIPO - CON DETALLADO POR LINEAS', 'Muestra la relación de Comisiones por Tipo de Venta, discriminando Vendedor, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN002.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR,DOCUMENTO', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010102', 'COMISIONES POR VENTA AGRUPADO POR ZONAS', 'Muestra la relación de Comisiones de Ventas por Zonas, discriminando Vendedor, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN016.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010103', 'COMISIONES POR VENTA AGRUPADO POR LINEAS', 'Muestra la relación de Comisiones de Ventas  discriminando Vendedor por cada factura mostrando por cada Línea la Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN016-1.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010112', 'COMISIÓN POR VENTAS AGRUPADO POR ZONAS DETALLO POR VENDEDOR', 'Muestra la relación de Comisiones de Ventas por Zonas, discriminando Vendedor detallado, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN021.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010106', 'COMISIONES POR RECAUDO', 'Muestra la relación de Comisiones por recaudo, Base Comisión y Valor Comisión.', 'VEN031.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010113', 'COMISIONES POR VENTA AGRUPADO POR LINEAS DETALLADO POR VENDEDOR', 'Muestra la relación de Comisiones de Ventas discriminando Vendedor detallado por cada factura mostrando por cada Línea la Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN022.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010114', 'COMISIONES POR VENTA AGRUPADO POR TIPO - DETALLADO POR LINEAS , VENDEDOR Y DOC', 'Muestra la relación de Comisiones por Tipo de Venta, discriminando Vendedor detallado, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN023.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR,DOCUMENTO', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010110', 'RELACION DE RECAUDOS POR VENDEDOR', 'Muestra la relación de los recaudos realizados por vendedor, discriminando por un rango de fechas el total, retenciones, descuentos y valor neto por cada documento.', 'VEN010.fr3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR,DOCUMENTO', 'S', 'SALDOS DE VENDEDORES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010116', 'COMISIONES POR RECAUDO DETALLADO POR VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión y Valor Comisión. Detallado por vendedor.', 'VEN035.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010117', 'COMISIONES POR RECAUDO AGRUPADO POR ZONAS Y VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por zonas. Detallado por vendedor.', 'VEN036.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010118', 'COMISIONES POR RECAUDO AGRUPADO POR LINEAS POR VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por lineas. Detallado por vendedor.', 'VEN037.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010119', 'COMISIONES POR RECAUDO AGRUPADO POR TIPO Y VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por tipo. Detallado por vendedor.', 'VEN038.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010105', 'RESUMEN COMISIONES POR VENTA', 'Muestra la relación de Comisiones por Venta por Vendedor, Base Comisión, Porcentaje, Valor Comisión y Saldo pendiente.', 'VEN020.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010108', 'COMISIONES POR RECAUDO AGRUPADO POR LINEAS', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por lineas.', 'VEN033.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010107', 'COMISIONES POR RECAUDO AGRUPADO POR ZONAS', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por zonas.', 'VEN032.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010100', 'COMISIONES POR VENTA AGRUPADO POR TIPO DOCUMENTO - RANGO FECHA', 'Por uno o por todos los vendedores. Requisito asignar previamente el porcentaje comision en venta desde la ventana Personal, % por cada línea o generica ''S'' automático por todas.', 'VEN029.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Info_Vtas_Dev (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Clase             VARCHAR(15),
    Cantidad          DOUBLE PRECISION,
    Tipo              VARCHAR(5),
    Prefijo           VARCHAR(5),
    Numero            VARCHAR(10),
    Renglon           INTEGER,
    Bruto             DOUBLE PRECISION,
    Unitario          DOUBLE PRECISION,
    Descuento         DOUBLE PRECISION,
    Gravado           DOUBLE PRECISION,
    No_Gravado        DOUBLE PRECISION,
    Retencion         DOUBLE PRECISION,
    Gran_Total        DOUBLE PRECISION,
    Fecha             DATE,
    Referencia        VARCHAR(20),
    Linea             VARCHAR(5),
    Tercero           VARCHAR(15),
    Usuario           VARCHAR(10),
    Computador        VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Nombre_Linea      VARCHAR(80),
    Nombre_Tercero    VARCHAR(164),
    Nombre_Usuario    VARCHAR(80),
    Desde             DATE,
    Hasta             DATE,
    Totales           DOUBLE PRECISION,
    Codigo2           VARCHAR(20),
    Nombre2           VARCHAR(20),
    Ubicacion         VARCHAR(20),
    Costo             DOUBLE PRECISION,
    Precio            DOUBLE PRECISION,
    Rentabilidad      DOUBLE PRECISION,
    Codmarca          VARCHAR(20),
    Genero            VARCHAR(10),
    Nom_Marca         VARCHAR(80),
    Cod_Genero        VARCHAR(10),
    Nom_Genero        VARCHAR(80))
AS
DECLARE VARIABLE V_Signo VARCHAR(10);
BEGIN

  FOR SELECT P.Cantidad,
             P.Tipo,
             P.Prefijo,
             LPAD(TRIM(P.Numero), 10),
             P.Renglon,
             P.Bruto,
             P.Unitario,
             P.Descuento,
             P.Gravado,
             P.No_Gravado,
             P.Retencion,
             P.Fecha,
             P.Referencia,
             P.Linea,
             P.Tercero,
             P.Usuario,
             P.Computador,
             P.Nombre_Referencia,
             P.Nombre_Linea,
             P.Nombre_Tercero,
             P.Nombre_Usuario,
             P.Desde,
             P.Hasta,
             D.Signo

      FROM Fx_Auxiliar_Pos(:Fecha_Desde, :Fecha_Hasta) P
      JOIN Documentos D ON (P.Tipo = D.Codigo)
      WHERE D.Grupo = 'VENTA'
            AND D.Es_Compra_Venta = 'S'

      INTO Cantidad,
           Tipo,
           Prefijo,
           Numero,
           Renglon,
           Bruto,
           Unitario,
           Descuento,
           Gravado,
           No_Gravado,
           Retencion,
           Fecha,
           Referencia,
           Linea,
           Tercero,
           Usuario,
           Computador,
           Nombre_Referencia,
           Nombre_Linea,
           Nombre_Tercero,
           Nombre_Usuario,
           Desde,
           Hasta,
           V_Signo

  DO
  BEGIN

    IF (V_Signo = 'CREDITO') THEN
    BEGIN
      Clase = '+VENTAS';
      Totales = ((Cantidad * Bruto) + COALESCE(Gravado, 0) + COALESCE(No_Gravado, 0) - COALESCE(Retencion, 0) - (Cantidad * Descuento));
      Gran_Total = ((Bruto - Descuento) * Cantidad);
    END
    ELSE
    BEGIN
      IF (V_Signo = 'DEBITO') THEN
        Clase = '-DEVOLUCION';
      Totales = (((Cantidad * Bruto) + COALESCE(Gravado, 0) + COALESCE(No_Gravado, 0) - COALESCE(Retencion, 0) - (Cantidad * Descuento)) * -1);
      Gran_Total = ((Bruto - Descuento) * Cantidad);
    END
    --Buscar campos en referencias
    SELECT Codigo2,
           Nombre2,
           Ubicacion,
           Costo,
           Precio,
           Rentabilidad,
           Codmarca,
           Codgenero
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Codigo2,
         Nombre2,
         Ubicacion,
         Costo,
         Precio,
         Rentabilidad,
         Codmarca,
         Genero;

    --Buscar Nombre_Marca
    SELECT Nombre
    FROM Marcas
    WHERE Codigo = :Codmarca
    INTO Nom_Marca;

    --Buscar Campos Generos
    SELECT Codigo,
           Nombre
    FROM Generos
    WHERE Codigo = :Genero
    INTO Cod_Genero,
         Nom_Genero;

    SUSPEND;
  END
END^

SET TERM ; ^

COMMIT WORK;



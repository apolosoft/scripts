SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Mensaje_Global_Borra
AS
BEGIN
  UPDATE Mensajes M
  SET Visto = 'S'
  WHERE M.Codusuario_I IS NULL
        AND Visto = 'N';
END^

SET TERM ; ^

EXECUTE PROCEDURE Pz_Mensaje_Global_Borra;
COMMIT WORK;



SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Vector_Salud_Ben (
    Tipo_         CHAR(5),
    Prefijo_      CHAR(5),
    Numero_       CHAR(10),
    Modulo_       CHAR(10),
    Beneficiario_ CHAR(15))
RETURNS (
    Reps      CHAR(15),
    Tdoc      CHAR(5),
    Ndoc      CHAR(15),
    Nombre    CHAR(40),
    Apellido  CHAR(40),
    Ciudad    CHAR(5),
    Direccion CHAR(80),
    Pais      CHAR(80))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Nom1           CHAR(20);
DECLARE VARIABLE V_Nom2           CHAR(20);
DECLARE VARIABLE V_Apl1           CHAR(20);
DECLARE VARIABLE V_Apl2           CHAR(20);
DECLARE VARIABLE V_Pais           CHAR(5);
BEGIN

  Ndoc = :Beneficiario_;
  -- datos del tercero
  SELECT Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2,
         Codmunicipio,
         Direccion,
         Codpais
  FROM Terceros T
  WHERE Codigo = :Beneficiario_
  INTO V_Tipo_Documento,
       V_Apl1,
       V_Apl2,
       V_Nom1,
       V_Nom2,
       Ciudad,
       Direccion,
       V_Pais;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  -- pais
  SELECT Nombre
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Pais;

  Nombre = TRIM(:V_Nom1) || IIF(TRIM(:V_Nom2) = '', '', ' ' || TRIM(:V_Nom2));
  Apellido = TRIM(:V_Apl1) || IIF(TRIM(:V_Apl2) = '', '', ' ' || TRIM(:V_Apl2));

  SUSPEND;
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TERCEROS TO PROCEDURE PZ_FE_VECTOR_SALUD_BEN;
GRANT SELECT ON IDENTIDADES TO PROCEDURE PZ_FE_VECTOR_SALUD_BEN;
GRANT SELECT ON PAISES TO PROCEDURE PZ_FE_VECTOR_SALUD_BEN;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_VECTOR_SALUD_BEN TO SYSDBA;
/* EXPORTADOS GESTION */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVA';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--953 Gestion 29/AGO/2023 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            ges:sector_salud
            ges:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 952 19/MAY/2023 v1

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
      (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
WHERE "noov:Nvfor_oper" = ''SS-CUFE''', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (269, 'ges:transporte1', 'DOC', '-- Vector Transporte

SELECT Atri "noov:Nvfac_atri",
       Natr "noov:Nvfac_natr",
       Vatr "noov:Nvfac_vatr"
FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon
      AND Natr IN (''01'',''02'')', 'S', 22, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (270, 'ges:transporte2', 'DOC', '-- Vector Transporte

SELECT Atri "noov:Nvfac_atri",
       Natr "noov:Nvfac_natr",
       (SELECT Valor
        FROM Redondeo_Dian(Vatr, 0)) "noov:Nvfac_vatr",
       Uatr "noov:Nvfac_uatr",
       (SELECT Valor
        FROM Redondeo_Dian(Catr, 0)) "noov:Nvfac_catr"
FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon
      AND Natr = ''03''', 'S', 23, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
      (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM pz_fe_impuestos_sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
WHERE "noov:Nvfor_oper" = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)
JOIN V_Trm ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)),
V_Pais
AS (SELECT "noov:Nvpro_pais" AS Pais
    FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       IIF(Pais = ''CO'', 0, "noov:Nvdet_piva") "noov:Nvdet_piva",
       IIF(Pais = ''CO'', 0,
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2))) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
JOIN V_Pais  ON (1 = 1)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE  "noov:Nvimp_cdia" NOT IN (''07'', ''99'',''00'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot"/Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti"/Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota"/Trm, 2)) "noov:Nvfac_tota",
       IIF("noov:Nvfac_tipo" IN (''DS'', ''CS''),
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp"/Trm, 2)),
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total/Trm, 2))) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo"/Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red/Trm, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base"/Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo"/Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;



SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    Es_Tarifa         CHAR(1),
    Clase             VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
BEGIN
  /*
SELECT Valor
FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
INTO V_Trm_Valor;
*/
  FOR SELECT Renglon
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      ORDER BY Renglon
      INTO :V_Renglon
  DO
  BEGIN

    /* Iniciar las varibles */
    "noov:Nvimp_cdia" = NULL;
    "noov:Nvimp_desc" = NULL;
    "noov:Nvimp_base" = 0;
    "noov:Nvimp_porc" = 0;
    "noov:Nvimp_valo" = 0;

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               "noov:Nvimp_base",
               "noov:Nvimp_porc",
               "noov:Nvimp_valo",
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :V_Renglon)
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :"noov:Nvimp_oper",
             :"noov:Nvimp_base",
             :"noov:Nvimp_porc",
             :"noov:Nvimp_valo",
             Es_Tarifa,
             Clase

    DO
    BEGIN
      SUSPEND;
    END
  END
END^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Detalle (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10),
    Renglon_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Ano           INTEGER;
DECLARE VARIABLE V_Cantidad      DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto         DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento     DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu           DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa        DOUBLE PRECISION;
DECLARE VARIABLE V_Base          DOUBLE PRECISION;
DECLARE VARIABLE V_Valor         DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
DECLARE VARIABLE V_Es_Tarifa     CHAR(1);
DECLARE VARIABLE V_Valor_Nominal DOUBLE PRECISION;
BEGIN
  /* IMPUESTOS */
  "noov:Nvimp_oper" = 'S';

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  FOR SELECT Cantidad,
             Bruto,
             Descuento,
             Aiu
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Descuento,
           :V_Aiu
  DO
  BEGIN

    V_Cantidad = COALESCE(V_Cantidad, 0);
    V_Bruto = COALESCE(V_Bruto, 0);
    V_Descuento = COALESCE(V_Descuento, 0);
    V_Aiu = COALESCE(V_Aiu, 0);

    IF (V_Aiu = 0) THEN
      V_Aiu = 100;

    V_Tipo_Impuesto = NULL;
    V_Es_Tarifa = NULL;

    FOR SELECT SKIP 1 Tipo_Impuesto,
                      Es_Tarifa,
                      SUM(Debito + Credito)
        FROM Reg_Impuestos
        WHERE Tipo = :Tipo_
              AND Prefijo = :Prefijo_
              AND Numero = :Numero_
              AND Renglon = :Renglon_
        GROUP BY Codigo_Fe, Tipo_Impuesto, Es_Tarifa
        INTO :V_Tipo_Impuesto,
             :V_Es_Tarifa,
             :V_Valor_Nominal
    DO
    BEGIN
      V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
      V_Tarifa = NULL;

      SELECT Tarifa
      FROM Data_Impuestos
      WHERE Codtipoimpuesto = :V_Tipo_Impuesto
            AND Ano = :V_Ano
      INTO :V_Tarifa;

      V_Tarifa = COALESCE(V_Tarifa, 0);
      -- V_Tarifa = CAST(V_Tarifa AS NUMERIC(17,4));

      V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento;
      -- v_base = v_cantidad * (v_bruto - v_descuento) * v_aiu / 100;

      IF (V_Es_Tarifa = 'S') THEN
        V_Valor = V_Base * V_Tarifa / 100;

      IF (V_Es_Tarifa = 'N') THEN
      BEGIN
        IF (V_Cantidad = 0) THEN
          V_Tarifa = 0;
        ELSE
          V_Tarifa = V_Valor_Nominal / V_Cantidad;
        V_Valor = V_Valor_Nominal;
      END

      SELECT Codigo_Fe,
             SUBSTRING(Nombre FROM 1 FOR 40)
      FROM Tipoimpuestos
      WHERE Codigo = :V_Tipo_Impuesto
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc";

      IF ("noov:Nvimp_cdia" = '22') THEN
        V_Base = 0;

      "noov:Nvimp_desc" = CASE
                            WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                            WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                            WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                            WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                            WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                            ELSE "noov:Nvimp_desc"
                          END;

      "noov:Nvimp_base" = V_Base;
      "noov:Nvimp_porc" = V_Tarifa;
      "noov:Nvimp_valo" = V_Valor;

      IF ("noov:Nvimp_cdia" NOT IN ('22', '99') AND
          NOT("noov:Nvimp_cdia" = '04' AND
          V_Tarifa = 0)) THEN
        SUSPEND;

    END

  END

END^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Renglon (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10),
    Renglon_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    Es_Tarifa         CHAR(1),
    Clase             VARCHAR(20))
AS
DECLARE VARIABLE V_Ano              INTEGER;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Porc_Descuento   DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valor            DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  V_Num_Linea = 0;
  FOR SELECT Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Aiu,
             Referencia,
             Stot_Red
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Porc_Descuento,
           :V_Descuento,
           :V_Aiu,
           :V_Referencia,
           :V_Base
  DO
  BEGIN

    IF (V_Num_Linea = 0) THEN
    BEGIN
      V_Cantidad = COALESCE(V_Cantidad, 0);
      V_Bruto = COALESCE(V_Bruto, 0);
      V_Descuento = COALESCE(V_Descuento, 0);
      V_Aiu = COALESCE(V_Aiu, 0);

      IF (V_Aiu = 0) THEN
        V_Aiu = 100;

      V_Esquema_Impuesto = NULL;
      V_Tipo_Impuesto = NULL;

      V_Tipo_Impuesto = NULL;
      Es_Tarifa = NULL;

      /* IMPUESTOS */

      "noov:Nvimp_oper" = 'S';

      FOR SELECT Tipo_Impuesto,
                 Es_Tarifa,
                 SUM(Debito + Credito)
          FROM Reg_Impuestos
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Codigo_Fe <> '00'
          GROUP BY Tipo_Impuesto, Es_Tarifa
          INTO :V_Tipo_Impuesto,
               :Es_Tarifa,
               :V_Valor_Nominal
      DO
      BEGIN
        V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
        V_Tarifa = NULL;

        SELECT Tarifa
        FROM Data_Impuestos
        WHERE Codtipoimpuesto = :V_Tipo_Impuesto
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        /*IF (V_Porc_Descuento=100) THEN
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100);
ELSE
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento; */

        IF (Es_Tarifa = 'S') THEN
          V_Valor = V_Base * V_Tarifa / 100;

        IF (Es_Tarifa = 'N') THEN
        BEGIN
          IF (V_Cantidad = 0) THEN
            V_Tarifa = 0;
          ELSE
            V_Tarifa = V_Valor_Nominal / V_Cantidad;
          V_Valor = V_Valor_Nominal;
        END

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40)
        FROM Tipoimpuestos
        WHERE Codigo = :V_Tipo_Impuesto
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc";

        IF ("noov:Nvimp_cdia" = '22') THEN
        BEGIN
          V_Base = 0;
          V_Num_Linea = 1;
        END

        "noov:Nvimp_desc" = CASE
                              WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                              WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                              WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                              WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                              WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                              WHEN V_Tarifa = 19 THEN 'Impuesto sobre la Ventas 19%'
                              WHEN V_Tarifa = 5 THEN 'Impuesto sobre la Ventas 5%'
                              WHEN V_Tarifa = 0 THEN 'Impuesto sobre la Ventas Exento'
                              ELSE "noov:Nvimp_desc"
                            END;

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Valor;

        IF (NOT("noov:Nvimp_cdia" = '04' AND
            V_Tarifa = 0)) THEN
          SUSPEND;

      END

      /* RETENCIONES */

      "noov:Nvimp_oper" = 'R';
      Es_Tarifa = 'S';

      V_Tipo_Retencion = NULL;
      V_Base = 0;
      V_Valor = 0;

      FOR SELECT Tipo_Retencion,
                 SUM(Base),
                 SUM(Debito + Credito)
          FROM Reg_Retenciones
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Publicar = 'S'
          GROUP BY Tipo_Retencion
          INTO :V_Tipo_Retencion,
               :V_Base,
               :V_Valor

      DO
      BEGIN

        "noov:Nvimp_cdia" = NULL;
        "noov:Nvimp_desc" = NULL;
        "noov:Nvimp_base" = 0;
        "noov:Nvimp_porc" = 0;
        "noov:Nvimp_valo" = 0;
        V_Tarifa = 0;

        V_Base = COALESCE(V_Base, 0);
        V_Valor = COALESCE(V_Valor, 0);

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40),
               Clase
        FROM Tiporetenciones
        WHERE Codigo = :V_Tipo_Retencion
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :Clase;

        SELECT Tarifa
        FROM Data_Retenciones
        WHERE Codtiporetencion = :V_Tipo_Retencion
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Base * V_Tarifa / 100;

        SUSPEND;
      END
    END
  END
END^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Sum (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    Valor_Red         DOUBLE PRECISION,
    Es_Tarifa         CHAR(1),
    Clase             VARCHAR(20))
AS
DECLARE VARIABLE V_Empresa      VARCHAR(15);
DECLARE VARIABLE V_Personalizar INTEGER;
DECLARE VARIABLE V_Iva          INTEGER;
DECLARE VARIABLE V_Ref          VARCHAR(20);
BEGIN
  -- Datos_Empresa
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  -- Personalizacion
  /******************************************************************/
  /**** COMERCIALIZADORA INDUSTRIAL CENTRO S.A.S  (EDS LA MARIA) ****/
  /******************************************************************/
  IF (TRIM(V_Empresa) = 'XX900253903XX') THEN
  BEGIN
    -- Se valida si se personaliza o no
    SELECT COUNT(1)
    FROM Tr_Inventario
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
          AND Codreferencia STARTING WITH 'IVA'
    INTO V_Personalizar;

    IF (V_Personalizar > 0) THEN
    BEGIN
      FOR SELECT "noov:Nvimp_cdia",
                 "noov:Nvdet_piva",
                 "noov:Nvpro_codi",
                 SUM("noov:Nvdet_viva"),
                 SUM("noov:Nvfac_stot")
          FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
          WHERE "noov:Nvimp_cdia" <> '00'
          GROUP BY 1, 2, 3
          INTO :"noov:Nvimp_cdia", :"noov:Nvimp_porc", V_Ref,
               :"noov:Nvimp_valo", :"noov:Nvimp_base"
      DO
      BEGIN
        V_Iva = IIF(TRIM(V_Ref) = 'IVA5', 5, 19);
        "noov:Nvimp_oper" = 'S';
        "noov:Nvimp_desc" = 'IVA ' || V_Iva || '%';

        SELECT Valor
        FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
        INTO Valor_Red;

        SUSPEND;
      END

    END
    ELSE
    /************************************************************/
    /****                   XML ESTANDAR                     ****/
    /************************************************************/
    BEGIN
      FOR SELECT "noov:Nvimp_cdia",
                 "noov:Nvimp_desc",
                 "noov:Nvimp_oper",
                 "noov:Nvimp_base",
                 "noov:Nvimp_porc",
                 "noov:Nvimp_valo",
                 Valor_Red,
                 Es_Tarifa,
                 Clase
          FROM Pz_Fe_Impuestos_Sum_E(:Tipo_, :Prefijo_, :Numero_)
          INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
               :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
               :Valor_Red, :Es_Tarifa, :Clase
      DO
      BEGIN
        SUSPEND;
      END

    END

  END
  ELSE
  /************************************************************/
  /****                   XML ESTANDAR                     ****/
  /************************************************************/
  BEGIN
    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               "noov:Nvimp_base",
               "noov:Nvimp_porc",
               "noov:Nvimp_valo",
               Valor_Red,
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos_Sum_E(:Tipo_, :Prefijo_, :Numero_)
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             :Valor_Red, :Es_Tarifa, :Clase
    DO
    BEGIN
      SUSPEND;
    END

  END

END^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Impuestos_Sum_E (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    Valor_Red         DOUBLE PRECISION,
    Es_Tarifa         CHAR(1),
    Clase             VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
DECLARE VARIABLE V_Empresa   VARCHAR(15);
BEGIN

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               SUM("noov:Nvimp_base"),
               "noov:Nvimp_porc",
               SUM("noov:Nvimp_valo"),
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
        WHERE Es_Tarifa = 'S'
        GROUP BY 1, 2, 3, 5, 7, 8
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             Es_Tarifa, Clase
    DO
    BEGIN
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
      INTO Valor_Red;
      SUSPEND;
    END

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               SUM("noov:Nvimp_base"),
               SUM("noov:Nvimp_porc"),
               SUM("noov:Nvimp_valo"),
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
        WHERE Es_Tarifa = 'N'
        GROUP BY 1, 2, 3, 7, 8
        INTO :"noov:Nvimp_cdia", :"noov:Nvimp_desc", :"noov:Nvimp_oper",
             :"noov:Nvimp_base", :"noov:Nvimp_porc", :"noov:Nvimp_valo",
             Es_Tarifa, Clase
    DO
    BEGIN
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
      INTO Valor_Red;
      SUSPEND;
    END


END^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Round (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10),
    Trm_     DOUBLE PRECISION)
RETURNS (
    Valor NUMERIC(12,2))
AS
DECLARE VARIABLE V_Valor_Mekano DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Totp   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Stot   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Desc   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Imp    DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Mek_Xml  DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Xml      DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo         VARCHAR(20);
BEGIN
  --CODIGO_FE
  SELECT TRIM(Codigo_Fe)
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Tipo;

  SELECT Com_Total,
         "noov:Nvfac_totp",
         "noov:Nvfac_stot",
         "noov:Nvfac_desc" + "noov:Nvfac_anti"
  FROM Pz_Fe_Comprobantes(:Tipo_, :Prefijo_, :Numero_, '%', '%')
  INTO V_Valor_Mekano,
       V_Valor_Totp,
       V_Valor_Stot,
       V_Valor_Desc;

  -- Impuestos
  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Valor_Imp;
  V_Valor_Imp = COALESCE(V_Valor_Imp, 0);

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Mekano / :Trm_, 2)
  INTO V_Valor_Mekano;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Totp / :Trm_, 2)
  INTO V_Valor_Totp;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Stot / :Trm_, 2)
  INTO V_Valor_Stot;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Desc / :Trm_, 2)
  INTO V_Valor_Desc;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Imp / :Trm_, 2)
  INTO V_Valor_Imp;

  -- Diferencia Mekano vs XML
  V_Dif_Mek_Xml = :V_Valor_Mekano - :V_Valor_Totp;

  -- Valor XML
  V_Dif_Xml = :V_Valor_Totp - :V_Valor_Stot - :V_Valor_Imp + COALESCE(:V_Valor_Desc, 0);

  IF (V_Tipo IN ('DOCUMENTO SOPORTE','NOTA DE AJUSTE A DS')) THEN
    Valor = V_Dif_Xml;
  ELSE
    Valor = V_Dif_Mek_Xml + V_Dif_Xml;

  IF (Valor > 5000 OR Valor < -5000) THEN
    Valor = 0;

  Valor = COALESCE(Valor, 0);
  SUSPEND;

END^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Trm (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Codigo CHAR(5),
    Valor  DOUBLE PRECISION,
    Fecha  CHAR(10))
AS
DECLARE VARIABLE V_Docu    CHAR(20);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Trm     DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Lista   CHAR(5);
BEGIN
  -- Se valida el tipo de documento
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

  SELECT Trm,
         Fecha,
         Codlista
  FROM Comprobantes
  WHERE Tipo = :V_Tipo
        AND Prefijo = :V_Prefijo
        AND Numero = :V_Numero
  INTO V_Trm,
       V_Fecha,
       V_Lista;

  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista
  INTO Codigo;

  IF (TRIM(COALESCE(Codigo, '')) = '') THEN
    Codigo = 'COP';

  Valor = 1;
  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Valor > 0
        AND Codmoneda = :Codigo
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO Valor;

  Valor = COALESCE(Valor, 1);

  IF (V_Trm > 0) THEN
    Valor = V_Trm;

  IF (Codigo = 'COP') THEN
    Valor = 1;

  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^

SET TERM ; ^

/*Pz_Fe_Detalle_Base*/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Detalle_Base (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Renglon        INTEGER,
    Referencia     CHAR(20),
    Nom_Referencia CHAR(80),
    Cantidad       DOUBLE PRECISION,
    Bruto          DOUBLE PRECISION,
    Unitario       DOUBLE PRECISION,
    Porc_Descuento DOUBLE PRECISION,
    Descuento      DOUBLE PRECISION,
    Aiu            DOUBLE PRECISION,
    Valor_Aiu      DOUBLE PRECISION,
    Stot           DOUBLE PRECISION,
    Nota           CHAR(200),
    Medida         CHAR(5),
    Linea          CHAR(5),
    Dsi            CHAR(1),
    Stot_Red       DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Medida    CHAR(5);
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto     DOUBLE PRECISION;
DECLARE VARIABLE V_Valo      DOUBLE PRECISION;
DECLARE VARIABLE V_Desc      DOUBLE PRECISION;
BEGIN
  FOR SELECT Renglon,
             Codreferencia,
             Entrada + Salida,
             Bruto,
             Unitario,
             Porcentaje_Descuento,
             Descuento,
             Aiu,
             Nota,
             Dsi
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO :Renglon, :Referencia, :Cantidad, :Bruto, :Unitario, :Porc_Descuento,
           :Descuento, :Aiu, :Nota, :Dsi
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);
    Aiu = COALESCE(Aiu, 0);

    V_Medida = NULL;
    Medida = NULL;

    --Referencias
    SELECT R.Nombre,
           R.Codmedida,
           R.Codlinea
    FROM Referencias R
    WHERE Codigo = :Referencia
    INTO Nom_Referencia, V_Medida, Linea;

    IF (Bruto * Cantidad > 0 OR (Linea = 'INC' AND
        Bruto * Cantidad = 0)) THEN
    BEGIN

      IF (V_Medida IS NOT NULL) THEN
        SELECT Codigo_Fe
        FROM Medidas
        WHERE Codigo = :V_Medida
        INTO Medida;

      Medida = COALESCE(Medida, '94');

      IF (Porc_Descuento = 0) THEN
        IF (Bruto = 0) THEN
          Porc_Descuento = 0;
        ELSE
          Porc_Descuento = Descuento * 100 / Bruto;

      Descuento = :Bruto * :Porc_Descuento / 100;

      IF (Aiu = 0) THEN
        Aiu = 100;
      Valor_Aiu = (Bruto - Descuento) * Aiu / 100;

      IF (Aiu = 100) THEN
        Valor_Aiu = 0;

      Bruto = Bruto - Valor_Aiu;
      Stot = Cantidad * (Bruto - Descuento);

      IF (Porc_Descuento = 100) THEN
        Stot = Cantidad * Bruto;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        V_Renglon = Renglon;
        SELECT GEN_ID(Gen_Tr_Inventario, 1)
        FROM Rdb$Database
        INTO Renglon;
      END
      Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      SUSPEND;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        Renglon = V_Renglon;
        Bruto = Valor_Aiu;
        V_Valo = (SELECT Valor
                  FROM Redondeo_Dian(:Bruto, 2));
        Porc_Descuento = 0;
        Descuento = 0;
        Stot = Valor_Aiu * Cantidad;
        Stot_Red = (V_Valo * Cantidad);
        Valor_Aiu = 0;
        Aiu = 0;

        SUSPEND;
      END
      IF (Linea = 'INC' AND
          Bruto > 0) THEN
      BEGIN
          SUSPEND;
      END
    END
  END
END^

SET TERM ; ^

/*Pz_Fe_Detalle_E*/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Detalle_E (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Renglon           INTEGER,
    Consecutivo       INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    "noov:Nvdet_tran" CHAR(1),
    Stot_Red          DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota             CHAR(200);
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario         DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Aiu        DOUBLE PRECISION;
DECLARE VARIABLE V_Linea            CHAR(5);
DECLARE VARIABLE V_Dsi              CHAR(1);
DECLARE VARIABLE V_Num_Linea        INTEGER;
DECLARE VARIABLE V_Empresa          VARCHAR(15);
DECLARE VARIABLE V_Renglon_1        INTEGER;
DECLARE VARIABLE V_Referencia_1     VARCHAR(20);
DECLARE VARIABLE V_Nom_Referencia_1 VARCHAR(198);
DECLARE VARIABLE V_Nota_1           VARCHAR(200);
DECLARE VARIABLE V_Medida_1         VARCHAR(5);
DECLARE VARIABLE V_Cantidad_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto_1          DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Pdescuento_1     DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_1      DOUBLE PRECISION;
DECLARE VARIABLE V_Iva              DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Linea       VARCHAR(20);
BEGIN
  V_Num_Linea = 0;

  FOR SELECT Renglon,
             Referencia,
             Nom_Referencia,
             Nota,
             Medida,
             Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Valor_Aiu,
             Stot_Red,
             Linea,
             Dsi
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      INTO Renglon,
           "noov:Nvpro_codi",
           "noov:Nvpro_nomb",
           V_Nota,
           "noov:Nvuni_desc",
           "noov:Nvfac_cant",
           V_Bruto,
           "noov:Nvfac_pdes",
           "noov:Nvfac_desc",
           V_Valor_Aiu,
           "noov:Nvfac_stot",
           V_Linea,
           V_Dsi
  DO
  BEGIN
    Consecutivo = 1;
    --impuestos
    "noov:Nvimp_cdia" = '00';
    "noov:Nvdet_piva" = 0;
    "noov:Nvdet_viva" = 0;

    IF (V_Linea = 'INC' AND
        V_Bruto > 0 AND
        V_Num_Linea > 0) THEN
      SELECT SKIP 1 "noov:Nvimp_cdia",
                    "noov:Nvimp_porc",
                    "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    ELSE
      SELECT FIRST 1 "noov:Nvimp_cdia",
                     "noov:Nvimp_porc",
                     "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    "noov:Nvimp_cdia" = COALESCE("noov:Nvimp_cdia", '00');
    "noov:Nvdet_piva" = COALESCE("noov:Nvdet_piva", 0);
    "noov:Nvdet_viva" = COALESCE("noov:Nvdet_viva", 0);

    "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
    "noov:Nvfac_valo" = V_Bruto;

    IF (V_Linea = 'INC' AND
        V_Bruto = 0) THEN
      "noov:Nvfac_valo" = "noov:Nvdet_viva" / "noov:Nvfac_cant";

    IF (V_Valor_Aiu > 0) THEN
      "noov:Nvimp_cdia" = '00';

    "noov:Nvdet_pref" = '';
    IF (V_Bruto = "noov:Nvfac_desc") THEN
      "noov:Nvdet_pref" = '01';

    IF (V_Valor_Aiu > 0) THEN
    BEGIN
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
    END
    "noov:Nvdet_tcod" = '';
    "noov:Nvpro_cean" = '';
    "noov:Nvdet_entr" = '';
    "noov:Nvuni_quan" = 0;

    IF (TRIM(V_Linea) IN ('AIU_A', 'AIU')) THEN
      "noov:Nvdet_nota" = 'Contrato de servicios AIU por concepto de: Servicios';
    ELSE
      "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

    "noov:Nvdet_padr" = 0;
    "noov:Nvdet_marc" = '';
    "noov:Nvdet_mode" = '';
    IF (V_Linea = 'INC' AND
        V_Bruto > 0) THEN
      V_Num_Linea = V_Num_Linea + 1;

    -- "noov:Nvfac_stot" = (V_Valo * "noov:Nvfac_cant") - V_Desc;

    IF ("noov:Nvfac_pdes" = 100) THEN
      "noov:Nvfac_desc" = "noov:Nvfac_stot";

    IF ("noov:Nvimp_cdia" = '22') THEN
      "noov:Nvfac_stot" = 0;

    -- Transporte de carga
    SELECT Tipo
    FROM Lineas
    WHERE Codigo = :V_Linea
    INTO :V_Tipo_Linea;

    "noov:Nvdet_tran" = '';
    IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE SERVICIO') THEN
      "noov:Nvdet_tran" = '0';
    ELSE
    IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE REMESA') THEN
      "noov:Nvdet_tran" = '1';

    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
    INTO Stot_Red;

    SUSPEND;
  END

END^

SET TERM ; ^

/*Pz_Fe_Detalle*/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Detalle (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Renglon           INTEGER,
    Consecutivo       INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    "noov:Nvdet_tran" CHAR(1),
    Stot_Red          DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota             CHAR(200);
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario         DOUBLE PRECISION;
DECLARE VARIABLE V_Dsi              CHAR(1);
DECLARE VARIABLE V_Empresa          VARCHAR(15);
DECLARE VARIABLE V_Renglon_1        INTEGER;
DECLARE VARIABLE V_Referencia_1     VARCHAR(20);
DECLARE VARIABLE V_Nom_Referencia_1 VARCHAR(198);
DECLARE VARIABLE V_Nota_1           VARCHAR(200);
DECLARE VARIABLE V_Medida_1         VARCHAR(5);
DECLARE VARIABLE V_Cantidad_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto_1          DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Pdescuento_1     DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_1      DOUBLE PRECISION;
DECLARE VARIABLE V_Iva              DOUBLE PRECISION;
DECLARE VARIABLE V_Personalizar     INTEGER;
BEGIN
  -- Datos_Empresa
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  -- Personalizacion
  /******************************************************************/
  /**** COMERCIALIZADORA INDUSTRIAL CENTRO S.A.S  (EDS LA MARIA) ****/
  /******************************************************************/
  IF (TRIM(V_Empresa) = 'XX900253903XX') THEN
  BEGIN

    -- Se valida si se personaliza o no
    SELECT COUNT(1)
    FROM Tr_Inventario
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
          AND Codreferencia STARTING WITH 'IVA'
    INTO V_Personalizar;

    IF (V_Personalizar > 0) THEN
    BEGIN

      -- Se busca el producto real inicial
      SELECT FIRST 1 Renglon,
                     Referencia,
                     Nom_Referencia,
                     Nota,
                     Medida,
                     Cantidad,
                     Bruto,
                     Unitario,
                     Porc_Descuento,
                     Descuento
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Referencia NOT LIKE 'IVA%'
      INTO V_Renglon_1,
           V_Referencia_1,
           V_Nom_Referencia_1,
           V_Nota_1,
           V_Medida_1,
           V_Cantidad_1,
           V_Bruto_1,
           V_Unitario_1,
           V_Pdescuento_1,
           V_Descuento_1;

      -- Los otros productos (IVA)
      FOR SELECT Renglon,
                 Referencia,
                 Nom_Referencia,
                 Nota,
                 Medida,
                 Cantidad,
                 Bruto,
                 Unitario,
                 Porc_Descuento,
                 Descuento,
                 Dsi
          FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
          WHERE Referencia LIKE 'IVA%'
          INTO Renglon,
               "noov:Nvpro_codi",
               "noov:Nvpro_nomb",
               V_Nota,
               "noov:Nvuni_desc",
               "noov:Nvfac_cant",
               V_Bruto,
               V_Unitario,
               "noov:Nvfac_pdes",
               "noov:Nvfac_desc",
               V_Dsi
      DO
      BEGIN
        V_Iva = IIF(TRIM("noov:Nvpro_codi") = 'IVA5', 5, 19);
        "noov:Nvfac_stot" = "noov:Nvfac_cant" * ((V_Unitario * 100) / V_Iva);
        Consecutivo = 1;
        --impuestos
        "noov:Nvimp_cdia" = '01';
        "noov:Nvdet_piva" = V_Iva;
        "noov:Nvdet_viva" = "noov:Nvfac_cant" * V_Unitario;

        "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
        "noov:Nvfac_valo" = "noov:Nvfac_stot" / "noov:Nvfac_cant";

        "noov:Nvdet_pref" = '';
        IF (V_Bruto = "noov:Nvfac_desc") THEN
          "noov:Nvdet_pref" = '01';

        "noov:Nvdet_tcod" = '';
        "noov:Nvpro_cean" = '';
        "noov:Nvdet_entr" = '';
        "noov:Nvuni_quan" = 0;

        "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

        "noov:Nvdet_padr" = 0;
        "noov:Nvdet_marc" = '';
        "noov:Nvdet_mode" = '';

        IF ("noov:Nvfac_pdes" = 100) THEN
          "noov:Nvfac_desc" = "noov:Nvfac_stot";

        SELECT Valor
        FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
        INTO Stot_Red;

        SUSPEND;
        V_Unitario_1 = V_Unitario_1 - "noov:Nvfac_valo";
      END
      -- Producto Real Digitado
      Renglon = V_Renglon_1;
      Consecutivo = 1;
      "noov:Nvpro_codi" = V_Referencia_1;
      "noov:Nvpro_nomb" = V_Nom_Referencia_1;
      "noov:Nvuni_desc" = V_Medida_1;
      "noov:Nvfac_cant" = V_Cantidad_1;
      "noov:Nvfac_valo" = V_Unitario_1;
      "noov:Nvimp_cdia" = '00';
      "noov:Nvfac_pdes" = V_Pdescuento_1;
      "noov:Nvfac_desc" = V_Descuento_1;
      "noov:Nvdet_pref" = '';
      "noov:Nvfac_stot" = V_Cantidad_1 * V_Unitario_1;
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
      "noov:Nvdet_tcod" = '';
      "noov:Nvpro_cean" = '';
      "noov:Nvdet_entr" = '';
      "noov:Nvuni_quan" = 0;
      "noov:Nvdet_nota" = V_Nota_1;
      "noov:Nvdet_padr" = 0;
      "noov:Nvdet_marc" = '';
      "noov:Nvdet_mode" = '';
      SELECT Valor
      FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
      INTO Stot_Red;
      SUSPEND;

    END
    ELSE
    BEGIN
      FOR SELECT Renglon,
                 Consecutivo,
                 "noov:Nvpro_codi",
                 "noov:Nvpro_nomb",
                 "noov:Nvuni_desc",
                 "noov:Nvfac_cant",
                 "noov:Nvfac_valo",
                 "noov:Nvimp_cdia",
                 "noov:Nvfac_pdes",
                 "noov:Nvfac_desc",
                 "noov:Nvdet_pref",
                 "noov:Nvfac_stot",
                 "noov:Nvdet_piva",
                 "noov:Nvdet_viva",
                 "noov:Nvdet_tcod",
                 "noov:Nvpro_cean",
                 "noov:Nvdet_entr",
                 "noov:Nvuni_quan",
                 "noov:Nvdet_nota",
                 "noov:Nvdet_padr",
                 "noov:Nvdet_marc",
                 "noov:Nvdet_mode",
                 Stot_Red
          FROM Pz_Fe_Detalle_E(:Tipo_, :Prefijo_, :Numero_)
          INTO :Renglon,
               :Consecutivo,
               :"noov:Nvpro_codi",
               :"noov:Nvpro_nomb",
               :"noov:Nvuni_desc",
               :"noov:Nvfac_cant",
               :"noov:Nvfac_valo",
               :"noov:Nvimp_cdia",
               :"noov:Nvfac_pdes",
               :"noov:Nvfac_desc",
               :"noov:Nvdet_pref",
               :"noov:Nvfac_stot",
               :"noov:Nvdet_piva",
               :"noov:Nvdet_viva",
               :"noov:Nvdet_tcod",
               :"noov:Nvpro_cean",
               :"noov:Nvdet_entr",
               :"noov:Nvuni_quan",
               :"noov:Nvdet_nota",
               :"noov:Nvdet_padr",
               :"noov:Nvdet_marc",
               :"noov:Nvdet_mode",
               :Stot_Red
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
  ELSE
  /************************************************************/
  /****                   XML ESTANDAR                     ****/
  /************************************************************/
  BEGIN
    FOR SELECT Renglon,
               Consecutivo,
               "noov:Nvpro_codi",
               "noov:Nvpro_nomb",
               "noov:Nvuni_desc",
               "noov:Nvfac_cant",
               "noov:Nvfac_valo",
               "noov:Nvimp_cdia",
               "noov:Nvfac_pdes",
               "noov:Nvfac_desc",
               "noov:Nvdet_pref",
               "noov:Nvfac_stot",
               "noov:Nvdet_piva",
               "noov:Nvdet_viva",
               "noov:Nvdet_tcod",
               "noov:Nvpro_cean",
               "noov:Nvdet_entr",
               "noov:Nvuni_quan",
               "noov:Nvdet_nota",
               "noov:Nvdet_padr",
               "noov:Nvdet_marc",
               "noov:Nvdet_mode",
               "noov:Nvdet_tran",
               Stot_Red
        FROM Pz_Fe_Detalle_E(:Tipo_, :Prefijo_, :Numero_)
        INTO :Renglon,
             :Consecutivo,
             :"noov:Nvpro_codi",
             :"noov:Nvpro_nomb",
             :"noov:Nvuni_desc",
             :"noov:Nvfac_cant",
             :"noov:Nvfac_valo",
             :"noov:Nvimp_cdia",
             :"noov:Nvfac_pdes",
             :"noov:Nvfac_desc",
             :"noov:Nvdet_pref",
             :"noov:Nvfac_stot",
             :"noov:Nvdet_piva",
             :"noov:Nvdet_viva",
             :"noov:Nvdet_tcod",
             :"noov:Nvpro_cean",
             :"noov:Nvdet_entr",
             :"noov:Nvuni_quan",
             :"noov:Nvdet_nota",
             :"noov:Nvdet_padr",
             :"noov:Nvdet_marc",
             :"noov:Nvdet_mode",
             :"noov:Nvdet_tran",
             :Stot_Red
    DO
    BEGIN
      SUSPEND;
    END
  END

END^

SET TERM ; ^


/*Pz_Fe_Comprobantes*/
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Comprobantes (
    Tipo_     VARCHAR(5),
    Prefijo_  VARCHAR(5),
    Numero_   VARCHAR(10),
    Emisor_   VARCHAR(15),
    Receptor_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(20),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_Docu_Ref        VARCHAR(20),
    Com_Total         DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Caiu            INTEGER;
DECLARE VARIABLE V_Dsi             INTEGER;
DECLARE VARIABLE V_Mand            INTEGER;
DECLARE VARIABLE V_Transporte      INTEGER;
DECLARE VARIABLE V_Cod_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Tesoreria       VARCHAR(10);
DECLARE VARIABLE V_Formapago       VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe    VARCHAR(5);
DECLARE VARIABLE V_Codbanco        VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe     VARCHAR(5);
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Codsociedad     VARCHAR(5);
DECLARE VARIABLE V_Email_Ter       VARCHAR(50);
DECLARE VARIABLE V_Codsucursal     VARCHAR(15);
DECLARE VARIABLE V_Email_Suc       VARCHAR(50);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Copag           INTEGER;
DECLARE VARIABLE V_Copago          DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Municipio       VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref        VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref     VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref      VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac       VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref        VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Stot_Red        DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det   DOUBLE PRECISION;
DECLARE VARIABLE V_Siif            VARCHAR(80);
DECLARE VARIABLE V_Fecre           INTEGER;
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  -- Si es NC o ND se toman datos del doc referencia
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tipo_Ref,
       V_Prefijo_Ref,
       V_Numero_Ref;

  V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
  V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
  V_Numero_Ref = COALESCE(V_Numero_Ref, '');

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Comprobantes
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Fecha_Ref;
  -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

  SELECT Cufe,
         EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Facturas
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Cufe_Ref,
       V_Fecha_Fac;
  V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
  V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tipo_Ref
  INTO V_Docu_Ref;
  V_Docu_Ref = COALESCE(V_Docu_Ref, '');

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       "noov:Nvfac_fech",
       "noov:Nvfac_venc",
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvres_nume";
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');

  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvfac_cdet";

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  -- Transportes
  SELECT COUNT(1)
  FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Transporte;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO "noov:Nvcli_ncon";

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  "noov:Nvcli_ncon" = COALESCE("noov:Nvcli_ncon", '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO "noov:Nvven_nomb";

  "noov:Nvven_nomb" = COALESCE("noov:Nvven_nomb", '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       V_Codsociedad,
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO "noov:Nvcli_regi",
       "noov:Nvcli_fisc";

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO "noov:Nvcli_pais";

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO "noov:Nvcli_depa";

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:"noov:Nvcli_pais") = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO "noov:Nvcli_ciud";

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos";

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  "noov:Nvfac_orde" = COALESCE("noov:Nvfac_orde", '');
  "noov:Nvfac_remi" = COALESCE("noov:Nvfac_remi", '');
  "noov:Nvfac_rece" = COALESCE("noov:Nvfac_rece", '');
  "noov:Nvfac_entr" = COALESCE("noov:Nvfac_entr", '');
  "noov:Nvfac_ccos" = COALESCE("noov:Nvfac_ccos", '');

  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);

  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Caiu <> 0 THEN '11'
                                              WHEN V_Mand <> 0 THEN '12'
                                              WHEN V_Transporte <> 0 THEN '18'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Caiu <> 0 THEN '15'
                                                   WHEN V_Mand <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;

  "noov:Nvfac_fpag" = CASE
                        WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                        WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                        ELSE 'ZZZ'
                      END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END
  ELSE
  BEGIN
    IF ("noov:Nvfac_fech" <> "noov:Nvfac_venc") THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    "noov:Nvcli_cper" = '2';
  ELSE
    "noov:Nvcli_cper" = '1';

  "noov:Nvcli_loca" = '';
  "noov:Nvcli_mail" = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  "noov:Nvema_copi" = '';

  "noov:Nvfac_obse" = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  SELECT SUM("noov:Nvfac_stot")
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Nvfac_Stot;

  "noov:Nvfac_stot" = V_Nvfac_Stot;

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;

  IF (V_Copag = 0) THEN
    "noov:Nvfac_desc" = V_Copago;
  ELSE
    "noov:Nvfac_anti" = V_Copago;

  "noov:Nvfac_desc" = "noov:Nvfac_desc" + V_Descuento_Det;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo")
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;

  V_Impuestos = COALESCE(V_Impuestos, 0);

  SELECT SUM(Stot_Red)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Stot_Red;

  "noov:Nvfac_totp" = V_Stot_Red + V_Impuestos - "noov:Nvfac_desc";

  "noov:Nvfor_oper" = '';
  IF ("noov:Nvfac_anti" > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      "noov:Nvfac_desc" = "noov:Nvfac_anti";
      "noov:Nvfac_anti" = 0;
    END
    ELSE
      "noov:Nvfac_totp" = "noov:Nvfac_totp" - "noov:Nvfac_anti";
    "noov:Nvfor_oper" = 'SS-CUFE';
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    "noov:Nvfor_oper" = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_numb" = '';
  "noov:Nvfac_obsb" = '';
  "noov:Nvcon_codi" = '';
  "noov:Nvcon_desc" = '';
  "noov:Nvfac_tcru" = '';
  "noov:Nvfac_numb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvcon_codi" = V_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      "noov:Nvfac_tcru" = 'L';
    ELSE
      "noov:Nvfac_tcru" = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijo_Ref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numero_Ref);
    "noov:Nvfac_fecb" = TRIM(COALESCE(V_Fecha_Ref, "noov:Nvfac_fech"));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END
  SUSPEND;

END^

SET TERM ; ^

COMMIT WORK;


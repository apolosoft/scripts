/* MENSAJES  */
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Mensaje_Global_Envia (
    Mensaje_ BLOB SUB_TYPE 1 SEGMENT SIZE 80)
AS
DECLARE VARIABLE V_Codigo CHAR(10);
BEGIN
  FOR SELECT Codigo
      FROM Usuarios
      WHERE Activo = 'S'
      INTO V_Codigo
  DO
  BEGIN
    EXECUTE PROCEDURE Mensaje_Inserta(NULL, :V_Codigo, :Mensaje_);
  END
END^

SET TERM ; ^

EXECUTE PROCEDURE Pz_Mensaje_Global_Envia('Noticia DIAN: Facturadores Electronicos

Sabado 9 de marzo desde 6:00 am hasta las 6:00 pm, el Sistema de Facturacion Electronico de la DIAN estara suspendido.

Se recomienda no abrir conectores hasta despues de las 6:00 pm, para evitar que estas se bloqueen por numero de intentos fallidos


Att. ApoloSoft')

COMMIT WORK;



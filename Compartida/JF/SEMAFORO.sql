EXECUTE BLOCK
AS
DECLARE VARIABLE Cant INTEGER;
BEGIN
  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'BLANCO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'BLANCO');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'VERDE'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'VERDE');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'AMARILLO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'AMARILLO');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'ROJO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'ROJO');
END;



UPDATE Interface_Rest
SET Registro = HASH(Consulta) - HASH(Clave);

SELECT Clave,
       HASH(Consulta) - HASH(Clave)
FROM Interface_Rest;
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Nom_Conflictos_Soluciona
AS
DECLARE VARIABLE Registros INTEGER;
DECLARE VARIABLE V_Rubro   CHAR(5);
DECLARE VARIABLE V_Orden   INTEGER;
BEGIN

  Registros = 0;

  SELECT COUNT(1)
  FROM Nom_Conflictos
  INTO :Registros;

  WHILE (Registros > 0) DO
  BEGIN

    FOR SELECT DISTINCT Rubro_Conflicto,
                        Orden
        FROM Nom_Conflictos
        INTO :V_Rubro,
             :V_Orden
    DO
    BEGIN

      UPDATE Rubros
      SET Orden = :V_Orden + 1
      WHERE Codigo = :V_Rubro;

    END

    SELECT COUNT(1)
    FROM Nom_Conflictos
    INTO :Registros;

  END

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE NOM_CONFLICTOS TO PROCEDURE NOM_CONFLICTOS_SOLUCIONA;
GRANT SELECT,UPDATE ON RUBROS TO PROCEDURE NOM_CONFLICTOS_SOLUCIONA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE NOM_CONFLICTOS_SOLUCIONA TO SYSDBA;
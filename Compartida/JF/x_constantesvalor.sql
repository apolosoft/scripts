CREATE OR ALTER PROCEDURE X_Constantesvalor_1
AS
BEGIN
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C05_S_INT', '2019', 0.7, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C16_RNDCC', '2019', 1.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C18_RDFCC', '2019', 0.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C13_HEND', '2019', 2.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C01_SMLV', '2019', 828116, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C02_AUXT', '2019', 97032, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C03_HMES', '2019', 240, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C05_S_INT', '2018', 0.7, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C05_S_INT', '2017', 0.7, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C10_HED', '2019', 1.25, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C11_HEN', '2019', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C30_EPS_PE', '2019', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C33_AFP_PE', '2019', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C22_PR_CES', '2019', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C21_PR_PRI', '2019', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C20_PR_VAC', '2019', 24, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C23_PR_INT', '2019', 0.12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C38_ARL', '2019', 0, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C37_CCF', '2019', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C31_EPS_EM', '2019', 8.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C39_ICBF', '2019', 3, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C34_AFP_EM', '2019', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C40_SENA', '2019', 2, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C14_RN', '2019', 0.35, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C15_RND', '2019', 2.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C17_RDF', '2019', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C12_HEDD', '2019', 2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_SP', '2019', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C04_S_IGE', '2019', 66.66, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C41_ESAP', '2019', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C42_MEN', '2019', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C43_CT', '2019', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C35_AFP_PI', '2019', 16, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C32_EPS_PI', '2019', 12.5, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C05_S_INT', '2020', 0.7, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C16_RNDCC', '2020', 1.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C18_RDFCC', '2020', 0.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C35_AFP_PI', '2017', 16, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C13_HEND', '2020', 2.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C01_SMLV', '2020', 877802, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C02_AUXT', '2020', 102854, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C03_HMES', '2020', 240, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C01_SMLV', '2017', 737717, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C02_AUXT', '2017', 83140, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C03_HMES', '2017', 240, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C10_HED', '2017', 1.25, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C11_HEN', '2017', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C12_HEDD', '2017', 2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C13_HEND', '2017', 2.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C14_RN', '2017', 0.35, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C15_RND', '2017', 2.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C17_RDF', '2017', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C30_EPS_PE', '2017', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C33_AFP_PE', '2017', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C22_PR_CES', '2017', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C21_PR_PRI', '2017', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C20_PR_VAC', '2017', 24, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C23_PR_INT', '2017', 0.12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C38_ARL', '2017', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C37_CCF', '2017', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C31_EPS_EM', '2017', 8.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C39_ICBF', '2017', 3, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C34_AFP_EM', '2017', 12, NULL);
END;

CREATE OR ALTER PROCEDURE X_Constantesvalor_2
AS
BEGIN
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C40_SENA', '2017', 2, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_SP', '2017', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C10_HED', '2020', 1.25, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C04_S_IGE', '2017', 66.66, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C11_HEN', '2020', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C30_EPS_PE', '2020', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C33_AFP_PE', '2020', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C22_PR_CES', '2020', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C21_PR_PRI', '2020', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C41_ESAP', '2017', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C42_MEN', '2017', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C20_PR_VAC', '2020', 24, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C03_HMES', '2018', 240, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C10_HED', '2018', 1.25, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C11_HEN', '2018', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C12_HEDD', '2018', 2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C13_HEND', '2018', 2.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C14_RN', '2018', 0.35, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C15_RND', '2018', 2.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C17_RDF', '2018', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C23_PR_INT', '2020', 0.12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C38_ARL', '2020', 0.522, '1');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C37_CCF', '2020', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C30_EPS_PE', '2018', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C33_AFP_PE', '2018', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C22_PR_CES', '2018', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C21_PR_PRI', '2018', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C20_PR_VAC', '2018', 24, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C23_PR_INT', '2018', 0.12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C38_ARL', '2018', 0, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C37_CCF', '2018', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C31_EPS_EM', '2018', 8.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C39_ICBF', '2018', 3, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C34_AFP_EM', '2018', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C40_SENA', '2018', 2, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_SP', '2018', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C31_EPS_EM', '2020', 8.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C04_S_IGE', '2018', 66.66, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C39_ICBF', '2020', 3, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C34_AFP_EM', '2020', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C40_SENA', '2020', 2, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C41_ESAP', '2018', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C42_MEN', '2018', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C14_RN', '2020', 0.35, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C15_RND', '2020', 2.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C17_RDF', '2020', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C12_HEDD', '2020', 2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_SP', '2020', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C04_S_IGE', '2020', 66.66, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C41_ESAP', '2020', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C42_MEN', '2020', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C43_CT', '2020', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C35_AFP_PI', '2020', 16, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C32_EPS_PI', '2020', 12.5, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S1', '2020', 0.2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S2', '2020', 0.4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S3', '2020', 0.6, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S4', '2020', 0.8, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S5', '2020', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C44_EXO', '2017', 1, 'S');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C44_EXO', '2020', 1, 'S');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S1', '2017', 0.2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S2', '2017', 0.4, NULL);
END;

CREATE OR ALTER PROCEDURE X_Constantesvalor_3
AS
BEGIN
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S3', '2017', 0.6, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S4', '2017', 0.8, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S5', '2017', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S1', '2018', 0.2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S2', '2018', 0.4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S3', '2018', 0.6, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S4', '2018', 0.8, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S5', '2018', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S1', '2019', 0.2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S2', '2019', 0.4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S3', '2019', 0.6, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S4', '2019', 0.8, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S5', '2019', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C44_EXO', '2018', 1, 'S');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C44_EXO', '2019', 1, 'S');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C05_S_INT', '2021', 0.7, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S1', '2021', 0.2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S2', '2021', 0.4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S3', '2021', 0.6, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S4', '2021', 0.8, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_S5', '2021', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C16_RNDCC', '2021', 1.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C18_RDFCC', '2021', 0.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C16_RNDCC', '2017', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C18_RDFCC', '2017', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C43_CT', '2018', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C01_SMLV', '2018', 781242, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C02_AUXT', '2018', 88211, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C32_EPS_PI', '2018', 12.5, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C44_EXO', '2021', 1, 'S');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C13_HEND', '2021', 2.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C01_SMLV', '2021', 908526, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C02_AUXT', '2021', 106454, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C16_RNDCC', '2018', 1.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C03_HMES', '2021', 240, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C10_HED', '2021', 1.25, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C18_RDFCC', '2018', 0.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C11_HEN', '2021', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C30_EPS_PE', '2021', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C33_AFP_PE', '2021', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C22_PR_CES', '2021', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C21_PR_PRI', '2021', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C20_PR_VAC', '2021', 24, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C23_PR_INT', '2021', 0.12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C38_ARL', '2021', 0.522, '1');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C37_CCF', '2021', 4, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C31_EPS_EM', '2021', 8.5, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C39_ICBF', '2021', 3, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C34_AFP_EM', '2021', 12, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C40_SENA', '2021', 2, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C14_RN', '2021', 0.35, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C15_RND', '2021', 2.1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C17_RDF', '2021', 1.75, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C12_HEDD', '2021', 2, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C36_AFP_SP', '2021', 1, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C04_S_IGE', '2021', 66.66, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C41_ESAP', '2021', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C42_MEN', '2021', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C43_CT', '2021', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C35_AFP_PI', '2021', 16, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C32_EPS_PI', '2021', 12.5, '');
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C43_CT', '2017', 0, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C35_AFP_PI', '2018', 16, NULL);
  INSERT INTO Constantesvalor (Codconstante, Ano, Valor, Texto)
  VALUES ('C32_EPS_PI', '2017', 12.5, '');
END;


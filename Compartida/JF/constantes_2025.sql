
/* SALARIO MINIMO 2024 */
UPDATE OR INSERT INTO Constantesvalor (Codconstante, Ano, Valor)
VALUES ('C01_SMLV', '2024', 1300000);
UPDATE OR INSERT INTO Constantesvalor (Codconstante, Ano, Valor)
VALUES ('C02_AUXT', '2024', 162000);
COMMIT WORK;

/* CONSTANTES 2025 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Constante CHAR(15);
DECLARE VARIABLE V_Ano       CHAR(4);
DECLARE VARIABLE V_Valor     NUMERIC(18,5);
DECLARE VARIABLE V_Texto     CHAR(100);
DECLARE VARIABLE V_nuevo      INTEGER;
BEGIN
  FOR SELECT Codconstante,
             Ano,
             Valor,
             Texto
      FROM Constantesvalor
      WHERE Ano = '2024'
            AND TRIM(Codconstante) <> 'C01_SMLV'
            AND TRIM(Codconstante) <> 'C02_AUXT'
      INTO V_Constante,
           V_Ano,
           V_Valor,
           V_Texto
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Constantesvalor
    WHERE Codconstante = :V_Constante
          AND Ano = '2025'
    INTO V_nuevo;

    IF (V_nuevo = 0) THEN
    BEGIN
      INSERT INTO Constantesvalor
      VALUES (:V_Constante, '2025', :V_Valor, :V_Texto);
    END
  END
END; 

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Codpersonal CHAR(15);
DECLARE VARIABLE V_Constante   CHAR(15);
DECLARE VARIABLE V_Ano         CHAR(4);
DECLARE VARIABLE V_Valor       NUMERIC(18,5);
DECLARE VARIABLE V_Texto       CHAR(100);
DECLARE VARIABLE V_nuevo        INTEGER;
BEGIN
  FOR SELECT Codpersonal,
             Codconstante,
             Ano,
             Valor,
             Texto
      FROM Constantes_Personal
      WHERE Ano = '2024'
      INTO V_Codpersonal,
           V_Constante,
           V_Ano,
           V_Valor,
           V_Texto
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Constantes_Personal
    WHERE Codconstante = :V_Constante
          AND Codpersonal = :V_Codpersonal
          AND Ano = '2025'
    INTO V_nuevo;

    IF (V_nuevo = 0) THEN
    BEGIN
      INSERT INTO constantes_personal
      VALUES (:v_codpersonal,:V_Constante, '2025', :V_Valor, :V_Texto);
    END
  END
END; 

COMMIT WORK;



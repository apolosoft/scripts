CREATE OR ALTER PROCEDURE X_Componentes
AS
BEGIN
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('01', 'ENCABEZADO', '--ENCABEZADO PILA
SELECT ''01'' C_01,
       ''1'' C_02,
       ''0001'' C_03,
       RPAD(TRIM(T.Nombre), 200, '' '') C_04,
       SUBSTRING(I.Codigo2 FROM 1 FOR 2) C_05,
       RPAD(TRIM(T.Codigo), 16, '' '') C_06,
       T.Dv C_07,
       ''E'' C_08,
       RPAD('' '', 10, '' '') C_09,
       RPAD('' '', 10, '' '') C_10,

RPAD((SELECT Forma
        FROM Puestos
        WHERE Codigo = (SELECT FIRST 1 TRIM(Codpuesto)
                        FROM Pila_Fechas)),1,'' '') C_11,
RPAD((SELECT CodPuesto
        FROM Puestos
        WHERE Codigo = (SELECT FIRST 1 TRIM(Codpuesto)
                        FROM Pila_Fechas)),10,'' '') C_12,
RPAD((SELECT Nombre
        FROM Puestos
        WHERE Codigo = (SELECT FIRST 1 TRIM(Codpuesto)
                        FROM Pila_Fechas)),40,'' '') C_13,

       RPAD(A.Codigo, 6, '' '') C_14,
       EXTRACT(YEAR FROM P.Desde) || ''-'' || LPAD(EXTRACT(MONTH FROM P.Desde), 2, ''0'') C_15,
       EXTRACT(YEAR FROM P.Hasta + 5) || ''-'' || LPAD(EXTRACT(MONTH FROM P.Hasta + 5), 2, ''0'') C_16,
       RPAD(''0'', 10, ''0'') C_17,
       RPAD('' '', 10, '' '') C_18,
       LPAD((SELECT COUNT(DISTINCT(Codpersonal))
             FROM Nom_Pila_2838_Puesto), 5, 0) C_19,
       LPAD((WITH T_Dias AS (SELECT C_04,
                                    SUM(C_36) T_36,
                                    SUM(C_37) T_37,
                                    SUM(C_38) T_38,
                                    SUM(C_39) T_39
                             FROM Nom_Pila_2838_Puesto
                             GROUP BY C_04) SELECT ROUND(SUM(ROUND(CEILING(ROUND(IIF(C_05 IN (''12'', ''19''), 0, IIF(C_39 = T_39, C_45 + B_19 + B_18, IIF(Tipo = ''RU'',
       (C_45 / T_39 * C_39) + B_19 + B_18, C_45 / T_39 * C_39))), 2)))))
                                            FROM Nom_Pila_2838_Puesto N
                                            JOIN T_Dias T USING (C_04)
                                            WHERE C_36 + C_37 + C_38 + C_39 > 0), 12, 0) C_20,
       ''01'' C21,
       ''86'' C_22

FROM Terceros T
JOIN Identidades I ON (T.Codidentidad = I.Codigo)
JOIN Tipoaportes A ON (A.Clase = ''ARL'')
JOIN Pila_Fechas P ON (1 = 1)
WHERE T.Datos_Empresa = ''S''', 'S');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('02', 'LIQUIDACION DETALLADA DE APORTES RESOLUCION 2388 DE 2016', '--DETALLE PILA
WITH T_Dias
AS (SELECT C_04,
           SUM(C_36) T_36,
           SUM(C_37) T_37,
           SUM(C_38) T_38,
           SUM(C_39) T_39
    FROM Nom_Pila_2838_Puesto
    GROUP BY C_04)
SELECT C_01,
       LPAD(C_02, 5, 0) AS C_02,
       RPAD(C_03, 2, '' '') AS C_03,
       RPAD(TRIM(C_04), 16, '' '') AS C_04,
       LPAD(IIF(TRIM(C_05) = '''', ''1'', TRIM(C_05)), 2, 0) AS C_05,
       LPAD(TRIM(C_06), 2, 0) AS C_06,
       RPAD(C_07, 1, '' '') AS C_07,
       RPAD(C_08, 1, '' '') AS C_08,
       RPAD(IIF(C_05 IN (''12'', ''19''),
        (SELECT TRIM(Codmunicipio)
         FROM Terceros
         WHERE Datos_Empresa = ''S''), C_09), 5, '' '') AS C_09,
       RPAD(C_11, 20, '' '') AS C_11,
       RPAD(TRIM(C_12), 30, '' '') AS C_12,
       RPAD(C_13, 20, '' '') AS C_13,
       RPAD(TRIM(C_14), 30, '' '') AS C_14,
       RPAD(C_15, 1, '' '') AS C_15,
       RPAD(C_16, 1, '' '') AS C_16,
       RPAD(C_17, 1, '' '') AS C_17,
       RPAD(C_18, 1, '' '') AS C_18,
       RPAD(C_19, 1, '' '') AS C_19,
       RPAD(C_20, 1, '' '') AS C_20,
       RPAD(C_21, 1, '' '') AS C_21,
       RPAD(C_22, 1, '' '') AS C_22,
       RPAD(C_23, 1, '' '') AS C_23,
       RPAD(C_24, 1, '' '') AS C_24,
       RPAD(C_25, 1, '' '') AS C_25,
       RPAD(C_26, 1, '' '') AS C_26,
       RPAD(IIF((C_27=''X'' AND B_17>0),''L'', C_27),1,'' '') AS C_27,
       RPAD(IIF(C_48>0,''X'',''''), 1, '' '') AS C_28,
       RPAD(C_29, 1, '' '') AS C_29,
       LPAD(TRIM(CAST(IIF(C_30 <> C_38, 0, C_30) AS CHAR(2))), 2, ''0'') AS C_30,
       RPAD(TRIM(C_31), 6, '' '') AS C_31,
       RPAD(COALESCE(C_32, ''''), 6, '' '') AS C_32,
       RPAD(COALESCE(C_33, ''''), 6, '' '') AS C_33,
       RPAD(COALESCE(C_34, ''''), 6, '' '') AS C_34,
       RPAD(COALESCE(C_35, ''''), 6, '' '') AS C_35,
       LPAD(TRIM(CAST(ROUND(IIF(C_05 IN (''12'', ''19'') OR C_06 > 0, 0, IIF(C_36 > 0, IIF(C_46 = 0, 0, IIF(C_05 = ''51'', B_14, C_36)), 0))) AS CHAR(2))), 2, ''0'') AS C_36,
       LPAD(TRIM(CAST(ROUND(IIF(C_37 > 0, IIF(C_05 = ''51'', B_15, C_37), 0)) AS CHAR(2))), 2, ''0'') AS C_37,
       LPAD(TRIM(CAST(ROUND(IIF(C_38 > 0, IIF(C_05 = ''51'', 30, IIF(C_05=''12'',0,C_38)), 0)) AS CHAR(2))), 2, ''0'') AS C_38,
       LPAD(TRIM(CAST(ROUND(IIF(C_05 IN (''19'', ''12''), 0, IIF(C_39 > 0, IIF(C_05 = ''51'', B_16, C_39), 0))) AS CHAR(2))), 2, ''0'') AS C_39,
       LPAD(TRIM(CAST(ROUND(C_40) AS CHAR(9))), 9, ''0'') AS C_40,
       RPAD(IIF(C_05 IN (''19'', ''12'', ''51''), '''', IIF(TRIM(COALESCE(C_41, ''''))='''',''F'',C_41) ), 1, '' '') AS C_41,
       LPAD(TRIM(CAST(ROUND(CEILING(ROUND(IIF(C_05 IN (''19'', ''12'') OR C_06 > 0, 0, IIF(C_36 = T_36, C_42 + B_19, IIF(C_23 = ''X'',(C_42 / T_36 * C_36) + B_19, C_42 / T_36 * C_36))), 2))) AS CHAR(9))), 9, ''0'') AS C_42,
       LPAD(TRIM(CAST(ROUND(CEILING(ROUND(IIF(C_37 = T_37, C_43 + B_19, IIF(C_23 = ''X'', (C_43 / T_37 * C_37) + B_19, C_43 / T_37 * C_37)), 2))) AS CHAR(9))), 9, ''0'') AS C_43,
       LPAD(TRIM(CAST(ROUND(CEILING(ROUND(IIF(C_05=''12'',0,IIF(C_38 = T_38, C_44 + B_19, IIF(C_23 = ''X'', (C_44 / T_38 * C_38) + B_19, C_44 / T_38 * C_38))), 2))) AS CHAR(9))), 9, ''0'') AS C_44,
       LPAD(TRIM(CAST(ROUND(CEILING(ROUND(IIF(C_05 IN (''12'', ''19''), 0, IIF(C_39 = T_39, C_45 + B_19 + B_18, IIF(Tipo = ''RU'', (C_45 / T_39 * C_39) + B_19 + B_18, C_45 / T_39 * C_39))), 2))) AS CHAR(9))), 9, ''0'') AS C_45,       
       RPAD(TRIM(CAST(IIF(C_05 IN (''12'', ''19'') OR C_06 > 0, 0, IIF(C_24 = ''X'', B_00 / 100, C_46 / 100)) AS CHAR(7))), 7, ''0'') AS C_46,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF((C_05 IN (''19'', ''12'')) or (C_06 > 0), 0, IIF(C_36 = T_36, C_42 + B_19, IIF(C_23 = ''X'', (C_42 / T_36 * C_36) + B_19, C_42 / T_36 * C_36))), 2))) * IIF((C_05 IN (''12'', ''19'')) or (C_06 > 0), 0, IIF(C_24 = ''X'', B_00 / 100, C_46 / 100)), 100))) AS CHAR(9))), 9, ''0'') AS C_47,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(C_48, 100))) AS CHAR(9))), 9, ''0'') AS C_48,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(C_49, 100))) AS CHAR(9))), 9, ''0'') AS C_49,
       LPAD(TRIM(CAST(ROUND(ROUND((SELECT Tope
                                   FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF((C_05 IN (''19'', ''12'')) OR (C_06 > 0) , 0, IIF(C_36 = T_36, C_42 + B_19, IIF(C_23 = ''X'', (C_42 / T_36 * C_36) + B_19, C_42 / T_36 * C_36))), 2))) * IIF((C_05 IN (''12'', ''19'')) OR (C_06 > 0), 0, IIF(C_24 = ''X'', B_00 / 100, C_46 / 100)), 100))) + ROUND((SELECT Tope
                                                                                                                                                                                                                                                                                                      FROM Sys_Redondea(C_48, 100))) + ROUND((SELECT Tope
                                                                                                                                                                                                                                                                                                                                              FROM Sys_Redondea(C_49, 100)))) AS CHAR(9))), 9, 0) AS C_50, 
      LPAD(TRIM(CAST(ROUND(IIF(C_51 = 0, 0, IIF(C_24 = ''X'', 0,
       (SELECT Tope
        FROM Sys_Redondea(ROUND((CEILING(IIF(C_36 = T_36, C_42 + B_19, IIF(C_23 = ''X'', (C_42 / T_36 * C_36) + B_19, C_42 / T_36 * C_36))) * B_02 / 100) / 2), 100))))) AS CHAR(9))), 9, ''0'') AS C_51,
       LPAD(TRIM(CAST(ROUND(IIF(C_51 = 0, 0, IIF(C_24 = ''X'', 0,
       (SELECT Tope
        FROM Sys_Redondea(ROUND((CEILING(IIF(C_36 = T_36, C_42 + B_19, IIF(C_23 = ''X'', (C_42 / T_36 * C_36) + B_19, C_42 / T_36 * C_36))) * B_02 / 100) / 2)+C_52, 100))))) AS CHAR(9))), 9, ''0'') AS C_52,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                            FROM Sys_Redondea(C_53, 100))) AS CHAR(9))), 9, ''0'') AS C_53,
       RPAD(TRIM(CAST((
       CASE
         WHEN C_05 = ''51'' THEN 0
         WHEN C_05 IN (''12'', ''19'') THEN C_54
         WHEN C_43 + B_19 >= C_00 * 10 AND C_24 = ''X'' THEN B_01
         WHEN C_43 + B_19 >= C_00 * 10 THEN C_54
         WHEN C_24 = ''X'' AND C_76 = ''S'' THEN 0
         WHEN C_24 = ''X'' AND C_76 = ''N'' THEN B_01
         WHEN C_43 + B_19 >= C_00 * 10 AND C_24 = ''X'' THEN B_01
         WHEN C_43 + B_19 >= C_00 * 10 THEN C_54
         WHEN C_76 = ''S'' THEN B_04
         ELSE C_54
       END / 100) AS CHAR(7))), 7, ''0'') AS C_54,
     LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF(Tipo = ''RU'', (C_43 / T_37 * C_37) + B_19, C_43 / T_37 * C_37), 2))) * (CASE
                                                                                                                                                       WHEN C_05 = ''51'' THEN 0
                                                                                                                                                       WHEN C_05 IN (''12'', ''19'') THEN C_54
                                                                                                                                                       WHEN C_43 + B_19 >= C_00 * 10 AND C_24 = ''X'' THEN B_01
                                                                                                                                                       WHEN C_43 + B_19 >= C_00 * 10 THEN C_54
                                                                                                                                                       WHEN C_24 = ''X'' AND C_76 = ''S'' THEN 0
                                                                                                                                                       WHEN C_24 = ''X'' AND C_76 = ''N'' THEN B_01
                                                                                                                                                       WHEN C_43 + B_19 >= C_00 * 10 AND C_24 = ''X'' THEN B_01
                                                                                                                                                       WHEN C_43 + B_19 >= C_00 * 10 THEN C_54
                                                                                                                                                       WHEN C_76 = ''S'' THEN B_04
                                                                                                                                                       ELSE C_54
                                                                                                                                                     END / 100), 100))) AS CHAR(9))), 9, ''0'') AS C_55,

       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(C_56, 100))) AS CHAR(9))), 9, ''0'') AS C_56,
       RPAD(C_57, 15, '' '') AS C_57,
       LPAD(TRIM(CAST(ROUND(C_58) AS CHAR(9))), 9, ''0'') AS C_58,
       RPAD(C_59, 15, '' '') AS C_59,
       LPAD(TRIM(CAST(ROUND(C_60) AS CHAR(9))), 9, ''0'') AS C_60,
       RPAD(TRIM(CAST(IIF(C_05=''12'',0,IIF((C_24 = ''X'' OR C_25 = ''X'' OR C_26 = ''X'' OR C_27 = ''X'' OR C_30 > 0) AND (Tipo <> ''RU''), 0, C_61 / 100)) AS CHAR(9))), 9, ''0'') AS C_61,
       LPAD(TRIM(CAST(TRIM(C_62) AS CHAR(9))), 9, ''0'') AS C_62,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF(C_05 IN (''12''), 0, IIF(C_38 = T_38, C_44 + B_19, IIF(Tipo = ''RU'', (C_44 / T_38 * C_38) + B_19, C_44 / T_38 * C_38))), 2))) * IIF(C_05 IN (''12''), 0, IIF((C_24 = ''X'' OR C_25 = ''X'' OR C_26 = ''X'' OR C_27 = ''X'' OR C_30 > 0) AND (Tipo <> ''RU''), 0, C_61 / 100)), 100))) AS CHAR(9))), 9, ''0'') AS C_63,

       RPAD(TRIM(CAST(IIF(C_05 IN (''12'', ''19''), 0, IIF((C_24 = ''X'' OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0) AND (Tipo <> ''RU''), 0, C_64 / 100)) AS CHAR(7))), 7, ''0'') AS C_64,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF(C_05 IN (''12'', ''19''), 0, IIF(C_39 = T_39, C_45 + B_19 + B_18, IIF(Tipo = ''RU'', (C_45 / T_39 * C_39) + B_19 + B_18, C_45 / T_39 * C_39))), 2))) * IIF(C_05 IN (''12'', ''19''), 0, IIF((C_24 = ''X'' OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0) AND (Tipo <> ''RU''), 0, C_64 / 100)), 100))) AS CHAR(9))), 9, ''0'') AS C_65,
       RPAD(TRIM(CAST(IIF(C_95=0 OR C_24 = ''X''  OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0 OR C_05 IN (''51'', ''19'', ''12''), 0, C_66 / 100) AS CHAR(7))), 7, ''0'') AS C_66,
       LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF(C_95=0 OR C_24 = ''X''  OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0 OR C_05 IN (''51'', ''19'', ''12''), 0, IIF(C_36 = T_36, C_95 + B_18 + B_19, IIF(C_23 = ''X'', (C_95 / T_36 * C_36) + B_18 + B_19+B_13, C_95 / T_36 * C_36))), 2))) * IIF(C_24 = ''X''  OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0 OR C_05 IN (''51'', ''19'', ''12''), 0, C_66 / 100), 100))) AS CHAR(9))), 9, ''0'') AS C_67,
      RPAD(TRIM(CAST(IIF(C_95=0 OR C_24 = ''X''  OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0 OR C_05 IN (''51'', ''19'', ''12''), 0, C_68 / 100) AS CHAR(7))), 7, ''0'') AS C_68,
      LPAD(TRIM(CAST(ROUND((SELECT Tope
                             FROM Sys_Redondea(ROUND(CEILING(ROUND(IIF(C_95=0 OR C_24 = ''X''  OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0 OR C_05 IN (''51'', ''19'', ''12''), 0, IIF(C_36 = T_36, C_95 + B_18 + B_19, IIF(C_23 = ''X'', (C_95 / T_36 * C_36) + B_18 + B_19+B_13, C_95 / T_36 * C_36))), 2))) * IIF(C_24 = ''X''  OR C_25 = ''X'' OR C_26 = ''X'' OR C_30 > 0 OR C_05 IN (''51'', ''19'', ''12''), 0, C_68 / 100), 100))) AS CHAR(9))), 9, ''0'') AS C_69,
       RPAD(TRIM(CAST(C_70 / 100 AS CHAR(7))), 7, ''0'') AS C_70,
       LPAD(TRIM(CAST(ROUND(C_71) AS CHAR(9))), 9, ''0'') AS C_71,
       RPAD(TRIM(CAST(C_72 / 100 AS CHAR(7))), 7, ''0'') AS C_72,
       LPAD(TRIM(CAST(ROUND(C_73) AS CHAR(9))), 9, ''0'') AS C_73,
       LPAD(TRIM(C_74), 2, '' '') AS C_74,
       LPAD(TRIM(C_75), 16, '' '') AS C_75,
       LPAD(IIF(C_05 IN (''51'', ''19'', ''12''), ''N'', iif(ROUND(CEILING(ROUND(IIF(C_37 = T_37, C_43 + B_19, IIF(C_23 = ''X'', (C_43 / T_37 * C_37) + B_19, C_43 / T_37 * C_37)), 2)))>=C_00*10,''N'',TRIM(C_76))), 1, '' '') AS C_76,
       RPAD(IIF(C_05 IN (''51'', ''12''), '''',COALESCE(C_77, '''')), 6, '' '') AS C_77,
       RPAD(TRIM(CAST(IIF(C_05=''12'',0,IIF((C_24 = ''X'' OR C_25 = ''X'' OR C_26 = ''X'' OR C_27 = ''X'' OR C_30 > 0) AND (Tipo <> ''RU''), 0, C_78)) AS CHAR(1))), 1, ''0'') AS C_78,
       LPAD(COALESCE(C_79, '' ''), 1, '' '') C_79,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_80) || ''-'' || LPAD(EXTRACT(MONTH FROM C_80), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_80), 2, ''0''), '' ''), 10, '' '') C_80,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_81) || ''-'' || LPAD(EXTRACT(MONTH FROM C_81), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_81), 2, ''0''), '' ''), 10, '' '') C_81,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_82) || ''-'' || LPAD(EXTRACT(MONTH FROM C_82), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_82), 2, ''0''), '' ''), 10, '' '') C_82,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_83) || ''-'' || LPAD(EXTRACT(MONTH FROM C_83), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_83), 2, ''0''), '' ''), 10, '' '') C_83,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_84) || ''-'' || LPAD(EXTRACT(MONTH FROM C_84), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_84), 2, ''0''), '' ''), 10, '' '') C_84,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_85) || ''-'' || LPAD(EXTRACT(MONTH FROM C_85), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_85), 2, ''0''), '' ''), 10, '' '') C_85,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_86) || ''-'' || LPAD(EXTRACT(MONTH FROM C_86), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_86), 2, ''0''), '' ''), 10, '' '') C_86,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_87) || ''-'' || LPAD(EXTRACT(MONTH FROM C_87), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_87), 2, ''0''), '' ''), 10, '' '') C_87,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_88) || ''-'' || LPAD(EXTRACT(MONTH FROM C_88), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_88), 2, ''0''), '' ''), 10, '' '') C_88,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_89) || ''-'' || LPAD(EXTRACT(MONTH FROM C_89), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_89), 2, ''0''), '' ''), 10, '' '') C_89,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_90) || ''-'' || LPAD(EXTRACT(MONTH FROM C_90), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_90), 2, ''0''), '' ''), 10, '' '') C_90,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_91) || ''-'' || LPAD(EXTRACT(MONTH FROM C_91), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_91), 2, ''0''), '' ''), 10, '' '') C_91,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_92) || ''-'' || LPAD(EXTRACT(MONTH FROM C_92), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_92), 2, ''0''), '' ''), 10, '' '') C_92,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_93) || ''-'' || LPAD(EXTRACT(MONTH FROM C_93), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_93), 2, ''0''), '' ''), 10, '' '') C_93,
       LPAD(COALESCE(EXTRACT(YEAR FROM C_94) || ''-'' || LPAD(EXTRACT(MONTH FROM C_94), 2, ''0'') || ''-'' || LPAD(EXTRACT(DAY FROM C_94), 2, ''0''), '' ''), 10, '' '') C_94,
       LPAD(TRIM(CAST(ROUND(CEILING(ROUND(IIF(C_69 = 0 or C_24=''X'', 0, IIF(C_05 IN (''12'', ''19'',''51''), 0, IIF(C_36 = T_36, C_95 + B_19 + B_18, IIF(Tipo = ''RU'', (C_95 / T_36 * C_36) + B_19 + B_18+B_13, C_95 / T_36 * C_36)))), 2))) AS CHAR(9))), 9, ''0'') AS C_95,
       LPAD(ROUND(C_37 * 8), 3, ''0'') C_96,
       LPAD('' '', 10) C_97
FROM Nom_Pila_2838_Puesto N
JOIN T_Dias T USING (C_04)
WHERE C_36 + C_37 + C_38 + C_39 > 0
ORDER BY C_02', 'S');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('03', 'TOTAL APORTES DEL PERIODO PARA PENSIONES', '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('04', 'TOTAL APORTES DEL PERIODO PARA SALUD', '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('05', 'TOTAL APORTES DEL PERIODO PARA RIESGOS PROFESIONALES', '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('06', 'TOTAL APORTES DEL PERIODO PARA CAJAS DE COMPENSACION FAMILIAR',
          '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('07', 'TOTAL APORTES DEL PERIODO AL SENA', '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('08', 'TOTAL APORTES DEL PERIODO AL ICBF', '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('09', 'TOTAL APORTES DEL PERIODO A LA ESAP', '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('10', 'TOTAL APORTES DEL PERIODO AL MINISTERIO DE EDUCACION NACIONAL',
          '', 'N');
  INSERT INTO Componentes (Codigo, Nombre, Sql, Activo)
  VALUES ('11', 'TOTAL A PAGAR DURANTE EL PERIODO', NULL, 'N');
END;


SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Vector_Salud_Usu (
    Tipo_     CHAR(5),
    Prefijo_  CHAR(5),
    Numero_   CHAR(10),
    Modulo_   CHAR(10),
    Receptor_ CHAR(15))
RETURNS (
    Reps              CHAR(15),
    Tdoc              CHAR(5),
    Ndoc              CHAR(15),
    Apl1              CHAR(20),
    Apl2              CHAR(20),
    Nom1              CHAR(20),
    Nom2              CHAR(20),
    Mcon              CHAR(2),
    Cobe              CHAR(2),
    Numero_Contrato   CHAR(15),
    Poliza            CHAR(15),
    Copago            NUMERIC(18,2),
    Cuota_Moderadora  NUMERIC(18,2),
    Pagos_Compartidos NUMERIC(18,2))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Resumen        CHAR(10);
DECLARE VARIABLE V_Usuario        CHAR(15);
BEGIN

  Ndoc = :Receptor_;
  -- datos del tercero
  SELECT Pagador,
         Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador,
       V_Tipo_Documento,
       Apl1,
       Apl2,
       Nom1,
       Nom2;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  IF (:V_Pagador = 'N') THEN
  BEGIN
    --Usuario
    SELECT Reps,
           Cod_Modalidad,
           Cod_Cobertura,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Reps,
         Mcon,
         Cobe,
         Numero_Contrato,
         Poliza,
         Copago,
         Cuota_Moderadora,
         Pagos_Compartidos;
    SUSPEND;
  END
  ELSE
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

  FOR SELECT Reps,
             Cod_Modalidad,
             Cod_Cobertura,
             Numero_Contrato,
             Poliza,
             Copago,
             Cuota_Moderadora,
             Pagos_Compartidos,
             Codtercero
      FROM Fe_Vector_Salud(:V_Resumen) S
      INTO Reps,
           Mcon,
           Cobe,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos,
           V_Usuario
  DO
  BEGIN
    -- Datos del usuario
    SELECT Codidentidad,
           Apl1,
           Apl2,
           Nom1,
           Nom2
    FROM Terceros
    WHERE Codigo = :v_usuario
    INTO V_Tipo_Documento,
         Apl1,
         Apl2,
         Nom1,
         Nom2;

    SELECT Codigo2
    FROM Identidades
    WHERE Codigo = :V_Tipo_Documento
    INTO :Tdoc;

    SUSPEND;
  END

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TERCEROS TO PROCEDURE PZ_FE_VECTOR_SALUD_USU;
GRANT SELECT ON IDENTIDADES TO PROCEDURE PZ_FE_VECTOR_SALUD_USU;
GRANT EXECUTE ON PROCEDURE FE_VECTOR_SALUD_COMPROBANTE TO PROCEDURE PZ_FE_VECTOR_SALUD_USU;
GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_FE_VECTOR_SALUD_USU;
GRANT EXECUTE ON PROCEDURE FE_VECTOR_SALUD TO PROCEDURE PZ_FE_VECTOR_SALUD_USU;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_VECTOR_SALUD_USU TO SYSDBA;
CREATE OR ALTER PROCEDURE X_Rubros_1
AS
BEGIN
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI004', 'DIAS DE SUSPENSION LABORALES - SLN', 'ADICION', 'N', 'S',
          'N', 'VALOR', 0, 'PLANILLA03', NULL, NULL, 'C_05', 'VALOR', 'C_24',
          'C_83', 'C_84', NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI005', 'DIAS DEVENGADOS', 'ADICION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
    RESULT:=DI022
ELSE
    RESULT:=DIAS_TRABAJADOS - DIAS_YA_LQ - DI001 - DI002 - DI003 - DI004 -  DI006 - DI007 - DI009 -  DI011 - DI012 - DI018 - DI019 - DI020',
          2, NULL, NULL, 'DTRA', '', NULL, NULL, NULL, NULL, NULL, 'S', 'S',
          'S', 'S', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE020', 'HORA EXTRA DIURNAS 125%', 'ADICION', 'N', 'S', 'N',
          'ROUND(SAL_BASICO/C03_HMES*VALOR*C10_HED)', 0, 'PLANILLA20', NULL,
          'HED', 'C_20', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE021', 'HORA EXTRA NOCTURNA 175%', 'ADICION', 'N', 'S', 'N',
          'ROUND(SAL_BASICO/C03_HMES*VALOR*C11_HEN)', 0, 'PLANILLA21', NULL,
          'HEN', 'C_21', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS023', 'IBC OTROS PARAFISCALES', 'ADICION', 'N', 'N', 'N', 'IF(QUINCENA<2 OR ES_SENA=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<=C01_SMLV*10) THEN 
    RESULT:=0
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((SAL_BASICO*C05_S_INT/30*(SS013+ANTE_DIAS))+HE038+DV997+ANTE_OS)
ELSE
    RESULT:=ROUND((SAL_BASICO/30*(SS013+ANTE_DIAS))+HE038+DV997+ANTE_OS)', 5,
          NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE022', 'HORA EXTRA DIURNA DOMINICAL/FESTIVA 200%', 'ADICION', 'N',
          'S', 'N', 'ROUND(SAL_BASICO/C03_HMES*VALOR*C12_HEDD)', 0,
          'PLANILLA22', NULL, 'HEDDF', 'C_22', NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD999', 'TOTAL A PAGAR LIQUIDACION DEFINITIVA', 'ADICION', 'N', 'N',
          'N', 'LD073+LD075+LD076+LD077+LD079', 50, NULL, NULL, 'COMT', 'B_99',
          NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE023', 'HORA EXTRA NOCTURNA DOMINICAL/FESTIVA 250%', 'ADICION', 'N',
          'S', 'N', 'ROUND(SAL_BASICO/C03_HMES*VALOR*C13_HEND)', 0,
          'PLANILLA23', NULL, 'HENDF', 'C_23', NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SI015', 'DIAS TRABAJADOS PENDIENTES POR VACACIONES', 'ADICION', 'N',
          'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE024', 'HORA RECARGO NOCTURNO 35%', 'ADICION', 'N', 'S', 'N',
          'ROUND(SAL_BASICO/C03_HMES*VALOR*C14_RN)', 0, 'PLANILLA24', NULL,
          'HRN', 'C_24', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI023', 'DIAS DE LA FAMILIA - LR', 'ADICION', 'N', 'S', 'N', 'VALOR',
          0, 'PLANILLA03', NULL, 'LRQ', 'C_08', 'VALOR', 'B_17', 'C_89', 'C_90',
          NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE025', 'HORA RECARGO DOMINICAL DIURNO 175%', 'ADICION', 'N', 'S',
          'N', 'ROUND(SAL_BASICO/C03_HMES*VALOR*C17_RDF)', 0, 'PLANILLA25',
          NULL, 'HRDDF', 'C_25', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N',
          'N', 'N', 'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV030', 'AUXILIO DE TRANSPORTE', 'ADICION', 'N', 'N', 'N', 'IF(SAL_BASICO>C01_SMLV*2) THEN 
  RESULT:=0 
ELSE 
  RESULT:=ROUND(C02_AUXT/30*(DI005-DI008-DI021-DI023))', 4, 'PLANILLA11', NULL,
          NULL, 'C_16', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD073', 'VACACIONES - LIQUIDACION DEFINITIVA', 'ADICION', 'N', 'N',
          'N', 'ROUND((SAL_BASICO*LD053)/720)', 4, NULL, NULL, 'VAC2S', 'B_51',
          NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA031', 'SALARIO ORDINARIO', 'ADICION', 'N', 'N', 'N', 'IF(SA032>0) THEN 
  RESULT:=0 
ELSE 
  RESULT:=ROUND(SAL_BASICO/30*DI005)', 4, 'PLANILLA10', NULL, 'STRA', 'C_10',
          NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV999', 'TOTAL ADICIONES (DEVENGADO)', 'ADICION', 'N', 'N', 'N',
          'SA011+SA017+SA031+SA032+DV030+DV040+DV998+DV997+HE038', 49,
          'PLANILLA31', NULL, 'DEVT', 'C_97', NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT033', 'DESCUENTO EPS', 'DEDUCCION', 'N', 'N', 'N', 'IF (ES_MEDT=1) THEN 
   RESULT:=0
ELSE
IF(ES_SENA=1 AND ES_EPS_PE=0) THEN 
   RESULT:=0
ELSE
IF(ES_INTEGRA=1) THEN 
   RESULT:=ROUND(((SAL_BASICO*C05_S_INT)/30*(SS012-DI001-DI002-DI004-DI018-DI009-DI020)+HE038+DV997+VA080+SA009+SA020)*C30_EPS_PE/100)
ELSE
   RESULT:=ROUND((SAL_BASICO/30*(SS012-DI001-DI002-DI004-DI018-DI009-DI020)+HE038+DV997+VA080+SA009+SA020)*C30_EPS_PE/100)',
          6, 'PLANILLA50', NULL, 'EPSDTO', 'C_52', '', '', NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT034', 'DESCUENTO AFP', 'DEDUCCION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
   RESULT:=ROUND((((C01_SMLV/4)*SS009)+HE038+DV997)*C33_AFP_PE/100)
ELSE
IF(ES_INTEGRA=1) THEN 
   RESULT:=ROUND(((SAL_BASICO*C05_S_INT)/30*(SS014-DI002-DI004-DI018-DI009-DI020)+HE038+DV997+VA080+SA009+SA020)*C33_AFP_PE/100)
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
  RESULT:=0
ELSE
    RESULT:=ROUND((SAL_BASICO/30*(SS014-DI002-DI004-DI018-DI009-DI020)+HE038+DV997+VA080+SA009+SA020)*C33_AFP_PE/100)',
          6, 'PLANILLA51', NULL, 'AFPDTO', 'C_53', NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT035', 'DESCUENTO SOLIDARIDAD PENSIONAL', 'DEDUCCION', 'N', 'N',
          'N', 'IF(QUINCENA<2) THEN
    RESULT:=0
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
    RESULT:=0
ELSE
  IF(ES_INTEGRA=1) THEN
      RESULT:=ROUND((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL)*C05_S_INT*C36_AFP_SP/100-ANTE_SP)
  ELSE
    IF((SA027+ANTE_SAL)>=C01_SMLV*4) THEN
        RESULT:=ROUND((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL)*C36_AFP_SP/100-ANTE_SP)
    ELSE
        RESULT:=0', 6, 'PLANILLA51', '03', 'SPDTO', 'C_54', 'CALCULO', 'C_51',
          NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA017', 'SALARIO EN INCAPACIDADES', 'ADICION', 'N', 'N', 'N',
          'SA003 + SA009 + SA006 + SA007 + SA019 + SA020', 3, 'PLANILLA10',
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD053', 'DIAS PARA VACACIONES', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=DIAS_VACA+DI015', 3, NULL, NULL, NULL, 'B_13', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA027', 'SALARIO PARA PRESTACIONES SIN AUX', 'ADICION', 'N', 'N',
          'N',
          'SA011+SA031+SA032+HE038+DV997+SAL_BASICO/30*(DI002+DI003+DI006+DI007+DI009+DI018+DI019+DI020)',
          5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA080', 'SALARIO VACACIONES', 'ADICION', 'N', 'N', 'N', 'IF (VA012>=1) THEN
     RESULT:=ROUND(SAL_BASICO/30*DIAS+PROM_HR_VA)
ELSE
    RESULT:=0', 1, NULL, NULL, NULL, 'C_10', NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA083', 'PROMEDIO RECARGOS', 'ADICION', 'N', 'N', 'N', 'PROM_HR_VA',
          1, NULL, NULL, NULL, 'C_11', NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA084', 'TOTAL DEDUCCIONES EN VACACIONES', 'DEDUCCION', 'N', 'N',
          'N', 'DT033+DT046', 11, NULL, NULL, NULL, 'C_98', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA100', 'SALARIO BASICO', 'ADICION', 'N', 'N', 'N', 'IF(DIAS_TRABAJADOS - DIAS_YA_LQ=0) THEN
   RESULT:=0
ELSE
   RESULT:=SAL_BASICO', 0, 'PLANILLA01', NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA999', 'TOTAL A PAGAR EN VACACIONES', 'ADICION', 'N', 'N', 'N',
          'VA085-VA084', 50, NULL, NULL, 'COMT', 'C_99', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE027', 'HORA RECARGO DOMINICAL DIURNO CON COMPENSATORIO 75%',
          'ADICION', 'N', 'S', 'N',
          'ROUND(SAL_BASICO/C03_HMES*VALOR*C18_RDFCC)', 0, 'PLANILLA27', NULL,
          'HRDDF', 'C_27', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE028', 'HORA RECARGO NOCTURNO DOMINICAL CON COMPENSATORIO 110%',
          'ADICION', 'N', 'S', 'N',
          'ROUND(SAL_BASICO/C03_HMES*VALOR*C16_RNDCC)', 0, 'PLANILLA28', NULL,
          'HRNDF', 'C_28', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD075', 'PRIMA DE SERVICIOS - LIQUIDACION DEFINITIVA', 'ADICION',
          'N', 'N', 'N', 'IF (SAL_PREST>0)
  THEN RESULT:=ROUND((SAL_PREST + LD102 + AUXTRANS) *LD050/ 360)
  ELSE RESULT:=ROUND((SAL_BASICO + LD102 + AUXTRANS) *LD050/ 360)', 4,
          'PLANILLA90', NULL, 'PRIP', 'B_52', NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD076', 'CESANTIAS - LIQUIDACION DEFINITIVA', 'ADICION', 'N', 'N',
          'N', 'IF (SAL_PREST>0)
  THEN RESULT:=ROUND((SAL_PREST + LD102 + AUXTRANS) *LD051/ 360)
  ELSE RESULT:=ROUND((SAL_BASICO + LD102 + AUXTRANS) *LD051/ 360)', 4,
          'PLANILLA89', NULL, 'CESPA', 'B_53', NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE038', 'TOTAL HORAS EXTRAS Y RECARGOS', 'ADICION', 'N', 'N', 'N',
          'HE020+HE021+HE022+HE023+HE024+HE025+HE026+HE027+HE028', 1, NULL,
          NULL, NULL, NULL, 'CALCULO', 'C_23', NULL, NULL, NULL, 'N', 'N', 'N',
          'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD077', 'INTERESES A LAS CESANTIAS - LIQUIDACION DEFINITIVA',
          'ADICION', 'N', 'N', 'N', 'ROUND((LD076 *LD052* C23_PR_INT)/ 360)', 6,
          'PLANILLA88', NULL, 'CESIPA', 'B_54', NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD078', 'AUXILIO DE TRANSPORTE LIQUIDACION DEFINITIVA', 'ADICION',
          'N', 'N', 'N', 'AUXTRANS', 0, NULL, NULL, NULL, 'B_00', NULL, NULL,
          NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD102', 'PROMEDIO HORAS EXTRAS LIQUIDACION', 'ADICION', 'N', 'N',
          'N', 'IF((TOTA_DIAS+DI015)=0) THEN 
  RESULT:=0 
ELSE 
  RESULT:=ROUND(((TOTA_HE_RE+HE038)/(TOTA_DIAS+DI015))*30)', 3, NULL, NULL,
          NULL, 'B_01', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV998', 'TOTAL OTRAS ADICIONES NO SALARIABLES', 'ADICION', 'N', 'N',
          'N',
          'DV100+DV101+DV102+DV103+DV104+DV105+DV106+DV107+DV108+DV109+DV110',
          48, 'PLANILLA30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT999', 'TOTAL DEDUCCIONES (DEDUCIDO)', 'DEDUCCION', 'N', 'N', 'N',
          'DT033+DT046+DT998', 49, 'PLANILLA70', NULL, 'DEDT', 'C_98', NULL,
          NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI006', 'DIAS INCAPACIDAD x ACCIDENTE DE TRABAJO - IRL', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'INLAQ', 'C_06',
          'VALOR', 'C_30', 'C_93', 'C_94', '', 'S', 'S', 'S', 'S', 'S', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI007',
          'DIAS INCAPACIDAD x LICENCIA DE MATERNIDAD O PATERNIDAD - LMA',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'LMQ',
          'C_07', 'VALOR', 'C_26', 'C_87', 'C_88', 'C_59', 'S', 'S', 'S', 'S',
          'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI008', 'DIAS LICENCIA REMUNERADA - LR', 'ADICION', 'N', 'S', 'N',
          'VALOR', 0, 'PLANILLA03', NULL, 'LRQ', 'C_08', 'VALOR', 'B_17',
          'C_89', 'C_90', NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
END;

CREATE OR ALTER PROCEDURE X_Rubros_2
AS
BEGIN
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HM034', 'DESCUENTO AFP TIPO 51 POR DIAS LABORADOS', 'DEDUCCION', 'N',
          'N', 'N', 'IF(ES_MEDT=1) THEN 
   RESULT:=ROUND(((SAL_BASICO/30)*DI005)*C33_AFP_PE/100)
ELSE
IF(ES_INTEGRA=1) THEN 
   RESULT:=ROUND(((SAL_BASICO*C05_S_INT)/30*(SS014-DI002-DI004-DI018-DI009-DI020)+HE038+DV997+VA080+SA009+SA020)*C33_AFP_PE/100)
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
  RESULT:=0
ELSE
    RESULT:=ROUND((SAL_BASICO/30*(SS014-DI002-DI004-DI018-DI009-DI020)+HE038+DV997+VA080+SA009+SA020)*C33_AFP_PE/100)',
          6, 'PLANILLA51', NULL, 'AFPDTO', 'C_53', NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA011', 'SALARIO POR VACACIONES EN NOMINA - VAC', 'ADICION', 'N',
          'N', 'N', 'ROUND(SAL_BASICO/30*DI011)', 2, 'PLANILLA10', NULL,
          'VAC1S', 'C_11', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA008', 'SALARIO POR LICENCIA REMUNERADA', 'ADICION', 'N', 'N', 'N',
          'ROUND(SAL_BASICO/30*(DI008+DI023))', 4, NULL, NULL, 'LRV', NULL,
          NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI018', 'DIAS A NO PAGAR IEG MAYOR A 180 DIAS', 'ADICION', 'N', 'S',
          'N', 'VALOR', 0, 'PLANILLA03', NULL, 'INCOQ', 'C_13', 'VALOR', 'C_25',
          'C_85', 'C_86', 'C_57', 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS060', 'SALARIO EN SUSPENSIONES PARA PILA', 'ADICION', 'N', 'N',
          'N', 'ROUND((SS018/30)*(DI001+DI004+ DI012+ANTE_SLN))', 6, NULL, NULL,
          NULL, NULL, 'CALCULO', 'B_13', NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS044', 'APORTE AFP', 'ADICION', 'N', 'N', 'N',
          'IF(SS034 MOD 100 =0) THEN RESULT:=SS034 ELSE RESULT:=TRUNC(SS034/100)*100+100',
          10, NULL, '03', NULL, NULL, 'CALCULO', 'C_47', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT207', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 7', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_63', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT208', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 8', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_64', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT203', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 3', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_59', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT202', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 2', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_58', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT201', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 1', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_57', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA085', 'TOTAL ADICIONES EN VACACIONES', 'ADICION', 'N', 'N', 'N',
          'VA080+DV040', 11, NULL, NULL, NULL, 'C_97', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA006', 'SALARIO POR INCAPACIDAD DE ACCIDENTE DE TRABAJO - IRL',
          'ADICION', 'N', 'N', 'N', 'ROUND(SAL_BASICO/30*DI006)', 2, NULL, NULL,
          'INLAV', 'C_13', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA007', 'SALARIO INCAPACIDAD LICENCIA MATERNA O PATERNA - LMA',
          'ADICION', 'N', 'N', 'N', 'ROUND(SAL_BASICO/30*DI007)', 2, NULL, NULL,
          'LMV', 'C_14', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS014', 'DIAS COTIZADOS AFP', 'ADICION', 'N', 'N', 'N', 'IF(ES_ARL=1) THEN 
    RESULT:=0
ELSE
   RESULT:=DIAS_TRABAJADOS - DIAS_YA_LQ  - DI001 - DI012', 4, NULL, NULL, NULL,
          NULL, 'CALCULO', 'B_14', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
END;

CREATE OR ALTER PROCEDURE X_Rubros_3
AS
BEGIN
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI009', 'DIAS DE INCAPACIDAD x ENFERMEDAD COMUN AL 66%- IGE',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'INCOQ',
          'C_09', 'VALOR', 'C_25', 'C_85', 'C_86', 'C_57', 'S', 'S', 'S', 'S',
          'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT039', 'VALOR APORTE VOLUNTARIO A PENSION AFILIADO - AVP',
          'DEDUCCION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA51', NULL, NULL,
          'C_55', '', '', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS206',
          'DIAS LABORADOS EN OTRO CENTRO DE TRABAJO. VARIACION CENTROS DE TRABAJO - VCT',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL,
          'CALCULO', 'C_29', 'C_91', 'C_92', NULL, 'N', 'N', 'N', 'N', 'S', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI010', 'DIAS DE VACACIONES COMPENSADAS EN DINERO PAGADOS - VAC',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, NULL, NULL, 'VAC2Q', NULL, '',
          '', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS024', 'IBC AFP', 'ADICION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
   RESULT:=(((C01_SMLV/4)*SS009)+HE038+DV997)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((SAL_BASICO*C05_S_INT/30*(SS014-DI009-DI020))+HE038+DV997+SA009+SA020)
ELSE
IF(SAL_BASICO>=C01_SMLV) THEN 
    RESULT:=ROUND((SAL_BASICO/30*(SS014-DI009-DI020))+HE038+DV997+SA009+SA020)
ELSE 
    RESULT:=ROUND((C01_SMLV/30*SS014)+HE038+DV997)', 5, NULL, NULL, NULL, NULL,
          '', '', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV040', 'VALOR VACACIONES COMPENSADAS EN DINERO EN LA NOMINA',
          'ADICION', 'N', 'N', 'N', 'ROUND(SAL_BASICO/30*DI010)', 2, NULL, NULL,
          'VAC2S', NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT046', 'TOTAL DESCUENTO PENSION. CONTABILIZACION Y TIPO APORTES',
          'DEDUCCION', 'N', 'N', 'N', 'DT034 + DT035 + DT036 + DT039 + VA035',
          8, NULL, '03', NULL, NULL, '', '', NULL, NULL, NULL, 'N', 'N', 'N',
          'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS030', 'APORTE ARL SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA_LE=1) THEN
  RESULT:=0
ELSE
 RESULT:=(SS020*C38_ARL)/100', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS012', 'DIAS COTIZADOS EPS', 'ADICION', 'N', 'N', 'N', 'IF(ES_ARL=1) THEN 
    RESULT:=0
ELSE
IF (ES_MEDT=1) THEN
 RESULT:=0
ELSE
 RESULT:=DIAS_TRABAJADOS - DIAS_YA_LQ  - DI012', 4, NULL, NULL, NULL, NULL,
          'CALCULO', 'B_15', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS022', 'IBC EPS', 'ADICION', 'N', 'N', 'N', 'IF (QUINCENA<2 OR ES_MEDT=1) THEN
 RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<=C01_SMLV*10 AND ES_SENA=0 AND C30_EPS_PE>0) THEN 
    RESULT:=0
ELSE
IF(ES_INTEGRA=1) THEN 
   RESULT:=ROUND((SAL_BASICO*C05_S_INT/30*(SS012+ANTE_DIASE))+HE038+DV997+ANTE_OS)
ELSE
 IF(SAL_BASICO>=C01_SMLV) THEN 
     RESULT:=ROUND((SAL_BASICO/30*(SS012+ANTE_DIASE))+HE038+DV997+ANTE_OS)
 ELSE 
     RESULT:=ROUND((C01_SMLV/30*(SS012+ANTE_DIASE))+HE038+DV997+ANTE_OS)', 5,
          NULL, NULL, 'AFPIBC', NULL, '', '', NULL, NULL, NULL, 'N', 'N', 'N',
          'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS010', 'DIAS COTIZADOS ARL', 'ADICION', 'N', 'N', 'N',
          'DIAS_TRABAJADOS - DIAS_YA_LQ  - DI001 - DI002 - DI003 - DI004 - DI006 - DI007 - DI008 - DI009 - DI011 - DI012 - DI018 - DI019 - DI020 - DI021 - DI023',
          4, NULL, NULL, NULL, NULL, 'CALCULO', 'C_38', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS020', 'IBC ARL', 'ADICION', 'N', 'N', 'N', 'IF(ES_ARL=1) THEN 
    RESULT:=ROUND(BASICO/30*SS010)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND(SAL_BASICO*C05_S_INT/30*SS010+HE038+DV997)
ELSE
  IF(SAL_BASICO>=C01_SMLV) THEN 
     RESULT:=ROUND(CEIL(SAL_BASICO/30*SS010)+HE038+DV997)
  ELSE 
      RESULT:=ROUND(CEIL(C01_SMLV/30*SS010)+HE038+DV997)', 5, NULL, NULL, NULL,
          NULL, '', '', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS011', 'DIAS COTIZADOS CCF', 'ADICION', 'N', 'N', 'N', 'IF(ES_ARL=1) THEN 
    RESULT:=0
ELSE
   RESULT:=DIAS_TRABAJADOS - DIAS_YA_LQ  - DI001 - DI003 - DI004 - DI006 - DI009 - DI012 - DI018 - DI019 - DI020',
          4, NULL, NULL, NULL, NULL, 'CALCULO', 'B_16', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI021', 'DIAS DE HUELGA AUTORIZADA - LR', 'ADICION', 'N', 'S', 'N',
          'VALOR', 0, 'PLANILLA03', NULL, 'HUQ', 'C_08', 'VALOR', 'B_17',
          'C_89', 'C_90', NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS021', 'IBC CCF', 'ADICION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
   RESULT:=(C01_SMLV/4)*SS009
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((SAL_BASICO*C05_S_INT/30*SS011)+HE038+DV997+DV040+LD073)
ELSE
    RESULT:=ROUND(CEIL(SAL_BASICO/30*SS011)+HE038+DV997+DV040+LD073)', 5, NULL,
          NULL, NULL, NULL, '', '', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS049',
          'APORTE VOLUNTARIO DEL AFILIADO AL FONDO DE PENSIONES OBLIGATORIAS',
          'ADICION', 'N', 'N', 'N', 'DT039', 1, NULL, '03', NULL, NULL,
          'CALCULO', 'C_48', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS059',
          'COTIZACION VOLUNTARIA DEL APORTANTE AL FONDO DE PENSIONES OBLIGATIORIA - EMPRESA',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, NULL, '03', NULL, NULL,
          'CALCULO', 'C_49', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS207', 'VALOR NO RETENIDO POR APORTES VOLUNTAROS', 'ADICION', 'N',
          'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL, 'CALCULO', 'C_53', NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA021', 'SALARIO POR HUELGA AUTORIZADA', 'ADICION', 'N', 'N', 'N',
          'ROUND(SAL_BASICO/30*DI021)', 4, NULL, NULL, 'HUV', NULL, NULL, NULL,
          NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS046', 'APORTES ESAP', 'ADICION', 'N', 'N', 'N',
          'IF(SS036 MOD 100 =0) THEN RESULT:=SS036 ELSE RESULT:=TRUNC(SS036/100)*100+100',
          7, NULL, '09', NULL, NULL, 'CALCULO', 'C_71', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA001', 'SALARIO TRANSITORIO VST', 'ADICION', 'N', 'N', 'N',
          'HE038+DV997', 9, NULL, NULL, NULL, NULL, 'CALCULO', 'B_19', NULL,
          NULL, NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SI052', 'DIAS TRABAJADOS INICIALES PARA INTERESES A LAS CESANTIAS',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS047', 'APORTES MEN', 'ADICION', 'N', 'N', 'N',
          'IF(SS037 MOD 100 =0) THEN RESULT:=SS037 ELSE RESULT:=TRUNC(SS037/100)*100+100',
          7, NULL, '10', NULL, NULL, 'CALCULO', 'C_73', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT209', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 9', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_65', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA003', 'SALARIO INCAPACIDAD ENFERMEDAD COMUN AL 100% - IGE',
          'ADICION', 'N', 'N', 'N', 'ROUND(SAL_BASICO/30*DI003)', 2, NULL, NULL,
          'INCOV', 'C_12', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA009', 'SALARIO INCAPACIDAD ENFERMEDAD COMUN AL 66% - IGE',
          'ADICION', 'N', 'N', 'N', 'IF(SAL_BASICO*(C04_S_IGE/100)>C01_SMLV) THEN   
  RESULT:= ROUND((SAL_BASICO/30*DI009)*(C04_S_IGE/100))
ELSE
  RESULT:= ROUND(C01_SMLV/30*DI009)', 2, NULL, NULL, 'INCOV', 'C_15', NULL,
          NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD050', 'DIAS PARA PRIMA', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_INTEGRA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=DIAS_PRIMA+DI015', 3, NULL, NULL, 'PRIQ', 'B_10', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD051', 'DIAS PARA CESANTIAS', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_INTEGRA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=DIAS_CES+DI015', 3, NULL, NULL, NULL, 'B_11', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD052', 'DIAS INTERESES A LAS CESANTIAS', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_INTEGRA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=DIAS_INT+DI015', 3, NULL, NULL, NULL, 'B_12', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS031', 'APORTE CCF SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE
  RESULT:=SS021*C37_CCF/100', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT210', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 10', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_66', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT211', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 11', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_67', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI001', 'DIAS DE LICENCIA NO REMUNERADA - SLN', 'ADICION', 'N', 'S',
          'N', 'VALOR', 0, 'PLANILLA03', NULL, 'LNQ', 'C_02', 'VALOR', 'C_24',
          'C_83', 'C_84', NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI012', 'DIAS DE COMISION DE SERVICIOS - SLN', 'ADICION', 'N', 'S',
          'N', 'VALOR', 0, 'PLANILLA03', NULL, NULL, 'C_01', 'VALOR', 'C_24',
          'C_83', 'C_84', NULL, 'S', 'S', 'S', 'S', 'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT212', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 12', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_68', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT213', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 13', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_69', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT214', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 14', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_70', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI017', 'DIAS LABORADOS', 'ADICION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
    RESULT:=DI022
ELSE
    RESULT:=DIAS_TRABAJADOS - DIAS_YA_LQ - DI001 - DI002 - DI003 - DI004 -   DI006  -  DI007  -  DI008  -  DI009 - DI011 - DI012  - DI019 - DI020 - DI021 - DI023',
          2, 'PLANILLA02', NULL, NULL, 'C_00', NULL, NULL, NULL, NULL, NULL,
          'S', 'S', 'S', 'S', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT215', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 15', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_71', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT216', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 16', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_72', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT217', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 17', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_73', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT218', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 18', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_74', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT219', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 19', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_75', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT220', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 20', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_76', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV109', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 9', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_39', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV110', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 10', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_40', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS032', 'APORTE EPS SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 AND ES_EPS_EM=0) THEN 
   RESULT:=SS022*C32_EPS_PI/100+SS050
ELSE
   RESULT:=SS022*C31_EPS_EM/100+SS050', 9, NULL, NULL, NULL, NULL, '', '', NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV997', 'TOTAL OTRAS ADICIONES SALARIABLES', 'ADICION', 'N', 'N',
          'N',
          'DV200+DV201+DV202+DV203+DV204+DV205+DV206+DV207+DV208+DV209+DV210',
          4, 'PLANILLA30', NULL, NULL, NULL, 'CALCULO', 'C_23', NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV200', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 0', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_41', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV201', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 1', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_42', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV202', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 2', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_43', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV203', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 3', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_44', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV204', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 4', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_45', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV205', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 5', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_46', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS050', 'DIFERENCIA DESCUENTO EPS SALARIO MENOR SMLV', 'DEDUCCION',
          'N', 'N', 'N', 'IF (ES_MEDT=0) THEN 
   RESULT:=0
ELSE
IF(ES_SENA=1 AND ES_EPS_PE=0) THEN 
   RESULT:=0
ELSE
IF(SAL_BASICO>=C01_SMLV) THEN 
   RESULT:=0
ELSE 
    RESULT:=ROUND((C01_SMLV/30*(SS012-DI002-DI001-DI004)+HE038+DV997+SAL_BASICO/30*DI002)*C30_EPS_PE/100)-DT033',
          7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
END;

CREATE OR ALTER PROCEDURE X_Rubros_4
AS
BEGIN
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS038', 'DESCUENTO EPS REDONDEADO', 'DEDUCCION', 'N', 'N', 'N',
          'IF(DT033 MOD 100 =0) THEN RESULT:=DT033 ELSE RESULT:=TRUNC(DT033/100)*100+100',
          8, NULL, '04', NULL, NULL, 'CALCULO', 'C_55', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA026', 'SALARIO PARA PRESTACIONES SIN AUX Y SIN VAC', 'ADICION',
          'N', 'N', 'N',
          'SA011+SA031+SA032+HE038+DV997+SAL_BASICO/30*(DI003+DI006+DI007+DI009+DI019+DI020)',
          5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SI050', 'DIAS TRABAJADOS INICIALES PARA PRIMA', 'ADICION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS013', 'DIAS COTIZADOS OTROS PARAFISCALES', 'ADICION', 'N', 'N',
          'N', 'IF(ES_ARL=1) THEN 
    RESULT:=0
ELSE
   RESULT:=DIAS_TRABAJADOS - DIAS_YA_LQ  - DI001 - DI004 - DI012', 4, NULL,
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS019', 'IBC AFP SIN VST - PILA', 'ADICION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
   RESULT:=(C01_SMLV/4)*SS009
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=((SAL_BASICO*C05_S_INT/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ-DI009-DI020)+SA009+SA020)
ELSE
IF(SAL_BASICO>=C01_SMLV) THEN 
    RESULT:=((SAL_BASICO/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ-DI009-DI020)+SA009+SA020)
ELSE 
    RESULT:=((C01_SMLV/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ))', 5, NULL, NULL,
          NULL, NULL, 'CALCULO', 'C_42', NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV206', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 6', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_47', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV207', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 7', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_48', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV208', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 8', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_49', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV209', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 9', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_50', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SI051', 'DIAS TRABAJADOS INICIALES PARA CESANTIAS', 'ADICION', 'N',
          'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS051', 'DIFERENCIA DESCUENTO AFP SALARIO MENOR SMLV', 'DEDUCCION',
          'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_PENSION=1) THEN
  RESULT:=0
ELSE
IF(SAL_BASICO>=C01_SMLV) THEN 
   RESULT:=0
ELSE 
    RESULT:=ROUND((C01_SMLV/30*(SS012-DI002-DI001-DI004)+HE038+DV997+SAL_BASICO/30*DI002)*C33_AFP_PE/100)-DT034',
          7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV210', 'AQUI OTRA ADICION SALARIABLE TIPO NOVEDAD 10', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_51', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS033', 'APORTE ICBF SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE
  RESULT:=SS023*C39_ICBF/100', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS009', 'CANTIDAD MONTO MINIMO SEMANAL TIPO 51', 'ADICION', 'N', 'N',
          'N', 'IF (DI022<=7) THEN
 RESULT:=1
ELSE
 IF (DI022<=14) THEN
  RESULT:=2
 ELSE
  IF (DI022<=21) THEN
   RESULT:=3
 ELSE
  RESULT:=4', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS035', 'APORTE SENA SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE
  RESULT:=SS023*C40_SENA/100', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS018', 'IBC OTROS PARAFISCALES SIN VST - PILA', 'ADICION', 'N', 'N',
          'N', 'IF(QUINCENA<2 OR ES_SENA=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<=C01_SMLV*10) THEN 
    RESULT:=0
ELSE
  IF(ES_INTEGRA=1) THEN 
      RESULT:=((SAL_BASICO*C05_S_INT/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS))
  ELSE
       RESULT:=((SAL_BASICO/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS))', 5,
          NULL, NULL, NULL, NULL, 'CALCULO', 'C_95', NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
END;

CREATE OR ALTER PROCEDURE X_Rubros_5
AS
BEGIN
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS036', 'APORTE ESAP SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE
  RESULT:=SS023*C41_ESAP/100', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS017', 'IBC EPS SIN VST - PILA', 'ADICION', 'N', 'N', 'N', 'IF(QUINCENA<2 OR ES_MEDT=1) THEN 
   RESULT:=0
ELSE
IF(ES_INTEGRA=1) THEN 
   RESULT:=((SAL_BASICO*C05_S_INT/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIAS))
ELSE
 IF(SAL_BASICO>=C01_SMLV) THEN 
     RESULT:=((SAL_BASICO/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIASE))
 ELSE 
     RESULT:=((C01_SMLV/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIASE))', 5, NULL,
          NULL, NULL, NULL, 'CALCULO', 'C_43', NULL, NULL, NULL, 'N', 'N', 'N',
          'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS037', 'APORTE MEN SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE
  RESULT:=SS023*C42_MEN/100', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS016', 'IBC CCF SIN VST - PILA', 'ADICION', 'N', 'N', 'N', 'IF(ES_MEDT=1) THEN 
   RESULT:=(C01_SMLV/4)*SS009
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=((SAL_BASICO*C05_S_INT/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ))
ELSE
    RESULT:=((SAL_BASICO/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ))', 5, NULL, NULL,
          NULL, NULL, 'CALCULO', 'C_45', NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA100', 'SALARIO BASICO EN VACACIONES', 'ADICION', 'N', 'N', 'N', 'IF (VA012>=1) THEN
     RESULT:=SAL_BASICO
ELSE
    RESULT:=0', 1, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'N', 'N',
          'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA002', 'SALARIO VACACIONES PARA PILA COMPENSADAS Y EN LIQUIDACION',
          'ADICION', 'N', 'N', 'N', 'DV040+LD073', 9, NULL, NULL, NULL, NULL,
          'CALCULO', 'B_18', NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA035', 'DESCUENTO SOLIDARIDAD PENSIONAL', 'DEDUCCION', 'N', 'N',
          'N', 'IF(ES_PENSION=1) THEN
    RESULT:=0
ELSE
  IF(ES_INTEGRA=1) THEN
      RESULT:=ROUND(VA080*C05_S_INT*C36_AFP_SP/100-ANTE_SP)
  ELSE
    IF(SAL_BASICO>=C01_SMLV*4) THEN
        RESULT:=ROUND(VA080*C36_AFP_SP/100-ANTE_SP)
    ELSE
        RESULT:=0', 6, 'PLANILLA51', '03', NULL, 'C_54', 'CALCULO', 'C_51',
          NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS015', 'IBC ARL SIN VST - PILA', 'ADICION', 'N', 'N', 'N', 'IF(ES_ARL=1) THEN 
    RESULT:=BASICO
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=C01_SMLV
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=((SAL_BASICO*C05_S_INT/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ))
ELSE
  IF(SAL_BASICO>=C01_SMLV) THEN 
     RESULT:=((SAL_BASICO/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ))
  ELSE 
      RESULT:=((C01_SMLV/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ))', 5, NULL, NULL,
          NULL, NULL, 'CALCULO', 'C_44', NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS034', 'APORTE AFP SIN REDONDEO', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_PENSION=1) THEN
  RESULT:=0
ELSE
  RESULT:=SS024*C34_AFP_EM/100+SS051', 9, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS039', 'DESCUENTO AFP REDONDEADO', 'DEDUCCION', 'N', 'N', 'N',
          'IF(DT034 MOD 100 =0) THEN RESULT:=DT034 ELSE RESULT:=TRUNC(DT034/100)*100+100',
          8, NULL, '03', NULL, NULL, 'CALCULO', 'C_47', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI011', 'DIAS DE VACACIONES DISFRUTADAS PAGADAS EN LA NOMINA - VAC',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'VAC1Q',
          'C_10', 'VALOR', 'C_27', 'C_89', 'C_90', NULL, 'S', 'S', 'S', 'S',
          'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV100', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 0', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_30', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD079', 'VALOR INDEMNIZACION - LIQUIDACION DEFINITIVA', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'B_55', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI019', 'DIAS INCAPACIDAD x ENFERMEDAD PROFESIONAL AL 100% - IGE',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'INPRQ',
          'C_11', 'VALOR', 'C_25', 'C_85', 'C_86', 'C_57', 'S', 'S', 'S', 'S',
          'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA036', 'SALARIO BASE PARA SOLIDARIDAD PENSIONAL Y SUBSISTENCIA',
          'ADICION', 'N', 'N', 'N', 'IF(QUINCENA<2) THEN
  RESULT:=0
ELSE
IF(SA027+ANTE_SAL>=C01_SMLV*4) THEN
        RESULT:=SA027+ANTE_SAL
    ELSE
        RESULT:=0', 6, NULL, NULL, 'SPIBC', NULL, NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT036', 'DESCUENTO SUBSISTENCIA', 'DEDUCCION', 'N', 'N', 'N', 'IF(SA036<(C01_SMLV*16)) THEN
  RESULT:=0
ELSE
IF((SA036>=(C01_SMLV*16)) AND (SA036<(C01_SMLV*17))) THEN
  RESULT:=ROUND(SA036*(C36_AFP_S1/100))
ELSE
IF((SA036>=(C01_SMLV*17)) AND (SA036<(C01_SMLV*18))) THEN
  RESULT:=ROUND(SA036*(C36_AFP_S2/100))
ELSE
IF((SA036>=(C01_SMLV*18)) AND (SA036<(C01_SMLV*19))) THEN
  RESULT:=ROUND(SA036*(C36_AFP_S3/100))
ELSE
IF((SA036>=(C01_SMLV*19)) AND (SA036=(C01_SMLV*20))) THEN
  RESULT:=ROUND(SA036*(C36_AFP_S4/100))
ELSE
IF(SA036>(C01_SMLV*20)) THEN
  RESULT:=ROUND(SA036*(C36_AFP_S5/100))
ELSE
  RESULT:=0', 7, 'PLANILLA51', '03', 'SUBDTO', '', 'CALCULO', 'C_52', NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI020', 'DIAS DE INCAPACIDAD x ENFERMEDAD PROFESIONAL AL 66%- IGE',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'INPRQ',
          'C_12', 'VALOR', 'C_25', 'C_85', 'C_86', 'C_57', 'S', 'S', 'S', 'S',
          'S', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI022', 'DIAS LABORADOS PARA EMPLEADOS TIPO 51', 'ADICION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA019', 'SALARIO INCAPACIDAD ENFERMEDAD PROFESIONAL AL 100% - IGE',
          'ADICION', 'N', 'N', 'N', 'ROUND(SAL_BASICO/30*DI019)', 2, NULL, NULL,
          'INPRV', 'C_12', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SI102', 'SALDO INICIAL DE HORAS EXTRAS Y RECARGOS', 'ADICION', 'N',
          'S', 'N', 'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA020', 'SALARIO INCAPACIDAD ENFERMEDAD PROFESIONAL AL 66% - IGE',
          'ADICION', 'N', 'N', 'N', 'IF(SAL_BASICO*(C04_S_IGE/100)>C01_SMLV) THEN   
  RESULT:= ROUND((SAL_BASICO/30*DI020)*(C04_S_IGE/100))
ELSE
  RESULT:= ROUND(C01_SMLV/30*DI020)', 2, NULL, NULL, 'INPRV', 'C_15', NULL,
          NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('IN001', 'VALOR MONETARIO EN DOTACION', 'ADICION', 'N', 'S', 'N',
          'VALOR', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV101', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 1', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_31', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA032', 'SALARIO ORDINARIO PRORRATEADO', 'ADICION', 'N', 'S', 'N',
          'ROUND(SAL_BASICO/30*DI005*VALOR/100)', 3, 'PLANILLA10', NULL, NULL,
          'C_10', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N',
          'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV102', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 2', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_32', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV103', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 3', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_33', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV104', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 4', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_34', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV105', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 5', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_35', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV106', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 6', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_36', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV107', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 7', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_37', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DV108', 'AQUI OTRA ADICION NO SALARIABLE TIPO NOVEDAD 8', 'ADICION',
          'N', 'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_38', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT200', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 0', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_56', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT204', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 4', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_60', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT205', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 5', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_61', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT206', 'AQUI OTRA DEDUCCION TIPO NOVEDAD 6', 'DEDUCCION', 'N', 'S',
          'N', 'VALOR', 0, NULL, NULL, NULL, 'C_62', NULL, NULL, NULL, NULL,
          NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DT998', 'TOTAL OTRAS DEDUCCIONES', 'DEDUCCION', 'N', 'N', 'N',
          'DT200+DT201+DT202+DT203+DT204+DT205+DT206+DT207+DT208+DT209+DT210+DT211+DT212+DT213+DT214+DT215+DT216+DT217+DT218+DT219+DT220',
          48, 'PLANILLA60', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'N');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD070', 'PRIMA DE SERVICIOS', 'ADICION', 'N', 'N', 'N',
          'ROUND((SAL_PREST + LD102 + AUXTRANS) *LD050/ 360)', 4, 'PLANILLA90',
          NULL, 'PRIP', NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS040', 'APORTE ARL', 'ADICION', 'N', 'N', 'N',
          'IF(SS030 MOD 100 =0) THEN RESULT:=SS030 ELSE RESULT:=TRUNC(SS030/100)*100+100',
          7, NULL, '05', NULL, NULL, 'CALCULO', 'C_63', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('VA012', 'DIAS HABILES A DISFRUTAR DE VACACIONES', 'ADICION', 'N',
          'S', 'N', 'VALOR', 0, NULL, NULL, NULL, 'C_00', NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS041', 'APORTE CCF', 'ADICION', 'N', 'N', 'N',
          'IF(SS031 MOD 100 =0) THEN RESULT:=SS031 ELSE RESULT:=TRUNC(SS031/100)*100+100',
          7, NULL, '06', NULL, NULL, 'CALCULO', 'C_65', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS042', 'APORTE EPS', 'ADICION', 'N', 'N', 'N',
          'IF(SS032 MOD 100 =0) THEN RESULT:=SS032 ELSE RESULT:=TRUNC(SS032/100)*100+100',
          10, NULL, '04', NULL, NULL, 'CALCULO', 'C_55', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS043', 'APORTE ICBF', 'ADICION', 'N', 'N', 'N',
          'IF(SS033 MOD 100 =0) THEN RESULT:=SS033 ELSE RESULT:=TRUNC(SS033/100)*100+100',
          7, NULL, '08', NULL, NULL, 'CALCULO', 'C_69', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD072', 'INTERESES A LAS CESANTIAS', 'ADICION', 'N', 'N', 'N',
          'ROUND((LD071*LD052*C23_PR_INT)/360)', 6, 'PLANILLA89', NULL,
          'CESIPA', NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI015', 'DIAS PARA PRESTACIONES', 'ADICION', 'N', 'N', 'N',
          'DIAS_TRABAJADOS - DIAS_YA_LQ - DI001 - DI004 - DI012', 2, NULL, NULL,
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('LD071', 'CESANTIAS', 'ADICION', 'N', 'N', 'N',
          'ROUND((SAL_PREST + LD102 + AUXTRANS) *LD051/ 360)', 4, 'PLANILLA89',
          NULL, 'CESPA', NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N',
          'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SA028', 'SALARIO PARA PRESTACIONES CON AUX', 'ADICION', 'N', 'N',
          'N', 'IF(SAL_BASICO>C01_SMLV*2) THEN 
   RESULT:=SA027 
ELSE 
   RESULT:=ROUND(C02_AUXT/30*DI015+SA027)', 6, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('SS045', 'APORTE SENA', 'ADICION', 'N', 'N', 'N',
          'IF(SS035 MOD 100 =0) THEN RESULT:=SS035 ELSE RESULT:=TRUNC(SS035/100)*100+100',
          7, NULL, '07', NULL, NULL, 'CALCULO', 'C_67', NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('TP999', 'TOTAL A PAGAR', 'ADICION', 'N', 'N', 'N', 'DV999-DT999', 50,
          'T_PAGAR', NULL, 'COMT', 'C_99', NULL, NULL, NULL, NULL, NULL, 'N',
          'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('PR060', 'PROVISION CESANTIAS', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_INTEGRA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=ROUND(SA028/C22_PR_CES)', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('PR061', 'PROVISION PRIMA', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1 OR ES_INTEGRA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=ROUND(SA028/C21_PR_PRI)', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('PR062', 'PROVISION INTERESES A LAS CESANTIAS', 'ADICION', 'N', 'N',
          'N', 'ROUND(PR060*C23_PR_INT)', 8, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('PR063', 'PROVISION VACACIONES', 'ADICION', 'N', 'N', 'N', 'IF(ES_SENA=1) THEN
  RESULT:=0
ELSE 
  RESULT:=ROUND((SAL_BASICO/30*DI015+HE024+HE025)/C20_PR_VAC)', 7, NULL, NULL,
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('HE026', 'HORA RECARGO NOCTURNO DOMINICAL 210%', 'ADICION', 'N', 'S',
          'N', 'ROUND(SAL_BASICO/C03_HMES*VALOR*C15_RND)', 0, 'PLANILLA26',
          NULL, 'HRNDF', 'C_26', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N',
          'N', 'N', 'S', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI002', 'DIAS DE VACACIONES NO LIQUIDADOS EN QUINCENA - VAC',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'VAC1Q',
          'C_03', 'VALOR', 'C_27', 'C_89', 'C_90', '', 'S', 'S', 'S', 'S', 'S',
          'N', 'S');
  INSERT INTO Rubros (Codigo, Nombre, Tipo, Publicar, Novedad, Acumula, Formula,
                      Orden, Codconjunto, Codcomponente, Codgrupo_Ne, Columna,
                      Dato_Pila, Columna_Pila, Columna_Inicio, Columna_Fin,
                      Columna_Texto, Dias_Pension, Dias_Salud, Dias_Arl,
                      Dias_Ccf, Dias_Nomina, Control_Tiempo, Nativo)
  VALUES ('DI003', 'DIAS INCAPACIDAD x ENFERMEDAD COMUN AL 100% - IGE',
          'ADICION', 'N', 'S', 'N', 'VALOR', 0, 'PLANILLA03', NULL, 'INCOQ',
          'C_04', 'VALOR', 'C_25', 'C_85', 'C_86', 'C_57', 'S', 'S', 'S', 'S',
          'S', 'N', 'S');
END;


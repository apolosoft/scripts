SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Vector_Salud (
    Tipo_     CHAR(5),
    Prefijo_  CHAR(5),
    Numero_   CHAR(10),
    Modulo_   CHAR(10),
    Receptor_ CHAR(15))
RETURNS (
    Desde       DATE,
    Hasta       DATE,
    Codreferido CHAR(15))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
BEGIN

  -- datos del tercero
  SELECT Pagador

  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'N') THEN
    SELECT Desde,
           Hasta,
           Codreferido
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Desde,
         Hasta,
         Codreferido;
  ELSE
  BEGIN
    Codreferido = 'NA';
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    SELECT FIRST 1 Desde,
                   Hasta
    FROM Fe_Vector_Salud(:V_Resumen)
    INTO Desde,
         Hasta;
  END
  SUSPEND;

END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TERCEROS TO PROCEDURE PZ_FE_VECTOR_SALUD;
GRANT EXECUTE ON PROCEDURE FE_VECTOR_SALUD_COMPROBANTE TO PROCEDURE PZ_FE_VECTOR_SALUD;
GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_FE_VECTOR_SALUD;
GRANT EXECUTE ON PROCEDURE FE_VECTOR_SALUD TO PROCEDURE PZ_FE_VECTOR_SALUD;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_VECTOR_SALUD TO SYSDBA;
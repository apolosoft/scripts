UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 964 10/Abr/2024 v1

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)
WHERE Nvfor_oper = ''SS-CUFE''', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (271, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'FACTURA', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (272, 'ges:transporte1', 'DOC', '-- Transporte 1
 SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr"
        FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero, ''GESTION'')
        WHERE Renglon = :Renglon
    AND  Natr IN (''01'', ''02'')', 'S', 32, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (273, 'ges:transporte2', 'DOC', '-- Transporte 2
 
SELECT   Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               Uatr "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
FROM Pz_Fe_Transporte(:Tipo, :Prefijo, :Numero, :Renglon)  
WHERE  Natr =''03''', 'S', 33, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (287, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Exportacion

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'EXPORTACION', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)
WHERE Nvfor_oper = ''SS-CUFE', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (309, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Contingencia

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (327, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (348, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (367, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 31, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste a DS

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Cdet "noov:Nvfac_cdet",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, 2)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_Carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, 2)) "noov:Nvfac_totp",
       Nvfac_Roun "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Zipc "noov:Nvpro_zipc",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor, :Clase_Fe)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (387, 'ges:tasa_alterna', 'DOC', '--Tasa Cambio Alterna Gestion Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'N', 21, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambioAlternativa', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


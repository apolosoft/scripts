EXECUTE BLOCK (
    Periodo_ INTEGER = :Periodo_)
RETURNS (
    Empleado CHAR(15),
    Dias     DOUBLE PRECISION,
    Salario  DOUBLE PRECISION,
    Promedio DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nomina   CHAR(5);
DECLARE VARIABLE V_Dias     DOUBLE PRECISION;
DECLARE VARIABLE V_Salario  DOUBLE PRECISION;
DECLARE VARIABLE V_Desde    DATE;
DECLARE VARIABLE V_Hasta    DATE;
DECLARE VARIABLE V_Ingreso  DATE;
DECLARE VARIABLE V_Retiro   DATE;
DECLARE VARIABLE V_Ini      DATE;
DECLARE VARIABLE V_Fin      DATE;
DECLARE VARIABLE V_Meses    DOUBLE PRECISION;
DECLARE VARIABLE V_Mes_Real INTEGER;
BEGIN
  V_Ini = '01.01.' || :Periodo_;
  V_Fin = '31.12.' || :Periodo_;

  FOR SELECT Codigo
      FROM Personal
      INTO Empleado
  DO
  BEGIN
    -- fecha ingreso
    SELECT Ingreso, Retiro
    FROM Pz_Fecha_Salarios(:Empleado)
    INTO V_Ingreso, V_Retiro;
    V_Retiro = COALESCE(:V_Retiro, :V_Fin);
    IF (:V_Retiro > :V_Fin) THEN
      V_Retiro = :V_Fin;

    IF (V_Ingreso < V_Fin AND
        V_Retiro > V_Ini) THEN
    BEGIN
      IF (V_Ingreso < :V_Ini) THEN
        V_Ingreso = :V_Ini;

      V_Meses = (:V_Retiro - :V_Ingreso);
      V_Meses = :V_Meses / 30;
      V_Mes_Real = ROUND(:V_Meses);

      --caso 1
      IF (:V_Mes_Real <= 6) THEN
      BEGIN
        V_Desde = :V_Ingreso;
        V_Hasta = :V_Retiro;
      END
      ELSE
      BEGIN
        -- caso 2
        IF (:V_Retiro = :V_Fin) THEN
        BEGIN
          V_Desde = '01.07.' || :Periodo_;
          V_Hasta = '31.12.' || :Periodo_;
        END
        ELSE
        BEGIN
          V_Desde = DATEADD(-6 MONTH TO :V_Retiro);
          V_Hasta = :V_Retiro;
        END
      END

      -- Planilla
      SELECT SUM(P.Adicion + P.Deduccion)
      FROM Planillas P
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      WHERE P.Codrubro = 'DI015'
            AND P.Codpersonal = :Empleado
            AND N.Fecha >= :V_Desde
            AND N.FECHA <= :V_HASTA
      INTO Dias;
      Dias = COALESCE(Dias, 0);

      SELECT SUM(P.Adicion + P.Deduccion)
      FROM Planillas P
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      WHERE P.Codrubro = 'SA027'
            AND P.Codpersonal = :Empleado
            AND N.Fecha >= :V_Desde
            AND N.FECHA <= :V_HASTA
      INTO Salario;
      Salario = COALESCE(Salario, 0);

      IF (Dias > 0) THEN
        Promedio = (Salario / Dias) * 30;
      ELSE
        Promedio = 0;
      SUSPEND;
    END

  END
END
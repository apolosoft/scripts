UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('CONTINGENCIA', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('CONTINGENCIA DIAN', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('DOCUMENTO SOPORTE', 30)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('FACTURA', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO POS', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA DE AJUSTE A DS', 30)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA DEBITO', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('POS ELECTRONICO', 0)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NA', 0)
                        MATCHING (CODIGO);

UPDATE OR INSERT INTO SISTEMA (CODIGO, VALOR)
                       VALUES ('FE_DIAS', '60')
                     MATCHING (CODIGO);

COMMIT WORK;


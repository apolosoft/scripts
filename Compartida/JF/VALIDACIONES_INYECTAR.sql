UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (1, 'TERCEROS', 'NATURALEZA', 'SELECT Naturaleza
FROM Terceros
WHERE Codigo = '':TERCERO''', 'N,J', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (2, 'TERCEROS', 'NATURALEZA', 'SELECT Naturaleza
FROM Terceros
WHERE Codigo = '':TERCERO''', 'N,J', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (3, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,47,48,50,91', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (4, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,47,48,50,91', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (7, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Impuestos I
JOIN Tipoimpuestos T ON (I.Tipo_Impuesto = T.Codigo)
WHERE TRIM(I.Tipo) = '':TIPO''
      AND TRIM(I.Prefijo) = '':PREFIJO''
      AND TRIM(I.Numero) = '':NUMERO''', '00,01,02,03,04,21,22,23,24,25,26,33,34,35,ZY,ZZ,99', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (8, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Impuestos('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tipoimpuestos T ON (T.Codigo = I.Codtipoimpuesto)', '00,01,02,03,04,21,22,23,24,25,26,33,34,35,ZY,ZZ,99', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (9, 'TIPORETENCIONES', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Retenciones R
JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
WHERE TRIM(R.Tipo) = '':TIPO''
      AND TRIM(R.Prefijo) = '':PREFIJO''
      AND TRIM(R.Numero) = '':NUMERO''', '05,06,07,20,25,26,ZZ', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (10, 'TIPORETENCIONES', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Retenciones('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tiporetenciones T ON (T.Codigo = I.Codtiporetencion)', '05,06,07,20,25,26,ZZ', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (5, 'SOCIEDADES', 'CODIGO_FE', 'SELECT S.Codigo_Fe
FROM Terceros T
JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE T.Codigo = '':TERCERO''', '04,05,48,49', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (6, 'SOCIEDADES', 'CODIGO_FE', 'SELECT S.Codigo_Fe
FROM Terceros T
JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE T.Codigo = '':TERCERO''', '04,05,48,49', 'CONTABLE')
                          MATCHING (ID);


COMMIT WORK;


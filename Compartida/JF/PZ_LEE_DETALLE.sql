SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Lee_Detalle (
    Detalle_ VARCHAR(100))
RETURNS (
    Centro     CHAR(5),
    Bodega     CHAR(5),
    Referencia CHAR(20),
    Entrada    NUMERIC(17,4),
    Salida     NUMERIC(17,4),
    Unitario   NUMERIC(17,4))
AS
-- Centro,Bodega, Referencia, Entrada, Salida, Unitario
DECLARE VARIABLE Detalle VARCHAR(100);
DECLARE VARIABLE Det     VARCHAR(30);
DECLARE VARIABLE Coma    INTEGER;
BEGIN
  Detalle = Detalle_;
  Centro = 'NA';
  Bodega = 'NA';
  Referencia = 'NA';
  Entrada = 0;
  Salida = 0;
  Unitario = 0;

  -- Centro
  Coma = POSITION(',', Detalle);
  IF (Coma > 1) THEN
  BEGIN
    Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
    Detalle = SUBSTRING(Detalle FROM Coma + 1);
    Centro = LEFT(Det, 5);

    -- Bodega
    Coma = POSITION(',', Detalle);
    IF (Coma > 1) THEN
    BEGIN
      Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
      Detalle = SUBSTRING(Detalle FROM Coma + 1);
      Bodega = LEFT(Det, 5);

      -- Referencia
      Coma = POSITION(',', Detalle);
      IF (Coma > 1) THEN
      BEGIN
        Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
        Detalle = SUBSTRING(Detalle FROM Coma + 1);
        Referencia = LEFT(Det, 20);

        -- Entrada
        Coma = POSITION(',', Detalle);
        IF (Coma > 1) THEN
        BEGIN
          Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
          Detalle = SUBSTRING(Detalle FROM Coma + 1);
          Entrada = LEFT(Det, 20);

          -- Salida
          Coma = POSITION(',', Detalle);
          IF (Coma > 1) THEN
          BEGIN
            Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
            Detalle = SUBSTRING(Detalle FROM Coma + 1);
            Salida = LEFT(Det, 20);

            -- Unitario
            Coma = POSITION(',', Detalle);
            IF (Coma > 1) THEN
            BEGIN
              Det = SUBSTRING(Detalle FROM 1 FOR Coma - 1);
              Detalle = SUBSTRING(Detalle FROM Coma + 1);
              Unitario = LEFT(Det, 20);

            END
            ELSE
              Unitario = LEFT(Detalle, 20);
          END
          ELSE
            Salida = LEFT(Detalle, 20);
        END
        ELSE
          Entrada = LEFT(Detalle, 20);
      END
      ELSE
        Referencia = LEFT(Detalle, 20);
    END
    ELSE
      Bodega = LEFT(Detalle, 5);
  END
  ELSE
    Centro = LEFT(Detalle, 5);
  SUSPEND;
END^

SET TERM ; ^

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_LEE_DETALLE TO SYSDBA;
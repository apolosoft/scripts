
/************************************************************/
/**** MODULO GESTION                                     ****/
/************************************************************/

/* EXPORTADOS GESTION INICIO*/

/* EXPORTADOS GESTION */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVA';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--967 Gestion 28/Ago/2024 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            <noov:CustomExtensionContent>
            ges:comprador
            ges:caja_venta
            </noov:CustomExtensionContent>
            ges:sector_salud
            ges:tasa_cambio
            ges:espera_pdf
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 967 28/Ago/2024 v1

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo: , :Prefijo: , :Numero: )
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo: , :Prefijo: , :Numero: , :Emisor: , :Receptor: , :Clase_Fe: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo: , :Numero: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura


WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)    
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (272, 'ges:transporte1', 'DOC', '-- Transporte 1
 SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr"
        FROM Fe_Vector_Transporte(:Tipo:, :Prefijo:, :Numero:, ''GESTION'')
        WHERE Renglon = :Renglon:
    AND  Natr IN (''01'', ''02'')', 'S', 32, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (273, 'ges:transporte2', 'DOC', '-- Transporte 2
 
SELECT   Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               Uatr "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
FROM Pz_Fe_Transporte(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
WHERE  Natr =''03''', 'S', 33, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (274, 'ges:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:)', 'S', 36, 'NOOVA', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuarioSalud', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (275, 'ges:salud_ben', 'DOC', '-- Sector Salud beneficiario

SELECT Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Nombre "noov:Nvsal_nomb",
       Apellido "noov:Nvsal_apel",
       Ciudad "noov:Nvsal_cciu",
       Direccion "noov:Nvsal_dire",
       Pais "noov:Nvsal_pais",
       ''Registraduria Nacional'' "noov:Nvent_nomb"
FROM Pz_Fe_Vector_Salud_Ben(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Renglon:)
WHERE Tdoc <> ''NA''', 'S', 37, 'NOOVA', 'FACTURA', 'noov:DTOBeneficiario', '', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (276, 'ges:salud_ref', 'DOC', '-- Sector Salud Referenciados

SELECT Nume "noov:Nvdoc_nume",
       Cufd "noov:Nvdoc_cufd",
       Algr "noov:Nvdoc_algr",
       Femi "noov:Nvdoc_femi",
       Idto "noov:Nvdoc_idto",
       Ndoc "noov:Nvusu_ndoc",
       Oper "noov:Nvfor_oper",
       Reps "noov:Nvsal_cpsa",
       '''' "noov:Nvsal_naut"
FROM Pz_Fe_Vector_Salud_Ref(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:)', 'S', 38, 'NOOVA', 'FACTURA', 'noov:lDocumentoReferencia', 'noov:DTODocumentoReferencia', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon:

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo:, :Prefijo:, :Numero:)
WHERE Renglon = :Renglon:', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                          FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                              FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                              WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                            FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
WHERE Nvfor_oper = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       IIF(TRIM(Nvfor_Oper) = ''SS-CUFE'', ''20'', Nvfor_Oper) "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"

FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(COALESCE(Com_Total, 0), Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                 FROM Redondeo_Dian(COALESCE(Nvfac_Stot, 0), Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                                           FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                                               FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                                               WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                                             FROM Redondeo_Dian(COALESCE(Nvfac_Desc, 0), Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste A Ds

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Zipc "noov:Nvpro_zipc",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1561, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Pos

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       ''N'' "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'POS ELECTRONICO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1562, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'POS ELECTRONICO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1563, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'POS ELECTRONICO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1564, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'POS ELECTRONICO', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1565, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'POS ELECTRONICO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1566, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'POS ELECTRONICO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1567, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'POS ELECTRONICO', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1568, 'ges:comprador', 'DOC', '-- Beneficios Comprador

SELECT Codigo "noov:Codigo",
       Nombresapellidos "noov:NombresApellidos",
       Cant_Puntos "noov:Puntos"
FROM Pz_Fe_Benef_Comprador(:Tipo:, :Prefijo:, :Numero:)', 'S', 40, 'NOOVA', 'POS ELECTRONICO', 'noov:InformacionBeneficiosComprador', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1569, 'ges:caja_venta', 'DOC', '-- Informacion Caja Venta

SELECT Placa "noov:PlacaCaja",
       Ubicacion "noov:UbicacionCaja",
       Cajero "noov:Cajero",
       Tipo_Caja "noov:TipoCaja",
       Codigo_Venta "noov:CodigoVenta",
       (SELECT Valor
        FROM Redondeo_Dian(Subtotal, 2)) "noov:Subtotal"
FROM Pz_Fe_Datos_Caja(:Tipo:, :Prefijo:, :Numero:)
WHERE TRIM(Codigo_Fe) = ''POS ELECTRONICO''', 'S', 45, 'NOOVA', 'POS ELECTRONICO', 'noov:InformacionCajaVenta', NULL, NULL, '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1570, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 1  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'POS ELECTRONICO', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1571, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'FACTURA', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1572, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'EXPORTACION', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1573, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'CONTINGENCIA', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1574, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA DEBITO', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1575, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA CREDITO', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1576, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1577, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1578, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito POS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT ''E'' "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       ''3'' "noov:Nvres_nume",
       ''EC'' "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       ''23'' "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       ''N'' "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"

FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1579, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO POS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1580, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))
SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO POS', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1581, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO POS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1582, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO POS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1583, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1584, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1586, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 1  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA CREDITO POS', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1587, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1588, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1589, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1590, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1591, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1592, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1593, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1594, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1595, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1596, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1597, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1598, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1599, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1600, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1601, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1602, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Totales_Comprobante (
    Tipo_    VARCHAR(5),
    Prefijo_ VARCHAR(5),
    Numero_  VARCHAR(10))
RETURNS (
    Copago         DOUBLE PRECISION,
    Descuento      DOUBLE PRECISION,
    Imp_Nominal    DOUBLE PRECISION,
    Imp_Porcentual DOUBLE PRECISION,
    Impuestos      DOUBLE PRECISION,
    Retenciones    DOUBLE PRECISION,
    Bruto          DOUBLE PRECISION,
    Total          DOUBLE PRECISION,
    Total_Fe       DOUBLE PRECISION)
AS
BEGIN
  -- Impuestos Porcentual
  SELECT SUM(Debito + Credito)
  FROM Reg_Impuestos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Es_Tarifa = 'S'
  INTO Imp_Porcentual;
  Imp_Porcentual = COALESCE(:Imp_Porcentual, 0);

  -- Impuestos Nominal
  SELECT SUM(Debito + Credito)
  FROM Reg_Impuestos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Es_Tarifa = 'N'
  INTO Imp_Nominal;
  Imp_Nominal = COALESCE(:Imp_Nominal, 0);

  Impuestos = :Imp_Nominal + :Imp_Porcentual;

  -- Retenciones
  SELECT SUM(Debito + Credito)
  FROM Reg_Retenciones
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Retenciones;
  Retenciones = COALESCE(Retenciones, 0);

  -- Detalle
  SELECT SUM(Bruto * (Entrada + Salida)),
         SUM(Descuento * (Entrada + Salida))
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Bruto,
       Descuento;

  -- Comprobante
  SELECT Copago
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Copago;

  Total_Fe = :Bruto - :Descuento + :Impuestos - :Copago;
  Total = Total_Fe - :Retenciones;

  SUSPEND;
END
^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Comprobantes (
    Tipo_     VARCHAR(5),
    Prefijo_  VARCHAR(5),
    Numero_   VARCHAR(10),
    Emisor_   VARCHAR(15),
    Receptor_ VARCHAR(15),
    Clase_Fe_ VARCHAR(20))
RETURNS (
    Nvfac_Orig CHAR(1),
    Nvemp_Nnit VARCHAR(15),
    Nvres_Nume VARCHAR(20),
    Nvfac_Tipo VARCHAR(5),
    Nvfac_Tcru CHAR(1),
    Nvres_Pref VARCHAR(5),
    Nvfac_Nume VARCHAR(15),
    Nvfac_Fech VARCHAR(20),
    Nvfac_Cdet INTEGER,
    Nvfac_Venc VARCHAR(20),
    Nvsuc_Codi CHAR(1),
    Nvmon_Codi VARCHAR(5),
    Nvfor_Codi VARCHAR(2),
    Nvven_Nomb VARCHAR(84),
    Nvfac_Fpag VARCHAR(5),
    Nvfac_Conv CHAR(1),
    Nvcli_Cper CHAR(1),
    Nvcli_Cdoc VARCHAR(5),
    Nvcli_Docu VARCHAR(20),
    Nvcli_Pais VARCHAR(5),
    Nvcli_Depa VARCHAR(80),
    Nvcli_Ciud VARCHAR(100),
    Nvcli_Loca VARCHAR(10),
    Nvcli_Dire VARCHAR(80),
    Nvcli_Ntel VARCHAR(200),
    Nvcli_Regi VARCHAR(5),
    Nvcli_Fisc VARCHAR(30),
    Nvcli_Nomb VARCHAR(80),
    Nvcli_Pnom VARCHAR(20),
    Nvcli_Snom VARCHAR(20),
    Nvcli_Apel VARCHAR(40),
    Nvcli_Mail VARCHAR(300),
    Nvema_Copi VARCHAR(50),
    Nvfac_Obse VARCHAR(4000),
    Nvfac_Orde VARCHAR(40),
    Nvfac_Remi VARCHAR(40),
    Nvfac_Rece VARCHAR(40),
    Nvfac_Entr VARCHAR(40),
    Nvfac_Ccos VARCHAR(40),
    Nvfac_Stot DOUBLE PRECISION,
    Nvfac_Desc DOUBLE PRECISION,
    Nvfac_Anti DOUBLE PRECISION,
    Nvfac_Carg DOUBLE PRECISION,
    Nvfac_Tota DOUBLE PRECISION,
    Nvfac_Totp DOUBLE PRECISION,
    Nvfac_Roun NUMERIC(12,2),
    Nvfac_Vcop DOUBLE PRECISION,
    Nvfac_Timp DOUBLE PRECISION,
    Nvfac_Coid CHAR(1),
    Nvfac_Obsb CHAR(1),
    Nvcli_Ncon VARCHAR(80),
    Nvcon_Codi VARCHAR(2),
    Nvcon_Desc VARCHAR(200),
    Nvfac_Numb VARCHAR(20),
    Nvfac_Fecb VARCHAR(20),
    Nvfor_Oper VARCHAR(20),
    Nvpro_Cper CHAR(1),
    Nvpro_Cdoc VARCHAR(5),
    Nvpro_Docu VARCHAR(15),
    Nvpro_Dive VARCHAR(1),
    Nvpro_Pais VARCHAR(5),
    Nvpro_Depa VARCHAR(80),
    Nvpro_Ciud VARCHAR(100),
    Nvpro_Zipc VARCHAR(6),
    Nvpro_Loca VARCHAR(10),
    Nvpro_Dire VARCHAR(80),
    Nvpro_Ntel VARCHAR(200),
    Nvpro_Regi VARCHAR(5),
    Nvpro_Fisc VARCHAR(30),
    Nvpro_Nomb VARCHAR(80),
    Nvpro_Pnom VARCHAR(20),
    Nvpro_Snom VARCHAR(20),
    Nvpro_Apel VARCHAR(40),
    Nvpro_Mail VARCHAR(300),
    Nvpro_Ncon VARCHAR(80),
    V_Docu_Ref VARCHAR(20),
    Com_Total  DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu             VARCHAR(20);
DECLARE VARIABLE V_Caiu             INTEGER;
DECLARE VARIABLE V_Dsi              INTEGER;
DECLARE VARIABLE V_Mand             INTEGER;
DECLARE VARIABLE V_Cod_Vendedor     VARCHAR(15);
DECLARE VARIABLE V_Tesoreria        VARCHAR(10);
DECLARE VARIABLE V_Formapago        VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe     VARCHAR(5);
DECLARE VARIABLE V_Codbanco         VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe      VARCHAR(5);
DECLARE VARIABLE V_Naturaleza       CHAR(1);
DECLARE VARIABLE V_Codsociedad      VARCHAR(5);
DECLARE VARIABLE V_Email_Ter        VARCHAR(80);
DECLARE VARIABLE V_Codsucursal      VARCHAR(15);
DECLARE VARIABLE V_Email_Suc        VARCHAR(80);
DECLARE VARIABLE V_Email_Otro       VARCHAR(100);
DECLARE VARIABLE V_Nota             VARCHAR(80);
DECLARE VARIABLE V_Meganota         VARCHAR(4000);
DECLARE VARIABLE V_Copag            INTEGER;
DECLARE VARIABLE V_Copago           DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos        DOUBLE PRECISION;
DECLARE VARIABLE V_Pais             VARCHAR(5);
DECLARE VARIABLE V_Municipio        VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref         VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref      VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref       VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref        VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac        VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref         VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe        CHAR(1);
DECLARE VARIABLE V_Concepto_Nc      VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc  VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo       VARCHAR(5);
DECLARE VARIABLE V_Stot_Red         DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot       DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det    DOUBLE PRECISION;
DECLARE VARIABLE V_Siif             VARCHAR(80);
DECLARE VARIABLE V_Fecre            INTEGER;
DECLARE VARIABLE V_Dscor            VARCHAR(50);
DECLARE VARIABLE V_Postal_Code      VARCHAR(6);
DECLARE VARIABLE V_Transporte       INTEGER;
DECLARE VARIABLE V_Impuestos_Red    DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Totp       DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Mekano     DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Stot       DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Imp        DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Desc       DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Mek_Xml      DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Xml          DOUBLE PRECISION;
DECLARE VARIABLE V_Oper_Salud       VARCHAR(20);
DECLARE VARIABLE V_Fecha            DATE;
DECLARE VARIABLE V_Vence            DATE;
DECLARE VARIABLE V_Suc_Nombre       VARCHAR(80);
DECLARE VARIABLE V_Suc_Direccion    VARCHAR(80);
DECLARE VARIABLE V_Suc_Telefono     VARCHAR(200);
DECLARE VARIABLE V_Suc_Codmunicipio CHAR(5);
BEGIN
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO Nvemp_Nnit;

  IF (Clase_Fe_ IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS', 'NOTA CREDITO POS')) THEN
  BEGIN

    -- Si es NC o ND se toman datos del doc referencia
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo_Ref,
         V_Prefijo_Ref,
         V_Numero_Ref;

    V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
    V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
    V_Numero_Ref = COALESCE(V_Numero_Ref, '');

    SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
    FROM Comprobantes
    WHERE Tipo = :V_Tipo_Ref
          AND Prefijo = :V_Prefijo_Ref
          AND Numero = :V_Numero_Ref
    INTO V_Fecha_Ref;
    -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

    SELECT Cufe,
           EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
    FROM Facturas
    WHERE Tipo = :V_Tipo_Ref
          AND Prefijo = :V_Prefijo_Ref
          AND Numero = :V_Numero_Ref
    INTO V_Cufe_Ref,
         V_Fecha_Fac;
    V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
    V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

    SELECT Codigo_Fe
    FROM Documentos
    WHERE Codigo = :V_Tipo_Ref
    INTO V_Docu_Ref;
    V_Docu_Ref = COALESCE(V_Docu_Ref, '');
  END

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto,
         Fecha,
         Vence
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       Nvfac_Fech,
       Nvfac_Venc,
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc,
       V_Fecha,
       V_Vence;
  V_Copago = COALESCE(V_Copago, 0);

  -- Transportes
  V_Transporte = 0;
  SELECT COUNT(1)
  FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
  INTO V_Transporte;

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe,
         Tipo_Operacion
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe,
       V_Oper_Salud;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO Nvres_Nume;
  Nvres_Nume = COALESCE(Nvres_Nume, '0');

  Nvres_Nume = CASE TRIM(V_Docu)
                 WHEN 'NOTA DEBITO' THEN '1'
                 WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                 ELSE Nvres_Nume
               END;

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO Nvfac_Cdet;

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO Nvcli_Ncon;

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  Nvcli_Ncon = COALESCE(Nvcli_Ncon, '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'N');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO Nvven_Nomb;

  Nvven_Nomb = COALESCE(Nvven_Nomb, '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       Nvcli_Cdoc,
       Nvcli_Docu,
       Nvcli_Dire,
       Nvcli_Ntel,
       V_Codsociedad,
       Nvcli_Nomb,
       Nvcli_Pnom,
       Nvcli_Snom,
       Nvcli_Apel,
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO Nvcli_Regi,
       Nvcli_Fisc;

  --Sucursales
  SELECT COALESCE(Email, ''),
         Nombre,
         Direccion,
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codmunicipio
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO :V_Email_Suc,
       :V_Suc_Nombre,
       :V_Suc_Direccion,
       :V_Suc_Telefono,
       :V_Suc_Codmunicipio;

  Nvcli_Nomb = IIF(COALESCE(:V_Suc_Nombre, '') = '', Nvcli_Nomb, :V_Suc_Nombre);
  Nvcli_Dire = IIF(COALESCE(:V_Suc_Direccion, '') = '', Nvcli_Dire, :V_Suc_Direccion);
  Nvcli_Ntel = IIF(COALESCE(:V_Suc_Telefono, '') = '', Nvcli_Ntel, :V_Suc_Telefono);
  V_Municipio = IIF(COALESCE(:V_Suc_Codmunicipio, '') = '', V_Municipio, :V_Suc_Codmunicipio);

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Nvcli_Pais;

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO Nvcli_Depa;

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:Nvcli_Pais) = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO Nvcli_Ciud;

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Nvfac_Orde,
       Nvfac_Remi,
       Nvfac_Rece,
       Nvfac_Entr,
       Nvfac_Ccos;

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA CREDITO POS') THEN
    SELECT Nombre
    FROM Notas_Eq
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  Nvfac_Orde = COALESCE(Nvfac_Orde, '');
  Nvfac_Remi = COALESCE(Nvfac_Remi, '');
  Nvfac_Rece = COALESCE(Nvfac_Rece, '');
  Nvfac_Entr = COALESCE(Nvfac_Entr, '');
  Nvfac_Ccos = COALESCE(Nvfac_Ccos, '');

  Nvfac_Orig = 'E';
  --  Nvemp_nnit = :Emisor_;
  Nvfac_Tipo = CASE TRIM(V_Docu)
                 WHEN 'FACTURA' THEN 'FV'
                 WHEN 'CONTINGENCIA' THEN 'FC'
                 WHEN 'EXPORTACION' THEN 'FE'
                 WHEN 'POS ELECTRONICO' THEN 'EQ'
                 WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                            WHEN 'EXPORTACION' THEN 'CE'
                                            WHEN 'CONTINGENCIA' THEN 'CC'
                                            ELSE 'CV'
                                          END
                 WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                           WHEN 'EXPORTACION' THEN 'DE'
                                           WHEN 'CONTINGENCIA' THEN 'DC'
                                           ELSE 'DV'
                                         END
                 WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                 WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

               END;

  Nvres_Pref = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    Nvres_Pref = '';
  Nvfac_Nume = TRIM(Nvres_Pref) || TRIM(Numero_);

  Nvsuc_Codi = '1';
  Nvmon_Codi = 'COP';
  Nvfor_Codi = CASE TRIM(V_Docu)
                 WHEN 'FACTURA' THEN CASE
                                       WHEN V_Caiu <> 0 THEN '11'
                                       WHEN V_Transporte <> 0 THEN '18'
                                       WHEN V_Mand <> 0 THEN '12'
                                       ELSE '1'
                                     END
                 WHEN 'CONTINGENCIA' THEN CASE
                                            WHEN V_Caiu <> 0 THEN '15'
                                            WHEN V_Mand <> 0 THEN '16'
                                            ELSE '7'
                                          END
                 WHEN 'EXPORTACION' THEN IIF(Nvcli_Pais = 'CO', '10', '4')
                 WHEN 'POS ELECTRONICO' THEN '22'
                 WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                            WHEN 'EXPORTACION' THEN '5'
                                            WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                            WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                            ELSE '2'
                                          END
                 WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                           WHEN 'EXPORTACION' THEN '6'
                                           WHEN 'FACTURA' THEN '3'
                                           ELSE '3'
                                         END
                 WHEN 'DOCUMENTO SOPORTE' THEN '20'
                 WHEN 'NOTA DE AJUSTE A DS' THEN '21'
               END;

  Nvfac_Fpag = CASE
                 WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                 WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                 ELSE 'ZZZ'
               END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      Nvfac_Conv = '2';
    ELSE
      Nvfac_Conv = '1';
  END
  ELSE
  BEGIN
    IF (Nvfac_Fech <> Nvfac_Venc) THEN
      Nvfac_Conv = '2';
    ELSE
      Nvfac_Conv = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    Nvcli_Cper = '2';
  ELSE
    Nvcli_Cper = '1';

  Nvcli_Loca = '';
  Nvcli_Mail = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  Nvema_Copi = '';

  Nvfac_Obse = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_pdes", 0)),
         SUM("noov:Nvfac_stot"),
         SUM(Stot_Red)
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det,
       V_Nvfac_Stot,
       V_Stot_Red;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  Nvfac_Stot = V_Stot_Red;

  Nvfac_Desc = 0;
  Nvfac_Anti = 0;

  IF (V_Copag = 0) THEN
    Nvfac_Desc = V_Copago;
  ELSE
    Nvfac_Anti = V_Copago;

  Nvfac_Desc = Nvfac_Desc + V_Descuento_Det;
  Nvfac_Carg = 0;
  Nvfac_Tota = Nvfac_Stot;

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo"),
         SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos,
       :V_Impuestos_Red;

  V_Impuestos = COALESCE(V_Impuestos, 0);
  V_Impuestos_Red = COALESCE(V_Impuestos_Red, 0);

  Nvfac_Totp = V_Stot_Red + V_Impuestos - Nvfac_Desc;

  Nvfor_Oper = '';
  IF (Nvfac_Anti > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      Nvfac_Desc = Nvfac_Anti;
      Nvfac_Anti = 0;
    END
    ELSE
      Nvfac_Totp = Nvfac_Totp - Nvfac_Anti;
    Nvfor_Oper = 'SS-CUFE';
  END

  -- Salud
  IF (TRIM(:V_Oper_Salud) <> 'NA') THEN
  BEGIN
    Nvfor_Oper = :V_Oper_Salud;
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    Nvfor_Oper = '20';

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Total_Fe)
  FROM Pz_Totales_Comprobante(:Tipo_, :Prefijo_, :Numero_)
  INTO Com_Total;

  -- redondeo
  SELECT Valor
  FROM Redondeo_Dian(:Nvfac_Totp, 2)
  INTO V_Valor_Totp;

  SELECT Valor
  FROM Redondeo_Dian(:Com_Total, 2)
  INTO V_Valor_Mekano;

  SELECT Valor
  FROM Redondeo_Dian(:V_Stot_Red, 2)
  INTO V_Valor_Stot;

  SELECT Valor
  FROM Redondeo_Dian(:V_Impuestos_Red, 2)
  INTO V_Valor_Imp;

  SELECT Valor
  FROM Redondeo_Dian(:Nvfac_Desc + :Nvfac_Anti, 2)
  INTO V_Valor_Desc;

  V_Dif_Mek_Xml = :V_Valor_Mekano - :V_Valor_Totp;

  V_Dif_Xml = :V_Valor_Totp - :V_Valor_Stot - :V_Valor_Imp + COALESCE(:V_Valor_Desc, 0);

  IF (:Clase_Fe_ IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
    Nvfac_Roun = V_Dif_Xml;
  ELSE
    Nvfac_Roun = V_Dif_Mek_Xml + V_Dif_Xml;

  IF (Nvfac_Roun > 5000 OR Nvfac_Roun < -5000) THEN
    Nvfac_Roun = 0;

  Nvfac_Roun = COALESCE(Nvfac_Roun, 0);

  Nvfac_Vcop = 0;
  Nvfac_Timp = 0;
  Nvfac_Coid = '';
  Nvfac_Numb = '';
  Nvfac_Obsb = '';
  Nvcon_Codi = '';
  Nvcon_Desc = '';
  Nvfac_Tcru = '';
  Nvfac_Numb = '';

  -- Consumidor Final
  IF (:Nvcli_Docu = '222222222222') THEN
    Nvcli_Mail = 'N';

  -- Vigencia
  IF (V_Docu IN ('FACTURA', 'POS ELECTRONICO', 'EXPORTACION','NOTA CREDITO','NOTA CREDITO POS')) THEN
  BEGIN
    V_Fecha = CURRENT_DATE;
    IF (:V_Vence < V_Fecha) THEN
    BEGIN
      V_Vence = CURRENT_DATE;
      Nvfac_Venc = EXTRACT(YEAR FROM V_Vence) || '-' || RIGHT(EXTRACT(MONTH FROM V_Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Vence) + 100, 2) || 'T00:00:00';
    END

    Nvfac_Fech = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2) || 'T00:00:00';
  END

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS', 'NOTA CREDITO POS')) THEN
  BEGIN
    Nvcon_Codi = V_Concepto_Nc;
    Nvcon_Desc = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      Nvfac_Tcru = 'L';
    ELSE
      Nvfac_Tcru = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      Nvfac_Numb = TRIM(V_Prefijo_Ref);
    Nvfac_Numb = TRIM(Nvfac_Numb) || TRIM(V_Numero_Ref);
    Nvfac_Fecb = TRIM(COALESCE(V_Fecha_Ref, Nvfac_Fech));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    Nvpro_Cper = Nvcli_Cper;
    Nvpro_Cdoc = Nvcli_Cdoc;
    Nvpro_Docu = TRIM(Receptor_);
    Nvpro_Dive = '';

    IF (Nvpro_Cdoc = '31') THEN
      Nvpro_Dive = RIGHT(Nvcli_Docu, 1);

    Nvpro_Pais = Nvcli_Pais;
    Nvpro_Depa = Nvcli_Depa;
    Nvpro_Ciud = Nvcli_Ciud;
    Nvpro_Zipc = V_Postal_Code;
    Nvpro_Loca = Nvcli_Loca;
    Nvpro_Dire = Nvcli_Dire;
    Nvpro_Ntel = Nvcli_Ntel;
    Nvpro_Regi = Nvcli_Regi;
    Nvpro_Fisc = Nvcli_Fisc;
    Nvpro_Nomb = Nvcli_Nomb;
    Nvpro_Pnom = Nvcli_Pnom;
    Nvpro_Snom = Nvcli_Snom;
    Nvpro_Apel = Nvcli_Apel;
    Nvpro_Mail = V_Dscor;
    Nvpro_Ncon = Nvcli_Ncon;
    Nvcli_Cper = '';
    Nvcli_Cdoc = '';
    Nvcli_Docu = '';
    Nvcli_Pais = '';
    Nvcli_Depa = '';
    Nvcli_Ciud = '';
    Nvcli_Loca = '';
    Nvcli_Dire = '';
    Nvcli_Ntel = '';
    Nvcli_Regi = '';
    Nvcli_Fisc = '';
    Nvcli_Nomb = '';
    Nvcli_Pnom = '';
    Nvcli_Snom = '';
    Nvcli_Apel = '';
    Nvcli_Mail = '';
    Nvcli_Ncon = '';
  END
  SUSPEND;
  /* 967 17.07.2024  CURRENT_DATE para facturas y pos
     967 23.07.2024  CURRENT_DATE para Exportacion 
     967 26.07.2024  Total Comprobante desde Tr_inventario y Consumidor final sin correo
     967 08.08.2024  Datos de Sucursal */
END
^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Detalle (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Renglon           INTEGER,
    Consecutivo       INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    "noov:Nvdet_tran" CHAR(1),
    Stot_Red          DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota             CHAR(200);
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario         DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Aiu        DOUBLE PRECISION;
DECLARE VARIABLE V_Linea            CHAR(5);
DECLARE VARIABLE V_Dsi              CHAR(1);
DECLARE VARIABLE V_Num_Linea        INTEGER;
DECLARE VARIABLE V_Empresa          VARCHAR(15);
DECLARE VARIABLE V_Renglon_1        INTEGER;
DECLARE VARIABLE V_Referencia_1     VARCHAR(20);
DECLARE VARIABLE V_Nom_Referencia_1 VARCHAR(198);
DECLARE VARIABLE V_Nota_1           VARCHAR(200);
DECLARE VARIABLE V_Medida_1         VARCHAR(5);
DECLARE VARIABLE V_Cantidad_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto_1          DOUBLE PRECISION;
DECLARE VARIABLE V_Unitario_1       DOUBLE PRECISION;
DECLARE VARIABLE V_Pdescuento_1     DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_1      DOUBLE PRECISION;
DECLARE VARIABLE V_Iva              DOUBLE PRECISION;
DECLARE VARIABLE V_Transporte       INTEGER;
DECLARE VARIABLE V_Tipo_Linea       CHAR(20);
DECLARE VARIABLE V_Alterna          DOUBLE PRECISION;
BEGIN
  V_Num_Linea = 0;

  FOR SELECT Renglon,
             Referencia,
             Nom_Referencia,
             Nota,
             Medida,
             Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Valor_Aiu,
             Stot_Red,
             Linea,
             Dsi
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      INTO Renglon,
           "noov:Nvpro_codi",
           "noov:Nvpro_nomb",
           V_Nota,
           "noov:Nvuni_desc",
           "noov:Nvfac_cant",
           V_Bruto,
           "noov:Nvfac_pdes",
           "noov:Nvfac_desc",
           V_Valor_Aiu,
           "noov:Nvfac_stot",
           V_Linea,
           V_Dsi
  DO
  BEGIN
    Consecutivo = 1;
    --impuestos
    "noov:Nvimp_cdia" = '00';
    "noov:Nvdet_piva" = 0;
    "noov:Nvdet_viva" = 0;

    IF (V_Linea = 'INC' AND
        V_Bruto > 0 AND
        V_Num_Linea > 0) THEN
      SELECT SKIP 1 "noov:Nvimp_cdia",
                    "noov:Nvimp_porc",
                    "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY Es_Tarifa DESC, "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    ELSE
      SELECT FIRST 1 "noov:Nvimp_cdia",
                     "noov:Nvimp_porc",
                     "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY Es_Tarifa DESC, "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    "noov:Nvimp_cdia" = COALESCE("noov:Nvimp_cdia", '00');
    "noov:Nvdet_piva" = COALESCE("noov:Nvdet_piva", 0);
    "noov:Nvdet_viva" = COALESCE("noov:Nvdet_viva", 0);

    "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
    "noov:Nvfac_valo" = V_Bruto;

    IF (V_Linea = 'INC' AND
        V_Bruto = 0) THEN
      "noov:Nvfac_valo" = "noov:Nvdet_viva" / "noov:Nvfac_cant";

    IF (V_Valor_Aiu > 0) THEN
      "noov:Nvimp_cdia" = '00';

    "noov:Nvdet_pref" = '';
    IF (V_Bruto = "noov:Nvfac_desc") THEN
      "noov:Nvdet_pref" = '01';

    IF (V_Valor_Aiu > 0) THEN
    BEGIN
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
    END
    "noov:Nvdet_tcod" = '';
    "noov:Nvpro_cean" = '';
    "noov:Nvdet_entr" = '';
    "noov:Nvuni_quan" = 0;

    IF (TRIM(V_Linea) IN ('AIU_A', 'AIU')) THEN
      "noov:Nvdet_nota" = 'Contrato de servicios AIU por concepto de: Servicios';
    ELSE
      "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

    "noov:Nvdet_padr" = 0;
    "noov:Nvdet_marc" = '';
    "noov:Nvdet_mode" = '';
    IF (V_Linea = 'INC' AND
        V_Bruto > 0) THEN
      V_Num_Linea = V_Num_Linea + 1;

    -- "noov:Nvfac_stot" = (V_Valo * "noov:Nvfac_cant") - V_Desc;

    IF ("noov:Nvfac_pdes" = 100) THEN
      "noov:Nvfac_desc" = "noov:Nvfac_stot";

    IF ("noov:Nvimp_cdia" = '22') THEN
      "noov:Nvfac_stot" = 0;

    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
    INTO Stot_Red;

    -- Transporte de carga
    SELECT COUNT(1)
    FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
    INTO V_Transporte;
    "noov:Nvdet_tran" = '';
    IF (V_Transporte > 1) THEN
    BEGIN
      SELECT Tipo
      FROM Lineas
      WHERE Codigo = :V_Linea
      INTO :V_Tipo_Linea;

      IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE SERVICIO') THEN
        "noov:Nvdet_tran" = '0';
      ELSE
      IF (TRIM(:V_Tipo_Linea) = 'TRANSPORTE REMESA') THEN
        "noov:Nvdet_tran" = '1';

      SELECT Otra_Salida
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
            AND Renglon = :Renglon
      INTO V_Alterna;
      V_Alterna = COALESCE(:V_Alterna, 0);
      IF (:V_Alterna > 0) THEN
        "noov:Nvuni_desc" = '94';
    END

    SUSPEND;
  END

END
^

SET TERM ; ^
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Transporte (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(10),
    Numero_  CHAR(10),
    Renglon_ INTEGER)
RETURNS (
    Atri VARCHAR(1),
    Natr VARCHAR(2),
    Vatr VARCHAR(20),
    Uatr VARCHAR(20),
    Catr VARCHAR(20))
AS
DECLARE VARIABLE V_Alterna NUMERIC(10,0);
BEGIN

  FOR SELECT Atri,
             Natr,
             Vatr,
             Uatr,
             Catr
      FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'GESTION')
      WHERE Renglon = :Renglon_
      INTO Atri,
           Natr,
           Vatr,
           Uatr,
           Catr
  DO
  BEGIN
    SELECT Otra_Salida
    FROM Tr_Inventario
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
          AND Renglon = :Renglon_
    INTO V_Alterna;
    V_Alterna = COALESCE(:V_Alterna, 0);
    IF (:V_Alterna > 0) THEN
      Catr = :V_Alterna;

    IF (TRIM(COALESCE(Uatr, '')) NOT IN ('KGM', 'GLL')) THEN
      Uatr = 'KGM';

    SUSPEND;
  END
END
^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Benef_Comprador (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Codigo           CHAR(15),
    Nombresapellidos CHAR(250),
    Cant_Puntos      INTEGER)
AS
DECLARE VARIABLE V_Codigo_Fe  CHAR(20);
DECLARE VARIABLE V_Referencia CHAR(20);
DECLARE VARIABLE V_Linea      CHAR(5);
DECLARE VARIABLE V_Total_Ref  DOUBLE PRECISION;
DECLARE VARIABLE V_Puntos     DOUBLE PRECISION;
BEGIN
  -- VALIDA SI ES EQ
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Codigo_Fe;

  IF (TRIM(V_Codigo_Fe) <> 'POS ELECTRONICO') THEN
  BEGIN
    EXIT;
  END

  -- Comprador
  SELECT Codtercero
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Codigo;

  SELECT Nombre
  FROM Terceros
  WHERE Codigo = :Codigo
  INTO :Nombresapellidos;

  -- Puntos
  Cant_Puntos = 0;
  FOR SELECT Codreferencia,
             Salida * (Bruto - Descuento)
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO V_Referencia,
           V_Total_Ref
  DO
  BEGIN
    SELECT Codlinea
    FROM Referencias
    WHERE Codigo = :V_Referencia
    INTO V_Linea;

    SELECT COALESCE(Valor_Punto, 0)
    FROM Lineas
    WHERE Codigo = :V_Linea
    INTO V_Puntos;
    IF (:V_Puntos > 0) THEN
      Cant_Puntos = Cant_Puntos + (:V_Total_Ref / :V_Puntos);
  END

  SUSPEND;
END
^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Redondeado (
    Tercero_ CHAR(15))
RETURNS (
    Redondeo INTEGER)
AS
DECLARE VARIABLE V_Empresa VARCHAR(15);
BEGIN
  Redondeo = 0;
  -- Datos empresa
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  SELECT COUNT(1)
  FROM Contactos
  WHERE Codcargo = 'XMLRE'
        AND Codtercero = :V_Empresa
  INTO Redondeo;

  IF (:Redondeo = 0) THEN
    SELECT COUNT(1)
    FROM Contactos
    WHERE Codcargo = 'XMLRE'
          AND Codtercero = :Tercero_
    INTO Redondeo;

  IF (Redondeo = 0) THEN
    Redondeo = 2;
  ELSE
    Redondeo = 0;

  SUSPEND;
END
^

SET TERM ; ^


COMMIT WORK;



/* EXPORTADOS GESTION FIN */


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('INVG111002', 'DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO PARCIAL', 'Comparar saldos del sistema con el conteo fisico del inventario solo para las referencias digitadas en el documento SF1 con el que se cargo el inventario fisico.', 'SELECT Fecha_Corte, Referencia, Nombre_Referencia, Bodega, Categoria, Lote,
       Linea, Nombre_Linea, Ubicacion, Saldo, Ponderado, Valor, Conteo,
       Valor_Conteo, Diferencia
FROM Pz_Sel_Conteo_Fisico_P(:A_Tipo, :B_Prefijo, :C_Numero)', 'S', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;


/* TRANSPORTE */

-- Contable
SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_TRANSPORTE (
    TIPO CHAR(5),
    PREFIJO CHAR(10),
    NUMERO CHAR(10))
RETURNS (
    RENGLON INTEGER,
    "noov:Nvfac_atri" VARCHAR(1),
    "noov:Nvfac_natr" VARCHAR(2),
    "noov:Nvfac_vatr" VARCHAR(20),
    "noov:Nvfac_uatr" VARCHAR(20),
    "noov:Nvfac_catr" VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^


SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_TRANSPORTE (
    TIPO CHAR(5),
    PREFIJO CHAR(10),
    NUMERO CHAR(10))
RETURNS (
    RENGLON INTEGER,
    "noov:Nvfac_atri" VARCHAR(1),
    "noov:Nvfac_natr" VARCHAR(2),
    "noov:Nvfac_vatr" VARCHAR(20),
    "noov:Nvfac_uatr" VARCHAR(20),
    "noov:Nvfac_catr" VARCHAR(20))
AS
DECLARE VARIABLE V_Medida   CHAR(5);
DECLARE VARIABLE V_Concepto CHAR(20);
BEGIN

  FOR SELECT Codconcepto,
             Renglon
      FROM Pz_Mov_Patrones(:Tipo, :Prefijo, :Numero)
      INTO V_Concepto,
           Renglon
  DO
  BEGIN
    -- MEDIDA
    SELECT Codmedida
    FROM Auto_Conceptos
    WHERE Codigo = :V_Concepto
    INTO V_Medida;

    FOR SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               COALESCE(Uatr, :V_Medida) "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
        FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero, 'CONTABLE')
        WHERE Renglon = :Renglon

        INTO "noov:Nvfac_atri",
             "noov:Nvfac_natr",
             "noov:Nvfac_vatr",
             "noov:Nvfac_uatr",
             "noov:Nvfac_catr"
    DO
    BEGIN
      SUSPEND;
    END
  END
END^

SET TERM ; ^



/**** MODULO GESTION FINAL                               ****/


/************************************************************/
/**** MODULO CONTABLE                                    ****/
/************************************************************/

/* EXPORTADOS CONTABLE INICIO*/

/* EXPORTADOS CONTABLE  */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVAC';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--967 Contable 27/Ago/2024 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            con:comprobante
            <noov:lDetalle>
                con:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
                con:impuestos
            </noov:lImpuestos>
            con:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVAC');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (461, 'con:comprobante', 'DOC', '-- Comprobante Contable Factura 967 27/Ago/2024 v1

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM((SELECT Valor
                                                 FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)))
                                     FROM Pz_Fe_Auto_Imp(:Tipo: , :Prefijo: , :Numero: )
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo: , :Prefijo: , :Numero: , :Emisor: , :Receptor: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (462, 'con:detalle', 'DOC', '-- Detalle Contable Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (463, 'con:impuestos', 'DOC', '-- Impuestos Contable Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (464, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE FACTURA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (465, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Factura

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 25, 'NOOVAC', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (466, 'con:transporte1', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" IN (''01'', ''02'')', 'S', 26, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (467, 'con:transporte2', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr",
       "noov:Nvfac_uatr",
       "noov:Nvfac_catr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" =''03''', 'S', 27, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (481, 'con:comprobante', 'DOC', '-- Comprobante Contable Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (482, 'con:detalle', 'DOC', '-- Detalle Contable Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (483, 'con:impuestos', 'DOC', '--Impuestos CONTABLE EXPORTACION

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (484, 'con:tasa_cambio', 'DOC', '--Tasa Cambio CONTABLE EXPORTACION

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (501, 'con:comprobante', 'DOC', '--Comprobante CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (502, 'con:detalle', 'DOC', '--Detalle CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (503, 'con:impuestos', 'DOC', '--Impuestos CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (504, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE CONTINGENCIA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (521, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian(("noov:Nvfac_desc" + (SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
                                                          FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:))), Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (522, 'con:detalle', 'DOC', '-- Detalle Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (523, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (524, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Debito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
                Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (525, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA DEBITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (541, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (542, 'con:detalle', 'DOC', '-- Detalle Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))
SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (543, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (544, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Credito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (545, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA CREDITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (561, 'con:comprobante', 'DOC', '-- Comprobante Contable Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (562, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 40, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:Proveedor', NULL, '', 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (565, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Documento Soporte

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (581, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (582, 'con:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero, :Emisor:, :Receptor:)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (583, 'con:detalle', 'DOC', '-- Detalle Contable Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (584, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota de Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (585, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Nota de Ajuste a DS

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Auto_Comp (
    Tipo_     CHAR(5),
    Prefijo_  CHAR(5),
    Numero_   CHAR(10),
    Emisor_   VARCHAR(15),
    Receptor_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(10),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80))
AS
DECLARE VARIABLE V_Id              INTEGER;
DECLARE VARIABLE V_Plazo           INTEGER;
DECLARE VARIABLE V_Codidentidad    CHAR(5);
DECLARE VARIABLE V_Fecha           CHAR(19);
DECLARE VARIABLE V_Vence           CHAR(19);
DECLARE VARIABLE V_Aiu             INTEGER;
DECLARE VARIABLE V_Manda           INTEGER;
DECLARE VARIABLE V_Transporte      INTEGER;
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Dv              CHAR(1);
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Pais_Fe         VARCHAR(5);
DECLARE VARIABLE V_Muni            VARCHAR(5);
DECLARE VARIABLE V_Nombre_Depa     VARCHAR(80);
DECLARE VARIABLE V_Nombre_Ciud     VARCHAR(80);
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Direccion       VARCHAR(80);
DECLARE VARIABLE V_Telefono        VARCHAR(100);
DECLARE VARIABLE V_Contc           VARCHAR(80);
DECLARE VARIABLE V_Sociedad        VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Fe     VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Nom    VARCHAR(80);
DECLARE VARIABLE V_Autoretenedor   CHAR(1);
DECLARE VARIABLE V_Jerarquia       INTEGER;
DECLARE VARIABLE V_Empresa         VARCHAR(80);
DECLARE VARIABLE V_Nom1            VARCHAR(20);
DECLARE VARIABLE V_Nom2            VARCHAR(20);
DECLARE VARIABLE V_Apl             VARCHAR(40);
DECLARE VARIABLE V_Email           VARCHAR(50);
DECLARE VARIABLE V_Otro_Email      VARCHAR(200);
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Orden           VARCHAR(40);
DECLARE VARIABLE V_Remision        VARCHAR(40);
DECLARE VARIABLE V_Recepcion       VARCHAR(40);
DECLARE VARIABLE V_Ean             VARCHAR(40);
DECLARE VARIABLE V_Centro          VARCHAR(40);
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Tiporef         VARCHAR(5);
DECLARE VARIABLE V_Prefijoref      VARCHAR(5);
DECLARE VARIABLE V_Fecha_Ref       CHAR(19);
DECLARE VARIABLE V_Numeroref       VARCHAR(10);
DECLARE VARIABLE V_Docuref         VARCHAR(20);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     CHAR(1);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
DECLARE VARIABLE V_Fechac          DATE;
DECLARE VARIABLE V_Vencec          DATE;
BEGIN

  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO "noov:Nvemp_nnit";

  SELECT Vence
  FROM Val_Documentos
  WHERE Coddocumento = :Tipo_
        AND Codprefijo = :Prefijo_
  INTO V_Plazo;

  SELECT Id,
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Fecha + COALESCE(:V_Plazo, 0)) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || 'T' || SUBSTRING(Hora FROM 1 FOR 8),
         Fecha
  FROM Fe_Apo_Comprobante(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Id,
       V_Fecha,
       V_Vence,
       V_Fechac;

  V_Vencec = :V_FechaC + COALESCE(:V_Plazo, 0);

  --Terceros
  SELECT Codidentidad,
         Naturaleza,
         Dv,
         Codpais,
         Codmunicipio,
         Direccion,
         COALESCE(TRIM(Telefono), '') || ', ' || COALESCE(Movil, ''),
         Codsociedad,
         Empresa,
         Nom1,
         Nom2,
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         Email,
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Codidentidad,
       V_Naturaleza,
       V_Dv,
       V_Pais,
       V_Muni,
       V_Direccion,
       V_Telefono,
       V_Sociedad,
       V_Empresa,
       V_Nom1,
       V_Nom2,
       V_Apl,
       V_Email,
       V_Postal_Code;

  --Pais
  SELECT Codigo_Fe
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO V_Pais_Fe;

  --Departamento
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Muni FROM 1 FOR 2)
  INTO V_Nombre_Depa;

  --Municipio
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = :V_Muni
  INTO V_Nombre_Ciud;

  --AIU
  SELECT COUNT(*)
  FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_) P
  WHERE (TRIM("noov:Nvpro_codi") STARTING WITH 'AIU')
        AND (P."noov:Nvfac_cant" * P."noov:Nvfac_valo") > 0
  INTO V_Aiu;

  --Mandatario
  SELECT COUNT(*)
  FROM Auto_Comp M
  JOIN Centros C ON (M.Codcentro = C.Codigo)
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND COALESCE(C.Codtercero, '') <> ''
  INTO V_Manda;

  -- Transportes
  SELECT COUNT(1)
  FROM Fe_Vector_Transporte(:Tipo_, :Prefijo_, :Numero_, 'CONTABLE')
  INTO V_Transporte;

  --Documentos
  SELECT Codigo_Fe,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Valida_Fe;

  --Documento ref
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Contable
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tiporef,
       V_Prefijoref,
       V_Numeroref;

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tiporef
  INTO V_Docuref;

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00'
  FROM Auto_Comp
  WHERE Tipo = :V_Tiporef
        AND Prefijo = :V_Prefijoref
        AND Numero = :V_Numeroref
  INTO V_Fecha_Ref;

  --Contacto
  SELECT FIRST 1 Nombre
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO V_Contc;
  V_Contc = COALESCE(V_Contc, '');

  --Otro email
  SELECT LIST(TRIM(Email), ';')
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Otro_Email;
  V_Contc = COALESCE(V_Contc, '');

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Sociedades
  SELECT Codigo_Fe,
         Nombre,
         Autoretenedor,
         Jerarquia
  FROM Sociedades
  WHERE Codigo = :V_Sociedad
  INTO V_Sociedad_Fe,
       V_Sociedad_Nom,
       V_Autoretenedor,
       V_Jerarquia;

  --Auto Comp
  SELECT Nota,
         Concepto
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Nota,
       V_Concepto_Nc;

  --Mega nota
  SELECT LIST(TRIM(C.Nota), IIF(RIGHT(TRIM(C.Nota), 1) = '.', ASCII_CHAR(13), ' '))
  FROM Auto_Movimiento C
  JOIN Auto_Comp C1 USING (Id)
  WHERE (C1.Tipo = :Tipo_
        AND C1.Prefijo = :Prefijo_
        AND C1.Numero = :Numero_)
        AND (C.Codconcepto = 'MEGANOTA')
        AND (TRIM(COALESCE(C.Nota, '')) <> '')
  INTO V_Meganota;

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Contabilidad
  WHERE Id = :V_Id
  INTO V_Orden,
       V_Remision,
       V_Recepcion,
       V_Ean,
       V_Centro;

  --Impuestos
  SELECT SUM((SELECT Valor
              FROM Redondeo_Dian("noov:Nvimp_valo", 2)))
  FROM Pz_Fe_Auto_Imp(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  -------
  "noov:Nvfac_orig" = 'E';
  --  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvres_nume" = (SELECT Numero
                       FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_));
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');
  "noov:Nvres_nume" = CASE TRIM(V_Docu)
                        WHEN 'NOTA DEBITO' THEN '1'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '2'
                        ELSE "noov:Nvres_nume"
                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);
  "noov:Nvfac_fech" = V_Fecha;
  "noov:Nvfac_venc" = V_Vence;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'
                      END;
  "noov:Nvfac_cdet" = (SELECT COUNT(1)
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_)
                       WHERE ("noov:Nvfac_cant" * "noov:Nvfac_valo") > 0);
  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = 'COP';
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Aiu <> 0 THEN '11'
                                              WHEN V_Manda <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Aiu <> 0 THEN '15'
                                                   WHEN V_Manda <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Aiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Aiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;
  "noov:Nvven_nomb" = '';
  "noov:Nvfac_fpag" = 'ZZZ';
  "noov:Nvfac_conv" = IIF(COALESCE(V_Plazo, 0) <> 0, '2', '1');
  "noov:Nvcli_cper" = IIF(V_Naturaleza = 'N', 2, 1);
  "noov:Nvcli_cdoc" = V_Codidentidad;
  "noov:Nvcli_docu" = TRIM(Receptor_) || IIF(TRIM(V_Codidentidad) = '31', IIF(V_Dv IS NULL, '', '-' || TRIM(V_Dv)), '');
  "noov:Nvcli_pais" = V_Pais_Fe;
  "noov:Nvcli_depa" = V_Nombre_Depa;
  "noov:Nvcli_ciud" = TRIM(V_Nombre_Ciud) || IIF(V_Docu = 'EXPORTACION', '', '@' || TRIM(V_Muni));
  "noov:Nvcli_loca" = '';
  "noov:Nvcli_dire" = V_Direccion;
  "noov:Nvcli_ntel" = V_Telefono;
  "noov:Nvcli_regi" = V_Sociedad_Fe;
  "noov:Nvcli_fisc" = IIF(UPPER(TRIM(V_Sociedad_Nom)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(V_Autoretenedor = 'S', 'O-15' || IIF(V_Jerarquia > 5, ';O-13', ''), IIF(V_Jerarquia > 5, 'O-13', 'R-99-PN')));
  "noov:Nvcli_nomb" = COALESCE(TRIM(V_Empresa), '');
  "noov:Nvcli_pnom" = COALESCE(TRIM(V_Nom1), '');
  "noov:Nvcli_snom" = COALESCE(TRIM(V_Nom2), '');
  "noov:Nvcli_apel" = V_Apl;
  "noov:Nvcli_mail" = TRIM(V_Email) || IIF(V_Otro_Email IS NULL OR TRIM(V_Otro_Email) = '', '', ';' || TRIM(V_Otro_Email));
  "noov:Nvema_copi" = '';
  "noov:Nvfac_obse" = SUBSTRING(TRIM(V_Nota) || IIF(TRIM(COALESCE(V_Meganota, '')) = '', '', ', ' || TRIM(V_Meganota)) FROM 1 FOR 4000);
  "noov:Nvfac_orde" = COALESCE(V_Orden, '');
  "noov:Nvfac_remi" = COALESCE(V_Remision, '');
  "noov:Nvfac_rece" = COALESCE(V_Recepcion, '');
  "noov:Nvfac_entr" = COALESCE(V_Ean, '');
  "noov:Nvfac_ccos" = COALESCE(V_Centro, '');
  "noov:Nvfac_stot" = (SELECT SUM("noov:Nvfac_stot")
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_));

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";
  "noov:Nvfac_totp" = "noov:Nvfac_stot" + V_Impuestos;
  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_obsb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvfac_tcru" = 'R';
    "noov:Nvfac_fecb" = V_Fecha_Ref;
    "noov:Nvcon_codi" = V_Concepto_Nc;
    -- Nombre concepto
    IF (V_Docu = 'NOTA CREDITO') THEN
      SELECT Nombre
      FROM Notas_Cre
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DEBITO') THEN
      SELECT Nombre
      FROM Notas_Deb
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
      SELECT Nombre
      FROM Notas_Ds
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    "noov:Nvfac_numb" = '';

    IF (TRIM(V_Prefijoref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijoref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numeroref);

    IF (V_Valida_Fe = 'N') THEN
    BEGIN
      "noov:Nvfac_tcru" = 'L';
      "noov:Nvfac_fecb" = V_Fecha;
      "noov:Nvfac_numb" = '';
    END
  END

  -- Vigencia
  IF (V_Docu IN ('FACTURA', 'POS ELECTRONICO', 'EXPORTACION','NOTA CREDITO','NOTA CREDITO POS')) THEN
  BEGIN
    V_Fechac = CURRENT_DATE;
    IF (:V_Vencec < V_Fechac) THEN
    BEGIN
      V_Vencec = CURRENT_DATE;
      "noov:Nvfac_venc" = EXTRACT(YEAR FROM V_Vencec) || '-' || RIGHT(EXTRACT(MONTH FROM V_Vencec) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Vencec) + 100, 2) || 'T00:00:00';
    END

    "noov:Nvfac_fech" = EXTRACT(YEAR FROM V_Fechac) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fechac) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fechac) + 100, 2) || 'T00:00:00';
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END

  SUSPEND;
END
^

SET TERM ; ^

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Redondeado (
    Tercero_ CHAR(15))
RETURNS (
    Redondeo INTEGER)
AS
DECLARE VARIABLE V_Empresa VARCHAR(15);
BEGIN
  Redondeo = 0;
  -- Datos empresa
  SELECT FIRST 1 Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  SELECT COUNT(1)
  FROM Contactos
  WHERE Codcargo = 'XMLRE'
        AND Codtercero = :V_Empresa
  INTO Redondeo;

  IF (:Redondeo = 0) THEN
    SELECT COUNT(1)
    FROM Contactos
    WHERE Codcargo = 'XMLRE'
          AND Codtercero = :Tercero_
    INTO Redondeo;

  IF (Redondeo = 0) THEN
    Redondeo = 2;
  ELSE
    Redondeo = 0;

  SUSPEND;
END
^

SET TERM ; ^

COMMIT WORK;


/* EXPORTADOS CONTABLE FIN*/

/* FECHA: 23ABR2024 */
/* CAMBIOS EXOGENA AG 2023 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_1004 INTEGER;
BEGIN
  V_1004 = 0;
  SELECT COUNT(1)
  FROM Mm_Formatos
  WHERE TRIM(Codigo) = '1004_V8'
  INTO V_1004;

  IF (V_1004 = 0) THEN
  BEGIN
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5086',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR RETEFUENTE TRASLADADA A TERCEROS POR PARTICIPACIONES O DIVIDENDOS RECIBIDOS DE SOC. NACIONALES','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5086',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR RETEFUENTE TRASLADADA A TERCEROS POR PARTICIPACIONES O DIVIDENDOS RECIBIDOS DE SOC. NACIONALES','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5087',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR PUNTOS PREMIO REDIMIDOS EN EL PERIODO QUE AFECTAN EL GASTO,PROCED. DE PROG. DE FIDELIZACION','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,TENDENCIA_CREDITO) VALUES ('1001_V10','5087',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'VALOR PUNTOS PREMIO REDIMIDOS EN EL PERIODO QUE AFECTAN EL GASTO,PROCED. DE PROG. DE FIDELIZACION','FINAL','UNO','100000','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1001_V10','5088',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'COSTOS O GASTOS POR DIFERENCIA EN CAMBIO.  SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','100000','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1001_V10','5088',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'COSTOS O GASTOS POR DIFERENCIA EN CAMBIO.  SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','100000','S','N' ) ;
    
    INSERT INTO MM_FORMATOS (CODIGO, NOMBRE, ENTINFO, CPT, TDOC, NID, DV, APL1, APL2, NOM1, NOM2, RAZ, DIR, DPTO, MUN, PAIS, EMAIL, VALOR1, VALOR2, VALOR3, VALOR4, VALOR5, VALOR6, VALOR7, VALOR8, VALOR9, VALOR0, VALOR10, VALOR11, VALOR12, VALOR13, VALOR14, VALOR15, VALOR16, VALOR17, VALOR18, VALOR19, VALOR20, VALOR21, VALOR22, VALOR23, VALOR24, VALOR25, VALOR26, TOTAL, IDEM, TDOCM, NITM, DVM, APL1M, APL2M, NOM1M, NOM2M, RAZM, INFORMAR, CON_INFORMADOS, NELEMENTO, NVALOR1, NVALOR2, NVALOR3, NVALOR4, NVALOR5, NVALOR6, NVALOR7, NVALOR8, NVALOR9, NVALOR0, NVALOR10, NVALOR11, NVALOR12, NVALOR13, NVALOR14, NVALOR15, NVALOR16, NVALOR17, NVALOR18, NVALOR19, NVALOR20, NVALOR21, NVALOR22, NVALOR23, NVALOR24, NVALOR25, NVALOR26, NIDEM, NCPT, REFERENCIA, REVISION, CADENA_ADICIONAL, EQUIVALENCIA, VALOR27, VALOR28, VALOR29, NVALOR27, NVALOR28, NVALOR29)
                 VALUES ('1004_V8', 'DESCUENTOS TRIBUTARIOS SOLICITADOS', NULL, 'S', 'S', 'S', 'N', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'descuentos', 'vdesc', 'vdescsol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cpt', 'DESCUENTOS TRIBUTARIOS SOLICITADOS', 8, NULL, 'nid=nit,nom1=pno,nom2=ono,apl1=pap,apl2=sap', 'N', 'N', 'N', NULL, NULL, NULL);

    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8303',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMP. PAG. EN EL EXT. SOLIC. POR LOS CONTRI. NACIONALES QUE PERCIBAN RENTAS DE FUENTE EXTRANJERA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8303',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMP. PAG. EN EL EXT. SOLIC. POR LOS CONTRI. NACIONALES QUE PERCIBAN RENTAS DE FUENTE EXTRANJERA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8305',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'EMPRESAS DE SERVICIOS PUBLICOS DOMICILIARIOS QUE PRESTEN SERVICIOS DE ACUEDUCTO Y ALCANTARILLADO.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8305',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'EMPRESAS DE SERVICIOS PUBLICOS DOMICILIARIOS QUE PRESTEN SERVICIOS DE ACUEDUCTO Y ALCANTARILLADO.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8316',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES DIRIGIDA A PROGRAMAS DE BECAS O CREDITOS CONDONABLES.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8316',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES DIRIGIDA A PROGRAMAS DE BECAS O CREDITOS CONDONABLES.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8317',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8317',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8318',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES SIN ANIMO DE LUCRO PERTENECIENTES AL REG. TRIBUTARIO ESPECIAL','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8318',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES SIN ANIMO DE LUCRO PERTENECIENTES AL REG. TRIBUTARIO ESPECIAL','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8319',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES S.A.L. NO CONTRIB. DE QUE TRATAN LOS ART. 22 Y 23 DEL E. T.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8319',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS A ENTIDADES S.A.L. NO CONTRIB. DE QUE TRATAN LOS ART. 22 Y 23 DEL E. T.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8320',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES REALIZADAS EN CONTROL, CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8320',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES REALIZADAS EN CONTROL, CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8321',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EN LA RED NACIONAL DE BIBLIOTECAS PUBLICAS Y BIBLIOTECA NACIONAL','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8321',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EN LA RED NACIONAL DE BIBLIOTECAS PUBLICAS Y BIBLIOTECA NACIONAL','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8322',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES A FAVOR DE FONDO PARA REPARACION DE VICTIMAS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8322',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES A FAVOR DE FONDO PARA REPARACION DE VICTIMAS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8323',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTOS PAGADOS EN EL EXTERIOR POR LA ENTIDAD CONTROLADA DEL EXTERIOR (ECE)','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8323',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTOS PAGADOS EN EL EXTERIOR POR LA ENTIDAD CONTROLADA DEL EXTERIOR (ECE)','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8324',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A LA CORP. GUSTAVO MATAMOROS Y DEMAS FUND. DEDICADAS A LA DEFENSA, PROTECCION DE D.H.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8324',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A LA CORP. GUSTAVO MATAMOROS Y DEMAS FUND. DEDICADAS A LA DEFENSA, PROTECCION DE D.H.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8325',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORGANISMOS DE DEPORTE AFICIONADO. E.T., ART. 126- 2, INCISO.2.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8325',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORGANISMOS DE DEPORTE AFICIONADO. E.T., ART. 126- 2, INCISO.2.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8326',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORG. DEPORTIVOS Y RECREATIVOS O CULTURALES PERS. JURIDICAS SIN ANIMO DE LUCRO','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8326',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACION A ORG. DEPORTIVOS Y RECREATIVOS O CULTURALES PERS. JURIDICAS SIN ANIMO DE LUCRO','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8327',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS PARA EL APADRINAMIENTO DE PARQUES NATURALES Y CONSER. DE BOSQUES NATURALES','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8327',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES EFECTUADAS PARA EL APADRINAMIENTO DE PARQUES NATURALES Y CONSER. DE BOSQUES NATURALES','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8328',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR APORTES AL S.G.P. A CARGO DEL EMPLEADOR CONTRIBUYENTE DEL IMP. UNIF. DEL R.S.T.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8328',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR APORTES AL S.G.P. A CARGO DEL EMPLEADOR CONTRIBUYENTE DEL IMP. UNIF. DEL R.S.T.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8329',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR VENTAS DE BIENES O SERVICIOS CON TARJETAS Y OTROS MECANISMOS ELECTRONICOS DE PAGOS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8329',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR VENTAS DE BIENES O SERVICIOS CON TARJETAS Y OTROS MECANISMOS ELECTRONICOS DE PAGOS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8330',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTO DE INDUSTRIA, COMERCIO, AVISOS Y TABLEROS. E.T., ART. 115 DEL E.T.','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8330',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IMPUESTO DE INDUSTRIA, COMERCIO, AVISOS Y TABLEROS. E.T., ART. 115 DEL E.T.','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8331',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IVA DE VENTAS EN LA IMPORT/ FORMACION/CONSTRUC. O ADQ. DE ACTIVOS FIJOS REALES PRODUCTIVOS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8331',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR IVA DE VENTAS EN LA IMPORT/ FORMACION/CONSTRUC. O ADQ. DE ACTIVOS FIJOS REALES PRODUCTIVOS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8332',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR CONV. CON COLDEPORTES PARA BECAS DE ESTUDIO Y MANUT. A DEPORTISTAS TALENTO O RESERVA DEPORTIVA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8332',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR CONV. CON COLDEPORTES PARA BECAS DE ESTUDIO Y MANUT. A DEPORTISTAS TALENTO O RESERVA DEPORTIVA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8333',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES EN CONTROL,CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE EN ACTIVIDADES TURISTICAS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8333',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'PARA INVERSIONES EN CONTROL,CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE EN ACTIVIDADES TURISTICAS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8334',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8334',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8336',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONAC. RECIB. POR EL FONDO NAL.DE FIN.PARA LA CIENCIA TECN. Y LA INNOV.,FRANCISCO JOSE DE CALDAS','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8336',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONAC. RECIB. POR EL FONDO NAL.DE FIN.PARA LA CIENCIA TECN. Y LA INNOV.,FRANCISCO JOSE DE CALDAS','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8337',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR REMUN. POR VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTRIB. DE RENTA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8337',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR REMUN. POR VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTRIB. DE RENTA','FINAL','DOS','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8338',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES RECIB. POR INTERMEDIO DEL ICETEX,BECAS QUE FINANCIEN LA EDUCACION A LA FUERZA PUBLICA','FINAL','UNO','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE, SALDO,VALOR,TENDENCIA_CREDITO ) VALUES ('1004_V8','8338',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'POR DONACIONES RECIB. POR INTERMEDIO DEL ICETEX,BECAS QUE FINANCIEN LA EDUCACION A LA FUERZA PUBLICA','FINAL','DOS','N' ) ;
    
    UPDATE MM_CONCEPTOS SET TENDENCIA_CREDITO='S' WHERE CODFORMATO='1007_V9' AND VALOR='UNO'; 
    UPDATE MM_CONCEPTOS SET TENDENCIA_CREDITO='N' WHERE CODFORMATO='1007_V9' AND VALOR='DOS'; 
    UPDATE MM_CONCEPTOS SET TOPE='0' WHERE CODFORMATO='1007_V9'; 
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1007_V9','4018',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'INGRESOS POR DIFERENCIA EN CAMBIO. SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','0','S','S' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1007_V9','4018',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'INGRESOS POR DIFERENCIA EN CAMBIO. SE DEBE REPORTAR CON EL NIT DEL INFORMANTE','FINAL','UNO','0','S','S' ) ;  
    
    UPDATE MM_CONCEPTOS SET TOPE='500000' WHERE CODFORMATO='1008_V7';
    UPDATE MM_CONCEPTOS SET TOPE='500000' WHERE CODFORMATO='1009_V7';  
    
    UPDATE MM_CONCEPTOS SET TOPE='3000000' WHERE CODFORMATO='1010_V8';
    
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - RENTAS EXENTAS POR INDEMNIZACIONES POR SEGUROS DE VIDA. E.T. ART. 223' WHERE CODFORMATO='1011_V6' AND CODIGO='8161';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - RENTA EX. POR LA GENERACION Y COMERCIALIZACION DE ENERGIA ELECTRICA' WHERE CODFORMATO='1011_V6' AND CODIGO='8163';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - RENTA EXENTA CREACIONES LITERARIAS DE LA ECONOMIA NARANJA' WHERE CODFORMATO='1011_V6' AND CODIGO='8170';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION POR CONCEPTO DE REGALIAS EN EL PAIS' WHERE CODFORMATO='1011_V6' AND CODIGO='8227';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DED. POR DONAC. E INVER. REALIZ. EN INVES., DESARROLLO TECNOLOGICO E INNOVACION' WHERE CODFORMATO='1011_V6' AND CODIGO='8229';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION POR LAS CONTRIBUCIONES A CARTERAS COLECTIVAS' WHERE CODFORMATO='1011_V6' AND CODIGO='8235';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION IMPUESTOS, REGALIAS Y CONTRIBUCIONES PAG. POR ORGAN. DESCENTRALIZADOS' WHERE CODFORMATO='1011_V6' AND CODIGO='8260';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - COSTOS DE IMPUESTOS DEVENGADOS. E.T., ART. 115' WHERE CODFORMATO='1011_V6' AND CODIGO='8280';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - DEDUCCION ESPECIAL DE I.V.A. POR ADQ. O IMP.DE BIENES DE CAPITAL GRAV. A LA T.G' WHERE CODFORMATO='1011_V6' AND CODIGO='8281';
    UPDATE MM_CONCEPTOS SET CODIGO='8405',NOMBRE='DEDUCCION POR DETERIORO ( PROVISION INDIVIDUAL ) DE CARTERA DE DUDOSO O DIFICIL COBRO' WHERE CODFORMATO='1011_V6' AND CODIGO='8205';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION DE IVA EN VENTA DE SERVICIOS DE CORRETAJE DE REASEGUROS' WHERE CODFORMATO='1011_V6' AND CODIGO='9022';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX DE IVA EN ALIM, VEST, E.ASEO, MEDI,BICI, MOTOCI,MOTOCA /AMAZ, GUAI Y VAUPES' WHERE CODFORMATO='1011_V6' AND CODIGO='9040';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX DE IVA EN BICI, MOTOCIC,MOTOCA, Y SUS PARTES DEST. AMAZONAS GUAINIA Y VAUPES' WHERE CODFORMATO='1011_V6' AND CODIGO='9050';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX. TRANS. IVA VENTA DE MAT.PRIMAS QUIMICAS NAL. E IMPOR. PARA LA PROD. DE MED.' WHERE CODFORMATO='1011_V6' AND CODIGO='9063';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION TRANSITORIA DE IVA EN CONTRATOS DE FRANQUICIA' WHERE CODFORMATO='1011_V6' AND CODIGO='9064';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EX. DE IVA SERV. ARTIST. PREST. PROD. AUDIOV. DE ESPECT. PUB. DE ARTE ESCENICO' WHERE CODFORMATO='1011_V6' AND CODIGO='9068';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION DEL IVA EN VENTAS SOBRE EL ARRENDAMIENTO DE LOCALES COMERCIALES' WHERE CODFORMATO='1011_V6' AND CODIGO='9070';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXCLUSION TRANSITORIA IVA EN LA PRESTACION DE SERVICIOS DE HOTELERIA Y TURISMO' WHERE CODFORMATO='1011_V6' AND CODIGO='9071';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA 5% EN LA 1A VENTA DE UND.DE VIVIENDA NUEVA CUYO VALOR SUPERE 26.800 UVT' WHERE CODFORMATO='1011_V6' AND CODIGO='9104';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA DEL 5% POR VENTA DE GASOLINA DE AVIACION JET A1 NACIONALES' WHERE CODFORMATO='1011_V6' AND CODIGO='9108';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA DEL 5% EN EL TRANSPORTE AEREO DE PASAJEROS' WHERE CODFORMATO='1011_V6' AND CODIGO='9109';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - TARIFA DEL 5% HASTA EL 31/DIC/2022 VENTA DE TIQ.AEREOS DE PASAJ., SERV.CONEXOS' WHERE CODFORMATO='1011_V6' AND CODIGO='9111';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA EN VENTA DE BIOCOMBUSTIBLE' WHERE CODFORMATO='1011_V6' AND CODIGO='9201';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA VENTAS NALES DE PROD. DE USO MEDICO E INSUMOS  SEGUN E.TECNICAS' WHERE CODFORMATO='1011_V6' AND CODIGO='9219';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA VENTAS NALES DE BIENES E INSUMOS MEDICOS SIN DERECHO A DEV.' WHERE CODFORMATO='1011_V6' AND CODIGO='9220';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXE. DE IVA IMPO. Y VENTAS NALES DE PROD. DE USO MEDICO E INS. SEGUN E.TECNICAS' WHERE CODFORMATO='1011_V6' AND CODIGO='9223';
    UPDATE MM_CONCEPTOS SET NOMBRE='NO USAR - A.G 2023 - EXENCION DE IVA EN SERVICIOS DE VOZ E INTERNET MOVILES' WHERE CODFORMATO='1011_V6' AND CODIGO='9224';      
    
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8406',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR DETERIORO ( PROVISION GENERAL ) DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8406',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR DETERIORO ( PROVISION GENERAL ) DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8413',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PROVISIONES DE CARTERA HIPOTECARIA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8413',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PROVISIONES DE CARTERA HIPOTECARIA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8414',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE E. P. S. - EPS EN LIQUIDACION FORZOSA INTERVENIDAS POR LA S.N.S.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8414',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE E. P. S. - EPS EN LIQUIDACION FORZOSA INTERVENIDAS POR LA S.N.S.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8415',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8415',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION DETERIORO DE CARTERA DE DUDOSO O DIFICIL COBRO','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8416',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPL. DE HIDROCARBUROS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8416',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPL. DE HIDROCARBUROS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8417',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE GAS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8417',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE GAS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8418',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE CARBON','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8418',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE CARBON','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8419',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAG. A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE OTROS MINERALES ','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8419',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAG. A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE OTROS MINERALES ','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8420',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE SAL Y MAT. DE C.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8420',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRIT. POR LA EXPLOT. DE SAL Y MAT. DE C.','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8421',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE ACTIVOS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8421',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE ACTIVOS','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8422',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE PLUSVALIA','FINAL','UNO','0','S','N' ) ;
    INSERT INTO MM_CONCEPTOS ( CODFORMATO,CODIGO, RENGLON,NOMBRE,SALDO,VALOR,TOPE,FORZOSO,TENDENCIA_CREDITO) VALUES ('1011_V6','8422',(SELECT GEN_ID(GEN_MM_CONCEPTOS,1) FROM RDB$DATABASE),'DEDUCCION POR PERDIDA EN LA ENAJENACION DE PLUSVALIA','FINAL','UNO','0','S','N' ) ;

  END

END;

COMMIT WORK;

/* CONTABILIDAD A DOCUMENTO DS*/
UPDATE Documentos
SET Contabilidad = 'S'
WHERE Codigo = 'DS';

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Tipo CHAR(5);
BEGIN
  FOR SELECT DISTINCT Tipo
      FROM Auto_Patrones
      INTO V_Tipo
  DO
  BEGIN
    UPDATE Documentos
    SET Contabilidad = 'S'
    WHERE Codigo = :V_Tipo;
  END
END;
COMMIT WORK; 


/**** MODULO CONTABLE FINAL                              ****/

/************************************************************/
/**** MODULO NOMINA                                      ****/
/************************************************************/

/* EXPORTADOS NOMINA INICIO*/

DELETE FROM Exportados WHERE Pt = 'NOOVA_NE';

COMMIT WORK;

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:noov="http://noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <noov:Username>UsuarioNoova</noov:Username>
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--967 Nomina 15/Ago/2024 v2-->
    <soapenv:Body>
        <noov:SetNomNominas>
            <noov:nomina>
                nom:nomina
                <noov:Periodo>
                    nom:periodo
                </noov:Periodo>
                <noov:InformacionGeneral>nom:inf_general</noov:InformacionGeneral>
                <noov:LNotas>nom:notas</noov:LNotas>
                <noov:Empleador>nom:empleador</noov:Empleador>
                <noov:Trabajador>nom:trabajador</noov:Trabajador>
                <noov:Pago>nom:pago</noov:Pago>
                <noov:Devengados>
                    <noov:Basico>nom:basico</noov:Basico>
                    nom:transporte
                    <noov:LHorasExtras>nom:horas_extras</noov:LHorasExtras>
                    <noov:LVacaciones>nom:vacaciones</noov:LVacaciones>
                    nom:primas
                    nom:cesantias
                    nom:dotacion
                    nom:indemnizacion
                    <noov:LIncapacidades>nom:incapacidad</noov:LIncapacidades>
                    <noov:LLicencias>nom:licencias</noov:LLicencias>
                    <noov:LBonificaciones>nom:bonificacion</noov:LBonificaciones>
                    <noov:LAuxilios>nom:auxilios</noov:LAuxilios>
                    <noov:LHuelgasLegales>nom:huelgas</noov:LHuelgasLegales>
                    <noov:LBonoEPCTVs>nom:bonos</noov:LBonoEPCTVs>
                    <noov:LComisiones>nom:comision</noov:LComisiones>
                    <noov:LOtrosConceptos>nom:otro_devengados</noov:LOtrosConceptos>
                </noov:Devengados>
                <noov:Deducciones>
                    nom:salud
                    nom:pension
                    nom:fondosp
                    <noov:LSindicatos>nom:sindicato</noov:LSindicatos>
                    <noov:LSanciones>nom:sancion</noov:LSanciones>
                    <noov:LLibranzas>nom:libranzas</noov:LLibranzas>
                    nom:pag_ter
                    nom:anticipos
                    nom:pvoluntaria
                    nom:retencionfuente
                    nom:ahorrofomentoconstr
                    nom:cooperativa
                    nom:embargofiscal
                    nom:plancomplementarios
                    nom:educacion
                    nom:reintegro
                    nom:deuda
                    nom:otras_ded
                </noov:Deducciones>
                nom:predecesor
                </noov:nomina>
        </noov:SetNomNominas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA_NE');


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1005, 'nom:nomina', 'DOC', '-- Encabezado NE 967 Nomina 15/Ago/2024 v2

SELECT 1 "noov:Nvsuc_codi",
       ''NE'' "noov:Nvnom_pref",
       :Numero: "noov:Nvnom_cons",
       ''NE'' || :Numero: "noov:Nvnom_nume",
       ''NM'' "noov:Nvope_tipo",
       EXTRACT(YEAR FROM CAST(:Fecha_Hasta: AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) "noov:Nvnom_fpag",
       2000 "noov:Nvnom_redo",
       COALESCE(Adicion, 0) "noov:Nvnom_devt",
       COALESCE(Deduccion, 0) "noov:Nvnom_dedt",
       COALESCE(Neto, 0) "noov:Nvnom_comt",
       IIF(:Clase_Fe: = ''FACTURA'', '''', ''CUNE'') "noov:Nvnom_cnov",
       (SELECT Ajuste
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvnom_tipo"
FROM Pz_Ne_Resumen(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)', 'S', 5, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1010, 'nom:periodo', 'DOC', '-- Periodo

WITH V_Fechas
AS (SELECT Codpersonal,
           Desde,
           Hasta,
           MAX(Ingreso) Ingreso,
           MAX(Retiro) Retiro
    FROM (SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE) Desde,
                 CAST(:Fecha_Hasta: AS DATE) Hasta,
                 MAX(S.Desde) Ingreso,
                 CAST(''01/01/1900'' AS DATE) Retiro
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Desde = ''INGRESO''
                AND S.Desde <= :Fecha_Hasta:
          GROUP BY 1, 2, 3
          UNION
          SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE),
                 CAST(:Fecha_Hasta: AS DATE),
                 CAST(''01/01/1900'' AS DATE),
                 MAX(S.Hasta)
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Hasta = ''RETIRO''
                AND S.Hasta <= :Fecha_Hasta:
          GROUP BY 1, 2, 3)
    GROUP BY 1, 2, 3)

SELECT EXTRACT(YEAR FROM Fe.Ingreso) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Ingreso) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Ingreso) + 100, 2) "noov:Nvper_fing",
       IIF(Fe.Retiro >= Fe.Desde AND Fe.Retiro <= Fe.Hasta, EXTRACT(YEAR FROM Fe.Retiro) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Retiro) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Retiro) + 100, 2), EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2)) "noov:Nvper_fret",
       EXTRACT(YEAR FROM Fe.Desde) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Desde) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Desde) + 100, 2) "noov:Nvper_fpin",
       EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2) "noov:Nvper_fpfi",
       DATEDIFF(DAY FROM Fe.Ingreso TO Fe.Hasta)+1 "noov:Nvper_tlab"
FROM V_Fechas Fe', 'S', 10, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1015, 'nom:inf_general', 'DOC', '-- Inf general

SELECT (SELECT Tipo_Nomina
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvinf_tnom",
        P.Codtipoperiodo "noov:Nvinf_pnom",
        ''COP'' "noov:Nvinf_tmon"
FROM Personal P
WHERE P.Codigo = :Receptor:', 'S', 15, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1020, 'nom:notas', 'DOC', '-- Notas

SELECT DISTINCT N.Nombre "noov:string"
FROM Nominas N
LEFT JOIN Planillas P ON (N.Codigo = P.Codnomina)
LEFT JOIN Planillas_Ne Ps ON (N.Codigo = Ps.Codnomina)
WHERE (P.Codpersonal = :Receptor: OR (Ps.Codpersonal = :Receptor:))
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:', 'S', 20, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1025, 'nom:empleador', 'DOC', '-- Empleador

SELECT FIRST 1 T.Nombre "noov:Nvemp_nomb",
               T.Codigo "noov:Nvemp_nnit",
               T.Dv "noov:Nvemp_endv",
               Pa.Codigo_Fe "noov:Nvemp_pais",
               SUBSTRING(T.Codmunicipio FROM 1 FOR 2) "noov:Nvemp_depa",
               T.Codmunicipio "noov:Nvemp_ciud",
               T.Direccion "noov:Nvemp_dire"
FROM Terceros T
JOIN Paises Pa ON (T.Codpais = Pa.Codigo)
WHERE T.Codigo = :Emisor:', 'S', 25, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1030, 'nom:trabajador', 'DOC', '-- Trabajador

WITH V_Correo
AS (SELECT FIRST 1 Codigo,
                   TRIM(Email) Email,
                   COUNT(1) Cant
    FROM Contactos
    WHERE Codcargo = ''NECOR''
          AND Codtercero = :Emisor:
    GROUP BY 1, 2
    ORDER BY Codigo)
SELECT LPAD(IIF(TRIM(E.Codcotizante) = ''20'', ''23'', TRIM(E.Codcotizante)), 2, 0) "noov:Nvtra_tipo",
       LPAD(IIF(TRIM(E.Codsubcotizante) <> ''0'', ''1'', TRIM(E.Codsubcotizante)), 2, 0) "noov:Nvtra_stip",
       IIF(COALESCE(E.Alto_Riesgo, 0) = ''N'', ''false'', ''true'') "noov:Nvtra_arpe", --Actividades de alto riesgo
       IIF(E.Codidentidad = ''48'', ''47'', E.Codidentidad) "noov:Nvtra_dtip",
       E.Codigo "noov:Nvtra_ndoc",
       E.Apl1 "noov:Nvtra_pape",
       IIF(COALESCE(E.Apl2, '''') = '''', ''.'', E.Apl2) "noov:Nvtra_sape",
       E.Nom1 "noov:Nvtra_pnom",
       E.Nom2 "noov:Nvtra_onom",
       ''CO'' "noov:Nvtra_ltpa",
       SUBSTRING(E.Codmunicipio FROM 1 FOR 2) "noov:Nvtra_ltde",
       E.Codmunicipio "noov:Nvtra_ltci",
       E.Direccion "noov:Nvtra_ltdi",
       IIF(E.Salario_Integral = ''X'', ''true'', ''false'') "noov:Nvtra_sint",
       C.Codtipocontrato "noov:Nvtra_tcon", --Tipo Contrato
       COALESCE((SELECT FIRST 1 Basico
                 FROM Nom_Pila_Salarios(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvtra_suel",
       E.Codigo "noov:Nvtra_codt",
       IIF(Ne.Cant > 0, COALESCE(Ne.Email, E.Email), ''ne.mekano@gmail.com'') "noov:Nvtra_mail"
FROM Personal E
JOIN Identidades I ON (E.Codidentidad = I.Codigo)
LEFT JOIN Contratacion C ON (E.Codigo = C.Codpersonal)
LEFT JOIN V_Correo Ne ON 1 = 1
WHERE E.Codigo = :Receptor:
      AND ((C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:))', 'S', 30, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1035, 'nom:pago', 'DOC', '--  Pagos

WITH V_Medio
AS (SELECT COALESCE((SELECT FIRST 1 F.Codigo_Fe
                     FROM Medios_Pago M
                     JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
                     WHERE M.Codpersonal = :Receptor:),
           (SELECT FIRST 1 F.Codigo_Fe
            FROM Medios_Pago M
            JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
            WHERE Codpersonal IS NULL)) Medio
    FROM Rdb$Database)
SELECT 1 "noov:Nvpag_form",
       M.Medio "noov:Nvpag_meto", --Metodo de pago
       B.Nombre "noov:Nvpag_banc", --Nombre banco
       Tc.Nombre "noov:Nvpag_tcue",
       E.Banco "noov:Nvpag_ncue"
FROM Personal E
LEFT JOIN Entidades B ON (E.Codentidad = B.Codigo)
LEFT JOIN Tipocuentas Tc ON (E.Codtipocuenta = Tc.Codigo)
JOIN V_Medio M ON (1 = 1)
WHERE E.Codigo = :Receptor:', 'S', 35, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1040, 'nom:basico', 'DOC', '-- Basico

SELECT COALESCE((SELECT IIF(ROUND(Valor) = 0, 1, ROUND(Valor))
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DTRA'')), 0) "noov:Nvbas_dtra",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''STRA'')), 0) "noov:Nvbas_stra"
FROM Rdb$Database', 'S', 40, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1042, 'nom:transporte', 'DOC', '-- Transporte NE

SELECT SUM(COALESCE(Valor, 0)) "noov:Nvtrn_auxt",
       SUM(0) "noov:Nvbon_vias",
       SUM(0) "noov:Nvbon_vins"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUX'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(COALESCE(Valor, 0)),
       SUM(0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VIAS'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(0),
       SUM(COALESCE(Valor, 0))
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VINS'')
HAVING SUM(Valor) > 0', 'S', 42, 'NOOVA_NE', 'FACTURA', 'noov:Transporte', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1045, 'nom:horas_extras', 'DOC', '-- Horas Extras NE

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HENDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HED''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRNDF''
      AND Valor > 0', 'S', 45, 'NOOVA_NE', 'FACTURA', 'noov:DTOHorasExtras', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1160, 'nom:vacaciones', 'DOC', '-- Vacaciones NE

SELECT
--'''' "noov:Nvcom_fini",
--      '''' "noov:Nvcom_ffin",
       SUM(IIF(Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q''), IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0)) "noov:Nvcom_cant",
       SUM(IIF(Codgrupo_Ne IN (''VAC1S'', ''VAC2S''), Valor, 0)) "noov:Nvcom_pago",
       Codigo_Ne "noov:Nvvac_tipo"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q'', ''VAC1S'', ''VAC2S'')
GROUP BY 3
HAVING SUM(Valor) > 0', 'S', 160, 'NOOVA_NE', 'FACTURA', 'noov:DTOVacaciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1165, 'nom:primas', 'DOC', '-- Primas NE

SELECT SUM("noov:Nvpri_cant") "noov:Nvpri_cant",
       SUM("noov:Nvpri_pago") "noov:Nvpri_pago",
       SUM("noov:Nvpri_pagn") "noov:Nvpri_pagn"
FROM (SELECT COALESCE(IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0) "noov:Nvpri_cant",
             0 "noov:Nvpri_pago",
             0 "noov:Nvpri_pagn"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIQ'')

      UNION ALL

      SELECT 0,
             COALESCE(Valor, 0),
             0
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIP'')
            AND (Valor > 0)

      UNION ALL

      SELECT 0,
             0,
             COALESCE(Valor, 0)
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIPN'')
            AND (Valor > 0))
HAVING SUM("noov:Nvpri_cant" + "noov:Nvpri_pago" + "noov:Nvpri_pagn") > 1', 'S', 165, 'NOOVA_NE', 'FACTURA', 'noov:Primas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1170, 'nom:cesantias', 'DOC', '-- Cesantias NE

SELECT COALESCE((SELECT SUM(Valor)
                 FROM Pz_Ne_Grupo_Ces(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvces_pago",
       IIF(COALESCE((SELECT Valor
                     FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESPA'')), 0) = 0, 0,
       (SELECT Codigo_Ne
        FROM Grupo_Ne
        WHERE Codigo = ''CESPOR'')) "noov:Nvces_porc",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESIPA'')), 0) "noov:Nvces_pagi"
FROM Rdb$Database
WHERE COALESCE((SELECT SUM(Valor)
                FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
                WHERE Codgrupo_Ne IN (''CESPA'', ''CESIPA'')), 0) > 0', 'S', 170, 'NOOVA_NE', 'FACTURA', 'noov:Cesantias', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1175, 'nom:incapacidad', 'DOC', '-- Incapacidades NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvinc_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvinc_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOQ'', ''INPRQ'', ''INLAQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOV'', ''INPRV'', ''INLAV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 175, 'NOOVA_NE', 'FACTURA', 'noov:DTOIncapacidad', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1195, 'nom:licencias', 'DOC', '-- Licencias NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvlic_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvlic_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMQ'', ''LRQ'', ''LNQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMV'', ''LRV'', ''LNV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 195, 'NOOVA_NE', 'FACTURA', 'noov:DTOLicencia', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1197, 'nom:bonificacion', 'DOC', '-- Bonificacion NE

SELECT COALESCE(Valor, 0) "noov:Nvbon_bofs",
       0 "noov:Nvbon_bons"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BOFS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BONS'')
WHERE (Valor > 0)', 'S', 197, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonificacion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1199, 'nom:auxilios', 'DOC', '-- Auxilios

SELECT COALESCE(Valor, 0) "noov:Nvaux_auxs",
       0 "noov:Nvaux_auns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUXS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUNS'')
WHERE (Valor > 0)', 'S', 199, 'NOOVA_NE', 'FACTURA', 'noov:DTOAuxilio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1200, 'nom:huelgas', 'DOC', '-- Huelgas NE

WITH V_Pago
AS (SELECT Gn.Codigo Tipo,
           SUM(P.Adicion + P.Deduccion) Valor
    FROM Planillas P
    JOIN Nominas N ON (P.Codnomina = N.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
    LEFT JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
    WHERE P.Codpersonal = :Receptor:
          AND N.Fecha >= :Fecha_Desde:
          AND N.Fecha <= :Fecha_Hasta:
          AND R.Codgrupo_Ne = ''HUV''
    GROUP BY 1)

SELECT G.Inicio "noov:Nvcom_fini",
       G.Fin "noov:Nvcom_ffin",
       P.Valor "noov:Nvcom_cant",
       Pago.Valor "noov:Nvcom_pago"
FROM Planillas P
JOIN Nominas N ON (P.Codnomina = N.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
JOIN V_Pago Pago ON (Gn.Codigo = Pago.Tipo)
WHERE P.Codpersonal = :Receptor:
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:
      AND R.Codgrupo_Ne = ''HUQ''', 'S', 235, 'NOOVA_NE', 'FACTURA', 'noov:DTOHuelgaLegal', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1205, 'nom:bonos', 'DOC', '-- Bonos

SELECT COALESCE(IIF(Codgrupo_Ne = ''PAGS'', Valor, 0), 0) "noov:Nvbon_pags",
       COALESCE(IIF(Codgrupo_Ne = ''PANS'', Valor, 0), 0) "noov:Nvbon_pans",
       COALESCE(IIF(Codgrupo_Ne = ''ALIS'', Valor, 0), 0) "noov:Nvbon_alis",
       COALESCE(IIF(Codgrupo_Ne = ''ALNS'', Valor, 0), 0) "noov:Nvbon_alns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''PAGS'', ''PANS'', ''ALIS'', ''ALNS'')', 'S', 236, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonoEPCTV', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1210, 'nom:comision', 'DOC', '-- Comisiones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''COMIS'')
WHERE Valor > 0', 'S', 210, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1215, 'nom:dotacion', 'DOC', '-- Dotacion

SELECT Valor "noov:Dotacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DOTAC'')
WHERE Valor > 0', 'S', 215, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1240, 'nom:otro_devengados', 'DOC', '-- Otros devengados

SELECT NOMRUBRO "noov:Nvotr_desc",
       IIF(CODGRUPO_NE = ''ODSA'', VALOR, 0) "noov:Nvotr_pags",
       IIF(CODGRUPO_NE = ''ODNS'', VALOR, 0) "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE (CODGRUPO_NE IN (''ODSA'', ''ODNS''))
      AND (VALOR > 0)

UNION ALL

SELECT ''CAUSACION VACACIONES'' "noov:Nvotr_desc",
       0 "noov:Nvotr_pags",
       VALOR "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE CODGRUPO_NE = ''ODNS_V''
      AND VALOR > 0', 'S', 240, 'NOOVA_NE', 'FACTURA', 'noov:DTOOtroDevengado', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1355, 'nom:salud', 'DOC', '-- Salud NE

SELECT COALESCE((SELECT IIF(Codcotizante IN (''51'', ''12'', ''19''), 0, Codigo_Ne)
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_dedu"
FROM Rdb$Database', 'S', 355, 'NOOVA_NE', 'FACTURA', 'noov:Salud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1360, 'nom:pension', 'DOC', '-- Pension NE

SELECT COALESCE((SELECT Codigo_Ne
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_dedu"
FROM Rdb$Database', 'S', 360, 'NOOVA_NE', 'FACTURA', 'noov:FondoPension', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1365, 'nom:fondosp', 'DOC', 'SELECT "noov:Nvfsp_porc",
       "noov:Nvfsp_dedu",
       "noov:Nvfsp_posb",
       "noov:Nvfsp_desb"
FROM Pz_Ne_Fondo_Sp(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE "noov:Nvfsp_dedu" + "noov:Nvfsp_desb" > 0', 'S', 365, 'NOOVA_NE', 'FACTURA', 'noov:FondoSP', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1370, 'nom:sindicato', 'DOC', '-- Sindicato NE

SELECT Valor "noov:Nvsin_porc",
       0 "noov:Nvsin_dedu"
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''PORSIN''
      AND Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''VAL_SIN''
      AND Valor > 0', 'S', 370, 'NOOVA_NE', 'FACTURA', 'noov:DTOSindicato', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1375, 'nom:sancion', 'DOC', '-- Sancion NE

SELECT Valor "noov:Nvsan_sapu",
       0 "noov:Nvsan_sapv"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPU'')
WHERE Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPV'')
WHERE Valor > 0', 'S', 375, 'NOOVA_NE', 'FACTURA', 'noov:DTOSancion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1380, 'nom:libranzas', 'DOC', '-- Libranzas

SELECT Nomrubro "noov:Nvlib_desc",
       Valor "noov:Nvlib_dedu"
FROM Pz_Ne_Grupo_Base(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE Codgrupo_Ne = ''LIBRA''
      AND Valor > 0', 'S', 380, 'NOOVA_NE', 'FACTURA', 'noov:DTOLibranza', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1382, 'nom:pag_ter', 'DOC', '-- Pagos a Terceros

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''PAG_TER'')
WHERE Valor > 0', 'S', 382, 'NOOVA_NE', 'FACTURA', 'noov:LPagosTerceros', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1383, 'nom:anticipos', 'DOC', '-- Anticipos

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT02'')
WHERE Valor > 0', 'S', 383, 'NOOVA_NE', 'FACTURA', 'noov:LAnticipos', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1384, 'nom:pvoluntaria', 'DOC', '-- Pension Voluntaria

SELECT Valor "noov:PensionVoluntaria"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT03'')
WHERE Valor > 0', 'S', 384, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1385, 'nom:retencionfuente', 'DOC', '-- RetencionFuente

SELECT Valor "noov:RetencionFuente"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT04'')
WHERE Valor > 0', 'S', 385, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1386, 'nom:ahorrofomentoconstr', 'DOC', '-- Ahorro Fomento Construccion

SELECT Valor "noov:AhorroFomentoConstr"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT06'')
WHERE Valor > 0', 'S', 386, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1387, 'nom:cooperativa', 'DOC', '-- Cooperativa

SELECT Valor "noov:Cooperativa"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT07'')
WHERE Valor > 0', 'S', 387, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1388, 'nom:embargofiscal', 'DOC', '-- Embargo Fiscal

SELECT Valor "noov:EmbargoFiscal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT08'')
WHERE Valor > 0', 'S', 388, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1389, 'nom:plancomplementarios', 'DOC', '-- Plan Complementarios

SELECT Valor "noov:PlanComplementarios"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT09'')
WHERE Valor > 0', 'S', 389, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1390, 'nom:educacion', 'DOC', '-- Educacion

SELECT Valor "noov:Educacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT10'')
WHERE Valor > 0', 'S', 390, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1391, 'nom:reintegro', 'DOC', '-- Reintegro

SELECT Valor "noov:Reintegro"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT11'')
WHERE Valor > 0', 'S', 391, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1392, 'nom:deuda', 'DOC', '-- Deuda

SELECT Valor "noov:Deuda"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT12'')
WHERE Valor > 0', 'S', 392, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1393, 'nom:otras_ded', 'DOC', '-- Otros deducciones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT00'')
WHERE Valor > 0', 'S', 393, 'NOOVA_NE', 'FACTURA', 'noov:LOtrasDeducciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1394, 'nom:predecesor', 'DOC', '-- Predecesor

SELECT Numero "noov:Nvpre_nume",
       Cune "noov:Nvpre_cune",
       EXTRACT(YEAR FROM CAST(Fecha_Ne AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(Fecha_Ne AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(Fecha_Ne AS DATE)) + 100, 2) "noov:Nvpre_fgen"
FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE trim(Tipo_Nomina) = 103', 'S', 394, 'NOOVA_NE', 'FACTURA', 'noov:Predecesor', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1395, 'nom:indemnizacion', 'DOC', '--INDEMNIZACION

SELECT Valor "noov:indemnizacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''INDEM'')
WHERE Valor > 0', 'S', 216, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;



/* EXPORTADOS NOMINA FIN*/



UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('NOM030220', 'PROMEDIO DE SALARIOS PARA EXOGENA', 'Promedio ultimos 6 meses del AG a reportar. Solo Nomimas nativas', 'SELECT Empleado, Nombre_Empleado, Dias, Acumulado_Devengado, Promedio
FROM Pz_Salario_Exo(:A_Periodo)', 'N', 'NOMINA', 'N', 'MOVIMIENTO NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;

-- SC_NOMINA080
UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=(0)
ELSE
IF(DIAS=0) THEN 
   RESULT:=(0)
ELSE
IF(QUINCENA<2 OR ES_MEDT=1) THEN 
   RESULT:=(0)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=(ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF(SAL_BASICO>=SMLV) THEN 
    RESULT:=(ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE 
    RESULT:=(ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ))))'
WHERE (CODIGO = 'SS017') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=(0)
ELSE
IF(QUINCENA<2) THEN 
   RESULT:=(0)
ELSE
IF(ES_ARL=1) THEN 
    RESULT:=(BASICO)
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=(SMLV)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=(ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=(ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
ELSE
  IF(SAL_BASICO>=SMLV) THEN 
     RESULT:=(ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG))
  ELSE 
      RESULT:=(ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ))))'
WHERE (CODIGO = 'SS015') AND
      (NATIVO = 'S');



COMMIT WORK;

-- NOMBRE RUBRO PARA EMPLEADOS SENA
SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Nom_Nomina (
    Nomina VARCHAR(5))
RETURNS (
    Empleado        VARCHAR(15),
    Nombre_Empleado VARCHAR(83),
    Esqnomina       VARCHAR(5),
    Banco           VARCHAR(80),
    Direccion       VARCHAR(80),
    Percentro       VARCHAR(5),
    Tel             VARCHAR(15),
    Cel             VARCHAR(15),
    Nom1            VARCHAR(20),
    Nom2            VARCHAR(20),
    Nomcargo        VARCHAR(80),
    Basico          DOUBLE PRECISION,
    Pension         VARCHAR(80),
    Salud           VARCHAR(80),
    Municipio       VARCHAR(80),
    Rubro           VARCHAR(5),
    Nomrubro        VARCHAR(80),
    Columna         VARCHAR(4),
    Valor           DOUBLE PRECISION,
    Adicion         DOUBLE PRECISION,
    Deduccion       DOUBLE PRECISION)
AS
DECLARE VARIABLE Codcargo    VARCHAR(5);
DECLARE VARIABLE V_Desde     DATE;
DECLARE VARIABLE V_Hasta     DATE;
DECLARE VARIABLE V_Fecha     DATE;
DECLARE VARIABLE V_Ciudad    VARCHAR(5);
DECLARE VARIABLE V_Percentro VARCHAR(5);
DECLARE VARIABLE V_Centroc   VARCHAR(5);
DECLARE VARIABLE V_Cotizante CHAR(2);
BEGIN

  SELECT Desde,
         Hasta,
         Fecha
  FROM Nominas
  WHERE Codigo = :Nomina
  INTO V_Desde,
       V_Hasta,
       V_Fecha;

  FOR SELECT TRIM(Empleado),
             Esquema,
             Rubro,
             TRIM(Nombre_Rubro),
             Valor,
             Adicion,
             Deduccion
      FROM Nom_Nomina(:Nomina)
      WHERE Adicion + Deduccion + Valor <> 0
      INTO Empleado,
           Esqnomina,
           Rubro,
           Nomrubro,
           Valor,
           Adicion,
           Deduccion
  DO
  BEGIN
    -- Personal
    SELECT TRIM(Nombre),
           TRIM(Banco),
           TRIM(Direccion),
           Codcentro,
           Codcargo,
           Telefono,
           Movil,
           TRIM(Nom1),
           TRIM(Nom2),
           Codmunicipio,
           Codcotizante
    FROM Personal P
    WHERE Codigo = :Empleado
    INTO Nombre_Empleado,
         Banco,
         Direccion,
         V_Percentro,
         Codcargo,
         Tel,
         Cel,
         Nom1,
         Nom2,
         V_Ciudad,
         V_Cotizante;
    --
    SELECT Codcentro
    FROM Planillas
    WHERE Codpersonal = :Empleado
          AND Codnomina = :Nomina
          AND Codrubro = 'SA100'

    INTO V_Centroc;
    Percentro = :V_Centroc;

    /*  -- Fechas nomina
    SELECT Desde,
           Hasta,
           Fecha
    FROM Nominas
    WHERE Codigo = :Nomina
    INTO V_Desde,
         V_Hasta,
         V_Fecha;    */

    -- Cargo
    SELECT Nombre
    FROM Cargos
    WHERE Codigo = :Codcargo
    INTO Nomcargo;

    -- Basico
    SELECT MAX(Basico)
    FROM Nom_Pila_Salarios(:Empleado, :V_Desde, :V_Hasta)
    INTO Basico;

    -- Fondo pension
    SELECT FIRST 1 T.Nombre
    FROM Aportes_Personal P
    JOIN Tipoaportes T ON (P.Codtipoaporte = T.Codigo AND
          :V_Fecha BETWEEN P.Desde AND P.Hasta)
    WHERE Codcomponente = '03'
          AND P.Codpersonal = :Empleado
    INTO Pension;

    -- EPS
    SELECT FIRST 1 T.Nombre
    FROM Aportes_Personal P
    JOIN Tipoaportes T ON (P.Codtipoaporte = T.Codigo AND
          :V_Fecha BETWEEN P.Desde AND P.Hasta)
    WHERE Codcomponente = '04'
          AND P.Codpersonal = :Empleado
    INTO Salud;

    -- Ciudad
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Ciudad
    INTO Municipio;

    -- Rubros
    SELECT Columna
    FROM Rubros
    WHERE Codigo = :Rubro
    INTO Columna;

    /* Manejo especial para los rubros de dias y horas */
    IF ((COALESCE(Columna, '') >= 'C_00' AND
        COALESCE(Columna, '') <= 'C_98') OR (COALESCE(Columna, '') >= 'B_00' AND
        COALESCE(Columna, '') <= 'B_98')) THEN
    BEGIN

      IF (Adicion > 0) THEN
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) = 'DIAS') THEN
        BEGIN
          Valor = Adicion;
          Adicion = 0;
        END
        ELSE
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) <> 'HORA') THEN
          Valor = 0;

      IF (Deduccion > 0) THEN
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) = 'DIAS') THEN
        BEGIN
          Valor = Deduccion;
          Deduccion = 0;
        END
        ELSE
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) <> 'HORA') THEN
          Valor = 0;
    END
    -- Nombre rubro para empleados Sena
    IF (:V_Cotizante IN ('12', '19', '20', '21')) THEN
      Nomrubro = CASE TRIM(Rubro)
                   WHEN 'DI017' THEN 'DIAS DE APOYO'
                   WHEN 'SA031' THEN 'APOYO DE SOSTENIMIENTO'
                   WHEN 'SA017' THEN 'APOYO DE SOSTENIMIENTO EN INCAPACIDADES'
                   ELSE Nomrubro
                 END;
    IF (COALESCE(Columna, '') <> '') THEN
      SUSPEND;
  END
END
^

SET TERM ; ^
COMMIT WORK;



/**** MODULO NOMINA FINAL                                ****/

/************************************************************/
/**** MODULO REST SERVER                                 ****/
/************************************************************/

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Rest_Select_Datos_Fe (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Cufe       CHAR(100),
    Qrdata     CHAR(2000),
    Validacion CHAR(50))
AS
DECLARE VARIABLE V_Cant       INTEGER;
DECLARE VARIABLE V_Encontrado INTEGER;
BEGIN
  V_Cant = 0;
  V_Encontrado = 0;
  WHILE (V_Cant < 2000000 AND V_Encontrado = 0) DO
  BEGIN

    SELECT COUNT(1)
    FROM Facturas
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Encontrado;
    V_Cant = V_Cant + 1;
  END
  IF (:V_Encontrado = 0) THEN
    EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_) || ' NO HA SIDO ENVIADO A LA DIAN AUN ***';

  SELECT Cufe,
         Qrdata,
         Validacion
  FROM Facturas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Cufe,
       Qrdata,
       Validacion;

    SUSPEND;
END
^

SET TERM ; ^
COMMIT WORK;

/***** MODULO REST SERVER FINAL                         ****/



/************************************************************/
/**** OTROS                                              ****/
/************************************************************/



UPDATE OR INSERT INTO IDENTIDADES (CODIGO, CODIGO2, NOMBRE, CODIGO_NE, CODIGO_SE, MODIFICADO)
                 VALUES ('48', 'PPT', 'PERMISO POR PROTECCION TEMPORAL', NULL, NULL, '2023-05-08');


COMMIT WORK;

/* FORMATO 1647 EXOGENA*/
UPDATE OR INSERT INTO MM_FORMATOS (CODIGO, NOMBRE, ENTINFO, CPT, TDOC, NID, DV, APL1, APL2, NOM1, NOM2, RAZ, DIR, DPTO, MUN, PAIS, EMAIL, VALOR1, VALOR2, VALOR3, VALOR4, VALOR5, VALOR6, VALOR7, VALOR8, VALOR9, VALOR0, VALOR10, VALOR11, VALOR12, VALOR13, VALOR14, VALOR15, VALOR16, VALOR17, VALOR18, VALOR19, VALOR20, VALOR21, VALOR22, VALOR23, VALOR24, VALOR25, VALOR26, TOTAL, IDEM, TDOCM, NITM, DVM, APL1M, APL2M, NOM1M, NOM2M, RAZM, INFORMAR, CON_INFORMADOS, NELEMENTO, NVALOR1, NVALOR2, NVALOR3, NVALOR4, NVALOR5, NVALOR6, NVALOR7, NVALOR8, NVALOR9, NVALOR0, NVALOR10, NVALOR11, NVALOR12, NVALOR13, NVALOR14, NVALOR15, NVALOR16, NVALOR17, NVALOR18, NVALOR19, NVALOR20, NVALOR21, NVALOR22, NVALOR23, NVALOR24, NVALOR25, NVALOR26, NIDEM, NCPT, REFERENCIA, REVISION, CADENA_ADICIONAL, EQUIVALENCIA, VALOR27, VALOR28, VALOR29, NVALOR27, NVALOR28, NVALOR29)
                           VALUES ('1647_V2', 'INFORMACION DE INGRESOS RECIBIDOS PARA TERCEROS', NULL, 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'S', 'S', 'N', 'S', 'S', 'S', 'S', 'S', 'N', 'N', 'ingresos', 'vtotal', 'ving', 'vret', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'con', 'INFORMACION DE INGRESOS RECIBIDOS PARA TERCEROS', 2, 'paist="169"', 'tdocm=tdoc2,nitm=nid2i,dvm=dv2i,razm=razi, apl1m=apl1i,apl2m=apl2i,nom1m=nom1i,nom2m=nom2i,dpto=cdpt,mun=cmcp', NULL, NULL, NULL, NULL, NULL, NULL)
                         MATCHING (CODIGO);

COMMIT WORK;


UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('2', 'Anulacion del Documento Soporte en Adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('3', 'Rebaja o descuento parcial o total')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('4', 'Ajuste de precio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('5', 'Otros')
                      MATCHING (CODIGO);


COMMIT WORK;

-----------
UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('SF1_TOTAL', 'AJUSTE DE INVENTARIO TOTAL SEGUN DOCUMENTO SF1', 'Ajuste de Inventario segun documento SF1. Recuerde sacar informe DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO para validar diferencias', 'EXECUTE PROCEDURE Pz_Conteo_Fisico(:Fecha_Corte,:A_Tipo,:B_Prefijo,:C_Numero,:Usuario);', 'S', 'GESTION', 'CIERRE INVENTARIO', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('SF1_PARCIA', 'AJUSTE DE INVENTARIO PARCIAL SEGUN DOCUMENTO SF1 (Solo referencias digitadas)', 'Ajuste de Inventario segun documento SF1. Recuerde sacar informe DIFERENCIAS EN INVENTARIO LUEGO DEL CONTEO FISICO PARCIAL', 'EXECUTE PROCEDURE Pz_Conteo_Fisico_P(:A_Tipo,:B_Prefijo,:C_Numero, :Usuario);', 'S', 'GESTION', 'CIERRE INVENTARIO', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

---------

-- Actualizar formula de promedio para prima

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DI015+SI050))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE   
  RESULT:=(ROUND((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DI015+SI050))*30))'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualizar formula de promedio para cesantias

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DCES+DI015+SI051)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DCES+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALA-TOTA_CONS+TOTA_SAREP)+(SA027-SA001+DV300)+SI100)/(TOTA_DCES+DI015+SI051))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE 
  RESULT:=(ROUND((((TOTA_SALA-TOTA_CONS+TOTA_SAREP)+(SA027-SA001+DV300)+SI100)/(TOTA_DCES+DI015+SI051))*30))'
WHERE (CODIGO = 'LD107') AND
      (NATIVO = 'S');

COMMIT WORK;
-------
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CTP070110', 'CONTRATOS EMPLEADOS', 'Contratos de empleados', 'SELECT (SELECT TRIM(Nombre_Empleado)
        FROM Fn_Nombre_Empleado(C.Codpersonal)) AS "NOMBRE EMPLEADO",
        C.Codpersonal Codigo,
        Inicio,
        Fin,
        Codtipocontrato Tipo,
        T.Nombre AS Contrato,
        Descripcion,
        Activo
FROM Contratacion C
JOIN Tipocontratos T ON (T.Codigo = C.Codtipocontrato)
ORDER BY 1', 'S', 'NOMINA', 'N', 'PERSONAL NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


-- SC_NOMINA074
-- Expresion del SA011 para incluir a fin de mes en descuentos
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_VAS', 'VALOR VACACIONES SA011 EN LA NOMINA EN EL MISMO MES - SA011', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''SA011'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;


-- Incluir el SA011 en el DT035

UPDATE RUBROS SET 
    FORMULA = 'IF(QUINCENA<2) THEN
RESULT:=(0)
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
RESULT:=(0)
ELSE
IF((ES_INTEGRA=0) AND ((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL+DV300)>(25*SMLV))) THEN
RESULT:=(ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG)+VA080+SA011+ANTE_VAS+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100))
ELSE
IF((ES_INTEGRA=1) AND (((((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL))*C05_S_INT)+HE038+DV997+DV300)>(25*SMLV))) THEN
RESULT:=(ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG)+VA080+SA011+ANTE_VAS+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100))
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=(ROUND((((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG))*C05_S_INT)+SA011+ANTE_VAS+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100))
ELSE
IF((SA027+ANTE_SAL)>=SMLV*4) THEN
RESULT:=(ROUND(((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI011-ANTE_DIVAN-DI018-DI009-DI020-ANTE_IEG))+SA011+ANTE_VAS+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100))
ELSE
RESULT:=(0)'
WHERE (CODIGO = 'DT035') AND
      (NATIVO = 'S');



COMMIT WORK;

-- SC_NOMINA075
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DIVAN', 'DIAS VACACIONES EN NOMINA MISMO PERIODO O MES - DI011', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI011'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;

-- SC_NOMINA076.sql
-- Se crean rubros PR04 para ajustar provisiones prima cesantias e intereses
EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR060' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR040'', ''AJUSTE VALOR PROVISION CESANTIAS'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR061' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR041'', ''AJUSTE VALOR PROVISION PRIMA'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

EXECUTE BLOCK AS
DECLARE VARIABLE Cod_Rubro CHAR(5);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codigo
      FROM Rubros
      WHERE Codigo = 'PR062' AND
            Nativo = 'S'
      INTO Cod_Rubro
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO RUBROS (CODIGO, NOMBRE, TIPO, PUBLICAR, NOVEDAD, ACUMULA, FORMULA, ORDEN, CODGRUPO_NE, COLUMNA, DATO_PILA, COLUMNA_PILA, COLUMNA_INICIO, COLUMNA_FIN, COLUMNA_TEXTO, DIAS_PENSION, DIAS_SALUD, DIAS_ARL, DIAS_CCF, DIAS_NOMINA, CONTROL_TIEMPO, NATIVO, REDONDEO)
                      VALUES (''PR042'', ''AJUSTE VALOR PROVISION INTERES A LAS CESANTIAS'', ''ADICION'', ''N'', ''S'', ''N'', ''VALOR'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'');';
    EXECUTE STATEMENT Vsql;
  END
END;

COMMIT WORK;

-- Se actualizan rubros provisiones incluyendo los ajuestes

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_INTEGRA=1 OR ES_ARL=1) THEN
  RESULT:=(0)
ELSE 
  RESULT:=(ROUND(SA028/C22_PR_CES)+PR040)'
WHERE (CODIGO = 'PR060') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_INTEGRA=1 OR ES_ARL=1) THEN
 RESULT:=(0)
ELSE
 RESULT:=(ROUND(((SAL_BASICO/30*(DI001+DI004+DI012))+SA028)/C21_PR_PRI)+PR041)'
WHERE (CODIGO = 'PR061') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'ROUND(PR060*C23_PR_INT)+PR042'
WHERE (CODIGO = 'PR062') AND
      (NATIVO = 'S');

COMMIT WORK;

-- Se incluyen los rubros PR04 en los esquemas que estan las provisiones

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR060' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR040'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR061' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR041'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE Vesquema CHAR(5);
DECLARE VARIABLE Vliquida CHAR(10);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Codesqnomina, Codliquidacion
      FROM Gruponomina G
      JOIN Rubros R ON (G.Codrubro = R.Codigo)
      WHERE G.Codrubro = 'PR062' AND
            R.Nativo = 'S'
      INTO Vesquema, Vliquida
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
        VALUES (''' || Vesquema || ''', ''PR042'', ''' || Vliquida || ''')';
    EXECUTE STATEMENT Vsql;
  END
END;
COMMIT WORK;

-- Actualiza rubro LD102 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(TOTA_HE_RE+HE038)
ELSE 
  RESULT:=(ROUND(((TOTA_HE_RE+HE038+SI102)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD102') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualiza rubro LD104 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(TOTA_SAPR+DV997)
ELSE 
  RESULT:=(ROUND(((TOTA_SAPR+DV997+SI104)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD104') AND
      (NATIVO = 'S');



COMMIT WORK;

-- Actualiza rubro LD106 incluyendo los dias suspensiones

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=(0) 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=(SAL_BASICO)
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30)<BASICO) THEN 
  RESULT:=(BASICO)
ELSE   
  RESULT:=(ROUND((((TOTA_SALAP-TOTA_CONSP+TOTA_SAREP)+(SA027-SA001+DV300)+SI101)/(TOTA_DIAS+DIAS_SLN+DI015+SI050+DI001+DI004+DI012))*30))'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');



COMMIT WORK;



-- PRESUPUESTO
UPDATE INFORMES SET 
    SQL_CONSULTA = 'SELECT  (P.RUBRO) AS CODRUBRO, (P.NOMBRE_RUBRO) AS "NOMBRE_RUBRO", TRIM(P.NIVEL) AS NIVEL,
COALESCE(P.PLANEADO_ANTERIOR, 0) AS "APROPIACION INICIAL",
COALESCE(P.CREDITO, 0) AS CREDITO,
COALESCE(P.CONTRA, 0) AS CONTRACREDITO,
COALESCE(P.ADICION, 0) AS ADICION,
COALESCE(P.REDUCCION, 0) AS REDUCCION,
COALESCE(P.PLANEADO_FINAL,0) AS  "APROPIACION VIGENTE",  
(IIF(P.APROBADO<0,0,P.APROBADO+P.LIBERACION_CDP)) AS "V/R. CDP - APROBADO",
(P.COMPROMISO+P.LIBERACION_RP) AS "V/R. RP - COMPROMISO",
(P.OBLIGACION) AS "V/R. OBLIGACION",
(P.PAGO) AS "V/R. PAGO",
(IIF(P.PAGO>=P.OBLIGACION,0,(P.OBLIGACION-P.PAGO))) AS "V/R. CXP",
(P.COMPROMISO_FINAL-P.OBLIGACION_FINAL) AS "GASTOS X COMPROMETER" ,
(P.APROBADO_FINAL-P.COMPROMISO_FINAL+(P.LIBERACION_CDP-P.LIBERACION_RP)) AS "CDP X COMPROMETER" ,
(P.PLANEADO_FINAL-P.APROBADO_FINAL-P.LIBERACION_CDP) AS "SALDO X EJECUTAR",
TRIM(T.NOMBRE) AS "NOMBRE RUBRO"
FROM GO_ARBOL_PRESUPUESTO(:PROYECTO,:FECHA_DESDE,:FECHA_HASTA) P
         INNER JOIN P_PPTO T ON (T.CODIGO=P.RUBRO)
WHERE P.RUBRO<>''NA''
ORDER BY 1'
WHERE (CODIGO = 'PPTO00010');

UPDATE INFORMES SET 
    SQL_CONSULTA = 'SELECT  (P.RUBRO) AS CODRUBRO, (P.NOMBRE_RUBRO) AS "NOMBRE RUBRO", TRIM(P.NIVEL) AS NIVEL,
COALESCE(P.PLANEADO_ANTERIOR, 0) AS "APROPIACION INICIAL",
COALESCE(P.CREDITO, 0) AS CREDITO,
COALESCE(P.CONTRA, 0) AS CONTRACREDITO,
COALESCE(P.ADICION, 0) AS ADICION,
COALESCE(P.REDUCCION, 0) AS REDUCCION,
COALESCE(P.PLANEADO_FINAL,0) AS  "APROPIACION VIGENTE",  
COALESCE(P.APROBADO_ANTERIOR,0) AS "CDP ANTERIOR",
COALESCE(P.COMPROMISO_ANTERIOR,0) AS "RP ANTERIOR",
COALESCE(P.OBLIGACION_ANTERIOR,0) AS "OBLIGACION ANTERIOR",
COALESCE(P.PAGO_ANTERIOR,0) AS "PAGO ANTERIOR",
(IIF(P.APROBADO<0,0,P.APROBADO+P.LIBERACION_CDP)) AS "V/R. CDP APROBADO",
(P.COMPROMISO+P.LIBERACION_RP) AS "V/R. RP COMPROMISO",
(P.OBLIGACION) AS "V/R. OBLIGACION",
(P.PAGO) AS "V/R. PAGO",
(IIF(P.PAGO>=P.OBLIGACION,0,(P.OBLIGACION-P.PAGO))) AS "V/R. CXP",
(P.APROBADO_FINAL+P.LIBERACION_CDP) AS "CDP ACUMULADO",
(P.COMPROMISO_FINAL+P.LIBERACION_RP) AS "RP  ACUMULADO",
(P.OBLIGACION_FINAL) AS "OBLIGACION  ACUMULADA",
(P.PAGO_FINAL) AS "PAGO ACUMULADO",    
(P.COMPROMISO_FINAL-P.OBLIGACION_FINAL+P.LIBERACION_RP) AS "GASTOS X COMPROMETER" ,
(P.APROBADO_FINAL-P.COMPROMISO_FINAL+(P.LIBERACION_CDP-P.LIBERACION_RP)) AS "CDP X COMPROMETER" ,
(P.PLANEADO_FINAL-P.APROBADO_FINAL-P.LIBERACION_CDP) AS "SALDO X EJECUTAR" ,
(IIF(P.PAGO_FINAL>=P.OBLIGACION_FINAL,0,(P.OBLIGACION_FINAL-P.PAGO_FINAL))) AS "CXP ACUMULADO",
P.LIBERACION_CDP,
P.LIBERACION_RP      
FROM GO_ARBOL_PRESUPUESTO(:PROYECTO,:FECHA_DESDE,:FECHA_HASTA) P
WHERE P.RUBRO<>''NA''
ORDER BY 1'
WHERE (CODIGO = 'PPTO00100');

COMMIT WORK;

---------960
/* CONTROL VACACIONES */

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('NOM040105', 'CONTROL DE VACACIONES POR EMPLEADO', 'Muestra los periodos vacacionales del empleado seleccionado, es importante que se hallan ingresado los dias habiles cada vez que se ha tomado vacaciones.', 'SELECT Empleado,
       (SELECT Nombre_Empleado
        FROM Fn_Nombre_Empleado(Empleado)) Nombre_Empleado,
       Desde,
       Hasta,
       Dias_Laborados,
       Dias_Periodo,
       Dias_Vac,
       Dias_Pendientes
FROM Pz_Control_Vacaciones(:Empleado, :Fecha_Corte)', 'S', 'NOMINA', 'N', 'MOVIMIENTO NOMINA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* HOJAS */

UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_001', 'GENERALIDADES - NOMINA', 'GENERALIDADES', 'ITEM, CODPERSONAL, CODRUBRO, CODCENTRO, NOTA, TEXTO, INICIO, FIN, VALOR, CODUSUARIO', 'https://n9.cl/pl_001_generalidades', 'CODPERSONAL, CODRUBRO, INICIO, FIN, VALOR, CODUSUARIO, ITEM')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_002', 'NOVEDADES - NOMINA', 'NOVEDADES', 'ITEM,CODPERSONAL, DIA, CODRUBRO, CODCENTRO, NOTA, VALOR, CODPRENOMINA', 'https://n9.cl/pl_002_novedades', 'ITEM, CODPERSONAL, DIA, CODRUBRO,  VALOR, CODPRENOMINA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_003', 'TERCEROS', 'TERCEROS', 'CODIGO, DV, NATURALEZA, NOM1, NOM2, APL1, APL2, EMPRESA, RAZON_COMERCIAL, DIRECCION, TELEFONO, MOVIL, EMAIL, GERENTE, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODACTIVIDAD, CODZONA', 'https://n9.cl/pl_003_terceros', 'CODIGO, NATURALEZA, DIRECCION, EMAIL, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODZONA, CODACTIVIDAD')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_004', 'EMPLEADOS - NOMINA', 'PERSONAL', 'CODIGO,NOM1, NOM2, APL1, APL2, DIRECCION, TELEFONO, MOVIL, EMAIL, NACIMIENTO, CODCARGO, CODPROFESION, CODCENTRO, CODSOCIEDAD, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODCOTIZANTE, CODSUBCOTIZANTE, CODENTIDAD, CODTIPOCUENTA, BANCO,SALARIO_INTEGRAL,CODTIPOPERIODO, ALTO_RIESGO', 'https://n9.cl/pl_004_empleados', 'CODIGO,NOM1, APL1, CODCARGO, CODPROFESION, CODCENTRO, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODSOCIEDAD, CODCOTIZANTE, CODSUBCOTIZANTE, SALARIO_INTEGRAL, CODTIPOPERIODO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_005', 'CARGOS - EMPLEADOS', 'CARGOS', 'CODIGO, NOMBRE', 'https://n9.cl/pl_005_cargos', 'CODIGO, NOMBRE')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_006', 'CONTRATOS - EMPLEADOS', 'CONTRATACION', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, DESCRIPCION, ACTIVO, CODUSUARIO', 'https://n9.cl/pl_006_contratos', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, ACTIVO, CODUSUARIO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_007', 'APORTES SEGURIDAD SOCIAL Y OTROS - EMPLEADOS', 'APORTES_PERSONAL', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA', 'https://n9.cl/pl_007_aportes', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_008', 'SALARIOS - EMPLEADOS', 'SALARIOS', 'Renglon, Codpersonal, Basico, Desde, Hasta,  Columna_Desde, Columna_Hasta', NULL, 'Renglon, Codpersonal, Basico, Desde, Hasta')
                   MATCHING (CODIGO);

COMMIT WORK;


COMMIT WORK;


-- SECTOR POR DEFECTO
UPDATE OR INSERT INTO SECTORES (CODIGO, NOMBRE, IMPRESORA)
                        VALUES ('NA', 'NO APLICA', 'NA')
                      MATCHING (CODIGO);


COMMIT WORK;


-----------------
--SEMAFOROS

EXECUTE BLOCK
AS
DECLARE VARIABLE Cant INTEGER;
BEGIN
  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'BLANCO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'BLANCO');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'VERDE'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'VERDE');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'AMARILLO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'AMARILLO');

  Cant = 0;
  SELECT COUNT(1)
  FROM Semaforos_Logistica
  WHERE TRIM(Codigo) = 'ROJO'
  INTO Cant;
  IF (Cant = 0) THEN
    UPDATE OR INSERT INTO Semaforos_Logistica (Desde, Hasta, Codigo)
    VALUES (0, 0, 'ROJO');
END;

COMMIT WORK;


-- FESTIVOS
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-01-01', 'Año nuevo')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-01-08', 'Día de los Reyes Magos')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-03-25', 'Día de San José')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-03-28', 'Jueves Santo')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-03-29', 'Viernes Santo')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-05-01', 'Día del Trabajo')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-05-13', 'Día de la Ascensión')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-06-03', 'Corpus Christi')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-06-10', 'Día del Sagrado Corazón de Jesús')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-07-01', 'Día de San Pedro y San Pablo')
                         MATCHING (FECHA);
--UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
--                           VALUES ('2024-07-20', 'Independencia de Colombia')
--                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-08-07', 'Batalla de Boyacá')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-08-19', 'Día de la Asunción de la Virgen María')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-10-14', 'Día de la Raza')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-11-04', 'Día de todos los santos')
                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-11-11', 'Independencia de Cartagena')
                         MATCHING (FECHA);
--UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
--                           VALUES ('2024-12-08', 'Día de la Inmaculada Concepción')
--                         MATCHING (FECHA);
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA, DESCRIPCION)
                           VALUES ('2024-12-25', 'Navidad')
                         MATCHING (FECHA);

COMMIT WORK;

UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45292,'AÑO NUEVO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45298,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45299,'DIA DE LOS REYES MAGOS');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45305,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45312,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45319,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45326,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45333,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45340,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45347,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45354,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45361,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45368,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45376,'DIA DE SAN JOSE');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45375,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45379,'JUEVES SANTO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45380,'VIERNES SANTO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45389,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45396,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45403,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45410,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45413,'DIA DEL TRABAJO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45417,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45424,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45425,'DIA DE LA ASCENCION');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45431,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45438,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45445,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45446,'CORPUS CHRISTI');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45452,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45453,'SAGRADO CORAZON');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45459,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45466,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45473,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45474,'DIA DE SAN PEDRO Y SAN PABLO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45480,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45487,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45493,'DIA DE LA INDEPENDENCIA');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45494,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45501,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45508,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45511,'DIA DE LA BATALLA DE BOYACA');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45515,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45522,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45523,'DIA DE LA ASUNCIÓN DE LA VIRGEN');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45529,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45536,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45543,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45550,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45557,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45564,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45571,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45578,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45579,'DIA DE LA RAZA');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45585,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45592,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45599,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45600,'DIA DE TODOS LOS SANTOS');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45606,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45607,'DIA DE LA INDEPENDENCIA DE CARTAGENA');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45613,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45620,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45627,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45634,'DIA DE LA INMACULADA CONCEPCION');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45641,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45648,'DOMINGO');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45651,'DIA DE NAVIDAD');
UPDATE OR INSERT INTO FESTIVOS (CODIGO, NOMBRE) VALUES(45655,'DOMINGO');

COMMIT WORK;


UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P96001', 'BLANQUEAR SALDOS DE INVENTARIO EN RUTINA LOGISTICA PARA ANALIZAR DE NUEVO', 'Permite blanquear los saldos en logistica para volver a realizar el analisis segun movimientos con corte al dia de ayer
REQUISITO: cerrar previamente dicha ventana', 'DELETE FROM RESUMEN_INVENTARIO;', 'N', 'GESTION', 'RUTINAS ESPECIALES', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


/* IMPUESTOS 2024 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Impuesto CHAR(5);
DECLARE VARIABLE V_Ano      INTEGER;
DECLARE VARIABLE V_Valor    NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa   NUMERIC(15,4);
DECLARE VARIABLE V_2024     INTEGER;
BEGIN
  FOR SELECT Codtipoimpuesto,
             Ano,
             Valor,
             Tarifa
      FROM Data_Impuestos
      WHERE Ano = 2023
      INTO V_Impuesto,
           V_Ano,
           V_Valor,
           V_Tarifa
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Data_Impuestos
    WHERE Codtipoimpuesto = :V_Impuesto
          AND Ano = 2024
    INTO V_2024;

    IF (V_2024 = 0) THEN
    BEGIN
      INSERT INTO Data_Impuestos
      VALUES (:V_Impuesto, 2024, :V_Valor, :V_Tarifa);
    END

  END
END;

COMMIT WORK;

/* RETENCIONES 2024 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Impuesto CHAR(5);
DECLARE VARIABLE V_Ano      INTEGER;
DECLARE VARIABLE V_Base     NUMERIC(17,4);
DECLARE VARIABLE V_Valor    NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa   NUMERIC(15,4);
DECLARE VARIABLE V_2024     INTEGER;
BEGIN
  FOR SELECT Codtiporetencion,
             Ano,
             Base,
             Valor,
             Tarifa
      FROM Data_Retenciones
      WHERE Ano = 2023
      INTO V_Impuesto,
           V_Ano,
           V_Base,
           V_Valor,
           V_Tarifa
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Data_Retenciones
    WHERE Codtiporetencion = :V_Impuesto
          AND Ano = 2024
    INTO V_2024;

    IF (V_2024 = 0) THEN
    BEGIN
      INSERT INTO Data_Retenciones
      VALUES (:V_Impuesto, 2024, :V_Base, :V_Valor, :V_Tarifa);
    END

  END
END;

UPDATE Data_Retenciones
SET Base = 188000
WHERE Base = 170000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 1271000
WHERE Base = 1145000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 7530000
WHERE Base = 6786000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 4330000
WHERE Base = 3902000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 2259000
WHERE Base = 2036000
      AND Ano = 2024;

COMMIT WORK;


/* CONSTANTES 2024 */
/*
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Constante CHAR(15);
DECLARE VARIABLE V_Ano       CHAR(4);
DECLARE VARIABLE V_Valor     NUMERIC(18,5);
DECLARE VARIABLE V_Texto     CHAR(100);
DECLARE VARIABLE V_2024      INTEGER;
BEGIN
  FOR SELECT Codconstante,
             Ano,
             Valor,
             Texto
      FROM Constantesvalor
      WHERE Ano = '2023'
            AND TRIM(Codconstante) <> 'C01_SMLV'
            AND TRIM(Codconstante) <> 'C02_AUXT'
      INTO V_Constante,
           V_Ano,
           V_Valor,
           V_Texto
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Constantesvalor
    WHERE Codconstante = :V_Constante
          AND Ano = '2024'
    INTO V_2024;

    IF (V_2024 = 0) THEN
    BEGIN
      INSERT INTO Constantesvalor
      VALUES (:V_Constante, '2024', :V_Valor, :V_Texto);
    END
  END
END; 

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Codpersonal CHAR(15);
DECLARE VARIABLE V_Constante   CHAR(15);
DECLARE VARIABLE V_Ano         CHAR(4);
DECLARE VARIABLE V_Valor       NUMERIC(18,5);
DECLARE VARIABLE V_Texto       CHAR(100);
DECLARE VARIABLE V_2024        INTEGER;
BEGIN
  FOR SELECT Codpersonal,
             Codconstante,
             Ano,
             Valor,
             Texto
      FROM Constantes_Personal
      WHERE Ano = '2023'
      INTO V_Codpersonal,
           V_Constante,
           V_Ano,
           V_Valor,
           V_Texto
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Constantes_Personal
    WHERE Codconstante = :V_Constante
          AND Codpersonal = :V_Codpersonal
          AND Ano = '2024'
    INTO V_2024;

    IF (V_2024 = 0) THEN
    BEGIN
      INSERT INTO constantes_personal
      VALUES (:v_codpersonal,:V_Constante, '2024', :V_Valor, :V_Texto);
    END
  END
END; 

COMMIT WORK;
*/
/* EXPRESIONES TOTA_ */
UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAREP') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SI101'';
DECLARE V_Rubro2       CHAR(5) = ''SA027'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SALAP') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV997'';
DECLARE V_Rubro2       CHAR(5) = ''SI105'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SACES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV997'';
DECLARE V_Rubro2       CHAR(5) = ''SI104'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAPR') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SI100'';
DECLARE V_Rubro2       CHAR(5) = ''SA027'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SALA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total cons cesan
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_CONS') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Dces
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = ''SI051'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_DCES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '-- Total Hces
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''HE038'';
DECLARE V_Rubro2       CHAR(5) = ''SI103'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_HCES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''HE038'';
DECLARE V_Rubro2       CHAR(5) = ''SI102'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_HE_RE') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Dias
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = ''SI050'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_DIAS') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAREC') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--TotalProVaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''PR063'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = '''';
DECLARE V_Rubro_Corte2 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_VAGA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--TotalDiasAnualesVaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = '''';
DECLARE V_Rubro_Corte2 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_VADIA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total cons corte prima
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_CONSP') AND
      (NATIVO = 'S');



COMMIT WORK;

/* VALIDACION VA012 PARA VACACIONES */
UPDATE RUBROS SET 
    FORMULA = 'IF ((VA012>0) AND (DI011>0)) THEN
     RESULT:=(ROUND((BASICO+PM001)/30*DI011))
ELSE
    RESULT:=(0)'
WHERE (CODIGO = 'SA011') AND
      (NATIVO = 'S');

COMMIT WORK;



/* SALARIO MINIMO 2024 */

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P80300', 'ACTUALIZA EL SALARIO MINIMO PARA 2024  (1.300.000)', 'Crea nuevos registros de salarios solo a los que tengan salario minimo', 'EXECUTE BLOCK
AS
DECLARE VARIABLE V_2024        INTEGER;
DECLARE VARIABLE V_Codpersonal VARCHAR(15);
DECLARE VARIABLE V_Sql         VARCHAR(500);
BEGIN
  FOR SELECT Codpersonal
      FROM Salarios
      WHERE Basico = 1160000
            AND Desde >= ''01/01/2023''
            AND Hasta IN (''31.12.2023'',''30.12.2023'')
            AND TRIM(COALESCE(Columna_Hasta, '''')) <> ''RETIRO''
      INTO V_Codpersonal
  DO
  BEGIN
    -- Valida si ya tiene datos para 2023
    V_Sql = ''SELECT COUNT(1)
    FROM Salarios
    WHERE Codpersonal = '''''' || V_Codpersonal || ''''''
          AND Desde = ''''01/01/2024'''''';

    EXECUTE STATEMENT(V_Sql)
        INTO V_2024;
    IF (V_2024 = 0) THEN
    BEGIN
      V_Sql = ''INSERT INTO Salarios (Codpersonal, Renglon, Basico, Desde, Hasta,
                            Columna_Desde)
      VALUES ('''''' || V_Codpersonal || '''''', (SELECT GEN_ID(Gen_Aportes, 1)
                               FROM Rdb$Database), 1300000, ''''01.01.2024'''',
              ''''31.12.2024'''', ''''VARIACION PERMANENTE'''')'';
      EXECUTE STATEMENT (V_Sql);
    END
  END
END', 'N', 'NOMINA', 'RUTINAS ESPECIALES', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

/* SC_NOMINA081 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Cant INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Conjuntos
  WHERE Codigo = 'PLANILLA83'
  INTO V_Cant;
  IF (V_Cant = 0) THEN
    UPDATE OR INSERT INTO Conjuntos (Codigo, Nombre)
    VALUES ('PLANILLA83', 'D.VAC');

  SELECT COUNT(1)
  FROM Conjuntos
  WHERE Codigo = 'PLANILLA87'
  INTO V_Cant;
  IF (V_Cant = 0) THEN
    UPDATE OR INSERT INTO Conjuntos (Codigo, Nombre)
    VALUES ('PLANILLA87', 'T.GVACA');
END; 

COMMIT WORK;


UPDATE RUBROS SET 
    CODCONJUNTO = 'PLANILLA83'
WHERE (CODIGO = 'PR050') AND
      (NATIVO = 'S') AND
      (CODCONJUNTO IS NULL); 

UPDATE RUBROS SET 
    CODCONJUNTO = 'PLANILLA87'
WHERE (CODIGO = 'PR051') AND
      (NATIVO = 'S') AND
      (CODCONJUNTO IS NULL);

COMMIT WORK;

/* MENSAJERO  */
EXECUTE PROCEDURE Mensaje_Global_Borra;
COMMIT WORK;
/*
EXECUTE PROCEDURE Mensaje_Global_Envia('ACTUALIZATE Y DISFRUTA DE LOS NUEVOS BENEFICIOS!

En ApoloSoft mejoramos cada dia nuestras aplicaciones, con el fin de safistacer 
a nuestro selecto grupo de clientes. Por eso los invitamos para que mantengan 
actualizado el programa y optimicen sus procesos internos. ya sabemos que dicha
actualizacion puede realizarla el mismo cliente, en cualquier momento del dia, 
gracias a la rutina de autoversion, que se ejecuta en el Mekano Server. 

Desde la semana pasada liberamos la version 9.67 cuyo mejora mas representativa 
es la generacion y envio de las notas de devolucion en el POS Electronico.
Actualmente contamos con la version 9.67.1 cuyo cambio mas importante consiste 
en poder transmitir documentos (facturas de venta y DE POS), que aun aparezcan 
en la bandeja de pendientes, como rechazados o con fecha diferente a la actual.

TAREA 1: Te invitamos a conocer dichos beneficios en el siguiente tutorial:
https://www.apolosoft.com/documentos/mekano/nv96.pdf (páginas 5, 9 y 58)

TAREA 2: Si tu programa ya esta en la NUEVA version 9.67, verifica que ya 
tengas la ultima 9.67.1, de lo contrario, en este computador ya mismo deberas
ejecutar su actualizacion rapida.  Ver video corto:
https://vimeo.com/apolosoft/autorapi-r

*SOMOS APOLOSOFT TU ALIADO TIC DESDE 1989*');

COMMIT WORK;
*/

/* FORMA DE PAGO 01 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_01 INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Formaspago
  WHERE Codigo = '01'
  INTO V_01;

  IF (:V_01 = 0) THEN
    UPDATE Formaspago
    SET Codigo = '01'
    WHERE Codigo = 'EF';
END;
COMMIT WORK;


/* Limite UVT*/

UPDATE Documentos
SET Limite_Uvt = 1
WHERE Limite_Uvt > 0 ;
COMMIT WORK;

/*MODIFICACION DE CAMPO VALOR POR VALOR_PAGO EN ARQUEOS*/


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00010', 'ARQUEO DE CAJA RESUMIDO POR MAQUINA - RANGO FECHA', NULL, 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) AS Forma_Pago,
       TRIM(R.Nombre_Forma_Pago) AS Nombre_Forma_Pago,
       TRIM(R.Computador) AS Maquina,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11
ORDER BY 3, 4', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00000', 'ARQUEO DE CAJA RESUMIDO - RANGO FECHA', NULL, 'SELECT * FROM FX_ARQUEO_RESUMEN(:_DESDE,:_HASTA)', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00100', 'ARQUEO DE CAJA DETALLADO POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - RANGO FECHA', 'Permite en un rango de fecha, generar el arqueo de caja agrupado por una máquina pos o por todas. 
Muestra formas de pago, documento, horario, cajero, vendedor y cliente', 'SELECT TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       R.Valor_Pago,
       R.Transacciones,
       TRIM(R.Forma_Pago) AS Forma_Pago,
       TRIM(R.Nombre_Forma_Pago) AS Nombre_Forma_Pago,
       TRIM(R.Computador) AS Maquina,
       C.Impreso,
       C.Hora,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO",
       TRIM(C.Codusuario) AS Codcajero,
       (SELECT TRIM(Nombre_Usuario)
        FROM Fn_Nombre_Usuario(C.Codusuario)) AS "NOMBRE CAJERO",
       TRIM(R.Vendedor) AS Vendedor,
       TRIM(R.Nombre_Vendedor) AS "NOMBRE VENDEDOR",
       TRIM(C.Codsede) AS Codsede,
       TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND C.Bloqueado = ''S''
ORDER BY 4, 1, 2, 3', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG040200', 'ARQUEO DE CAJA DETALLADO PARA MEDIO DE PAGO DIFERENTE A EFECTIVO - RANGO FECHA', 'Muestra por rango de fecha los medios de pago diferentes a efectivo, por un documento, maquina, 
Incluye datos descriptivos de transacciones como autorización pagos con tarjeta y número de cheque', 'SELECT trim(R.TIPO) AS TIPO, TRIM(R.PREFIJO) AS PREFIJO, TRIM(R.NUMERO) AS NUMERO, C.FECHA, TRIM(R.CODFORMASPAGO) AS CODFORMASPAGO,  TRIM(R.DOCUMENTO) AS DOCUMENTO,  C.COMPUTADOR,   
      TRIM(F.Nombre) AS NOM_FORMA_PAGO, R.Valor, C.CODTERCERO,  (SELECT TRIM(NOMBRE_TERCERO) FROM FN_NOMBRE_TERCERO (C.CODTERCERO)) AS "NOMBRE TERCERO", C.CODUSUARIO, C.CODVENDEDOR, C.CODBANCO, C.IMPRESO
      FROM Formaspago F
      JOIN Reg_Pagos R ON (R.Codformaspago = F.Codigo)
      JOIN Comprobantes C ON (R.Tipo = C.Tipo AND R.Prefijo = C.Prefijo AND R.Numero = C.Numero)
      JOIN Documentos D ON (C.Tipo = D.Codigo)
      WHERE TRIM(C.Tipo) LIKE :DOCUMENTO || ''%''
            AND ((C.Fecha >= :_Desde) and (C.fecha<=:_hasta)) 
            AND TRIM(C.Computador) LIKE :Computador || ''%''
            AND D.Grupo = ''VENTA''
            AND D.Es_Compra_Venta = ''S''
            AND D.SIGNO=''CREDITO''    AND TRIM(R.CODFORMASPAGO)<>''01'' AND TRIM(C.CODBANCO) LIKE :CAJA  AND C.BLOQUEADO=''S''         
ORDER BY 1,2,3,4,5,6,7', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG040101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha. Incluye cajeros, formas de pago y vendedor.
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_PAGO) AS Valor,
       TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       C.Impreso,
       C.Codsede,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''PERIODO: '' || EXTRACT(YEAR FROM C.Fecha) || '' / '' || RIGHT(EXTRACT(MONTH FROM C.Fecha) + 100, 2) AS "AÑO Y MES",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/*organizacion de cubos arqueos*/
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010103', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha y por una máquina o por todas. Incluye cajeros, formas de pago y vendedor.', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''AÑO: '' || EXTRACT(YEAR FROM C.Fecha) AS "AÑO",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       ''SEMANA: '' || LPAD(TRIM(EXTRACT(WEEK FROM C.Fecha)), 2, ''0'') AS Semana,
       ''DIA: '' || LPAD(TRIM(EXTRACT(DAY FROM C.Fecha)), 2, ''0'') AS Dia,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010101', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - HOY', 'Permite hacer el arqueo de caja a hoy y agrupado por cajeros. Incluye máquina pos y formas de pago', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(CURRENT_DATE, CURRENT_DATE) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010102', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - UNA FECHA', 'Permite hacer el arqueo de caja agrupado por cajeros según la fecha indicada. Incluye máquina pos y forma de pago', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:Fecha, :Fecha) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);


COMMIT WORK;


/* EXPEDIR CERTIFICADO */

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('TRICER022', 'EXPEDIR CERTIFICADO INGRESOS Y RETENCIONES -EXOGENA 2276_V4 (AG.2022/2023)', 'REQUISITO PREVIO: Configurar cuentas y renglones (campo item) desde Mekano Configuracion\Parametros Medios, Formato 2276_V4
Luego elegir el AG 2022 o 2023', 'R_TRI_CER_V4A.FR3', NULL, 'S', 'CONTABLE', 'A_PERIODO', 'N', 'TRIBUTARIOS EXPEDIR', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;

/*cambio de parametros por error en doble codlinea*/
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTP100511', 'PORTAFOLIO DE REFERENCIAS CON FOTO', 'Listado de productos y servicios con atributo Portafolio, incluye foto y descripción. El precio es tomado de la ventana Referencias
Indicar o buscar código de la Lista de Precios a utilizar', 'CT0310.FR3', NULL, 'S', 'PARAMETROS', 'CODLINEA', 'S', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTP100511B', 'PORTAFOLIO DE REFERENCIAS POR UNA LISTA DE PRECIOS Y CON FOTO..', 'Listado de productos y servicios con atributo Portafolio, incluye foto y descripción. 
El precio con o sin impuestos depende de la Lista de Precios seleccionada
Por una o todas las Lineas', 'CT0310B.FR3', NULL, 'S', 'PARAMETROS', 'LISTA_PRECIOS,CODLINEA', 'N', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 10)
                      MATCHING (CODIGO);


COMMIT WORK;

/* ENVIADO NOMINA E */
UPDATE Envios_Ne SET Enviado = 'N'
   WHERE COALESCE(Enviado, '') <> 'S'; 

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CONTABLE2G', 'INTERFACE DE GESTION -EXPORTA A EXCEL RUTINA HOJA CALCULO CONTABLE COPIAR PEGAR', 'Permite en un rango de fechas mes, exportar todo el movimiento contable para descargar a excel para luego subirlo a cualquier empresa copiando y pegando por hoja calculo a contable', 'SELECT *
FROM Pz_G_Contable2(:Fecha_Desde, :Fecha_Hasta)', 'S', 'GESTION', 'N', 'RUTINAS MIGRACIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_G_Contable2 (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Tipo_Doc       VARCHAR(5),
    Prefijo        VARCHAR(5),
    Numero         VARCHAR(10),
    Fecha          DATE,
    Cuenta         VARCHAR(30),
    Tercero        VARCHAR(15),
    Centro         VARCHAR(5),
    Detalle        VARCHAR(80),
    Debito         DOUBLE PRECISION,
    Credito        DOUBLE PRECISION,
    Base           DOUBLE PRECISION,
    Usuario        VARCHAR(10),
    Nombre_Tercero VARCHAR(163),
    Nombre_Centro  VARCHAR(80))
AS
DECLARE VARIABLE V_Cod_Tercero VARCHAR(15);
BEGIN
  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Codusuario,
             Codtercero
      FROM Comprobantes
      WHERE Fecha >= :Fecha_Desde
            AND Fecha <= :Fecha_Hasta
      INTO Tipo_Doc,
           Prefijo,
           Numero,
           Fecha,
           Usuario,
           V_Cod_Tercero
  DO
  BEGIN
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Detalle,
               Debito,
               Credito,
               Base
        FROM Reg_Contable
        WHERE Tipo = :Tipo_Doc
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND Codcuenta <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Detalle,
             Debito,
             Credito,
             Base
    DO
    BEGIN
      IF (Tercero IS NULL) THEN
        Tercero = COALESCE(Tercero, '');

      IF (Cuenta = '13050501') THEN
        Tercero = V_Cod_Tercero;

      Centro = COALESCE(Centro, '');
      Detalle = COALESCE(Detalle, '-');

      --MOSTRAMOS NOMBRE TERCERO
      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      --MOSTRAMOS NOMBRE CENTRO

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');
      SUSPEND;

    END
    Detalle = 'JUEGO DE INVENTARIOS';
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Debito,
               Credito,
               Base
        FROM Reg_Juego
        WHERE Tipo = :Tipo_Doc
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND Codcuenta <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Debito,
             Credito,
             Base

    DO
    BEGIN
      IF (Tercero IS NULL) THEN
        Tercero = COALESCE(Tercero, '');

      IF (Cuenta = '13050501') THEN
        Tercero = V_Cod_Tercero;

      Centro = COALESCE(Centro, '');
      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      --MOSTRAMOS NOMBRE CENTRO

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');

      SUSPEND;
    END
  END
END^

SET TERM ; ^

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010109', 'COMISIONES POR RECAUDO AGRUPADO POR TIPO', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por tipo.', 'VEN034.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010111', 'COMISIONES POR VENTA DETALLADO POR VENDEDOR', 'Muestra la relación de Comisiones de Ventas discriminando Vendedor detallado, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN020.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010101', 'COMISIONES POR VENTA', 'Muestra la relación de Comisiones de Ventas discriminando Vendedor, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN001.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010104', 'COMISIONES POR VENTA AGRUPADO POR TIPO - CON DETALLADO POR LINEAS', 'Muestra la relación de Comisiones por Tipo de Venta, discriminando Vendedor, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN002.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR,DOCUMENTO', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010102', 'COMISIONES POR VENTA AGRUPADO POR ZONAS', 'Muestra la relación de Comisiones de Ventas por Zonas, discriminando Vendedor, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN016.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010103', 'COMISIONES POR VENTA AGRUPADO POR LINEAS', 'Muestra la relación de Comisiones de Ventas  discriminando Vendedor por cada factura mostrando por cada Línea la Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN016-1.fr3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'S', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010112', 'COMISIÓN POR VENTAS AGRUPADO POR ZONAS DETALLO POR VENDEDOR', 'Muestra la relación de Comisiones de Ventas por Zonas, discriminando Vendedor detallado, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN021.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010106', 'COMISIONES POR RECAUDO', 'Muestra la relación de Comisiones por recaudo, Base Comisión y Valor Comisión.', 'VEN031.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010113', 'COMISIONES POR VENTA AGRUPADO POR LINEAS DETALLADO POR VENDEDOR', 'Muestra la relación de Comisiones de Ventas discriminando Vendedor detallado por cada factura mostrando por cada Línea la Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN022.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010114', 'COMISIONES POR VENTA AGRUPADO POR TIPO - DETALLADO POR LINEAS , VENDEDOR Y DOC', 'Muestra la relación de Comisiones por Tipo de Venta, discriminando Vendedor detallado, Base Comisión, Valor Comisión y Saldo pendiente por cada factura.', 'VEN023.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR,DOCUMENTO', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010110', 'RELACION DE RECAUDOS POR VENDEDOR', 'Muestra la relación de los recaudos realizados por vendedor, discriminando por un rango de fechas el total, retenciones, descuentos y valor neto por cada documento.', 'VEN010.fr3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR,DOCUMENTO', 'S', 'SALDOS DE VENDEDORES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010116', 'COMISIONES POR RECAUDO DETALLADO POR VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión y Valor Comisión. Detallado por vendedor.', 'VEN035.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010117', 'COMISIONES POR RECAUDO AGRUPADO POR ZONAS Y VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por zonas. Detallado por vendedor.', 'VEN036.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010118', 'COMISIONES POR RECAUDO AGRUPADO POR LINEAS POR VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por lineas. Detallado por vendedor.', 'VEN037.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010119', 'COMISIONES POR RECAUDO AGRUPADO POR TIPO Y VENDEDOR', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por tipo. Detallado por vendedor.', 'VEN038.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010105', 'RESUMEN COMISIONES POR VENTA', 'Muestra la relación de Comisiones por Venta por Vendedor, Base Comisión, Porcentaje, Valor Comisión y Saldo pendiente.', 'VEN020.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010108', 'COMISIONES POR RECAUDO AGRUPADO POR LINEAS', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por lineas.', 'VEN033.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010107', 'COMISIONES POR RECAUDO AGRUPADO POR ZONAS', 'Muestra la relación de Comisiones por recaudo, Base Comisión, Valor Comisión, agrupado por zonas.', 'VEN032.FR3', '', 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('VENG010100', 'COMISIONES POR VENTA AGRUPADO POR TIPO DOCUMENTO - RANGO FECHA', 'Por uno o por todos los vendedores. Requisito asignar previamente el porcentaje comision en venta desde la ventana Personal, % por cada línea o generica ''S'' automático por todas.', 'VEN029.FR3', NULL, 'S', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,VENDEDOR', 'N', 'COMISIONES', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Info_Vtas_Dev (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Clase             VARCHAR(15),
    Cantidad          DOUBLE PRECISION,
    Tipo              VARCHAR(5),
    Prefijo           VARCHAR(5),
    Numero            VARCHAR(10),
    Renglon           INTEGER,
    Bruto             DOUBLE PRECISION,
    Unitario          DOUBLE PRECISION,
    Descuento         DOUBLE PRECISION,
    Gravado           DOUBLE PRECISION,
    No_Gravado        DOUBLE PRECISION,
    Retencion         DOUBLE PRECISION,
    Gran_Total        DOUBLE PRECISION,
    Fecha             DATE,
    Referencia        VARCHAR(20),
    Linea             VARCHAR(5),
    Tercero           VARCHAR(15),
    Usuario           VARCHAR(10),
    Computador        VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Nombre_Linea      VARCHAR(80),
    Nombre_Tercero    VARCHAR(164),
    Nombre_Usuario    VARCHAR(80),
    Desde             DATE,
    Hasta             DATE,
    Totales           DOUBLE PRECISION,
    Codigo2           VARCHAR(20),
    Nombre2           VARCHAR(20),
    Ubicacion         VARCHAR(20),
    Costo             DOUBLE PRECISION,
    Precio            DOUBLE PRECISION,
    Rentabilidad      DOUBLE PRECISION,
    Codmarca          VARCHAR(20),
    Genero            VARCHAR(10),
    Nom_Marca         VARCHAR(80),
    Cod_Genero        VARCHAR(10),
    Nom_Genero        VARCHAR(80))
AS
DECLARE VARIABLE V_Signo VARCHAR(10);
BEGIN

  FOR SELECT P.Cantidad,
             P.Tipo,
             P.Prefijo,
             LPAD(TRIM(P.Numero), 10),
             P.Renglon,
             P.Bruto,
             P.Unitario,
             P.Descuento,
             P.Gravado,
             P.No_Gravado,
             P.Retencion,
             P.Fecha,
             P.Referencia,
             P.Linea,
             P.Tercero,
             P.Usuario,
             P.Computador,
             P.Nombre_Referencia,
             P.Nombre_Linea,
             P.Nombre_Tercero,
             P.Nombre_Usuario,
             P.Desde,
             P.Hasta,
             D.Signo

      FROM Fx_Auxiliar_Pos(:Fecha_Desde, :Fecha_Hasta) P
      JOIN Documentos D ON (P.Tipo = D.Codigo)
      WHERE D.Grupo = 'VENTA'
            AND D.Es_Compra_Venta = 'S'

      INTO Cantidad,
           Tipo,
           Prefijo,
           Numero,
           Renglon,
           Bruto,
           Unitario,
           Descuento,
           Gravado,
           No_Gravado,
           Retencion,
           Fecha,
           Referencia,
           Linea,
           Tercero,
           Usuario,
           Computador,
           Nombre_Referencia,
           Nombre_Linea,
           Nombre_Tercero,
           Nombre_Usuario,
           Desde,
           Hasta,
           V_Signo

  DO
  BEGIN

    IF (V_Signo = 'CREDITO') THEN
    BEGIN
      Clase = '+VENTAS';
      Totales = ((Cantidad * Bruto) + COALESCE(Gravado, 0) + COALESCE(No_Gravado, 0) - COALESCE(Retencion, 0) - (Cantidad * Descuento));
      Gran_Total = ((Bruto - Descuento) * Cantidad);
    END
    ELSE
    BEGIN
      IF (V_Signo = 'DEBITO') THEN
        Clase = '-DEVOLUCION';
      Totales = (((Cantidad * Bruto) + COALESCE(Gravado, 0) + COALESCE(No_Gravado, 0) - COALESCE(Retencion, 0) - (Cantidad * Descuento)) * -1);
      Gran_Total = ((Bruto - Descuento) * Cantidad);
    END
    --Buscar campos en referencias
    SELECT Codigo2,
           Nombre2,
           Ubicacion,
           Costo,
           Precio,
           Rentabilidad,
           Codmarca,
           Codgenero
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Codigo2,
         Nombre2,
         Ubicacion,
         Costo,
         Precio,
         Rentabilidad,
         Codmarca,
         Genero;

    --Buscar Nombre_Marca
    SELECT Nombre
    FROM Marcas
    WHERE Codigo = :Codmarca
    INTO Nom_Marca;

    --Buscar Campos Generos
    SELECT Codigo,
           Nombre
    FROM Generos
    WHERE Codigo = :Genero
    INTO Cod_Genero,
         Nom_Genero;

    SUSPEND;
  END
END^

SET TERM ; ^

COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Diario_Ventas (
    Fecha_ DATE,
    Tipo_  VARCHAR(5),
    Compu_ VARCHAR(20))
RETURNS (
    Tipo         VARCHAR(5),
    Prefijo      VARCHAR(5),
    Numero       VARCHAR(10),
    Renglon      INTEGER,
    Referencia   VARCHAR(20),
    Cantidad     DOUBLE PRECISION,
    Bruto        DOUBLE PRECISION,
    Descuento    DOUBLE PRECISION,
    Grvado       DOUBLE PRECISION,
    No_Gravado   DOUBLE PRECISION,
    Linea        VARCHAR(5),
    Usuario      VARCHAR(10),
    Computador   VARCHAR(20),
    Nombre_Linea VARCHAR(80),
    Tarifa       DOUBLE PRECISION,
    Fecha        DATE,
    Tesoreria    VARCHAR(10),
    Minimo       INTEGER,
    Maximo       INTEGER)
AS
DECLARE VARIABLE V_Personal VARCHAR(15);
DECLARE VARIABLE V_Maquina  VARCHAR(20);
DECLARE VARIABLE V_Min_Max  VARCHAR(25);
BEGIN
  V_Min_Max = '';
  FOR SELECT C.Codusuario,
             C.Computador,
             TRIM(C.Tipo),
             TRIM(C.Prefijo),
             TRIM(C.Numero),
             C.Fecha,
             D.Tesoreria

      FROM Comprobantes C
      JOIN Documentos D ON (C.Tipo = D.Codigo)
      WHERE C.Fecha = :Fecha_
            AND TRIM(C.Tipo) LIKE :Tipo_ || '%'
            AND TRIM(C.Computador) LIKE :Compu_ || '%'
            AND C.Bloqueado <> 'N'
            AND D.Grupo = 'VENTA'
            AND D.Es_Compra_Venta = 'S'
            AND D.Signo = 'CREDITO'
            AND D.M_Aplica <> 'S'
      ORDER BY 2, 3, 4, 5
      INTO Usuario,
           Computador,
           Tipo,
           Prefijo,
           Numero,
           Fecha,
           Tesoreria
  DO
  BEGIN
    -- TR_Inventario
    FOR SELECT Renglon,
               Codreferencia,
               Salida,
               Bruto * Salida,
               Descuento * Salida
        FROM Tr_Inventario
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero
        INTO Renglon,
             Referencia,
             Cantidad,
             Bruto,
             Descuento
    DO
    BEGIN

      -- Impuestos
      Grvado = 0;
      No_Gravado = 0;
      SELECT SUM(IIF(Ti.Gravado = 'S', Credito, 0)),
             SUM(IIF(Ti.Gravado = 'N', Credito, 0))
      FROM Reg_Impuestos Ri
      JOIN Tipoimpuestos Ti ON (Ri.Tipo_Impuesto = Ti.Codigo)
      WHERE Tipo = :Tipo
            AND Prefijo = :Prefijo
            AND Numero = :Numero
            AND Renglon = :Renglon
      INTO Grvado,
           No_Gravado;

      --Tarifa
      Tarifa = NULL;
      SELECT FIRST 1 Tarifa
      FROM Reg_Impuestos
      WHERE Tipo = :Tipo
            AND Prefijo = :Prefijo
            AND Numero = :Numero
            AND Renglon = :Renglon
            AND Tarifa > 0
      INTO Tarifa;
      Tarifa = COALESCE(Tarifa, 0);

      -- Linea
      Linea = NULL;
      Nombre_Linea = NULL;
      SELECT Codlinea
      FROM Referencias
      WHERE Codigo = :Referencia
      INTO Linea;

      SELECT Nombre
      FROM Lineas
      WHERE Codigo = :Linea
      INTO Nombre_Linea;

      -- Usuarios
      V_Personal = NULL;
      SELECT Codpersonal
      FROM Usuarios
      WHERE Codigo = :Usuario
      INTO V_Personal;
      V_Personal = COALESCE(V_Personal, '');

      -- Computador
      V_Maquina = NULL;
      SELECT Codigo2
      FROM Personal
      WHERE Codigo = :V_Personal
      INTO V_Maquina;

      IF ((TRIM(COALESCE(V_Maquina, '')) <> '') AND
          (Computador CONTAINING 'MIAMI')) THEN
      BEGIN
        UPDATE Comprobantes
        SET Computador = :V_Maquina
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero;
        Computador = V_Maquina;
      END

      IF (V_Min_Max <> TRIM(:Computador) || TRIM(:Prefijo)) THEN
      BEGIN
        -- Minimo y Maximo
        SELECT MIN(CAST(C.Numero AS INTEGER)),
               MAX(CAST(C.Numero AS INTEGER))
        FROM Comprobantes C
        JOIN Documentos D ON (C.Tipo = D.Codigo)
        WHERE C.Fecha = :Fecha_
              AND TRIM(C.Tipo) LIKE :Tipo_ || '%'
              AND C.Computador = :Computador
              AND C.Prefijo = :Prefijo
              AND C.Bloqueado <> 'N'
              AND D.Grupo = 'VENTA'
              AND D.Es_Compra_Venta = 'S'
              AND D.Signo = 'CREDITO'
              AND D.M_Aplica <> 'S'

        INTO Minimo,
             Maximo;
        V_Min_Max = TRIM(:Computador) || TRIM(:Prefijo);
      END

      SUSPEND;
    END
  END
END^

SET TERM ; ^

COMMIT WORK;

/* Conceptos NC y ND Anexo 1.9 */
UPDATE Notas_Cre
SET Nombre = 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio'
WHERE Codigo = '1';
UPDATE Notas_Cre
SET Nombre = 'Rebaja o descuento parcial o total'
WHERE Codigo = '3';
UPDATE Notas_Cre
SET Nombre = 'Ajuste de precio'
WHERE Codigo = '4';
UPDATE Notas_Cre
SET Nombre = 'Descuento comercial por pronto pago'
WHERE Codigo = '5';
UPDATE Notas_Cre
SET Nombre = 'Descuento comercial por volumen de ventas'
WHERE Codigo = '6';

UPDATE OR INSERT INTO Notas_Deb (Codigo, Nombre)
VALUES ('4', 'Otros');
COMMIT WORK;


UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_001', 'GENERALIDADES - NOMINA', 'GENERALIDADES', 'ITEM, CODPERSONAL, CODRUBRO, CODCENTRO, NOTA, TEXTO, INICIO, FIN, VALOR, CODUSUARIO', 'https://n9.cl/pl_001_generalidades', 'CODPERSONAL, CODRUBRO, INICIO, FIN, VALOR, CODUSUARIO, ITEM')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_002', 'NOVEDADES - NOMINA', 'NOVEDADES', 'ITEM,CODPERSONAL, DIA, CODRUBRO, CODCENTRO, NOTA, VALOR, CODPRENOMINA', 'https://n9.cl/pl_002_novedades', 'ITEM, CODPERSONAL, DIA, CODRUBRO,  VALOR, CODPRENOMINA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_003', 'TERCEROS', 'TERCEROS', 'CODIGO, DV, NATURALEZA, NOM1, NOM2, APL1, APL2, EMPRESA, RAZON_COMERCIAL, DIRECCION, TELEFONO, MOVIL, EMAIL, GERENTE, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODACTIVIDAD, CODZONA', 'https://n9.cl/pl_003_terceros', 'CODIGO, NATURALEZA, DIRECCION, EMAIL, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODZONA, CODACTIVIDAD')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_004', 'EMPLEADOS - NOMINA', 'PERSONAL', 'CODIGO,NOM1, NOM2, APL1, APL2, DIRECCION, TELEFONO, MOVIL, EMAIL, NACIMIENTO, CODCARGO, CODPROFESION, CODCENTRO, CODSOCIEDAD, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODCOTIZANTE, CODSUBCOTIZANTE, CODENTIDAD, CODTIPOCUENTA, BANCO,SALARIO_INTEGRAL,CODTIPOPERIODO, ALTO_RIESGO', 'https://n9.cl/pl_004_empleados', 'CODIGO,NOM1, APL1, CODCARGO, CODPROFESION, CODCENTRO, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODSOCIEDAD, CODCOTIZANTE, CODSUBCOTIZANTE, SALARIO_INTEGRAL, CODTIPOPERIODO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_005', 'CARGOS - EMPLEADOS', 'CARGOS', 'CODIGO, NOMBRE', 'https://n9.cl/pl_005_cargos', 'CODIGO, NOMBRE')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_006', 'CONTRATOS - EMPLEADOS', 'CONTRATACION', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, DESCRIPCION, ACTIVO, CODUSUARIO', 'https://n9.cl/pl_006_contratos', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, ACTIVO, CODUSUARIO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_007', 'APORTES SEGURIDAD SOCIAL Y OTROS - EMPLEADOS', 'APORTES_PERSONAL', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA', 'https://n9.cl/pl_007_aportes', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_008', 'SALARIOS - EMPLEADOS', 'SALARIOS', 'Renglon, Codpersonal, Basico, Desde, Hasta,  Columna_Desde, Columna_Hasta', NULL, 'Renglon, Codpersonal, Basico, Desde, Hasta')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_010', 'PRECIOS', 'PRECIOS', 'CODLISTA,CODREFERENCIA,PRECIO,MINIMO,MAXDESCUENTO', 'https://xurl.es/pl_010_precios', 'CODLISTA,CODREFERENCIA,PRECIO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_011', 'CUENTAS', 'CUENTAS', 'CODIGO,NOMBRE,NIVEL,PRESUPUESTO,TERCERO,ACTIVO,EMPLEADO,CENTRO,BASE,PORCENTAJE,CORRIENTE,EFE,ANALISIS_COSTOS', 'https://xurl.es/pl_011_cuentas', 'CODIGO,NOMBRE,NIVEL,TERCERO,ACTIVO,EMPLEADO,CENTRO,BASE,PORCENTAJE')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_012', 'PRESUPUESTO COMERCIAL - CONTABLE', 'INI_PRESUPUESTO', 'ITEM,DEBITO,CREDITO,CODCUENTA,CODCENTRO,CODPRESUPUESTO', NULL, 'ITEM,DEBITO,CREDITO,CODCUENTA,CODCENTRO,CODPRESUPUESTO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_013', 'REFERENCIAS - STOCK', 'REFERENCIAS_STOCKS', 'CODREFERENCIA,CODBODEGA,STOCK,CODTIPO,CODPREFIJO,CODSEDE,CODTERCERO', NULL, 'CODREFERENCIA,CODBODEGA,STOCK,CODTIPO,CODPREFIJO,CODSEDE,CODTERCERO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_009', 'REFERENCIAS - GASTOS Y/O SERVICIOS', 'REFERENCIAS', 'CODIGO,NOMBRE,CODLINEA,CODMEDIDA,COSTO,PRECIO,COD_ESQCONTABLE,COD_ESQIMPUESTO,COD_ESQRETENCION', 'https://xurl.es/pl_009_ref_ser', 'CODIGO,NOMBRE,CODLINEA,CODMEDIDA,COSTO,PRECIO,COD_ESQCONTABLE,COD_ESQIMPUESTO,COD_ESQRETENCION')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_009.1', 'REFERENCIAS - CAMPOS COMPLETOS', 'REFERENCIAS', 'CODIGO,CODIGO2,NOMBRE,NOMBRE2,UBICACION,UNIDAD_COMPRA,STOCK,TIEMPO,TIEMPO_MAXIMO,COSTO,PRECIO,PESO,ALTO,ANCHO,FONDO,MEDIDA_RASTREO,VALORIZA,COSTEA,SALDOS,ALTERNA,BALANZA,LOTE,SERIAL,CATEGORIA,PORTAFOLIO,ACTIVA,INTERES,GRADOS,VOLUMEN,RENTABILIDAD,SUBPARTIDA,CODLINEA,CODMEDIDA,COD_ESQIMPUESTO,COD_ESQRETENCION,COD_ESQCONTABLE,CODGENERO,CODMARCA,CODSECTOR,ES_ENSAMBLE,ES_PRODUCCION,ENSAMBLE_EDITABLE,SALUD', 'https://xurl.es/pl_009.1_ref_todo', 'CODIGO,NOMBRE,CODLINEA,CODMEDIDA,COD_ESQIMPUESTO,COD_ESQRETENCION,COD_ESQCONTABLE,CODGENERO,CODMARCA,CODSECTOR,VALORIZA,COSTEA,SALDOS')
                   MATCHING (CODIGO);
COMMIT WORK;

/*notas pos*/
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Existe_Doc INTEGER;
DECLARE VARIABLE V_Existe_Gen INTEGER;
BEGIN
  V_Existe_Doc = 0;
  V_Existe_Gen = 0;

  UPDATE OR INSERT INTO Prefijos (Codigo, Nombre, Modificado)
  VALUES ('NPOS', 'NOTAS CREDITO POS', '2024-06-25');

  SELECT COUNT(1)
  FROM Documentos
  WHERE TRIM(Codigo) = 'NC7'
  INTO V_Existe_Doc;

  IF (:V_Existe_Doc = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe, Limite_Uvt,
                            Valida_Trm, Conector, Radicacion, Escanear, Salud,
                            Tipo_Operacion, Desglose, Ruta_Fe)
    VALUES ('NC7', 'NOTA CREDITO POS',
            'Nota de Ajuste del Documento Equivalente Electronico',
            'Util para Manejo de Devoluciones o nota para disminución en las ventas de contado POS Electronico',
            'F_NPOS_GES_01A.FR3', 'F_NPOS_GES_01A.FR3', 'F_NPOS_GES_02A.FR3',
            'F_NCE_GES_01A.FR3', '', 'S', 'N', 'N', 'N', 'DEBITO', 'VENTA', 'S',
            'N', 'CONTADO', 'ENTRADA', 'N', 'N', 'S', 'S', 'S', 'S', 'S', 'N',
            'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'S',
            'N', 'N', 'N', 'S', 'S', 'N', 'S', 'S', 'S', NULL, NULL, NULL, NULL,
            'S', 'N', 0, 3000, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', '', 'S', 'N', 'N', 'N', 'NOTA CREDITO POS', '2024-06-27', 'N',
            'N', 'N', 'N', 'S', 0, 'N', 'N', 'N', 'N', 'N', NULL, 'N', NULL);

    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario, Notac,
                                Notam)
    VALUES ('NC7', 'NPOS', 'NA', '-', '-');

    SELECT COUNT(1)
    FROM Consecutivos
    WHERE Generador = 'PAR_95'
    INTO V_Existe_Gen;

    IF (:V_Existe_Gen = 0) THEN
      INSERT INTO Consecutivos (Coddocumento, Codprefijo, Item, Fecha,
                                Generador)
      VALUES ('NC7', 'NPOS', 26, '2024-05-01', 'PAR_95');
    ELSE
      INSERT INTO Consecutivos (Coddocumento, Codprefijo, Item, Fecha)
      VALUES ('NC7', 'NPOS', 26, '2024-05-01');

  END

END;
COMMIT WORK; 

UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('2', 'Anulacion del documento equivalente electronico');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('3', 'Rebaja o descuento parcial o total');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('4', 'Ajuste de precio');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('5', 'Otros');

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('NOM020155', 'TRANSFERENCIA NOMINA A CUENTA BANCOLOMBIA -EXPORTAR A ARCHIVO PLANO', 'Requisitos: 
En Tesorería campo titular, asignar A o C y cuenta empresa sin puntos ni comas (ej: C70505874552)
En Personal Asignar campos Entidad Bancaria, Tipo de Cuenta y Numero de cuenta.', 'WITH Vt_Saldos
AS (SELECT P.Codnomina,
           SUM(P.Adicion) AS Valor,
           COUNT(P.Codrubro) AS Cantidad
    FROM Planillas P
    INNER JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    WHERE P.Codnomina = :Cod_Nomina
          AND R.Codconjunto = ''T_PAGAR''
          AND COALESCE(TRIM(P1.Banco), '''') <> ''''
    GROUP BY P.Codnomina)

SELECT ''1'' || LPAD(TRIM(T.Codigo), 15, ''0'') || ''I'' || LPAD('''', 15, '' '') || ''225'' || RPAD(''NOMINA'', 10, '' '') || EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || RIGHT(TRIM(V.Codnomina), 2) || EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || LPAD(V.Cantidad, 6, 0) || LPAD(''0'', 17, ''0'') || LPAD(V.Valor, 15, 0) || ''00'' || LPAD(TRIM(SUBSTRING(B.Titular FROM 2)), 11, 0) ||
       CASE LEFT(B.Titular, 1)
         WHEN ''A'' THEN ''S''
         WHEN ''C'' THEN ''D''
         ELSE ''D''
       END || LPAD('''', 149, '' '') Plano

FROM Terceros T, Vt_Saldos V, Bancos B
WHERE Datos_Empresa = ''S''
      AND B.Codigo = :Banco

UNION ALL

SELECT ''6'' || RPAD(TRIM(Beneficiario), 15, '' '') || RPAD(TRIM(Nombre), 30, '' '') || LPAD(COALESCE(''0'' || TRIM(Banco), 0), 9, 0) || RPAD(COALESCE(TRIM(Cuenta), '' ''), 17, '' '') || '' '' || Tipo_Transaccion || LPAD(ROUND(Valor), 15, 0) || ''00'' || EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || RPAD(Referencia, 21, '' '') || '' '' || LPAD(0, 5, 0) || RPAD('' '', 15, '' '') || RPAD(Email, 80, '' '') || RPAD('' '', 15, '' '') || RPAD('' '', 27, '' '')
FROM (SELECT TRIM(P.Codpersonal) Beneficiario,
             TRIM(E.Nombre) Nombre,
             COALESCE(''0'' || TRIM(E.Codentidad), 0) Banco,
             TRIM(E.Banco) Cuenta,
             IIF(E.Codtipocuenta = ''C'', ''27'', ''37'') AS Tipo_Transaccion,
             SUM(ROUND(P.Adicion)) Valor,
             CURRENT_DATE Fecha,
             N.Nombre Referencia,
             IIF(E.Email CONTAINING ''@'', COALESCE(E.Email, '' ''), '''') Email

      FROM Planillas P
      JOIN Personal E ON (P.Codpersonal = E.Codigo)
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      JOIN Rubros R ON (P.Codrubro = R.Codigo)
      WHERE P.Codnomina = :Nomina
            AND R.Codconjunto = ''T_PAGAR''
            AND COALESCE(TRIM(E.Banco), '''') <> ''''
      GROUP BY 1, 2, 3, 4, 5, 7, 8, 9)', 'S', 'NOMINA', NULL, 'N', 'PAGOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* FE_DIAS */

UPDATE OR INSERT INTO SISTEMA (CODIGO, VALOR)
                       VALUES ('FE_DIAS', '60')
                     MATCHING (CODIGO);


/* QUITAR COMILLAS DE REFERENCIAS*/
UPDATE Referencias
SET Nombre = REPLACE(Nombre,'''', ' ');

COMMIT WORK;

/* VIGENCIA PARA ENVIAR CON CURRENT_DATE */
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('FACTURA', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('EXPORTACION', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('POS ELECTRONICO', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO', 5)
                        MATCHING (CODIGO);      
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO POS', 5)
                        MATCHING (CODIGO);                                          
COMMIT WORK;

/* CREACION EN GRUPOS DE INTERFACE A GESTION*/
UPDATE OR INSERT INTO GRUPOS (CODIGO, NOMBRE, VALIDA_TRANSITO)
                      VALUES ('RUTINAS MIGRACIONES GESTION', 'INTERFACE A GESTION', 'N')
                    MATCHING (CODIGO);


COMMIT WORK;

/* CREACION DE INFORMES PARA INTERFACEA GESTION*/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('GESTION1G', 'INTERFACE DE GESTON -EXPORTA A EXCEL RUTINA HOJA CALCULO GESTION COPIAR PEGAR', 'Permite en un rango de fechas mes, exportar todo el movimiento gestion para descargar a excel para luego subirlo a cualquier empresa copiando y pegando por hoja calculo a gestion', 'SELECT *
FROM Pz_Gestion_Gestion(:Fecha_Desde, :Fecha_Hasta)', 'S', 'GESTION', NULL, 'N', 'RUTINAS MIGRACIONES GESTION', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
/* CREACION SP PARA INTERFACE GESTION A GESTION*/

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Gestion_Gestion (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Tipo_Doc       VARCHAR(5),
    Prefijo_Doc    VARCHAR(5),
    Numero_Doc     VARCHAR(10),
    Fecha          DATE,
    Vence          DATE,
    Tercero        CHAR(15),
    Bodega         VARCHAR(5),
    Centro         VARCHAR(5),
    Categoria      VARCHAR(15),
    Lote           VARCHAR(20),
    Referencia     VARCHAR(20),
    Cantidad       DOUBLE PRECISION,
    Unitario       DOUBLE PRECISION,
    Descuento      DOUBLE PRECISION,
    Nota           VARCHAR(200),
    Nombre_Tercero VARCHAR(164))
AS
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
BEGIN

  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Vence,
             Codtercero
      FROM Comprobantes
      WHERE Fecha >= :Fecha_Desde
            AND Fecha <= :Fecha_Hasta
            AND Numero > '0'
      ORDER BY Numero
      INTO V_Tipo,
           V_Prefijo,
           Numero_Doc,
           Fecha,
           Vence,
           Tercero
  DO
  BEGIN
    --MOSTRAMOS NOMBRE TERCERO
    SELECT Nombre_Tercero
    FROM Fn_Nombre_Tercero(:Tercero)
    INTO Nombre_Tercero;
    Nombre_Tercero = COALESCE(Nombre_Tercero, '');

    Tipo_Doc = :V_Tipo;
    Prefijo_Doc = :V_Prefijo;

    FOR SELECT Codbodega,
               Codcentro,
               Codreferencia,
               Salida+Entrada,
               Codcategoria,
               Codlote,
               Unitario,
               Descuento,
               Nota
        FROM Tr_Inventario
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :Numero_Doc
        INTO Bodega,
             Centro,
             Referencia,
             Cantidad,
             Categoria,
             Lote,
             Unitario,
             Descuento,
             Nota

    DO
    BEGIN
      Referencia = TRIM(Referencia);
      SUSPEND;
    END

  END

END^

SET TERM ; ^

COMMIT WORK;

/* MODIFICACION SP API PARA CONSULTA FACTURAS FE*/

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Rest_Select_Datos_Fe (
    Tipo_    CHAR(5),
    Prefijo_ CHAR(5),
    Numero_  CHAR(10))
RETURNS (
    Cufe       CHAR(100),
    Qrdata     CHAR(2000),
    Validacion CHAR(50),
    Signature  VARCHAR(1000))
AS
DECLARE VARIABLE V_Cant       INTEGER;
DECLARE VARIABLE V_Encontrado INTEGER;
BEGIN
  V_Cant = 0;
  V_Encontrado = 0;
  WHILE (V_Cant < 2000000 AND V_Encontrado = 0) DO
  BEGIN

    SELECT COUNT(1)
    FROM Facturas
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Encontrado;
    V_Cant = V_Cant + 1;
  END
  IF (:V_Encontrado = 0) THEN
    EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_) || ' NO HA SIDO ENVIADO A LA DIAN AUN ***';

  SELECT Cufe,
         Qrdata,
         Validacion,
         Signature
  FROM Facturas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO Cufe,
       Qrdata,
       Validacion,
       Signature;

  SUSPEND;
END^

SET TERM ; ^

COMMIT WORK;

/* INFORMES DE RETENCION CON NATURALEZA */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('TRI004_JN', 'ANEXOS DE RETENCION EN LA FUENTE POR CUENTA Y NATURALEZA TERCERO', 'Anexos de retención en un rango de  cuentas, por uno o todos los terceros, discriminando personas naturales y jurídicas por un rango de fechas dado.', 'SELECT P.Cuenta,
       P.Nombre_Cuenta AS "NOMBRE CUENTA",
       P.Desde,
       P.Hasta,
       IIF(P.Tercero IS NULL, P.Empleado, P.Tercero) AS Tercero,
       IIF(P.Tercero IS NULL, P.Nombre_Empleado, P.Nombre_Tercero) AS "NOMBRE TERCERO",
       IIF(T.Naturaleza=''N'',''PERSONA NATURAL'',''PERSONA JURIDICA'') "NATURALEZA",
       SUM(P.Debito - P.Credito) AS Creditos,
       SUM(P.Base) AS Base
FROM Conta_Auxiliar(:_DESDE, :_HASTA) P
JOIN Terceros T ON (P.Tercero = T.Codigo)
WHERE (P.Saldo_Inicial = ''N'')
      AND ((P.Cuenta >= :Cuenta_Desde)
      AND (P.Cuenta <= :Cuenta_Hasta))
      AND (TRIM(IIF(P.Tercero IS NULL, P.Empleado, P.Tercero)) LIKE :Tercero)
GROUP BY 1, 2, 3, 4, 5, 6, 7
ORDER BY 1, 2, 7', 'S', 'CONTABLE', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D6A79733833160C0B1E3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797D2D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3833160C0B1E3130323D2D3A3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CD17E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E3B3A2C3B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E373E2C2B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796C3833160C0B1E3130323D2D3A2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C0C7E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1779723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D79797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79713833160C0B1E3C2D3A3B362B302C79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2D79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D78797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2779723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D77797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'TRIBUTARIOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020301', 'TOTAL RETENCIONES POR GRUPO', 'Relación de retenciones, agrupadas por Grupo Y Naturaleza. Muestra el concepto de retención, la base y el valor de la retención, por un rango de fecha dado.', 'SELECT F.Grupo,
       TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       SUM(F.Base) AS Base,
       SUM(F.Credito) AS Credito,
       SUM(F.Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7379743833160C0B1E382D2A2F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797D4E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C827F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1279723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0D79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0179723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020302', 'TOTAL RETENCIONES POR TERCERO', 'RelaciÃ³n de retenciones, agrupadas por Tercero y Naturaleza. Muestra el concepto de retenciÃ³n, la base y el valor de la retenciÃ³n, por un rango de fecha dado.', 'SELECT TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       TRIM(F.Tercero) || ''   '' || TRIM(F.Nombre_Tercero) AS Tercero,
       SUM(F.Base) AS Base,
       SUM(F.Credito) AS Credito,
       SUM(F.Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7479793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797B2B0D0A1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F79713916130B1A0D322D2A360B1A124E7D777909371A0745393A3939393939394F4F4F4E4F4F4F4F4F4F4F4F4B4B4A474B4E4B494B3B4A4B4F4F4F4B4F4F4F4F4F4F4C4F4D3A4C4F4C4F4F4B4F4F4F4F4F4F4F3B4F4F4F4F4F4F4B484B3C4946484C484B494E4B4C4A4D4B4A4B4B4B464A4B4B394F4A4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F79713916130B1A0D322D2A360B1A124F7D77790B371A0745393A3939393939394F4F4F4E4F4F4F4F4F4F4F4F4B4B4A474B4E4B494B3B4A4B4F4F4F4B4F4F4F4F4F4F4C4F4D3A4C4F4C4F4F4A4F4F4F4F4F4F4F3C4F4F4F4F4F4F4B484B3C4946484C484B494E4B4B4B4A4B4D4B464A4B4B394F4A4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7979703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797C297E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7C7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C737E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CF67F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0B79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CEE7F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7D7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020303', 'TOTAL RETENCIONES POR CUENTA', 'Relación de retenciones, agrupadas por Cuenta y Naturaleza. Muestra el concepto de retención, la base y el valor de la retención, por un rango de fecha dado.', 'SELECT TRIM(Cuenta) || ''   '' || Nombre_Cuenta AS Cuenta,
       TRIM(Retencion) || ''   '' || Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       SUM(Base) AS Base,
       SUM(Credito) AS Credito,
       SUM(Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7379733833160C0B1E3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797CD57E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C367E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0179723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0C79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0A79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020304', 'TOTAL RETENCIONES POR TIPO DE DOCUMENTO', 'Relación de retenciones, agrupadas por Tipo de Documento y Naturaleza. Muestra el concepto de retención, la base y el valor de la retención, por un rango de fecha dado.', 'SELECT TRIM(F.Tipo) || ''   '' || TRIM(D.Nombre) AS Documento,
       TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       SUM(F.Base) AS Base,
       SUM(F.Credito) AS Credito,
       SUM(F.Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Documentos D ON (F.Tipo = D.Codigo)
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D6D79703833160C0B1E3B303C2A323A312B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797CC67F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C4F7E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CF37F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0079723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('TRI004JN', 'ANEXOS DE RETENCION EN LA FUENTE POR CUENTA Y NATURALEZA TERCERO', 'Permite imprimir los anexos de retención en un rango de  cuentas, por uno o todos los terceros, discriminando personas naturales y jurídicas por un rango de fechas dado.', 'TRI0004_JN.FR3', NULL, 'S', 'CONTABLE', 'FECHA_INICIAL,FECHA_FINAL,CUENTA_DESDE,CUENTA_HASTA,TERCERO', 'N', 'TRIBUTARIOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020201', 'RELACION RETENCIONES EN COMPRA POR TERCERO', 'Muestra la relación de retenciones en Grupo Compra, agrupado por tercero, muestra por cada documento las retenciones y sus bases por un rango de fecha dado.', 'SELECT Grupo,
       IIF(Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       Tipo,
       Prefijo,
       Numero,
       Fecha,
       Tercero,
       Nombre_Tercero,
       Desde,
       Hasta,
       SUM(Base) AS Baseretefte,
       SUM(Retefuente) AS Retefuente,
       SUM(Baseriva) AS Basereteiva,
       SUM(Reteiva) AS Reteiva,
       SUM(Baseica) AS Baseica,
       SUM(Reteica) AS Reteica
FROM (SELECT F.Grupo,
             F.Tipo,
             F.Prefijo,
             LPAD(TRIM(F.Numero), 10) AS Numero,
             F.Fecha,
             TRIM(F.Tercero) AS Tercero,
             TRIM(F.Nombre_Tercero) AS Nombre_Tercero,
             Te.Naturaleza,
             F.Desde,
             F.Hasta,
             IIF(T.Clase = ''RETEFUENTE'', F.Base, 0) AS Base,
             IIF(T.Clase = ''RETEIVA'', F.Base, 0) AS Baseriva,
             IIF(T.Clase = ''RETEICA'', F.Base, 0) AS Baseica,
             IIF(T.Clase = ''RETEFUENTE'', F.No_Ligada, 0) AS Retefuente,
             IIF(T.Clase = ''RETEIVA'', F.Ligada, 0) AS Reteiva,
             IIF(T.Clase = ''RETEICA'', F.No_Ligada, 0) AS Reteica
      FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
      JOIN Tiporetenciones T ON (F.Retencion = T.Codigo)
      JOIN Terceros Te ON (F.Tercero = Te.Codigo)
      WHERE (F.Grupo = ''COMPRA''))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
ORDER BY 2, Fecha, Tipo, Prefijo, Numero, Tercero', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D6379743833160C0B1E382D2A2F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E2B362F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D5779723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2F2D3A3936353079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4679723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E312A323A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E393A3C373E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3579723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D79797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796B3833160C0B1E3130323D2D3A202B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C7C7E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D78797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E3B3A2C3B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D77797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E373E2C2B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D76797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0679723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D75797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E2D3A2B3A392A3A312B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1179723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D74797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796E3833160C0B1E3D3E2C3A2D3A2B3A36293E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0979723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D73797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2D3A2B3A36293E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3579723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D72797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3D3E2C3A363C3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D71797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2D3A2B3A363C3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D70797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020203', 'RELACION RETENCIONES EN COMPRA POR CUENTA', 'Muestra la relación de retenciones en Grupo Compra, agrupadas por cuentas y concepto de retención. Muestra el documento,la base, el crédito y debito, por un rango de fecha dado.', 'SELECT IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       F.Tipo,
       F.Prefijo,
       LPAD(TRIM(F.Numero), 10) AS Numero,
       TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       TRIM(F.Cuenta) || ''   '' || F.Nombre_Cuenta AS Cuenta,
       F.Base,
       F.Credito,
       F.Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Documentos D ON (F.Tipo = D.Codigo)
JOIN Terceros T ON (F.Tercero = T.Codigo)
WHERE (D.Grupo = ''COMPRA''
      AND D.Publica_Retenciones = ''S'')
ORDER BY 1, 6, 5, 2, 3, 4', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797A391E130C1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7F7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7679703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C277E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E2B362F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D6279723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2F2D3A3936353079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4479723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E312A323A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D79797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797CD57E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D78797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D77797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020202', 'RELACION RETENCIONES EN COMPRA POR CONCEPTO', 'Muestra la relación de retenciones en Grupo Compra, agrupadas por concepto de retención. Muestra el documento,la base, el crédito y debito, por tercero y por un rango de fecha dado.', 'SELECT T.Clase,
       IIF(Te.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       F.Fecha,
       F.Tipo,
       F.Prefijo,
       LPAD(TRIM(F.Numero), 10) AS Numero,
       F.Tercero,
       TRIM(F.Nombre_Tercero) AS "NOMBRE TERCERO",
       (T.Codigo || ''   '' || T.Nombre) AS "TIPO RETENCION",
       F.Base,
       F.Debito,
       F.Credito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Tiporetenciones T ON (F.Retencion = T.Codigo)
JOIN Terceros Te ON (F.Tercero = Te.Codigo)
WHERE (F.Grupo = ''COMPRA'')
ORDER BY 7, F.Fecha, F.Tipo, F.Prefijo, LPAD(TRIM(F.Numero), 10), F.Nombre_Tercero, F.Tercero', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797A391E130C1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7F7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7379743833160C0B1E3C333E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0379723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79743833160C0B1E393A3C373E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E2B362F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D5D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2F2D3A3936353079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D5D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E312A323A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D79797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796C3833160C0B1E3130323D2D3A2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CA37C79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D78797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796C3833160C0B1E2B362F302D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797C6B7D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D77797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D76797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D75797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D74797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* se sube sp gestion primario bloque con nota para comprobante*/

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Rest_Insert_Gestion_Primario_B (
    Tipo_     VARCHAR(5),
    Prefijo_  VARCHAR(5),
    Numero_   VARCHAR(10),
    Fecha_    DATE,
    Vence_    DATE,
    Tercero_  VARCHAR(15),
    Vendedor_ VARCHAR(15),
    Lista_    VARCHAR(5),
    Banco_    VARCHAR(15),
    Usuario_  VARCHAR(10),
    Nota_     VARCHAR(80),
    Detalle_  BLOB SUB_TYPE 1 SEGMENT SIZE 80)
AS
DECLARE VARIABLE V_Plazo          INTEGER;
DECLARE VARIABLE V_Renglon        INTEGER;
DECLARE VARIABLE V_Registro       INTEGER;
DECLARE VARIABLE V_Doc            VARCHAR(20);
DECLARE VARIABLE V_Descuento      NUMERIC(17,4);
DECLARE VARIABLE V_Bloqueado      VARCHAR(1);
DECLARE VARIABLE Detalle_Tr       VARCHAR(300);
DECLARE VARIABLE V_Centro         VARCHAR(5);
DECLARE VARIABLE V_Bodega         VARCHAR(5);
DECLARE VARIABLE V_Referencia     VARCHAR(20);
DECLARE VARIABLE V_Entrada        NUMERIC(17,4);
DECLARE VARIABLE V_Salida         NUMERIC(17,4);
DECLARE VARIABLE V_Unitario       NUMERIC(17,4);
DECLARE VARIABLE V_Porc_Descuento NUMERIC(17,4);
DECLARE VARIABLE V_Nota           VARCHAR(200);
BEGIN

  V_Doc = TRIM(Tipo_) || ' ' || TRIM(Prefijo_) || ' ' || TRIM(Numero_);

  /* Validamos que no exista el doc */
  SELECT COUNT(1)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN

    /* Valida que el tercero exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

    /* Valida que el vendedor exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Empleado(:Vendedor_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL EMPLEADO ' || TRIM(Vendedor_) || ' NO EXISTE ***';

    /* Valida que la caja o banco exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Banco(:Banco_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA CAJA o BANCO ' || TRIM(Banco_) || ' NO EXISTE ***';

    /* Valida que la lista de precios exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Lista(:Lista_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA LISTA DE PRECIOS ' || TRIM(Lista_) || ' NO EXISTE ***';

    /* Valida que el usuario exista */
    IF ((SELECT Registro
         FROM Rest_Valida_Usuario(:Usuario_)) = 0) THEN
      EXCEPTION Rest_Error '*** EL USUARIO ' || TRIM(Usuario_) || ' NO EXISTE ***';

    /* Hallamos plazo */
    IF (:Vence_ > :Fecha_) THEN
      V_Plazo = :Vence_ - :Fecha_;
    ELSE
      V_Plazo = 0;

    /*bloqueado char(1)
   si es positivo = R
   si es negativo = N*/
    IF (:Numero_ < 0) THEN
      V_Bloqueado = 'N';
    ELSE
      V_Bloqueado = 'R';

    /* Insertamos en COMPROBANTES */
    INSERT INTO Comprobantes (Tipo, Prefijo, Numero, Fecha, Plazo, Vence, Nota, Bloqueado, Codtercero, Codvendedor,
                              Codlista, Codbanco, Codusuario, Codescenario)
    VALUES (:Tipo_, :Prefijo_, :Numero_, :Fecha_, :V_Plazo, :Vence_, :Nota_, :V_Bloqueado, :Tercero_, :Vendedor_, :Lista_,
            :Banco_, :Usuario_, 'NA');

    -- Ciclo para leer cada detalle del blob
    FOR SELECT Detalle
        FROM Pz_Separa_Renglones(:Detalle_)
        INTO Detalle_Tr
    DO
    BEGIN -- Se disgrega cada renglon en los campos respectivos
      SELECT Centro,
             Bodega,
             Referencia,
             Entrada,
             Salida,
             Unitario,
             Porc_Descuento,
             Nota
      FROM Pz_Separa_Campos_Tr_Inventario(:Detalle_Tr)
      INTO V_Centro,
           V_Bodega,
           V_Referencia,
           V_Entrada,
           V_Salida,
           V_Unitario,
           V_Porc_Descuento,
           V_Nota;

      /* Valida que el centro exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Centro(:V_Centro)) = 0) THEN
        EXCEPTION Rest_Error '*** EL CENTRO ' || TRIM(V_Centro) || ' NO EXISTE ***';

      /* Valida que la bodega exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Bodega(:V_Bodega)) = 0) THEN
        EXCEPTION Rest_Error '*** LA BODEGA ' || TRIM(V_Bodega) || ' NO EXISTE ***';

      /* Valida que la referencia exista */
      IF ((SELECT Registro
           FROM Rest_Valida_Referencia(:V_Referencia)) = 0) THEN
        EXCEPTION Rest_Error '*** LA REFERENCIA ' || TRIM(V_Referencia) || ' NO EXISTE ***';

      V_Renglon = (SELECT GEN_ID(Gen_Tr_Inventario, 1)
                   FROM Rdb$Database);

      /* Insertamos TR_INVENTARIO */
      /*Calcular el descuento en base al porc_descuento*/
      V_Descuento = ((:V_Porc_Descuento / 100) * :V_Unitario);

      INSERT INTO Tr_Inventario (Tipo, Prefijo, Numero, Renglon, Codcentro, Codbodega, Codreferencia, Entrada, Salida,
                                 Unitario, Porcentaje_Descuento, Descuento, Nota, Codusuario)
      VALUES (:Tipo_, :Prefijo_, :Numero_, :V_Renglon, :V_Centro, :V_Bodega, :V_Referencia, :V_Entrada, :V_Salida,
              :V_Unitario, :V_Porc_Descuento, :V_Descuento, :V_Nota, :Usuario_);

      /* Con base al unitario(con o sin IVA) hallamos el BRUTO y demÃ¡s cÃ¡lculos */
      EXECUTE PROCEDURE Fx_Recalcula_Registro(:Tipo_, :Prefijo_, :Numero_, :V_Renglon);
    END
  END
  ELSE
    EXCEPTION Rest_Error '*** EL DOCUMENTO ' || TRIM(V_Doc) || ' YA EXISTE ***';
END^

SET TERM ; ^
COMMIT WORK;

UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_014A', 'COMPROBANTE REEMBOLSO GESTION PASO 1', 'COMPROBANTES', 'TIPO,PREFIJO,NUMERO,CODESCENARIO,FECHA,VENCE,CODTERCERO,CODVENDEDOR,CODBANCO,CODLISTA,NOTA', 'https://xurl.es/pl_014_reembolso_C', 'CODTERCERO,CODVENDEDOR,CODBANCO')
                   MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_014B', 'MOVIMIENTO REEMBOLSO GESTION PASO 2', 'TR_INVENTARIO', 'TIPO,PREFIJO,NUMERO,RENGLON,CODTERCERO,CODBODEGA,CODCENTRO,CODREFERENCIA,ENTRADA,UNITARIO,NOTA', 'https://xurl.es/pl_015_reembolso_D', 'CODTERCERO,CODCENTRO,CODBODEGA,CODREFERENCIA')
                   MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P90000', 'ORGANIZAR NUMERO DE CONTROL PARA VOLVER A EJECUTAR EL AUTOCONTENIDO', 'Luego el supervisor debera cerrar el programa totalmente, reiniciando el mekano server.
por ultimo, volver a ingresar a la empresa y tan solo esperar a que el proceso de autocontenido concluya', 'UPDATE COMPUTADORES SET ID_CONTENIDO=ID_CONTENIDO-1 WHERE COMPUTADOR=''SCRIPT'';', 'N', 'HERRAMIENT', 'RUTINAS ESTANDAR', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('CUEC010101', 'SALDOS DE CUENTAS AUXILIARES MES POR MES - SALDO INICIAL Y FINAL', 'Muestra los saldos de las cuentas auxiliares por mes con su respectivo saldo inicial, débitos, créditos y saldo final por rango de fechas y cuentas específico', 'SELECT P.Saldo_Inicial,
       ''FECHA:'' || P.Fecha || ''  • '' || TRIM(P.Tipo) || '' '' || TRIM(P.Prefijo) || '': '' || TRIM(P.Numero) AS "FECHA Y DOCUMENTO",
       TRIM(P.Cuenta) || '' : '' || TRIM(C.Nombre) AS Cuenta,
       TRIM(P.Centro) || '' : '' || TRIM(CE.Nombre) AS Codcentro,
       TRIM(T.Nombre) || '' : '' || TRIM(P.Tercero) AS "NOMBRE TERCERO",
       EXTRACT(YEAR FROM P.Fecha) AS "ANO",
       CASE EXTRACT(MONTH FROM P.Fecha)
         WHEN 1 THEN ''01.ENERO''
         WHEN 2 THEN ''02.FEBRERO''
         WHEN 3 THEN ''03.MARZO''
         WHEN 4 THEN ''04.ABRIL''
         WHEN 5 THEN ''05.MAYO''
         WHEN 6 THEN ''06.JUNIO''
         WHEN 7 THEN ''07.JULIO''
         WHEN 8 THEN ''08.AGOSTO''
         WHEN 9 THEN ''09.SEPTIEMBRE''
         WHEN 10 THEN ''10.OCTUBRE''
         WHEN 11 THEN ''11.NOVIEMBRE''
         WHEN 12 THEN ''12.DICIEMBRE''
       END AS Mes,
       P.Centro,
       P.Tercero,
       P.Empleado,
       P.Nombre_Centro,
       SUM(IIF(P.Saldo_Inicial = ''S'', 0, P.Debito)) AS Debito,
       SUM(IIF(P.Saldo_Inicial = ''S'', 0, P.Credito)) AS Credito,
       SUM(P.Base) AS Base,
       SUM(IIF(P.Saldo_Inicial = ''S'',
       (P.Debito - P.Credito), 0)) AS Inicial,
       SUM(P.Debito - P.Credito) AS Final
FROM Conta_Auxiliar(:_Desde, :_Hasta) P
JOIN Cuentas C ON (P.Cuenta = C.Codigo)
LEFT JOIN Terceros T ON (P.Tercero = T.Codigo)
LEFT JOIN Centros Ce ON (P.Centro = Ce.Codigo)
WHERE ((P.Cuenta >= :Cuenta_Desde)
      AND (P.Cuenta <= :Cuenta_Hasta))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
ORDER BY 3, 5', 'S', 'CONTABLE', :h00000000_00000000, 'N', 'AUXILIAR DE CUENTAS', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('LOGTER030', 'RELACION LOG DE CAMBIOS TABLA TERCEROS x USUARIO Y RANGO FECHA', 'Muestra cambios sobre la tabla Terceros.  Basta con indicar rango de fecha, por un usuario o un tercero o por todos', 'LOG_CAMBIOS_TER.FR3', NULL, 'S', 'HERRAMIENT', '_DESDE,_HASTA,CODUSUARIO,TERCERO', 'N', 'AUDITORIAS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LOGTER030', 'RELACION LOG DE CAMBIOS TABLA TERCEROS x USUARIO Y RANGO FECHA', 'Muestra cambios sobre la tabla Terceros.  Basta con indicar rango de fecha, por un usuario o un tercero o por todos', 'SELECT L.FECHA AS "FECHA Y HORA", L.*, CAST(replace(replace(SUBSTRING(TRIM(L.CAMBIO) FROM 1 FOR 1000),  ASCII_CHAR(10), '' '' ),  ASCII_CHAR(13), ''   -   '') as CHAR(3000))  AS CAMBIO
FROM LOG_TERCEROS L
WHERE ((cast(L.FECHA as date)>=:_DESDE) AND (cast(L.FECHA as date)<=:_HASTA)) AND (TRIM(L.USUARIO) LIKE :CODUSUARIO)  AND (TRIM(L.TERCERO) LIKE :TERCERO)', 'N', 'HERRAMIENT', NULL, 'N', 'AUDITORIAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

CREATE OR ALTER PROCEDURE X_Borra_Nominas
AS
BEGIN
  DELETE FROM Correos
  WHERE Codnomina IS NOT NULL;
  DELETE FROM Personal_Puestos;
  DELETE FROM Constantes_Personal;
  UPDATE Personal
  SET Codcotizante = NULL;
  UPDATE Personal
  SET Codesqnomina = 'NA';
  DELETE FROM Cotizantes;
  UPDATE Personal
  SET Codsubcotizante = NULL;
  DELETE FROM Subcotizantes;
  UPDATE Personal
  SET Codpuesto = NULL, Codpuesto2 = NULL;
  UPDATE Tipoaportes
  SET Codrubro_Personal = NULL, Codrubro_Empresa = NULL, Codcomponente = NULL;
  DELETE FROM Puestos;
  UPDATE Rubros
  SET Codconjunto = NULL
  WHERE Codconjunto LIKE 'COLUMNA%';
  DELETE FROM Conjuntos
  WHERE Codigo LIKE 'COLUMNA%';
  DELETE FROM Reg_Nomina;
  DELETE FROM Generalidades;
  DELETE FROM Log_Generalidades;
  DELETE FROM Reg_Novedades;
  DELETE FROM Novedades;
  DELETE FROM Prenominas;
  DELETE FROM Turnos;
  DELETE FROM Planillas;
  DELETE FROM Nominas;
  DELETE FROM Log_Nominas;
  DELETE FROM Expresiones;
  DELETE FROM Gruponomina;
  DELETE FROM Esqnomina
  WHERE Codigo <> 'NA';
  DELETE FROM Liquidaciones;
  DELETE FROM Rubros;
  DELETE FROM Log_Rubros;
  DELETE FROM Componentes;
  DELETE FROM Conjuntos;
  DELETE FROM Constantesvalor;
  DELETE FROM Constantes;
  DELETE FROM Puestos;
  DELETE FROM Cotizantes;
  DELETE FROM Subcotizantes;
  DELETE FROM Procesos
  WHERE Codigo = '_NOM_000';
  DELETE FROM Tipocontratos;
  DELETE FROM Tipocuentas;
  DELETE FROM Tipoperiodos;
  DELETE FROM Entidades;
  DELETE FROM Grupo_Ne;
END;


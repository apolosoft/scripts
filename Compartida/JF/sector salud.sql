UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo, :Prefijo, :Numero, ''GESTION'', :Receptor) S', 'S', 35, 'NOOVA', 'FACTURA', 'noov:DTOSectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (274, 'ges:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Apl1 "noov:Nvsal_pape",
       Apl2 "noov:Nvsal_sape",
       Nom1 "noov:Nvsal_pnom",
       Nom2 "noov:Nvsal_snom",
       '''' "noov:Nvsal_tusu",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       '''' "noov:Nvsal_npre",
       '''' "noov:Nvsal_ndip",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo, :Prefijo, :Numero, ''GESTION'', :Receptor)', 'S', 36, 'NOOVA', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuariosSalud', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (275, 'ges:salud_ben', 'DOC', '-- Sector Salud beneficiario

SELECT Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Nombre "noov:Nvsal_nomb",
       Apellido "noov:Nvsal_apel",
       Ciudad "noov:Nvsal_cciu",
       Direccion "noov:Nvsal_dire",
       Pais "noov:Nvsal_pais",
       ''Registraduria Nacional'' "noov:Nvent_nomb"
FROM Pz_Fe_Vector_Salud_Ben(:Tipo, :Prefijo, :Numero, ''GESTION'', :Renglon)
WHERE Tdoc <> ''NA''', 'S', 37, 'NOOVA', 'FACTURA', 'noov:DTOBeneficiario', '', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (276, 'ges:salud_ref', 'DOC', '-- Sector Salud Referenciados

SELECT Nume "noov:Nvdoc_nume",
       Cufd "noov:Nvdoc_cufd",
       Algr "noov:Nvdoc_algr",
       Femi "noov:Nvdoc_femi",
       Idto "noov:Nvdoc_idto",
       Ndoc "noov:Nvusu_ndoc",
       Oper "noov:Nvfor_oper",
       Reps "noov:Nvsal_cpsa",
       '''' "noov:Nvsal_naut"
FROM Pz_Fe_Vector_Salud_Ref(:Tipo, :Prefijo, :Numero, ''GESTION'', :Receptor)', 'S', 38, 'NOOVA', 'FACTURA', 'noov:lDocumentoReferencia', 'noov:DTODocumentoReferencia', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (468, 'con:sector_salud', 'DOC', '-- Sector Salud Contable Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Receptor) S', 'S', 35, 'NOOVAC', 'FACTURA', 'noov:DTOSectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (469, 'con:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Apl1 "noov:Nvsal_pape",
       Apl2 "noov:Nvsal_sape",
       Nom1 "noov:Nvsal_pnom",
       Nom2 "noov:Nvsal_snom",
       '''' "noov:Nvsal_tusu",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       '''' "noov:Nvsal_npre",
       '''' "noov:Nvsal_ndip",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Receptor)', 'S', 36, 'NOOVAC', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuariosSalud', NULL, 'con:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (470, 'con:salud_ben', 'DOC', '-- Sector Salud beneficiario

SELECT Tdoc "noov:Nvsal_tdoc",
       Ndoc "noov:Nvsal_ndoc",
       Nombre "noov:Nvsal_nomb",
       Apellido "noov:Nvsal_apel",
       Ciudad "noov:Nvsal_cciu",
       Direccion "noov:Nvsal_dire",
       Pais "noov:Nvsal_pais",
       ''Registraduria Nacional'' "noov:Nvent_nomb"
FROM Pz_Fe_Vector_Salud_Ben(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Renglon)
WHERE Tdoc <> ''NA''', 'S', 37, 'NOOVAC', 'FACTURA', 'noov:DTOBeneficiario', '', NULL, 'con:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (471, 'con:salud_ref', 'DOC', '-- Sector Salud Referenciados

SELECT Nume "noov:Nvdoc_nume",
       Cufd "noov:Nvdoc_cufd",
       Algr "noov:Nvdoc_algr",
       Femi "noov:Nvdoc_femi",
       Idto "noov:Nvdoc_idto",
       Ndoc "noov:Nvusu_ndoc",
       Oper "noov:Nvfor_oper",
       Reps "noov:Nvsal_cpsa",
       '''' "noov:Nvsal_naut"
FROM Pz_Fe_Vector_Salud_Ref(:Tipo, :Prefijo, :Numero, ''CONTABLE'', :Receptor)', 'S', 38, 'NOOVAC', 'FACTURA', 'noov:lDocumentoReferencia', 'noov:DTODocumentoReferencia', NULL, 'con:sector_salud')
                        MATCHING (ID);


COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    DESDE DATE,
    HASTA DATE,
    CODREFERIDO CHAR(15),
    ES_SALUD CHAR(1))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_BEN (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    BENEFICIARIO_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    NOMBRE CHAR(40),
    APELLIDO CHAR(40),
    CIUDAD CHAR(5),
    DIRECCION CHAR(80),
    PAIS CHAR(80))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_REF (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    NUME CHAR(15),
    CUFD CHAR(96),
    ALGR CHAR(11),
    FEMI CHAR(10),
    IDTO CHAR(5),
    NDOC CHAR(15),
    OPER CHAR(15),
    REPS CHAR(15))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_USU (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    APL1 CHAR(20),
    APL2 CHAR(20),
    NOM1 CHAR(20),
    NOM2 CHAR(20),
    MCON CHAR(5),
    COBE CHAR(5),
    NUMERO_CONTRATO CHAR(15),
    POLIZA CHAR(15),
    COPAGO NUMERIC(18,2),
    CUOTA_MODERADORA NUMERIC(18,2),
    PAGOS_COMPARTIDOS NUMERIC(18,2))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    DESDE DATE,
    HASTA DATE,
    CODREFERIDO CHAR(15),
    ES_SALUD CHAR(1))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
BEGIN
  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  -- datos del tercero
  SELECT Pagador

  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'N') THEN
    SELECT Desde,
           Hasta,
           Codreferido
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Desde,
         Hasta,
         Codreferido;
  ELSE
  BEGIN
    Codreferido = 'NA';
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    SELECT FIRST 1 Desde,
                   Hasta
    FROM Fe_Vector_Salud(:V_Resumen)
    INTO Desde,
         Hasta;
  END
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_BEN (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    BENEFICIARIO_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    NOMBRE CHAR(40),
    APELLIDO CHAR(40),
    CIUDAD CHAR(5),
    DIRECCION CHAR(80),
    PAIS CHAR(80))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Nom1           CHAR(20);
DECLARE VARIABLE V_Nom2           CHAR(20);
DECLARE VARIABLE V_Apl1           CHAR(20);
DECLARE VARIABLE V_Apl2           CHAR(20);
DECLARE VARIABLE V_Pais           CHAR(5);
DECLARE VARIABLE Es_Salud         CHAR(1);
BEGIN

  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  Ndoc = :Beneficiario_;
  -- datos del tercero
  SELECT Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2,
         Codmunicipio,
         Direccion,
         Codpais
  FROM Terceros T
  WHERE Codigo = :Beneficiario_
  INTO V_Tipo_Documento,
       V_Apl1,
       V_Apl2,
       V_Nom1,
       V_Nom2,
       Ciudad,
       Direccion,
       V_Pais;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  -- pais
  SELECT Nombre
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO Pais;

  Nombre = TRIM(:V_Nom1) || IIF(TRIM(:V_Nom2) = '', '', ' ' || TRIM(:V_Nom2));
  Apellido = TRIM(:V_Apl1) || IIF(TRIM(:V_Apl2) = '', '', ' ' || TRIM(:V_Apl2));

  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_REF (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    NUME CHAR(15),
    CUFD CHAR(96),
    ALGR CHAR(11),
    FEMI CHAR(10),
    IDTO CHAR(5),
    NDOC CHAR(15),
    OPER CHAR(15),
    REPS CHAR(15))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Empresa CHAR(15);
DECLARE VARIABLE Es_Salud  CHAR(1);
BEGIN

  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  -- Datos empresa
  SELECT Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Empresa;

  -- datos del tercero
  SELECT Pagador
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'S') THEN
  BEGIN
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    FOR SELECT Tipo,
               Prefijo,
               Numero,
               SUBSTRING(Fecha FROM 1 FOR 10),
               Codtercero,
               Reps
        FROM Fe_Vector_Salud(:V_Resumen) S
        INTO :V_Tipo,
             :V_Prefijo,
             :V_Numero,
             Femi,
             Ndoc,
             Reps

    DO
    BEGIN
      Nume = IIF(TRIM(:V_Prefijo) = '_', '', TRIM(:V_Prefijo)) || TRIM(:V_Numero);

      -- BUSCAR CUFE
      Cufd = '';
      SELECT Cufe
      FROM Facturas
      WHERE Tipo = :V_Tipo
            AND Prefijo = :V_Prefijo
            AND Numero = :V_Numero
      INTO Cufd;
      Cufd = COALESCE(Cufd, '');

      -- Tipo de operacion
      SELECT Tipo_Operacion
      FROM Documentos
      WHERE Codigo = :Tipo_
      INTO Oper;
      Algr = '';
      Algr = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN 'CUFE-SHA384'
               WHEN 'SS-CUDE' THEN 'CUDE-SHA384'
               WHEN 'SS-POS' THEN 'POS'
             END;

      Idto = '';
      Idto = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN '01'
               WHEN 'SS-CUDE' THEN '215'
               WHEN 'SS-POS' THEN '230'
             END;
      IF (:Ndoc <> :V_Empresa) THEN
        SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_VECTOR_SALUD_USU (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    MODULO_ CHAR(10),
    RECEPTOR_ CHAR(15))
RETURNS (
    REPS CHAR(15),
    TDOC CHAR(5),
    NDOC CHAR(15),
    APL1 CHAR(20),
    APL2 CHAR(20),
    NOM1 CHAR(20),
    NOM2 CHAR(20),
    MCON CHAR(5),
    COBE CHAR(5),
    NUMERO_CONTRATO CHAR(15),
    POLIZA CHAR(15),
    COPAGO NUMERIC(18,2),
    CUOTA_MODERADORA NUMERIC(18,2),
    PAGOS_COMPARTIDOS NUMERIC(18,2))
AS
DECLARE VARIABLE V_Pagador        CHAR(1);
DECLARE VARIABLE V_Tipo_Documento CHAR(5);
DECLARE VARIABLE V_Resumen        CHAR(10);
DECLARE VARIABLE V_Usuario        CHAR(15);
DECLARE VARIABLE Es_Salud         CHAR(1);
BEGIN

  -- Es salud
  SELECT Salud
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO Es_Salud;
  IF (Es_Salud = 'N') THEN
    EXIT;

  Ndoc = :Receptor_;
  -- datos del tercero
  SELECT Pagador,
         Codidentidad,
         Apl1,
         Apl2,
         Nom1,
         Nom2
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador,
       V_Tipo_Documento,
       Apl1,
       Apl2,
       Nom1,
       Nom2;

  SELECT Codigo2
  FROM Identidades
  WHERE Codigo = :V_Tipo_Documento
  INTO :Tdoc;

  IF (:V_Pagador = 'N') THEN
  BEGIN
    --Usuario
    SELECT Reps,
           Cod_Modalidad,
           Cod_Cobertura,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos
    FROM Fe_Vector_Salud_Comprobante(:Tipo_, :Prefijo_, :Numero_, :Modulo_)
    INTO Reps,
         Mcon,
         Cobe,
         Numero_Contrato,
         Poliza,
         Copago,
         Cuota_Moderadora,
         Pagos_Compartidos;
    SUSPEND;
  END
  ELSE
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

  FOR SELECT Reps,
             Cod_Modalidad,
             Cod_Cobertura,
             Numero_Contrato,
             Poliza,
             Copago,
             Cuota_Moderadora,
             Pagos_Compartidos,
             Codtercero
      FROM Fe_Vector_Salud(:V_Resumen) S
      INTO Reps,
           Mcon,
           Cobe,
           Numero_Contrato,
           Poliza,
           Copago,
           Cuota_Moderadora,
           Pagos_Compartidos,
           V_Usuario
  DO
  BEGIN
    -- Datos del usuario
    SELECT Codidentidad,
           Apl1,
           Apl2,
           Nom1,
           Nom2
    FROM Terceros
    WHERE Codigo = :V_Usuario
    INTO V_Tipo_Documento,
         Apl1,
         Apl2,
         Nom1,
         Nom2;

    SELECT Codigo2
    FROM Identidades
    WHERE Codigo = :V_Tipo_Documento
    INTO :Tdoc;

    SUSPEND;
  END

END^


SET TERM ; ^
COMMIT WORK;


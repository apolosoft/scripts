EXECUTE BLOCK (
    Presupuesto INTEGER = :Presupuesto)
AS
DECLARE VARIABLE V_Suma DOUBLE PRECISION;
BEGIN
  SELECT SUM(Debito + Credito)
  FROM Ini_Presupuesto
  WHERE Codpresupuesto = :Presupuesto
  INTO V_Suma;

  IF (V_Suma = 0) THEN
    DELETE FROM Ini_Presupuesto
    WHERE Codpresupuesto = :Presupuesto;

END
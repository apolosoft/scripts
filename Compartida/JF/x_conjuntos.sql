CREATE OR ALTER PROCEDURE X_Conjuntos
AS
BEGIN
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA03', 'DIAS AUS');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA21', 'HEN');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA22', 'HEDD');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA25', 'RDD');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA26', 'RND');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA31', 'T.DEVENG');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA30', 'T.OTRAS ADIC.');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA70', 'T.DEDUC.');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA60', 'T.OTRAS DEDUC.');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA01', 'BASICO');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA02', 'DIAS LAB');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA10', 'SALARIOS');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA11', 'AUX TRA');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA24', 'RN');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA20', 'HED');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA23', 'HEND');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA50', 'D.EPS');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA51', 'D.AFP');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA90', 'T.PRIMA');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA89', 'T.CESANTIAS');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('T_PAGAR', 'T PAGAR');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA27', 'RDDC');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA88', 'T. INT CESANTIAS');
  INSERT INTO Conjuntos (Codigo, Nombre)
  VALUES ('PLANILLA28', 'RNDC');
END;


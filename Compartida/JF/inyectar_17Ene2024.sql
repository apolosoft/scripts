EXECUTE BLOCK
AS
DECLARE VARIABLE V_Cant INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Conjuntos
  WHERE Codigo = 'PLANILLA83'
  INTO V_Cant;
  IF (V_Cant = 0) THEN
    UPDATE OR INSERT INTO Conjuntos (Codigo, Nombre)
    VALUES ('PLANILLA83', 'D.VAC');

  SELECT COUNT(1)
  FROM Conjuntos
  WHERE Codigo = 'PLANILLA87'
  INTO V_Cant;
  IF (V_Cant = 0) THEN
    UPDATE OR INSERT INTO Conjuntos (Codigo, Nombre)
    VALUES ('PLANILLA87', 'T.GVACA');
END; 

COMMIT WORK;


UPDATE RUBROS SET 
    CODCONJUNTO = 'PLANILLA83'
WHERE (CODIGO = 'PR050') AND
      (NATIVO = 'S') AND
      (CODCONJUNTO IS NULL); 

UPDATE RUBROS SET 
    CODCONJUNTO = 'PLANILLA87'
WHERE (CODIGO = 'PR051') AND
      (NATIVO = 'S') AND
      (CODCONJUNTO IS NULL);

COMMIT WORK;



UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);

COMMIT WORK;

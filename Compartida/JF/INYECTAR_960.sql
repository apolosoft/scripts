
/* IMPUESTOS 2024 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Impuesto CHAR(5);
DECLARE VARIABLE V_Ano      INTEGER;
DECLARE VARIABLE V_Valor    NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa   NUMERIC(15,4);
DECLARE VARIABLE V_2024     INTEGER;
BEGIN
  FOR SELECT Codtipoimpuesto,
             Ano,
             Valor,
             Tarifa
      FROM Data_Impuestos
      WHERE Ano = 2023
      INTO V_Impuesto,
           V_Ano,
           V_Valor,
           V_Tarifa
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Data_Impuestos
    WHERE Codtipoimpuesto = :V_Impuesto
          AND Ano = 2024
    INTO V_2024;

    IF (V_2024 = 0) THEN
    BEGIN
      INSERT INTO Data_Impuestos
      VALUES (:V_Impuesto, 2024, :V_Valor, :V_Tarifa);
    END

  END
END;

COMMIT WORK;

/* RETENCIONES 2024 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Impuesto CHAR(5);
DECLARE VARIABLE V_Ano      INTEGER;
DECLARE VARIABLE V_Base     NUMERIC(17,4);
DECLARE VARIABLE V_Valor    NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa   NUMERIC(15,4);
DECLARE VARIABLE V_2024     INTEGER;
BEGIN
  FOR SELECT Codtiporetencion,
             Ano,
             Base,
             Valor,
             Tarifa
      FROM Data_Retenciones
      WHERE Ano = 2023
      INTO V_Impuesto,
           V_Ano,
           V_Base,
           V_Valor,
           V_Tarifa
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Data_Retenciones
    WHERE Codtiporetencion = :V_Impuesto
          AND Ano = 2024
    INTO V_2024;

    IF (V_2024 = 0) THEN
    BEGIN
      INSERT INTO Data_Retenciones
      VALUES (:V_Impuesto, 2024, :V_Base, :V_Valor, :V_Tarifa);
    END

  END
END;

UPDATE Data_Retenciones
SET Base = 188000
WHERE Base = 170000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 1271000
WHERE Base = 1145000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 7530000
WHERE Base = 6786000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 4330000
WHERE Base = 3902000
      AND Ano = 2024;
UPDATE Data_Retenciones
SET Base = 2259000
WHERE Base = 2036000
      AND Ano = 2024;

COMMIT WORK;


/* VALIDACIONES IMPUESTOS */

UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (7, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Impuestos I
JOIN Tipoimpuestos T ON (I.Tipo_Impuesto = T.Codigo)
WHERE TRIM(I.Tipo) = '':TIPO''
      AND TRIM(I.Prefijo) = '':PREFIJO''
      AND TRIM(I.Numero) = '':NUMERO''', '00,01,02,03,04,21,22,23,24,25,26,ZY,ZZ', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (8, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Impuestos('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tipoimpuestos T ON (T.Codigo = I.Codtipoimpuesto)', '00,01,02,03,04,21,22,23,24,25,26,ZY,ZZ', 'CONTABLE')
                          MATCHING (ID);
COMMIT WORK;


/* SECTOR POR DEFECTO */

UPDATE OR INSERT INTO SECTORES (CODIGO, NOMBRE, IMPRESORA)
                        VALUES ('NA', 'NO APLICA', 'NA')
                      MATCHING (CODIGO);

UPDATE REFERENCIAS SET CODSECTOR='NA' WHERE CODSECTOR IS NULL;

COMMIT WORK;


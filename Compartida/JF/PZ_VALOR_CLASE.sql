SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Valor_Clase (
    Empleado_ VARCHAR(15),
    Desde_    DATE,
    Hasta_    DATE,
    Clase_    VARCHAR(30))
RETURNS (
    Valor DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nomina VARCHAR(5);
DECLARE VARIABLE V_Valor  INTEGER;
BEGIN
  Valor = 0;
  FOR SELECT Codigo
      FROM Nominas
      WHERE Fecha >= :Desde_
            AND Fecha < :Hasta_
      INTO V_Nomina
  DO
  BEGIN
    SELECT SUM(P.Adicion + P.Deduccion)
    FROM Planillas P
    JOIN Clases_Rubros R ON (P.Codrubro = R.Codrubro)
    WHERE R.Codclase = :Clase_
          AND R.Activo = 'S'
          AND P.Codnomina = :V_Nomina
          AND P.Codpersonal = :Empleado_
    INTO V_Valor;
    V_Valor = COALESCE(:V_Valor, 0);
    Valor = :Valor + V_Valor;
  END
  SUSPEND;
END^

SET TERM ; ^


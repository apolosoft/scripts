UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (269, 'ges:transporte1', 'DOC', '-- Vector Transporte

SELECT Atri "noov:Nvfac_atri",
       Natr "noov:Nvfac_natr",
       Vatr "noov:Nvfac_vatr"
FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon
      AND Natr IN (''01'',''02'')', 'S', 22, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (270, 'ges:transporte2', 'DOC', '-- Vector Transporte

SELECT Atri "noov:Nvfac_atri",
       Natr "noov:Nvfac_natr",
       Vatr "noov:Nvfac_vatr",
       Uatr "noov:Nvfac_uatr",
       Catr "noov:Nvfac_catr"
FROM Fe_Vector_Transporte(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon
      AND Natr = ''03''', 'S', 23, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);


COMMIT WORK;


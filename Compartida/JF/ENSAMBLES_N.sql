EXECUTE BLOCK
AS
DECLARE VARIABLE V_Ensamble CHAR(4);
DECLARE VARIABLE V_Doc      CHAR(5);
BEGIN
  FOR SELECT Codigo,
             Ensamble FOR Documentos
      INTO V_Doc,
           V_Ensamble
  DO
  BEGIN
    IF (TRIM(:V_Ensamble) IN ('S', 'AUTO') THEN
      UPDATE Documentos
      SET Valida_Saldos = 'S';
  END

  UPDATE Bodegas
  SET Ensamble = 'N';
  UPDATE Documentos
  SET Ensamble = 'N';

END;

COMMIT WORK; 
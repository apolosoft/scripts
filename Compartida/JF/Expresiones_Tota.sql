
/* EXPRESIONES TOTA_ */
UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAREP') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SI101'';
DECLARE V_Rubro2       CHAR(5) = ''SA027'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SALAP') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV997'';
DECLARE V_Rubro2       CHAR(5) = ''SI105'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SACES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV997'';
DECLARE V_Rubro2       CHAR(5) = ''SI104'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAPR') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SI100'';
DECLARE V_Rubro2       CHAR(5) = ''SA027'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SALA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total cons cesan
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_CONS') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Dces
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = ''SI051'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_DCES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '-- Total Hces
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''HE038'';
DECLARE V_Rubro2       CHAR(5) = ''SI103'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_HCES') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total HE_rE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''HE038'';
DECLARE V_Rubro2       CHAR(5) = ''SI102'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_HE_RE') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Dias
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = ''SI050'';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_DIAS') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD071'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD076'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_SAREC') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--TotalProVaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''PR063'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = '''';
DECLARE V_Rubro_Corte2 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_VAGA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--TotalDiasAnualesVaca
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DI015'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = '''';
DECLARE V_Rubro_Corte2 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_VADIA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = '--Total cons corte prima
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END'
WHERE (CODIGO = 'TOTA_CONSP') AND
      (NATIVO = 'S');

COMMIT WORK;

/* VALIDACION VA012 PARA VACACIONES */
UPDATE RUBROS SET 
    FORMULA = 'IF ((VA012>0) AND (DI011>0)) THEN
     RESULT:=(ROUND((BASICO+PM001)/30*DI011))
ELSE
    RESULT:=(0)'
WHERE (CODIGO = 'SA011') AND
      (NATIVO = 'S');

COMMIT WORK;



CREATE OR ALTER PROCEDURE X_Gruponomina_1
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'PR061', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'PR062', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'PR063', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD070', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD071', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD071', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD072', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE026', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA100', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA100', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA027', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA028', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA100', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI005', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV998', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI006', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI007', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI008', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE038', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD102', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SI050', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SI051', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV030', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD102', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS044', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI002', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI003', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI004', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI015', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE024', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE025', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV030', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA031', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT033', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT034', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT035', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SI050', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SI051', 'LQ0INI');

END;

CREATE OR ALTER PROCEDURE X_Gruponomina_2
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SI050', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SI051', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT046', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT046', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT046', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT998', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS040', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS041', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS042', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS043', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS045', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'TP999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'PR060', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'PR061', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'PR062', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'PR063', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD070', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD071', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD071', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD072', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE026', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA100', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA100', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA027', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA028', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA100', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI005', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV998', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI006', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI007', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI008', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE038', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD102', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT046', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE027', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV030', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD102', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA085', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS044', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA012', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA080', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA083', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA084', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA999', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA012', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA080', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA083', 'LQ5VAC');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_3
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA084', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA999', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA100', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD075', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD076', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD077', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD073', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD078', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA100', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT209', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT209', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT209', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT209', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT209', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT209', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT210', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD075', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD076', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD077', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD073', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE028', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD078', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA006', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV100', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV101', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV103', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV104', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV105', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV106', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV107', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV108', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA006', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA006', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT200', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA006', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT204', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT205', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT208', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT203', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT202', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT201', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA006', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV100', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_4
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV101', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV103', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV104', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV105', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV106', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV107', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV108', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA006', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA007', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT200', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA007', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT204', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT205', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT208', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT203', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT202', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT201', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA007', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA007', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA007', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT200', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA085', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT204', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT205', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT208', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT203', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT202', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT201', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI002', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI003', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI004', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE024', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE025', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV030', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA031', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT033', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT034', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT035', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT998', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS040', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_5
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS041', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS042', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS043', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS045', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'TP999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'PR060', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'PR061', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'PR062', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'PR063', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE026', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA027', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA028', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI005', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV998', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI006', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI007', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI008', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE038', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS044', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI002', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI003', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI004', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE024', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE025', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV030', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA031', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT033', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT034', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT035', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT998', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS040', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT210', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT210', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS041', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS042', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS043', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS045', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'TP999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'PR060', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'PR061', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'PR062', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'PR063', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE026', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_6
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA027', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA028', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI005', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV998', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT210', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI006', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI007', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI008', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE038', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS044', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT210', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI002', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI003', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI004', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE024', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE025', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV030', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA031', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT033', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT034', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT998', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS040', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS041', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS042', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS043', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS045', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'TP999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'PR060', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'PR061', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'PR062', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'PR063', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE026', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA027', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA028', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE027', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI005', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV998', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT210', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI006', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI007', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI008', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_7
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE038', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS044', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT211', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI010', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT039', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT046', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT033', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT046', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS049', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS014', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS024', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS010', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS059', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'HE028', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT034', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA085', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT039', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE027', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT033', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT034', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS046', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS047', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT211', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI010', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT039', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT039', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT046', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS049', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS014', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS024', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS010', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS059', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT033', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT034', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE028', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA035', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT039', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT046', 'LQ5VAC');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_8
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS046', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS047', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA007', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT211', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD999', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SI015', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SI015', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS032', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD052', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD052', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD052', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD050', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD051', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD052', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD050', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD052', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD051', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD050', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD051', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD052', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV100', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV101', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV102', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV103', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV104', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV105', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV106', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV107', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV108', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT200', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT204', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT205', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT206', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_9
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT208', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT203', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT202', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT201', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV100', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV101', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV102', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV103', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV104', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV105', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV106', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV107', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV108', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT200', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS030', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT204', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT205', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT208', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT203', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT202', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT201', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS030', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS030', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS030', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT200', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS030', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT204', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT205', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT208', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT203', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT202', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT201', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI002', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI003', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI004', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI015', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE024', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE025', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_10
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV030', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA031', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT033', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT034', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT035', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT998', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS040', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS041', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS042', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS043', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS045', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'TP999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'PR060', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'PR061', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'PR062', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'PR063', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD070', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD071', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD071', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD072', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE026', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA100', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA100', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT211', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT211', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT211', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT212', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT212', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV209', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV209', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV209', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV209', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV209', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV210', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV210', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV210', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV210', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV210', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV210', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV997', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV997', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV997', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV997', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV997', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV997', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS030', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS031', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS031', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_11
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS031', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS031', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS031', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS031', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS032', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS032', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS032', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS032', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS032', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS038', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS038', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS038', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS038', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS038', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS038', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS013', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS013', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS013', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS013', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS013', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS013', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS033', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS033', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS033', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS033', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS033', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS033', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS035', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS035', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS035', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS035', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS035', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS035', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS037', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS037', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS037', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS037', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS037', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS037', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS034', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS034', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS034', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS034', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS034', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_12
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS034', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS039', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS039', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS039', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS039', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS039', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS039', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SI015', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD053', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD053', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD053', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD079', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'LD079', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'LD079', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE027', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'HE028', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE027', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'HE028', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE027', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV040', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV040', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV040', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV040', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV040', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV040', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA032', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA032', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA032', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA032', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA032', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA032', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI012', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI012', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI012', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS206', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_13
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SI050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SI051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SI052', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SI015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SI050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SI051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SI052', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS060', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS060', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS060', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS060', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS060', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS060', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI018', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI018', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI018', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI018', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI018', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI018', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA035', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA035', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI010', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV040', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI010', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV040', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI010', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV040', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA002', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA002', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA002', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA020', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_14
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA008', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA008', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA008', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA008', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA008', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA008', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SI102', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SI102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SI102', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SI102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SI102', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SI102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'IN001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'IN001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'IN001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'IN001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'IN001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'IN001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI015', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI015', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI023', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_15
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI023', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT035', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT035', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT035', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI015', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS050', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DI010', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT039', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT212', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT212', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT212', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT212', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT213', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT213', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT213', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT213', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT213', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT213', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT214', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT214', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT214', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT214', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT214', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT214', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT215', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT215', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT215', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT215', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT215', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT215', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT216', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT216', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT216', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT216', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT216', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT216', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT217', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT217', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT217', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT217', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT217', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT217', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT218', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT218', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT218', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT218', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT218', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT218', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_16
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT219', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT219', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT219', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT219', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT219', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT219', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT220', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT220', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT220', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT220', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT220', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT220', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'HE028', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA003', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV101', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV101', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV102', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV103', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS010', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS051', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA027', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA028', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA100', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA012', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA080', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA083', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA084', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA999', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA100', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD075', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD076', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD077', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD073', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD078', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI005', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV998', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI006', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI007', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DI008', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'HE038', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'LD102', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD102', 'LQ2PRI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV030', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD102', 'LQ3CES');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS044', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_17
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI002', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI003', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI004', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DI015', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE023', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE024', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'HE025', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV030', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA031', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT033', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT034', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT035', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT998', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS040', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS041', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS042', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS043', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS045', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'TP999', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'PR060', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV103', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DI010', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT039', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS012', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS014', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS049', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS014', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS024', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS012', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS022', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS010', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS020', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS011', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS021', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS059', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS024', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS046', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS047', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS049', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS059', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_18
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS046', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS047', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DI010', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT039', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV104', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA003', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA003', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA003', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA003', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV104', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV105', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV105', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV106', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV106', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV107', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV107', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV108', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV108', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV109', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV109', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV109', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV109', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV109', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV109', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV110', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV110', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV110', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV110', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV110', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV110', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV100', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV100', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV200', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV200', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV200', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV200', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV200', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV200', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV201', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV201', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV201', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV201', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_19
AS
BEGIN

  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV201', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV201', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV202', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV202', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV202', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV202', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV202', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV202', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV203', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV203', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV203', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV203', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV203', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV203', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV204', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV204', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV204', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV204', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV204', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV204', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV205', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV205', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV205', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV205', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV205', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV205', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV206', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV207', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV208', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DV208', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DV208', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DV208', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DV208', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DV208', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DV209', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS010', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS012', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS014', 'LQ9DEF');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_20
AS
BEGIN

  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS024', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT046', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS046', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS047', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS049', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS059', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DI010', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT039', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA003', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS010', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS011', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS012', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS014', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS020', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS021', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS022', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS024', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS046', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS047', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS049', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS059', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS206', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS207', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS050', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS051', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS050', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS051', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'LD051', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'LD051', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'LD051', 'LQ4INT');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA001', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA001', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA002', 'LQ1NOM');
END;

CREATE OR ALTER PROCEDURE X_Gruponomina_21
AS
BEGIN
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS015', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS016', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS018', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA002', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS016', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS018', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA002', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS015', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS016', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS018', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA002', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS016', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS018', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA002', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS016', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS017', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS018', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS019', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA002', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS015', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS016', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS017', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS018', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS019', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SI052', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SI052', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SI052', 'LQ0INI');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SS009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SS009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SS009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SS009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SS009', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SS009', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA026', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA026', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA026', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'VA100', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT035', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'VA100', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'DT036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'VA100', 'LQ5VAC');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'DT036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'DT036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'DT036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'DT036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'DT036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM1', 'SA036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SA036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO1', 'SA036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SA036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN1', 'SA036', 'LQ1NOM');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EVEN2', 'SA036', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SI015', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SI050', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SI051', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EADM2', 'SI052', 'LQ9DEF');
  INSERT INTO Gruponomina (Codesqnomina, Codrubro, Codliquidacion)
  VALUES ('EPRO2', 'SI015', 'LQ9DEF');
END;


EXECUTE BLOCK
AS
DECLARE VARIABLE V_Existe_Doc INTEGER;
DECLARE VARIABLE V_Existe_Gen INTEGER;
BEGIN
  V_Existe_Doc = 0;
  V_Existe_Gen = 0;

  UPDATE OR INSERT INTO Prefijos (Codigo, Nombre, Modificado)
  VALUES ('NPOS', 'NOTAS CREDITO POS', '2024-06-25');

  SELECT COUNT(1)
  FROM Documentos
  WHERE TRIM(Codigo) = 'NC7'
  INTO V_Existe_Doc;

  IF (:V_Existe_Doc = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe, Limite_Uvt,
                            Valida_Trm, Conector, Radicacion, Escanear, Salud,
                            Tipo_Operacion, Desglose, Ruta_Fe)
    VALUES ('NC7', 'NOTA CREDITO POS',
            'Nota de Ajuste del Documento Equivalente Electronico',
            'Util para Manejo de Devoluciones o nota para disminución en las ventas de contado POS Electronico',
            'F_NPOS_GES_01A.FR3', 'F_NPOS_GES_01A.FR3', 'F_NPOS_GES_02A.FR3',
            'F_NCE_GES_01A.FR3', '', 'S', 'N', 'N', 'N', 'DEBITO', 'VENTA', 'S',
            'N', 'CONTADO', 'ENTRADA', 'N', 'N', 'S', 'S', 'S', 'S', 'S', 'N',
            'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'S',
            'N', 'N', 'N', 'S', 'S', 'N', 'S', 'S', 'S', NULL, NULL, NULL, NULL,
            'S', 'N', 0, 3000, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', '', 'S', 'N', 'N', 'N', 'NOTA CREDITO POS', '2024-06-27', 'N',
            'N', 'N', 'N', 'S', 0, 'N', 'N', 'N', 'N', 'N', NULL, 'N', NULL);

    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario, Notac,
                                Notam)
    VALUES ('NC7', 'NPOS', 'NA', '-', '-');

    SELECT COUNT(1)
    FROM Consecutivos
    WHERE Generador = 'PAR_95'
    INTO V_Existe_Gen;

    IF (:V_Existe_Gen = 0) THEN
      INSERT INTO Consecutivos (Coddocumento, Codprefijo, Item, Fecha,
                                Generador)
      VALUES ('NC7', 'NPOS', 26, '2024-05-01', 'PAR_95');
    ELSE
      INSERT INTO Consecutivos (Coddocumento, Codprefijo, Item, Fecha)
      VALUES ('NC7', 'NPOS', 26, '2024-05-01');

  END

END;
COMMIT WORK; 

UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('2', 'Anulacion del documento equivalente electronico');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('3', 'Rebaja o descuento parcial o total');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('4', 'Ajuste de precio');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('5', 'Otros');

COMMIT WORK;


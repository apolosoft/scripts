UPDATE Cocomp SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2023';
UPDATE Cocomp SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Cocomp SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Cocomp SET Nota = REPLACE(Nota, '2018', '2021');
UPDATE Cocomp SET Nota = REPLACE(Nota, '2017', '2020');

UPDATE Comovi SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Comovi SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Comovi SET Nota = REPLACE(Nota, '2018', '2021');
UPDATE Comovi SET Nota = REPLACE(Nota, '2017', '2020');

UPDATE Comprobantes SET Fecha = DATEADD(3 YEAR TO Fecha), Vence = DATEADD(3 YEAR TO Vence), Impreso = DATEADD(3 YEAR TO Impreso);
UPDATE Comprobantes SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Comprobantes SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Comprobantes SET Nota = REPLACE(Nota, '2018', '2021');
UPDATE Comprobantes SET Nota = REPLACE(Nota, '2017', '2020');

UPDATE Tr_Inventario SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Tr_Inventario SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Tr_Inventario SET Nota = REPLACE(Nota, '2018', '2021');
UPDATE Tr_Inventario SET Nota = REPLACE(Nota, '2017', '2020');

UPDATE Nominas SET Fecha = DATEADD(3 YEAR TO Fecha), Desde = DATEADD(3 YEAR TO Desde),
                   Hasta = DATEADD(3 YEAR TO Hasta),
                   Fecha_Procesado = DATEADD(3 YEAR TO Fecha_Procesado), Fecha_Contabilizado = DATEADD(3 YEAR TO Fecha_Contabilizado); --WHERE Fecha < '01/01/2019';
UPDATE Nominas SET Nombre = REPLACE(Nombre, '2020', '2023');
UPDATE Nominas SET Nombre = REPLACE(Nombre, '2019', '2022');
UPDATE Nominas SET Nombre = REPLACE(Nombre, '2018', '2021');
UPDATE Nominas SET Nombre = REPLACE(Nombre, '2017', '2020');

UPDATE Prenominas SET Nombre = REPLACE(Nombre, '2020', '2023');
UPDATE Prenominas SET Nombre = REPLACE(Nombre, '2019', '2022');
UPDATE Prenominas SET Nombre = REPLACE(Nombre, '2018', '2021');
UPDATE Prenominas SET Nombre = REPLACE(Nombre, '2017', '2020');

UPDATE Reg_Nomina SET Detalle = REPLACE(Detalle, '2020', '2023');
UPDATE Reg_Nomina SET Detalle = REPLACE(Detalle, '2019', '2022');
UPDATE Reg_Nomina SET Detalle = REPLACE(Detalle, '2018', '2021');
UPDATE Reg_Nomina SET Detalle = REPLACE(Detalle, '2017', '2020');

UPDATE Aportes_Personal SET Desde = DATEADD(3 YEAR TO Desde), Hasta = DATEADD(3 YEAR TO Hasta); --WHERE Desde < '01/01/2019';

UPDATE Personal_Centros SET Desde = DATEADD(3 YEAR TO Desde), Hasta = DATEADD(3 YEAR TO Hasta); --WHERE Desde < '01/01/2019';

UPDATE Salarios SET Desde = DATEADD(3 YEAR TO Desde), Hasta = DATEADD(3 YEAR TO Hasta); --WHERE Desde < '01/01/2019';

UPDATE Constantes_Personal SET Ano = CAST(CAST(Ano AS NUMERIC) + 3 AS CHAR(4)); --WHERE Ano <> '2019';

UPDATE Generalidades SET Inicio = DATEADD(3 YEAR TO Inicio), Fin = DATEADD(3 YEAR TO Fin); --WHERE Inicio < '01/01/2019';
UPDATE Generalidades SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Generalidades SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Generalidades SET Nota = REPLACE(Nota, '2018', '2021');
UPDATE Generalidades SET Nota = REPLACE(Nota, '2017', '2020');

UPDATE Novedades SET Dia = DATEADD(3 YEAR TO Dia); --WHERE Dia < '01/01/2019';

UPDATE Auto_Comp SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';


UPDATE Reg_Contable SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';
UPDATE Reg_Contable SET Detalle = REPLACE(Detalle, '2020', '2023');
UPDATE Reg_Contable SET Detalle = REPLACE(Detalle, '2019', '2022');
UPDATE Reg_Contable SET Detalle = REPLACE(Detalle, '2018', '2021');
UPDATE Reg_Contable SET Detalle = REPLACE(Detalle, '2017', '2020');

UPDATE Reg_Juego SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';

UPDATE Reg_Niifs SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';

UPDATE Reg_Valorizados SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';

UPDATE Facturas SET Fecha_Envio = DATEADD(3 YEAR TO Fecha_Envio); --WHERE Fecha_Envio < '01/01/2019';

UPDATE Activos SET Inicio = DATEADD(3 YEAR TO Inicio), Fin = DATEADD(3 YEAR TO Fin),
                   Inicio_Niif = DATEADD(3 YEAR TO Inicio_Niif), Fin_Niif = DATEADD(3 YEAR TO Fin_Niif); --WHERE Inicio < '01/01/2019';

UPDATE Depreciaciones SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';
UPDATE Depreciaciones SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Depreciaciones SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Depreciaciones SET Nota = REPLACE(Nota, '2018', '2021');

UPDATE Amortizaciones SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';
UPDATE Amortizaciones SET Nota = REPLACE(Nota, '2020', '2023');
UPDATE Amortizaciones SET Nota = REPLACE(Nota, '2019', '2022');
UPDATE Amortizaciones SET Nota = REPLACE(Nota, '2018', '2021');

UPDATE Conciliaciones SET Desde = DATEADD(3 YEAR TO Desde), Hasta = DATEADD(3 YEAR TO Hasta); --WHERE Desde < '01/01/2019';
UPDATE Conciliaciones SET Nombre = REPLACE(Nombre, '2020', '2023');
UPDATE Conciliaciones SET Nombre = REPLACE(Nombre, '2019', '2022');
UPDATE Conciliaciones SET Nombre = REPLACE(Nombre, '2018', '2021');

UPDATE Consolidaciones SET Fecha = DATEADD(3 YEAR TO Fecha); --WHERE Fecha < '01/01/2019';

UPDATE Lotes SET Llegada = DATEADD(3 YEAR TO Llegada), Vencimiento = DATEADD(3 YEAR TO Vencimiento), Periodo = CAST(CAST(Periodo AS NUMERIC) + 3 AS CHAR(4)); --WHERE Llegada < '01/01/2019';

UPDATE Meses SET Periodo = Periodo + 3 WHERE Periodo = 2020;
UPDATE Meses SET Periodo = Periodo + 3 WHERE Periodo = 2019;
UPDATE Meses SET Periodo = Periodo + 3 WHERE Periodo = 2018;
UPDATE Meses SET Periodo = Periodo + 3 WHERE Periodo = 2017;

UPDATE Reg_Cartera SET Fecha = DATEADD(3 YEAR TO Fecha), Vence = DATEADD(3 YEAR TO Vence); --WHERE Fecha < '01/01/2019';


SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Fe_Vector_Salud_Ref (
    Tipo_     CHAR(5),
    Prefijo_  CHAR(5),
    Numero_   CHAR(10),
    Modulo_   CHAR(10),
    Receptor_ CHAR(15))
RETURNS (
    Nume CHAR(15),
    Cufd CHAR(96),
    Algr CHAR(11),
    Femi CHAR(10),
    Idto CHAR(5),
    Ndoc CHAR(15),
    Oper CHAR(15),
    Reps CHAR(15))
AS
DECLARE VARIABLE V_Pagador CHAR(1);
DECLARE VARIABLE V_Resumen CHAR(10);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
BEGIN
  -- datos del tercero
  SELECT Pagador
  FROM Terceros T
  WHERE Codigo = :Receptor_
  INTO V_Pagador;

  IF (:V_Pagador = 'S') THEN
  BEGIN
    -- Buscar registro resumen
    SELECT Codrips
    FROM Comprobantes
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO :V_Resumen;

    FOR SELECT Tipo,
               Prefijo,
               Numero,
               SUBSTRING(Fecha FROM 1 FOR 10),
               Codtercero,
               Reps
        FROM Fe_Vector_Salud(:V_Resumen) S
        INTO :V_Tipo,
             :V_Prefijo,
             :V_Numero,
             Femi,
             Ndoc,
             Reps

    DO
    BEGIN
      Nume = IIF(TRIM(:V_Prefijo) = '_', '', TRIM(:V_Prefijo)) || TRIM(:V_Numero);

      -- BUSCAR CUFE
      Cufd = '';
      SELECT Cufe
      FROM Facturas
      WHERE Tipo = :V_Tipo
            AND Prefijo = :V_Prefijo
            AND Numero = :V_Numero
      INTO Cufd;
      Cufd = COALESCE(Cufd, '');

      -- Tipo de operacion
      SELECT Tipo_Operacion
      FROM Documentos
      WHERE Codigo = :Tipo_
      INTO Oper;
      Algr = '';
      Algr = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN 'CUFE-SHA384'
               WHEN 'SS-CUDE' THEN 'CUDE-SHA384'
               WHEN 'SS-POS' THEN 'POS'
             END;

      Idto = '';
      Idto = CASE TRIM(Oper)
               WHEN 'SS-CUFE' THEN '01'
               WHEN 'SS-CUDE' THEN '215'
               WHEN 'SS-POS' THEN '230'
             END;

      SUSPEND;
    END
  END
END^

SET TERM ; ^

/* Following GRANT statements are generated automatically */

GRANT SELECT ON TERCEROS TO PROCEDURE PZ_FE_VECTOR_SALUD_REF;
GRANT SELECT ON COMPROBANTES TO PROCEDURE PZ_FE_VECTOR_SALUD_REF;
GRANT EXECUTE ON PROCEDURE FE_VECTOR_SALUD TO PROCEDURE PZ_FE_VECTOR_SALUD_REF;
GRANT SELECT ON FACTURAS TO PROCEDURE PZ_FE_VECTOR_SALUD_REF;
GRANT SELECT ON DOCUMENTOS TO PROCEDURE PZ_FE_VECTOR_SALUD_REF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PZ_FE_VECTOR_SALUD_REF TO SYSDBA;